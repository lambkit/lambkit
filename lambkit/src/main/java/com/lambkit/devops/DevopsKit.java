package com.lambkit.devops;

import cn.hutool.core.date.DateUtil;
import cn.hutool.core.map.MapUtil;
import cn.hutool.core.util.StrUtil;
import com.lambkit.core.Lambkit;
import com.lambkit.core.LambkitConfig;
import com.lambkit.db.DbPool;
import com.lambkit.db.RowData;
import com.lambkit.node.LambkitNodeManager;
import com.lambkit.api.sign.SignKit;
import com.lambkit.api.sign.Signature;

import javax.servlet.http.HttpServletRequest;
import java.util.List;
import java.util.Map;

public class DevopsKit {

	public static String getDatabaseConfigName() {
		return Lambkit.context().getAttr("lambkit.devops.name");
	}

	public static void setDatabaseConfigName(String value) {
		Lambkit.context().setAttr("lambkit.devops.name", value);
	}

	public static Map<String, String> header() {
		String id = Lambkit.config(LambkitConfig.class).getName() + "-" + LambkitNodeManager.me().getNode().getId();
		String secret = DevopsKit.getSetting("lambkit.devops.parent.secret", "gk46buwsrw6cwzkw");
		String key = "devopsForLambkit";
		Signature signature = SignKit.createSignature(key, id, secret);
		Map<String, String> headerMap = MapUtil.newHashMap();
		headerMap.put("DEVOPS-WEB-ID", signature.getId());
		headerMap.put("DEVOPS-WEB-KEY", signature.getKey());
		headerMap.put("DEVOPS-WEB-SIGN", signature.getSign());
		headerMap.put("DEVOPS-WEB-TIME", String.valueOf(signature.getDatetime()));
		return headerMap;
	}
	
	public static Map<String, String> header(HttpServletRequest request) {
		String secret = DevopsKit.getSetting("lambkit.devops.parent.secret", "gk46buwsrw6cwzkw");
		Signature signature = SignKit.copySignature(request, "DEVOPS-WEB", secret);
		Map<String, String> headerMap = MapUtil.newHashMap();
		headerMap.put("DEVOPS-WEB-ID", signature.getId());
		headerMap.put("DEVOPS-WEB-KEY", signature.getKey());
		headerMap.put("DEVOPS-WEB-SIGN", signature.getSign());
		headerMap.put("DEVOPS-WEB-TIME", String.valueOf(signature.getDatetime()));
		return headerMap;
	}
	
	public static SqliteOpt getSqliteOperate() {
		SqliteOpt sqliteOpt = new SqliteOpt();
		sqliteOpt.setPassword("lambkitDevops");
		return sqliteOpt;
	}

	public static String getConfig(String key) {
		return Lambkit.context().getAttr(key);
	}

	public static void setConfig(String key, String value) {
		Lambkit.context().setAttr(key, value);
	}
	
	public static String getSetting(String key, String defaultValue) {
		String value = getSetting(key);
		return StrUtil.isNotBlank(value) ? value : defaultValue;
	}
	
	public static String getSetting(String key) {
		String devlopName = DevopsKit.getDatabaseConfigName();
		if (StrUtil.isBlank(devlopName)) {
			return null;
		}
		RowData record = DbPool.use(devlopName).findFirst("select * from devops_setting where setting_key=?", key);
		if (record == null) {
			//增加默认从config中取值
			String value = Lambkit.configCenter().getValue(key);
			if(StrUtil.isNotBlank(value)) {
				record = DbPool.use(devlopName).newRowData();
				record.set("setting_key", key);
				record.set("setting_value", value);
				record.set("created", DateUtil.now());
				record.setTableName("devops_setting");
				record.setPrimaryKey("setting_id");
				DbPool.use(devlopName).save(record);
			}
			return value;
		}
		return record.getStr("setting_value");
	}

	public static int putSetting(String key, String value) {
		String devlopName = DevopsKit.getDatabaseConfigName();
		if (StrUtil.isBlank(devlopName)) {
			return 0;
		}
		RowData record = DbPool.use(devlopName).findFirst("select * from devops_setting where setting_key=?", key);
		if (record == null) {
			record = DbPool.use(devlopName).newRowData();
			record.set("setting_key", key);
			record.set("setting_value", value);
			record.set("created", DateUtil.now());
			record.setTableName("devops_setting");
			record.setPrimaryKey("setting_id");
			boolean flag = DbPool.use(devlopName).save(record);
			if(flag) {
				logDbTableSetting(key, value);
			}
			return flag ? 1 : 0;
		} else {
			int res = DbPool.use(devlopName).update("update devops_setting set setting_value=? where setting_key=?", value,
					key);
			if(res > 0) {
				logDbTableSetting(key, value);
			}
			return res;
		}
	}
	
	public static void logDbTableSetting(String key, String value) {
		if("lambkit.devops.logdbtask".equals(key)) {
			DevopsLogDbTask logDbTask = new DevopsLogDbTask();
			if("true".equals(value)) {
				logDbTask.onStart();
			} else {
				logDbTask.onStop();
			}
		}
	}

	public static List<RowData> settingList() {
		String devlopName = DevopsKit.getDatabaseConfigName();
		if (StrUtil.isBlank(devlopName)) {
			return null;
		}
		return DbPool.use(devlopName).find("select * from devops_setting");
	}

	public static List<RowData> systemList() {
		String devlopName = DevopsKit.getDatabaseConfigName();
		if (StrUtil.isBlank(devlopName)) {
			return null;
		}
		return DbPool.use(devlopName).find("select * from devops_system");
	}

	public static int putSystem(String system, RowData data) {
		String devlopName = DevopsKit.getDatabaseConfigName();
		if (StrUtil.isBlank(devlopName)) {
			return 0;
		}
		data.remove("system");
		RowData record = DbPool.use(devlopName).findFirst("select * from devops_system where code=?", system);
		if (record == null) {
			record = DbPool.use(devlopName).newRowData();
			record.set("code", system);
			record.setColumns(data);
			record.set("created", DateUtil.now());
			record.setTableName("devops_system");
			record.setPrimaryKey("system_id");
			boolean flag = DbPool.use(devlopName).save(record);
			return flag ? 1 : 0;
		} else {
			data.setTableName("devops_system");
			data.setPrimaryKey("system_id");
			boolean flag = DbPool.use(devlopName).update(data);
			return flag ? 1 : 0;
		}
	}

	public static String shell(String system, String cmd) {
		String devlopName = DevopsKit.getDatabaseConfigName();
		if (StrUtil.isBlank(devlopName)) {
			return null;
		}
		RowData systemRecord = DbPool.use(devlopName).findFirst("select * from devops_system where code=?", system);
		if (systemRecord == null) {
			return null;
		}

		return null;
	}
}
