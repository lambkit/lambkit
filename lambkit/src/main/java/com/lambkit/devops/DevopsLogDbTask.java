package com.lambkit.devops;

import cn.hutool.core.date.DateUtil;
import com.lambkit.util.FileKit;

import java.io.File;
import java.util.Date;

public class DevopsLogDbTask implements Runnable {

	@Override
	public void run() {
		// TODO Auto-generated method stub
		String value = DevopsKit.getSetting("lambkit.devops.logdbtask");
		if("true".equals(value)) {
			onStart();
		}
	}

	public void onStart() {
		String date = DateUtil.format(new Date(), "yyyyMMdd");
		String filePathAndName = "./lambkit/log-" + date + ".db";
		FileKit.createDirectory("./lambkit/");
		File file = new File(filePathAndName);
		SqliteOpt sqliteOpt = DevopsKit.getSqliteOperate();
		if(!file.exists() ) {
			initSqlite(sqliteOpt, filePathAndName);
		}
		sqliteOpt.start("devops_log", filePathAndName);
		DevopsKit.setConfig("lambkit.devops.logname", "devops_log");
		DevopsKit.setConfig("lambkit.devops.logdb", filePathAndName);
	}

	public void onStop() {
		SqliteOpt sqliteOpt = DevopsKit.getSqliteOperate();
		sqliteOpt.stop("devops_log");
		DevopsKit.setConfig("lambkit.devops.logname", null);
	}
	
	public void initSqlite(SqliteOpt sqliteOpt, String filePathAndName) {
		sqliteOpt.setConnection(filePathAndName);
		
		//log4j
		String sql1 = "CREATE TABLE IF NOT EXISTS devops_log_record (\n" 
				+ "	log_id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,\n"
				+ "	pclass TEXT,\n" 
				+ "	method TEXT,\n" 
				+ "	created TEXT,\n"
				+ "	level TEXT,\n" 
				+ "	msg TEXT\n" 
				+ ");";
		sqliteOpt.update(sql1);
		
		//api健康监测log
		String sql2 = "CREATE TABLE IF NOT EXISTS devops_api_status (\n" 
				+ "	api_status_id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,\n"
				+ "	code TEXT,\n" 
				+ "	url TEXT,\n"
				+ "	status INTEGER,\n" 
				+ "	created TEXT\n" 
				+ ");";
		sqliteOpt.update(sql2);
		
		// post上报记录
		String sql3 = "CREATE TABLE IF NOT EXISTS devops_post_record (\n" 
				+ "	post_record_id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,\n"
				+ "	name TEXT,\n"
				+ "	url TEXT,\n"
				+ "	content TEXT,\n"
				+ "	result TEXT,\n"
				+ "	status INTEGER,\n" 
				+ "	created TEXT\n" 
				+ ");";
		sqliteOpt.update(sql3);
		
		sqliteOpt.endConnection();
	}
}
