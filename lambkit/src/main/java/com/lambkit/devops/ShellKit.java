package com.lambkit.devops;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

public class ShellKit {

	public List<String> execCmd(String cmd) throws IOException {
		List<String> list = new ArrayList<>();
		Runtime run = Runtime.getRuntime();
		Process proc = null;
		BufferedReader br = null;
		InputStream in = null;

		try {
			proc = run.exec(new String[] { "/bin/sh", "-c", cmd });
			in = proc.getInputStream();
			br = new BufferedReader(new InputStreamReader(in));

			String result;
			while ((result = br.readLine()) != null) {
				//Printer.print(this, "devops"("job result [" + result + "]");
				list.add(result);
			}
			proc.waitFor();
			return list;
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (proc != null) {
				proc.destroy();
			}
			if (in != null) {
				in.close();
			}
			if (br != null) {
				br.close();
			}
		}
		return list;
	}
}
