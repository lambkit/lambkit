package com.lambkit.web.template;

import cn.hutool.core.util.StrUtil;

import javax.servlet.http.HttpServletRequest;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

public class Template implements Serializable {

    private String id;
    private String type;
    private String title;
    private String description;
    private String author;
    private String authorWebsite;
    private String version;
    private int versionCode;
    private String updateUrl;
    //jfinal模板prefix是前缀，这个存储的是相对地址
    private String folder;
    private String screenshot;
    private String mobileTpl;
    private String wechatTpl;

    private List<String> htmls = new ArrayList<String>();
    private List<String> flags = new ArrayList<String>();

    private String templatePath;

    public Template() {
    }

    public Template(String id) {
        this.id = id;
        this.folder = id;
        this.mobileTpl = "mobile";
        this.wechatTpl = "wechatTpl";
    }

    public Template(String templatePath, Properties templateProp, String templateFolderAbsolutePath) {
        this.templatePath = templatePath;
        this.folder = TemplateManager.me().getTemplatePro().buildFolder(templatePath, templateFolderAbsolutePath);
        //Printer.print(this, "template"(getClass().getName() + " template() folder: " + folder);

        this.id = templateProp.getProperty("id");
        //Printer.print(this, "template"(getClass().getName() + " template() id: " + id);
        this.type = templateProp.getProperty("type");
        this.title = templateProp.getProperty("title");
        this.description = templateProp.getProperty("description");
        this.author = templateProp.getProperty("author");
        this.authorWebsite = templateProp.getProperty("authorWebsite");
        this.version = templateProp.getProperty("version");
        this.updateUrl = templateProp.getProperty("updateUrl");
        this.mobileTpl = templateProp.getProperty("mobileTpl");
        this.wechatTpl = templateProp.getProperty("wechatTpl");


        String vcode = templateProp.getProperty("versionCode");
        this.versionCode = StrUtil.isBlank(vcode) ? 1 : Integer.valueOf(vcode);
        this.screenshot = getWebAbsolutePath() + "/screenshot.png";

        String flagStrings = templateProp.getProperty("flags");
        if (StrUtil.isNotBlank(flagStrings)) {
            String[] strings = flagStrings.split(",");
            for (String s : strings) {
                if (StrUtil.isBlank(s)) {
                    continue;
                }
                this.flags.add(s.trim());
            }
        }
    }

    /**
     * 找出可以用来渲染的 html 模板
     *
     * @param template
     * @return
     */
    public String matchTemplateFile(String template, boolean isMoblieBrowser) {
        return TemplateManager.me().getTemplatePro().matchTemplateFile(htmls, template, isMoblieBrowser);
    }

    public String getFolder(HttpServletRequest request) {
        //Printer.print(this, "template"("Template folder: " + folder);
        return TemplateManager.me().getTemplatePro().getFolder(folder, mobileTpl, wechatTpl, request);
    }

    public String getAbsolutePath() {
        return TemplateManager.me().getTemplatePro().getAbsolutePath(folder, templatePath);
    }

    public String getWebAbsolutePath() {
        return TemplateManager.me().getTemplatePro().getWebAbsolutePath(folder, templatePath);
    }

    /**
     * 获得某个模块下支持的样式
     * 一般用于在后台设置
     *
     * @param prefix
     * @return
     */
    public List<String> getSupportStyles(String prefix) {
        return TemplateManager.me().getTemplatePro().getSupportStyles(htmls, prefix);
    }

    public void uninstall() {
        TemplateManager.me().getTemplatePro().uninstall(folder, templatePath);
    }

    ///////////////////////////////////////////////////////////////////

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public String getAuthorWebsite() {
        return authorWebsite;
    }

    public void setAuthorWebsite(String authorWebsite) {
        this.authorWebsite = authorWebsite;
    }

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    public int getVersionCode() {
        return versionCode;
    }

    public void setVersionCode(int versionCode) {
        this.versionCode = versionCode;
    }

    public String getUpdateUrl() {
        return updateUrl;
    }

    public void setUpdateUrl(String updateUrl) {
        this.updateUrl = updateUrl;
    }

    public String getFolder() {
        return folder;
    }

    public void setFolder(String folder) {
        this.folder = folder;
    }

    public String getScreenshot() {
        return screenshot;
    }

    public void setScreenshot(String screenshot) {
        this.screenshot = screenshot;
    }

    public List<String> getFlags() {
        return flags;
    }

    public void setFlags(List<String> flags) {
        this.flags = flags;
    }

	public String getMobileTpl() {
		return mobileTpl;
	}

	public void setMobileTpl(String mobileTpl) {
		this.mobileTpl = mobileTpl;
	}

	public String getWechatTpl() {
		return wechatTpl;
	}

	public void setWechatTpl(String wechatTpl) {
		this.wechatTpl = wechatTpl;
	}

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public List<String> getHtmls() {
        return htmls;
    }

    public void setHtmls(List<String> htmls) {
        this.htmls = htmls;
    }

    public String getTemplatePath() {
        return templatePath;
    }

    public void setTemplatePath(String templatePath) {
        this.templatePath = templatePath;
    }
}