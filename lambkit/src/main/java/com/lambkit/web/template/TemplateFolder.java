package com.lambkit.web.template;

import cn.hutool.core.map.MapUtil;
import cn.hutool.core.util.StrUtil;
import cn.hutool.log.StaticLog;
import com.lambkit.core.Lambkit;

import javax.servlet.http.HttpServletRequest;
import java.io.Serializable;
import java.util.List;
import java.util.Map;

public class TemplateFolder implements Serializable {

    private String currentTemplateId;
	private String templatePath = "/templates";//classpath:templates
    private Map<String, Template> templateMap = MapUtil.newHashMap();

    public TemplateFolder(String type, String templateId) {
        this.currentTemplateId = templateId;
        initDefaultTemplate(type, templateId);
    }

    public TemplateFolder(String templatePath, String type, String templateId) {
        this.templatePath = templatePath;
        this.currentTemplateId = templateId;
        initDefaultTemplate(type, templateId);
    }

    /**
     * 扫描模板目录，生成模板对象
     */
    private void initDefaultTemplate(String type, String templateId) {
        if (StrUtil.isBlank(templateId)) {
            return;
        }
        if(Lambkit.context().getDevMode()) {
            StaticLog.info("initDefaultTemplate type: " + type + ", id: " + templateId);
        }
        List<Template> templates = TemplateManager.me().getTemplateFolderPro().getInstalledTemplates(templatePath, type, templateId);
        if (templates == null || templates.isEmpty()) {
            StaticLog.warn("can not find tempalte " + templateId);
        } else {
            for(Template template : templates) {
                templateMap.put(template.getId(), template);
            }
        }
    }

    public Template getCurrentTemplate() {
        return templateMap.get(currentTemplateId);
    }

    public void setCurrentTemplate(String templateId) {
        this.currentTemplateId = templateId;
    }

    public Template getTemplateById(String templateId) {
        return templateMap.get(templateId);
    }

    /**
     * html生成的时候，获取模板地址
     * @param request
     * @return
     */
    public String getCurrentWebPath(HttpServletRequest request) {
        //jfinal模板prefix是前缀，这个获取的是相对地址
        return TemplateManager.me().getTemplateFolderPro().getCurrentWebPath(this, request);
	}
	
	public String getCurrentFilePath(HttpServletRequest request, String file) {
        return TemplateManager.me().getTemplateFolderPro().getCurrentFilePath(this, request, file);
	}

    //////////////////////////////////////////////////////////////////////
    
    public String getTemplatePath() {
		return templatePath;
	}
    
    public void setTemplatePath(String templatePath) {
		this.templatePath = templatePath;
	}

    public Map<String, Template> getTemplateMap() {
        return templateMap;
    }

    public String getCurrentTemplateId() {
        return currentTemplateId;
    }
}