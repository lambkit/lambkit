package com.lambkit.web.template;

import cn.hutool.core.map.MapUtil;
import com.lambkit.core.Lambkit;

import javax.servlet.http.HttpServletRequest;
import java.io.File;
import java.util.Map;

public class TemplateManager {

    public static final int PATH_TYPE_WEBAPP = 0;
    public static final int PATH_TYPE_CLASSPATH = 1;
    public static final int PATH_TYPE_RESOURCES = 2;

    public static final int PATH_TYPE_FILE = 3;
    public static final int PATH_TYPE_DATABASE = 4;
    /**
     *  0-webapp,1-resources
     */
    private int templatePathType = PATH_TYPE_WEBAPP;
    private String baseTemplatePath = null;
	private String defaultTemplatePath = "/templates";

    private String prefix = "";

    private Map<String, TemplateFolder> currentTemplate = MapUtil.newHashMap();

    private static final TemplateManager me = new TemplateManager();

    private ITemplatePro templatePro;

    private ITemplateFolderPro templateFolderPro;

    private TemplateManager() {
    }

    public static TemplateManager me() {
        return me;
    }

    public void init(String type, String templateId) {
        TemplateFolder templateFolder = new TemplateFolder(defaultTemplatePath, type, templateId);
        currentTemplate.put(type, templateFolder);
    }

    public void init(String type, String templatePath, String templateId) {
        TemplateFolder templateFolder = new TemplateFolder(templatePath, type, templateId);
        currentTemplate.put(type, templateFolder);
    }

    public TemplateFolder getTemplateFolder(String type) {
        return currentTemplate.get(type);
    }

    public Template getTemplateById(String type, String id) {
        TemplateFolder templateFolder = getTemplateFolder(type);
        return templateFolder != null ? templateFolder.getTemplateById(id) : null;
    }

    public Template getCurrentTemplate(String type) {
        TemplateFolder templateFolder = getTemplateFolder(type);
        return templateFolder != null ? templateFolder.getCurrentTemplate() : null;
    }

    public String getCurrentWebPath(String type, HttpServletRequest request) {
        TemplateFolder templateFolder = getTemplateFolder(type);
        //Printer.print(this, "template"("Current Template Folder: " + templateFolder.getTemplatePath() + ", " + templateFolder.getCurrentTemplateId());
        return templateFolder != null ? templateFolder.getCurrentWebPath(request) : defaultTemplatePath;
	}
	
	public String getCurrentWebPath(String type, HttpServletRequest request, String file) {
		return getCurrentWebPath(type, request) + "/" + file;
	}
	
	public String getCurrentFilePath(String type, HttpServletRequest request, String file) {
        TemplateFolder templateFolder = getTemplateFolder(type);
        if(templateFolder!=null) {
            return templateFolder.getCurrentFilePath(request, file);
        } else {
            return Lambkit.context().resourceService().getWebRootPath() + defaultTemplatePath + File.separator + file;
        }
	}

    public String getDefaultTemplatePath() {
        return defaultTemplatePath;
    }

    public void setDefaultTemplatePath(String defaultTemplatePath) {
        this.defaultTemplatePath = defaultTemplatePath;
    }

    public int getTemplatePathType() {
        return templatePathType;
    }

    public void setTemplatePathType(int templatePathType) {
        this.templatePathType = templatePathType;
    }

    public ITemplateFolderPro getTemplateFolderPro() {
        return templateFolderPro;
    }

    public ITemplatePro getTemplatePro() {
        return templatePro;
    }

    public void setTemplateFolderPro(ITemplateFolderPro templateFolderPro) {
        this.templateFolderPro = templateFolderPro;
    }

    public void setTemplatePro(ITemplatePro templatePro) {
        this.templatePro = templatePro;
    }

    public String getBaseTemplatePath() {
        return baseTemplatePath;
    }

    public void setBaseTemplatePath(String baseTemplatePath) {
        this.baseTemplatePath = baseTemplatePath;
    }

    public String getPrefix() {
        return prefix;
    }

    public void setPrefix(String prefix) {
        this.prefix = prefix;
    }
}