/**
 * Copyright (c) 2015-2017, Henry Yang 杨勇 (gismail@foxmail.com).
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.lambkit.web.enjoy;

import com.jfinal.template.Env;
import com.jfinal.template.io.Writer;
import com.jfinal.template.stat.Scope;
import com.lambkit.db.DbPool;
import com.lambkit.db.PageData;
import com.lambkit.db.RowData;
import com.lambkit.db.Sql;

/**
 * #findPage("select * from table",1,10) #for(x:model.getList())
 * <li>#(x.id)</li> #end #end
 * 
 * @author 孤竹行
 */
//@JFinalDirective("findPage")
public class FindPaginate extends LambkitDirective {

	@Override
	public void onRender(Env env, Scope scope, Writer writer) {
		// TODO Auto-generated method stub
		String key = "model";
		String sql = getPara(0, scope);
		int pageNum = (Integer) exprList.getExpr(1).eval(scope);
		int pageSize = (Integer) exprList.getExpr(2).eval(scope);
		Sql sqlEntity = new Sql(sql);
		if (exprList.length() > 3) {
			for (int i = 3; i < exprList.length(); i++) {
				sqlEntity.addPara(getPara(i, scope));
			}
		}
		PageData<RowData> page = DbPool.use().paginate(pageNum, pageSize, sqlEntity);
		scope.set(key, page);
		renderBody(env, scope, writer);// 执行自定义标签中包围的 html
	}

	@Override
	public boolean hasEnd() {
		return true;
	}
}
