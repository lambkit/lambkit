/**
 * Copyright (c) 2015-2017, Henry Yang 杨勇 (gismail@foxmail.com).
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.lambkit.web;

import cn.hutool.core.util.StrUtil;
import com.lambkit.core.config.annotation.PropConfig;

import java.io.Serializable;

@PropConfig(prefix = "lambkit.web")
public class WebConfig implements Serializable {
	public static final String NAME_DEFAULT = "default";
	
	private String name;
	private String url;//主页地址
	private String ctx;//外部ctx地址
	private String template = "default";//模板
	private boolean jsonp = false;
	private boolean excel = false;

	//验证码默认过期时长 180 秒
	private int captchaExpireTime = 180;

	private String errorRenderType = "redirect";

	public boolean isConfigOk() {
		// TODO Auto-generated method stub
		return StrUtil.isNotBlank(name) && StrUtil.isNotBlank(url);
	}
	
	public boolean isExcel() {
		return excel;
	}
	public void setExcel(boolean excel) {
		this.excel = excel;
	}
	public boolean isJsonp() {
		return jsonp;
	}
	public void setJsonp(boolean jsonp) {
		this.jsonp = jsonp;
	}
	public String getUrl() {
		return url;
	}
	public void setUrl(String url) {
		this.url = url;
	}
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}

	public String getTemplate() {
		return template;
	}

	public void setTemplate(String template) {
		this.template = template;
	}

	public String getCtx() {
		return ctx;
	}

	public void setCtx(String ctx) {
		this.ctx = ctx;
	}

	public int getCaptchaExpireTime() {
		return captchaExpireTime;
	}

	public void setCaptchaExpireTime(int captchaExpireTime) {
		this.captchaExpireTime = captchaExpireTime;
	}

	public String getErrorRenderType() {
		return errorRenderType;
	}

	public void setErrorRenderType(String errorRenderType) {
		this.errorRenderType = errorRenderType;
	}
}
