/**
 * Copyright (c) 2015-2017, Henry Yang 杨勇 (gismail@foxmail.com).
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.lambkit.web;

import cn.hutool.core.util.StrUtil;
import com.jfinal.kit.Kv;
import com.lambkit.core.LambkitResult;

public class AjaxResult extends LambkitResult {

	/**
	 * 
	 */
	private static final long serialVersionUID = 6639387523151265051L;

	// 类型
    private String type;

	// 数据总数
	private Integer count;

	public AjaxResult() {
	}
    
	public AjaxResult(int code, String message, Object data) {
		setCode(code);
		setMessage(message);
		setData(data);
		setSuccess(getCode() == 1 ? true : false);
	}
	
	public AjaxResult(int code, String type, String message, Object data) {
		setCode(code);
		setMessage(message);
		setData(data);
		setSuccess(getCode() == 1 ? true : false);
		this.type = type;
		if(StrUtil.isNotBlank(type) && !"default".equalsIgnoreCase(type)) {
			if(code==1) {
				setCode(0);
			}
			else if(code==0) {
				setCode(1);
			}
		}
	}

	public static AjaxResult success() {
		AjaxResult ajaxResult = new AjaxResult();
		ajaxResult.setCode(1);
		ajaxResult.setSuccess(true);
		return ajaxResult;
	}

	public static AjaxResult success(String type) {
		AjaxResult ajaxResult = new AjaxResult();
		ajaxResult.setType(type);
		if(StrUtil.isNotBlank(type) && !"default".equalsIgnoreCase(type)) {
			ajaxResult.setCode(0);
		} else {
			ajaxResult.setCode(1);
		}
		ajaxResult.setSuccess(true);
		return ajaxResult;
	}

	public static AjaxResult success(int code) {
		AjaxResult ajaxResult = new AjaxResult();
		ajaxResult.setCode(code);
		ajaxResult.setSuccess(true);
		return ajaxResult;
	}

	public static AjaxResult fail() {
		AjaxResult ajaxResult = new AjaxResult();
		ajaxResult.setCode(0);
		ajaxResult.setSuccess(false);
		return ajaxResult;
	}

	public static AjaxResult fail(String type) {
		AjaxResult ajaxResult = new AjaxResult();
		ajaxResult.setType(type);
		if(StrUtil.isNotBlank(type) && !"default".equalsIgnoreCase(type)) {
			ajaxResult.setCode(1);
		} else {
			ajaxResult.setCode(0);
		}
		ajaxResult.setSuccess(false);
		return ajaxResult;
	}

	public static AjaxResult fail(int code) {
		AjaxResult ajaxResult = new AjaxResult();
		ajaxResult.setCode(code);
		ajaxResult.setSuccess(false);
		return ajaxResult;
	}
	
//	public static AjaxResult code(int code) {
//		AjaxResult ajaxResult = new AjaxResult();
//		ajaxResult.setCode(code);
//		ajaxResult.setSuccess(code == 1 ? true : false);
//		return ajaxResult;
//	}

	public static AjaxResult code(int code, String type) {
		AjaxResult ajaxResult = new AjaxResult();
		ajaxResult.setType(type);
		ajaxResult.setSuccess(code == 1 ? true : false);
		if(StrUtil.isNotBlank(type) && !"default".equalsIgnoreCase(type)) {
			if(code==1) {
				ajaxResult.setCode(0);
			}
			else if(code==0) {
				ajaxResult.setCode(1);
			}
		}
		return ajaxResult;
	}

	public static AjaxResult by(ResultConsts resultConsts) {
		AjaxResult ajaxResult = new AjaxResult();
		ajaxResult.setCode(resultConsts.code);
		ajaxResult.setMessage(resultConsts.message);
		ajaxResult.setSuccess(resultConsts.code == 1 ? true : false);
		return ajaxResult;
	}

	public AjaxResult message(String message) {
		this.setMessage(message);
		return this;
	}

	public AjaxResult data(Object data) {
		this.setData(data);
		return this;
	}

	public AjaxResult data(String key, Object value) {
		this.setData(Kv.by(key, value));
		return this;
	}

	public AjaxResult count(int count) {
		this.count = count;
		return this;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public Integer getCount() {
		return count;
	}

	public void setCount(Integer count) {
		this.count = count;
	}

}
