package com.lambkit.web.log;

import com.lambkit.core.http.IHttpContext;

/**
 * UV独立访客日活日志
 */
public interface UniqueVisitorLogService {
    void log(IHttpContext context);
}
