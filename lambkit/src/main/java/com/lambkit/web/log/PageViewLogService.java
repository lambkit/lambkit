package com.lambkit.web.log;


import com.lambkit.core.http.IHttpContext;

/**
 * PV访问量日志
 */
public interface PageViewLogService {
    void log(IHttpContext context);
}
