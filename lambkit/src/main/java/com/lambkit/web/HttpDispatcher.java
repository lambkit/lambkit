package com.lambkit.web;

import com.lambkit.core.Lambkit;
import com.lambkit.core.http.IHttpContext;
import com.lambkit.core.task.http.HttpWorkCenter;

public class HttpDispatcher {
    private HttpWorkCenter workCenter;

    public void init(HttpWorkCenter workCenter) {
        this.workCenter = workCenter;
    }

    public boolean start(IHttpContext context) {
        HttpContextHolder.hold(context);
        if(workCenter == null) {
            workCenter =  Lambkit.app().context().getWorkCenter(HttpWorkCenter.class);
        }
        if(workCenter != null) {
            return workCenter.handle(context);
        }
        return false;
    }

    public void stop() {
        HttpContextHolder.release();
    }
}
