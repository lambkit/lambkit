package com.lambkit.plugin.sqlite;

import com.lambkit.core.config.annotation.PropConfig;

import java.io.File;

@PropConfig(prefix = "lambkit.sqlite")
public class SqliteConfig {

	private boolean enable = false;
	private String url = "./lambkit";
	private String name = "config";
	private String username;

	private String password;

	public boolean isEnable() {
		return enable;
	}

	public void setEnable(boolean enable) {
		this.enable = enable;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}
}
