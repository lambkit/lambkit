package com.lambkit.plugin.registry;

import cn.hutool.core.util.StrUtil;
import com.lambkit.core.Lambkit;
import com.lambkit.core.registry.*;

/**
 * @author yangyong(孤竹行)
 */
public class LambkitRegisterCenter implements RegisterCenter {

    private final String CacheNamePrefix = "RegisterCenter";
    private String cacheType = "ehcache";//"redis";

    @Override
    public ServiceInstance getInstance(ServiceInstanceVo serviceInstanceVo) {
        String serviceName = serviceInstanceVo.getServiceName();
        String namespaceId = serviceInstanceVo.getNamespaceId();
        String groupName = serviceInstanceVo.getGroupName();
        ServiceInstanceList serviceInstanceList = getInstanceList(namespaceId, groupName, serviceName);
        return serviceInstanceList.getInstance(serviceInstanceVo);
    }

    @Override
    public ServiceInstanceList getInstanceList(ServiceInstanceListVo serviceInstanceListVo) {
        String serviceName = serviceInstanceListVo.getServiceName();
        String namespaceId = serviceInstanceListVo.getNamespaceId();
        String groupName = serviceInstanceListVo.getGroupName();
        ServiceInstanceList serviceInstanceList = getInstanceList(namespaceId, groupName, serviceName);
        return serviceInstanceList;
    }

    protected ServiceInstanceList getInstanceList(String namespaceId, String groupName, String serviceName) {
        namespaceId = StrUtil.isBlank(namespaceId) ? "public" : namespaceId;
        groupName = StrUtil.isBlank(groupName) ? "default" : groupName;
        ServiceInstanceList serviceInstanceList = Lambkit.getCache(cacheType).get(CacheNamePrefix + "#" + namespaceId , groupName + "@" + serviceName);
        return serviceInstanceList;
    }

    protected void setInstanceList(ServiceInstanceList serviceInstanceList, String namespaceId, String groupName, String serviceName) {
        namespaceId = StrUtil.isBlank(namespaceId) ? "public" : namespaceId;
        groupName = StrUtil.isBlank(groupName) ? "default" : groupName;
        Lambkit.getCache(cacheType).put(CacheNamePrefix + "#" + namespaceId , groupName + "@" + serviceName, serviceInstanceList, serviceInstanceList.getCacheMillis() / 1000);
    }

    @Override
    public boolean createInstance(ServiceInstance serviceInstance) {
        String serviceName = serviceInstance.getServiceName();
        String namespaceId = serviceInstance.getNamespaceId();
        String groupName = serviceInstance.getGroupName();
        ServiceInstanceList serviceInstanceList = getInstanceList(namespaceId, groupName, serviceName);
        if(serviceInstanceList==null) {
            serviceInstanceList = new ServiceInstanceList();
            serviceInstanceList.setName(groupName + "@" + serviceName);
            serviceInstanceList.setGroupName(groupName);
            serviceInstanceList.setValid(true);
            serviceInstanceList.setLastRefTime(System.currentTimeMillis());
            serviceInstanceList.addInstance(serviceInstance);
            setInstanceList(serviceInstanceList, namespaceId, groupName, serviceName);
            return true;
        } else {
            return serviceInstanceList.addInstance(serviceInstance);
        }
    }

    @Override
    public boolean updateInstance(ServiceInstance serviceInstance) {
        String serviceName = serviceInstance.getServiceName();
        String namespaceId = serviceInstance.getNamespaceId();
        String groupName = serviceInstance.getGroupName();
        ServiceInstanceList serviceInstanceList = getInstanceList(namespaceId, groupName, serviceName);
        if(serviceInstanceList==null) {
            return false;
        } else {
            serviceInstanceList.addInstance(serviceInstance);
        }
        return true;
    }

    @Override
    public boolean removeInstance(ServiceInstance serviceInstance) {
        String serviceName = serviceInstance.getServiceName();
        String namespaceId = serviceInstance.getNamespaceId();
        String groupName = serviceInstance.getGroupName();
        ServiceInstanceList serviceInstanceList = getInstanceList(namespaceId, groupName, serviceName);
        if(serviceInstanceList==null) {
            return false;
        } else {
            return serviceInstanceList.removeInstance(serviceInstance);
        }
    }
}
