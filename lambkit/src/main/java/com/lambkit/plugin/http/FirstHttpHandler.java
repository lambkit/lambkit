package com.lambkit.plugin.http;

import cn.hutool.core.util.StrUtil;
import com.lambkit.core.http.*;

/**
 * @author yangyong(孤竹行)
 */
public class FirstHttpHandler implements Handler {
    public boolean handleBefore(IHttpContext httpContext, HttpAction action) {
        String target = httpContext.getTarget();
        if (target.indexOf('.') != -1 && !target.startsWith("/lambkit/lms/druid")) {
            //资源文件
            return false;
        }
        /// 非浏览器请求返回失败
        String userAgent = httpContext.getRequest().getHeader("user-agent");
        if (!StrUtil.isNotBlank(userAgent)) {
            httpContext.renderError(404, "");
            return false;
        }
        httpContext.setCookie("lambkit", "", -1, "/", null, false, "strict");
        return true;
    }
}
