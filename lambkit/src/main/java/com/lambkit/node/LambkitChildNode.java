package com.lambkit.node;

import com.lambkit.node.c2p.LambkitNodeC2p;

/**
 * @author yangyong(孤竹行)
 */
public class LambkitChildNode extends LambkitNodeC2p {

    /**
     * 提交数据时间
     */
    private long commitTime;
    /**
     * 启动时间
     */
    private long startTime;


    private boolean expire = false;

    public long getCommitTime() {
        return commitTime;
    }

    public void setCommitTime(long commitTime) {
        this.commitTime = commitTime;
    }

    public long getStartTime() {
        return startTime;
    }

    public void setStartTime(long startTime) {
        this.startTime = startTime;
    }

    public boolean isExpire() {
        return expire;
    }

    public void setExpire(boolean expire) {
        this.expire = expire;
    }
}
