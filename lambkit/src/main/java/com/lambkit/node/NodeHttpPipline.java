package com.lambkit.node;

import java.util.List;

public class NodeHttpPipline {

	private NodeHttpAction action;
	
	private List<NodeHttpInterceptor> inters;
	
	private int index = 0;
	
	public NodeHttpPipline(NodeHttpAction action, List<NodeHttpInterceptor> inters) {
		this.action = action;
		this.inters = inters;
	}
	
	public void invoke() {
		if (index < inters.size()) {
			inters.get(index).intercept(this);
		}
		else if (index++ == inters.size()) {	// index++ ensure invoke action only one time
			action.run();
		}
	}
	
	public NodeHttpAction getAction() {
		return action;
	}
}
