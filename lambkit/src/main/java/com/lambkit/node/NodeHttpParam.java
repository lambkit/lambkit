package com.lambkit.node;

import java.io.Serializable;
import java.util.Map;

public class NodeHttpParam implements Serializable {

	private static final long serialVersionUID = -5582883091525322097L;
	/**
	 * 节点ID
	 */
	private String nodeId;
	/**
	 * 应用ID
	 */
	private String appId;
	/**
	 * 请求时间
	 */
	private String requestTime;
	/**
	 * token
	 */
	private String token;
	/**
	 * 跟踪ID
	 */
	private String traceId;
	/**
	 * 请求响应的service名称
	 */
	private String service;
	/**
	 * 请求响应的action名称
	 */
	private String method;
	/**
	 * 请求响应的method的参数
	 */
	private Map<String, Object> param;

	public String getNodeId() {
		return nodeId;
	}

	public void setNodeId(String nodeId) {
		this.nodeId = nodeId;
	}

	public String getAppId() {
		return appId;
	}

	public void setAppId(String appId) {
		this.appId = appId;
	}

	public String getRequestTime() {
		return requestTime;
	}

	public void setRequestTime(String requestTime) {
		this.requestTime = requestTime;
	}

	public String getToken() {
		return token;
	}

	public void setToken(String token) {
		this.token = token;
	}

	public String getTraceId() {
		return traceId;
	}

	public void setTraceId(String traceId) {
		this.traceId = traceId;
	}

	public String getService() {
		return service;
	}

	public void setService(String service) {
		this.service = service;
	}

	public String getMethod() {
		return method;
	}

	public void setMethod(String method) {
		this.method = method;
	}

	public Map<String, Object> getParam() {
		return param;
	}

	public void setParam(Map<String, Object> param) {
		this.param = param;
	}

}
