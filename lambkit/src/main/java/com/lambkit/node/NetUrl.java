package com.lambkit.node;

import cn.hutool.core.util.StrUtil;

import java.io.Serializable;
import java.net.InetAddress;
import java.net.NetworkInterface;
import java.net.SocketException;
import java.net.UnknownHostException;
import java.util.Enumeration;

/**
 * 网络地址
 * 
 * @author yangyong
 *
 */
public class NetUrl implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1255554062123581256L;

	/**
	 * 网络名称
	 */
	private String group = "internet";
	private String protocal = "http";
	private String domain;
	/**
	 * ctx,request.contextPath
	 */
	//private String contexPath;
	private int port = 80;

	public NetUrl() {
		InetAddress localHost;
		try {
			localHost = InetAddress.getLocalHost();
			// String hostName = localHost.getHostName();
			domain = localHost.getHostAddress();
		} catch (UnknownHostException e) {
			e.printStackTrace();
		}
	}

	public NetUrl(String domain, int port) {
		
		this.domain = domain;
		this.port = port;
	}

	public NetUrl(String group, String domain, int port) {
		
		this.group = group;
		this.domain = domain;
		this.port = port;
	}

	public NetUrl(String group, String protocal, String domain, int port) {
		
		this.group = group;
		this.protocal = protocal;
		this.domain = domain;
		this.port = port;
	}
	
	public String url() {
		StringBuffer strb = new StringBuffer();
		strb.append(protocal).append("://").append(domain).append(":").append(port);
		return strb.toString();
	}
	
	public String simpleUrl() {
		StringBuffer strb = new StringBuffer();
		strb.append(domain).append(":").append(port);
		return strb.toString();
	}

	public void init(int port) {
		if (StrUtil.isBlank(domain)) {
			String ip = "127.0.0.1";
			try {
				// host = getLocalHostName();
				ip = getLocalIP();
			} catch (Exception e) {
			}
			//int p = LambkitBeanManager.me().getPort();
			this.domain = ip;
			this.port = port;
		}
	}

	public boolean isUsable() {
		return (StrUtil.isNotBlank(domain) && port > 0);
	}

	/**
	 * 获取本地IP地址
	 *
	 * @throws SocketException
	 */
	private String getLocalIP() throws Exception {
		if (isWindowsOS()) {
			return InetAddress.getLocalHost().getHostAddress();
		} else {
			return getLinuxLocalIp();
		}
	}

	/**
	 * 判断操作系统是否是Windows
	 *
	 * @return
	 */
	private boolean isWindowsOS() {
		boolean isWindowsOS = false;
		String osName = System.getProperty("os.name");
		if (osName.toLowerCase().indexOf("windows") > -1) {
			isWindowsOS = true;
		}
		return isWindowsOS;
	}

	/**
	 * 获取本地Host名称
	 */
//	private String getLocalHostName() throws UnknownHostException {
//		return InetAddress.getLocalHost().getHostName();
//	}

	/**
	 * 获取Linux下的IP地址
	 *
	 * @return IP地址
	 * @throws SocketException
	 */
	private String getLinuxLocalIp() throws SocketException {
		String ip = "";
		try {
			for (Enumeration<NetworkInterface> en = NetworkInterface.getNetworkInterfaces(); en.hasMoreElements();) {
				NetworkInterface intf = en.nextElement();
				String name = intf.getName();
				if (!name.contains("docker") && !name.contains("lo")) {
					for (Enumeration<InetAddress> enumIpAddr = intf.getInetAddresses(); enumIpAddr.hasMoreElements();) {
						InetAddress inetAddress = enumIpAddr.nextElement();
						if (!inetAddress.isLoopbackAddress()) {
							String ipaddress = inetAddress.getHostAddress().toString();
							if (!ipaddress.contains("::") && !ipaddress.contains("0:0:")
									&& !ipaddress.contains("fe80")) {
								ip = ipaddress;
								// Printer.print(this, "node"(ipaddress);
							}
						}
					}
				}
			}
		} catch (SocketException ex) {
			// Printer.print(this, "node"("获取ip地址异常");
			ip = "127.0.0.1";
			ex.printStackTrace();
		}
		// Printer.print(this, "node"("IP:" + ip);
		return ip;
	}
	

	/**
	 * 获取当前机器的IP
	 * 
	 * @throws UnknownHostException
	 */
//	public String getLocalIP() throws Exception {
//		InetAddress addr = InetAddress.getLocalHost();
//		byte[] ipAddr = addr.getAddress();
//		String ipAddrStr = "";
//		for (int i = 0; i < ipAddr.length; i++) {
//			if (i > 0) {
//				ipAddrStr += ".";
//			}
//			ipAddrStr += ipAddr[i] & 0xFF;
//		}
//		return ipAddrStr;
//	}

	public String getGroup() {
		return group;
	}

	public void setGroup(String group) {
		this.group = group;
	}

	public String getProtocal() {
		return protocal;
	}

	public void setProtocal(String protocal) {
		this.protocal = protocal;
	}

	public String getDomain() {
		return domain;
	}

	public void setDomain(String domain) {
		this.domain = domain;
	}

	public int getPort() {
		return port;
	}

	public void setPort(int port) {
		this.port = port;
	}

}
