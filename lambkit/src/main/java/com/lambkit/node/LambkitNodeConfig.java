package com.lambkit.node;

import com.lambkit.core.config.annotation.PropConfig;

@PropConfig(prefix = "lambkit.node")
public class LambkitNodeConfig {
	/**
	 * 节点ID
	 */
	private String id;
	/**
	 * 应用名称
	 */
	private String applicationName;
	
	private String type = "normal";

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getApplicationName() {
		return applicationName;
	}

	public void setApplicationName(String applicationName) {
		this.applicationName = applicationName;
	}
}
