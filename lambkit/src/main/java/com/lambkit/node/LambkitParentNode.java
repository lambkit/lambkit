package com.lambkit.node;

import com.lambkit.core.Lambkit;
import com.lambkit.node.c2p.LambkitNodeC2pConfig;
import com.lambkit.node.parent.LambkitNodeP2c;

/**
 * @author yangyong(孤竹行)
 */
public class LambkitParentNode extends LambkitNodeP2c {

    /**
     * 提交数据时间
     */
    private long commitTime;

    private String url;

    public LambkitParentNode() {

    }

    public LambkitParentNode(LambkitNodeP2c node) {
        super(node);
        LambkitNodeC2pConfig c2pConfig = Lambkit.config(LambkitNodeC2pConfig.class);
        this.url = c2pConfig.getPurl();
        this.commitTime = System.currentTimeMillis();
    }

    public long getCommitTime() {
        return commitTime;
    }

    public void setCommitTime(long commitTime) {
        this.commitTime = commitTime;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }
}
