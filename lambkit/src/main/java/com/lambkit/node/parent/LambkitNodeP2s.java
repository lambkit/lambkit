package com.lambkit.node.parent;

import com.lambkit.node.LambkitChildNode;
import com.lambkit.node.LambkitNodeManager;
import com.lambkit.node.NetNode;
import com.lambkit.node.NetUrl;
import com.lambkit.node.c2p.LambkitNodeC2p;

import java.util.Collection;

/**
 * 网络节点
 * 
 * @author yangyong
 *
 */
public class LambkitNodeP2s implements NetNode {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * id
	 */
	private String id;
	/**
	 * 用户的key
	 */
	private String key;
	/**
	 * 树结构的层级
	 */
	private String applicationName;
	/**
	 * 主要地址
	 */
	private NetUrl url;
	/**
	 * 父节点
	 */
	private String parentNodeUrl;
	/**
	 * 父子连通情况
	 */
	private int linkType = 1;// 0单向，1双向
	
	private String type = "normal";

	private Collection<LambkitChildNode> children;
	public LambkitNodeP2s() {

	}

	public LambkitNodeP2s(LambkitNodeC2p node) {
		if(node!=null) {
			this.id = node.getId();
			this.applicationName = node.getApplicationName();
			this.type = node.getType();
			this.key = node.getKey();
			this.url = node.getUrl();
			this.parentNodeUrl = node.getParentNodeUrl();
			this.linkType = node.getLinkType();
			this.children = LambkitNodeManager.me().getChildNodes().values();
		}
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public int getLinkType() {
		return linkType;
	}

	public void setLinkType(int linkType) {
		this.linkType = linkType;
	}

	public String getParentNodeUrl() {
		return parentNodeUrl;
	}

	public void setParentNodeUrl(String parentNodeUrl) {
		this.parentNodeUrl = parentNodeUrl;
	}

	public String getKey() {
		return key;
	}

	public void setKey(String key) {
		this.key = key;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getApplicationName() {
		return applicationName;
	}

	public void setApplicationName(String applicationName) {
		this.applicationName = applicationName;
	}

	public NetUrl getUrl() {
		return url;
	}

	public void setUrl(NetUrl url) {
		this.url = url;
	}

	public Collection<LambkitChildNode> getChildren() {
		return children;
	}

	public void setChildren(Collection<LambkitChildNode> children) {
		this.children = children;
	}
}
