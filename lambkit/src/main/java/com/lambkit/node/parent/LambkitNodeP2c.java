package com.lambkit.node.parent;

import com.lambkit.node.LambkitNode;
import com.lambkit.node.LambkitNodeManager;
import com.lambkit.node.LambkitSiblingNode;

import java.io.Serializable;

/**
 * 网络节点
 * 
 * @author yangyong
 *
 */
public class LambkitNodeP2c implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * id
	 */
	private String id;
	/**
	 * 树结构的层级
	 */
	private int level;
	private String applicationName;
	private LambkitSiblingNode uncleNode;

	public LambkitNodeP2c() {

	}

	public LambkitNodeP2c(LambkitNode node) {
		if(node!=null) {
			this.id = node.getId();
			this.applicationName = node.getApplicationName();
			this.level = node.getLevel();
			this.uncleNode = LambkitNodeManager.me().getSiblingNode();
		}
	}

	public LambkitNodeP2c(LambkitNodeP2c node) {
		if(node!=null) {
			this.id = node.getId();
			this.applicationName = node.getApplicationName();
			this.level = node.getLevel();
			this.uncleNode = node.getUncleNode();
		}
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public int getLevel() {
		return level;
	}

	public void setLevel(int level) {
		this.level = level;
	}

	public String getApplicationName() {
		return applicationName;
	}

	public void setApplicationName(String applicationName) {
		this.applicationName = applicationName;
	}

	public LambkitSiblingNode getUncleNode() {
		return uncleNode;
	}

	public void setUncleNode(LambkitSiblingNode uncleNode) {
		this.uncleNode = uncleNode;
	}
}
