package com.lambkit.node;

public interface NodeHttpInterceptor {
	public void intercept(NodeHttpPipline pipline);
}
