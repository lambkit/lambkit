/**
 * 节点
 * @author yangyong(孤竹行)
 *
 * {
 *     "id": "1",
 *     "name": "UPMS",
 *     "key": "75e2578c0d931ab6fead5b351cecf0b9",
 *     ips:[
 *       {
 *           "ip": "219.202.204.156",
 *           "port": "8080",
 *           "group": "internet"
 *       },
 *       {
 *           "ip": "192.168.1.2",
 *           "port": "8080",
 *           "group": "mynet"
 *       }
 *     ],
 *     attrs: {
 *         "attr1": "value1",
 *     }
 * }
 */
package com.lambkit.node;