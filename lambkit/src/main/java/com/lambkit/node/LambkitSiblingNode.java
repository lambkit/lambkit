package com.lambkit.node;

import com.lambkit.node.s2p.LambkitNodeS2p;

/**
 * @author yangyong(孤竹行)
 */
public class LambkitSiblingNode extends LambkitNodeS2p {

    /**
     * 提交数据时间
     */
    private long commitTime;

    private String siblingUrl;

    public LambkitSiblingNode() {

    }

    public LambkitSiblingNode(LambkitNodeS2p s2p) {
        super(s2p);
        this.commitTime = System.currentTimeMillis();
    }

    public long getCommitTime() {
        return commitTime;
    }

    public void setCommitTime(long commitTime) {
        this.commitTime = commitTime;
    }

    public String getSiblingUrl() {
        return siblingUrl;
    }

    public void setSiblingUrl(String siblingUrl) {
        this.siblingUrl = siblingUrl;
    }
}
