package com.lambkit.node.c2p;

import com.lambkit.node.LambkitNode;
import com.lambkit.node.NetNode;
import com.lambkit.node.NetUrl;

/**
 * 网络节点
 * 
 * @author yangyong
 *
 */
public class LambkitNodeC2p implements NetNode {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * id
	 */
	private String id;
	/**
	 * 用户的key
	 */
	private String key;
	/**
	 * 树结构的层级
	 */
	private String applicationName;
	/**
	 * 主要地址
	 */
	private NetUrl url;
	/**
	 * 父子连通情况
	 */
	private int linkType = 0;// 0单向，1双向
	
	private String type = "normal";

	private String parentNodeUrl;

	public LambkitNodeC2p() {
	}
	public LambkitNodeC2p(LambkitNode node) {
		if(node!=null) {
			this.id = node.getId();
			this.applicationName = node.getApplicationName();
			this.type = node.getType();
		}
	}

	public String getParentNodeUrl() {
		return parentNodeUrl;
	}

	public void setParentNodeUrl(String parentNodeUrl) {
		this.parentNodeUrl = parentNodeUrl;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public int getLinkType() {
		return linkType;
	}

	public void setLinkType(int linkType) {
		this.linkType = linkType;
	}
	public String getKey() {
		return key;
	}

	public void setKey(String key) {
		this.key = key;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getApplicationName() {
		return applicationName;
	}

	public void setApplicationName(String applicationName) {
		this.applicationName = applicationName;
	}

	public NetUrl getUrl() {
		return url;
	}

	public void setUrl(NetUrl url) {
		this.url = url;
	}


}
