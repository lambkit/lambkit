package com.lambkit.node.c2p;

import com.lambkit.core.config.annotation.PropConfig;

@PropConfig(prefix = "lambkit.node.c2p.curl")
public class LambkitNodeC2pUrlConfig {
	/**
	 * 网络名称
	 */
	private String group = "internet";
	private String protocal = "http";
	private String domain;
	private int port = 0;

	public String getGroup() {
		return group;
	}

	public void setGroup(String group) {
		this.group = group;
	}

	public String getProtocal() {
		return protocal;
	}

	public void setProtocal(String protocal) {
		this.protocal = protocal;
	}

	public String getDomain() {
		return domain;
	}

	public void setDomain(String domain) {
		this.domain = domain;
	}

	public int getPort() {
		return port;
	}

	public void setPort(int port) {
		this.port = port;
	}
}
