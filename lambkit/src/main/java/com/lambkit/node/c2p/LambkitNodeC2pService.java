package com.lambkit.node.c2p;

/**
 * @author yangyong(孤竹行)
 */
public interface LambkitNodeC2pService {
    String query();
}
