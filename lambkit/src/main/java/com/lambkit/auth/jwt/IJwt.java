/**
 * Copyright (c) 2015-2017, Henry Yang 杨勇 (gismail@foxmail.com).
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.lambkit.auth.jwt;

import cn.hutool.core.date.DateUtil;
import cn.hutool.jwt.JWT;
import cn.hutool.jwt.JWTUtil;
import cn.hutool.jwt.JWTValidator;
import cn.hutool.jwt.signers.JWTSigner;
import cn.hutool.jwt.signers.JWTSignerUtil;
import com.lambkit.auth.AuthConfig;
import com.lambkit.auth.AuthUser;
import com.lambkit.core.Lambkit;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * FOR : hutool Jwt操作工具类, 未完成
 */
public interface IJwt {

	final String CLAIM_KEY_USERNAME = "sub";
	final String CLAIM_KEY_CREATED = "created";
	/**
	 * 通过 用户名密码 获取 token 如果为null，那么用户非法
	 * @return
	 */
	String getToken(AuthUser authUser);

	/**
	 * 用户通过其他认证方式登录，这里仅用于管理token，不验证用户
	 * 
	 * @param userName
	 * @return
	 */
	String getToken(String userName);

	/**
	 * 通过 旧的token来交换新的token
	 *
	 * @param token
	 * @return
	 */
	String refreshToken(String token);

	/**
	 * 验证token
	 * @param token
	 * @return
	 */
	String validateToken(String token);

	/**
	 * 从用户Token中获取用户名信息
	 *
	 * @param authToken
	 * @return
	 */
	String getUser(String authToken);
}