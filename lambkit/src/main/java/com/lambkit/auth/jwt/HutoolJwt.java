/**
 * Copyright (c) 2015-2017, Henry Yang 杨勇 (gismail@foxmail.com).
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.lambkit.auth.jwt;

import cn.hutool.core.date.DateUtil;
import cn.hutool.jwt.JWT;
import cn.hutool.jwt.JWTUtil;
import cn.hutool.jwt.JWTValidator;
import cn.hutool.jwt.signers.JWTSigner;
import cn.hutool.jwt.signers.JWTSignerUtil;
import com.lambkit.core.Lambkit;
import com.lambkit.auth.AuthConfig;
import com.lambkit.auth.AuthUser;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * FOR : hutool Jwt操作工具类, 未完成
 */
public class HutoolJwt implements IJwt {
	/**
	 * 通过 用户名密码 获取 token 如果为null，那么用户非法
	 * @return
	 */
	public String getToken(AuthUser authUser) {
		if (authUser == null) {
			return null;
		}
		AuthConfig config = Lambkit.config(AuthConfig.class);
		// 构建服务器端储存对象
		//String cacheName = config.getCacheName();
		//Lambkit.getCache().put(cacheName, userName, user);// 在服务器端储存jwtBean
		// jwtStore.set(userName, user);
		// writeFile();
		// 用userName创建token
		String token = generateToken(authUser.getUserName());
		String tokenPrefix = config.getTokenPrefix();
		return tokenPrefix + token;
	}

	/**
	 * 用户通过其他认证方式登录，这里仅用于管理token，不验证用户
	 * 
	 * @param userName
	 * @return
	 */
	public String getToken(String userName) {
		// 用userName创建token
		AuthConfig config = Lambkit.config(AuthConfig.class);
		String tokenPrefix = config.getTokenPrefix();
		String token = generateToken(userName);
		return tokenPrefix + token;
	}

	/**
	 * 通过 旧的token来交换新的token
	 *
	 * @param token
	 * @return
	 */
	public String refreshToken(String token) {
		AuthConfig config = Lambkit.config(AuthConfig.class);
		String cacheName = config.getCacheName();
		String tokenPrefix = config.getTokenPrefix();
		if (token == null || token.length() < tokenPrefix.length()) {
			throw new JwtTokenConfigException("token", "被解析");
		}
		String trueToken = token.substring(tokenPrefix.length(), token.length());
		if (isTokenExpired(trueToken)) { // 如果已经过期
			// 解析出用户名
			String userName = getUser(trueToken);
			AuthUser jwtBean = (AuthUser) Lambkit.getCache().get(cacheName, userName);
			if (jwtBean == null) {
				return token;
			}
			return generateToken(userName); // 在此匹配生成token
		}
		return token;
	}

	/**
	 * 验证token
	 * @param token
	 * @return
	 */
	public String validateToken(String token) {
		String jwtUser = getUser(token); // 从token中解析出jwtAble
		if (jwtUser != null) {
			refreshExpirationDate(token);
			return jwtUser;
		}
		return null;
	}

	/**
	 * 从用户Token中获取用户名信息
	 *
	 * @param authToken
	 * @return
	 */
	public String getUser(String authToken) {
		String jwtUser = null;
		final JWT jwt = JWTUtil.parseToken(authToken);
		Object sub = jwt.getPayload(CLAIM_KEY_USERNAME);
		jwtUser = sub.toString();
		return jwtUser;
	}


	/**
	 * 判断Token是否已经过期
	 *
	 * @param token
	 * @return
	 */
	private Boolean isTokenExpired(String token) {
		try{
			JWTValidator.of(token).validateDate(DateUtil.date());
			return true;
		} catch (Exception e) {
			return false;
		}
	}


	/**
	 * 根据用户信息生成Token
	 *
	 * @param userName
	 * @return
	 */
	private String generateToken(String userName) {
		Map<String, Object> claims = new HashMap<String, Object>();
		claims.put(CLAIM_KEY_USERNAME, userName);
		claims.put(CLAIM_KEY_CREATED, new Date());
		return generateToken(claims);
	}

	/**
	 * 根据Claims信息来创建Token
	 *
	 * @param claims
	 * @returns
	 */
	private String generateToken(Map<String, Object> claims) {
		AuthConfig config = Lambkit.config(AuthConfig.class);
		String secret = config.getSecret();
		final JWTSigner signer = JWTSignerUtil.hs512(secret.getBytes());
		return JWTUtil.createToken(claims, signer);
	}

	/**
	 * 生成令牌的过期日期
	 *
	 * @return
	 */
	private Date generateExpirationDate() {
		AuthConfig config = Lambkit.config(AuthConfig.class);
		int expirationSecond = config.getLoginExpirationSecond();
		return new Date(System.currentTimeMillis() + expirationSecond * 1000);
	}

	/**
	 * 刷新令牌的过期时间
	 */
	private void refreshExpirationDate(String authToken) {
		AuthConfig config = Lambkit.config(AuthConfig.class);
		String secret = config.getSecret();
		final JWTSigner signer = JWTSignerUtil.hs512(secret.getBytes());
		boolean flag = JWTUtil.verify(authToken, signer);

		final JWT jwt = JWTUtil.parseToken(authToken);
		int expirationSecond = config.getLoginExpirationSecond();
		Date date = generateExpirationDate();
		jwt.setExpiresAt(date);
	}
}