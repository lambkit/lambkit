package com.lambkit.auth.exception;

/**
 * @author yangyong(孤竹行)
 */
public class LockedAccountException extends AuthException{
    public LockedAccountException() {
        super();
    }

    public LockedAccountException(String message) {
        super(message);
    }

    public LockedAccountException(String message, Throwable cause) {
        super(message, cause);
    }

    public LockedAccountException(Throwable cause) {
        super(cause);
    }
}
