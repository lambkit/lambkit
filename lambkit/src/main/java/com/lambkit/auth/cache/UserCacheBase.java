/**
 * Copyright (c) 2015-2017, Henry Yang 杨勇 (gismail@foxmail.com).
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.lambkit.auth.cache;

import cn.hutool.core.util.StrUtil;
import com.lambkit.core.Lambkit;
import com.lambkit.auth.AuthConfig;
import com.lambkit.auth.AuthUser;
import com.lambkit.core.cache.ICache;
import com.lambkit.util.Printer;

import java.util.List;
import java.util.Set;

/**
 * @author yangyong
 */
public abstract class UserCacheBase implements IUserCache {

	private final String userCacheName = "LAMBKIT_AUTH_CACHE";
	public final String LAMBKIT_AUTH_USER = "lambkit-auth-user";
	public final String LAMBKIT_AUTH_SESSION_ID = "lambkit-auth-session-id";
	public final String LAMBKIT_LOGIN_ERROR_USER = "lambkit-login-error-user";
	public final String LAMBKIT_LOGIN_ERROR_SESSION_ID = "lambkit-login-error-session-id";

	public abstract ICache getCache();

	public String getUserCacheName() {
		return userCacheName;
	}

	@Override
	public boolean hasUser(String sessionid) {
		
		AuthUser user = getAuthUser(sessionid);
		if (user == null) {
			return false;
		}
		Printer.print(this, "auth", "AuthUserCahce: the user is " + user.getUserName());
		String session_id = getCache().get(getUserCacheName(), LAMBKIT_AUTH_USER + "_" + user.getUserName());
		if (StrUtil.isNotBlank(session_id) && !session_id.equals(sessionid)) {
			return false;
		}
		return true;
	}

	@Override
	public AuthUser getAuthUser(String sessionid) {
		
		Object t = getCache().get(getUserCacheName(), LAMBKIT_AUTH_SESSION_ID + "_" + sessionid);
		return (AuthUser) t;
	}

	@Override
	public String getSessionId(String username) {
		
		String oldsessionid = getCache().get(getUserCacheName(), LAMBKIT_AUTH_USER + "_" + username);
		if (StrUtil.isNotBlank(oldsessionid)) {
			return oldsessionid;
		}
		return null;
	}
	@Override
	public int loginStatus(String username, String sessionid) {
		
		// 用户已登录
		if (hasUser(sessionid)) {
			return 1;
		} else {
			String oldsessionid = getCache().get(getUserCacheName(), LAMBKIT_AUTH_USER + "_" + username);
			Printer.print(this, "auth", "loginStatus :" + oldsessionid);
			if (StrUtil.isNotBlank(oldsessionid)) {
				// 用户在别处已登录
				return 2;
			} else {
				return 0;
			}
		}
	}
	@Override
	public AuthUser login(AuthUser user, String sessionid) {
		
		String oldsessionid = getCache().get(getUserCacheName(), LAMBKIT_AUTH_USER + "_" + user.getUserName());
		if (StrUtil.isNotBlank(oldsessionid)) {
			logout(oldsessionid);
		}
		int liveSecond = Lambkit.config(AuthConfig.class).getErrorExpirationSecond();
		if(liveSecond > 0) {
			getCache().put(getUserCacheName(), LAMBKIT_AUTH_SESSION_ID + "_" + sessionid, user, liveSecond);
			// 保存session
			getCache().put(getUserCacheName(), LAMBKIT_AUTH_USER + "_" + user.getUserName(), sessionid, liveSecond);
		} else {
			// 保存uset
			getCache().put(getUserCacheName(), LAMBKIT_AUTH_SESSION_ID + "_" + sessionid, user);
			// 保存session
			getCache().put(getUserCacheName(), LAMBKIT_AUTH_USER + "_" + user.getUserName(), sessionid);
		}

		return user;
	}

	@Override
	public AuthUser validate(String sessionId) {
		if(StrUtil.isBlank(sessionId)) {
			return null;
		}
		AuthUser user = getAuthUser(sessionId);
		if(user!=null) {
			int liveSecond = Lambkit.config(AuthConfig.class).getErrorExpirationSecond();
			if(liveSecond > 0) {
				getCache().expire(getUserCacheName(), LAMBKIT_AUTH_SESSION_ID + "_" + sessionId, liveSecond);
				//getCache().put(getUserCacheName(), LAMBKIT_AUTH_USER + "_" + user.getUserName(), sessionId);
			}
		}
		return user;
	}

	@Override
	public void logout(String sessionid) {
		
		// 移除session
		AuthUser uc = getAuthUser(sessionid);
		if (uc != null) {
			String username = uc.getUserName();
			if (StrUtil.isNotBlank(username)) {
				getCache().remove(getUserCacheName(), LAMBKIT_AUTH_USER + "_" + username);
			}
		}
		// 移除user
		getCache().remove(getUserCacheName(), LAMBKIT_AUTH_SESSION_ID + "_" + sessionid);
	}

	public void clear() {
		getCache().removeAll(getUserCacheName());
	}

	public Integer getLoginUserError(String username) {
		Integer errorNum = getCache().get(getUserCacheName(), LAMBKIT_LOGIN_ERROR_USER + "_" + username);
		return errorNum==null ? 0 : errorNum.intValue();
	}
	public Integer addLoginUserError(String username) {
		Integer errorNum = getCache().get(getUserCacheName(), LAMBKIT_LOGIN_ERROR_USER + "_" + username);
		errorNum = errorNum==null ? 1 : errorNum++;
		int liveSecond = Lambkit.config(AuthConfig.class).getErrorExpirationSecond();//1分钟
		getCache().put(getUserCacheName(), LAMBKIT_LOGIN_ERROR_USER + "_" + username, errorNum, liveSecond);
		return errorNum;
	}

	public Integer getLoginError(String sessionId) {
		Object error = getCache().get(getUserCacheName(), LAMBKIT_LOGIN_ERROR_SESSION_ID + "_" + sessionId);
		if(error!=null) {
			Integer ier = (Integer) error;
			return ier;
		} else {
			return 0;
		}
	}
	public Integer addLoginError(String sessionId) {
		Object error = getCache().get(getUserCacheName(), LAMBKIT_LOGIN_ERROR_SESSION_ID + "_" + sessionId);
		int liveSecond = Lambkit.config(AuthConfig.class).getErrorExpirationSecond();//1分钟
		if(error!=null) {
			Integer ier = (Integer) error;
			ier++;
			getCache().put(getUserCacheName(), LAMBKIT_LOGIN_ERROR_SESSION_ID + "_" + sessionId, ier, liveSecond);
			return ier;
		} else {
			getCache().put(getUserCacheName(), LAMBKIT_LOGIN_ERROR_SESSION_ID + "_" + sessionId, Integer.valueOf(1), liveSecond);
			return 1;
		}
	}

	////////////////////////

	public <T> T get(Object key) {
		return getCache().get(getUserCacheName(), key);
	}

	public void put(String key, Object value, int seconds) {
		getCache().put(getUserCacheName(), key, value, seconds);
	}

	public void set(String key, Object value, int seconds) {
		getCache().put(getUserCacheName(), key, value, seconds);
	}

	public void setex(String key, Object value, int seconds) {
		getCache().put(getUserCacheName(), key, value, seconds);
	}

	public Long expire(Object key, int seconds) {
		return getCache().expire(getUserCacheName(), key, seconds);
	}

	public void del(Object key) {
		getCache().remove(getUserCacheName(), key);
	}


	public void lpush(Object key, Object value) {
		getCache().lpush(getUserCacheName(), key, value);
	}

	public long llen(Object key) {
		return getCache().llen(getUserCacheName(), key);
	}

	public List lrange(Object key, int start, int end) {
		return getCache().lrange(getUserCacheName(), key, start, end);
	}

	public void srem(Object key, Object... members) {
		getCache().srem(getUserCacheName(), key, members);
	}

	public void lrem(Object key, int count, Object value) {
		getCache().lrem(getUserCacheName(), key, count, value);
	}

	public Set smembers(Object key) {
		return getCache().smembers(getUserCacheName(), key);
	}

	public Long scard(Object key) {
		return getCache().scard(getUserCacheName(), key);
	}

	public void saddAndExpire(Object key, Object value, int seconds) {
		getCache().saddAndExpire(getUserCacheName(), key, value, seconds);
	}
}
