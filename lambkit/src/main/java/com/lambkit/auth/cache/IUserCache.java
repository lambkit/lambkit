/**
 * Copyright (c) 2015-2017, Henry Yang 杨勇 (gismail@foxmail.com).
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.lambkit.auth.cache;

import com.lambkit.auth.AuthUser;

import java.util.List;
import java.util.Set;

public interface IUserCache {

	/**
	 * 用户是否存在，(等同于用户是否登陆)
	 * 
	 * @return
	 */
	public boolean hasUser(String sessionid);
	
	/**
	 * 用户缓存
	 * 
	 * @return
	 */
	public AuthUser getAuthUser(String sessionid);

	public String getSessionId(String username);

	/**
	 * 用户状态
	 * @param username
	 * @param sessionid
	 * @return
	 */
	public int loginStatus(String username, String sessionid);

	/**
	 * 用户登录
	 * @param user
	 * @param sessionid
	 * @return
	 */
	public AuthUser login(AuthUser user, String sessionid);

	public AuthUser validate(String sessionId);
	/**
	 * 用户退出
	 * @param sessionid
	 */
	public void logout(String sessionid);

	/**
	 * 用户登录错误次数
	 * @param username
	 * @return
	 */
	public Integer getLoginUserError(String username);
	public Integer addLoginUserError(String username);

	public Integer getLoginError(String sessionId);
	public Integer addLoginError(String sessionId);
	
	public void clear();

	/////////////////////
	public <T> T get(Object key);

	public void set(String key, Object value, int seconds);

	public void del(Object key);

	public void lpush(Object key, Object value);

	public long llen(Object key);

	public List lrange(Object key, int start, int end);

	public void srem(Object key, Object... members);

	public void lrem(Object key, int count, Object value);

	public Set smembers(Object key);

	public Long scard(Object key);
}
