package com.lambkit.event;

import com.lambkit.core.event.Event;
import com.lambkit.core.event.EventDispatcher;
import com.lambkit.core.event.IEventListener;

/**
 * @author yangyong(孤竹行)
 */
public class EventKit {
    private static EventDispatcher eventDispatcher = new EventDispatcher();

    public static void register(Class<? extends IEventListener> listenerClass) {
        eventDispatcher.register(listenerClass);
    }

    public static void unRegister(Class<? extends IEventListener> listenerClass) {
        eventDispatcher.unregister(listenerClass);
    }

    public static void sendEvent(Event message) {
        eventDispatcher.dispatch(message);
    }

    public static void sendEvent(String action, Object data) {
        eventDispatcher.dispatch(new Event(action, data));
    }

    public static void sendEvent(String action) {
        eventDispatcher.dispatch(new Event(action, null));
    }
}
