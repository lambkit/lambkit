package com.lambkit.servlet;

import cn.hutool.core.map.MapUtil;
import com.lambkit.core.http.ICookie;
import com.lambkit.core.http.IRequest;
import com.lambkit.core.http.IResponse;
import com.lambkit.core.http.ISession;
import com.lambkit.util.ServletRequestKit;

import javax.servlet.ServletException;
import javax.servlet.ServletResponse;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.io.BufferedReader;
import java.io.IOException;
import java.util.Enumeration;
import java.util.Map;

/**
 * @author yangyong(孤竹行)
 */
public class LambkitServletRequest implements IRequest {
    private HttpServletRequest request;

    private String rawData;

    public LambkitServletRequest(HttpServletRequest request) {
        this.request = request;
    }

    @Override
    public Object getSource() {
        return request;
    }

    @Override
    public Enumeration<String> getAttributeNames() {
        return request.getAttributeNames();
    }

    @Override
    public <T> T getAttribute(String name) {
        return (T) request.getAttribute(name);
    }

    @Override
    public Map<String, Object> getAttributeMap() {
        Map<String, Object> attrs = MapUtil.newHashMap();
        Enumeration<String> names = request.getAttributeNames();
        while(names.hasMoreElements()) {
            String name = names.nextElement();
            attrs.put(name, request.getAttribute(name));
        }
        return attrs;
    }

    @Override
    public void setAttribute(String name, Object value) {
        request.setAttribute(name, value);
    }

    @Override
    public void removeAttribute(String name) {
        request.removeAttribute(name);
    }

    @Override
    public String getRawData() {
        if(rawData == null) {
            rawData = ServletRequestKit.readData(request);
        }
        return rawData;
    }

    @Override
    public String getParameter(String name) {
        return request.getParameter(name);
    }

    @Override
    public Map<String, String[]> getParameterMap() {
        return request.getParameterMap();
    }

    @Override
    public Enumeration<String> getParameterNames() {
        return request.getParameterNames();
    }

    @Override
    public String[] getParameterValues(String name) {
        return request.getParameterValues(name);
    }

    @Override
    public String getHeader(String name) {
        return request.getHeader(name);
    }

    @Override
    public ISession getSession(boolean create) {
        HttpSession session = request.getSession(create);
        LambkitServletSession session1 = new LambkitServletSession(session);
        return session1;
    }

    @Override
    public ICookie[] getCookies() {
        Cookie[] cookies = request.getCookies();
        if(cookies!=null) {
            LambkitServletCookie[] cookies1 = new LambkitServletCookie[cookies.length];
            for (int i = 0; i < cookies.length; i++) {
                cookies1[i] = new LambkitServletCookie(cookies[i]);
            }
            return cookies1;
        }
        return null;
    }

    @Override
    public String getMethod() {
        return request.getMethod();
    }

    @Override
    public String getRequestURI() {
        return request.getRequestURI();
    }

    @Override
    public String getRequestURL() {
        return request.getRequestURL().toString();
    }

    @Override
    public void forward(String path, IResponse response) {
        try {
            request.getRequestDispatcher(path).forward(request, (ServletResponse) response);
        } catch (ServletException e) {
            throw new RuntimeException(e);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public String getRemoteAddr() {
        return request.getRemoteAddr();
    }

    @Override
    public String getScheme() {
        return request.getScheme();
    }

    @Override
    public String getServerName() {
        return request.getServerName();
    }

    @Override
    public int getServerPort() {
        return request.getServerPort();
    }

    @Override
    public BufferedReader getReader() throws IOException {
        return request.getReader();
    }

    @Override
    public String getContentType() {
        return request.getContentType();
    }

    @Override
    public String getQueryString() {
        return request.getQueryString();
    }
}
