package com.lambkit.servlet;

import com.lambkit.core.http.ICookie;

import javax.servlet.http.Cookie;

/**
 * @author yangyong(孤竹行)
 */
public class LambkitServletCookie implements ICookie {

    private Cookie cookie;

    private String sameSite;
    //public final static String LAX = "Lax";
    public final static String NONE = "None";
    //public final static String STRICT = "Strict";
    //public final static String SET_COOKIE = "Set-Cookie";

    public LambkitServletCookie(Cookie cookie) {
        this.cookie = cookie;
    }

    public LambkitServletCookie(String name, String value) {
        this.cookie = new Cookie(name, value);
    }

    @Override
    public String getValue() {
        return cookie.getValue();
    }

    @Override
    public String getName() {
        return cookie.getName();
    }

    @Override
    public void setMaxAge(int maxAgeInSeconds) {
        cookie.setMaxAge(maxAgeInSeconds);
    }

    @Override
    public void setPath(String path) {
        cookie.setPath(path);
    }

    @Override
    public void setDomain(String domain) {
        cookie.setDomain(domain);
    }

    @Override
    public void setHttpOnly(Boolean isHttpOnly) {
        cookie.setHttpOnly(isHttpOnly);
    }

    public Cookie getCookie() {
        return cookie;
    }

    public void setCookie(Cookie cookie) {
        this.cookie = cookie;
    }

    public String getSameSite() {
        return sameSite;
    }

    public void setSameSite(String sameSite) {
        this.sameSite = sameSite;
        if (NONE.equals(sameSite)) {
            cookie.setSecure(true);
        }
    }
}
