package com.lambkit.servlet;

import cn.hutool.log.Log;
import com.lambkit.core.http.ICookie;
import com.lambkit.core.http.IResponse;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.OutputStream;
import java.util.Collection;

/**
 * @author yangyong(孤竹行)
 */
public class LambkitServletResponse implements IResponse {

    private HttpServletResponse httpResponse;

    public LambkitServletResponse(HttpServletResponse httpResponse) {
        this.httpResponse = httpResponse;
    }


    public void setStatus(int status) {
        httpResponse.setStatus(status);
    }

    @Override
    public void setContentType(String contentType) {
        httpResponse.setContentType(contentType);
    }

    @Override
    public String getContentType() {
        return httpResponse.getContentType();
    }

    @Override
    public void write(byte[] data) throws IOException {
        httpResponse.getOutputStream().write(data);
    }

    @Override
    public void close() {
        try {
            httpResponse.getOutputStream().flush();
            httpResponse.getOutputStream().close();
        } catch (Exception exception) {
            Log.get(this.getClass()).error(exception.getMessage(), exception);
        }
    }

    @Override
    public Object getSource() {
        return httpResponse;
    }

    @Override
    public int getHttpStatus() {
        return httpResponse.getStatus();
    }

    @Override
    public void setHttpStatus(int status, String msg) {
        httpResponse.setStatus(status);
    }

    @Override
    public void setHeader(String name, String value) {
        httpResponse.setHeader(name, value);
    }

    @Override
    public void addHeader(String name, String value) {
        httpResponse.addHeader(name, value);
    }

    @Override
    public String getHeader(String name) {
        return httpResponse.getHeader(name);
    }

    @Override
    public void setDateHeader(String expires, int i) {
        httpResponse.setDateHeader(expires, i);
    }

    @Override
    public Collection<String> getHeaders(String name) {
        return httpResponse.getHeaders(name);
    }

    @Override
    public Collection<String> getHeaderNames() {
        return httpResponse.getHeaderNames();
    }

    @Override
    public void setContentLength(int contentLength) {
        httpResponse.setContentLength(contentLength);
    }

    @Override
    public int getContentLength() {
        String contentLength = httpResponse.getHeader("Content-Length");
        return contentLength == null ? 0 : Integer.parseInt(contentLength);
    }

    @Override
    public void addCookie(ICookie cookie) {
        if(cookie instanceof LambkitServletCookie) {
            httpResponse.addCookie(((LambkitServletCookie) cookie).getCookie());
        }
    }

    @Override
    public OutputStream getOutputStream() throws IOException {
        return httpResponse.getOutputStream();
    }

    @Override
    public void redirect(String url, int code) throws IOException {
        httpResponse.sendRedirect(url);
    }
}
