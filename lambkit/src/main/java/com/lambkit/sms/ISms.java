package com.lambkit.sms;

import cn.hutool.core.util.StrUtil;
import com.lambkit.core.Lambkit;

public interface ISms {
	/**
	 * 发送验证码短信
	 *
	 * @param to  手机号码
	 */
	public int sendNoticeSMS(String to, String notice);

	/**
	 * 发送验证码短信
	 * 
	 * @param to  手机号码
	 */
	public int sendVerifyLoginSMS(String to);

	/**
	 * 验证短信验证码登陆
	 * @param to 手机号
	 * @param verifyCode
	 * @return
	 */
	public default boolean verifySMS(String to, String verifyCode) {
		if(StrUtil.isBlank(verifyCode)) {
			return false;
		}
		// 缓存中验证码
		String cacheVerifyCode = Lambkit.getCache().get("SMS_CACHE_PHONE", to);
		//Printer.print(this, "sms"("mobile: " + to + ", code: " + cacheVerifyCode + ", verifyCode: " + verifyCode);
		// 如果赎金来的验证码和缓存中的验证码一致，则验证成功
		if (verifyCode.equals(cacheVerifyCode)) {
			return true;
		}
		return false;
	}

	/**
	 * 生成6位随机数验证码
	 * 
	 * @return
	 */
	public default String vcode() {
		String vcode = "";
		for (int i = 0; i < 6; i++) {
			vcode = vcode + (int) (Math.random() * 9);
		}
		return vcode;
	}
}
