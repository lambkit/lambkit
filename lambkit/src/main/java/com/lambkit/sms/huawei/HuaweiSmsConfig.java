package com.lambkit.sms.huawei;

import com.lambkit.core.config.annotation.PropConfig;

@PropConfig(prefix="lambkit.sms.huawei")
public class HuaweiSmsConfig {
	// APP接入地址(在控制台"应用管理"页面获取)+接口访问URI
	private String url;

	// APP_Key
	private String appKey;

	// APP_Secret
	private String appSecret;

	// 国内短信签名通道号或国际/港澳台短信通道号
	private String sender;

	// 模板ID
	private String templateId;

	// 签名名称
	private String signature ;

	public HuaweiSmsConfig() {
	}

	public HuaweiSmsConfig(String appKey, String appSecret, String sender, String templateId, String signature) {
		this.appKey = appKey;
		this.appSecret = appSecret;
		this.sender = sender;
		this.templateId = templateId;
		this.signature = signature;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public String getAppKey() {
		return appKey;
	}

	public void setAppKey(String appKey) {
		this.appKey = appKey;
	}

	public String getAppSecret() {
		return appSecret;
	}

	public void setAppSecret(String appSecret) {
		this.appSecret = appSecret;
	}

	public String getSender() {
		return sender;
	}

	public void setSender(String sender) {
		this.sender = sender;
	}

	public String getTemplateId() {
		return templateId;
	}

	public void setTemplateId(String templateId) {
		this.templateId = templateId;
	}

	public String getSignature() {
		return signature;
	}

	public void setSignature(String signature) {
		this.signature = signature;
	}

}
