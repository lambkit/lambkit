/**
 * Copyright (c) 2015-2017, Henry Yang 杨勇 (gismail@foxmail.com).
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.lambkit.generator.impl;

import com.lambkit.generator.BaseGenerator;

import java.util.Map;

public class CommonGenerator extends BaseGenerator {

	@Override
	public void generate(String templateFolder, String templatePath, Map<String, Object> options) {
		// TODO Auto-generated method stub
		if(context==null) {
			return;
		}
		Map<String, Object> templateModel = createTemplateModel(options);
		context.generate(templateModel, templateFolder, templatePath);
	}

	@Override
	public void generate(String templateFolder, String templateFilePath, String generatFilePath, Map<String, Object> options) {
		if(context==null) {
			return;
		}
		Map<String, Object> templateModel = createTemplateModel(options);
		context.generate(templateModel, templateFolder, templateFilePath, generatFilePath);
	}

	@Override
	public Object execute(String templateFolder, String templateFilePath, Map<String, Object> options) {
		// TODO Auto-generated method stub
		if(context==null) {
			return null;
		} else {
			Map<String, Object> templateModel = createTemplateModel(options);
			return context.execute(templateModel, templateFolder, templateFilePath);
		}
	}

	public Map<String, Object> createTemplateModel(Map<String, Object> options) {
		Map<String, Object> templateModel = context.createTemplateModel(options);
		return templateModel;
	}
}
