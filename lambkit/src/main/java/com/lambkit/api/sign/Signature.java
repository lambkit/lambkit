package com.lambkit.api.sign;

import java.io.Serializable;


public class Signature implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private String id;
	private String key;
	private long datetime;
	private String sign;
	
	public Signature() {
		this.datetime = System.currentTimeMillis();
	}
	
	public String content(String secret) {
		return id + key + secret + datetime;
	}
	
	public boolean verifyTime() {
		long curt = System.currentTimeMillis();
		long dtime = curt - datetime;
		if(dtime > 0 && dtime < 300000) {//5分钟
			return true;
		}
		return false;
	}
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getKey() {
		return key;
	}
	public void setKey(String key) {
		this.key = key;
	}
	public long getDatetime() {
		return datetime;
	}
	public void setDatetime(long datetime) {
		this.datetime = datetime;
	}
	public String getSign() {
		return sign;
	}
	public void setSign(String sign) {
		this.sign = sign;
	}
	
}
