package com.lambkit.test;

import cn.hutool.json.JSONUtil;
import com.lambkit.core.Attr;

/**
 * @author yangyong(孤竹行)
 */
public class AttributeTest {
    public static void main(String[] args) {
        Attr attr = Attr.by("data", "test");
        String jsonStr = JSONUtil.toJsonStr(attr);
        System.out.println(jsonStr);
    }
}
