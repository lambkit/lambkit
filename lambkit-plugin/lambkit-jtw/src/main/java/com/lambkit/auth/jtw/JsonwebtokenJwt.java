/**
 * Copyright (c) 2015-2017, Henry Yang 杨勇 (gismail@foxmail.com).
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.lambkit.auth.jtw;

import com.lambkit.auth.jwt.IJwt;
import com.lambkit.auth.jwt.JwtTokenConfigException;
import com.lambkit.core.Lambkit;
import com.lambkit.auth.AuthConfig;
import com.lambkit.auth.AuthUser;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * FOR : Jwt操作工具类
 */
public class JsonwebtokenJwt implements IJwt {

	/**
	 * 通过 用户名密码 获取 token 如果为null，那么用户非法
	 * @return
	 */
	public String getToken(AuthUser authUser) {
		if (authUser == null) {
			return null;
		}
		AuthConfig config = Lambkit.config(AuthConfig.class);
		// 构建服务器端储存对象
		//String cacheName = config.getCacheName();
		//Lambkit.getCache().put(cacheName, userName, user);// 在服务器端储存jwtBean
		// jwtStore.set(userName, user);
		// writeFile();
		// 用userName创建token
		String token = generateToken(authUser.getUserName());
		String tokenPrefix = config.getTokenPrefix();
		return tokenPrefix + token;
	}

	/**
	 * 用户通过其他认证方式登录，这里仅用于管理token，不验证用户
	 * 
	 * @param userName
	 * @return
	 */
	public String getToken(String userName) {
		// 用userName创建token
		AuthConfig config = Lambkit.config(AuthConfig.class);
		String tokenPrefix = config.getTokenPrefix();
		String token = generateToken(userName);
		return tokenPrefix + token;
	}

	/**
	 * 通过 旧的token来交换新的token
	 *
	 * @param token
	 * @return
	 */
	public String refreshToken(String token) {
		AuthConfig config = Lambkit.config(AuthConfig.class);
		String cacheName = config.getCacheName();
		String tokenPrefix = config.getTokenPrefix();
		if (token == null || token.length() < tokenPrefix.length()) {
			throw new JwtTokenConfigException("token", "被解析");
		}
		String trueToken = token.substring(tokenPrefix.length(), token.length());
		if (isTokenExpired(trueToken)) { // 如果已经过期
			// 解析出用户名
			String userName = getUser(trueToken);
			AuthUser jwtBean = (AuthUser) Lambkit.getCache().get(cacheName, userName);
			if (jwtBean == null) {
				return token;
			}
			return generateToken(userName); // 在此匹配生成token
		}
		return token;
	}

	/**
	 * 验证token
	 * @param token
	 * @return
	 */
	public String validateToken(String token) {
		String jwtUser = getUser(token); // 从token中解析出jwtAble
		if (jwtUser != null) {
			//Date created = getCreatedDateFormToken(token);
			refreshExpirationDate(token);
			return jwtUser;
		}
		return null;
	}

	/**
	 * 从用户Token中获取用户名信息
	 *
	 * @param authToken
	 * @return
	 */
	public String getUser(String authToken) {
		String jwtUser = null;
		try {
			final Claims claims = getClaimsFromToken(authToken);
			jwtUser = claims != null ? claims.getSubject() : null;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return jwtUser;
	}

	/**
	 * 获取Token的过期日期
	 *
	 * @param token
	 * @return
	 */
	public Date getExpirationDateFromToken(String token) {
		Date expiration;
		try {
			final Claims claims = getClaimsFromToken(token);
			expiration = claims.getExpiration();
		} catch (Exception e) {
			expiration = null;
		}
		return expiration;
	}

	/**
	 * 获取用户Token的创建日期
	 *
	 * @param authToken
	 * @return
	 */
	public Date getCreatedDateFormToken(String authToken) {
		Date creatd;
		try {
			final Claims claims = getClaimsFromToken(authToken);
			creatd = new Date((Long) claims.get(IJwt.CLAIM_KEY_CREATED)); // 把时间戳转化为日期类型
		} catch (Exception e) {
			creatd = null;
		}

		return creatd;
	}

	/**
	 * 判断Token是否已经过期
	 *
	 * @param token
	 * @return
	 */
	private Boolean isTokenExpired(String token) {
		final Date expiration = getExpirationDateFromToken(token);
		return expiration.before(new Date());
	}

	/**
	 * 将Token信息解析成Claims
	 *
	 * @param token
	 * @return
	 */
	private Claims getClaimsFromToken(String token) {
		Claims claims;
		AuthConfig config = Lambkit.config(AuthConfig.class);
		String secret = config.getSecret();
		try {
			claims = Jwts.parser().setSigningKey(secret).parseClaimsJws(token).getBody();
		} catch (Exception e) {
			claims = null;
		}
		return claims;
	}

	/**
	 * 根据用户信息生成Token
	 *
	 * @param userName
	 * @return
	 */
	private String generateToken(String userName) {
		Map<String, Object> claims = new HashMap<String, Object>();
		claims.put(IJwt.CLAIM_KEY_USERNAME, userName);
		claims.put(IJwt.CLAIM_KEY_CREATED, new Date());
		return generateToken(claims);
	}

	/**
	 * 根据Claims信息来创建Token
	 *
	 * @param claims
	 * @returns
	 */
	private String generateToken(Map<String, Object> claims) {
		AuthConfig config = Lambkit.config(AuthConfig.class);
		String secret = config.getSecret();
		return Jwts.builder().setClaims(claims).setExpiration(generateExpirationDate())
				.signWith(SignatureAlgorithm.HS512, secret).compact();
	}

	/**
	 * 生成令牌的过期日期
	 *
	 * @return
	 */
	private Date generateExpirationDate() {
		AuthConfig config = Lambkit.config(AuthConfig.class);
		int expirationSecond = config.getLoginExpirationSecond();
		return new Date(System.currentTimeMillis() + expirationSecond * 1000);
	}

	/**
	 * 刷新令牌的过期时间
	 */
	private void refreshExpirationDate(String authToken) {
		final Claims claims = getClaimsFromToken(authToken);
		AuthConfig config = Lambkit.config(AuthConfig.class);
		int expirationSecond = config.getLoginExpirationSecond();
		Date date = new Date(System.currentTimeMillis() + expirationSecond * 1000);
		claims.setExpiration(date);
	}
}