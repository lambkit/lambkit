package com.lambkit.cache;

/**
 * @author yangyong(孤竹行)
 */
public abstract class BaseLambkitCache implements LambkitCache {
    @Override
    public boolean isNoneCache() {
        return false;
    }
}
