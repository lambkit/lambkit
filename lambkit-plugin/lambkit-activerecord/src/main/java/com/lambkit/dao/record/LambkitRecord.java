package com.lambkit.dao.record;

import com.jfinal.plugin.activerecord.Record;
import com.lambkit.dao.ILambkitRow;

import java.beans.Transient;
import java.util.Map;

public class LambkitRecord extends Record implements ILambkitRow<Record> {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	/**
     * Return column names of this record.
     */
    @Override
    @Transient
    public String[] getColumnNames() {
        return super.getColumnNames();
    }

    /**
     * Return column values of this record.
     */
    @Override
    @Transient
    public Object[] getColumnValues() {
        return super.getColumnValues();
    }

    @Override
    @Transient
    public Map<String, Object> getColumns() {
        return super.getColumns();
    }
}
