/**
 * Copyright (c) 2015-2017, Henry Yang 杨勇 (gismail@foxmail.com).
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.lambkit.dao.record;

import com.jfinal.plugin.activerecord.Page;
import com.jfinal.plugin.activerecord.Record;
import com.lambkit.dao.model.LambkitModelService;
import com.lambkit.db.sql.Columns;
import com.lambkit.db.sql.Example;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.ParameterizedType;
import java.util.List;

/**
 * 实现LambkitService抽象类
 */
public abstract class BaseServiceImpl<Query, M> implements LambkitModelService<M> {

	public Query query;
	
	protected abstract void initQuery();

	public Query getQuery() {
		return query;
	}
	
	/**
	 * 获取类泛型class
	 * @return
	 */
	public Class<Query> getQueryClass() {
		return (Class<Query>) ((ParameterizedType) getClass().getGenericSuperclass()).getActualTypeArguments()[0];
	}

	@Override
	public M findById(Object id) {
		try {
			//DynamicDataSource.setDataSource(DataSourceEnum.SLAVE.getName());
			Method findByPrimaryKey = query.getClass().getDeclaredMethod("findById", id.getClass());
			Object result = findByPrimaryKey.invoke(query, id);
			return (M) result;
		} catch (IllegalAccessException e) {
			e.printStackTrace();
		} catch (InvocationTargetException e) {
			e.printStackTrace();
		} catch (NoSuchMethodException e) {
			e.printStackTrace();
		}
		//DynamicDataSource.clearDataSource();
		return null;
	}
	
	@Override
	public M findByPrimaryKey(Object id) {
		try {
			//DynamicDataSource.setDataSource(DataSourceEnum.SLAVE.getName());
			Method findByPrimaryKey = query.getClass().getDeclaredMethod("findByPrimaryKey", id.getClass());
			Object result = findByPrimaryKey.invoke(query, id);
			return (M) result;
		} catch (IllegalAccessException e) {
			e.printStackTrace();
		} catch (InvocationTargetException e) {
			e.printStackTrace();
		} catch (NoSuchMethodException e) {
			e.printStackTrace();
		}
		//DynamicDataSource.clearDataSource();
		return null;
	}
	
	@Override
	public M findFirst(Example example) {
		try {
			//DynamicDataSource.setDataSource(DataSourceEnum.SLAVE.getName());
			Method select = query.getClass().getDeclaredMethod("findFirst", example.getClass());
			List<M> result = (List<M>) select.invoke(query, example);
			if (null != result && result.size() > 0) {
				return result.get(0);
			}
		} catch (IllegalAccessException e) {
			e.printStackTrace();
		} catch (InvocationTargetException e) {
			e.printStackTrace();
		} catch (NoSuchMethodException e) {
			e.printStackTrace();
		}
		//DynamicDataSource.clearDataSource();
		return null;
	}
	
	@Override
	public M findFirst(Columns columns) {
		return findFirst(Example.create(getTableName(), columns));
	}
	

	@Override
	public M findFirst(Columns columns, String orderby) {
		return findFirst(Example.create(getTableName(), columns).setOrderBy(orderby));
	}
	
	@Override
	public M findFirstByColumns(Columns columns) {
		return findFirst(Example.create(getTableName(), columns));
	}
	
	@Override
	public M findFirstByColumns(Columns columns, String orderby) {
		return findFirst(Example.create(getTableName(), columns).setOrderBy(orderby));
	}
	
	@Override
	public List<M> find(Example example, Integer count) {
		try {
			//DynamicDataSource.setDataSource(DataSourceEnum.SLAVE.getName());
			Method select = query.getClass().getDeclaredMethod("find", example.getClass());
			Object result = select.invoke(query, example);
			return (List<M>) result;
		} catch (IllegalAccessException e) {
			e.printStackTrace();
		} catch (InvocationTargetException e) {
			e.printStackTrace();
		} catch (NoSuchMethodException e) {
			e.printStackTrace();
		}
		//DynamicDataSource.clearDataSource();
		return null;
	}

	@Override
	public List<M> find(Example example) {
		try {
			//DynamicDataSource.setDataSource(DataSourceEnum.SLAVE.getName());
			Method select = query.getClass().getDeclaredMethod("find", example.getClass());
			Object result = select.invoke(query, example);
			return (List<M>) result;
		} catch (IllegalAccessException e) {
			e.printStackTrace();
		} catch (InvocationTargetException e) {
			e.printStackTrace();
		} catch (NoSuchMethodException e) {
			e.printStackTrace();
		}
		//DynamicDataSource.clearDataSource();
		return null;
	}
	
	@Override
	public List<M> find(Columns columns, Integer count) {

		return find(Example.create(getTableName(), columns), count);
	}
	

	@Override
	public List<M> find(Columns columns, String orderby, Integer count) {

		return find(Example.create(getTableName(), columns).setOrderBy(orderby), count);
	}
	@Override
	public List<M> findListByColumns(Columns columns, Integer count) {

		return find(Example.create(getTableName(), columns), count);
	}
	@Override
	public List<M> findListByColumns(Columns columns, String orderby, Integer count) {

		return find(Example.create(getTableName(), columns).setOrderBy(orderby), count);
	}

	@Override
	public List<M> find(Columns columns) {

		return find(Example.create(getTableName(), columns));
	}

	@Override
	public List<M> find(Columns columns, String orderby) {

		return find(Example.create(getTableName(), columns).setOrderBy(orderby));
	}
	@Override
	public List<M> findListByColumns(Columns columns) {

		return find(Example.create(getTableName(), columns));
	}
	@Override
	public List<M> findListByColumns(Columns columns, String orderby) {

		return find(Example.create(getTableName(), columns).setOrderBy(orderby));
	}
	
	@Override
	public Page<M> paginate(Integer pageNumber, Integer pageSize, Example example) {
		try {
			//DynamicDataSource.setDataSource(DataSourceEnum.SLAVE.getName());
			Method selectWithBLOBs = query.getClass().getDeclaredMethod("paginate", Integer.class, Integer.class, example.getClass());
			//PageHelper.startPage(pageNum, pageSize, false);
			Object result = selectWithBLOBs.invoke(query, pageNumber, pageSize, example);
			return (Page<M>) result;
		} catch (IllegalAccessException e) {
			e.printStackTrace();
		} catch (InvocationTargetException e) {
			e.printStackTrace();
		} catch (NoSuchMethodException e) {
			e.printStackTrace();
		}
		//DynamicDataSource.clearDataSource();
		return null;
	}
	
	@Override
	public Page<M> paginate(Integer pageNumber, Integer pageSize, Columns columns) {

		return paginate(pageNumber, pageSize, Example.create(getTableName(), columns));
	}

	@Override
	public Page<M> paginate(Integer pageNumber, Integer pageSize, Columns columns, String orderby) {

		return paginate(pageNumber, pageSize, Example.create(getTableName(), columns).setOrderBy(orderby));
	}
	@Override
	public Page<M> paginateByColumns(Integer pageNumber, Integer pageSize, Columns columns) {

		return paginate(pageNumber, pageSize, Example.create(getTableName(), columns));
	}
	@Override
	public Page<M> paginateByColumns(Integer pageNumber, Integer pageSize, Columns columns, String orderby) {

		return paginate(pageNumber, pageSize, Example.create(getTableName(), columns).setOrderBy(orderby));
	}
	
	@Override
	public Page<M> paginate(Example example, Integer offset, Integer limit) {
		try {
			//DynamicDataSource.setDataSource(DataSourceEnum.SLAVE.getName());
			Method paginate = query.getClass().getDeclaredMethod("paginate", example.getClass(), Integer.class, Integer.class);
			//PageHelper.offsetPage(offset, limit, false);
			Object result = paginate.invoke(query, offset/limit+1, limit, example);
			return (Page<M>) result;
		} catch (IllegalAccessException e) {
			e.printStackTrace();
		} catch (InvocationTargetException e) {
			e.printStackTrace();
		} catch (NoSuchMethodException e) {
			e.printStackTrace();
		}
		//DynamicDataSource.clearDataSource();
		return null;
	}
	
	@Override
	public Page<M> paginate(Columns columns, Integer offset, Integer limit) {

		return paginate(Example.create(getTableName(), columns), offset, limit);
	}

	@Override
	public Page<M> paginate(Columns columns, String orderby, Integer offset, Integer limit) {

		return paginate(Example.create(getTableName(), columns).setOrderBy(orderby), offset, limit);
	}
	
	@Override
	public Page<M> paginateByColumns(Columns columns, Integer offset, Integer limit) {

		return paginate(Example.create(getTableName(), columns), offset, limit);
	}
	
	@Override
	public Page<M> paginateByColumns(Columns columns, String orderby, Integer offset, Integer limit) {

		return paginate(Example.create(getTableName(), columns).setOrderBy(orderby), offset, limit);
	}
	
	@Override
	public Long count(Example example) {
		try {
			//DynamicDataSource.setDataSource(DataSourceEnum.SLAVE.getName());
			Method count = query.getClass().getDeclaredMethod("count", example.getClass());
			Object result = count.invoke(query, example);
			return Long.valueOf(String.valueOf(result));
		} catch (IllegalAccessException e) {
			e.printStackTrace();
		} catch (InvocationTargetException e) {
			e.printStackTrace();
		} catch (NoSuchMethodException e) {
			e.printStackTrace();
		}
		//DynamicDataSource.clearDataSource();
		return null;
	}
	
	@Override
	public Long count(Columns columns) {
		return count(Example.create(getTableName(), columns));
	}

	@Override
	public boolean insert(Record record) {
		try {
			//DynamicDataSource.setDataSource(DataSourceEnum.MASTER.getName());
			Method insert = query.getClass().getDeclaredMethod("insert", record.getClass());
			Object result = insert.invoke(query, record);
			return Boolean.valueOf(String.valueOf(result));
		} catch (IllegalAccessException e) {
			e.printStackTrace();
		} catch (InvocationTargetException e) {
			e.printStackTrace();
		} catch (NoSuchMethodException e) {
			e.printStackTrace();
		}
		//DynamicDataSource.clearDataSource();
		return false;
	}
	
	@Override
	public boolean insert(String primaryKey, Record record) {
		try {
			//DynamicDataSource.setDataSource(DataSourceEnum.MASTER.getName());
			Method insert = query.getClass().getDeclaredMethod("insert", String.class, record.getClass());
			Object result = insert.invoke(query, primaryKey, record);
			return Boolean.valueOf(String.valueOf(result));
		} catch (IllegalAccessException e) {
			e.printStackTrace();
		} catch (InvocationTargetException e) {
			e.printStackTrace();
		} catch (NoSuchMethodException e) {
			e.printStackTrace();
		}
		//DynamicDataSource.clearDataSource();
		return false;
	}

	@Override
	public boolean deleteById(Object id) {
		try {
			//DynamicDataSource.setDataSource(DataSourceEnum.MASTER.getName());
			Method deleteByPrimaryKey = query.getClass().getDeclaredMethod("deleteById", id.getClass());
			Object result = deleteByPrimaryKey.invoke(query, id);
			return Boolean.valueOf(String.valueOf(result));
		} catch (IllegalAccessException e) {
			e.printStackTrace();
		} catch (InvocationTargetException e) {
			e.printStackTrace();
		} catch (NoSuchMethodException e) {
			e.printStackTrace();
		}
		//DynamicDataSource.clearDataSource();
		return false;
	}

	@Override
	public int deleteByPrimaryKey(Object id) {
		try {
			//DynamicDataSource.setDataSource(DataSourceEnum.MASTER.getName());
			Method deleteByPrimaryKey = query.getClass().getDeclaredMethod("deleteByPrimaryKey", id.getClass());
			Object result = deleteByPrimaryKey.invoke(query, id);
			return Integer.parseInt(String.valueOf(result));
		} catch (IllegalAccessException e) {
			e.printStackTrace();
		} catch (InvocationTargetException e) {
			e.printStackTrace();
		} catch (NoSuchMethodException e) {
			e.printStackTrace();
		}
		//DynamicDataSource.clearDataSource();
		return 0;
	}
	

	@Override
	public int deleteByPrimaryKeys(String ids) {
		try {
			//DynamicDataSource.setDataSource(DataSourceEnum.MASTER.getName());
			Method deleteByPrimaryKey = query.getClass().getDeclaredMethod("deleteByPrimaryKeys", String.class);
			Object result = deleteByPrimaryKey.invoke(query, ids);
			return Integer.parseInt(String.valueOf(result));
		} catch (IllegalAccessException e) {
			e.printStackTrace();
		} catch (InvocationTargetException e) {
			e.printStackTrace();
		} catch (NoSuchMethodException e) {
			e.printStackTrace();
		}
		//DynamicDataSource.clearDataSource();
		return 0;
	}

	@Override
	public int delete(Example example) {
		try {
			//DynamicDataSource.setDataSource(DataSourceEnum.MASTER.getName());
			Method delete = query.getClass().getDeclaredMethod("delete", example.getClass());
			Object result = delete.invoke(query, example);
			return Integer.parseInt(String.valueOf(result));
		} catch (IllegalAccessException e) {
			e.printStackTrace();
		} catch (InvocationTargetException e) {
			e.printStackTrace();
		} catch (NoSuchMethodException e) {
			e.printStackTrace();
		}
		//DynamicDataSource.clearDataSource();
		return 0;
	}
	
	@Override
	public int delete(Columns columns) {

		return delete(Example.create(getTableName(), columns));
	}
	
	@Override
	public int update(Record record, Example example) {
		try {
			//DynamicDataSource.setDataSource(DataSourceEnum.MASTER.getName());
			Method update = query.getClass().getDeclaredMethod("update", record.getClass(), example.getClass());
			Object result = update.invoke(query, example);
			return Integer.parseInt(String.valueOf(result));
		} catch (IllegalAccessException e) {
			e.printStackTrace();
		} catch (InvocationTargetException e) {
			e.printStackTrace();
		} catch (NoSuchMethodException e) {
			e.printStackTrace();
		}
		//DynamicDataSource.clearDataSource();
		return 0;
	}
	
	@Override
	public int update(Record record, Columns columns) {

		return update(record, Example.create(getTableName(), columns));
	}

	@Override
	public boolean updateByPrimaryKey(String primaryKey, Record record) {
		try {
			//DynamicDataSource.setDataSource(DataSourceEnum.MASTER.getName());
			Method updateByPrimaryKey = query.getClass().getDeclaredMethod("updateByPrimaryKey", String.class, record.getClass());
			Object result = updateByPrimaryKey.invoke(query, primaryKey, record);
			return Boolean.valueOf(String.valueOf(result));
		} catch (IllegalAccessException e) {
			e.printStackTrace();
		} catch (InvocationTargetException e) {
			e.printStackTrace();
		} catch (NoSuchMethodException e) {
			e.printStackTrace();
		}
		//DynamicDataSource.clearDataSource();
		return false;
	}
	
	@Override
	public boolean update(Record record) {

		try {
			//DynamicDataSource.setDataSource(DataSourceEnum.MASTER.getName());
			Method updateByPrimaryKey = query.getClass().getDeclaredMethod("update", record.getClass());
			Object result = updateByPrimaryKey.invoke(query, record);
			return Boolean.valueOf(String.valueOf(result));
		} catch (IllegalAccessException e) {
			e.printStackTrace();
		} catch (InvocationTargetException e) {
			e.printStackTrace();
		} catch (NoSuchMethodException e) {
			e.printStackTrace();
		}
		//DynamicDataSource.clearDataSource();
		return false;
	}
	
}