package com.lambkit.db;

import cn.hutool.core.map.MapUtil;
import com.jfinal.plugin.IPlugin;

import java.util.Map;

/**
 * @author yangyong(孤竹行)
 */
public class DataSourceInitPlugin implements IPlugin {

    private Map<String, Map<String, String>> initSql;

    public DataSourceInitPlugin() {
        initSql = MapUtil.newHashMap();
    }
    @Override
    public boolean start() {
        //TODO 【lambkit研发】给数据库初始化表格，待实现
        return true;
    }

    @Override
    public boolean stop() {
        return true;
    }

    public void addInitSql(String key, String tableName, String sql) {
        Map<String, String> sqls = initSql.get(key);
        if(sqls == null) {
            sqls = MapUtil.newHashMap();
        }
        sqls.put(tableName, sql);
    }
}
