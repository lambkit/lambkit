package com.lambkit.plugin.redis.mq;

import com.lambkit.core.mq.MqType;
import com.lambkit.core.mq.Receiver;

import java.io.IOException;

/**
 * @author yangyong(孤竹行)
 */
public class RedisStreamReceiver implements Receiver {
    @Override
    public void handle(Object message) {

    }

    @Override
    public String getName() {
        return null;
    }

    @Override
    public MqType getType() {
        return null;
    }

    @Override
    public void close() throws IOException {

    }
}
