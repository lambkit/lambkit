package com.lambkit.plugin.satoken;

import cn.dev33.satoken.SaManager;
import cn.dev33.satoken.application.ApplicationInfo;
import cn.dev33.satoken.context.model.SaRequest;
import cn.dev33.satoken.util.SaFoxUtil;
import com.lambkit.core.http.ICookie;
import com.lambkit.core.http.IRequest;
import com.lambkit.web.HttpContextHolder;

import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

/**
 * @author yangyong(孤竹行)
 */
public class SaRequestForLambkit implements SaRequest {
    /**
     * 底层Request对象
     */
    protected IRequest request;

    /**
     * 实例化
     * @param request request对象
     */
    public SaRequestForLambkit(IRequest request) {
        this.request = request;
    }

    /**
     * 获取底层源对象
     */
    @Override
    public Object getSource() {
        return request;
    }

    /**
     * 在 [请求体] 里获取一个值
     */
    @Override
    public String getParam(String name) {
        return request.getParameter(name);
    }

    /**
     * 获取 [请求体] 里提交的所有参数名称
     * @return 参数名称列表
     */
    @Override
    public List<String> getParamNames(){
        return Collections.list(request.getParameterNames());
    }

    /**
     * 获取 [请求体] 里提交的所有参数
     *
     * @return 参数列表
     */
    @Override
    public Map<String, String> getParamMap(){
        // 获取所有参数
        Map<String, String[]> parameterMap = request.getParameterMap();
        Map<String, String> map = new LinkedHashMap<>(parameterMap.size());
        for (String key : parameterMap.keySet()) {
            String[] values = parameterMap.get(key);
            map.put(key, values[0]);
        }
        return map;
    }

    /**
     * 在 [请求头] 里获取一个值
     */
    @Override
    public String getHeader(String name) {
        return request.getHeader(name);
    }

    /**
     * 在 [Cookie作用域] 里获取一个值
     */
    @Override
    public String getCookieValue(String name) {
        return getCookieLastValue(name);
    }

    /**
     * 在 [ Cookie作用域 ] 里获取一个值 (第一个此名称的)
     */
    @Override
    public String getCookieFirstValue(String name){
        ICookie[] cookies = request.getCookies();
        if (cookies != null) {
            for (ICookie cookie : cookies) {
                if (cookie != null && name.equals(cookie.getName())) {
                    return cookie.getValue();
                }
            }
        }
        return null;
    }

    /**
     * 在 [ Cookie作用域 ] 里获取一个值 (最后一个此名称的)
     * @param name 键
     * @return 值
     */
    @Override
    public String getCookieLastValue(String name){
        String value = null;
        ICookie[] cookies = request.getCookies();
        if (cookies != null) {
            for (ICookie cookie : cookies) {
                if (cookie != null && name.equals(cookie.getName())) {
                    value = cookie.getValue();
                }
            }
        }
        return value;
    }

    /**
     * 返回当前请求path (不包括上下文名称)
     */
    @Override
    public String getRequestPath() {
        return ApplicationInfo.cutPathPrefix(request.getRequestURI());
    }

    /**
     * 返回当前请求的url，例：http://xxx.com/test
     * @return see note
     */
    public String getUrl() {
        String currDomain = SaManager.getConfig().getCurrDomain();
        if(!SaFoxUtil.isEmpty(currDomain)) {
            return currDomain + this.getRequestPath();
        }
        return request.getRequestURL().toString();
    }

    /**
     * 返回当前请求的类型
     */
    @Override
    public String getMethod() {
        return request.getMethod();
    }

    /**
     * 转发请求
     */
    @Override
    public Object forward(String path) {
        request.forward(path, HttpContextHolder.get().getResponse());
        return null;
    }
}
