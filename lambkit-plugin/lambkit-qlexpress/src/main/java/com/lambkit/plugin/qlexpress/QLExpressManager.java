package com.lambkit.plugin.qlexpress;

import com.ql.util.express.ExpressRunner;
import com.ql.util.express.IExpressContext;

import java.util.Collections;
import java.util.List;
import java.util.Map;

/**
 * @author yangyong(孤竹行)
 */
public class QLExpressManager {

    private ExpressRunner runner;

    public Object execute(String statement, Map<String, Object> context) throws Exception {
        IExpressContext expressContext = new QLExpressContext(context != null ? context : Collections.EMPTY_MAP);
        return runner.execute(statement, expressContext, null, true, false);
    }

    public Object execute(String statement, IExpressContext<String, Object> expressContext) throws Exception {
        return runner.execute(statement, expressContext, null, true, false);
    }

    public Object execute(String statement, Map<String, Object> context, List<String> errorList, boolean isCache, boolean isTrace) throws Exception {
        IExpressContext expressContext = new QLExpressContext(context != null ? context : Collections.EMPTY_MAP);
        return runner.execute(statement, expressContext, errorList, isCache, isTrace);
    }

    public Object execute(String statement, IExpressContext<String, Object> expressContext, List<String> errorList, boolean isCache, boolean isTrace) throws Exception {
        return runner.execute(statement, expressContext, errorList, isCache, isTrace);
    }

    public ExpressRunner getRunner() {
        return runner;
    }

    public void setRunner(ExpressRunner runner) {
        this.runner = runner;
    }
}
