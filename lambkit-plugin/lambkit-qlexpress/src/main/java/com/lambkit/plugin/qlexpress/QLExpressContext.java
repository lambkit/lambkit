package com.lambkit.plugin.qlexpress;

import com.lambkit.core.Lambkit;
import com.ql.util.express.IExpressContext;

import java.util.HashMap;
import java.util.Map;

/**
 * @author yangyong(孤竹行)
 */
public class QLExpressContext extends HashMap<String, Object> implements IExpressContext<String, Object> {

    public QLExpressContext(Map<String, Object> properties) {
        super(properties);
    }

    @Override
    public Object get(Object name) {
        Object result;
        result = super.get(name);

        try {
            if (result == null) {
                result = Lambkit.get((String) name);
            }
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
        return result;
    }

    @Override
    public Object put(String name, Object object) {
        super.put(name, object);
        return object;
    }
}
