package com.lambkit.plugin.qlexpress;

import java.lang.annotation.*;

/**
 * QLRule 注解
 * @author yangyong(孤竹行)
 */
@Documented
@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
public @interface QlRule {
    /**
     * 方法名称
     */
    String methodName();

    /**
     * 方法描述
     */
    String desc() default "";

}
