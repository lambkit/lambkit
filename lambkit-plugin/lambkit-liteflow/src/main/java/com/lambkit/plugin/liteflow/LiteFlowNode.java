package com.lambkit.plugin.liteflow;

import java.lang.annotation.*;

/**
 * @author yangyong(孤竹行)
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.TYPE)
@Documented
public @interface LiteFlowNode {

    String id();
    String name();

    //组件类型，common,switch,boolean,for,iterator
    String type() default "common";
}
