package com.lambkit.plugin.liteflow;

public interface INodeParamItem {
    /**
     * 参数名称
     * @return
     */
    String getName();

    /**
     * 参数标题
     * @return
     */
    String getTitle();

    /**
     * java类型
     * @return
     */
    String getJavaType();

    /**
     * 集合类型
     * @return
     */
    String getCollType();
    /**
     * 是否必填项
     * @return
     */
    boolean isRequired();

    /**
     * 显示排序
     * @return
     */
    int getOrders();

    /**
     * item的值
     * @return
     */
    String getValue();
}
