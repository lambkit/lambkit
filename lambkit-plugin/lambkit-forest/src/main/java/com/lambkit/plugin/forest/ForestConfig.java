package com.lambkit.plugin.forest;

import com.lambkit.core.config.annotation.PropConfig;

/**
 * @author yangyong(孤竹行)
 */
@PropConfig(prefix = "lambkit.forest")
public class ForestConfig {

    private String backendName = "okhttp3";
    private int maxConnections = 100;

    private int connectTimeout = 2000;

    private int readTimeout = 2000;

    private boolean logEnabled = true;

    public int getMaxConnections() {
        return maxConnections;
    }

    public void setMaxConnections(int maxConnections) {
        this.maxConnections = maxConnections;
    }

    public int getConnectTimeout() {
        return connectTimeout;
    }

    public void setConnectTimeout(int connectTimeout) {
        this.connectTimeout = connectTimeout;
    }

    public int getReadTimeout() {
        return readTimeout;
    }

    public void setReadTimeout(int readTimeout) {
        this.readTimeout = readTimeout;
    }

    public String getBackendName() {
        return backendName;
    }

    public void setBackendName(String backendName) {
        this.backendName = backendName;
    }

    public boolean isLogEnabled() {
        return logEnabled;
    }

    public void setLogEnabled(boolean logEnabled) {
        this.logEnabled = logEnabled;
    }
}
