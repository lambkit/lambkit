package com.lambkit.plugin.forest;

import com.dtflys.forest.Forest;
import com.dtflys.forest.config.ForestConfiguration;
import com.lambkit.core.Lambkit;
import com.lambkit.core.LifecycleException;
import com.lambkit.core.plugin.Plugin;

/**
 * @author yangyong(孤竹行)
 */
public class ForestPllugin extends Plugin {

    private ForestConfig forestConfig;

    public ForestPllugin() {
        forestConfig = Lambkit.config(ForestConfig.class);
    }

    public ForestPllugin(ForestConfig forestConfig) {
        this.forestConfig = forestConfig;
    }

    @Override
    public void start() throws LifecycleException {
        // 获取 Forest 全局配置对象
        ForestConfiguration configuration = Forest.config();
        // 支持okhttp3和httpclient两种后端 HTTP API，若不配置该属性，默认为okhttp3。
        configuration.setBackendName(forestConfig.getBackendName());
        // 连接池最大连接数
        configuration.setMaxConnections(forestConfig.getMaxConnections());
        // 连接超时时间，单位为毫秒
        configuration.setConnectTimeout(forestConfig.getConnectTimeout());
        // 数据读取超时时间，单位为毫秒
        configuration.setReadTimeout(forestConfig.getReadTimeout());
        // 打开或关闭日志，默认为true
        configuration.setLogEnabled(forestConfig.isLogEnabled());
    }
}
