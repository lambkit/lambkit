package com.lambkit.plugin.forest;

import com.dtflys.forest.callback.AddressSource;
import com.dtflys.forest.http.ForestAddress;
import com.dtflys.forest.http.ForestRequest;

/**
 * @author yangyong(孤竹行)
 */
public class DefaultAddressSource implements AddressSource {
    @Override
    public ForestAddress getAddress(ForestRequest req) {
        return null;
    }
}
