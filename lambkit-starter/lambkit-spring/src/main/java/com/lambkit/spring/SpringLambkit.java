package com.lambkit.spring;

import com.lambkit.core.Lambkit;
import org.springframework.boot.SpringApplication;
import org.springframework.context.ApplicationContext;

/**
 * @author yangyong(孤竹行)
 */
public class SpringLambkit {
    private static SpringApp app;
    public static SpringApp run(Class<?> primarySource, String... args) {
        ApplicationContext applicationContext = SpringApplication.run(primarySource, args);
        Lambkit.context().setAttr("springContext", applicationContext);
        app = (SpringApp) Lambkit.start(SpringApp.class, primarySource, args);
        return app;
    }

    public static SpringApp getApp() {
        return app;
    }
}
