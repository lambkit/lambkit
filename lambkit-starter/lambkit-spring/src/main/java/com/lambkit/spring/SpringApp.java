package com.lambkit.spring;

import cn.hutool.core.date.DateUtil;
import cn.hutool.core.date.TimeInterval;
import com.lambkit.core.Lambkit;
import com.lambkit.core.LambkitApp;
import com.lambkit.core.LifecycleException;
import com.lambkit.core.task.http.HttpWorkCenter;
import com.lambkit.plugin.http.HttpPlugin;
import com.lambkit.spring.service.SpringBeanFactory;
import com.lambkit.spring.service.SpringResourceFactory;
import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.stereotype.Service;

/**
 * @author yangyong(孤竹行)
 */
@Service
public class SpringApp extends LambkitApp {
    private TimeInterval timer = DateUtil.timer();

    public SpringApp(String name) {
        super(name);
        ApplicationContext applicationContext = Lambkit.context().getAttr("springContext");
        context().setAttr("ApplicationContext", applicationContext);
        context().setBeanFactory(new SpringBeanFactory(applicationContext));
        context().setResourceFactory(new SpringResourceFactory());
    }

    @Override
    public void start() throws LifecycleException {
        //初始化工作中心
        HttpWorkCenter workCenter = new HttpWorkCenter();
        context().addWorkCenter(workCenter.runOn(this));
        context().addPlugin(new HttpPlugin());
        super.start();
    }

    @Override
    public void postStart() throws LifecycleException {
        System.out.println(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>");
        System.out.println("Spring Lambkit Application Starting Complete in " + timer.interval() + "ms.");
        super.postStart();
    }

    @Override
    public void stop() throws LifecycleException {
        super.stop();
    }


}
