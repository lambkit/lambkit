package com.lambkit.spring;

import com.lambkit.core.LifecycleException;
import com.lambkit.core.config.AppConfig;

/**
 * @author yangyong(孤竹行)
 */
public class SpringAppConfig extends AppConfig {
    @Override
    public void start() throws LifecycleException {

    }
}
