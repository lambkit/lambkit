package com.lambkit.plugin.http.pipeline;


import com.lambkit.core.Attr;
import com.lambkit.core.http.IHttpContext;
import com.lambkit.core.task.http.HttpTaskContext;
import com.lambkit.core.task.http.HttpTaskValve;

/**
 * @author yangyong(孤竹行)
 */
public class HttpTaskValve001 extends HttpTaskValve {
    @Override
    public void invoke(HttpTaskContext valveContext, boolean isHandled) {
        //valveContext.getTask().setMessage("Hello AppValve001");
        IHttpContext context =  valveContext.getHttpContext();
        String msg = context.get("msg");
        System.out.println("HttpValve001: " + msg);
        next(valveContext, isHandled);
        valveContext.getResult().message(msg).attr(Attr.by("valve001", msg));
        //String msg = valveContext.getTask().getMessage();
        //valveContext.getTask().setMessage(msg + " end001");
    }
}
