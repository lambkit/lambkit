package com.lambkit.plugin.smarthttp.http;

/**
 * @author yangyong(孤竹行)
 */
import java.util.Enumeration;
import java.util.Iterator;
import java.util.NoSuchElementException;

public class ArrayEnumeration implements Enumeration<String> {
    private final Iterator<String> iterator;

    public ArrayEnumeration(String[] array) {
        if (array == null) {
            throw new IllegalArgumentException("Array cannot be null");
        }
        this.iterator = iterator(array);
    }

    private <T> Iterator<T> iterator(T[] array) {
        return new Iterator<T>() {
            private int index = 0;

            @Override
            public boolean hasNext() {
                return index < array.length;
            }

            @Override
            public T next() {
                if (this.hasNext()) {
                    return array[index++];
                }
                throw new ArrayIndexOutOfBoundsException();
            }
        };
    }

    @Override
    public boolean hasMoreElements() {
        return iterator.hasNext();
    }

    @Override
    public String nextElement() {
        if (iterator.hasNext()) {
            return iterator.next();
        } else {
            throw new NoSuchElementException("No more elements in the enumeration");
        }
    }
}
