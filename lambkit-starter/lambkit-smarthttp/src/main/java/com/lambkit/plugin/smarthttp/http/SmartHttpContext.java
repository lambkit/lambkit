package com.lambkit.plugin.smarthttp.http;

import com.lambkit.core.http.*;
import org.smartboot.http.server.HttpRequest;
import org.smartboot.http.server.HttpResponse;

import java.util.List;

/**
 * @author yangyong(孤竹行)
 */
public class SmartHttpContext extends BaseHttpContext {

    private SmartHttpRequest request;

    private SmartHttpResponse response;

    private String target;

    public SmartHttpContext(HttpRequest request, HttpResponse response) {
        this.request = new SmartHttpRequest(request);
        this.response = new SmartHttpResponse(response);
        this.target = request.getRequestURI();
    }

    @Override
    public IRequest getRequest() {
        return request;
    }

    @Override
    public void setHttpServletRequest(IRequest request) {
        if(request instanceof SmartHttpRequest) {
            this.request = (SmartHttpRequest) request;
            this.target = request.getRequestURI();
        }
    }

    @Override
    public void setHttpServletResponse(IResponse response) {
        if(response instanceof SmartHttpResponse) {
            this.response = (SmartHttpResponse) response;
        }
    }

    @Override
    public IResponse getResponse() {
        return response;
    }

    @Override
    public ISession getSession() {
        return request.getSession(false);
    }

    @Override
    public ISession getSession(boolean create) {
        return request.getSession(create);
    }

    public String getTarget() {
        return target;
    }

    @Override
    public void setTarget(String pathNew) {
        target = pathNew;
    }

    @Override
    public ICookie createCookie(String name, String value) {
        return new SmartHttpCookie(name, value);
    }

    @Override
    public void setCookie(String name, String value, int maxAgeInSeconds, String path, String domain, Boolean isHttpOnly, String sameSite) {
        ICookie cookie = new SmartHttpCookie(name, value);
        cookie.setMaxAge(maxAgeInSeconds);
        // set the default path value to "/"
        if (path == null) {
            path = "/";
        }
        cookie.setPath(path);

        if (domain != null) {
            cookie.setDomain(domain);
        }
        if (isHttpOnly != null) {
            cookie.setHttpOnly(isHttpOnly);
        }
        if (sameSite != null) {
            cookie.setSameSite(sameSite);
        }
        response.addCookie(cookie);
    }

    @Override
    public void doSetCookie(String name, String value, int maxAgeInSeconds, String path, String domain, Boolean isHttpOnly) {
        ICookie cookie = new SmartHttpCookie(name, value);
        cookie.setMaxAge(maxAgeInSeconds);
        // set the default path value to "/"
        if (path == null) {
            path = "/";
        }
        cookie.setPath(path);

        if (domain != null) {
            cookie.setDomain(domain);
        }
        if (isHttpOnly != null) {
            cookie.setHttpOnly(isHttpOnly);
        }
        response.addCookie(cookie);
    }

    @Override
    public IUploadFile getFile(String name, String uploadPath, long maxPostSize) {

        return null;
    }

    @Override
    public List<IUploadFile> getFiles(String uploadPath) {

        return null;
    }
}
