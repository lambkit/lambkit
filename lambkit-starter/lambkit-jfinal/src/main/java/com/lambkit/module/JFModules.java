package com.lambkit.module;

import com.jfinal.config.*;
import com.jfinal.template.Engine;

import java.util.ArrayList;
import java.util.List;

/**
 * @author yangyong(孤竹行)
 */
public class JFModules {
    private List<JFModule> modules = new ArrayList<JFModule>();

    public void configConstant(Constants constants) {
        for(int i=0; i<modules.size(); i++) {
            modules.get(i).configConstant(constants);
        }
    }

    public void configRoute(Routes routes) {
        for(int i=0; i<modules.size(); i++) {
            modules.get(i).configRoute(routes);
        }
    }

    public void configEngine(Engine engine) {
        for(int i=0; i<modules.size(); i++) {
            modules.get(i).configEngine(engine);
        }
    }

    public void configPlugin(Plugins plugins) {
        for(int i=0; i<modules.size(); i++) {
            modules.get(i).configPlugin(plugins);
        }
    }

    public void configInterceptor(Interceptors interceptors) {
        for(int i=0; i<modules.size(); i++) {
            modules.get(i).configInterceptor(interceptors);
        }
    }

    public void configHandler(Handlers handlers) {
        for(int i=0; i<modules.size(); i++) {
            modules.get(i).configHandler(handlers);
        }
    }

    /**
     * 启动模块
     */
    public void onStart(){
        for(int i=0; i<modules.size(); i++) {
            modules.get(i).start();
        }
    }

    /**
     * 停止模块
     */
    public void onStop(){
        for(int i=0; i<modules.size(); i++) {
            modules.get(i).stop();
        }
    }

    /**
     * 加入模块
     * @param module
     */
    public void addModule(JFModule module) {
        if(module != null) {
            module.init();
            modules.add(module);
        }
    }

    public List<JFModule> getModules() {
        return modules;
    }

    public void setModules(List<JFModule> modules) {
        this.modules = modules;
    }
}
