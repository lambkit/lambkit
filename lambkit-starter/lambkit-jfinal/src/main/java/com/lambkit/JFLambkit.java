package com.lambkit;

import com.lambkit.core.LambkitConfig;
import com.lambkit.core.Lambkit;
import com.lambkit.core.config.ConfigCenterContainer;
import com.lambkit.cache.JFCache;
import com.lambkit.cache.CacheManager;
import com.lambkit.plugin.ehcache.EhcacheCacheImpl;
import com.lambkit.plugin.redis.RedisCacheImpl;
import com.lambkit.service.*;
import com.lambkit.web.template.TemplateManager;
import com.lambkit.node.LambkitNode;
import com.lambkit.node.LambkitNodeManager;

import javax.servlet.http.HttpServletRequest;
import java.util.List;
import java.util.Map;

/**
 * @author yangyong(孤竹行)
 */
public class JFLambkit {

    private static JFinalApp jfApp;

    public static JFinalApp getApp() {
        return jfApp;
    }

    public static void setApp(JFinalApp app) {
        jfApp = app;
    }

    /**
     * 获取Config 配置文件
     *
     * @return
     */
    public static LambkitConfig getLambkitConfig() {
        return config(LambkitConfig.class);
    }

    public static ServiceManager serviceManager() {
        return ServiceManager.me();
    }

    /**
     * 获取实例
     * @param <T>
     * @param clazzName
     * @return
     */
    public static <T> T get(String clazzName) {
        return ServiceManager.me().get(clazzName);
    }

    /**
     * 获取实例
     * @param <T>
     * @param interfaceClass
     * @return
     */
    public static <T> T get(Class<T> interfaceClass) {
        return ServiceManager.me().get(interfaceClass);
    }

    public static <T> T get(Class<T> interfaceClass, Class<? extends T> defaultClass) {
        return ServiceManager.me().get(interfaceClass, defaultClass);
    }

    /**
     * 给实例注入对象
     * @return
     * @param <T>
     */
    public static <T> T inject(T targetObject) {
        return ServiceManager.me().inject(targetObject);
    }

    /**
     * 获取单例
     * @param clazz
     * @param <T>
     * @return
     */
    public static <T> T singleton(Class<T> clazz) {
        return ServiceManager.me().singleton(clazz);
    }

    public static <T> List<T> getBeansOfType(Class<T> interfaceClass) throws Exception {
        return ServiceManager.me().getBeansOfType(interfaceClass);
    }

    public static <T> Map<String, T> getBeansMapOfType(Class<T> interfaceClass) throws Exception {
        return ServiceManager.me().getBeansMapOfType(interfaceClass);
    }

    /**
     * 获取远程方法调用，采用RPC和Forest等方法实现。
     * @param clazz
     * @return
     * @param <T>
     */
//    public static <T> T client(Class<T> clazz) {
//        LambkitClient lambkitClient = clazz.getAnnotation(LambkitClient.class);
//        if (lambkitClient != null) {
//            return (T) HttpRpcProxy.getObject("", clazz);
//        }
//        ForestClient forestClient = clazz.getAnnotation(ForestClient.class);
//        if (forestClient != null) {
//            return (T) Forest.client(clazz);
//        }
//        return get(clazz);
//    }
    /**
     * 获取配置信息
     *
     * @param clazz
     * @param <T>
     * @return
     */
    public static <T> T config(Class<T> clazz) {
        return Lambkit.config(clazz);
    }

    /**
     * 读取配置文件信息
     *
     * @param clazz
     * @param prefix
     * @param <T>
     * @return
     */
    public static <T> T config(Class<T> clazz, String prefix) {
        return Lambkit.config(clazz, prefix);
    }

    public static ConfigCenterContainer configCenter() {
        return Lambkit.configCenter();
    }

    /**
     * 模板管理器
     * @return
     */
    public static TemplateManager getWebTemplates() {
        return TemplateManager.me();
    }

    public static String getAdminTemplateWebPath(HttpServletRequest request) {
        if(TemplateManager.me().getTemplateFolder(LambkitConsts.ADMIN_TEMPLATE_TYPE)==null) {
            //初始化admin管理页面的模板，upms，cms的管理页面都用到了
            TemplateManager.me().init(LambkitConsts.ADMIN_TEMPLATE_TYPE, "lambkit");
        }
        return TemplateManager.me().getCurrentWebPath(LambkitConsts.ADMIN_TEMPLATE_TYPE, request);
    }

    public static String getPortalTemplateWebPath(HttpServletRequest request) {
        if(TemplateManager.me().getTemplateFolder(LambkitConsts.PORTAL_TEMPLATE_TYPE)==null) {
            //初始化admin管理页面的模板，upms，cms的管理页面都用到了
            TemplateManager.me().init(LambkitConsts.PORTAL_TEMPLATE_TYPE, "portal");
        }
        return TemplateManager.me().getCurrentWebPath(LambkitConsts.PORTAL_TEMPLATE_TYPE, request);
    }

    public static String getTemplateWebPath(HttpServletRequest request, String templateId) {
        if(TemplateManager.me().getTemplateFolder(templateId)==null) {
            TemplateManager.me().init(templateId, templateId);
        }
        return TemplateManager.me().getCurrentWebPath(templateId, request);
    }
    /**
     * 获取系统节点信息
     * @return
     */
    public static LambkitNode getNode() {
        return LambkitNodeManager.me().getNode();
    }

    /**
     * 缓存
     * @return
     */
    public static JFCache getCache(String cacheKeys) {
        return CacheManager.me().getCache(cacheKeys);
    }

    public static JFCache getCache() {
        return CacheManager.me().getCache();
    }

//    public static EhcacheCacheImpl getEhCache() {
//        return CacheManager.me().getEhCache();
//    }
//
//    public static RedisCacheImpl getRedis() {
//        return CacheManager.me().getRedis();
//    }

    /**
     * 是否是开发模式
     *
     * @return
     */
    public static boolean isDevMode() {
        return getLambkitConfig().isDevMode();
    }

    /*****************************
     * 全局变量定义
     *****************************/
    private static Boolean isRunInjar = null;

    /**
     * 是否在jar包里运行
     *
     * @return
     */
    public static boolean isRunInJar() {
        if (isRunInjar == null) {
            isRunInjar = Thread.currentThread().getContextClassLoader().getResource("") == null;
        }
        return isRunInjar;
    }
}
