package com.lambkit.undertow;

import com.jfinal.server.undertow.UndertowConfig;
import com.jfinal.server.undertow.WebBuilder;
import com.lambkit.undertow.UndertowCallback;
import io.undertow.servlet.api.DeploymentInfo;

/**
 * @author yangyong(孤竹行)
 */
public class DefaultUndertowCallback implements UndertowCallback {
    @Override
    public void config(UndertowConfig undertowConfig) {

    }

    @Override
    public void configFilter(DeploymentInfo deploymentInfo) {

    }

    @Override
    public void configBuilder(WebBuilder builder) {

    }
}
