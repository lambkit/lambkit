/**
 * Copyright (c) 2015-2017, Henry Yang 杨勇 (gismail@foxmail.com).
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.lambkit.dao.record;

import cn.hutool.core.util.StrUtil;

/**
 * LambkitApiService接口工厂类
 */
public class LambkitApiServiceFactory {

	public <M extends LambkitRecord> LambkitApiService<M> create(Class<M> clazz, String configName, String tableName, String pkName) {
		LambkitApiServiceImpl<M> apiService = new LambkitApiServiceImpl<M>();
		if(StrUtil.isNotBlank(configName)) {
			apiService.mapping(configName, tableName, pkName);
		} else {
			apiService.mapping(tableName, pkName);
		}
		apiService.setLambkitPojoClass(clazz);
		return apiService;
	}
	
	public <M extends LambkitRecord> LambkitApiService<M> create(Class<M> clazz, String configName) {
		LambkitApiServiceImpl<M> apiService = new LambkitApiServiceImpl<M>();
		apiService.init(clazz, configName);
		return apiService;
	}
}