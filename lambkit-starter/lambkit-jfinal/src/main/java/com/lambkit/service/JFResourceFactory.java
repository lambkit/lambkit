package com.lambkit.service;

import com.lambkit.core.service.ResourceFactory;
import com.lambkit.core.service.ResourceService;
import com.lambkit.core.service.impl.ResourceServiceImpl;

/**
 * @author yangyong(孤竹行)
 */
public class JFResourceFactory implements ResourceFactory {

    private ResourceService resourceService = new ResourceServiceImpl();
    @Override
    public ResourceService getResourceService() {
        return resourceService;
    }
}
