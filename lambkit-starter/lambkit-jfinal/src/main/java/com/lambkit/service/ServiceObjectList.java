/**
 * Copyright (c) 2015-2017, Henry Yang 杨勇 (gismail@foxmail.com).
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */package com.lambkit.service;

import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.map.MapUtil;
import cn.hutool.core.util.StrUtil;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

/**
 * 接口服务对象
 */
public class ServiceObjectList implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private String serviceName;
	private List<ServiceObject> serviceObjectList;
	
	public ServiceObjectList(ServiceObject serviceObject) {
		serviceObjectList = CollUtil.newArrayList();
		serviceObjectList.add(serviceObject);
		this.serviceName = serviceObject.getInterfaceClass().getName();
	}
	
	public void addServiceObject(ServiceObject serviceObject) {
//		for (ServiceObject serviceObj : serviceObjectList) {
//			if(serviceObj.equals(serviceObject)) {
//				//存在相同的情况，直接返回
//				return;
//			}
//		}
		if(serviceObjectList.contains(serviceObject)) {
			//存在相同的情况，直接返回
			return;
		}
		serviceObjectList.add(serviceObject);
	}
	
	public ServiceObject getServiceObject() {
		if(serviceObjectList==null) {
			return null;
		}
		ServiceObject serviceObject = serviceObjectList.get(0);
		return serviceObject;
	}
	
	public ServiceObject getServiceObject(int type) {
		if(serviceObjectList==null) {
			return null;
		}
		for (ServiceObject serviceObject : serviceObjectList) {
			if(serviceObject.getType() == type) {
				return serviceObject;
			}
		}
		return null;
	}
	
	public ServiceObject getServiceObject(String code) {
		if(serviceObjectList==null) {
			return null;
		}
		if(StrUtil.isBlank(code)) {
			return serviceObjectList.get(0);
		}
		for (ServiceObject serviceObject : serviceObjectList) {
			if(serviceObject.getCode().equals(code)) {
				return serviceObject;
			}
		}
		return null;
	}
	
	public <T> T get() {
		if(serviceObjectList==null) {
			return null;
		}
		ServiceObject serviceObject = serviceObjectList.get(0);
		return serviceObject==null ? null : serviceObject.instance();
	}
	
	public <T> List<T> getList() {
		if(serviceObjectList==null) {
			return null;
		}
		List<T> targetObjects = CollUtil.newArrayList();
		for(ServiceObject serviceObject : serviceObjectList ) {
			if(serviceObject!=null ) {
				targetObjects.add(serviceObject.instance());
			}			
		}
		return targetObjects;
	}
	
	public <T> Map<String, T> getMap() {
		if(serviceObjectList==null) {
			return null;
		}
		Map<String, T> targetObjects = MapUtil.newHashMap();
		for(ServiceObject serviceObject : serviceObjectList ) {
			if(serviceObject!=null ) {
				targetObjects.put(serviceObject.getImplementClass().getSimpleName(),serviceObject.instance());
			}			
		}
		return targetObjects;
	}
	
	public <T> T get(int type) {
		if(serviceObjectList==null) {
			return null;
		}
		for (ServiceObject serviceObject : serviceObjectList) {
			if(serviceObject.getType() == type) {
				return serviceObject==null ? null : serviceObject.instance();
			}
		}
		return null;
	}
	
	public <T> T get(String code) {
		if(serviceObjectList==null) {
			return null;
		}
		if(StrUtil.isBlank(code)) {
			return get();
		}
		for (ServiceObject serviceObject : serviceObjectList) {
			if(serviceObject.getCode().equals(code)) {
				return serviceObject==null ? null : serviceObject.instance();
			}
		}
		return null;
	}
	
	public String getServiceName() {
		return serviceName;
	}

	public void setServiceName(String serviceName) {
		this.serviceName = serviceName;
	}
	
	public List<ServiceObject> getServiceObjectList() {
		return serviceObjectList;
	}
	
	public void setServiceObjectList(List<ServiceObject> serviceObjectList) {
		this.serviceObjectList = serviceObjectList;
	}
}
