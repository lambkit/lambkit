package com.lambkit.service;

import java.util.List;
import java.util.Map;

public interface BeanService {

    <T> T getBean(Class<T> clazz) throws Exception;

    <T> Map<String, T> getBeansMapOfType(Class<T> clazz) throws Exception;

    <T> List<T> getBeansOfType(Class<T> type) throws Exception;

}
