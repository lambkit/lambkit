package com.lambkit.annotion;

import com.jfinal.config.JFinalConfig;
import com.lambkit.undertow.DefaultUndertowCallback;
import com.lambkit.undertow.UndertowCallback;

import java.lang.annotation.*;

/**
 * @author yangyong(孤竹行)
 */
@Documented
@Target({ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
@Inherited
public @interface JFConfig {
    Class<? extends JFinalConfig> value();

    Class<? extends UndertowCallback> undertowCallback() default DefaultUndertowCallback.class;
}
