package com.lambkit.generator;

import com.jfinal.plugin.druid.DruidPlugin;
import com.lambkit.core.Lambkit;
import com.lambkit.db.datasource.DataSourceConfig;
import com.lambkit.generator.impl.CommonGenerator;
import com.lambkit.generator.impl.DatabaseGenerator;
import com.lambkit.generator.impl.MgrdbGenerator;
import com.lambkit.plugin.activerecord.JFinalDb;
import com.lambkit.plugin.activerecord.dialect.*;

import javax.sql.DataSource;
import java.util.Map;

public class Msch {

	public static void generator(String templateFolder, String templatePath, Map<String,Object> options, GeneratorConfig config) {
		//创建生成器
		IGenerator generator = createGenerator(null, options, config);
		//执行
		if(generator!=null) {
			generator.generate(templateFolder, templatePath, options);
			System.out.println("-------over-------");
		} else {
			System.out.println("生成器创建失败，请先初始化GeneratorManager!");
		}
	}
	public static void generator(DataSourceConfig dataSourceConfig, String templateFolder, String templatePath, Map<String,Object> options) {
		//创建生成器
		IGenerator generator = createGenerator(dataSourceConfig, options, null);
		//执行
		if(generator!=null) {
			generator.generate(templateFolder, templatePath, options);
			System.out.println("-------over-------");
		} else {
			System.out.println("生成器创建失败，请先初始化GeneratorManager!");
		}
	}

	public static IGenerator createGenerator(DataSourceConfig dataSourceConfig, Map<String,Object> options)  {
		return createGenerator(dataSourceConfig, options, null);
	}

	public static IGenerator createGeneratorForDb(JFinalDb jfdb, Map<String,Object> options, GeneratorConfig config)  {
		options.put("dialect", jfdb.getDialect());
		//创建生成器
		IGenerator generator = null;
		String generatortype = options.get("type")==null ? GeneratorType.DB.getValue() : options.get("type").toString();
		GeneratorType type = GeneratorType.valueOf(generatortype.trim().toUpperCase());
		switch (type) {
			case COMMON:
				generator = new CommonGenerator();
				break;
			case DB:
				generator = new DatabaseGenerator();
				break;
			case MGRDB:
				generator = new MgrdbGenerator();
				break;
			default:
				generator = Lambkit.get(generatortype.trim());
				break;
		}
		generator.context(jfdb.getDataSource(), options, config);
		return generator;
	}

	public static IGenerator createGenerator(DataSourceConfig dataSourceConfig, Map<String,Object> options, GeneratorConfig config)  {
		switch (dataSourceConfig.getType()) {
			case DataSourceConfig.TYPE_MYSQL:
				options.put("dialect", new LambkitMysqlDialect());
				break;
			case DataSourceConfig.TYPE_ORACLE:
				options.put("dialect", new LambkitOracleDialect());
				break;
			case DataSourceConfig.TYPE_SQLSERVER:
				options.put("dialect", new LambkitSqlServerDialect());
				break;
			case DataSourceConfig.TYPE_SQLITE:
				options.put("dialect", new LambkitSqlite3Dialect());
				break;
			case DataSourceConfig.TYPE_ANSISQL:
				options.put("dialect", new LambkitAnsiSqlDialect());
				break;
			case DataSourceConfig.TYPE_POSTGRESQL:
				options.put("dialect", new LambkitPostgreSqlDialect());
				break;
		}
		//创建生成器
		IGenerator generator = null;
		String generatortype = options.get("type")==null ? GeneratorType.DB.getValue() : options.get("type").toString();
		GeneratorType type = GeneratorType.valueOf(generatortype.trim().toUpperCase());
		switch (type) {
			case COMMON:
				generator = new CommonGenerator();
				break;
			case DB:
				generator = new DatabaseGenerator();
				break;
			case MGRDB:
				generator = new MgrdbGenerator();
				break;
			default:
				generator = Lambkit.get(generatortype.trim());
				break;
		}
		generator.context(getDataSource(dataSourceConfig), options, config);
		return generator;
	}

	private static DataSource getDataSource(DataSourceConfig dataSourceConfig) {
		DruidPlugin druidPlugin = new DruidPlugin(
				dataSourceConfig.getUrl(),
				dataSourceConfig.getUser(),
				dataSourceConfig.getPassword(),
				dataSourceConfig.getDriverClassName());
		boolean flag = druidPlugin.start();
		if(!flag) {
			System.out.println("数据库连接失败，请检查配置！");
			return null;
		}
		return druidPlugin.getDataSource();
	}
}
