/**
 * Copyright (c) 2015-2017, Henry Yang 杨勇 (gismail@foxmail.com).
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package #(basepackage).dao;

import com.jfinal.kit.StrKit;
import com.lambkit.Lambkit;
import com.lambkit.common.service.ServiceKit;
import com.lambkit.db.sql.column.Column;

import #(basepackage).dao.base.#(classname)Base;
import #(basepackage).dao.sql.#(classname)SqlBuilder;
import #(basepackage).dao.service.#(classname)ModelService;
import #(basepackage).dao.service.impl.#(classname)ModelServiceImpl;
import #(basepackage).pojo.#(classname);

/**
 * @author yangyong
 * @website: www.lambkit.com
 * @email: gismail@foxmail.com
 * @date #(date)
 * @version 1.0
 * @since 1.0
 */
public class #(classname)Model extends #(classname)Base<#(classname)Model> {

	private static final long serialVersionUID = 1L;

	public static #(classname)ModelService service() {
		return ServiceKit.get(#(classname)ModelService.class, #(classname)ModelServiceImpl.class);
	}

	public static #(classname)SqlBuilder sql() {
		return new #(classname)SqlBuilder();
	}

	public static #(classname)SqlBuilder sql(Column column) {
		#(classname)SqlBuilder that = new #(classname)SqlBuilder();
		that.add(column);
        return that;
    }

	public static #(classname)Model create(#(classname) bean) {
		#(classname)Model model = new #(classname)Model();
		model._setOrPut(bean.getColumns());
		return model;
	}

	public #(classname)Model() {
		#(moduleConfigClass) config = Lambkit.config(#(moduleConfigClass).class);
		String db_config = config.getDbconfig();
		if(StrKit.notBlank(db_config)) {
			this.use(db_config);
		}
	}

	public #(classname) toPojo() {
		#(classname) bean = new #(classname)();
		bean.setColumns(this);
		return bean;
	}
}
