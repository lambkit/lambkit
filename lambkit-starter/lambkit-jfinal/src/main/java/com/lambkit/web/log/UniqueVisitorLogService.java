package com.lambkit.web.log;

import com.jfinal.core.Controller;

/**
 * UV独立访客日活日志
 */
public interface UniqueVisitorLogService {
    void log(Controller controller);
}
