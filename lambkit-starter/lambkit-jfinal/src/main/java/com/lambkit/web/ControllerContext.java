package com.lambkit.web;

import cn.hutool.captcha.CaptchaUtil;
import cn.hutool.captcha.ShearCaptcha;
import cn.hutool.core.util.StrUtil;
import com.jfinal.core.Controller;
import com.jfinal.core.Injector;
import com.jfinal.core.converter.TypeConverter;
import com.jfinal.kit.Kv;
import com.jfinal.kit.Ret;
import com.jfinal.upload.MultipartRequest;
import com.jfinal.upload.UploadFile;
import com.lambkit.core.LambkitResult;
import com.lambkit.core.exception.LambkitException;
import com.lambkit.util.ServletRequestKit;
import com.lambkit.web.render.LambkitJsonRender;
import com.lambkit.web.render.LambkitTemplateRender;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.text.ParseException;
import java.util.*;

/**
 * 控制器上下文
 */
public class ControllerContext {

    private Controller controller;
    private HttpServletRequest request;
    private HttpServletResponse response;

    private String urlPara;
    private String[] urlParaArray;

    private String rawData;

    private Object handler;

    private static final String[] NULL_URL_PARA_ARRAY = new String[0];
    private String urlParaSeparator = "-";

    public ControllerContext(Controller controller) {
        this.request = controller.getRequest();
        this.response = controller.getResponse();
        this.rawData = controller.getRawData();
        this.controller = controller;
    }

    public ControllerContext(HttpServletRequest request, HttpServletResponse response, Object handler) {
        this.request = request;
        this.response = response;
        this.handler = handler;
    }

    public Object getHandler() {
        return handler;
    }

    public void setHandler(Object handler) {
        this.handler = handler;
    }

    /**
     * 获取 http 请求 body 中的原始数据，通常用于接收 json String 这类数据<br>
     * 可多次调用此方法，避免掉了 HttpKit.readData(...) 方式获取该数据时多次调用
     * 引发的异常
     * @return http 请求 body 中的原始数据
     */
    public String getRawData() {
        if (rawData == null) {
            rawData = ServletRequestKit.readData(request);
        }
        return rawData;
    }

    /**
     * Stores an attribute in this request
     * @param name a String specifying the name of the attribute
     * @param value the Object to be stored
     */
    public ControllerContext setAttr(String name, Object value) {
        request.setAttribute(name, value);
        return this;
    }

    /**
     * Removes an attribute from this request
     * @param name a String specifying the name of the attribute to remove
     */
    public ControllerContext removeAttr(String name) {
        request.removeAttribute(name);
        return this;
    }

    /**
     * Stores attributes in this request, key of the map as attribute name and value of the map as attribute value
     * @param attrMap key and value as attribute of the map to be stored
     */
    public ControllerContext setAttrs(Map<String, Object> attrMap) {
        for (Map.Entry<String, Object> entry : attrMap.entrySet()) {
            request.setAttribute(entry.getKey(), entry.getValue());
        }
        return this;
    }

    /**
     * Returns the value of a request parameter as a String, or null if the parameter does not exist.
     * <p>
     * You should only use this method when you are sure the parameter has only one value. If the
     * parameter might have more than one value, use getParaValues(java.lang.String).
     * <p>
     * If you use this method with a multivalued parameter, the value returned is equal to the first
     * value in the array returned by getParameterValues.
     * @param name a String specifying the name of the parameter
     * @return a String representing the single value of the parameter
     */
    public String getPara(String name) {
        return request.getParameter(name);
    }

    /**
     * Returns the value of a request parameter as a String, or default value if the parameter does not exist.
     * @param name a String specifying the name of the parameter
     * @param defaultValue a String value be returned when the value of parameter is null
     * @return a String representing the single value of the parameter
     */
    public String getPara(String name, String defaultValue) {
        String result = request.getParameter(name);
        return result != null && !"".equals(result) ? result : defaultValue;
    }

    /**
     * Returns the values of the request parameters as a Map.
     * @return a Map contains all the parameters name and value
     */
    public Map<String, String[]> getParaMap() {
        return request.getParameterMap();
    }

    /**
     * Returns an Enumeration of String objects containing the names of the parameters
     * contained in this request. If the request has no parameters, the method returns
     * an empty Enumeration.
     * @return an Enumeration of String objects, each String containing the name of
     * 			a request parameter; or an empty Enumeration if the request has no parameters
     */
    public Enumeration<String> getParaNames() {
        return request.getParameterNames();
    }

    /**
     * Returns an array of String objects containing all of the values the given request
     * parameter has, or null if the parameter does not exist. If the parameter has a
     * single value, the array has a length of 1.
     * @param name a String containing the name of the parameter whose value is requested
     * @return an array of String objects containing the parameter's values
     */
    public String[] getParaValues(String name) {
        return request.getParameterValues(name);
    }

    /**
     * Returns an array of Integer objects containing all of the values the given request
     * parameter has, or null if the parameter does not exist. If the parameter has a
     * single value, the array has a length of 1.
     * @param name a String containing the name of the parameter whose value is requested
     * @return an array of Integer objects containing the parameter's values
     */
    public Integer[] getParaValuesToInt(String name) {
        String[] values = request.getParameterValues(name);
        if (values == null || values.length == 0) {
            return null;
        }
        Integer[] result = new Integer[values.length];
        for (int i=0; i<result.length; i++) {
            result[i] = StrUtil.isBlank(values[i]) ? null : Integer.parseInt(values[i]);
        }
        return result;
    }

    public Long[] getParaValuesToLong(String name) {
        String[] values = request.getParameterValues(name);
        if (values == null || values.length == 0) {
            return null;
        }
        Long[] result = new Long[values.length];
        for (int i=0; i<result.length; i++) {
            result[i] = StrUtil.isBlank(values[i]) ? null : Long.parseLong(values[i]);
        }
        return result;
    }

    /**
     * Returns an Enumeration containing the names of the attributes available to this request.
     * This method returns an empty Enumeration if the request has no attributes available to it.
     * @return an Enumeration of strings containing the names of the request's attributes
     */
    public Enumeration<String> getAttrNames() {
        return request.getAttributeNames();
    }

    /**
     * Returns the value of the named attribute as an Object, or null if no attribute of the given name exists.
     * @param name a String specifying the name of the attribute
     * @return an Object containing the value of the attribute, or null if the attribute does not exist
     */
    public <T> T getAttr(String name) {
        return (T)request.getAttribute(name);
    }

    public <T> T getAttr(String name, T defaultValue) {
        T result = (T)request.getAttribute(name);
        return result != null ? result : defaultValue;
    }

    /**
     * Returns the value of the named attribute as an Object, or null if no attribute of the given name exists.
     * @param name a String specifying the name of the attribute
     * @return an String Object containing the value of the attribute, or null if the attribute does not exist
     */
    public String getAttrForStr(String name) {
        return (String)request.getAttribute(name);
    }

    /**
     * Returns the value of the named attribute as an Object, or null if no attribute of the given name exists.
     * @param name a String specifying the name of the attribute
     * @return an Integer Object containing the value of the attribute, or null if the attribute does not exist
     */
    public Integer getAttrForInt(String name) {
        return (Integer)request.getAttribute(name);
    }

    /**
     * Returns the value of the specified request header as a String.
     */
    public String getHeader(String name) {
        return request.getHeader(name);
    }

    private Integer toInt(String value, Integer defaultValue) {
        try {
            if (StrUtil.isBlank(value)) {
                return defaultValue;
            }
            value = value.trim();
            if (value.startsWith("N") || value.startsWith("n")) {
                return -Integer.parseInt(value.substring(1));
            }
            return Integer.parseInt(value);
        } catch (Exception e) {
            throw new LambkitException("Can not parse the parameter \"" + value + "\" to Integer value.");
            //throw new ActionException(400, renderManager.getRenderFactory().getErrorRender(400),  "Can not parse the parameter \"" + value + "\" to Integer value.");
        }
    }

    /**
     * Returns the value of a request parameter and convert to Integer.
     * @param name a String specifying the name of the parameter
     * @return a Integer representing the single value of the parameter
     */
    public Integer getParaToInt(String name) {
        return toInt(request.getParameter(name), null);
    }

    /**
     * Returns the value of a request parameter and convert to Integer with a default value if it is null.
     * @param name a String specifying the name of the parameter
     * @return a Integer representing the single value of the parameter
     */
    public Integer getParaToInt(String name, Integer defaultValue) {
        return toInt(request.getParameter(name), defaultValue);
    }

    private Long toLong(String value, Long defaultValue) {
        try {
            if (StrUtil.isBlank(value)) {
                return defaultValue;
            }
            value = value.trim();
            if (value.startsWith("N") || value.startsWith("n")) {
                return -Long.parseLong(value.substring(1));
            }
            return Long.parseLong(value);
        }
        catch (Exception e) {
            throw new LambkitException("Can not parse the parameter \"" + value + "\" to Long value.");
            //throw new ActionException(400, renderManager.getRenderFactory().getErrorRender(400),  "Can not parse the parameter \"" + value + "\" to Long value.");
        }
    }

    /**
     * Returns the value of a request parameter and convert to Long.
     * @param name a String specifying the name of the parameter
     * @return a Integer representing the single value of the parameter
     */
    public Long getParaToLong(String name) {
        return toLong(request.getParameter(name), null);
    }

    /**
     * Returns the value of a request parameter and convert to Long with a default value if it is null.
     * @param name a String specifying the name of the parameter
     * @return a Integer representing the single value of the parameter
     */
    public Long getParaToLong(String name, Long defaultValue) {
        return toLong(request.getParameter(name), defaultValue);
    }

    private Boolean toBoolean(String value, Boolean defaultValue) {
        if (StrUtil.isBlank(value)) {
            return defaultValue;
        }
        value = value.trim().toLowerCase();
        if ("1".equals(value) || "true".equals(value)) {
            return Boolean.TRUE;
        }
        else if ("0".equals(value) || "false".equals(value)) {
            return Boolean.FALSE;
        }
        throw new LambkitException("Can not parse the parameter \"" + value + "\" to Boolean value.");
        //throw new ActionException(400, renderManager.getRenderFactory().getErrorRender(400),  "Can not parse the parameter \"" + value + "\" to Boolean value.");
    }

    /**
     * Returns the value of a request parameter and convert to Boolean.
     * @param name a String specifying the name of the parameter
     * @return true if the value of the parameter is "true" or "1", false if it is "false" or "0", null if parameter is not exists
     */
    public Boolean getParaToBoolean(String name) {
        return toBoolean(request.getParameter(name), null);
    }

    /**
     * Returns the value of a request parameter and convert to Boolean with a default value if it is null.
     * @param name a String specifying the name of the parameter
     * @return true if the value of the parameter is "true" or "1", false if it is "false" or "0", default value if it is null
     */
    public Boolean getParaToBoolean(String name, Boolean defaultValue) {
        return toBoolean(request.getParameter(name), defaultValue);
    }

    /**
     * Get all para from url and convert to Boolean
     */
//    public Boolean getParaToBoolean() {
//        return toBoolean(getPara(), null);
//    }
//
//    /**
//     * Get para from url and conver to Boolean. The first index is 0
//     */
//    public Boolean getParaToBoolean(int index) {
//        return toBoolean(getPara(index), null);
//    }
//
//    /**
//     * Get para from url and conver to Boolean with default value if it is null.
//     */
//    public Boolean getParaToBoolean(int index, Boolean defaultValue) {
//        return toBoolean(getPara(index), defaultValue);
//    }

    private Date toDate(String value, Date defaultValue) {
        try {
            if (StrUtil.isBlank(value)) {
                return defaultValue;
            }
            // return new java.text.SimpleDateFormat("yyyy-MM-dd").parse(value.trim());
            return (Date) TypeConverter.me().convert(Date.class, value);
        } catch (Exception e) {
            throw new LambkitException("Can not parse the parameter \"" + value + "\" to Date value.");
            //throw new ActionException(400, renderManager.getRenderFactory().getErrorRender(400),  "Can not parse the parameter \"" + value + "\" to Date value.");
        }
    }

    /**
     * Returns the value of a request parameter and convert to Date.
     * @param name a String specifying the name of the parameter
     * @return a Date representing the single value of the parameter
     */
    public Date getParaToDate(String name) {
        return toDate(request.getParameter(name), null);
    }

    /**
     * Returns the value of a request parameter and convert to Date with a default value if it is null.
     * @param name a String specifying the name of the parameter
     * @return a Date representing the single value of the parameter
     */
    public Date getParaToDate(String name, Date defaultValue) {
        return toDate(request.getParameter(name), defaultValue);
    }

    /**
     * Return HttpServletRequest. Do not use HttpServletRequest Object in constructor of ControllerContext
     */
    public HttpServletRequest getRequest() {
        return request;
    }

    public void setHttpServletRequest(HttpServletRequest request) {
        this.request = request;
    }

    public void setHttpServletResponse(HttpServletResponse response) {
        this.response = response;
    }

    /**
     * Return HttpServletResponse. Do not use HttpServletResponse Object in constructor of ControllerContext
     */
    public HttpServletResponse getResponse() {
        return response;
    }

    /**
     * Return HttpSession.
     */
    public HttpSession getSession() {
        return request.getSession();
    }

    /**
     * Return HttpSession.
     * @param create a boolean specifying create HttpSession if it not exists
     */
    public HttpSession getSession(boolean create) {
        return request.getSession(create);
    }

    /**
     * Return a Object from session.
     * @param key a String specifying the key of the Object stored in session
     */
    public <T> T getSessionAttr(String key) {
        HttpSession session = request.getSession(false);
        return session != null ? (T)session.getAttribute(key) : null;
    }

    public <T> T getSessionAttr(String key, T defaultValue) {
        T result = getSessionAttr(key);
        return result != null ? result : defaultValue;
    }

    /**
     * Store Object to session.
     * @param key a String specifying the key of the Object stored in session
     * @param value a Object specifying the value stored in session
     */
    public ControllerContext setSessionAttr(String key, Object value) {
        request.getSession(true).setAttribute(key, value);
        return this;
    }

    /**
     * Remove Object in session.
     * @param key a String specifying the key of the Object stored in session
     */
    public ControllerContext removeSessionAttr(String key) {
        HttpSession session = request.getSession(false);
        if (session != null) {
            session.removeAttribute(key);
        }
        return this;
    }

    /**
     * Get cookie value by cookie name.
     */
    public String getCookie(String name, String defaultValue) {
        Cookie cookie = getCookieObject(name);
        return cookie != null ? cookie.getValue() : defaultValue;
    }

    /**
     * Get cookie value by cookie name.
     */
    public String getCookie(String name) {
        return getCookie(name, null);
    }

    /**
     * Get cookie value by cookie name and convert to Integer.
     */
    public Integer getCookieToInt(String name) {
        String result = getCookie(name);
        return result != null ? Integer.parseInt(result) : null;
    }

    /**
     * Get cookie value by cookie name and convert to Integer.
     */
    public Integer getCookieToInt(String name, Integer defaultValue) {
        String result = getCookie(name);
        return result != null ? Integer.parseInt(result) : defaultValue;
    }

    /**
     * Get cookie value by cookie name and convert to Long.
     */
    public Long getCookieToLong(String name) {
        String result = getCookie(name);
        return result != null ? Long.parseLong(result) : null;
    }

    /**
     * Get cookie value by cookie name and convert to Long.
     */
    public Long getCookieToLong(String name, Long defaultValue) {
        String result = getCookie(name);
        return result != null ? Long.parseLong(result) : defaultValue;
    }

    /**
     * Get cookie object by cookie name.
     */
    public Cookie getCookieObject(String name) {
        Cookie[] cookies = request.getCookies();
        if (cookies != null) {
            for (Cookie cookie : cookies) {
                if (cookie.getName().equals(name)) {
                    return cookie;
                }
            }
        }
        return null;
    }

    /**
     * Get all cookie objects.
     */
    public Cookie[] getCookieObjects() {
        Cookie[] result = request.getCookies();
        return result != null ? result : new Cookie[0];
    }

    /**
     * Set Cookie.
     * @param name cookie name
     * @param value cookie value
     * @param maxAgeInSeconds -1: clear cookie when close browser. 0: clear cookie immediately.  n>0 : max age in n seconds.
     * @param isHttpOnly true if this cookie is to be marked as HttpOnly, false otherwise
     */
    public ControllerContext setCookie(String name, String value, int maxAgeInSeconds, boolean isHttpOnly) {
        return doSetCookie(name, value, maxAgeInSeconds, null, null, isHttpOnly);
    }

    /**
     * Set Cookie.
     * @param name cookie name
     * @param value cookie value
     * @param maxAgeInSeconds -1: clear cookie when close browser. 0: clear cookie immediately.  n>0 : max age in n seconds.
     */
    public ControllerContext setCookie(String name, String value, int maxAgeInSeconds) {
        return doSetCookie(name, value, maxAgeInSeconds, null, null, null);
    }

    /**
     * Set Cookie to response.
     */
    public ControllerContext setCookie(Cookie cookie) {
        response.addCookie(cookie);
        return this;
    }

    /**
     * Set Cookie to response.
     * @param name cookie name
     * @param value cookie value
     * @param maxAgeInSeconds -1: clear cookie when close browser. 0: clear cookie immediately.  n>0 : max age in n seconds.
     * @param path see Cookie.setPath(String)
     * @param isHttpOnly true if this cookie is to be marked as HttpOnly, false otherwise
     */
    public ControllerContext setCookie(String name, String value, int maxAgeInSeconds, String path, boolean isHttpOnly) {
        return doSetCookie(name, value, maxAgeInSeconds, path, null, isHttpOnly);
    }

    /**
     * Set Cookie to response.
     * @param name cookie name
     * @param value cookie value
     * @param maxAgeInSeconds -1: clear cookie when close browser. 0: clear cookie immediately.  n>0 : max age in n seconds.
     * @param path see Cookie.setPath(String)
     */
    public ControllerContext setCookie(String name, String value, int maxAgeInSeconds, String path) {
        return doSetCookie(name, value, maxAgeInSeconds, path, null, null);
    }

    /**
     * Set Cookie to response.
     * @param name cookie name
     * @param value cookie value
     * @param maxAgeInSeconds -1: clear cookie when close browser. 0: clear cookie immediately.  n>0 : max age in n seconds.
     * @param path see Cookie.setPath(String)
     * @param domain the domain name within which this cookie is visible; form is according to RFC 2109
     * @param isHttpOnly true if this cookie is to be marked as HttpOnly, false otherwise
     */
    public ControllerContext setCookie(String name, String value, int maxAgeInSeconds, String path, String domain, boolean isHttpOnly) {
        return doSetCookie(name, value, maxAgeInSeconds, path, domain, isHttpOnly);
    }

    /**
     * Remove Cookie.
     */
    public ControllerContext removeCookie(String name) {
        return doSetCookie(name, null, 0, null, null, null);
    }

    /**
     * Remove Cookie.
     */
    public ControllerContext removeCookie(String name, String path) {
        return doSetCookie(name, null, 0, path, null, null);
    }

    /**
     * Remove Cookie.
     */
    public ControllerContext removeCookie(String name, String path, String domain) {
        return doSetCookie(name, null, 0, path, domain, null);
    }

    private ControllerContext doSetCookie(String name, String value, int maxAgeInSeconds, String path, String domain, Boolean isHttpOnly) {
        Cookie cookie = new Cookie(name, value);
        cookie.setMaxAge(maxAgeInSeconds);
        // set the default path value to "/"
        if (path == null) {
            path = "/";
        }
        cookie.setPath(path);

        if (domain != null) {
            cookie.setDomain(domain);
        }
        if (isHttpOnly != null) {
            cookie.setHttpOnly(isHttpOnly);
        }
        response.addCookie(cookie);
        return this;
    }

    /**
     * 获取被 Kv 封装后的参数，便于使用 Kv 中的一些工具方法
     *
     * 由于 Kv 继承自 HashMap，也便于需要使用 HashMap 的场景，
     * 例如：
     * Record record = new Record().setColumns(getKv());
     */
    public Kv getKv() {
        Kv kv = new Kv();
        Map<String, String[]> paraMap = request.getParameterMap();
        for (Map.Entry<String, String[]> entry : paraMap.entrySet()) {
            String[] values = entry.getValue();
            String value = (values != null && values.length > 0) ? values[0] : null;
            kv.put(entry.getKey(), "".equals(value) ? null : value);
        }
        return kv;
    }

    /**
     * Keep all parameter's value except model value
     */
    public ControllerContext keepPara() {
        Map<String, String[]> map = request.getParameterMap();
        for (Map.Entry<String, String[]> e: map.entrySet()) {
            String[] values = e.getValue();
            if (values.length == 1) {
                request.setAttribute(e.getKey(), values[0]);
            }
            else {
                request.setAttribute(e.getKey(), values);
            }
        }
        return this;
    }

    /**
     * Keep parameter's value names pointed, model value can not be kept
     */
    public ControllerContext keepPara(String... names) {
        for (String name : names) {
            String[] values = request.getParameterValues(name);
            if (values != null) {
                if (values.length == 1) {
                    request.setAttribute(name, values[0]);
                }
                else {
                    request.setAttribute(name, values);
                }
            }
        }
        return this;
    }

    /**
     * Convert para to special type and keep it
     */
    public ControllerContext keepPara(Class type, String name) {
        String[] values = request.getParameterValues(name);
        if (values != null) {
            if (values.length == 1) {
                try {
                    request.setAttribute(name, TypeConverter.me().convert(type, values[0]));
                } catch (ParseException e) {
                    com.jfinal.kit.LogKit.logNothing(e);
                }
            }
            else {
                request.setAttribute(name, values);
            }
        }
        return this;
    }

    public ControllerContext keepPara(Class type, String... names) {
        if (type == String.class) {
            return keepPara(names);
        }
        if (names != null) {
            for (String name : names) {
                keepPara(type, name);
            }
        }
        return this;
    }

    /**
     * Return true if the para value is blank otherwise return false
     */
    public boolean isParaBlank(String paraName) {
        return StrUtil.isBlank(request.getParameter(paraName));
    }

    /**
     * Return true if the para exists otherwise return false
     */
    public boolean isParaExists(String paraName) {
        return request.getParameterMap().containsKey(paraName);
    }

    /**
     * 为了进一步省代码，创建与 setAttr(...) 功能一模一样的缩短版本 set(...)
     */
    public ControllerContext set(String attributeName, Object attributeValue) {
        request.setAttribute(attributeName, attributeValue);
        return this;
    }

    // --- 以下是为了省代码，为 getPara 系列方法创建的缩短版本

    public String get(String name) {
        return getPara(name);
    }

    public String get(String name, String defaultValue) {
        return getPara(name, defaultValue);
    }

    public Integer getInt(String name) {
        return getParaToInt(name);
    }

    public Integer getInt(String name, Integer defaultValue) {
        return getParaToInt(name, defaultValue);
    }

    public Long getLong(String name) {
        return getParaToLong(name);
    }

    public Long getLong(String name, Long defaultValue) {
        return getParaToLong(name, defaultValue);
    }

    public Boolean getBoolean(String name) {
        return getParaToBoolean(name);
    }

    public Boolean getBoolean(String name, Boolean defaultValue) {
        return getParaToBoolean(name, defaultValue);
    }

    public Date getDate(String name) {
        return getParaToDate(name);
    }

    public Date getDate(String name, Date defaultValue) {
        return getParaToDate(name, defaultValue);
    }

    // --- 以下是将自定义参数解析方法/action/para1-para2-para3形式

    public void setUrlPara(String urlPara) {
        this.urlPara = urlPara;
        this.urlParaArray = null;
    }

    public void setUrlPara(String urlPara, String separator) {
        this.urlPara = urlPara;
        this.urlParaArray = null;
        this.urlParaSeparator = separator;
    }

    public void setUrlParaSeparator(String separator) {
        this.urlParaSeparator = separator;
    }

    /**
     * Get all para with separator char from url
     */
    public String getPara() {
        if ("".equals(urlPara)) {// urlPara maybe is "" see ActionMapping.getAction(String)
            urlPara = null;
        }
        return urlPara;
    }

    /**
     * Get para from url. The index of first url para is 0.
     */
    public String getPara(int index) {
        if (index < 0) {
            return getPara();
        }
        if (urlParaArray == null) {
            if (urlPara == null || "".equals(urlPara)) {    // urlPara maybe is "" see ActionMapping.getAction(String)
                urlParaArray = NULL_URL_PARA_ARRAY;
            } else {
                urlParaArray = urlPara.split(urlParaSeparator);
            }
            for (int i=0; i<urlParaArray.length; i++) {
                if ("".equals(urlParaArray[i])) {
                    urlParaArray[i] = null;
                }
            }
        }
        return urlParaArray.length > index ? urlParaArray[index] : null;
    }

    /**
     * Get para from url with default value if it is null or "".
     */
    public String getPara(int index, String defaultValue) {
        String result = getPara(index);
        return result != null && !"".equals(result) ? result : defaultValue;
    }

    /**
     * Get para from url and conver to Integer. The first index is 0
     */
    public Integer getParaToInt(int index) {
        return toInt(getPara(index), null);
    }

    /**
     * Get para from url and conver to Integer with default value if it is null.
     */
    public Integer getParaToInt(int index, Integer defaultValue) {
        return toInt(getPara(index), defaultValue);
    }

    /**
     * Get para from url and conver to Long.
     */
    public Long getParaToLong(int index) {
        return toLong(getPara(index), null);
    }

    /**
     * Get para from url and conver to Long with default value if it is null.
     */
    public Long getParaToLong(int index, Long defaultValue) {
        return toLong(getPara(index), defaultValue);
    }

    /**
     * Get all para from url and convert to Integer
     */
    public Integer getParaToInt() {
        return toInt(getPara(), null);
    }

    /**
     * Get all para from url and convert to Long
     */
    public Long getParaToLong() {
        return toLong(getPara(), null);
    }

    /**
     * Get all para from url and convert to Boolean
     */
    public Boolean getParaToBoolean() {
        return toBoolean(getPara(), null);
    }

    /**
     * Get para from url and conver to Boolean. The first index is 0
     */
    public Boolean getParaToBoolean(int index) {
        return toBoolean(getPara(index), null);
    }

    /**
     * Get para from url and conver to Boolean with default value if it is null.
     */
    public Boolean getParaToBoolean(int index, Boolean defaultValue) {
        return toBoolean(getPara(index), defaultValue);
    }

    // --- 以下是 getPara 系列中获取 urlPara 的缩短版本

	/* 为了让继承类可以使用名为 get 的 action 注掉此方法，可使用 get(-1) 来实现本方法的功能
	public String get() {
		return getPara();
	} */

    public String get(int index) {
        return getPara(index);
    }

    public String get(int index, String defaultValue) {
        return getPara(index, defaultValue);
    }

    public Integer getInt() {
        return getParaToInt();
    }

    public Integer getInt(int index) {
        return getParaToInt(index);
    }

    public Integer getInt(int index, Integer defaultValue) {
        return getParaToInt(index, defaultValue);
    }

    public Long getLong() {
        return getParaToLong();
    }

    public Long getLong(int index) {
        return getParaToLong(index);
    }

    public Long getLong(int index, Long defaultValue) {
        return getParaToLong(index, defaultValue);
    }

    public Boolean getBoolean() {
        return getParaToBoolean();
    }

    public Boolean getBoolean(int index) {
        return getParaToBoolean(index);
    }

    public Boolean getBoolean(int index, Boolean defaultValue) {
        return getParaToBoolean(index, defaultValue);
    }

    // --- model and bean

    /**
     * Get model from http request.
     */
    public <T> T getModel(Class<T> modelClass) {
        return (T)Injector.injectModel(modelClass, request, false);
    }

    public <T> T getModel(Class<T> modelClass, boolean skipConvertError) {
        return (T)Injector.injectModel(modelClass, request, skipConvertError);
    }

    /**
     * Get model from http request.
     */
    public <T> T getModel(Class<T> modelClass, String modelName) {
        return (T)Injector.injectModel(modelClass, modelName, request, false);
    }

    public <T> T getModel(Class<T> modelClass, String modelName, boolean skipConvertError) {
        return (T)Injector.injectModel(modelClass, modelName, request, skipConvertError);
    }

    public <T> T getBean(Class<T> beanClass) {
        return (T) Injector.injectBean(beanClass, request, false);
    }

    public <T> T getBean(Class<T> beanClass, boolean skipConvertError) {
        return (T)Injector.injectBean(beanClass, request, skipConvertError);
    }

    public <T> T getBean(Class<T> beanClass, String beanName) {
        return (T)Injector.injectBean(beanClass, beanName, request, false);
    }

    public <T> T getBean(Class<T> beanClass, String beanName, boolean skipConvertError) {
        return (T)Injector.injectBean(beanClass, beanName, request, skipConvertError);
    }

    /**
     * 校验码
     */
    public void renderCaptcha() {
        response.setHeader("Cache-Control", "no-store");
        response.setHeader("Pragma", "no-cache");
        response.setDateHeader("Expires", 0);
        response.setContentType("image/png");

        ShearCaptcha shearCaptcha = CaptchaUtil.createShearCaptcha(108, 40, 4, 2);

        // 验证码存入session
        getSession().setAttribute("verifyCode", shearCaptcha.getCode());
        //System.out.println("verifyCode: " + getSession().getAttribute("verifyCode"));

        // 输出图片流
        try {
            shearCaptcha.write(response.getOutputStream());
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
    public boolean validateCaptcha(String captcha) {
        String captchaCode = getPara(captcha);
        //System.out.println("captchaCode: " + captchaCode);
        if(StrUtil.isBlank(captchaCode)){
            return false;
        }
//        ShearCaptcha shearCaptcha = (ShearCaptcha) getSession().getAttribute("verifyCode");
//        System.out.println("verifyCode: " + getSession().getAttribute("verifyCode"));
//        if(shearCaptcha==null || !shearCaptcha.verify(captchaCode)) {
//            return false;
//        }
        String shearCaptcha = (String) getSession().getAttribute("verifyCode");
        //System.out.println("verifyCode: " + getSession().getAttribute("verifyCode"));
        if(shearCaptcha==null || !shearCaptcha.equalsIgnoreCase(captchaCode)) {
            return false;
        }
        return true;
    }

    public boolean isPOST() {
        return getRequest().getMethod().equals("POST");
    }

    public boolean isGET() {
        return getRequest().getMethod().equals("GET");
    }

    public boolean isPut() {
        return getRequest().getMethod().equals("PUT");
    }

    public boolean isDelete() {
        return getRequest().getMethod().equals("DELETE");
    }

    // --------

    /**
     * Get upload file from multipart request.
     */
    public List<UploadFile> getFiles(String uploadPath, long maxPostSize, String encoding) {
        if(controller!=null) {
            return controller.getFiles(uploadPath, maxPostSize, encoding);
        }
        if (request instanceof MultipartRequest == false) {
            request = new MultipartRequest(request, uploadPath, maxPostSize, encoding);
        }
        return ((MultipartRequest)request).getFiles();
    }

    public UploadFile getFile(String parameterName, String uploadPath, long maxPostSize, String encoding) {
        if(controller!=null) {
            return controller.getFile(parameterName, uploadPath, maxPostSize, encoding);
        }
        getFiles(uploadPath, maxPostSize, encoding);
        return getFile(parameterName);
    }

    public List<UploadFile> getFiles(String uploadPath, long maxPostSize) {
        if(controller!=null) {
            return controller.getFiles(uploadPath, maxPostSize);
        }
        if (request instanceof MultipartRequest == false) {
            request = new MultipartRequest(request, uploadPath, maxPostSize);
        }
        return ((MultipartRequest)request).getFiles();
    }

    public UploadFile getFile(String parameterName, String uploadPath, long maxPostSize) {
        if(controller!=null) {
            return controller.getFile(parameterName, uploadPath, maxPostSize);
        }
        getFiles(uploadPath, maxPostSize);
        return getFile(parameterName);
    }

    public List<UploadFile> getFiles(String uploadPath) {
        if(controller!=null) {
            return controller.getFiles(uploadPath);
        }
        if (request instanceof MultipartRequest == false) {
            request = new MultipartRequest(request, uploadPath);
        }
        return ((MultipartRequest)request).getFiles();
    }

    public UploadFile getFile(String parameterName, String uploadPath) {
        if(controller!=null) {
            return controller.getFile(parameterName, uploadPath);
        }
        getFiles(uploadPath);
        return getFile(parameterName);
    }

    public List<UploadFile> getFiles() {
        if(controller!=null) {
            return controller.getFiles();
        }
        if (request instanceof MultipartRequest == false) {
            request = new MultipartRequest(request);
        }
        return ((MultipartRequest)request).getFiles();
    }

    public UploadFile getFile() {
        if(controller!=null) {
            return controller.getFile();
        }
        List<UploadFile> uploadFiles = getFiles();
        return uploadFiles.size() > 0 ? uploadFiles.get(0) : null;
    }

    public UploadFile getFile(String parameterName) {
        if(controller!=null) {
            return controller.getFile(parameterName);
        }
        List<UploadFile> uploadFiles = getFiles();
        for (UploadFile uploadFile : uploadFiles) {
            if (uploadFile.getParameterName().equals(parameterName)) {
                return uploadFile;
            }
        }
        return null;
    }

    // --------
    public void renderHtml(String html) {
        LambkitTemplateRender templateRender = new LambkitTemplateRender(html);
        templateRender.setContext(request, response);
        templateRender.render();
    }

    public void renderJson(LambkitResult result) {
        LambkitJsonRender jsonRender = new LambkitJsonRender(result);
        jsonRender.setContext(request, response);
        jsonRender.forIE().render();
    }

    public void renderJson(Ret result) {
        LambkitJsonRender jsonRender = new LambkitJsonRender(result);
        jsonRender.setContext(request, response);
        jsonRender.forIE().render();
    }

    // --------
    /**
     * It creates the extra attribute below while tomcat take SSL open.
     * http://git.oschina.net/jfinal/jfinal/issues/10
     */
    protected static final Set<String> excludedAttrs = new HashSet<String>() {
        private static final long serialVersionUID = 9186138395157680676L;
        {
            add("javax.servlet.request.ssl_session");
            add("javax.servlet.request.ssl_session_id");
            add("javax.servlet.request.ssl_session_mgr");
            add("javax.servlet.request.key_size");
            add("javax.servlet.request.cipher_suite");
            add("_res");	// I18nInterceptor 中使用的 _res
        }
    };

    public Map getRequestAttrs(String[] attrs) {
        Map map = new HashMap();
        if (attrs != null) {
            for (String key : attrs) {
                map.put(key, request.getAttribute(key));
            }
        } else {
            for (Enumeration<String> attrsEnu=request.getAttributeNames(); attrsEnu.hasMoreElements();) {
                String key = attrsEnu.nextElement();
                if (excludedAttrs.contains(key)) {
                    continue;
                }

                Object value = request.getAttribute(key);
                map.put(key, value);
            }
        }
        return map;
        //this.jsonText = JsonKit.toJson(map);
    }



}
