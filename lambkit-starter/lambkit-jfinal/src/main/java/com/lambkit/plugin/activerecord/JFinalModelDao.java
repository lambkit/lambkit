package com.lambkit.plugin.activerecord;

import com.jfinal.plugin.activerecord.ActiveRecordException;
import com.jfinal.plugin.activerecord.Config;
import com.jfinal.plugin.activerecord.PageSqlKit;
import com.jfinal.plugin.activerecord.SqlPara;
import com.lambkit.db.*;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

/**
 * @author yangyong(孤竹行)
 */
public class JFinalModelDao<M extends RowModel<M>> extends JFinalDbOpt<M, PageData<M>>  implements IRowDao<M> {

    private Class<M> modelClass;

    public JFinalModelDao(JFinalDb jFinalDb, Class<M> clazz) {
        super(jFinalDb.getDb(), jFinalDb.getDialect());
        this.modelClass = clazz;
    }

    public SqlPara toSqlPara(Sql sql) {
        SqlPara sqlPara = new SqlPara();
        sqlPara.setSql(sql.getSql());
        List<Object> paras = sql.getParaList();
        for(int i=0; i<paras.size(); i++) {
            sqlPara.addPara(paras.get(i));
        }
        return sqlPara;
    }

    public Sql toSql(SqlPara sqlPara) {
        Sql sql = new Sql();
        sql.setSql(sqlPara.getSql());
        Object[] paras = sqlPara.getPara();
        sql.setPara(paras);
        return sql;
    }

    @Override
    public PageData<M> paginate(Integer pageNumber, Integer pageSize, Sql sql) {
        String[] sqls = PageSqlKit.parsePageSql(sql.getSql());
        String select = sqls[0];
        String sqlExceptSelect = sqls[1];
        return doPaginate(pageNumber, pageSize, (Boolean) null, select, sqlExceptSelect, sql.getPara());
    }

    @Override
    public List<M> find(String sql, Object... paras) {
        Connection conn = null;
        try {
            conn = getConnection();
            return find(getConfig(), conn, sql, paras);
        } catch (Exception e) {
            throw new ActiveRecordException(e);
        } finally {
            closeConnection(conn);
        }
    }

    @Override
    public M findFirst(String sql, Object... paras) {
        List<M> result = this.find(sql, paras);
        return result.size() > 0 ? result.get(0) : null;
//        Connection conn = null;
//        try {
//            conn = getConnection();
//            return findFirst(getConfig(), conn, sql, paras);
//        } catch (Exception e) {
//            throw new ActiveRecordException(e);
//        } finally {
//            closeConnection(conn);
//        }
    }

    protected List<M> find(Config config, Connection conn, String sql, Object... paras) throws SQLException, InstantiationException, IllegalAccessException {
        try (PreparedStatement pst = conn.prepareStatement(sql)) {
            config.getDialect().fillStatement(pst, paras);
            ResultSet rs = pst.executeQuery();
            List<M> result = ResultSetBuilder.of().buildList(rs, true, modelClass);
            close(rs);
            return result;
        }
    }

    protected M findFirst(Config config, Connection conn, String sql, Object... paras) throws SQLException, InstantiationException, IllegalAccessException {
        try (PreparedStatement pst = conn.prepareStatement(sql)) {
            config.getDialect().fillStatement(pst, paras);
            ResultSet rs = pst.executeQuery();
            M result = ResultSetBuilder.of().build(rs, true, modelClass);
            close(rs);
            return result;
        }
    }

    protected PageData<M> doPaginate(int pageNumber, int pageSize, Boolean isGroupBySql, String select, String sqlExceptSelect, Object... paras) {
        Connection conn = null;
        try {
            conn = getConfig().getConnection();
            String totalRowSql = getConfig().getDialect().forPaginateTotalRow(select, sqlExceptSelect, null);
            StringBuilder findSql = new StringBuilder();
            findSql.append(select).append(' ').append(sqlExceptSelect);
            return doPaginateByFullSql(getConfig(), conn, pageNumber, pageSize, isGroupBySql, totalRowSql, findSql, paras);
        } catch (Exception e) {
            throw new ActiveRecordException(e);
        } finally {
            getConfig().close(conn);
        }
    }

    protected PageData<M> doPaginateByFullSql(Config config, Connection conn, int pageNumber, int pageSize, Boolean isGroupBySql, String totalRowSql, StringBuilder findSql, Object... paras) throws SQLException, InstantiationException, IllegalAccessException {
        if (pageNumber < 1 || pageSize < 1) {
            throw new ActiveRecordException("pageNumber and pageSize must more than 0");
        }
        if (config.getDialect().isTakeOverDbPaginate()) {
            //return getConfig().getDialect().takeOverDbPaginate(conn, pageNumber, pageSize, isGroupBySql, totalRowSql, findSql, paras);
            throw new RuntimeException("You should implements this method in " + config.getDialect().getClass().getName());
        }

        List result = query(config, conn, totalRowSql, paras);
        int size = result.size();
        if (isGroupBySql == null) {
            isGroupBySql = size > 1;
        }

        long totalRow;
        if (isGroupBySql) {
            totalRow = size;
        } else {
            totalRow = (size > 0) ? ((Number)result.get(0)).longValue() : 0;
        }
        if (totalRow == 0) {
            return new PageData<M>(new ArrayList<M>(0), pageNumber, pageSize, 0, 0);
        }

        int totalPage = (int) (totalRow / pageSize);
        if (totalRow % pageSize != 0) {
            totalPage++;
        }

        if (pageNumber > totalPage) {
            return new PageData<M>(new ArrayList<M>(0), pageNumber, pageSize, totalPage, (int)totalRow);
        }

        // --------
        String sql = config.getDialect().forPaginate(pageNumber, pageSize, findSql);
        List<M> list = find(config, conn, sql, paras);
        return new PageData<M>(list, pageNumber, pageSize, totalPage, (int)totalRow);
    }

    protected <T> List<T> query(Config config, Connection conn, String sql, Object... paras) throws SQLException {
        List result = new ArrayList();
        try (PreparedStatement pst = conn.prepareStatement(sql)) {
            config.getDialect().fillStatement(pst, paras);
            ResultSet rs = pst.executeQuery();
            int colAmount = rs.getMetaData().getColumnCount();
            if (colAmount > 1) {
                while (rs.next()) {
                    Object[] temp = new Object[colAmount];
                    for (int i=0; i<colAmount; i++) {
                        temp[i] = rs.getObject(i + 1);
                    }
                    result.add(temp);
                }
            }
            else if(colAmount == 1) {
                while (rs.next()) {
                    result.add(rs.getObject(1));
                }
            }
            close(rs);
            return result;
        }
    }

    public Connection getConnection() throws SQLException {
        return getConfig().getConnection();
    }

    public void closeConnection(Connection connection) {
        getConfig().close(connection);
    }

    public Config getConfig() {
        return getDb().getConfig();
    }

    void close(ResultSet rs, Statement st) throws SQLException {
        if (rs != null) {rs.close();}
        if (st != null) {st.close();}
    }

    void close(ResultSet rs) throws SQLException {
        if (rs != null) {rs.close();}
    }

    void close(Statement st) throws SQLException {
        if (st != null) {st.close();}
    }
}
