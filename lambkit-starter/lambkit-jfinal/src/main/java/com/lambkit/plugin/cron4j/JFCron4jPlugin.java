package com.lambkit.plugin.cron4j;

import com.jfinal.kit.Prop;
import com.jfinal.plugin.cron4j.Cron4jPlugin;
import com.lambkit.core.LifecycleException;
import com.lambkit.core.plugin.Plugin;
import it.sauronsoftware.cron4j.ProcessTask;
import it.sauronsoftware.cron4j.Task;

public class JFCron4jPlugin extends Plugin {
    private Cron4jPlugin cron4jPlugin;

    public JFCron4jPlugin() {
        cron4jPlugin = new Cron4jPlugin();
    }

    public JFCron4jPlugin(String configFile) {
        cron4jPlugin = new Cron4jPlugin(configFile);
    }

    public JFCron4jPlugin(Prop configProp) {
        cron4jPlugin = new Cron4jPlugin(configProp);
    }

    public JFCron4jPlugin(String configFile, String configName) {
        cron4jPlugin = new Cron4jPlugin(configFile, configName);
    }

    public JFCron4jPlugin(Prop configProp, String configName) {
        cron4jPlugin = new Cron4jPlugin(configProp, configName);
    }

    public JFCron4jPlugin addTask(String cron, Runnable task, boolean daemon, boolean enable) {
        cron4jPlugin.addTask(cron, task, daemon, enable);
        return this;
    }

    public JFCron4jPlugin addTask(String cron, Runnable task, boolean daemon) {
        return this.addTask(cron, task, daemon, true);
    }

    public JFCron4jPlugin addTask(String cron, Runnable task) {
        return this.addTask(cron, task, false, true);
    }

    public JFCron4jPlugin addTask(String cron, ProcessTask processTask, boolean daemon, boolean enable) {
        cron4jPlugin.addTask(cron, processTask, daemon, enable);
        return this;
    }

    public JFCron4jPlugin addTask(String cron, ProcessTask processTask, boolean daemon) {
        return this.addTask(cron, processTask, daemon, true);
    }

    public JFCron4jPlugin addTask(String cron, ProcessTask processTask) {
        return this.addTask(cron, processTask, false, true);
    }

    public JFCron4jPlugin addTask(String cron, Task task, boolean daemon, boolean enable) {
        cron4jPlugin.addTask(cron, task, daemon, enable);
        return this;
    }

    public JFCron4jPlugin addTask(String cron, Task task, boolean daemon) {
        return this.addTask(cron, task, daemon, true);
    }

    public JFCron4jPlugin addTask(String cron, Task task) {
        return this.addTask(cron, task, false, true);
    }

    @Override
    public void start() throws LifecycleException {
        cron4jPlugin.start();
    }

    @Override
    public void stop() throws LifecycleException {
        cron4jPlugin.stop();
    }
}
