package com.lambkit.plugin.activerecord;

import com.jfinal.plugin.activerecord.cache.ICache;
import com.lambkit.cache.CacheManager;

public class ActiveRecordCache implements ICache {

    @Override
    public <T> T get(String s, Object o) {
        return CacheManager.me().getCache().get(s, o);
    }

    @Override
    public void put(String s, Object o, Object o1) {
        CacheManager.me().getCache().put(s, o, o1);
    }

    @Override
    public void remove(String s, Object o) {
        CacheManager.me().getCache().remove(s, o);
    }

    @Override
    public void removeAll(String s) {
        CacheManager.me().getCache().removeAll(s);
    }
}
