package com.lambkit.node;

import com.jfinal.config.Plugins;
import com.jfinal.config.Routes;
import com.lambkit.core.CronExecutor;
import com.lambkit.module.JFModule;

public class LambkitNodeModule extends JFModule {

	@Override
	public void configRoute(Routes me) {
		super.configRoute(me);
		me.add(LambkitNodeManager.route, LambkitNodeParentController.class);
	}

	@Override
	public void configPlugin(Plugins me) {
		super.configPlugin(me);
//		Cron4jPlugin cron4j = new Cron4jPlugin();
//		cron4j.addTask("* * * * *", new LambkitNodeTask());
	}

	@Override
	public void onStart() {
		super.onStart();
        //子节点去访问父节点
		CronExecutor cronExecutor = CronExecutor.create(5, true, new LambkitNodeTask());
		cronExecutor.start();
	}

}
