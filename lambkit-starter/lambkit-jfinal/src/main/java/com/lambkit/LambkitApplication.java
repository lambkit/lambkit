package com.lambkit;

import com.lambkit.core.Lambkit;

/**
 * @author yangyong(孤竹行)
 */
public class LambkitApplication {
    public static void start(Class<?> tartgetClass, String[] args) {
        Lambkit.start(tartgetClass, args);
    }
}
