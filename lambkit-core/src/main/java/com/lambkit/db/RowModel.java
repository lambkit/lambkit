package com.lambkit.db;

import java.io.Serializable;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.sql.Time;
import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.util.*;

/**
 * @author yangyong(孤竹行)
 */
public abstract class RowModel<M extends RowModel> implements IRowData<M>, Serializable {
    String configName;
    Set<String> modifyFlag;

    private RowData rowData = new RowData();

    @Override
    public String tableName() {
        return rowData.tableName();
    }

    @Override
    public M setTableName(String tableName) {
        rowData.setTableName(tableName);
        return (M) this;
    }

    @Override
    public Set<String> columnNames() {
        return rowData.columnNames();
    }

    @Override
    public M setColumnNames(Collection<String> columnName) {
        rowData.setColumnNames(columnName);
        return (M) this;
    }

    @Override
    public M addColumnNames(String... columnName) {
        rowData.addColumnNames(columnName);
        return (M) this;
    }

    @Override
    public void remove(String columnName) {
        rowData.remove(columnName);
    }

    @Override
    public String primaryKey() {
        return rowData.primaryKey();
    }

    @Override
    public M setPrimaryKey(String primaryKey) {
        rowData.setPrimaryKey(primaryKey);
        return (M) this;
    }

    @Override
    public Map<String, Object> toMap() {
        return rowData.toMap();
    }

    @Override
    public M put(Map<String, Object> otherMap) {
        rowData.put(otherMap);
        return (M) this;
    }

    @Override
    public M set(String key, Object value) {
        rowData.set(key, value);
        return (M) this;
    }

    @Override
    public <T> T get(String key) {
        return rowData.get(key);
    }

    @Override
    public <T> T get(String key, T defaultValue) {
        return rowData.get(key, defaultValue);
    }

    @Override
    public String getStr(String key) {
        return rowData.getStr(key);
    }

    @Override
    public Integer getInt(String key) {
        return rowData.getInt(key);
    }

    @Override
    public Long getLong(String key) {
        return rowData.getLong(key);
    }

    @Override
    public BigInteger getBigInteger(String key) {
        return rowData.getBigInteger(key);
    }

    @Override
    public Date getDate(String key) {
        return rowData.getDate(key);
    }

    @Override
    public LocalDateTime getLocalDateTime(String key) {
        return rowData.getLocalDateTime(key);
    }

    @Override
    public Time getTime(String key) {
        return rowData.getTime(key);
    }

    @Override
    public Timestamp getTimestamp(String key) {
        return rowData.getTimestamp(key);
    }

    @Override
    public Double getDouble(String key) {
        return rowData.getDouble(key);
    }

    @Override
    public Float getFloat(String key) {
        return rowData.getFloat(key);
    }

    @Override
    public Short getShort(String key) {
        return rowData.getShort(key);
    }

    @Override
    public Byte getByte(String key) {
        return rowData.getByte(key);
    }

    @Override
    public Boolean getBoolean(String key) {
        return rowData.getBoolean(key);
    }

    @Override
    public BigDecimal getBigDecimal(String key) {
        return rowData.getBigDecimal(key);
    }

    @Override
    public byte[] getBytes(String key) {
        return rowData.getBytes(key);
    }

    @Override
    public Number getNumber(String key) {
        return rowData.getNumber(key);
    }

    @Override
    public int size() {
        return rowData.size();
    }
}
