package com.lambkit.db.hutool;

import cn.hutool.db.handler.RsHandler;
import com.lambkit.db.PageData;
import com.lambkit.db.ResultSetBuilder;
import com.lambkit.db.RowModel;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

/**
 * @author yangyong(孤竹行)
 */
public class ModelPageHandler<ROW extends RowModel<ROW>> implements RsHandler<PageData<ROW>> {

    private boolean withMetaInfo = true;
    private Class<ROW> rowClass;
    private int pageNumber;
    private int pageSize;
    private int total;

    public ModelPageHandler(Class<ROW> rowClass, boolean withMetaInfo, int pageNumber, int pageSize, int total) {
        this.rowClass = rowClass;
        this.withMetaInfo = withMetaInfo;
        this.pageNumber = pageNumber;
        this.pageSize = pageSize;
        this.total = total;
    }

    @Override
    public PageData<ROW> handle(ResultSet rs) throws SQLException {
        return ResultSetBuilder.of().buildPage(rs, withMetaInfo, pageNumber, pageSize, total, rowClass);
    }

    public void setWithMetaInfo(boolean withMetaInfo) {
        this.withMetaInfo = withMetaInfo;
    }
}
