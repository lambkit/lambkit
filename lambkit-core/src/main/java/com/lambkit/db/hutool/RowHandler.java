package com.lambkit.db.hutool;

import cn.hutool.db.handler.RsHandler;
import com.lambkit.db.IRowData;
import com.lambkit.db.ResultSetBuilder;
import com.lambkit.db.RowModel;

import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * @author yangyong(孤竹行)
 */
public class RowHandler<ROW extends IRowData<ROW>> implements RsHandler<ROW> {

    private boolean withMetaInfo = true;

    private Class<ROW> rowClass;

    public RowHandler(Class<ROW> rowClass, boolean withMetaInfo) {
        this.rowClass = rowClass;
        this.withMetaInfo = withMetaInfo;
    }

    @Override
    public ROW handle(ResultSet rs) throws SQLException {
        return ResultSetBuilder.of().build(rs, withMetaInfo, rowClass);
    }

    public void setWithMetaInfo(boolean withMetaInfo) {
        this.withMetaInfo = withMetaInfo;
    }
}
