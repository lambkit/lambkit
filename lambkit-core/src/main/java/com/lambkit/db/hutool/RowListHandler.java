package com.lambkit.db.hutool;

import cn.hutool.db.handler.RsHandler;
import com.lambkit.db.IRowData;
import com.lambkit.db.ResultSetBuilder;
import com.lambkit.db.RowModel;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

/**
 * @author yangyong(孤竹行)
 */
public class RowListHandler<ROW extends IRowData<ROW>> implements RsHandler<List<ROW>> {

    private boolean withMetaInfo = true;

    private Class<ROW> rowClass;

    public RowListHandler(Class<ROW> rowClass, boolean withMetaInfo) {
        this.rowClass = rowClass;
        this.withMetaInfo = withMetaInfo;
    }

    @Override
    public List<ROW> handle(ResultSet rs) throws SQLException {
        return ResultSetBuilder.of().buildList(rs, withMetaInfo, rowClass);
    }

    public void setWithMetaInfo(boolean withMetaInfo) {
        this.withMetaInfo = withMetaInfo;
    }
}
