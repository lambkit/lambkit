package com.lambkit.db.hutool;

import cn.hutool.db.handler.RsHandler;
import com.lambkit.db.ResultSetBuilder;
import com.lambkit.db.RowData;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

/**
 * @author yangyong(孤竹行)
 */
public class RowDataListHandler implements RsHandler<List<RowData>> {

    private boolean withMetaInfo = true;

    public static RowDataListHandler create() {
        return new RowDataListHandler();
    }

    public static RowDataListHandler create(boolean withMetaInfo) {
        RowDataListHandler handler = new RowDataListHandler();
        handler.setWithMetaInfo(withMetaInfo);
        return handler;
    }

    @Override
    public List<RowData> handle(ResultSet rs) throws SQLException {
        return ResultSetBuilder.of().buildList(rs, withMetaInfo, RowData.class);
    }

    public void setWithMetaInfo(boolean withMetaInfo) {
        this.withMetaInfo = withMetaInfo;
    }
}
