package com.lambkit.db.sql;

import java.io.Serializable;

public class JoinOnField implements Serializable {

	private String mainField;
	private String joinField;

	public JoinOnField(String mainField, String joinField) {
		this.mainField = mainField;
		this.joinField = joinField;
	}

	public String getJoinField() {
		return joinField;
	}

	public void setJoinField(String joinField) {
		this.joinField = joinField;
	}

	public String getMainField() {
		return mainField;
	}

	public void setMainField(String mainField) {
		this.mainField = mainField;
	}
}
