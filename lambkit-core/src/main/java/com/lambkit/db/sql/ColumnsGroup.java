package com.lambkit.db.sql;

import cn.hutool.core.collection.CollUtil;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import cn.hutool.core.util.StrUtil;
import com.lambkit.db.meta.ColumnMeta;
import com.lambkit.db.meta.TableMeta;

import java.util.List;

public class ColumnsGroup extends Columns {

	/**
	 * 
	 */
	private static final long serialVersionUID = 4312562714499507154L;
	
	private List<Columns> oredColumns;

	private SqlConcatGroup sqlConcatGroup = new SqlConcatGroup();

	public ColumnsGroup() {
		oredColumns = CollUtil.newArrayList();
	}
	
	
	public static ColumnsGroup create() {
        return new ColumnsGroup();
    }
    
    public static ColumnsGroup create(Column column) {
    	ColumnsGroup that = new ColumnsGroup();
        that.oredColumns.add(Columns.create(column));
        return that;

    }

    public static Columns create(String name, Object value) {
        return create().eq(name, value);
    }
    
    public static ColumnsGroup by() {
        return new ColumnsGroup();
    }
    
    public static ColumnsGroup by(Column column) {
    	ColumnsGroup that = new ColumnsGroup();
        that.oredColumns.add(Columns.create(column));
        return that;
    }

    public static ColumnsGroup by(String name, Object value) {
    	ColumnsGroup that = new ColumnsGroup();
        that.oredColumns.add(Columns.by(name, value));
        return that;
    }
    
	public ColumnsGroup columnsWithOr(Columns columns) {
		columns.withOr();
        oredColumns.add(columns);
        return this;
    }
	
	public ColumnsGroup columnsWithAnd(Columns columns) {
		columns.withAnd();
        oredColumns.add(columns);
        return this;
    }
	
	public ColumnsGroup addColumns(Columns columns) {
        oredColumns.add(columns);
        return this;
    }
	
	public ColumnsGroup columns(Columns columns) {
        oredColumns.add(columns);
        return this;
    }

	public List<Columns> getOredColumns() {
		return oredColumns;
	}

	public void setOredColumns(List<Columns> oredColumns) {
		this.oredColumns = oredColumns;
	}
	
	public Columns columns(int i) {
		return oredColumns.get(i);
    }
	
	public ColumnsGroup filter(String json, TableMeta tableMeta) {
		JSONObject jsonObj = JSON.parseObject(json);
		filter(jsonObj, tableMeta);
		return this;
	}
	
	public ColumnsGroup filter(JSONObject jsonObj, TableMeta tableMeta) {
		sqlConcatGroup.filter(this, jsonObj, tableMeta);
		return this;
	}
		
	public ColumnsGroup filter(String field, String value, ColumnMeta columnMeta) {
		if(StrUtil.isNotBlank(field)) {
			String javatype = columnMeta!=null ? columnMeta.getJavaType() : "java.lang.String";
			filter(field, value, javatype, true);
		}
		return this;
	}

	public ColumnsGroup filter(String field, String value, String type, boolean javaType) {
		sqlConcatGroup.filter(this, field, value, type, javaType);
		return this;
	}
	
	/**
	 * 归集所有warns消息
	 * @return
	 */
	public List<String> getMessageList() {
		List<String> msg = CollUtil.newArrayList();
		if(getWarns()!=null) {
			msg.addAll(getWarns());
		}
		for (Columns columns : getOredColumns()) {
			if(columns instanceof ColumnsGroup) {
    			ColumnsGroup group = (ColumnsGroup) columns;
    			msg.addAll(group.getMessageList());
    		} else {
    			if(columns.getWarns()!=null) {
    				msg.addAll(columns.getWarns());
    			}
            }
		}
        return msg;
	}
	
	/**
	 * 归集所有引用
	 * @return
	 */
	public List<String> getRefList() {
		List<String> msg = CollUtil.newArrayList();
		if(getRefs()!=null) {
			msg.addAll(getRefs());
		}
		for (Columns columns : getOredColumns()) {
			if(columns instanceof ColumnsGroup) {
    			ColumnsGroup group = (ColumnsGroup) columns;
    			msg.addAll(group.getRefList());
    		} else {
    			if(columns.getRefs()!=null) {
    				msg.addAll(columns.getRefs());
    			}
            }
		}
        return msg;
	}
}
