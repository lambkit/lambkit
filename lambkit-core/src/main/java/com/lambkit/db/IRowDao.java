package com.lambkit.db;

/**
 * @author yangyong(孤竹行)
 */
public interface IRowDao<M extends RowModel<M>> extends IDbOpt<M, PageData<M>> {
}
