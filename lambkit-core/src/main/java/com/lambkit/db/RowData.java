package com.lambkit.db;

import cn.hutool.core.collection.CollectionUtil;
import cn.hutool.core.util.ArrayUtil;
import cn.hutool.db.Entity;
import com.lambkit.core.BaseAttr;

import java.util.Collection;
import java.util.Collections;
import java.util.Map;
import java.util.Set;

/**
 * @author yangyong(孤竹行)
 */
public class RowData extends BaseAttr<RowData> implements IRowData<RowData>  {
    private String tableName;

    private Set<String> columnNames;

    private String primaryKey;

    public static RowData of(Entity entity) {
        RowData rowData = new RowData();
        rowData.put(entity);
        rowData.setTableName(entity.getTableName());
        rowData.setColumnNames(entity.getFieldNames());
        return rowData;
    }

    public static RowData by(Map<String, Object> record) {
        RowData rowData = new RowData();
        rowData.put(record);
        return rowData;
    }

    @Override
    public String tableName() {
        return tableName;
    }

    @Override
    public RowData setTableName(String tableName) {
        this.tableName = tableName;
        return this;
    }

    @Override
    public Set<String> columnNames() {
        return columnNames;
    }

    @Override
    public RowData setColumnNames(Collection<String> columnName) {
        if (CollectionUtil.isNotEmpty(columnName)) {
            this.columnNames = CollectionUtil.newHashSet(true, columnName);
        }
        return this;
    }

    @Override
    public RowData addColumnNames(String... columnName) {
        if (ArrayUtil.isNotEmpty(columnName)) {
            if (null == this.columnNames) {
                this.columnNames = CollectionUtil.newLinkedHashSet(columnName);
            } else {
                Collections.addAll(this.columnNames, columnName);
            }
        }
        return this;
    }

    @Override
    public void remove(String columnName) {
        super.remove(columnName);
    }

    @Override
    public String primaryKey() {
        return primaryKey;
    }

    @Override
    public RowData setPrimaryKey(String primaryKey) {
        this.primaryKey = primaryKey;
        return this;
    }
}
