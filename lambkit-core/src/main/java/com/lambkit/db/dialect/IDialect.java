/**
 * Copyright (c) 2015-2017, Henry Yang 杨勇 (gismail@foxmail.com).
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.lambkit.db.dialect;

import com.lambkit.db.IRowData;
import com.lambkit.db.Sql;
import com.lambkit.db.sql.Columns;
import com.lambkit.db.sql.Example;

public interface IDialect {

	public String selectSql(String sql, String orderBy, Object limit);
    public String selectSqlByColumns(String table, String loadColumns, Columns columns, String orderBy, Object limit);
    public String paginateSelect(String loadColumns);
    public String paginateByColumns(String table, Columns columns, String orderBy);
    
    public Sql findBySql(Sql sqlPara, String orderBy, Object limit);
    public Sql findByExample(Example example, Object limit);
    public Sql paginateByExample(Example example);
    public Sql paginateFormByExample(Example example);
    public Sql deleteByExample(Example example);
    public Sql updateByExample(IRowData rowData, Example example);
    String tableBuilderDoBuild(String tableName);
}
