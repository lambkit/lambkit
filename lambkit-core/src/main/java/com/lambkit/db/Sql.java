package com.lambkit.db;

import cn.hutool.core.collection.CollUtil;

import java.util.List;

/**
 * @author yangyong(孤竹行)
 */
public class Sql {

    private String sql;
    private List<Object> para;

    public Sql() {
        para = CollUtil.newArrayList();
    }

    public Sql(String sql, Object... para) {
        this.sql = sql;
        this.para = CollUtil.newArrayList(para);
    }

    public String getSql() {
        return sql;
    }

    public void setSql(String sql) {
        this.sql = sql;
    }

    public Object[] getPara() {
        return para.toArray(new Object[para.size()]);
    }

    public List<Object> getParaList() {
        return para;
    }

    public void setPara(Object[] para) {
        this.para = CollUtil.newArrayList(para);
    }

    public void addPara(Object value) {
        this.para.add(value);
    }

    public void addPara(Object... value) {
        this.para.addAll(CollUtil.newArrayList(value));
    }
}
