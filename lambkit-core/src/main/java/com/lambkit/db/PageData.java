package com.lambkit.db;

import java.util.List;

/**
 * @author yangyong(孤竹行)
 */
public class PageData<ROW extends IRowData<ROW>> implements IPageData<ROW> {

    private List<ROW> rows;

    private int pageNumber;
    private int pageSize;
    private int totalPage;
    private int totalRow;

    public PageData() {
    }

    public PageData(int pageNumber, int pageSize, int total) {
        this.pageNumber = Math.max(pageNumber, 0);
        this.pageSize = pageSize <= 0 ? 20 : pageSize;
        this.totalRow = total;
        long totalCount = (long) total;
        this.totalPage = Math.toIntExact(totalCount % pageSize == 0 ? (totalCount / pageSize) : (totalCount / pageSize + 1));
    }

    public PageData(List<ROW> rows, int pageNumber, int pageSize, int totalPage, int totalRow) {
        this.rows = rows;
        this.pageNumber = pageNumber;
        this.pageSize = pageSize;
        this.totalPage = totalPage;
        this.totalRow = totalRow;
    }
    @Override
    public List<ROW> getList() {
        return rows;
    }

    @Override
    public void setList(List<ROW> list) {
        this.rows = list;
    }

    @Override
    public int getPage() {
        return pageNumber;
    }

    @Override
    public void setPage(int pageNumber) {
        this.pageNumber = pageNumber;
    }

    @Override
    public int getPageSize() {
        return pageSize;
    }

    @Override
    public void setPageSize(int pageSize) {
        this.pageSize = pageSize;
    }

    @Override
    public int getTotalPage() {
        return totalPage;
    }

    @Override
    public void setTotalPage(int totalPage) {
        this.totalPage = totalPage;
    }

    @Override
    public int getTotal() {
        return totalRow;
    }

    @Override
    public void setTotal(int totalRow) {
        this.totalRow = totalRow;
    }

    @Override
    public boolean isFirst() {
        return this.pageNumber == 1;
    }

    @Override
    public boolean isLast() {
        return this.pageNumber >= this.totalPage;
    }
}
