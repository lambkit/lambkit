package com.lambkit.db;

import com.lambkit.core.IAttr;

import java.util.Collection;
import java.util.Set;

/**
 * @author yangyong(孤竹行)
 */
public interface IRowData<M> extends IAttr<M> {

    /**
     * 表名
     * @return
     */
    String tableName();

    M setTableName(String tableName);

    String primaryKey();

    M setPrimaryKey(String primaryKey);

    /**
     * 字段名列表，用于限制加入的字段的值
     * @return
     */
    Set<String> columnNames();

    M setColumnNames(Collection<String> columnNames);

    M addColumnNames(String... columnName);

    void remove(String columnName);

    default void setColumns(IRowData data) {
        this.put(data.toMap());
        this.setPrimaryKey(data.primaryKey());
        this.setTableName(data.tableName());
    }
}
