/**
 * Copyright (c) 2015-2017, Henry Yang 杨勇 (gismail@foxmail.com).
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.lambkit.db.mgr.util;

import java.sql.Connection;
import java.util.List;
import java.util.Map;
import java.util.Set;

import cn.hutool.core.map.MapUtil;
import com.lambkit.db.*;
import com.lambkit.db.dialect.IManagerDialect;
import com.lambkit.db.mgr.IField;
import com.lambkit.db.mgr.MgrdbService;

/**
 * 数据库操作
 * @author lamb
 *
 */
public class MgrDb {
	
	private static MgrDbPro pro = null;
	private static final Map<String, MgrDbPro> map = MapUtil.newConcurrentHashMap();
	
	
//	public static MgrDbPro use() {
//		if(pro==null) {
//			pro = new MgrDbPro();
//		}
//		return pro;
//	}
	
//	public static MgrDbPro use(MgrdbService mgrdbService, String configName) {
//		MgrDbPro result = map.get(configName);
//		if(result==null) {
//			result = new MgrDbPro(configName);
//			map.put(configName, result);
//		}
//		return result;
//	}

	public static MgrDbPro use(MgrdbService mgrdbService, String configName, String dbName) {
		MgrDbPro result = map.get(configName);
		if(result==null) {
			result = new MgrDbPro(mgrdbService, configName, dbName);
			map.put(configName, result);
		}
		return result;
	}
	
	public IManagerDialect getDialect() {
		return pro.getDialect();
	}
	
	//------------------------------------
	// 数据库信息
	//------------------------------------	
	public List<RowData> version() {
		return pro.version();
	}
	
	public List<RowData> find(String sql) {
		return pro.find(sql);
	}

	public PageData<RowData> paginate(int pageNumber, int pageSize, Sql sql) {
		return pro.paginate(pageNumber, pageSize, sql);
	}
	
	public int update(String sql) {
		return pro.update(sql);
	}
	
	public int delete(String sql) {
		return pro.delete(sql);
	}
	//------------------------------------
	// 查询数据库
	//------------------------------------	
	/**
	 * 查询所有表名
	 * @return
	 */
	public List<RowData> getTableList(String dbname) {
		return pro.getTableList(dbname);
	}
	
	public List<RowData> getTableListRecord(String dbname) {
		return pro.getTableListRecord(dbname);
	}
	
	public Map<String, RowData> getTableListMap(String dbname) {
		return pro.getTableListMap(dbname);
	}
	
	public List<RowData> getTableList() {
		return pro.getTableList();
	}
	
	/**
	 * 查询表中所有列名
	 * @param tableName
	 * @return
	 */
	public List<RowData> getColumnList(String dbname, String tableName) {
		return pro.getColumnList(dbname, tableName);
	}
	
	public List<RowData> getColumnListRecord(String dbname, String tableName) {
		return pro.getColumnListRecord(dbname, tableName);
	}
	
	public Set<String> getColumnNameSet(String dbname, String tableName) {
		return pro.getColumnNameSet(dbname, tableName);
	}
	
	public List<RowData> getColumnList(String tableName) {
		return pro.getColumnList(tableName);
	}
	
	public RowData getColumn(String dbname, String tableName, String colname) {
		return pro.getColumn(dbname, tableName, colname);
	}
	
	public RowData getColumn(String tableName, String colname) {
		return pro.getColumn(tableName, colname);
	}
	
	public String getPrimaryKey(String schema, String tableName) {
		return pro.getPrimaryKey(schema, tableName);
	}
	
	//------------------------------------
	// 数据库操作
	//------------------------------------	
	/**
	 * 执行SQL语句
	 * @param conn
	 * @param sql
	 * @return
	 */
	public boolean execute(Connection conn, String sql) {
		return pro.execute(conn, sql);
	}
	
	public boolean execute(String sql) {
		return pro.execute(sql);
	}
	
	/**
	 * 执行SQL语句,
	 * @param tableName
	 * @param sql
	 * @return
	 */
	public boolean executeHasTable(String tableName, String sql, boolean flag) {
		return pro.executeHasTable(tableName, sql, flag);
	}
	
	/**
	 * 是否存在数据表
	 * @param tableName
	 * @return
	 */
	public boolean hasTable(String tableName) {
		return pro.hasTable(tableName);
	}
	
	public boolean hasTable(String dbname, String tableName) {
		return pro.hasTable(dbname, tableName);
	}
	
	public boolean hasTable(Connection conn, String tableName) {
		return pro.hasTable(conn, tableName);
	}
	
	public boolean hasTable(Connection conn, String dbname, String tableName) {
		return pro.hasTable(conn, dbname, tableName);
	}

	/**
	 * 删除数据表
	 * @param tableName
	 * @return
	 */
	public boolean dropTable(String tableName) {
		return pro.dropTable(tableName);
	}
	
	//------------------------------------
	// 数据库字段操作
	//------------------------------------
	
	/**
	 * 添加字段
	 * ALTER TABLE 表名称 ADD id int unsigned not Null auto_increment primary key
	 * 
	 * @param tableName
	 * @param fldinfo
	 */
	public boolean addField(String tableName, String fldinfo, String after) {
		return pro.addField(tableName, fldinfo, after);
	}

	/**
	 * 修改某个表的字段名称及指定为空或非空 alter table 表名称 change 字段原名称 字段新名称 字段类型 [是否允许非空];
	 * 修改某个表的字段类型及指定为空或非空 alter table 表名称 change 字段名称 字段名称 字段类型 [是否允许非空];
	 * @param tableName : 表名称
	 * @param fldinfo : 字段名称 字段类型 [是否允许非空]
	 * @param after
	 * @return
	 */
	public boolean changeField(String tableName, String fldinfo, String after) {
		return pro.changeField(tableName, fldinfo, after);
	}

	/**
	 * 修改某个表的字段类型及指定为空或非空 alter table 表名称 modify 字段名称 字段类型 [是否允许非空];
	 * 
	 * @param tableName
	 *            : 表名称
	 * @param fldinfo
	 *            : 字段名称 字段类型 [是否允许非空]
	 */
	public boolean modifyFieldType(String tableName, String fldinfo, String after) {
		return pro.modifyFieldType(tableName, fldinfo, after);
	}

	/**
	 * 删除某一字段，可用命令：ALTER TABLE mytable DROP 字段名;
	 * 
	 * @param tableName
	 *            : mytable
	 * @param fldname
	 *            : 字段名
	 */
	public boolean dropField(String tableName, String fldname) {
		return pro.dropField(tableName, fldname);
	}
	
	//------------------------------------
	// Mgrdb数据库操作
	//------------------------------------	
	
	/**
	 * 删除数据表
	 * 
	 * @param tbid
	 * @param tableName
	 *            默认为NULL，通过tbid获得
	 * @return
	 */
	public boolean dropTableByMgrdb(Long tbid, String tableName) {
		return pro.dropTableByMgrdb(tbid, tableName);
	}

	/**
	 * 创建数据表
	 * 
	 * @param tbid
	 * @param tableName
	 * @return
	 */
	public boolean createTableByMgrdb(Object tbid, String tableName) {
		return pro.createTableByMgrdb(tbid, tableName);
	}
	
	/**
	 * 创建数据表
	 * 
	 * @param tbid
	 * @param tableName
	 * @return
	 */
	public boolean updateTableByMgrdb(Object tbid, String tableName, String sqlfrom, String type) {
		return pro.updateTableByMgrdb(tbid, tableName, sqlfrom, type);
	}
	
	//------------------------------------
	// Mgrdb数据库字段操作
	//------------------------------------	
		
	/**
	 * 删除字段
	 * @param tbid
	 * @param tableName
	 * @param fldname
	 * @return
	 */
	public boolean dropFieldByMgrdb(Long tbid, String tableName, String fldname) {
		return pro.dropFieldByMgrdb(tbid, tableName, fldname);
	}
	/**
	 * 删除字段
	 * @param tbid
	 * @param tableName
	 * @param fldid
	 * @return
	 */
	public boolean dropFieldByMgrdb(Long tbid, String tableName, Long fldid) {
		return pro.dropFieldByMgrdb(tbid, tableName, fldid);
	}
	
	/**
	 * 添加字段
	 * @param tbid
	 * @param tableName
	 * @param fldid
	 * @param after
	 * @return
	 */
	public boolean addFieldByMgrdb(Long tbid, String tableName, Long fldid, String after) {
		return pro.addFieldByMgrdb(tbid, tableName, fldid, after);
	}
	
	/**
	 * 添加字段
	 * @param tbid
	 * @param tableName
	 * @param resfcv
	 * @param after
	 * @return
	 */
	public boolean addFieldByMgrdb(Long tbid, String tableName, IField resfcv, String after) {
		return pro.addFieldByMgrdb(tbid, tableName, resfcv, after);
	}
	
	/**
	 * 修改字段
	 * @param tbid
	 * @param tableName
	 * @param fldid
	 * @param after
	 * @return
	 */
	public boolean changeFieldByMgrdb(Long tbid, String tableName, String resname, Long fldid, String after) {
		return pro.changeFieldByMgrdb(tbid, tableName, resname, fldid, after);
	}
	/**
	 * 修改字段
	 * @param tbid
	 * @param tableName
	 * @param resname
	 * @param resfcv
	 * @param after
	 * @return
	 */
	public boolean changeFieldByMgrdb(Long tbid, String tableName, String resname, IField resfcv, String after) {
		return pro.changeFieldByMgrdb(tbid, tableName, resname, resfcv, after);
	}
	
	/**
	 * 修改字段类型
	 * @param tbid
	 * @param tableName
	 * @param fldid
	 * @param after
	 * @return
	 */
	public boolean modifyFieldTypeByMgrdb(Long tbid, String tableName, Long fldid, String after) {
		return pro.modifyFieldTypeByMgrdb(tbid, tableName, fldid, after);
	}
	
	////////////////////////////////////////////////////////////////////
	/**
	 * 获取表格名称
	 * @param tbid
	 * @param tableName
	 * @return
	 */
	public String getTbNameByMgrdb(Object tbid, String tableName) {
		return pro.getTbNameByMgrdb(tbid, tableName);
	}
	
	/**
	 * 获取没个字段的sql
	 * @param fcv
	 * @return
	 */
	public String getFieldSQL(IField fcv) {
		return pro.getFieldSQL(fcv);
	}
	
}
