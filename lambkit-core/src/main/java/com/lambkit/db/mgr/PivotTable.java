/**
 * Copyright (c) 2015-2017, Henry Yang 杨勇 (gismail@foxmail.com).
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.lambkit.db.mgr;

import com.lambkit.core.Attr;

import java.util.ArrayList;
import java.util.List;

public class PivotTable {
	/**
	 * 行维度, name, label, category
	 */
	private List<IField> rows;
	/**
	 * 列维度, name, label, category
	 */
	private List<IField> columns;
	/**
	 * 度量, name, label, data
	 */
	private List<IField> measures;

	private List<Attr> rowHead;//
	private List<Attr> colHead;//
	private int rowlen;//
	private List<Attr> data;
	private List<Attr> rowCategory;
	private List<Attr> colCategory;

	public PivotTable() {
		rows = new ArrayList<IField>();
		columns = new ArrayList<IField>();
		measures = new ArrayList<IField>();
		rowHead = new ArrayList<>();
		colHead = new ArrayList<>();
	}

	/**
	 * 添加行名称
	 */
	public void addRow(IField fld) {
		int collen = columns.size();
		Attr attr = new Attr();
		attr.set("property", fld.getName());
		attr.set("label", fld.getTitle());
		attr.set("rowspan", collen + 1);
		rowHead.add(attr);
		rows.add(fld);
	}
	/**
	 * 添加列名称
	 */
	public void addColumn(IField fld) {
		columns.add(fld);
	}
	/**
	 * 添加名称
	 */
	public void addMeasure(IField fld) {
		measures.add(fld);
	}

	public List<Attr> getData() {
		return data;
	}

	public List<IField> getMeasures() {
		return measures;
	}
	public void setMeasures(List<IField> measures) {
		this.measures = measures;
	}

	public List<IField> getRows() {
		return rows;
	}

	public void setRows(List<IField> rows) {
		this.rows = rows;
	}

	public List<IField> getColumns() {
		return columns;
	}

	public void setColumns(List<IField> columns) {
		this.columns = columns;
	}

	public void setData(List<Attr> data) {
		this.data = data;
	}

	public int getRowlen() {
		return rowlen;
	}

	public void setRowlen(int rowlen) {
		this.rowlen = rowlen;
	}

	public List<Attr> getColHead() {
		return colHead;
	}

	public void setColHead(List<Attr> colHead) {
		this.colHead = colHead;
	}

	public List<Attr> getRowHead() {
		return rowHead;
	}

	public void setRowHead(List<Attr> rowHead) {
		this.rowHead = rowHead;
	}

	public List<Attr> getRowCategory() {
		return rowCategory;
	}

	public void setRowCategory(List<Attr> rowCategory) {
		this.rowCategory = rowCategory;
	}

	public List<Attr> getColCategory() {
		return colCategory;
	}

	public void setColCategory(List<Attr> colCategory) {
		this.colCategory = colCategory;
	} 
}
