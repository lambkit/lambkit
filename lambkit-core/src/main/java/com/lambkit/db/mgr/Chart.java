/**
 * Copyright (c) 2015-2017, Henry Yang 杨勇 (gismail@foxmail.com).
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.lambkit.db.mgr;

import cn.hutool.core.util.StrUtil;
import com.lambkit.db.meta.TableMeta;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * 图表数据获取
 * @author yangyong
 */
public class Chart implements Serializable {
	private String classify;
	private String serias;
	private String seriasSQL;
	private String operation;
	private List<String> seriasNames;
	private String classifyName;
	private String dataSQL;
	private Object[] sqlParas;
//	private String fldname = "fldname";

	public Chart() {
		seriasNames = new ArrayList<String>();
		seriasSQL = "";
	}

	public static Chart by(MgrTable tbc, String cls, String serias, String yuns, String seriasPrefix) {
		ChartBuilder builder = new ChartBuilder();
		return builder.build(tbc, cls, serias, yuns, seriasPrefix);
	}

	public static Chart by(TableMeta meta, String cls, String serias, String yuns, String seriasPrefix, String title) {
		ChartBuilder builder = new ChartBuilder();
		return builder.build(meta, cls, serias, yuns, seriasPrefix, title);
	}

	public void addSerias(String serias_name) {
		seriasNames.add(serias_name);
	}

	public String getSelectSQL() {
		String selectSQL = "";
		if(StrUtil.isNotBlank(classify)) {
			selectSQL += " " + classify + " as cls ";
		}
		if(StrUtil.isNotBlank(seriasSQL)) {
			selectSQL += "," + seriasSQL;
		}
		selectSQL += " ";
		return selectSQL;
	}

	public String getClassify() {
		return classify;
	}
	public void setClassify(String classify) {
		this.classify = classify;
	}
	public String getSeriasSQL() {
		return seriasSQL;
	}
	public void setSeriasSQL(String seriasSQL) {
		this.seriasSQL = seriasSQL;
	}
	public String getOperation() {
		return operation;
	}
	public void setOperation(String operation) {
		this.operation = operation;
	}
	public List<String> getSeriasNames() {
		return seriasNames;
	}
	public void setSeriasNames(List<String> seriasNames) {
		this.seriasNames = seriasNames;
	}

	public String getClassifyName() {
		return classifyName;
	}

	public void setClassifyName(String classifyName) {
		this.classifyName = classifyName;
	}

	public String getSerias() {
		return serias;
	}

	public void setSerias(String serias) {
		this.serias = serias;
	}

	public String getDataSQL() {
		return dataSQL;
	}

	public void setDataSQL(String dataSQL) {
		this.dataSQL = dataSQL;
	}

	public Object[] getSqlParas() {
		return sqlParas;
	}

	public void setSqlParas(Object[] sqlParas) {
		this.sqlParas = sqlParas;
	}

//	public String getFldname() {
//		return fldname;
//	}
//
//	public void setFldname(String fldname) {
//		this.fldname = fldname;
//	}
}