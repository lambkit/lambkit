package com.lambkit.db;

import com.lambkit.db.dialect.IDialect;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.SQLException;

/**
 * @author yangyong(孤竹行)
 */
public interface IDb extends IDbOpt<RowData, PageData<RowData>> {
    IDialect getDialect();

    void setDialect(IDialect dialect);

    Connection getConnection() throws SQLException;

    void closeConnection(Connection connection);

    DataSource getDataSource();

    RowData newRowData();

    PageData<RowData> newPageData();

    <T extends RowModel<T>> IRowDao<T> dao(Class<T> clazz);
    <T extends IRowData, P extends IPageData<T>> IDbOpt by(Class<T> rowClazz, Class<P> pageClazz);
}
