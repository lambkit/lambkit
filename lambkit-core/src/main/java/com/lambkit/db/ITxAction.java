package com.lambkit.db;

import java.sql.SQLException;

/**
 * @author yangyong(孤竹行)
 */
public interface ITxAction<T> {
    boolean execute(T dbOpt) throws SQLException;
}
