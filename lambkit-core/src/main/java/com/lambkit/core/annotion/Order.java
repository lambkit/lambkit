package com.lambkit.core.annotion;

import java.lang.annotation.*;

/**
 * 插件等的排序设置
 * @author yangyong(孤竹行)
 */
@Documented
@Target({ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
@Inherited
public @interface Order {
    /**
     * 排序
     * @return
     */
    int index() default 9999;
}
