package com.lambkit.core.script;

import java.util.Map;

/**
 * @author yangyong(孤竹行)
 */
public interface LambkitScriptEngine extends IScriptEngine {

    /**
     * 获取脚本
     * @param scriptId
     * @return
     */
    String getScript(String scriptId);

    /**
     * 执行已有脚本
     * @param scriptId
     * @param context
     * @return
     */
    Object execute(String scriptId, Map<String, Object> context) throws Exception;

}
