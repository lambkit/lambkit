package com.lambkit.core.script;

import java.util.Map;

/**
 * @author yangyong(孤竹行)
 */
public interface IScriptEngine {
    /**
     * 执行临时脚本
     * @param scriptText
     * @param context
     * @return
     */
    Object invoke(String scriptText, Map<String, Object> context) throws Exception;

    /**
     * 执行临时脚本
     * @param scriptText
     * @param functionName
     * @param context
     * @return
     */
    Object invokeMethod(String scriptText, String functionName, Map<String, Object> context) throws Exception;
}
