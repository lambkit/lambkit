package com.lambkit.core;

public interface LambkitServerLifecycle extends Lifecycle {
    void init();
    void start();
    void stop();
}
