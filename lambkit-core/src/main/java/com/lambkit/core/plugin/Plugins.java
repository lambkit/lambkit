package com.lambkit.core.plugin;

import java.util.ArrayList;
import java.util.List;

/**
 * @author yangyong(孤竹行)
 */
public class Plugins {
    private final List<IPlugin> pluginList = new ArrayList<>();

    public void add(IPlugin plugin) {
        if(!pluginList.contains(plugin)) {
            pluginList.add(plugin);
        }
    }

    public List<IPlugin> getPluginList() {
        return pluginList;
    }
}
