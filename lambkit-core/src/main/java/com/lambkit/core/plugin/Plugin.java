package com.lambkit.core.plugin;

import com.lambkit.core.AppContext;
import com.lambkit.core.LifecycleState;

/**
 * @author yangyong(孤竹行)
 */
public abstract class Plugin implements IPlugin {
    private AppContext appContext;
    private LifecycleState currentState = LifecycleState.NEW;
    @Override
    public LifecycleState getCurrentState() {
        return currentState;
    }

    public void setCurrentState(LifecycleState currentState) {
        this.currentState = currentState;
    }

    @Override
    public void setContext(AppContext appContext) {
        this.appContext = appContext;
    }

    public AppContext getAppContext() {
        return appContext;
    }
}
