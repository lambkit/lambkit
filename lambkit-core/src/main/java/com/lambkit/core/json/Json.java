package com.lambkit.core.json;

import cn.hutool.core.map.MapUtil;
import cn.hutool.core.util.StrUtil;

import java.util.Map;

/**
 * @author yangyong(孤竹行)
 */
public class Json {
    private static Map<String, IJson> jsonMap = MapUtil.newConcurrentHashMap();
    private static String defaultName;
    public static IJson use() {
        if(StrUtil.isBlank(defaultName)) {
            defaultName = HtJson.class.getName();
            jsonMap.put(defaultName, new HtJson());
        }
        return jsonMap.get(defaultName);
    }

    public static IJson use(String name) {
        return jsonMap.get(name);
    }

    public static void register(String name, IJson json) {
        jsonMap.put(name, json);
        if(StrUtil.isBlank(defaultName)) {
            defaultName = name;
        }
    }

    public static void registerDefault(IJson json) {
        if(StrUtil.isBlank(defaultName)) {
            defaultName = json.getClass().getSimpleName();
        }
        register(defaultName, json);
    }
}
