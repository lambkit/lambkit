package com.lambkit.core.json;

import cn.hutool.json.JSONUtil;

/**
 * @author yangyong(孤竹行)
 */
public class HtJson implements IJson {
    @Override
    public String toJson(Object object) {
        return JSONUtil.toJsonStr(object);
    }

    @Override
    public <T> T parse(String jsonString, Class<T> type) {
        return JSONUtil.toBean(jsonString, type);
    }
}
