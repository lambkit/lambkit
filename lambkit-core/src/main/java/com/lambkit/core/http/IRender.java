package com.lambkit.core.http;

/**
 * @author yangyong(孤竹行)
 */
public interface IRender {
    void render(IHttpContext context);
}
