package com.lambkit.core.http;

import com.lambkit.core.Lambkit;

/**
 * @author yangyong(孤竹行)
 */
public abstract class Render implements IRender {
    public String getEncoding() {
        return Lambkit.config("lambkit.http.encoding", "UTF-8");
    }
}
