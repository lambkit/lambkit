package com.lambkit.core.http;

/**
 * @author yangyong(孤竹行)
 */
public interface ICookie {
    String getValue();

    String getName();

    void setMaxAge(int maxAgeInSeconds);

    void setPath(String path);

    void setDomain(String domain);

    void setHttpOnly(Boolean isHttpOnly);

    void setSameSite(String sameSite);
}
