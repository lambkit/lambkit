package com.lambkit.core.http;

import com.lambkit.core.pipeline.BaseValve;

/**
 * @author yangyong(孤竹行)
 */
public abstract class HttpValve extends BaseValve<HttpValve, HttpPipLineContext> {
}
