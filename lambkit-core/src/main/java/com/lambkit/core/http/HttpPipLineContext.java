package com.lambkit.core.http;

import cn.hutool.core.util.IdUtil;
import com.lambkit.core.BaseContext;
import com.lambkit.core.LambkitResult;
import com.lambkit.core.LifecycleState;

/**
 * @author yangyong(孤竹行)
 */
public class HttpPipLineContext extends BaseContext {
    private IHttpContext httpContext;
    private String uri;
    public HttpPipLineContext(String uri, IHttpContext httpContext) {
        this.setId(IdUtil.simpleUUID());
        this.setCurrentState(LifecycleState.NEW);
        this.setResult(new LambkitResult());
        this.setStartTime(System.currentTimeMillis());
        this.httpContext = httpContext;
        this.uri = uri;
    }

    public boolean isReady() {
        if(httpContext == null) {
            return false;
        }
        if(uri == null) {
            return false;
        }
        return true;
    }

    public void percent(int percent, String msg) {
        getResult().percent(percent).message(msg);
        if(percent == 100) {
            getResult().code(1).setSuccess(true);
        }
    }

    public void render(IRender render) {
        httpContext.render(render);
    }

    public void renderJson(LambkitResult result) {
        if(result != null) {
            setResult(result);
        }
        httpContext.renderJson(getResult());
    }

    public void renderHtml(String html) {
        httpContext.renderHtml(html);
    }

    public void renderError(int status, String msg) {
        resultError(status, msg);
        httpContext.renderError(status, msg);
    }

    public IHttpContext getHttpContext() {
        return httpContext;
    }

    public void setHttpContext(IHttpContext httpContext) {
        this.httpContext = httpContext;
    }

    public void setUri(String uri) {
        this.uri = uri;
    }

    public String getUri() {
        return uri;
    }
}