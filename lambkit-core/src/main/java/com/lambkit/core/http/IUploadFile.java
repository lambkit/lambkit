package com.lambkit.core.http;

import java.io.File;

/**
 * @author yangyong(孤竹行)
 */
public interface IUploadFile {
    String getFileName();

    File getFile();
}
