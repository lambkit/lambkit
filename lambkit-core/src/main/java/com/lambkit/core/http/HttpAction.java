package com.lambkit.core.http;

import java.io.Serializable;
import java.lang.reflect.Method;

/**
 * @author yangyong(孤竹行)
 */
public class HttpAction implements Serializable {
    private String target;
    private Method method;
    private Class controller;

    public HttpAction() {

    }

    public HttpAction(String target, Method method, Class controller) {
        this.target = target;
        this.method = method;
        this.controller = controller;
    }

    public String getTarget() {
        return target;
    }

    public void setTarget(String target) {
        this.target = target;
    }

    public Method getMethod() {
        return method;
    }

    public void setMethod(Method method) {
        this.method = method;
    }

    public Class getController() {
        return controller;
    }

    public void setController(Class controller) {
        this.controller = controller;
    }
}
