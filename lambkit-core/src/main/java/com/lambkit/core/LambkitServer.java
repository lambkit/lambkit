package com.lambkit.core;

/**
 * 服务器
 */
public interface LambkitServer {
    void start();
    void stop();
}
