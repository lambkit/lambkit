package com.lambkit.core.limiter;

/**
 * 降级策略
 * <li></li>
 * @author yangyong
 *
 */
public enum LimitFallbackStrategy {
	/**
     * 快速失败
     */
    FAIL_FAST,
    /**
     * 回退
     */
    FALLBACK;
}
