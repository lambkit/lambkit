package com.lambkit.core.limiter;

public enum LimiterType {
	/**
	 * 令牌桶，通过 guava 的 RateLimiter 来实现 时间有关，每秒钟允许有多少个请求
	 */
	// TOKEN_BUCKET = "tb";
	/**
	 * 滑动时间窗口算法
	 */
	// SEMAPHORE = "sem";

	/**
	 * 分布式限流，令牌痛算法
	 */
	// REDIS_TOKEN_BUCKET = "rtb";
	/**
	 * 分布式限流 滑动窗口算法
	 */
	// REDIS_SLIDING = "rs";
	/**
	 * 分布式限流 固定窗口算法
	 */
	// REDIS_FIDED = "rf";

	/**
	 * 令牌桶，通过 guava 的 RateLimiter 来实现 时间有关，每秒钟允许有多少个请求
	 */
	TOKEN_BUCKET,
	/**
	 * 并发量，通过 Semaphore 来实现
     * 和并发有关，和请求时间无关
	 */
	SEMAPHORE,
	/**
	 * 滑动时间窗口算法
	 */
	SLIDING,
	/**
	 * 分布式限流，令牌痛算法
	 */
	REDIS_TOKEN_BUCKET,
	/**
	 * 分布式限流 滑动窗口算法
	 */
	REDIS_SLIDING,
	/**
	 * 分布式限流 固定窗口算法
	 */
	REDIS_FIXED;
	// 基于 BBR 算法的自适应限流
}
