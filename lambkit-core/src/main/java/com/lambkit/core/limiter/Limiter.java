package com.lambkit.core.limiter;

import java.util.concurrent.TimeUnit;

public interface Limiter {

	/** 默认时间单位:秒 */
    TimeUnit DEFAULT_TIME_UNIT = TimeUnit.SECONDS;
    /** 默认限流时间范围:1 */
    int DEFAULT_PERIOD = 1;
    /** 默认限流数量: 10 */
    int DEFAULT_RATE = 10;

    default boolean tryAcquire(String resource) {
    	return tryAcquire(resource, DEFAULT_RATE);
    }
    
    default boolean tryAcquire(String resource, int rate) {
    	return tryAcquire(resource, rate, DEFAULT_PERIOD);
    }
    
    default boolean tryAcquire(String resource ,int rate, int period) {
    	return tryAcquire(resource, rate, period, DEFAULT_TIME_UNIT);
    }
    /**
	 * 限流验证
	 * @param resource	限流标识
	 * @param rate		限流次数
	 * @param period	限制时长
	 * @param timeUnit	时间单位
	 * @return
	 */
	boolean tryAcquire(String resource ,int rate, int period, TimeUnit timeUnit);
	
	/**
	 * 限流处理完成 
	 */
	void release(String resource);
}
