package com.lambkit.core.limiter;

import com.lambkit.core.http.IHttpContext;

public interface LimitFallback {

	void handle(String target, IHttpContext httpContext, LimitFallbackStrategy fallbackStrategy);
}
