/**
 * Copyright (c) 2015-2017, Henry Yang 杨勇 (gismail@foxmail.com).
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.lambkit.core;

import java.io.Serializable;
import java.util.Map;

/**
 * 统一返回结果类
 */
public class LambkitResult implements Serializable {

    /**
	 * 
	 */
	private static final long serialVersionUID = -6928386812907728048L;
	
	// 标识码
    private String lambkit;

	// 状态码：1成功，其他为失败
    private int code;

    private boolean success;

    // 成功为success，其他为失败原因
    private String message;

    // 数据结果集
    private Object data;

    private Integer percent; //进度

    private Attr attr; //属性

    public LambkitResult() {
    }

    public static LambkitResult success() {
        LambkitResult result = new LambkitResult();
        result.setSuccess(true);
        result.setCode(1);
        return result;
    }

    public static LambkitResult fail() {
        LambkitResult result = new LambkitResult();
        result.setSuccess(false);
        result.setCode(0);
        return result;
    }

    public static LambkitResult error(int code) {
        LambkitResult result = new LambkitResult();
        result.setSuccess(false);
        result.setCode(code);
        return result;
    }


    public LambkitResult code(int code) {
        this.code = code;
        return this;
    }

	public LambkitResult message(String message) {
		this.message = message;
		return this;
	}

	public LambkitResult data(Object data) {
		this.data = data;
		return this;
	}

    public LambkitResult percent(int percent) {
        this.percent = percent;
        return this;
    }

    public LambkitResult attr(Attr attr) {
        this.attr = attr;
        return this;
    }

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public String getLambkit() {
    	return lambkit;
    }
    
    public void setLambkit(String lambkit) {
    	this.lambkit = lambkit;
    }
    
    public int getCode() {
        return code;
    }

    public LambkitResult setCode(int code) {
        this.code = code;
        return this;
    }

    public String getMessage() {
        return message;
    }

    public LambkitResult setMessage(String message) {
        this.message = message;
        return this;
    }

    public Object getData() {
        return data;
    }

    public LambkitResult setData(Object data) {
        this.data = data;
        return this;
    }

    public Integer getPercent() {
        return percent;
    }

    public void setPercent(Integer percent) {
        this.percent = percent;
    }

    public Map<String, Object> getAttr() {
        return attr!=null ? attr.toMap() : null;
    }

    public Attr attribute() {
        return attr;
    }

    public void setAttr(Attr attr) {
        this.attr = attr;
    }
}
