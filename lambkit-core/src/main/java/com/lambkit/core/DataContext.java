package com.lambkit.core;

import com.lambkit.core.BaseContext;

/**
 * 处理器上下文
 * @param <T>
 */
public class DataContext<T> extends BaseContext {

    private T data;

    public T getData() {
        return data;
    }

    public void setData(T data) {
        this.data = data;
    }
}
