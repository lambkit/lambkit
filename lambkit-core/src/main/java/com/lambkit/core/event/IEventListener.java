package com.lambkit.core.event;

/**
 * 时间监听
 */
public interface IEventListener {
	public void onEvent(IEvent event);
}
