package com.lambkit.core.event;

/**
 * @author yangyong(孤竹行)
 */
public interface IEventDispatcher {

    void register(Class<? extends IEventListener> listener);

    void unregister(Class<? extends IEventListener> listener);

    void dispatch(IEvent event);
}
