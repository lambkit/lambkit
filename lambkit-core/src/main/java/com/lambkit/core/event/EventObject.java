package com.lambkit.core.event;

import com.lambkit.core.Attr;

/**
 * 事件对象
 * @author yangyong(孤竹行)
 */
public class EventObject implements java.io.Serializable {
    private static final long serialVersionUID = 1L;
    private String code;//事件编码ID
    private String name;//事件名称、事项、有字典表的，也能识别类型的
    private String title;//事件标题，可见可读的
    private String target;//事件目标
    private long time;//事件时间
    private Double latitude;//纬度
    private Double longitude;//经度
    private String source;//事件来源
    private Attr attr;//事件属性

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getTarget() {
        return target;
    }

    public void setTarget(String target) {
        this.target = target;
    }

    public long getTime() {
        return time;
    }

    public void setTime(long time) {
        this.time = time;
    }

    public Double getLatitude() {
        return latitude;
    }

    public void setLatitude(Double latitude) {
        this.latitude = latitude;
    }

    public Double getLongitude() {
        return longitude;
    }

    public void setLongitude(Double longitude) {
        this.longitude = longitude;
    }

    public String getSource() {
        return source;
    }

    public void setSource(String source) {
        this.source = source;
    }

    public Attr getAttr() {
        return attr;
    }

    public void setAttr(Attr attr) {
        this.attr = attr;
    }
}
