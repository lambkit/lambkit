package com.lambkit.core;

import com.lambkit.core.processor.Aware;

/**
 * @author yangyong(孤竹行)
 */
public interface AppContextAware extends Aware {
    void setAppContext(AppContext appContext);
}
