package com.lambkit.core.flow;

import cn.hutool.core.util.StrUtil;
import com.lambkit.core.Attr;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * @author yangyong(孤竹行)
 */
public class FlowNodeObject implements Serializable {
    private static final long serialVersionUID = 1L;
    private String code;
    private String title;//节点标题
    private String type = "normal"; //节点类型
    private String name;//节点类 或 节点名称
    private String status = "init"; // 节点状态, init/start/stop
    private List<String> next; //后置节点code

    private Attr attr; //节点属性

    public FlowNodeObject() {
        next = new ArrayList<>();
    }

    public List<FlowNodeObject> getByStatus(String status, FlowObject flowObject)  {
        List<FlowNodeObject> nodes = new ArrayList<>();
        if(getStatus().equals(status)) {
            nodes.add(this);
        } else {
            if(getNext()!=null) {
                for(String nextCode : getNext()) {
                    FlowNodeObject node = flowObject.getNodeByCode(nextCode);
                    nodes.addAll(node.getByStatus(status, flowObject));
                }
            }
        }
        return nodes;
    }

    /////////////////////////////////////////////////
    //get and set

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public List<String> getNext() {
        return next;
    }

    public void setNext(List<String> next) {
        this.next = next;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }



    public Attr getAttr() {
        return attr;
    }

    public void setAttr(Attr attr) {
        this.attr = attr;
    }
}
