package com.lambkit.core.flow.node;

import com.lambkit.core.LifecycleState;
import com.lambkit.core.flow.IFlowNode;
import com.lambkit.core.flow.IFlowNodePreRule;

import java.util.List;

/**
 * @author yangyong(孤竹行)
 */
public class PreRuleForParallel implements IFlowNodePreRule {
    @Override
    public boolean check(IFlowNode flowNode) {
        List<IFlowNode> children = flowNode.getPreNode();
        if(children==null || children.size()==0) {
            return true;
        }
        for(IFlowNode child : children) {
            //Printer.print(this, "flow", "child.getId(): " + child.getId());
            //Printer.print(this, "flow", "child.getCurrentState(): " + child.getCurrentState().name());
            if(child.getCurrentState() != LifecycleState.STOPPED) {
                return false;
            }
        }
        return true;
    }
}
