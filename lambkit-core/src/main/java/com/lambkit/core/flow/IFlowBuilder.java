package com.lambkit.core.flow;

/**
 * 流程存储接口
 * @author yangyong(孤竹行)
 */
public interface IFlowBuilder {
    FlowObject getFlow();
    void saveFlow();
}