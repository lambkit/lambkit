package com.lambkit.core.flow;

import com.lambkit.core.Lifecycle;

/**
 * @author yangyong(孤竹行)
 */
public interface IFlowInstance extends Lifecycle {
    void add(IFlowNode flowNode);

    IFlowContext getFlowContext();
}
