package com.lambkit.core.flow.node;

import com.lambkit.core.LifecycleException;
import com.lambkit.core.flow.FlowInstance;
import com.lambkit.core.flow.BaseFlowNode;
import com.lambkit.core.flow.FlowNodeObject;
import com.lambkit.core.flow.IFlowContext;
import com.lambkit.core.flow.annotion.FlowNode;
import com.lambkit.util.Printer;

/**
 * @author yangyong(孤竹行)
 */
@FlowNode(name = "start", desc = "开始节点")
public class StartFlowNode extends BaseFlowNode {
    public StartFlowNode(FlowNodeObject flowNodeObject, FlowInstance flowInstance) {
        super(flowNodeObject, flowInstance);
        this.setPreRule(null);
        this.setNextRule(null);
    }

    @Override
    public int execute(IFlowContext flowContext) {
        Printer.print(this, "flow", "execute start node:" + getId());
        try {
            stop(); // 执行完毕后停止
        } catch (LifecycleException e) {
            throw new RuntimeException(e);
        }
        return 0;
    }
}
