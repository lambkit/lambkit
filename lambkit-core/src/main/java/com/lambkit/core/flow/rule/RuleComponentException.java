package com.lambkit.core.flow.rule;

public class RuleComponentException extends RuntimeException {

    private Integer errorCode;

    public RuleComponentException() {
        super();
    }

    public RuleComponentException(String message) {
        super(message);
    }

    public RuleComponentException(int errorCode, String message) {
        super(message);
        this.errorCode = errorCode;
    }

    public RuleComponentException(String message, Throwable cause) {
        super(message, cause);
    }

    public RuleComponentException(Throwable cause) {
        super(cause);
    }

    protected RuleComponentException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
