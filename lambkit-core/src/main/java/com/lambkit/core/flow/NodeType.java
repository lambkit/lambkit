package com.lambkit.core.flow;

public enum NodeType {
    START, TASK, GATEWAY, END
}
