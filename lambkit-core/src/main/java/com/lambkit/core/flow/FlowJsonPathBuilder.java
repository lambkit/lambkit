package com.lambkit.core.flow;

import cn.hutool.core.io.FileUtil;
import cn.hutool.core.io.resource.ResourceUtil;
import cn.hutool.core.util.IdUtil;
import cn.hutool.core.util.StrUtil;
import cn.hutool.json.JSONObject;
import cn.hutool.json.JSONUtil;

import java.io.File;
import java.nio.charset.Charset;

/**
 * 流程目录管理
 * @author yangyong(孤竹行)
 */
public class FlowJsonPathBuilder implements IFlowBuilder {

    private FlowObject flowObject;
    private String jsonPath;

    public FlowJsonPathBuilder(String jsonPath, String flowId) {
        this.jsonPath = jsonPath;
        readFlowObjectFromFile(flowId);
    }

    public FlowJsonPathBuilder(String jsonPath, FlowObject flowObject) {
        this.jsonPath = jsonPath;
        this.flowObject = flowObject;
    }

    private void readFlowObjectFromFile(String flowId) {
        String json = ResourceUtil.readUtf8Str(jsonPath + File.separator + flowId + ".json");
        readFlowObjectFromJson(json);
    }

    private void readFlowObjectFromJson(String json) {
        if(StrUtil.isNotBlank(json)) {
            JSONObject jsonFlow = JSONUtil.parseObj(json);
            this.flowObject = jsonFlow.toBean(FlowObject.class);
        } else {
            this.flowObject = new FlowObject();
            this.flowObject.setCode(IdUtil.simpleUUID());
        }
    }

    @Override
    public FlowObject getFlow() {
        return flowObject;
    }

    @Override
    public void saveFlow() {
        if (flowObject != null) {
            String flowId = flowObject.getCode();
            File file = new File(jsonPath + File.separator + flowId + ".json");
            String json = JSONUtil.toJsonPrettyStr(flowObject);
            FileUtil.writeString(json, file, Charset.forName("UTF-8"));
        }
    }
}
