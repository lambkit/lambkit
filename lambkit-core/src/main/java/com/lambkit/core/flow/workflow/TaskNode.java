package com.lambkit.core.flow.workflow;

import com.lambkit.core.flow.NodeType;

/**
 * 任务节点实现
 */
public class TaskNode extends WorkFlowNode {
    private String assignee; // 任务处理人
    public TaskNode(String id, String name) {
        super(id, name, NodeType.TASK);
    }
}
