package com.lambkit.core.flow;

import com.lambkit.core.Lifecycle;

import java.util.List;

/**
 * @author yangyong(孤竹行)
 */
public interface IFlowNode extends Lifecycle {
    /**
     * 流程节点名称
     * @return
     */
    String getId();

    IFlowNode addNextNode(IFlowNode node);

    /**
     * 执行, 0 无结果, 1 成功结果, -1 失败结果
     * @param flowContext
     */
    int execute(IFlowContext flowContext);

    /**
     * 前序规则
     * @return
     */
    IFlowNodePreRule getPreRule();

    /**
     * 后续规则
     * @return
     */
    IFlowNodeNextRule getNextRule();

    /**
     * 前序节点
     * @return
     */
    List<IFlowNode> getPreNode();

    /**
     * 后序节点
     * @return
     */
    List<IFlowNode> getNextNode();
}
