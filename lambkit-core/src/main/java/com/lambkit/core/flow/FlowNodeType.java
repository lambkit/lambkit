package com.lambkit.core.flow;

/**
 * @author yangyong(孤竹行)
 */
public enum FlowNodeType {
    NORMAL,//普通节点
    START,//开始节点
    END,//结束节点
    CHOICE,//选择节点
    MERGE,//合并节点
    MANUAL,//人工节点
    SUBFLOW,//子流程节点
    PARALLEL,//并行节点

}
