package com.lambkit.core.flow;

public enum NodeGatewayType {
    PARALLEL_GATEWAY,  // 并行网关（中国特色多部门协作）
    INCLUSIVE_GATEWAY, // 包容网关
    EXCLUSIVE_GATEWAY, // 排他网关（结合规则引擎）
}
