package com.lambkit.core.flow;

public enum NodeTaskType {
    USER_TASK,         // 人工审批节点（支持会签）
    SERVICE_TASK,      // 服务调用节点
}
