package com.lambkit.core.flow;

/**
 * 前序规则
 * @author yangyong(孤竹行)
 */
public interface IFlowNodePreRule extends IFlowNodeRule {
    boolean check(IFlowNode flowNode);
}
