package com.lambkit.core.flow;

import com.lambkit.core.IContext;

/**
 * 流程上下文
 * @author yangyong(孤竹行)
 */
public interface IFlowContext extends IContext {
    FlowInstanceObject getInstanceObject();
}
