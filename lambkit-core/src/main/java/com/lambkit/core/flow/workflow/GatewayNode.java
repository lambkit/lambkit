package com.lambkit.core.flow.workflow;

import com.lambkit.core.flow.NodeType;

/**
 * 网关节点实现（支持分支条件）
 */
public class GatewayNode extends WorkFlowNode {
    public GatewayNode(String id, String name) {
        super(id, name, NodeType.GATEWAY);
    }
}
