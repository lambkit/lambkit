package com.lambkit.core.flow;

/**
 * 流程节点执行步骤之间的切换规则，A->B->C，A->B的切换规则，B->C的切换规则
 * @author yangyong(孤竹行)
 */
public interface IFlowNodeRule {
}
