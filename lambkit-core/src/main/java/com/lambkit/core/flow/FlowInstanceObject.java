package com.lambkit.core.flow;

import cn.hutool.core.util.StrUtil;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * @author yangyong(孤竹行)
 */
public class FlowInstanceObject implements Serializable {
    private static final long serialVersionUID = 1L;
    private String code; // 实例id

    private String title; // 实例名称

    private String description; // 实例描述

    private String status = "init"; // 实例状态, init/start/stop

    private long createTime; // 创建时间

    private FlowObject flow; // 流程

    //////////////////////////////////////////////////
    public FlowNodeObject getNode(String flowNodeId) {
        if(flow == null || StrUtil.isBlank(flowNodeId)) {
            return null;
        }
        return flow.getNodeByCode(flowNodeId);
    }

    public List<FlowNodeObject> getCurrentNodes() {
        List<FlowNodeObject> nodes = new ArrayList<>();
        if(flow != null && StrUtil.isNotBlank(flow.getStartNode())) {
            FlowNodeObject startNode = flow.getNodeByCode(flow.getStartNode());
            if(startNode != null) {
                nodes.addAll(startNode.getByStatus("init", flow));
            }
        }
        return nodes;
    }

    /////////////////////////////////////////////////
    //get and set

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public long getCreateTime() {
        return createTime;
    }

    public void setCreateTime(long createTime) {
        this.createTime = createTime;
    }

    public FlowObject getFlow() {
        return flow;
    }

    public void setFlow(FlowObject flow) {
        this.flow = flow;
    }
}
