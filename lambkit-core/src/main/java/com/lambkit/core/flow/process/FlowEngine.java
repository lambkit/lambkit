package com.lambkit.core.flow.process;

import com.lambkit.core.flow.FlowContext;
import com.lambkit.core.flow.annotion.FlowNode;

/**
 * 工作流引擎
 * @author yangyong(孤竹行)
 */
public class FlowEngine {
    private FlowNode startNode;
    private FlowContext context;

    public FlowEngine(FlowNode startNode) {
        this.startNode = startNode;
    }

    public void execute() {
//        FlowNode currentNode = startNode;
//        while(currentNode != null) {
//            currentNode.getTask().execute(context);
//            currentNode = currentNode.getNextNode(context);
//        }
    }
}
