package com.lambkit.core.pipeline;

public interface IPipeLineFactory {

    <T extends IValve, C extends IPipLineContext> IPipeLine<T, C> createPipline();

    IPipLineContext createValveContext();

    PipeLineService getPipeLineService();
}
