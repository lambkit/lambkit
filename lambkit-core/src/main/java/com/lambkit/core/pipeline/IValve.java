package com.lambkit.core.pipeline;

public interface IValve<T extends IValve, C extends IPipLineContext> {

    void invoke(C context, boolean isHandled);

    void setNext(T valve);

    T getNext();

    default int getPriority() {
        return 0;
    }
}
