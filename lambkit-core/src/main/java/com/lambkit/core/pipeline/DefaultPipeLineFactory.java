package com.lambkit.core.pipeline;

import com.lambkit.core.task.app.AppTaskContext;
import com.lambkit.core.task.app.AppTaskPipLine;

/**
 * @author yangyong(孤竹行)
 */
public class DefaultPipeLineFactory implements IPipeLineFactory {

    private PipeLineService pipeLineService = new PipeLineServiceImpl();
    @Override
    public AppTaskPipLine createPipline() {
        return new AppTaskPipLine();
    }

    @Override
    public IPipLineContext createValveContext() {
        return new AppTaskContext();
    }

    @Override
    public PipeLineService getPipeLineService() {
        return pipeLineService;
    }
}
