package com.lambkit.core.pipeline;

public abstract class BasePipeLine<T extends IValve, C extends IPipLineContext> implements IPipeLine<T, C> {

    private T firstValve;

    @Override
    public void addValve(T valve) {
        T valveT = valve;
        if (firstValve == null) {
            firstValve = valveT;
        } else {
            IValve current = firstValve;
            while (current != null) {
                if (current.getNext() == null) {
                    current.setNext(valve);
                    break;
                }
                current = current.getNext();
            }
        }
    }

    public T firstValve() {
        return firstValve;
    }
}
