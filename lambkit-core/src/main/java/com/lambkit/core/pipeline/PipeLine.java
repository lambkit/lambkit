package com.lambkit.core.pipeline;

public class PipeLine extends BasePipeLine<Valve, PipeLineContext> {
    @Override
    public void invoke(PipeLineContext context) {
        Valve f = firstValve();
        if(f != null) {
            f.invoke(context, false);
        }
    }
}
