package com.lambkit.core.pipeline;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * @author yangyong(孤竹行)
 */
public class PipeLineContainer {
    protected Map<String, IPipeLine> objectMap = new ConcurrentHashMap<>(5);

    public IPipeLine get(String key) {
        return objectMap.get(key);
    }

    public void add(String name, IPipeLine bean) {
        objectMap.put(name, bean);
    }

    public void put(String name, IPipeLine bean) {
        objectMap.put(name, bean);
    }

    public void set(String name, IPipeLine bean) {
        objectMap.put(name, bean);
    }

    public void remove(String name) {
        objectMap.remove(name);
    }

    public void rem(String name) {
        objectMap.remove(name);
    }

    public void del(String name) {
        objectMap.remove(name);
    }
}
