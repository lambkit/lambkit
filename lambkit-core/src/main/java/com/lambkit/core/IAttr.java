package com.lambkit.core;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.sql.Time;
import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.util.Date;
import java.util.Map;

/**
 * @author yangyong(孤竹行)
 */
public interface IAttr<M> {
    Map<String, Object> toMap();

    M put(Map<String, Object> otherMap);

    M set(String key, Object value);

    <T> T get(String key);

    <T> T get(String key, T defaultValue);

    String getStr(String key);

    Integer getInt(String key);

    Long getLong(String key);

    BigInteger getBigInteger(String key);

    Date getDate(String key);

    LocalDateTime getLocalDateTime(String key);

    Time getTime(String key);

    Timestamp getTimestamp(String key);

    Double getDouble(String key);

    Float getFloat(String key);

    Short getShort(String key);

    Byte getByte(String key);

    Boolean getBoolean(String key);

    BigDecimal getBigDecimal(String key);

    byte[] getBytes(String key);

    Number getNumber(String key);

    int size();
}
