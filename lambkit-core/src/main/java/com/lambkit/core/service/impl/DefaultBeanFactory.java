package com.lambkit.core.service.impl;

import cn.hutool.core.util.ReflectUtil;
import com.lambkit.core.service.AbstractBeanFactory;

/**
 * @author yangyong(孤竹行)
 */
public class DefaultBeanFactory extends AbstractBeanFactory {

    @Override
    public <T> T newInstance(Class<T> clazz) {
        return ReflectUtil.newInstance(clazz);
    }
}
