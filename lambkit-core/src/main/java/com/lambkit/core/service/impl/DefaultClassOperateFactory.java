package com.lambkit.core.service.impl;

import com.lambkit.core.service.ClassOperateFactory;
import com.lambkit.core.service.ClassOperateService;

/**
 * @author yangyong(孤竹行)
 */
public class DefaultClassOperateFactory implements ClassOperateFactory {

    private ClassOperateService classOperateService = new ClassOperateServiceImpl();
    @Override
    public ClassOperateService getClassOperateService() {
        return classOperateService;
    }
}
