package com.lambkit.core.service;

public interface ResourceFactory {

    ResourceService getResourceService();
}
