package com.lambkit.core.service;

import java.lang.annotation.Annotation;
import java.util.Set;

/**
 * @author yangyong(孤竹行)
 */
public interface ClassOperateService {

    void scanPackageByAnnotation(String packageNames, final Class<? extends Annotation> annotationClass, ScanClassProcess scanClassProcess);

    void scanPackageBySuper(String packageNames, Class<?> superClass, ScanClassProcess scanClassProcess);

    Set<Class<?>> scanPackageBySuper(String packageNames, Class<?> superClass);
}
