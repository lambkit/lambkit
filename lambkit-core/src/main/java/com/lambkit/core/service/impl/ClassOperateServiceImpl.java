package com.lambkit.core.service.impl;

import cn.hutool.core.util.ClassUtil;
import cn.hutool.core.util.StrUtil;
import com.lambkit.core.service.ClassOperateService;
import com.lambkit.core.service.ScanClassProcess;

import java.lang.annotation.Annotation;
import java.util.Set;

/**
 * @author yangyong(孤竹行)
 */
public class ClassOperateServiceImpl implements ClassOperateService {
    @Override
    public void scanPackageByAnnotation(String packageNames, final Class<? extends Annotation> annotationClass, ScanClassProcess scanClassProcess) {
        if(StrUtil.isNotBlank(packageNames)) {
            Set<Class<?>> classes = ClassUtil.scanPackageByAnnotation(packageNames, annotationClass);
            if (classes.size()==0) {
                return;
            }
            for (Class<?> clazz : classes) {
                scanClassProcess.process(clazz);
            }
        }
    }

    @Override
    public void scanPackageBySuper(String packageNames, Class<?> superClass, ScanClassProcess scanClassProcess) {
        if(StrUtil.isNotBlank(packageNames)) {
            Set<Class<?>> classes = ClassUtil.scanPackageBySuper(packageNames, superClass);
            if (classes.size()==0) {
                return;
            }
            for (Class<?> clazz : classes) {
                scanClassProcess.process(clazz);
            }
        }
    }

    @Override
    public Set<Class<?>> scanPackageBySuper(String packageNames, Class<?> superClass) {
        if(StrUtil.isNotBlank(packageNames)) {
            return ClassUtil.scanPackageBySuper(packageNames, superClass);
        }
        return null;
    }
}
