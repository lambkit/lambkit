package com.lambkit.core.service;

import java.io.File;
import java.util.jar.JarEntry;
import java.util.jar.JarFile;

/**
 * @author yangyong(孤竹行)
 */
public interface ScanFileProcess {
    boolean process(File file);

    boolean process(JarFile jarFile, JarEntry jarEntry);
}
