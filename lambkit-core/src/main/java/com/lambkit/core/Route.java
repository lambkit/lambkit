package com.lambkit.core;

import java.util.HashMap;
import java.util.Map;

/**
 * @author yangyong(孤竹行)
 */
public class Route implements IRoute {

    private Map<String, Class<?>> routes;

    public Route() {
        routes = new HashMap<>(5);
    }

    public Route(Map<String, Class<?>> routes) {
        this.routes = routes;
    }

    public static Route by(String path, Class<?> clazz) {
        Route route = new Route();
        route.add(path, clazz);
        return route;
    }

    public Route add(String path, Class<?> clazz) {
        routes.put(path, clazz);
        return this;
    }

    public Class<?> get(String path) {
        return routes.get(path);
    }

    public void remove(String path) {
        routes.remove(path);
    }

    public Map<String, Class<?>> getRoutes() {
        return routes;
    }

    public void setRoutes(Map<String, Class<?>> routes) {
        this.routes = routes;
    }
}
