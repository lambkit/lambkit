package com.lambkit.core.processor;

import com.lambkit.core.IContext;

/**
 * 后处理器
 * @param <T>
 */
public interface IPostProcessor<T extends IContext> {
    default public boolean handleBefore(T context) {
        return true;
    }

    default public void handleAfter(T context) {
    }

    default int getPriority() {
        return 0;
    }
}
