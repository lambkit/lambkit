package com.lambkit.core.task;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * @author yangyong(孤竹行)
 */
public class TaskContainer {
    protected Map<String, ITask> objectMap = new ConcurrentHashMap<>(5);

    public ITask get(String key) {
        return objectMap.get(key);
    }

    public ITask route(String key) {
        ITask task = objectMap.get(key);
        if(task == null) {
            String skey = key.endsWith("/") ? key : key + "/";
            for (String name : objectMap.keySet()) {
                String sname = name.endsWith("/") ? name : name + "/";
                if(sname.startsWith(skey)) {
                    task = objectMap.get(name);
                    break;
                }
            }
        }
        return task;
    }

    public void add(String name, ITask task) {
        objectMap.put(name, task);
    }

    public void put(String name, ITask task) {
        objectMap.put(name, task);
    }

    public void set(String name, ITask task) {
        objectMap.put(name, task);
    }

    public void remove(String name) {
        objectMap.remove(name);
    }

    public void rem(String name) {
        objectMap.remove(name);
    }

    public void del(String name) {
        objectMap.remove(name);
    }
}
