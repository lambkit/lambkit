package com.lambkit.core.task;

/**
 * 任务队列
 * @author yangyong(孤竹行)
 */
public interface IWorkQueue {
    /**
     * 插入任务
     * @param task
     * @return
     */
    boolean push(ITaskInstance task);

    /**
     * 获取下一个任务
     * @return
     */
    ITaskInstance next();

    boolean remove(ITaskInstance task);

    int size();
}
