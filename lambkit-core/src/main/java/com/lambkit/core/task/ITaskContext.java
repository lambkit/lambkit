package com.lambkit.core.task;

import com.lambkit.core.pipeline.IPipLineContext;

/**
 * 任务上下文
 */
public interface ITaskContext extends IPipLineContext {

}
