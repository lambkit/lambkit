package com.lambkit.core.task.app;

import com.lambkit.core.pipeline.BasePipeLine;

/**
 * @author yangyong(孤竹行)
 */
public class AppTaskPipLine extends BasePipeLine<AppTaskValve, AppTaskContext> {
    @Override
    public void invoke(AppTaskContext valveContext) {
        AppTaskValve f = firstValve();
        if(f != null) {
            f.invoke(valveContext, true);
        }
    }
}
