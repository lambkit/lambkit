package com.lambkit.core.task;

import com.lambkit.core.Lifecycle;

/**
 * 具体某一个处理任务，即工作
 * @author yangyong(孤竹行)
 */
public interface ITaskInstance<C extends ITaskContext, T extends BaseTask> extends Lifecycle {
    /**
     * 工作名称/标识
     * @return
     */
    String getId();

    C getContext();

    void setContext(C context);


}
