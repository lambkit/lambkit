package com.lambkit.core.task.app;

import com.lambkit.core.pipeline.BaseValve;

/**
 * @author yangyong(孤竹行)
 */
public abstract class AppTaskValve extends BaseValve<AppTaskValve, AppTaskContext>  {
}
