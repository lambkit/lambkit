package com.lambkit.core.task.app;

import com.lambkit.core.task.BaseTask;

/**
 * @author yangyong(孤竹行)
 */
public class AppTask extends BaseTask<AppTaskPipLine, AppTaskValve, AppTaskContext> {

    public AppTask() {
        this.setPipLine(new AppTaskPipLine());
    }

    public static AppTask by(AppTaskValve valve) {
        AppTask task = new AppTask();
        task.addValve(valve);
        return task;
    }

    public AppTask addValve(AppTaskValve valve) {
        super.addValve(valve);
        return this;
    }
}
