package com.lambkit.core.task.http;

import com.lambkit.core.task.TaskInstance;

/**
 * @author yangyong(孤竹行)
 */
public class HttpTaskInstance extends TaskInstance<HttpTaskContext, HttpTask> {

    public HttpTaskInstance() {
        super(new HttpTaskContext());
    }

    public HttpTaskInstance(HttpTaskContext context) {
        super(context);
    }

    public HttpTaskInstance runOn(HttpTaskContext context) {
        super.runOn(context);
        return this;
    }
}
