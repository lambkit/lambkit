package com.lambkit.core.task.http;

import com.lambkit.core.pipeline.BaseValve;

/**
 * @author yangyong(孤竹行)
 */
public abstract class HttpTaskValve extends BaseValve<HttpTaskValve, HttpTaskContext>  {
}
