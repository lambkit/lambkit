package com.lambkit.core.task;

import com.lambkit.core.LambkitApp;
import com.lambkit.core.LifecycleState;

/**
 * 任务池（定时任务 + 任务队列）
 * 任务中心，监听Queue队列中的任务执行进度，进行超时中断操作
 * @author yangyong(孤竹行)
 */
public abstract class WorkCenter<T extends ITask, I extends ITaskInstance> implements IWorkCenter<T, I> {
    private LambkitApp app;

    private TaskContainer taskContainer = new TaskContainer();

    private LifecycleState currentState = LifecycleState.NEW;

    @Override
    public WorkCenter runOn(LambkitApp app) {
        this.app = app;
        return this;
    }

    public WorkCenter addTask(String name, T task) {
        task.setId(name);
        taskContainer.add(name, task);
        return this;
    }

//    public WorkCenter addTaskValve(String name, Class<? extends Valve> clazz) {
//        ITask task = taskContainer.get(name);
//        if(task != null) {
//            task.addValve((Valve) app.getBeanFactory().get(clazz));
//        }
//        return this;
//    }

    public WorkCenter addExecutor(String name, Class<? extends ITaskExecutor> clazz) {
        ITask task = taskContainer.route(name);
        if(task != null) {
            task.addExecutor(name, clazz);
        }
        return this;
    }

    public T route(String name) {
        return (T) taskContainer.route(name);
    }

    @Override
    public LifecycleState getCurrentState() {
        return currentState;
    }

    public void setCurrentState(LifecycleState currentState) {
        this.currentState = currentState;
    }

    public LambkitApp getApp() {
        return app;
    }

    public TaskContainer getTaskContainer() {
        return taskContainer;
    }

    @Override
    public IWorkQueue getQueue() {
        return null;
    }
}