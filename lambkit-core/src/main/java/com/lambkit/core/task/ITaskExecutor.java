package com.lambkit.core.task;

/**
 * 任务最终执行者
 * @author yangyong(孤竹行)
 */
public interface ITaskExecutor {
    void execute(ITaskContext context);
}
