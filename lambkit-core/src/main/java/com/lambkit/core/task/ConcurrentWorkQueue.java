package com.lambkit.core.task;

import java.util.concurrent.ConcurrentLinkedQueue;

/**
 * @author yangyong(孤竹行)
 */
public class ConcurrentWorkQueue implements IWorkQueue {

    /**
     * ConcurrentLinkedQueue：一个基于链表的无界非阻塞队列，使用 CAS 操作来实现线程安全。适用于高并发场景。
     */
    ConcurrentLinkedQueue<ITaskInstance> queue = new ConcurrentLinkedQueue<>();

    /**
     * offer(E e)：尝试插入元素到队列中，如果成功则返回 true，如果队列已满则立即返回 false
     * @param task
     * @return
     */
    public boolean push(ITaskInstance task) {
        return queue.offer(task);
    }

    /**
     * poll()：尝试从队列中取出一个元素，如果成功则返回元素，如果队列为空则立即返回 null。
     * @return
     */
    public ITaskInstance next() {
        return queue.poll();
    }

    public boolean remove(ITaskInstance task) {
        return queue.remove(task);
    }

    @Override
    public int size() {
        return queue.size();
    }
}
