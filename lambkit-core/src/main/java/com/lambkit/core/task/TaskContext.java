package com.lambkit.core.task;

import cn.hutool.core.util.IdUtil;
import com.lambkit.core.BaseContext;
import com.lambkit.core.LambkitResult;
import com.lambkit.core.LifecycleState;

/**
 * @author yangyong(孤竹行)
 */
public class TaskContext<T extends BaseTask> extends BaseContext {
    private T task;
    private String uri;

    public TaskContext() {
        this.setId(IdUtil.simpleUUID());
        this.setCurrentState(LifecycleState.NEW);
        this.setResult(new LambkitResult());
        this.setStartTime(System.currentTimeMillis());
    }

    public boolean isReady() {
        if(task == null) {
            return false;
        }
        if(uri == null) {
            return false;
        }
        return true;
    }

    public void percent(int percent, String msg) {
        getResult().percent(percent).message(msg);
        if(percent == 100) {
            getResult().code(1).setSuccess(true);
        }
    }

    public T getTask() {
        return task;
    }

    public void setTask(T task) {
        this.task = task;
    }

    public void setUri(String uri) {
        this.uri = uri;
    }

    public String getUri() {
        return uri;
    }
}