package com.lambkit.core.task.app;

import com.lambkit.core.task.TaskInstance;

/**
 * @author yangyong(孤竹行)
 */
public class AppTaskInstance extends TaskInstance<AppTaskContext, AppTask> {
    public AppTaskInstance() {
        super(new AppTaskContext());
    }

    public AppTaskInstance(AppTaskContext context) {
        super(context);
    }

    public AppTaskInstance runOn(AppTaskContext context) {
        super.runOn(context);
        return this;
    }
}
