package com.lambkit.core.task;

import com.lambkit.core.LambkitApp;
import com.lambkit.core.LambkitResult;
import com.lambkit.core.Lifecycle;
import com.lambkit.core.pipeline.IValve;

/**
 * 任务池（定时任务 + 任务队列）
 * 任务中心，监听Queue队列中的任务执行进度，进行超时中断操作
 */
public interface IWorkCenter<T extends ITask, I extends ITaskInstance> extends Lifecycle {

    /**
     * 加入任务，返回任务编号
     * @param task
     * @return
     */
    void execute(ITaskInstance task);

    IWorkQueue getQueue();

    /**
     * 获取任务结果
     * @param taskId
     * @return
     */
    LambkitResult getResult(String taskId);

    ITaskContext getContext(String taskId);
    /**
     * 如果任务已完成，获取任务并移出工作中心，否则返回null
     * @param taskId
     * @return
     */
    I poll(String taskId);

    IWorkCenter runOn(LambkitApp app);

    IWorkCenter addTask(String name, T task);

    IWorkCenter addValve(Class<? extends IValve> clazz);

    IWorkCenter addExecutor(String name, Class<? extends ITaskExecutor> clazz);

    T route(String name);
}
