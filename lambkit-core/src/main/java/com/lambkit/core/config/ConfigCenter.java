package com.lambkit.core.config;

import java.util.List;

/**
 * 配置中心
 * @author yangyong
 *
 */
public interface ConfigCenter {
	
	/**
	 * 配置中心名称
	 * @return
	 */
	public String getName();

	/**
	 * 获取配置
	 * @param key
	 * @return
	 */
	public String getValue(String key);
    
	/**
	 * 是否有配置
	 * @param key
	 * @return
	 */
    public boolean containsKey(String key);

	public List<String> getKeys(String prefix);
    /**
     * 更新、刷新、重启配置
     * @return
     */
    public boolean refresh();
    
    /**
     * 写入配置
	 * @param name configCenter的name
     * @param key
     * @param value
     */
    public void setValue(String name, String key, String value);
    
    /**
     * 移除配置
     * @param key
     */
    public void removeValue(String key);
}
