package com.lambkit.core.config;

import cn.hutool.core.util.StrUtil;

import java.util.*;
import java.util.concurrent.ConcurrentHashMap;

/**
 * 用于读取本地配置信息
 */
public class MapConfigCenter implements ConfigCenter {

	private Map<String, String> mapConfig = new ConcurrentHashMap<>();
	private String name = "map";
	
	public MapConfigCenter() {
	}
	
	public MapConfigCenter(String name) {
		this.name = name;
	}
	
	@Override
	public String getName() {
		return name;
	}

	@Override
	public String getValue(String key) {
		return mapConfig.get(key);
	}

	@Override
	public boolean containsKey(String key) {
		return mapConfig.containsKey(key);
	}

	@Override
	public boolean refresh() {
		return true;
	}

	@Override
	public void setValue(String name, String key, String value) {
		if(StrUtil.isBlank(name) || this.name.equals(name)) {
			mapConfig.put(key, value);
		}
	}

	@Override
	public void removeValue(String key) {
		mapConfig.remove(key);
	}

	public List<String> getKeys(String prefix) {
		List<String> keyList = new ArrayList<String>();
		for (Map.Entry<String, String> entry : mapConfig.entrySet()) {
			String key = entry.getKey().toString();
			if (key.startsWith(prefix) && entry.getValue() != null) {
				keyList.add(key);
			}
		}
		return keyList;
	}

	public List<String> getKeys() {
		Set<String> keys = mapConfig.keySet();
		List<String> keyList = new ArrayList<String>();
		keyList.addAll(keys);
		return keyList;
	}

	public Properties getProperties() {
		Properties properties = new Properties();
		properties.putAll(mapConfig);
		return properties;
	}

	public Map<String, String> getMapConfig() {
		return mapConfig;
	}
}
