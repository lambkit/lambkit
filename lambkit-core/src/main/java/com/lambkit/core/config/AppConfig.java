package com.lambkit.core.config;

import com.lambkit.core.AppContext;
import com.lambkit.core.AppLifecycle;
import com.lambkit.core.LifecycleException;
import com.lambkit.core.LifecycleState;
import com.lambkit.core.plugin.IPlugin;

/**
 * @author yangyong(孤竹行)
 */
public abstract class AppConfig implements AppLifecycle {
    private AppContext appContext;
    private LifecycleState currentState = LifecycleState.NEW;
    @Override
    public LifecycleState getCurrentState() {
        return currentState;
    }

    public void setCurrentState(LifecycleState currentState) {
        this.currentState = currentState;
    }

    @Override
    public void setContext(AppContext appContext) {
        this.appContext = appContext;
    }

    public AppContext getAppContext() {
        return appContext;
    }

    @Override
    public void start() throws LifecycleException {

    }
}
