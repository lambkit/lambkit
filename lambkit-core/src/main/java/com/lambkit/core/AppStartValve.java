package com.lambkit.core;

import cn.hutool.core.date.DateUtil;
import cn.hutool.core.date.TimeInterval;
import com.lambkit.core.pipeline.PipeLineContext;
import com.lambkit.core.pipeline.Valve;

import java.util.Collection;

public class AppStartValve extends Valve {
    @Override
    public void invoke(PipeLineContext context, boolean isHandled) {
        Collection<LambkitApp> apps = Lambkit.context().appCollection();
        for (LambkitApp app : apps) {
            TimeInterval timer = DateUtil.timer();
            if (app == null) {
                break;
            }
            try {
                // 启动开始
                app.start();
                next(context, isHandled);
                // 启动之后
                app.postStart();
            } catch (LifecycleException e) {
                throw new RuntimeException(e);
            }
            System.out.println(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>");
            System.out.println("LambkitApp [" + app.getClass().getName() + "] Starting Complete in " + timer.interval() + "ms.");
        }
    }
}
