package com.lambkit.core;

import cn.hutool.core.util.ArrayUtil;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * @author yangyong(孤竹行)
 */
public class BeanContainer implements Lifecycle {
    private final Map<Class<?>, Object> beanMap = new ConcurrentHashMap<>(5);
    private final Map<Class<?>, Class<?>[]> beanImpls = new ConcurrentHashMap<>(64);

    public <T> T getBean(Class<T> clazz) {
        return (T) beanMap.get(clazz);
    }

    public <T> Class<?>[] getImpl(Class<T> clazz) {
        return beanImpls.get(clazz);
    }

    public void addBean(Class<?> clazz, Object bean) {
        beanMap.put(clazz, bean);
    }

    public void addImpl(Class<?> clazz, Class<?>... implClasses) {
        Class<?>[] beanImplClassArray = beanImpls.get(clazz);
        if(beanImplClassArray==null) {
            beanImpls.put(clazz, implClasses);
        } else {
            Class<?>[] newImplClasses = ArrayUtil.append(beanImplClassArray, implClasses);
            beanImpls.put(clazz, newImplClasses);
        }
    }

    public void removeBean(Class<?> clazz) {
        beanMap.remove(clazz);
    }

    public void removeImpl(Class<?> clazz) {
        beanImpls.remove(clazz);
    }

    @Override
    public LifecycleState getCurrentState() {
        return null;
    }

    @Override
    public void start() throws LifecycleException {

    }
}
