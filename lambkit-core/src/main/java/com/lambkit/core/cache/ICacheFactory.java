package com.lambkit.core.cache;

/**
 * @author yangyong(孤竹行)
 */
public interface ICacheFactory {
    ICache getCache();

    ICache getCache(String type);
}
