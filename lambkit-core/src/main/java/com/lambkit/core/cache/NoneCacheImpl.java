/**
 * Copyright (c) 2015-2017, Henry Yang 杨勇 (gismail@foxmail.com).
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.lambkit.core.cache;

import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Set;


public class NoneCacheImpl extends BaseCache {

	@Override
	public String getCacheType() {
		return "none";
	}

	@Override
	public Long size(String cacheName) {
		return 0L;
	}

	@Override
	public Collection getValues(String cacheName) {
		return null;
	}

	@Override
	public List getKeys(String cacheName) {
		return null;
	}

	@Override
	public List<String> getKeys(String cacheName, String keySearch) {
		return null;
	}

	@Override
	public Map getAll(String cacheName) {
		return null;
	}

	@Override
	public <T> T get(String cacheName, Object key) {
		return null;
	}

	@Override
	public void put(String cacheName, Object key, Object value) {
	}

	@Override
	public void remove(String cacheName, Object key) {
	}

	@Override
	public void removeAll(String cacheName) {
	}

	@Override
	public boolean isNoneCache() {
		return true;
	}

	@Override
	public String[] getCacheNames() {
		return new String[0];
	}

	@Override
	public void put(String cacheName, Object key, Object value, int liveSeconds) {
	}

	@Override
	public Long llen(String cacheName, Object key) {
		return 0L;
	}

	@Override
	public void lrem(String cacheName, Object key, int count, Object value) {
	}

	@Override
	public List lrange(String cacheName, Object key, int start, int end) {
		return null;
	}

	@Override
	public void srem(String cacheName, Object key, Object... members) {
	}

	@Override
	public Set smembers(String cacheName, Object key) {
		return null;
	}

	@Override
	public Long scard(String cacheName, Object key) {
		return 0L;
	}

	@Override
	public void lpush(String cacheName, Object key, Object... values) {
	}

	@Override
	public Long expire(String cacheName, Object key, int seconds) {
		return null;
	}

	@Override
	public long getExpire(String cacheName, String key) {
		return 0;
	}

	@Override
	public Long sadd(String cacheName, Object key, Object... members) {
		return null;
	}

	@Override
	public void saddAndExpire(String cacheName, Object key, Object value, int seconds) {
	}

}
