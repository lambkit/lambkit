package com.lambkit.core;

import cn.hutool.core.date.DateUtil;
import cn.hutool.core.date.TimeInterval;
import com.lambkit.core.pipeline.PipeLineContext;
import com.lambkit.core.pipeline.Valve;

import java.util.Collection;

public class AppStopValve extends Valve {
    @Override
    public void invoke(PipeLineContext context, boolean isHandled) {
        Collection<LambkitApp> apps = Lambkit.context().appCollection();
        for (LambkitApp app : apps) {
            TimeInterval timer = DateUtil.timer();
            if(app == null) {
                break;
            }
            try {
                // 停止之前
                app.preStop();
                next(context, isHandled);
                // 停止之后
                app.stop();
            } catch (LifecycleException e) {
                throw new RuntimeException(e);
            }
            System.out.println(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>");
            System.out.println("LambkitApp [" + app.getClass().getName() + "] Stopping Complete in " + timer.interval() + "ms.");
        }
    }
}
