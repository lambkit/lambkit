package com.lambkit.core;

import java.util.Map;

/**
 * 上下文
 * @author yangyong(孤竹行)
 */
public interface IContext {

    /**
     * 上下文编号
     * @return
     */
    String getId();

    /**
     * 生命周期状态
     * @return
     */
    LifecycleState getCurrentState();

    void setCurrentState(LifecycleState currentState);

    /**
     * 处理结果
     * @return
     */
    LambkitResult getResult();

    void setResult(LambkitResult result);

    public Attr attr();

    /**
     * Returns the value of the named attribute as an Object, or null if no attribute of the given name exists.
     * @param name a String specifying the name of the attribute
     * @return an Object containing the value of the attribute, or null if the attribute does not exist
     */
    public <T> T getAttr(String name);

    /**
     * Returns the value of the named attribute as an Object, or null if no attribute of the given name exists.
     * @param name a String specifying the name of the attribute
     * @param defaultValue the default value to return if the attribute does not exist
     * @return an Object containing the value of the attribute, or null if the attribute does not exist
     */
    public <T> T getAttr(String name, T defaultValue);

    /**
     * Returns the value of the named attribute as an Object, or null if no attribute of the given name exists.
     * @param name a String specifying the name of the attribute
     * @return an String Object containing the value of the attribute, or null if the attribute does not exist
     */
    public String getAttrToStr(String name);

    /**
     * Returns the value of the named attribute as an Object, or null if no attribute of the given name exists.
     * @param name a String specifying the name of the attribute
     * @return an Integer Object containing the value of the attribute, or null if the attribute does not exist
     */
    public Integer getAttrToInt(String name);

    /**
     * Stores an attribute in this context
     * @param name a String specifying the name of the attribute
     * @param value the Object to be stored
     */
    void setAttr(String name, Object value);

    /**
     * Removes an attribute from this context
     * @param name a String specifying the name of the attribute to remove
     */
    void removeAttr(String name);

    /**
     * Stores attributes in this context, key of the map as attribute name and value of the map as attribute value
     * @param attrMap key and value as attribute of the map to be stored
     */
    void setAttrs(Map<String, Object> attrMap);
}
