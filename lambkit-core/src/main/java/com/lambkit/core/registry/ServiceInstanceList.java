package com.lambkit.core.registry;

import java.io.Serializable;
import java.util.List;
import java.util.stream.Collectors;

/**
 * @author yangyong(孤竹行)
 */
public class ServiceInstanceList implements Serializable {
    private String name;//	String	分组名@@服务名

    private String groupName;//	String	否	分组名，默认为DEFAULT_GROUP

    private int cacheMillis = 10000;//	int	缓存时间

    private long lastRefTime;//	int	上次刷新时间

    private int checksum;//	int	校验码

    private boolean allIPs;//	boolean

    private boolean reachProtectionThreshold;//	boolean	是否到达保护阈值

    private boolean valid;//	boolean	是否有效

    private List<ServiceInstance> hosts;//实例列表


    public ServiceInstance getInstance() {
        List<ServiceInstance> hostList = hosts.stream().sorted((o1, o2) -> o1.getWeight() - o2.getWeight()).collect(Collectors.toList());;
        for (ServiceInstance host : hostList) {
            if(host.isEnabled() && host.isHealthy()) {
                return host;
            }
        }
        return null;
    }

    public ServiceInstance getInstance(ServiceInstanceVo serviceInstanceVo) {
        ServiceInstance serviceInstance = hosts.stream().filter(host -> host.getIp().equals(serviceInstanceVo.getIp()) && host.getPort()==serviceInstanceVo.getPort()).findFirst().orElse(null);
        return serviceInstance;
    }

    public boolean addInstance(ServiceInstance serviceInstance) {
        for (ServiceInstance host : hosts) {
            if(host.getIp().equals(serviceInstance.getIp()) && host.getPort()==serviceInstance.getPort()) {
                host = serviceInstance;
                return false;
            }
        }
        return hosts.add(serviceInstance);
    }

    public boolean removeInstance(ServiceInstance serviceInstance) {
        ServiceInstance removeInstance = hosts.stream().filter(host -> host.getIp().equals(serviceInstance.getIp()) && host.getPort()==serviceInstance.getPort()).findFirst().orElse(null);
        return removeInstance != null ? hosts.remove(removeInstance) : false;
    }

    //---------------------------------------
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getGroupName() {
        return groupName;
    }

    public void setGroupName(String groupName) {
        this.groupName = groupName;
    }

    public int getCacheMillis() {
        return cacheMillis;
    }

    public void setCacheMillis(int cacheMillis) {
        this.cacheMillis = cacheMillis;
    }

    public long getLastRefTime() {
        return lastRefTime;
    }

    public void setLastRefTime(long lastRefTime) {
        this.lastRefTime = lastRefTime;
    }

    public int getChecksum() {
        return checksum;
    }

    public void setChecksum(int checksum) {
        this.checksum = checksum;
    }

    public boolean isAllIPs() {
        return allIPs;
    }

    public void setAllIPs(boolean allIPs) {
        this.allIPs = allIPs;
    }

    public boolean isReachProtectionThreshold() {
        return reachProtectionThreshold;
    }

    public void setReachProtectionThreshold(boolean reachProtectionThreshold) {
        this.reachProtectionThreshold = reachProtectionThreshold;
    }

    public boolean isValid() {
        return valid;
    }

    public void setValid(boolean valid) {
        this.valid = valid;
    }

    public List<ServiceInstance> getHosts() {
        return hosts;
    }

    public void setHosts(List<ServiceInstance> hosts) {
        this.hosts = hosts;
    }


}
