package com.lambkit.core.registry;

/**
 * 注册中心
 * @author yangyong(孤竹行)
 */
public interface RegisterCenter {
    ServiceInstance getInstance(ServiceInstanceVo serviceInstanceVo);

    ServiceInstanceList getInstanceList(ServiceInstanceListVo serviceInstanceListVo);

    boolean createInstance(ServiceInstance serviceInstance);

    boolean updateInstance(ServiceInstance serviceInstance);

    boolean removeInstance(ServiceInstance serviceInstance);
}
