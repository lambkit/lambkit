package com.lambkit.data;

/**
 * 数据表
 * @author DELL
 *
 */
public interface DataTable {
	
	DataRecordCollection getDataRecords();
	
	DataRecordCollection getDataRecords(DataFilter filter);
	
}
