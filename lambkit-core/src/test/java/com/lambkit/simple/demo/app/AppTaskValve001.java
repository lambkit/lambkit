package com.lambkit.simple.demo.app;

import com.lambkit.core.task.app.AppTaskContext;
import com.lambkit.core.task.app.AppTaskValve;

/**
 * @author yangyong(孤竹行)
 */
public class AppTaskValve001 extends AppTaskValve {
    @Override
    public void invoke(AppTaskContext valveContext, boolean isHandled) {
        System.out.println("AppTaskValve001 invoke");
        valveContext.setAttr("valve001", "Hello AppValve001");
        valveContext.percent(10, "AppTaskValve001 progress");
        next(valveContext, isHandled);
        String msg = valveContext.getAttr("valve001");
        valveContext.setAttr("valve001", msg + " end001");
    }
}
