package com.lambkit.simple.demo.app;

import com.lambkit.core.task.app.AppTaskContext;
import com.lambkit.core.task.app.AppTaskValve;

/**
 * @author yangyong(孤竹行)
 */
public class AppTaskValve002 extends AppTaskValve {
    @Override
    public void invoke(AppTaskContext taskContext, boolean isHandled) {
        System.out.println("AppTaskValve002 invoke");
        taskContext.setAttr("valve002", "welcome to lambkit 002");
        taskContext.percent(30, "AppTaskValve002 progress");
        next(taskContext, isHandled);
        String msg = taskContext.getAttr("valve002");
        taskContext.setAttr("valve002", msg + " !");
    }
}
