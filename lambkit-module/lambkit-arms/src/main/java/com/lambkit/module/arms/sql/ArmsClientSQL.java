/**
 * Copyright (c) 2015-2017, Henry Yang 杨勇 (gismail@foxmail.com).
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.lambkit.module.arms.sql;

import com.lambkit.db.sql.ConditionMode;
import com.lambkit.db.sql.Column;
import com.lambkit.db.sql.Columns;
import com.lambkit.db.sql.Example;

import java.io.Serializable;
import java.util.List;

/**
 * @author yangyong 
 * @website: www.lambkit.com
 * @email: gismail@foxmail.com
 * @date ${date}
 * @version 1.0
 * @since 1.0
 */

public class ArmsClientSQL extends Columns implements Serializable {
	private static final long serialVersionUID = 1L;
	
	public static ArmsClientSQL of() {
		return new ArmsClientSQL();
	}
	
	public static ArmsClientSQL by(Column column) {
		ArmsClientSQL that = new ArmsClientSQL();
		that.add(column);
        return that;
    }

    public static ArmsClientSQL by(String name, Object value) {
        return (ArmsClientSQL) create().eq(name, value);
    }
    
    public Example example() {
    	return Example.create("arms_client", this);
    }
    
    /**
     * equals
     *
     * @param name
     * @param value
     * @return
     */
    public ArmsClientSQL eq(String name, Object value) {
    	super.eq(name, value);
        return this;
    }

    /**
     * not equals !=
     *
     * @param name
     * @param value
     * @return
     */
    public ArmsClientSQL ne(String name, Object value) {
    	super.ne(name, value);
        return this;
    }


    /**
     * like
     *
     * @param name
     * @param value
     * @return
     */

    public ArmsClientSQL like(String name, Object value) {
    	super.like(name, value);
        return this;
    }
    
    public ArmsClientSQL notLike(String name, Object value) {
    	super.notLike(name, value);
        return this;
    }

    /**
     * 大于 great than
     *
     * @param name
     * @param value
     * @return
     */
    public ArmsClientSQL gt(String name, Object value) {
    	super.gt(name, value);
        return this;
    }

    /**
     * 大于等于 great or equal
     *
     * @param name
     * @param value
     * @return
     */
    public ArmsClientSQL ge(String name, Object value) {
    	super.ge(name, value);
        return this;
    }

    /**
     * 小于 less than
     *
     * @param name
     * @param value
     * @return
     */
    public ArmsClientSQL lt(String name, Object value) {
    	super.lt(name, value);
        return this;
    }

    /**
     * 小于等于 less or equal
     *
     * @param name
     * @param value
     * @return
     */
    public ArmsClientSQL le(String name, Object value) {
    	super.le(name, value);
        return this;
    }
    
    public ArmsClientSQL isnull(String name) {
    	super.isnull(name);
        return this;
    } 

    public ArmsClientSQL notNull(String name) {
    	super.notNull(name);
        return this;
    } 
    
    public ArmsClientSQL empty(String name) {
    	super.empty(name);
        return this;
    } 
    
    public ArmsClientSQL notEmpty(String name) {
    	super.notEmpty(name);
        return this;
    } 
    
    public ArmsClientSQL add(Column column) {
    	super.add(column);
    	return this;
    }
    
    /**************************/
	
	public void addCriterion(String name, Object value, ConditionMode logic, String property, String typeHandler, String valueType) {
		 if (value == null) {
			 throw new RuntimeException("Value for " + property + " cannot be null");
		 }
		 add(Column.create(name, value, logic, typeHandler, valueType));
	}
   
	public void addCriterion(String name, Object value1, Object value2, ConditionMode logic, String property, String typeHandler, String valueType) {
		 if (value1 == null || value2 == null) {
			 throw new RuntimeException("Between values for " + property + " cannot be null");
		 }
		 add(Column.create(name, value1, value2, logic, typeHandler, valueType));
	}
		 
	public ArmsClientSQL andIdIsNull() {
		isnull("id");
		return this;
	}
	
	public ArmsClientSQL andIdIsNotNull() {
		notNull("id");
		return this;
	}
	
	public ArmsClientSQL andIdIsEmpty() {
		empty("id");
		return this;
	}

	public ArmsClientSQL andIdIsNotEmpty() {
		notEmpty("id");
		return this;
	}
      public ArmsClientSQL andIdEqualTo(java.lang.Long value) {
          addCriterion("id", value, ConditionMode.EQUAL, "id", "java.lang.Long", "Float");
          return this;
      }

      public ArmsClientSQL andIdNotEqualTo(java.lang.Long value) {
          addCriterion("id", value, ConditionMode.NOT_EQUAL, "id", "java.lang.Long", "Float");
          return this;
      }

      public ArmsClientSQL andIdGreaterThan(java.lang.Long value) {
          addCriterion("id", value, ConditionMode.GREATER_THEN, "id", "java.lang.Long", "Float");
          return this;
      }

      public ArmsClientSQL andIdGreaterThanOrEqualTo(java.lang.Long value) {
          addCriterion("id", value, ConditionMode.GREATER_EQUAL, "id", "java.lang.Long", "Float");
          return this;
      }

      public ArmsClientSQL andIdLessThan(java.lang.Long value) {
          addCriterion("id", value, ConditionMode.LESS_THEN, "id", "java.lang.Long", "Float");
          return this;
      }

      public ArmsClientSQL andIdLessThanOrEqualTo(java.lang.Long value) {
          addCriterion("id", value, ConditionMode.LESS_EQUAL, "id", "java.lang.Long", "Float");
          return this;
      }

      public ArmsClientSQL andIdBetween(java.lang.Long value1, java.lang.Long value2) {
    	  addCriterion("id", value1, value2, ConditionMode.BETWEEN, "id", "java.lang.Long", "Float");
    	  return this;
      }

      public ArmsClientSQL andIdNotBetween(java.lang.Long value1, java.lang.Long value2) {
          addCriterion("id", value1, value2, ConditionMode.NOT_BETWEEN, "id", "java.lang.Long", "Float");
          return this;
      }
        
      public ArmsClientSQL andIdIn(List<java.lang.Long> values) {
          addCriterion("id", values, ConditionMode.IN, "id", "java.lang.Long", "Float");
          return this;
      }

      public ArmsClientSQL andIdNotIn(List<java.lang.Long> values) {
          addCriterion("id", values, ConditionMode.NOT_IN, "id", "java.lang.Long", "Float");
          return this;
      }
	public ArmsClientSQL andClientIdIsNull() {
		isnull("client_id");
		return this;
	}
	
	public ArmsClientSQL andClientIdIsNotNull() {
		notNull("client_id");
		return this;
	}
	
	public ArmsClientSQL andClientIdIsEmpty() {
		empty("client_id");
		return this;
	}

	public ArmsClientSQL andClientIdIsNotEmpty() {
		notEmpty("client_id");
		return this;
	}
       public ArmsClientSQL andClientIdLike(java.lang.String value) {
    	   addCriterion("client_id", value, ConditionMode.FUZZY, "clientId", "java.lang.String", "Float");
    	   return this;
      }

      public ArmsClientSQL andClientIdNotLike(java.lang.String value) {
          addCriterion("client_id", value, ConditionMode.NOT_FUZZY, "clientId", "java.lang.String", "Float");
          return this;
      }
      public ArmsClientSQL andClientIdEqualTo(java.lang.String value) {
          addCriterion("client_id", value, ConditionMode.EQUAL, "clientId", "java.lang.String", "String");
          return this;
      }

      public ArmsClientSQL andClientIdNotEqualTo(java.lang.String value) {
          addCriterion("client_id", value, ConditionMode.NOT_EQUAL, "clientId", "java.lang.String", "String");
          return this;
      }

      public ArmsClientSQL andClientIdGreaterThan(java.lang.String value) {
          addCriterion("client_id", value, ConditionMode.GREATER_THEN, "clientId", "java.lang.String", "String");
          return this;
      }

      public ArmsClientSQL andClientIdGreaterThanOrEqualTo(java.lang.String value) {
          addCriterion("client_id", value, ConditionMode.GREATER_EQUAL, "clientId", "java.lang.String", "String");
          return this;
      }

      public ArmsClientSQL andClientIdLessThan(java.lang.String value) {
          addCriterion("client_id", value, ConditionMode.LESS_THEN, "clientId", "java.lang.String", "String");
          return this;
      }

      public ArmsClientSQL andClientIdLessThanOrEqualTo(java.lang.String value) {
          addCriterion("client_id", value, ConditionMode.LESS_EQUAL, "clientId", "java.lang.String", "String");
          return this;
      }

      public ArmsClientSQL andClientIdBetween(java.lang.String value1, java.lang.String value2) {
    	  addCriterion("client_id", value1, value2, ConditionMode.BETWEEN, "clientId", "java.lang.String", "String");
    	  return this;
      }

      public ArmsClientSQL andClientIdNotBetween(java.lang.String value1, java.lang.String value2) {
          addCriterion("client_id", value1, value2, ConditionMode.NOT_BETWEEN, "clientId", "java.lang.String", "String");
          return this;
      }
        
      public ArmsClientSQL andClientIdIn(List<java.lang.String> values) {
          addCriterion("client_id", values, ConditionMode.IN, "clientId", "java.lang.String", "String");
          return this;
      }

      public ArmsClientSQL andClientIdNotIn(List<java.lang.String> values) {
          addCriterion("client_id", values, ConditionMode.NOT_IN, "clientId", "java.lang.String", "String");
          return this;
      }
	public ArmsClientSQL andClientSecretIsNull() {
		isnull("client_secret");
		return this;
	}
	
	public ArmsClientSQL andClientSecretIsNotNull() {
		notNull("client_secret");
		return this;
	}
	
	public ArmsClientSQL andClientSecretIsEmpty() {
		empty("client_secret");
		return this;
	}

	public ArmsClientSQL andClientSecretIsNotEmpty() {
		notEmpty("client_secret");
		return this;
	}
       public ArmsClientSQL andClientSecretLike(java.lang.String value) {
    	   addCriterion("client_secret", value, ConditionMode.FUZZY, "clientSecret", "java.lang.String", "String");
    	   return this;
      }

      public ArmsClientSQL andClientSecretNotLike(java.lang.String value) {
          addCriterion("client_secret", value, ConditionMode.NOT_FUZZY, "clientSecret", "java.lang.String", "String");
          return this;
      }
      public ArmsClientSQL andClientSecretEqualTo(java.lang.String value) {
          addCriterion("client_secret", value, ConditionMode.EQUAL, "clientSecret", "java.lang.String", "String");
          return this;
      }

      public ArmsClientSQL andClientSecretNotEqualTo(java.lang.String value) {
          addCriterion("client_secret", value, ConditionMode.NOT_EQUAL, "clientSecret", "java.lang.String", "String");
          return this;
      }

      public ArmsClientSQL andClientSecretGreaterThan(java.lang.String value) {
          addCriterion("client_secret", value, ConditionMode.GREATER_THEN, "clientSecret", "java.lang.String", "String");
          return this;
      }

      public ArmsClientSQL andClientSecretGreaterThanOrEqualTo(java.lang.String value) {
          addCriterion("client_secret", value, ConditionMode.GREATER_EQUAL, "clientSecret", "java.lang.String", "String");
          return this;
      }

      public ArmsClientSQL andClientSecretLessThan(java.lang.String value) {
          addCriterion("client_secret", value, ConditionMode.LESS_THEN, "clientSecret", "java.lang.String", "String");
          return this;
      }

      public ArmsClientSQL andClientSecretLessThanOrEqualTo(java.lang.String value) {
          addCriterion("client_secret", value, ConditionMode.LESS_EQUAL, "clientSecret", "java.lang.String", "String");
          return this;
      }

      public ArmsClientSQL andClientSecretBetween(java.lang.String value1, java.lang.String value2) {
    	  addCriterion("client_secret", value1, value2, ConditionMode.BETWEEN, "clientSecret", "java.lang.String", "String");
    	  return this;
      }

      public ArmsClientSQL andClientSecretNotBetween(java.lang.String value1, java.lang.String value2) {
          addCriterion("client_secret", value1, value2, ConditionMode.NOT_BETWEEN, "clientSecret", "java.lang.String", "String");
          return this;
      }
        
      public ArmsClientSQL andClientSecretIn(List<java.lang.String> values) {
          addCriterion("client_secret", values, ConditionMode.IN, "clientSecret", "java.lang.String", "String");
          return this;
      }

      public ArmsClientSQL andClientSecretNotIn(List<java.lang.String> values) {
          addCriterion("client_secret", values, ConditionMode.NOT_IN, "clientSecret", "java.lang.String", "String");
          return this;
      }
	public ArmsClientSQL andResourceIdsIsNull() {
		isnull("resource_ids");
		return this;
	}
	
	public ArmsClientSQL andResourceIdsIsNotNull() {
		notNull("resource_ids");
		return this;
	}
	
	public ArmsClientSQL andResourceIdsIsEmpty() {
		empty("resource_ids");
		return this;
	}

	public ArmsClientSQL andResourceIdsIsNotEmpty() {
		notEmpty("resource_ids");
		return this;
	}
       public ArmsClientSQL andResourceIdsLike(java.lang.String value) {
    	   addCriterion("resource_ids", value, ConditionMode.FUZZY, "resourceIds", "java.lang.String", "String");
    	   return this;
      }

      public ArmsClientSQL andResourceIdsNotLike(java.lang.String value) {
          addCriterion("resource_ids", value, ConditionMode.NOT_FUZZY, "resourceIds", "java.lang.String", "String");
          return this;
      }
      public ArmsClientSQL andResourceIdsEqualTo(java.lang.String value) {
          addCriterion("resource_ids", value, ConditionMode.EQUAL, "resourceIds", "java.lang.String", "String");
          return this;
      }

      public ArmsClientSQL andResourceIdsNotEqualTo(java.lang.String value) {
          addCriterion("resource_ids", value, ConditionMode.NOT_EQUAL, "resourceIds", "java.lang.String", "String");
          return this;
      }

      public ArmsClientSQL andResourceIdsGreaterThan(java.lang.String value) {
          addCriterion("resource_ids", value, ConditionMode.GREATER_THEN, "resourceIds", "java.lang.String", "String");
          return this;
      }

      public ArmsClientSQL andResourceIdsGreaterThanOrEqualTo(java.lang.String value) {
          addCriterion("resource_ids", value, ConditionMode.GREATER_EQUAL, "resourceIds", "java.lang.String", "String");
          return this;
      }

      public ArmsClientSQL andResourceIdsLessThan(java.lang.String value) {
          addCriterion("resource_ids", value, ConditionMode.LESS_THEN, "resourceIds", "java.lang.String", "String");
          return this;
      }

      public ArmsClientSQL andResourceIdsLessThanOrEqualTo(java.lang.String value) {
          addCriterion("resource_ids", value, ConditionMode.LESS_EQUAL, "resourceIds", "java.lang.String", "String");
          return this;
      }

      public ArmsClientSQL andResourceIdsBetween(java.lang.String value1, java.lang.String value2) {
    	  addCriterion("resource_ids", value1, value2, ConditionMode.BETWEEN, "resourceIds", "java.lang.String", "String");
    	  return this;
      }

      public ArmsClientSQL andResourceIdsNotBetween(java.lang.String value1, java.lang.String value2) {
          addCriterion("resource_ids", value1, value2, ConditionMode.NOT_BETWEEN, "resourceIds", "java.lang.String", "String");
          return this;
      }
        
      public ArmsClientSQL andResourceIdsIn(List<java.lang.String> values) {
          addCriterion("resource_ids", values, ConditionMode.IN, "resourceIds", "java.lang.String", "String");
          return this;
      }

      public ArmsClientSQL andResourceIdsNotIn(List<java.lang.String> values) {
          addCriterion("resource_ids", values, ConditionMode.NOT_IN, "resourceIds", "java.lang.String", "String");
          return this;
      }
	public ArmsClientSQL andScopeIsNull() {
		isnull("scope");
		return this;
	}
	
	public ArmsClientSQL andScopeIsNotNull() {
		notNull("scope");
		return this;
	}
	
	public ArmsClientSQL andScopeIsEmpty() {
		empty("scope");
		return this;
	}

	public ArmsClientSQL andScopeIsNotEmpty() {
		notEmpty("scope");
		return this;
	}
       public ArmsClientSQL andScopeLike(java.lang.String value) {
    	   addCriterion("scope", value, ConditionMode.FUZZY, "scope", "java.lang.String", "String");
    	   return this;
      }

      public ArmsClientSQL andScopeNotLike(java.lang.String value) {
          addCriterion("scope", value, ConditionMode.NOT_FUZZY, "scope", "java.lang.String", "String");
          return this;
      }
      public ArmsClientSQL andScopeEqualTo(java.lang.String value) {
          addCriterion("scope", value, ConditionMode.EQUAL, "scope", "java.lang.String", "String");
          return this;
      }

      public ArmsClientSQL andScopeNotEqualTo(java.lang.String value) {
          addCriterion("scope", value, ConditionMode.NOT_EQUAL, "scope", "java.lang.String", "String");
          return this;
      }

      public ArmsClientSQL andScopeGreaterThan(java.lang.String value) {
          addCriterion("scope", value, ConditionMode.GREATER_THEN, "scope", "java.lang.String", "String");
          return this;
      }

      public ArmsClientSQL andScopeGreaterThanOrEqualTo(java.lang.String value) {
          addCriterion("scope", value, ConditionMode.GREATER_EQUAL, "scope", "java.lang.String", "String");
          return this;
      }

      public ArmsClientSQL andScopeLessThan(java.lang.String value) {
          addCriterion("scope", value, ConditionMode.LESS_THEN, "scope", "java.lang.String", "String");
          return this;
      }

      public ArmsClientSQL andScopeLessThanOrEqualTo(java.lang.String value) {
          addCriterion("scope", value, ConditionMode.LESS_EQUAL, "scope", "java.lang.String", "String");
          return this;
      }

      public ArmsClientSQL andScopeBetween(java.lang.String value1, java.lang.String value2) {
    	  addCriterion("scope", value1, value2, ConditionMode.BETWEEN, "scope", "java.lang.String", "String");
    	  return this;
      }

      public ArmsClientSQL andScopeNotBetween(java.lang.String value1, java.lang.String value2) {
          addCriterion("scope", value1, value2, ConditionMode.NOT_BETWEEN, "scope", "java.lang.String", "String");
          return this;
      }
        
      public ArmsClientSQL andScopeIn(List<java.lang.String> values) {
          addCriterion("scope", values, ConditionMode.IN, "scope", "java.lang.String", "String");
          return this;
      }

      public ArmsClientSQL andScopeNotIn(List<java.lang.String> values) {
          addCriterion("scope", values, ConditionMode.NOT_IN, "scope", "java.lang.String", "String");
          return this;
      }
	public ArmsClientSQL andAuthorizedGrantTypesIsNull() {
		isnull("authorized_grant_types");
		return this;
	}
	
	public ArmsClientSQL andAuthorizedGrantTypesIsNotNull() {
		notNull("authorized_grant_types");
		return this;
	}
	
	public ArmsClientSQL andAuthorizedGrantTypesIsEmpty() {
		empty("authorized_grant_types");
		return this;
	}

	public ArmsClientSQL andAuthorizedGrantTypesIsNotEmpty() {
		notEmpty("authorized_grant_types");
		return this;
	}
       public ArmsClientSQL andAuthorizedGrantTypesLike(java.lang.String value) {
    	   addCriterion("authorized_grant_types", value, ConditionMode.FUZZY, "authorizedGrantTypes", "java.lang.String", "String");
    	   return this;
      }

      public ArmsClientSQL andAuthorizedGrantTypesNotLike(java.lang.String value) {
          addCriterion("authorized_grant_types", value, ConditionMode.NOT_FUZZY, "authorizedGrantTypes", "java.lang.String", "String");
          return this;
      }
      public ArmsClientSQL andAuthorizedGrantTypesEqualTo(java.lang.String value) {
          addCriterion("authorized_grant_types", value, ConditionMode.EQUAL, "authorizedGrantTypes", "java.lang.String", "String");
          return this;
      }

      public ArmsClientSQL andAuthorizedGrantTypesNotEqualTo(java.lang.String value) {
          addCriterion("authorized_grant_types", value, ConditionMode.NOT_EQUAL, "authorizedGrantTypes", "java.lang.String", "String");
          return this;
      }

      public ArmsClientSQL andAuthorizedGrantTypesGreaterThan(java.lang.String value) {
          addCriterion("authorized_grant_types", value, ConditionMode.GREATER_THEN, "authorizedGrantTypes", "java.lang.String", "String");
          return this;
      }

      public ArmsClientSQL andAuthorizedGrantTypesGreaterThanOrEqualTo(java.lang.String value) {
          addCriterion("authorized_grant_types", value, ConditionMode.GREATER_EQUAL, "authorizedGrantTypes", "java.lang.String", "String");
          return this;
      }

      public ArmsClientSQL andAuthorizedGrantTypesLessThan(java.lang.String value) {
          addCriterion("authorized_grant_types", value, ConditionMode.LESS_THEN, "authorizedGrantTypes", "java.lang.String", "String");
          return this;
      }

      public ArmsClientSQL andAuthorizedGrantTypesLessThanOrEqualTo(java.lang.String value) {
          addCriterion("authorized_grant_types", value, ConditionMode.LESS_EQUAL, "authorizedGrantTypes", "java.lang.String", "String");
          return this;
      }

      public ArmsClientSQL andAuthorizedGrantTypesBetween(java.lang.String value1, java.lang.String value2) {
    	  addCriterion("authorized_grant_types", value1, value2, ConditionMode.BETWEEN, "authorizedGrantTypes", "java.lang.String", "String");
    	  return this;
      }

      public ArmsClientSQL andAuthorizedGrantTypesNotBetween(java.lang.String value1, java.lang.String value2) {
          addCriterion("authorized_grant_types", value1, value2, ConditionMode.NOT_BETWEEN, "authorizedGrantTypes", "java.lang.String", "String");
          return this;
      }
        
      public ArmsClientSQL andAuthorizedGrantTypesIn(List<java.lang.String> values) {
          addCriterion("authorized_grant_types", values, ConditionMode.IN, "authorizedGrantTypes", "java.lang.String", "String");
          return this;
      }

      public ArmsClientSQL andAuthorizedGrantTypesNotIn(List<java.lang.String> values) {
          addCriterion("authorized_grant_types", values, ConditionMode.NOT_IN, "authorizedGrantTypes", "java.lang.String", "String");
          return this;
      }
	public ArmsClientSQL andWebServerRedirectUriIsNull() {
		isnull("web_server_redirect_uri");
		return this;
	}
	
	public ArmsClientSQL andWebServerRedirectUriIsNotNull() {
		notNull("web_server_redirect_uri");
		return this;
	}
	
	public ArmsClientSQL andWebServerRedirectUriIsEmpty() {
		empty("web_server_redirect_uri");
		return this;
	}

	public ArmsClientSQL andWebServerRedirectUriIsNotEmpty() {
		notEmpty("web_server_redirect_uri");
		return this;
	}
       public ArmsClientSQL andWebServerRedirectUriLike(java.lang.String value) {
    	   addCriterion("web_server_redirect_uri", value, ConditionMode.FUZZY, "webServerRedirectUri", "java.lang.String", "String");
    	   return this;
      }

      public ArmsClientSQL andWebServerRedirectUriNotLike(java.lang.String value) {
          addCriterion("web_server_redirect_uri", value, ConditionMode.NOT_FUZZY, "webServerRedirectUri", "java.lang.String", "String");
          return this;
      }
      public ArmsClientSQL andWebServerRedirectUriEqualTo(java.lang.String value) {
          addCriterion("web_server_redirect_uri", value, ConditionMode.EQUAL, "webServerRedirectUri", "java.lang.String", "String");
          return this;
      }

      public ArmsClientSQL andWebServerRedirectUriNotEqualTo(java.lang.String value) {
          addCriterion("web_server_redirect_uri", value, ConditionMode.NOT_EQUAL, "webServerRedirectUri", "java.lang.String", "String");
          return this;
      }

      public ArmsClientSQL andWebServerRedirectUriGreaterThan(java.lang.String value) {
          addCriterion("web_server_redirect_uri", value, ConditionMode.GREATER_THEN, "webServerRedirectUri", "java.lang.String", "String");
          return this;
      }

      public ArmsClientSQL andWebServerRedirectUriGreaterThanOrEqualTo(java.lang.String value) {
          addCriterion("web_server_redirect_uri", value, ConditionMode.GREATER_EQUAL, "webServerRedirectUri", "java.lang.String", "String");
          return this;
      }

      public ArmsClientSQL andWebServerRedirectUriLessThan(java.lang.String value) {
          addCriterion("web_server_redirect_uri", value, ConditionMode.LESS_THEN, "webServerRedirectUri", "java.lang.String", "String");
          return this;
      }

      public ArmsClientSQL andWebServerRedirectUriLessThanOrEqualTo(java.lang.String value) {
          addCriterion("web_server_redirect_uri", value, ConditionMode.LESS_EQUAL, "webServerRedirectUri", "java.lang.String", "String");
          return this;
      }

      public ArmsClientSQL andWebServerRedirectUriBetween(java.lang.String value1, java.lang.String value2) {
    	  addCriterion("web_server_redirect_uri", value1, value2, ConditionMode.BETWEEN, "webServerRedirectUri", "java.lang.String", "String");
    	  return this;
      }

      public ArmsClientSQL andWebServerRedirectUriNotBetween(java.lang.String value1, java.lang.String value2) {
          addCriterion("web_server_redirect_uri", value1, value2, ConditionMode.NOT_BETWEEN, "webServerRedirectUri", "java.lang.String", "String");
          return this;
      }
        
      public ArmsClientSQL andWebServerRedirectUriIn(List<java.lang.String> values) {
          addCriterion("web_server_redirect_uri", values, ConditionMode.IN, "webServerRedirectUri", "java.lang.String", "String");
          return this;
      }

      public ArmsClientSQL andWebServerRedirectUriNotIn(List<java.lang.String> values) {
          addCriterion("web_server_redirect_uri", values, ConditionMode.NOT_IN, "webServerRedirectUri", "java.lang.String", "String");
          return this;
      }
	public ArmsClientSQL andAuthoritiesIsNull() {
		isnull("authorities");
		return this;
	}
	
	public ArmsClientSQL andAuthoritiesIsNotNull() {
		notNull("authorities");
		return this;
	}
	
	public ArmsClientSQL andAuthoritiesIsEmpty() {
		empty("authorities");
		return this;
	}

	public ArmsClientSQL andAuthoritiesIsNotEmpty() {
		notEmpty("authorities");
		return this;
	}
       public ArmsClientSQL andAuthoritiesLike(java.lang.String value) {
    	   addCriterion("authorities", value, ConditionMode.FUZZY, "authorities", "java.lang.String", "String");
    	   return this;
      }

      public ArmsClientSQL andAuthoritiesNotLike(java.lang.String value) {
          addCriterion("authorities", value, ConditionMode.NOT_FUZZY, "authorities", "java.lang.String", "String");
          return this;
      }
      public ArmsClientSQL andAuthoritiesEqualTo(java.lang.String value) {
          addCriterion("authorities", value, ConditionMode.EQUAL, "authorities", "java.lang.String", "String");
          return this;
      }

      public ArmsClientSQL andAuthoritiesNotEqualTo(java.lang.String value) {
          addCriterion("authorities", value, ConditionMode.NOT_EQUAL, "authorities", "java.lang.String", "String");
          return this;
      }

      public ArmsClientSQL andAuthoritiesGreaterThan(java.lang.String value) {
          addCriterion("authorities", value, ConditionMode.GREATER_THEN, "authorities", "java.lang.String", "String");
          return this;
      }

      public ArmsClientSQL andAuthoritiesGreaterThanOrEqualTo(java.lang.String value) {
          addCriterion("authorities", value, ConditionMode.GREATER_EQUAL, "authorities", "java.lang.String", "String");
          return this;
      }

      public ArmsClientSQL andAuthoritiesLessThan(java.lang.String value) {
          addCriterion("authorities", value, ConditionMode.LESS_THEN, "authorities", "java.lang.String", "String");
          return this;
      }

      public ArmsClientSQL andAuthoritiesLessThanOrEqualTo(java.lang.String value) {
          addCriterion("authorities", value, ConditionMode.LESS_EQUAL, "authorities", "java.lang.String", "String");
          return this;
      }

      public ArmsClientSQL andAuthoritiesBetween(java.lang.String value1, java.lang.String value2) {
    	  addCriterion("authorities", value1, value2, ConditionMode.BETWEEN, "authorities", "java.lang.String", "String");
    	  return this;
      }

      public ArmsClientSQL andAuthoritiesNotBetween(java.lang.String value1, java.lang.String value2) {
          addCriterion("authorities", value1, value2, ConditionMode.NOT_BETWEEN, "authorities", "java.lang.String", "String");
          return this;
      }
        
      public ArmsClientSQL andAuthoritiesIn(List<java.lang.String> values) {
          addCriterion("authorities", values, ConditionMode.IN, "authorities", "java.lang.String", "String");
          return this;
      }

      public ArmsClientSQL andAuthoritiesNotIn(List<java.lang.String> values) {
          addCriterion("authorities", values, ConditionMode.NOT_IN, "authorities", "java.lang.String", "String");
          return this;
      }
	public ArmsClientSQL andAccessTokenValidityIsNull() {
		isnull("access_token_validity");
		return this;
	}
	
	public ArmsClientSQL andAccessTokenValidityIsNotNull() {
		notNull("access_token_validity");
		return this;
	}
	
	public ArmsClientSQL andAccessTokenValidityIsEmpty() {
		empty("access_token_validity");
		return this;
	}

	public ArmsClientSQL andAccessTokenValidityIsNotEmpty() {
		notEmpty("access_token_validity");
		return this;
	}
      public ArmsClientSQL andAccessTokenValidityEqualTo(java.lang.Integer value) {
          addCriterion("access_token_validity", value, ConditionMode.EQUAL, "accessTokenValidity", "java.lang.Integer", "Float");
          return this;
      }

      public ArmsClientSQL andAccessTokenValidityNotEqualTo(java.lang.Integer value) {
          addCriterion("access_token_validity", value, ConditionMode.NOT_EQUAL, "accessTokenValidity", "java.lang.Integer", "Float");
          return this;
      }

      public ArmsClientSQL andAccessTokenValidityGreaterThan(java.lang.Integer value) {
          addCriterion("access_token_validity", value, ConditionMode.GREATER_THEN, "accessTokenValidity", "java.lang.Integer", "Float");
          return this;
      }

      public ArmsClientSQL andAccessTokenValidityGreaterThanOrEqualTo(java.lang.Integer value) {
          addCriterion("access_token_validity", value, ConditionMode.GREATER_EQUAL, "accessTokenValidity", "java.lang.Integer", "Float");
          return this;
      }

      public ArmsClientSQL andAccessTokenValidityLessThan(java.lang.Integer value) {
          addCriterion("access_token_validity", value, ConditionMode.LESS_THEN, "accessTokenValidity", "java.lang.Integer", "Float");
          return this;
      }

      public ArmsClientSQL andAccessTokenValidityLessThanOrEqualTo(java.lang.Integer value) {
          addCriterion("access_token_validity", value, ConditionMode.LESS_EQUAL, "accessTokenValidity", "java.lang.Integer", "Float");
          return this;
      }

      public ArmsClientSQL andAccessTokenValidityBetween(java.lang.Integer value1, java.lang.Integer value2) {
    	  addCriterion("access_token_validity", value1, value2, ConditionMode.BETWEEN, "accessTokenValidity", "java.lang.Integer", "Float");
    	  return this;
      }

      public ArmsClientSQL andAccessTokenValidityNotBetween(java.lang.Integer value1, java.lang.Integer value2) {
          addCriterion("access_token_validity", value1, value2, ConditionMode.NOT_BETWEEN, "accessTokenValidity", "java.lang.Integer", "Float");
          return this;
      }
        
      public ArmsClientSQL andAccessTokenValidityIn(List<java.lang.Integer> values) {
          addCriterion("access_token_validity", values, ConditionMode.IN, "accessTokenValidity", "java.lang.Integer", "Float");
          return this;
      }

      public ArmsClientSQL andAccessTokenValidityNotIn(List<java.lang.Integer> values) {
          addCriterion("access_token_validity", values, ConditionMode.NOT_IN, "accessTokenValidity", "java.lang.Integer", "Float");
          return this;
      }
	public ArmsClientSQL andRefreshTokenValidityIsNull() {
		isnull("refresh_token_validity");
		return this;
	}
	
	public ArmsClientSQL andRefreshTokenValidityIsNotNull() {
		notNull("refresh_token_validity");
		return this;
	}
	
	public ArmsClientSQL andRefreshTokenValidityIsEmpty() {
		empty("refresh_token_validity");
		return this;
	}

	public ArmsClientSQL andRefreshTokenValidityIsNotEmpty() {
		notEmpty("refresh_token_validity");
		return this;
	}
      public ArmsClientSQL andRefreshTokenValidityEqualTo(java.lang.Integer value) {
          addCriterion("refresh_token_validity", value, ConditionMode.EQUAL, "refreshTokenValidity", "java.lang.Integer", "Float");
          return this;
      }

      public ArmsClientSQL andRefreshTokenValidityNotEqualTo(java.lang.Integer value) {
          addCriterion("refresh_token_validity", value, ConditionMode.NOT_EQUAL, "refreshTokenValidity", "java.lang.Integer", "Float");
          return this;
      }

      public ArmsClientSQL andRefreshTokenValidityGreaterThan(java.lang.Integer value) {
          addCriterion("refresh_token_validity", value, ConditionMode.GREATER_THEN, "refreshTokenValidity", "java.lang.Integer", "Float");
          return this;
      }

      public ArmsClientSQL andRefreshTokenValidityGreaterThanOrEqualTo(java.lang.Integer value) {
          addCriterion("refresh_token_validity", value, ConditionMode.GREATER_EQUAL, "refreshTokenValidity", "java.lang.Integer", "Float");
          return this;
      }

      public ArmsClientSQL andRefreshTokenValidityLessThan(java.lang.Integer value) {
          addCriterion("refresh_token_validity", value, ConditionMode.LESS_THEN, "refreshTokenValidity", "java.lang.Integer", "Float");
          return this;
      }

      public ArmsClientSQL andRefreshTokenValidityLessThanOrEqualTo(java.lang.Integer value) {
          addCriterion("refresh_token_validity", value, ConditionMode.LESS_EQUAL, "refreshTokenValidity", "java.lang.Integer", "Float");
          return this;
      }

      public ArmsClientSQL andRefreshTokenValidityBetween(java.lang.Integer value1, java.lang.Integer value2) {
    	  addCriterion("refresh_token_validity", value1, value2, ConditionMode.BETWEEN, "refreshTokenValidity", "java.lang.Integer", "Float");
    	  return this;
      }

      public ArmsClientSQL andRefreshTokenValidityNotBetween(java.lang.Integer value1, java.lang.Integer value2) {
          addCriterion("refresh_token_validity", value1, value2, ConditionMode.NOT_BETWEEN, "refreshTokenValidity", "java.lang.Integer", "Float");
          return this;
      }
        
      public ArmsClientSQL andRefreshTokenValidityIn(List<java.lang.Integer> values) {
          addCriterion("refresh_token_validity", values, ConditionMode.IN, "refreshTokenValidity", "java.lang.Integer", "Float");
          return this;
      }

      public ArmsClientSQL andRefreshTokenValidityNotIn(List<java.lang.Integer> values) {
          addCriterion("refresh_token_validity", values, ConditionMode.NOT_IN, "refreshTokenValidity", "java.lang.Integer", "Float");
          return this;
      }
	public ArmsClientSQL andAdditionalInformationIsNull() {
		isnull("additional_information");
		return this;
	}
	
	public ArmsClientSQL andAdditionalInformationIsNotNull() {
		notNull("additional_information");
		return this;
	}
	
	public ArmsClientSQL andAdditionalInformationIsEmpty() {
		empty("additional_information");
		return this;
	}

	public ArmsClientSQL andAdditionalInformationIsNotEmpty() {
		notEmpty("additional_information");
		return this;
	}
       public ArmsClientSQL andAdditionalInformationLike(java.lang.String value) {
    	   addCriterion("additional_information", value, ConditionMode.FUZZY, "additionalInformation", "java.lang.String", "Float");
    	   return this;
      }

      public ArmsClientSQL andAdditionalInformationNotLike(java.lang.String value) {
          addCriterion("additional_information", value, ConditionMode.NOT_FUZZY, "additionalInformation", "java.lang.String", "Float");
          return this;
      }
      public ArmsClientSQL andAdditionalInformationEqualTo(java.lang.String value) {
          addCriterion("additional_information", value, ConditionMode.EQUAL, "additionalInformation", "java.lang.String", "String");
          return this;
      }

      public ArmsClientSQL andAdditionalInformationNotEqualTo(java.lang.String value) {
          addCriterion("additional_information", value, ConditionMode.NOT_EQUAL, "additionalInformation", "java.lang.String", "String");
          return this;
      }

      public ArmsClientSQL andAdditionalInformationGreaterThan(java.lang.String value) {
          addCriterion("additional_information", value, ConditionMode.GREATER_THEN, "additionalInformation", "java.lang.String", "String");
          return this;
      }

      public ArmsClientSQL andAdditionalInformationGreaterThanOrEqualTo(java.lang.String value) {
          addCriterion("additional_information", value, ConditionMode.GREATER_EQUAL, "additionalInformation", "java.lang.String", "String");
          return this;
      }

      public ArmsClientSQL andAdditionalInformationLessThan(java.lang.String value) {
          addCriterion("additional_information", value, ConditionMode.LESS_THEN, "additionalInformation", "java.lang.String", "String");
          return this;
      }

      public ArmsClientSQL andAdditionalInformationLessThanOrEqualTo(java.lang.String value) {
          addCriterion("additional_information", value, ConditionMode.LESS_EQUAL, "additionalInformation", "java.lang.String", "String");
          return this;
      }

      public ArmsClientSQL andAdditionalInformationBetween(java.lang.String value1, java.lang.String value2) {
    	  addCriterion("additional_information", value1, value2, ConditionMode.BETWEEN, "additionalInformation", "java.lang.String", "String");
    	  return this;
      }

      public ArmsClientSQL andAdditionalInformationNotBetween(java.lang.String value1, java.lang.String value2) {
          addCriterion("additional_information", value1, value2, ConditionMode.NOT_BETWEEN, "additionalInformation", "java.lang.String", "String");
          return this;
      }
        
      public ArmsClientSQL andAdditionalInformationIn(List<java.lang.String> values) {
          addCriterion("additional_information", values, ConditionMode.IN, "additionalInformation", "java.lang.String", "String");
          return this;
      }

      public ArmsClientSQL andAdditionalInformationNotIn(List<java.lang.String> values) {
          addCriterion("additional_information", values, ConditionMode.NOT_IN, "additionalInformation", "java.lang.String", "String");
          return this;
      }
	public ArmsClientSQL andAutoapproveIsNull() {
		isnull("autoapprove");
		return this;
	}
	
	public ArmsClientSQL andAutoapproveIsNotNull() {
		notNull("autoapprove");
		return this;
	}
	
	public ArmsClientSQL andAutoapproveIsEmpty() {
		empty("autoapprove");
		return this;
	}

	public ArmsClientSQL andAutoapproveIsNotEmpty() {
		notEmpty("autoapprove");
		return this;
	}
       public ArmsClientSQL andAutoapproveLike(java.lang.String value) {
    	   addCriterion("autoapprove", value, ConditionMode.FUZZY, "autoapprove", "java.lang.String", "String");
    	   return this;
      }

      public ArmsClientSQL andAutoapproveNotLike(java.lang.String value) {
          addCriterion("autoapprove", value, ConditionMode.NOT_FUZZY, "autoapprove", "java.lang.String", "String");
          return this;
      }
      public ArmsClientSQL andAutoapproveEqualTo(java.lang.String value) {
          addCriterion("autoapprove", value, ConditionMode.EQUAL, "autoapprove", "java.lang.String", "String");
          return this;
      }

      public ArmsClientSQL andAutoapproveNotEqualTo(java.lang.String value) {
          addCriterion("autoapprove", value, ConditionMode.NOT_EQUAL, "autoapprove", "java.lang.String", "String");
          return this;
      }

      public ArmsClientSQL andAutoapproveGreaterThan(java.lang.String value) {
          addCriterion("autoapprove", value, ConditionMode.GREATER_THEN, "autoapprove", "java.lang.String", "String");
          return this;
      }

      public ArmsClientSQL andAutoapproveGreaterThanOrEqualTo(java.lang.String value) {
          addCriterion("autoapprove", value, ConditionMode.GREATER_EQUAL, "autoapprove", "java.lang.String", "String");
          return this;
      }

      public ArmsClientSQL andAutoapproveLessThan(java.lang.String value) {
          addCriterion("autoapprove", value, ConditionMode.LESS_THEN, "autoapprove", "java.lang.String", "String");
          return this;
      }

      public ArmsClientSQL andAutoapproveLessThanOrEqualTo(java.lang.String value) {
          addCriterion("autoapprove", value, ConditionMode.LESS_EQUAL, "autoapprove", "java.lang.String", "String");
          return this;
      }

      public ArmsClientSQL andAutoapproveBetween(java.lang.String value1, java.lang.String value2) {
    	  addCriterion("autoapprove", value1, value2, ConditionMode.BETWEEN, "autoapprove", "java.lang.String", "String");
    	  return this;
      }

      public ArmsClientSQL andAutoapproveNotBetween(java.lang.String value1, java.lang.String value2) {
          addCriterion("autoapprove", value1, value2, ConditionMode.NOT_BETWEEN, "autoapprove", "java.lang.String", "String");
          return this;
      }
        
      public ArmsClientSQL andAutoapproveIn(List<java.lang.String> values) {
          addCriterion("autoapprove", values, ConditionMode.IN, "autoapprove", "java.lang.String", "String");
          return this;
      }

      public ArmsClientSQL andAutoapproveNotIn(List<java.lang.String> values) {
          addCriterion("autoapprove", values, ConditionMode.NOT_IN, "autoapprove", "java.lang.String", "String");
          return this;
      }
	public ArmsClientSQL andCreateUserIsNull() {
		isnull("create_user");
		return this;
	}
	
	public ArmsClientSQL andCreateUserIsNotNull() {
		notNull("create_user");
		return this;
	}
	
	public ArmsClientSQL andCreateUserIsEmpty() {
		empty("create_user");
		return this;
	}

	public ArmsClientSQL andCreateUserIsNotEmpty() {
		notEmpty("create_user");
		return this;
	}
      public ArmsClientSQL andCreateUserEqualTo(java.lang.Long value) {
          addCriterion("create_user", value, ConditionMode.EQUAL, "createUser", "java.lang.Long", "Float");
          return this;
      }

      public ArmsClientSQL andCreateUserNotEqualTo(java.lang.Long value) {
          addCriterion("create_user", value, ConditionMode.NOT_EQUAL, "createUser", "java.lang.Long", "Float");
          return this;
      }

      public ArmsClientSQL andCreateUserGreaterThan(java.lang.Long value) {
          addCriterion("create_user", value, ConditionMode.GREATER_THEN, "createUser", "java.lang.Long", "Float");
          return this;
      }

      public ArmsClientSQL andCreateUserGreaterThanOrEqualTo(java.lang.Long value) {
          addCriterion("create_user", value, ConditionMode.GREATER_EQUAL, "createUser", "java.lang.Long", "Float");
          return this;
      }

      public ArmsClientSQL andCreateUserLessThan(java.lang.Long value) {
          addCriterion("create_user", value, ConditionMode.LESS_THEN, "createUser", "java.lang.Long", "Float");
          return this;
      }

      public ArmsClientSQL andCreateUserLessThanOrEqualTo(java.lang.Long value) {
          addCriterion("create_user", value, ConditionMode.LESS_EQUAL, "createUser", "java.lang.Long", "Float");
          return this;
      }

      public ArmsClientSQL andCreateUserBetween(java.lang.Long value1, java.lang.Long value2) {
    	  addCriterion("create_user", value1, value2, ConditionMode.BETWEEN, "createUser", "java.lang.Long", "Float");
    	  return this;
      }

      public ArmsClientSQL andCreateUserNotBetween(java.lang.Long value1, java.lang.Long value2) {
          addCriterion("create_user", value1, value2, ConditionMode.NOT_BETWEEN, "createUser", "java.lang.Long", "Float");
          return this;
      }
        
      public ArmsClientSQL andCreateUserIn(List<java.lang.Long> values) {
          addCriterion("create_user", values, ConditionMode.IN, "createUser", "java.lang.Long", "Float");
          return this;
      }

      public ArmsClientSQL andCreateUserNotIn(List<java.lang.Long> values) {
          addCriterion("create_user", values, ConditionMode.NOT_IN, "createUser", "java.lang.Long", "Float");
          return this;
      }
	public ArmsClientSQL andCreateTimeIsNull() {
		isnull("create_time");
		return this;
	}
	
	public ArmsClientSQL andCreateTimeIsNotNull() {
		notNull("create_time");
		return this;
	}
	
	public ArmsClientSQL andCreateTimeIsEmpty() {
		empty("create_time");
		return this;
	}

	public ArmsClientSQL andCreateTimeIsNotEmpty() {
		notEmpty("create_time");
		return this;
	}
      public ArmsClientSQL andCreateTimeEqualTo(java.util.Date value) {
          addCriterion("create_time", value, ConditionMode.EQUAL, "createTime", "java.util.Date", "String");
          return this;
      }

      public ArmsClientSQL andCreateTimeNotEqualTo(java.util.Date value) {
          addCriterion("create_time", value, ConditionMode.NOT_EQUAL, "createTime", "java.util.Date", "String");
          return this;
      }

      public ArmsClientSQL andCreateTimeGreaterThan(java.util.Date value) {
          addCriterion("create_time", value, ConditionMode.GREATER_THEN, "createTime", "java.util.Date", "String");
          return this;
      }

      public ArmsClientSQL andCreateTimeGreaterThanOrEqualTo(java.util.Date value) {
          addCriterion("create_time", value, ConditionMode.GREATER_EQUAL, "createTime", "java.util.Date", "String");
          return this;
      }

      public ArmsClientSQL andCreateTimeLessThan(java.util.Date value) {
          addCriterion("create_time", value, ConditionMode.LESS_THEN, "createTime", "java.util.Date", "String");
          return this;
      }

      public ArmsClientSQL andCreateTimeLessThanOrEqualTo(java.util.Date value) {
          addCriterion("create_time", value, ConditionMode.LESS_EQUAL, "createTime", "java.util.Date", "String");
          return this;
      }

      public ArmsClientSQL andCreateTimeBetween(java.util.Date value1, java.util.Date value2) {
    	  addCriterion("create_time", value1, value2, ConditionMode.BETWEEN, "createTime", "java.util.Date", "String");
    	  return this;
      }

      public ArmsClientSQL andCreateTimeNotBetween(java.util.Date value1, java.util.Date value2) {
          addCriterion("create_time", value1, value2, ConditionMode.NOT_BETWEEN, "createTime", "java.util.Date", "String");
          return this;
      }
        
      public ArmsClientSQL andCreateTimeIn(List<java.util.Date> values) {
          addCriterion("create_time", values, ConditionMode.IN, "createTime", "java.util.Date", "String");
          return this;
      }

      public ArmsClientSQL andCreateTimeNotIn(List<java.util.Date> values) {
          addCriterion("create_time", values, ConditionMode.NOT_IN, "createTime", "java.util.Date", "String");
          return this;
      }
	public ArmsClientSQL andUpdateUserIsNull() {
		isnull("update_user");
		return this;
	}
	
	public ArmsClientSQL andUpdateUserIsNotNull() {
		notNull("update_user");
		return this;
	}
	
	public ArmsClientSQL andUpdateUserIsEmpty() {
		empty("update_user");
		return this;
	}

	public ArmsClientSQL andUpdateUserIsNotEmpty() {
		notEmpty("update_user");
		return this;
	}
      public ArmsClientSQL andUpdateUserEqualTo(java.lang.Long value) {
          addCriterion("update_user", value, ConditionMode.EQUAL, "updateUser", "java.lang.Long", "Float");
          return this;
      }

      public ArmsClientSQL andUpdateUserNotEqualTo(java.lang.Long value) {
          addCriterion("update_user", value, ConditionMode.NOT_EQUAL, "updateUser", "java.lang.Long", "Float");
          return this;
      }

      public ArmsClientSQL andUpdateUserGreaterThan(java.lang.Long value) {
          addCriterion("update_user", value, ConditionMode.GREATER_THEN, "updateUser", "java.lang.Long", "Float");
          return this;
      }

      public ArmsClientSQL andUpdateUserGreaterThanOrEqualTo(java.lang.Long value) {
          addCriterion("update_user", value, ConditionMode.GREATER_EQUAL, "updateUser", "java.lang.Long", "Float");
          return this;
      }

      public ArmsClientSQL andUpdateUserLessThan(java.lang.Long value) {
          addCriterion("update_user", value, ConditionMode.LESS_THEN, "updateUser", "java.lang.Long", "Float");
          return this;
      }

      public ArmsClientSQL andUpdateUserLessThanOrEqualTo(java.lang.Long value) {
          addCriterion("update_user", value, ConditionMode.LESS_EQUAL, "updateUser", "java.lang.Long", "Float");
          return this;
      }

      public ArmsClientSQL andUpdateUserBetween(java.lang.Long value1, java.lang.Long value2) {
    	  addCriterion("update_user", value1, value2, ConditionMode.BETWEEN, "updateUser", "java.lang.Long", "Float");
    	  return this;
      }

      public ArmsClientSQL andUpdateUserNotBetween(java.lang.Long value1, java.lang.Long value2) {
          addCriterion("update_user", value1, value2, ConditionMode.NOT_BETWEEN, "updateUser", "java.lang.Long", "Float");
          return this;
      }
        
      public ArmsClientSQL andUpdateUserIn(List<java.lang.Long> values) {
          addCriterion("update_user", values, ConditionMode.IN, "updateUser", "java.lang.Long", "Float");
          return this;
      }

      public ArmsClientSQL andUpdateUserNotIn(List<java.lang.Long> values) {
          addCriterion("update_user", values, ConditionMode.NOT_IN, "updateUser", "java.lang.Long", "Float");
          return this;
      }
	public ArmsClientSQL andClientStatusIsNull() {
		isnull("client_status");
		return this;
	}
	
	public ArmsClientSQL andClientStatusIsNotNull() {
		notNull("client_status");
		return this;
	}
	
	public ArmsClientSQL andClientStatusIsEmpty() {
		empty("client_status");
		return this;
	}

	public ArmsClientSQL andClientStatusIsNotEmpty() {
		notEmpty("client_status");
		return this;
	}
      public ArmsClientSQL andClientStatusEqualTo(java.lang.Integer value) {
          addCriterion("client_status", value, ConditionMode.EQUAL, "clientStatus", "java.lang.Integer", "Float");
          return this;
      }

      public ArmsClientSQL andClientStatusNotEqualTo(java.lang.Integer value) {
          addCriterion("client_status", value, ConditionMode.NOT_EQUAL, "clientStatus", "java.lang.Integer", "Float");
          return this;
      }

      public ArmsClientSQL andClientStatusGreaterThan(java.lang.Integer value) {
          addCriterion("client_status", value, ConditionMode.GREATER_THEN, "clientStatus", "java.lang.Integer", "Float");
          return this;
      }

      public ArmsClientSQL andClientStatusGreaterThanOrEqualTo(java.lang.Integer value) {
          addCriterion("client_status", value, ConditionMode.GREATER_EQUAL, "clientStatus", "java.lang.Integer", "Float");
          return this;
      }

      public ArmsClientSQL andClientStatusLessThan(java.lang.Integer value) {
          addCriterion("client_status", value, ConditionMode.LESS_THEN, "clientStatus", "java.lang.Integer", "Float");
          return this;
      }

      public ArmsClientSQL andClientStatusLessThanOrEqualTo(java.lang.Integer value) {
          addCriterion("client_status", value, ConditionMode.LESS_EQUAL, "clientStatus", "java.lang.Integer", "Float");
          return this;
      }

      public ArmsClientSQL andClientStatusBetween(java.lang.Integer value1, java.lang.Integer value2) {
    	  addCriterion("client_status", value1, value2, ConditionMode.BETWEEN, "clientStatus", "java.lang.Integer", "Float");
    	  return this;
      }

      public ArmsClientSQL andClientStatusNotBetween(java.lang.Integer value1, java.lang.Integer value2) {
          addCriterion("client_status", value1, value2, ConditionMode.NOT_BETWEEN, "clientStatus", "java.lang.Integer", "Float");
          return this;
      }
        
      public ArmsClientSQL andClientStatusIn(List<java.lang.Integer> values) {
          addCriterion("client_status", values, ConditionMode.IN, "clientStatus", "java.lang.Integer", "Float");
          return this;
      }

      public ArmsClientSQL andClientStatusNotIn(List<java.lang.Integer> values) {
          addCriterion("client_status", values, ConditionMode.NOT_IN, "clientStatus", "java.lang.Integer", "Float");
          return this;
      }
	public ArmsClientSQL andIsDeletedIsNull() {
		isnull("is_deleted");
		return this;
	}
	
	public ArmsClientSQL andIsDeletedIsNotNull() {
		notNull("is_deleted");
		return this;
	}
	
	public ArmsClientSQL andIsDeletedIsEmpty() {
		empty("is_deleted");
		return this;
	}

	public ArmsClientSQL andIsDeletedIsNotEmpty() {
		notEmpty("is_deleted");
		return this;
	}
      public ArmsClientSQL andIsDeletedEqualTo(java.lang.Integer value) {
          addCriterion("is_deleted", value, ConditionMode.EQUAL, "isDeleted", "java.lang.Integer", "Float");
          return this;
      }

      public ArmsClientSQL andIsDeletedNotEqualTo(java.lang.Integer value) {
          addCriterion("is_deleted", value, ConditionMode.NOT_EQUAL, "isDeleted", "java.lang.Integer", "Float");
          return this;
      }

      public ArmsClientSQL andIsDeletedGreaterThan(java.lang.Integer value) {
          addCriterion("is_deleted", value, ConditionMode.GREATER_THEN, "isDeleted", "java.lang.Integer", "Float");
          return this;
      }

      public ArmsClientSQL andIsDeletedGreaterThanOrEqualTo(java.lang.Integer value) {
          addCriterion("is_deleted", value, ConditionMode.GREATER_EQUAL, "isDeleted", "java.lang.Integer", "Float");
          return this;
      }

      public ArmsClientSQL andIsDeletedLessThan(java.lang.Integer value) {
          addCriterion("is_deleted", value, ConditionMode.LESS_THEN, "isDeleted", "java.lang.Integer", "Float");
          return this;
      }

      public ArmsClientSQL andIsDeletedLessThanOrEqualTo(java.lang.Integer value) {
          addCriterion("is_deleted", value, ConditionMode.LESS_EQUAL, "isDeleted", "java.lang.Integer", "Float");
          return this;
      }

      public ArmsClientSQL andIsDeletedBetween(java.lang.Integer value1, java.lang.Integer value2) {
    	  addCriterion("is_deleted", value1, value2, ConditionMode.BETWEEN, "isDeleted", "java.lang.Integer", "Float");
    	  return this;
      }

      public ArmsClientSQL andIsDeletedNotBetween(java.lang.Integer value1, java.lang.Integer value2) {
          addCriterion("is_deleted", value1, value2, ConditionMode.NOT_BETWEEN, "isDeleted", "java.lang.Integer", "Float");
          return this;
      }
        
      public ArmsClientSQL andIsDeletedIn(List<java.lang.Integer> values) {
          addCriterion("is_deleted", values, ConditionMode.IN, "isDeleted", "java.lang.Integer", "Float");
          return this;
      }

      public ArmsClientSQL andIsDeletedNotIn(List<java.lang.Integer> values) {
          addCriterion("is_deleted", values, ConditionMode.NOT_IN, "isDeleted", "java.lang.Integer", "Float");
          return this;
      }
	public ArmsClientSQL andTenantIdIsNull() {
		isnull("tenant_id");
		return this;
	}
	
	public ArmsClientSQL andTenantIdIsNotNull() {
		notNull("tenant_id");
		return this;
	}
	
	public ArmsClientSQL andTenantIdIsEmpty() {
		empty("tenant_id");
		return this;
	}

	public ArmsClientSQL andTenantIdIsNotEmpty() {
		notEmpty("tenant_id");
		return this;
	}
       public ArmsClientSQL andTenantIdLike(java.lang.String value) {
    	   addCriterion("tenant_id", value, ConditionMode.FUZZY, "tenantId", "java.lang.String", "Float");
    	   return this;
      }

      public ArmsClientSQL andTenantIdNotLike(java.lang.String value) {
          addCriterion("tenant_id", value, ConditionMode.NOT_FUZZY, "tenantId", "java.lang.String", "Float");
          return this;
      }
      public ArmsClientSQL andTenantIdEqualTo(java.lang.String value) {
          addCriterion("tenant_id", value, ConditionMode.EQUAL, "tenantId", "java.lang.String", "String");
          return this;
      }

      public ArmsClientSQL andTenantIdNotEqualTo(java.lang.String value) {
          addCriterion("tenant_id", value, ConditionMode.NOT_EQUAL, "tenantId", "java.lang.String", "String");
          return this;
      }

      public ArmsClientSQL andTenantIdGreaterThan(java.lang.String value) {
          addCriterion("tenant_id", value, ConditionMode.GREATER_THEN, "tenantId", "java.lang.String", "String");
          return this;
      }

      public ArmsClientSQL andTenantIdGreaterThanOrEqualTo(java.lang.String value) {
          addCriterion("tenant_id", value, ConditionMode.GREATER_EQUAL, "tenantId", "java.lang.String", "String");
          return this;
      }

      public ArmsClientSQL andTenantIdLessThan(java.lang.String value) {
          addCriterion("tenant_id", value, ConditionMode.LESS_THEN, "tenantId", "java.lang.String", "String");
          return this;
      }

      public ArmsClientSQL andTenantIdLessThanOrEqualTo(java.lang.String value) {
          addCriterion("tenant_id", value, ConditionMode.LESS_EQUAL, "tenantId", "java.lang.String", "String");
          return this;
      }

      public ArmsClientSQL andTenantIdBetween(java.lang.String value1, java.lang.String value2) {
    	  addCriterion("tenant_id", value1, value2, ConditionMode.BETWEEN, "tenantId", "java.lang.String", "String");
    	  return this;
      }

      public ArmsClientSQL andTenantIdNotBetween(java.lang.String value1, java.lang.String value2) {
          addCriterion("tenant_id", value1, value2, ConditionMode.NOT_BETWEEN, "tenantId", "java.lang.String", "String");
          return this;
      }
        
      public ArmsClientSQL andTenantIdIn(List<java.lang.String> values) {
          addCriterion("tenant_id", values, ConditionMode.IN, "tenantId", "java.lang.String", "String");
          return this;
      }

      public ArmsClientSQL andTenantIdNotIn(List<java.lang.String> values) {
          addCriterion("tenant_id", values, ConditionMode.NOT_IN, "tenantId", "java.lang.String", "String");
          return this;
      }
}