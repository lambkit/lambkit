/**
 * Copyright (c) 2015-2017, Henry Yang 杨勇 (gismail@foxmail.com).
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.lambkit.module.arms.sql;

import com.lambkit.db.sql.ConditionMode;
import com.lambkit.db.sql.Column;
import com.lambkit.db.sql.Columns;
import com.lambkit.db.sql.Example;

import java.io.Serializable;
import java.util.List;

/**
 * @author yangyong 
 * @website: www.lambkit.com
 * @email: gismail@foxmail.com
 * @date ${date}
 * @version 1.0
 * @since 1.0
 */

public class ArmsTenantSQL extends Columns implements Serializable {
	private static final long serialVersionUID = 1L;
	
	public static ArmsTenantSQL of() {
		return new ArmsTenantSQL();
	}
	
	public static ArmsTenantSQL by(Column column) {
		ArmsTenantSQL that = new ArmsTenantSQL();
		that.add(column);
        return that;
    }

    public static ArmsTenantSQL by(String name, Object value) {
        return (ArmsTenantSQL) create().eq(name, value);
    }
    
    public Example example() {
    	return Example.create("arms_tenant", this);
    }
    
    /**
     * equals
     *
     * @param name
     * @param value
     * @return
     */
    public ArmsTenantSQL eq(String name, Object value) {
    	super.eq(name, value);
        return this;
    }

    /**
     * not equals !=
     *
     * @param name
     * @param value
     * @return
     */
    public ArmsTenantSQL ne(String name, Object value) {
    	super.ne(name, value);
        return this;
    }


    /**
     * like
     *
     * @param name
     * @param value
     * @return
     */

    public ArmsTenantSQL like(String name, Object value) {
    	super.like(name, value);
        return this;
    }
    
    public ArmsTenantSQL notLike(String name, Object value) {
    	super.notLike(name, value);
        return this;
    }

    /**
     * 大于 great than
     *
     * @param name
     * @param value
     * @return
     */
    public ArmsTenantSQL gt(String name, Object value) {
    	super.gt(name, value);
        return this;
    }

    /**
     * 大于等于 great or equal
     *
     * @param name
     * @param value
     * @return
     */
    public ArmsTenantSQL ge(String name, Object value) {
    	super.ge(name, value);
        return this;
    }

    /**
     * 小于 less than
     *
     * @param name
     * @param value
     * @return
     */
    public ArmsTenantSQL lt(String name, Object value) {
    	super.lt(name, value);
        return this;
    }

    /**
     * 小于等于 less or equal
     *
     * @param name
     * @param value
     * @return
     */
    public ArmsTenantSQL le(String name, Object value) {
    	super.le(name, value);
        return this;
    }
    
    public ArmsTenantSQL isnull(String name) {
    	super.isnull(name);
        return this;
    } 

    public ArmsTenantSQL notNull(String name) {
    	super.notNull(name);
        return this;
    } 
    
    public ArmsTenantSQL empty(String name) {
    	super.empty(name);
        return this;
    } 
    
    public ArmsTenantSQL notEmpty(String name) {
    	super.notEmpty(name);
        return this;
    } 
    
    public ArmsTenantSQL add(Column column) {
    	super.add(column);
    	return this;
    }
    
    /**************************/
	
	public void addCriterion(String name, Object value, ConditionMode logic, String property, String typeHandler, String valueType) {
		 if (value == null) {
			 throw new RuntimeException("Value for " + property + " cannot be null");
		 }
		 add(Column.create(name, value, logic, typeHandler, valueType));
	}
   
	public void addCriterion(String name, Object value1, Object value2, ConditionMode logic, String property, String typeHandler, String valueType) {
		 if (value1 == null || value2 == null) {
			 throw new RuntimeException("Between values for " + property + " cannot be null");
		 }
		 add(Column.create(name, value1, value2, logic, typeHandler, valueType));
	}
		 
	public ArmsTenantSQL andIdIsNull() {
		isnull("id");
		return this;
	}
	
	public ArmsTenantSQL andIdIsNotNull() {
		notNull("id");
		return this;
	}
	
	public ArmsTenantSQL andIdIsEmpty() {
		empty("id");
		return this;
	}

	public ArmsTenantSQL andIdIsNotEmpty() {
		notEmpty("id");
		return this;
	}
      public ArmsTenantSQL andIdEqualTo(java.lang.Long value) {
          addCriterion("id", value, ConditionMode.EQUAL, "id", "java.lang.Long", "Float");
          return this;
      }

      public ArmsTenantSQL andIdNotEqualTo(java.lang.Long value) {
          addCriterion("id", value, ConditionMode.NOT_EQUAL, "id", "java.lang.Long", "Float");
          return this;
      }

      public ArmsTenantSQL andIdGreaterThan(java.lang.Long value) {
          addCriterion("id", value, ConditionMode.GREATER_THEN, "id", "java.lang.Long", "Float");
          return this;
      }

      public ArmsTenantSQL andIdGreaterThanOrEqualTo(java.lang.Long value) {
          addCriterion("id", value, ConditionMode.GREATER_EQUAL, "id", "java.lang.Long", "Float");
          return this;
      }

      public ArmsTenantSQL andIdLessThan(java.lang.Long value) {
          addCriterion("id", value, ConditionMode.LESS_THEN, "id", "java.lang.Long", "Float");
          return this;
      }

      public ArmsTenantSQL andIdLessThanOrEqualTo(java.lang.Long value) {
          addCriterion("id", value, ConditionMode.LESS_EQUAL, "id", "java.lang.Long", "Float");
          return this;
      }

      public ArmsTenantSQL andIdBetween(java.lang.Long value1, java.lang.Long value2) {
    	  addCriterion("id", value1, value2, ConditionMode.BETWEEN, "id", "java.lang.Long", "Float");
    	  return this;
      }

      public ArmsTenantSQL andIdNotBetween(java.lang.Long value1, java.lang.Long value2) {
          addCriterion("id", value1, value2, ConditionMode.NOT_BETWEEN, "id", "java.lang.Long", "Float");
          return this;
      }
        
      public ArmsTenantSQL andIdIn(List<java.lang.Long> values) {
          addCriterion("id", values, ConditionMode.IN, "id", "java.lang.Long", "Float");
          return this;
      }

      public ArmsTenantSQL andIdNotIn(List<java.lang.Long> values) {
          addCriterion("id", values, ConditionMode.NOT_IN, "id", "java.lang.Long", "Float");
          return this;
      }
	public ArmsTenantSQL andTenantIdIsNull() {
		isnull("tenant_id");
		return this;
	}
	
	public ArmsTenantSQL andTenantIdIsNotNull() {
		notNull("tenant_id");
		return this;
	}
	
	public ArmsTenantSQL andTenantIdIsEmpty() {
		empty("tenant_id");
		return this;
	}

	public ArmsTenantSQL andTenantIdIsNotEmpty() {
		notEmpty("tenant_id");
		return this;
	}
       public ArmsTenantSQL andTenantIdLike(java.lang.String value) {
    	   addCriterion("tenant_id", value, ConditionMode.FUZZY, "tenantId", "java.lang.String", "Float");
    	   return this;
      }

      public ArmsTenantSQL andTenantIdNotLike(java.lang.String value) {
          addCriterion("tenant_id", value, ConditionMode.NOT_FUZZY, "tenantId", "java.lang.String", "Float");
          return this;
      }
      public ArmsTenantSQL andTenantIdEqualTo(java.lang.String value) {
          addCriterion("tenant_id", value, ConditionMode.EQUAL, "tenantId", "java.lang.String", "String");
          return this;
      }

      public ArmsTenantSQL andTenantIdNotEqualTo(java.lang.String value) {
          addCriterion("tenant_id", value, ConditionMode.NOT_EQUAL, "tenantId", "java.lang.String", "String");
          return this;
      }

      public ArmsTenantSQL andTenantIdGreaterThan(java.lang.String value) {
          addCriterion("tenant_id", value, ConditionMode.GREATER_THEN, "tenantId", "java.lang.String", "String");
          return this;
      }

      public ArmsTenantSQL andTenantIdGreaterThanOrEqualTo(java.lang.String value) {
          addCriterion("tenant_id", value, ConditionMode.GREATER_EQUAL, "tenantId", "java.lang.String", "String");
          return this;
      }

      public ArmsTenantSQL andTenantIdLessThan(java.lang.String value) {
          addCriterion("tenant_id", value, ConditionMode.LESS_THEN, "tenantId", "java.lang.String", "String");
          return this;
      }

      public ArmsTenantSQL andTenantIdLessThanOrEqualTo(java.lang.String value) {
          addCriterion("tenant_id", value, ConditionMode.LESS_EQUAL, "tenantId", "java.lang.String", "String");
          return this;
      }

      public ArmsTenantSQL andTenantIdBetween(java.lang.String value1, java.lang.String value2) {
    	  addCriterion("tenant_id", value1, value2, ConditionMode.BETWEEN, "tenantId", "java.lang.String", "String");
    	  return this;
      }

      public ArmsTenantSQL andTenantIdNotBetween(java.lang.String value1, java.lang.String value2) {
          addCriterion("tenant_id", value1, value2, ConditionMode.NOT_BETWEEN, "tenantId", "java.lang.String", "String");
          return this;
      }
        
      public ArmsTenantSQL andTenantIdIn(List<java.lang.String> values) {
          addCriterion("tenant_id", values, ConditionMode.IN, "tenantId", "java.lang.String", "String");
          return this;
      }

      public ArmsTenantSQL andTenantIdNotIn(List<java.lang.String> values) {
          addCriterion("tenant_id", values, ConditionMode.NOT_IN, "tenantId", "java.lang.String", "String");
          return this;
      }
	public ArmsTenantSQL andTenantNameIsNull() {
		isnull("tenant_name");
		return this;
	}
	
	public ArmsTenantSQL andTenantNameIsNotNull() {
		notNull("tenant_name");
		return this;
	}
	
	public ArmsTenantSQL andTenantNameIsEmpty() {
		empty("tenant_name");
		return this;
	}

	public ArmsTenantSQL andTenantNameIsNotEmpty() {
		notEmpty("tenant_name");
		return this;
	}
       public ArmsTenantSQL andTenantNameLike(java.lang.String value) {
    	   addCriterion("tenant_name", value, ConditionMode.FUZZY, "tenantName", "java.lang.String", "String");
    	   return this;
      }

      public ArmsTenantSQL andTenantNameNotLike(java.lang.String value) {
          addCriterion("tenant_name", value, ConditionMode.NOT_FUZZY, "tenantName", "java.lang.String", "String");
          return this;
      }
      public ArmsTenantSQL andTenantNameEqualTo(java.lang.String value) {
          addCriterion("tenant_name", value, ConditionMode.EQUAL, "tenantName", "java.lang.String", "String");
          return this;
      }

      public ArmsTenantSQL andTenantNameNotEqualTo(java.lang.String value) {
          addCriterion("tenant_name", value, ConditionMode.NOT_EQUAL, "tenantName", "java.lang.String", "String");
          return this;
      }

      public ArmsTenantSQL andTenantNameGreaterThan(java.lang.String value) {
          addCriterion("tenant_name", value, ConditionMode.GREATER_THEN, "tenantName", "java.lang.String", "String");
          return this;
      }

      public ArmsTenantSQL andTenantNameGreaterThanOrEqualTo(java.lang.String value) {
          addCriterion("tenant_name", value, ConditionMode.GREATER_EQUAL, "tenantName", "java.lang.String", "String");
          return this;
      }

      public ArmsTenantSQL andTenantNameLessThan(java.lang.String value) {
          addCriterion("tenant_name", value, ConditionMode.LESS_THEN, "tenantName", "java.lang.String", "String");
          return this;
      }

      public ArmsTenantSQL andTenantNameLessThanOrEqualTo(java.lang.String value) {
          addCriterion("tenant_name", value, ConditionMode.LESS_EQUAL, "tenantName", "java.lang.String", "String");
          return this;
      }

      public ArmsTenantSQL andTenantNameBetween(java.lang.String value1, java.lang.String value2) {
    	  addCriterion("tenant_name", value1, value2, ConditionMode.BETWEEN, "tenantName", "java.lang.String", "String");
    	  return this;
      }

      public ArmsTenantSQL andTenantNameNotBetween(java.lang.String value1, java.lang.String value2) {
          addCriterion("tenant_name", value1, value2, ConditionMode.NOT_BETWEEN, "tenantName", "java.lang.String", "String");
          return this;
      }
        
      public ArmsTenantSQL andTenantNameIn(List<java.lang.String> values) {
          addCriterion("tenant_name", values, ConditionMode.IN, "tenantName", "java.lang.String", "String");
          return this;
      }

      public ArmsTenantSQL andTenantNameNotIn(List<java.lang.String> values) {
          addCriterion("tenant_name", values, ConditionMode.NOT_IN, "tenantName", "java.lang.String", "String");
          return this;
      }
	public ArmsTenantSQL andDomainIsNull() {
		isnull("domain");
		return this;
	}
	
	public ArmsTenantSQL andDomainIsNotNull() {
		notNull("domain");
		return this;
	}
	
	public ArmsTenantSQL andDomainIsEmpty() {
		empty("domain");
		return this;
	}

	public ArmsTenantSQL andDomainIsNotEmpty() {
		notEmpty("domain");
		return this;
	}
       public ArmsTenantSQL andDomainLike(java.lang.String value) {
    	   addCriterion("domain", value, ConditionMode.FUZZY, "domain", "java.lang.String", "String");
    	   return this;
      }

      public ArmsTenantSQL andDomainNotLike(java.lang.String value) {
          addCriterion("domain", value, ConditionMode.NOT_FUZZY, "domain", "java.lang.String", "String");
          return this;
      }
      public ArmsTenantSQL andDomainEqualTo(java.lang.String value) {
          addCriterion("domain", value, ConditionMode.EQUAL, "domain", "java.lang.String", "String");
          return this;
      }

      public ArmsTenantSQL andDomainNotEqualTo(java.lang.String value) {
          addCriterion("domain", value, ConditionMode.NOT_EQUAL, "domain", "java.lang.String", "String");
          return this;
      }

      public ArmsTenantSQL andDomainGreaterThan(java.lang.String value) {
          addCriterion("domain", value, ConditionMode.GREATER_THEN, "domain", "java.lang.String", "String");
          return this;
      }

      public ArmsTenantSQL andDomainGreaterThanOrEqualTo(java.lang.String value) {
          addCriterion("domain", value, ConditionMode.GREATER_EQUAL, "domain", "java.lang.String", "String");
          return this;
      }

      public ArmsTenantSQL andDomainLessThan(java.lang.String value) {
          addCriterion("domain", value, ConditionMode.LESS_THEN, "domain", "java.lang.String", "String");
          return this;
      }

      public ArmsTenantSQL andDomainLessThanOrEqualTo(java.lang.String value) {
          addCriterion("domain", value, ConditionMode.LESS_EQUAL, "domain", "java.lang.String", "String");
          return this;
      }

      public ArmsTenantSQL andDomainBetween(java.lang.String value1, java.lang.String value2) {
    	  addCriterion("domain", value1, value2, ConditionMode.BETWEEN, "domain", "java.lang.String", "String");
    	  return this;
      }

      public ArmsTenantSQL andDomainNotBetween(java.lang.String value1, java.lang.String value2) {
          addCriterion("domain", value1, value2, ConditionMode.NOT_BETWEEN, "domain", "java.lang.String", "String");
          return this;
      }
        
      public ArmsTenantSQL andDomainIn(List<java.lang.String> values) {
          addCriterion("domain", values, ConditionMode.IN, "domain", "java.lang.String", "String");
          return this;
      }

      public ArmsTenantSQL andDomainNotIn(List<java.lang.String> values) {
          addCriterion("domain", values, ConditionMode.NOT_IN, "domain", "java.lang.String", "String");
          return this;
      }
	public ArmsTenantSQL andLinkmanIsNull() {
		isnull("linkman");
		return this;
	}
	
	public ArmsTenantSQL andLinkmanIsNotNull() {
		notNull("linkman");
		return this;
	}
	
	public ArmsTenantSQL andLinkmanIsEmpty() {
		empty("linkman");
		return this;
	}

	public ArmsTenantSQL andLinkmanIsNotEmpty() {
		notEmpty("linkman");
		return this;
	}
       public ArmsTenantSQL andLinkmanLike(java.lang.String value) {
    	   addCriterion("linkman", value, ConditionMode.FUZZY, "linkman", "java.lang.String", "String");
    	   return this;
      }

      public ArmsTenantSQL andLinkmanNotLike(java.lang.String value) {
          addCriterion("linkman", value, ConditionMode.NOT_FUZZY, "linkman", "java.lang.String", "String");
          return this;
      }
      public ArmsTenantSQL andLinkmanEqualTo(java.lang.String value) {
          addCriterion("linkman", value, ConditionMode.EQUAL, "linkman", "java.lang.String", "String");
          return this;
      }

      public ArmsTenantSQL andLinkmanNotEqualTo(java.lang.String value) {
          addCriterion("linkman", value, ConditionMode.NOT_EQUAL, "linkman", "java.lang.String", "String");
          return this;
      }

      public ArmsTenantSQL andLinkmanGreaterThan(java.lang.String value) {
          addCriterion("linkman", value, ConditionMode.GREATER_THEN, "linkman", "java.lang.String", "String");
          return this;
      }

      public ArmsTenantSQL andLinkmanGreaterThanOrEqualTo(java.lang.String value) {
          addCriterion("linkman", value, ConditionMode.GREATER_EQUAL, "linkman", "java.lang.String", "String");
          return this;
      }

      public ArmsTenantSQL andLinkmanLessThan(java.lang.String value) {
          addCriterion("linkman", value, ConditionMode.LESS_THEN, "linkman", "java.lang.String", "String");
          return this;
      }

      public ArmsTenantSQL andLinkmanLessThanOrEqualTo(java.lang.String value) {
          addCriterion("linkman", value, ConditionMode.LESS_EQUAL, "linkman", "java.lang.String", "String");
          return this;
      }

      public ArmsTenantSQL andLinkmanBetween(java.lang.String value1, java.lang.String value2) {
    	  addCriterion("linkman", value1, value2, ConditionMode.BETWEEN, "linkman", "java.lang.String", "String");
    	  return this;
      }

      public ArmsTenantSQL andLinkmanNotBetween(java.lang.String value1, java.lang.String value2) {
          addCriterion("linkman", value1, value2, ConditionMode.NOT_BETWEEN, "linkman", "java.lang.String", "String");
          return this;
      }
        
      public ArmsTenantSQL andLinkmanIn(List<java.lang.String> values) {
          addCriterion("linkman", values, ConditionMode.IN, "linkman", "java.lang.String", "String");
          return this;
      }

      public ArmsTenantSQL andLinkmanNotIn(List<java.lang.String> values) {
          addCriterion("linkman", values, ConditionMode.NOT_IN, "linkman", "java.lang.String", "String");
          return this;
      }
	public ArmsTenantSQL andContactNumberIsNull() {
		isnull("contact_number");
		return this;
	}
	
	public ArmsTenantSQL andContactNumberIsNotNull() {
		notNull("contact_number");
		return this;
	}
	
	public ArmsTenantSQL andContactNumberIsEmpty() {
		empty("contact_number");
		return this;
	}

	public ArmsTenantSQL andContactNumberIsNotEmpty() {
		notEmpty("contact_number");
		return this;
	}
       public ArmsTenantSQL andContactNumberLike(java.lang.String value) {
    	   addCriterion("contact_number", value, ConditionMode.FUZZY, "contactNumber", "java.lang.String", "String");
    	   return this;
      }

      public ArmsTenantSQL andContactNumberNotLike(java.lang.String value) {
          addCriterion("contact_number", value, ConditionMode.NOT_FUZZY, "contactNumber", "java.lang.String", "String");
          return this;
      }
      public ArmsTenantSQL andContactNumberEqualTo(java.lang.String value) {
          addCriterion("contact_number", value, ConditionMode.EQUAL, "contactNumber", "java.lang.String", "String");
          return this;
      }

      public ArmsTenantSQL andContactNumberNotEqualTo(java.lang.String value) {
          addCriterion("contact_number", value, ConditionMode.NOT_EQUAL, "contactNumber", "java.lang.String", "String");
          return this;
      }

      public ArmsTenantSQL andContactNumberGreaterThan(java.lang.String value) {
          addCriterion("contact_number", value, ConditionMode.GREATER_THEN, "contactNumber", "java.lang.String", "String");
          return this;
      }

      public ArmsTenantSQL andContactNumberGreaterThanOrEqualTo(java.lang.String value) {
          addCriterion("contact_number", value, ConditionMode.GREATER_EQUAL, "contactNumber", "java.lang.String", "String");
          return this;
      }

      public ArmsTenantSQL andContactNumberLessThan(java.lang.String value) {
          addCriterion("contact_number", value, ConditionMode.LESS_THEN, "contactNumber", "java.lang.String", "String");
          return this;
      }

      public ArmsTenantSQL andContactNumberLessThanOrEqualTo(java.lang.String value) {
          addCriterion("contact_number", value, ConditionMode.LESS_EQUAL, "contactNumber", "java.lang.String", "String");
          return this;
      }

      public ArmsTenantSQL andContactNumberBetween(java.lang.String value1, java.lang.String value2) {
    	  addCriterion("contact_number", value1, value2, ConditionMode.BETWEEN, "contactNumber", "java.lang.String", "String");
    	  return this;
      }

      public ArmsTenantSQL andContactNumberNotBetween(java.lang.String value1, java.lang.String value2) {
          addCriterion("contact_number", value1, value2, ConditionMode.NOT_BETWEEN, "contactNumber", "java.lang.String", "String");
          return this;
      }
        
      public ArmsTenantSQL andContactNumberIn(List<java.lang.String> values) {
          addCriterion("contact_number", values, ConditionMode.IN, "contactNumber", "java.lang.String", "String");
          return this;
      }

      public ArmsTenantSQL andContactNumberNotIn(List<java.lang.String> values) {
          addCriterion("contact_number", values, ConditionMode.NOT_IN, "contactNumber", "java.lang.String", "String");
          return this;
      }
	public ArmsTenantSQL andAddressIsNull() {
		isnull("address");
		return this;
	}
	
	public ArmsTenantSQL andAddressIsNotNull() {
		notNull("address");
		return this;
	}
	
	public ArmsTenantSQL andAddressIsEmpty() {
		empty("address");
		return this;
	}

	public ArmsTenantSQL andAddressIsNotEmpty() {
		notEmpty("address");
		return this;
	}
       public ArmsTenantSQL andAddressLike(java.lang.String value) {
    	   addCriterion("address", value, ConditionMode.FUZZY, "address", "java.lang.String", "String");
    	   return this;
      }

      public ArmsTenantSQL andAddressNotLike(java.lang.String value) {
          addCriterion("address", value, ConditionMode.NOT_FUZZY, "address", "java.lang.String", "String");
          return this;
      }
      public ArmsTenantSQL andAddressEqualTo(java.lang.String value) {
          addCriterion("address", value, ConditionMode.EQUAL, "address", "java.lang.String", "String");
          return this;
      }

      public ArmsTenantSQL andAddressNotEqualTo(java.lang.String value) {
          addCriterion("address", value, ConditionMode.NOT_EQUAL, "address", "java.lang.String", "String");
          return this;
      }

      public ArmsTenantSQL andAddressGreaterThan(java.lang.String value) {
          addCriterion("address", value, ConditionMode.GREATER_THEN, "address", "java.lang.String", "String");
          return this;
      }

      public ArmsTenantSQL andAddressGreaterThanOrEqualTo(java.lang.String value) {
          addCriterion("address", value, ConditionMode.GREATER_EQUAL, "address", "java.lang.String", "String");
          return this;
      }

      public ArmsTenantSQL andAddressLessThan(java.lang.String value) {
          addCriterion("address", value, ConditionMode.LESS_THEN, "address", "java.lang.String", "String");
          return this;
      }

      public ArmsTenantSQL andAddressLessThanOrEqualTo(java.lang.String value) {
          addCriterion("address", value, ConditionMode.LESS_EQUAL, "address", "java.lang.String", "String");
          return this;
      }

      public ArmsTenantSQL andAddressBetween(java.lang.String value1, java.lang.String value2) {
    	  addCriterion("address", value1, value2, ConditionMode.BETWEEN, "address", "java.lang.String", "String");
    	  return this;
      }

      public ArmsTenantSQL andAddressNotBetween(java.lang.String value1, java.lang.String value2) {
          addCriterion("address", value1, value2, ConditionMode.NOT_BETWEEN, "address", "java.lang.String", "String");
          return this;
      }
        
      public ArmsTenantSQL andAddressIn(List<java.lang.String> values) {
          addCriterion("address", values, ConditionMode.IN, "address", "java.lang.String", "String");
          return this;
      }

      public ArmsTenantSQL andAddressNotIn(List<java.lang.String> values) {
          addCriterion("address", values, ConditionMode.NOT_IN, "address", "java.lang.String", "String");
          return this;
      }
	public ArmsTenantSQL andCreateUserIsNull() {
		isnull("create_user");
		return this;
	}
	
	public ArmsTenantSQL andCreateUserIsNotNull() {
		notNull("create_user");
		return this;
	}
	
	public ArmsTenantSQL andCreateUserIsEmpty() {
		empty("create_user");
		return this;
	}

	public ArmsTenantSQL andCreateUserIsNotEmpty() {
		notEmpty("create_user");
		return this;
	}
      public ArmsTenantSQL andCreateUserEqualTo(java.lang.Long value) {
          addCriterion("create_user", value, ConditionMode.EQUAL, "createUser", "java.lang.Long", "Float");
          return this;
      }

      public ArmsTenantSQL andCreateUserNotEqualTo(java.lang.Long value) {
          addCriterion("create_user", value, ConditionMode.NOT_EQUAL, "createUser", "java.lang.Long", "Float");
          return this;
      }

      public ArmsTenantSQL andCreateUserGreaterThan(java.lang.Long value) {
          addCriterion("create_user", value, ConditionMode.GREATER_THEN, "createUser", "java.lang.Long", "Float");
          return this;
      }

      public ArmsTenantSQL andCreateUserGreaterThanOrEqualTo(java.lang.Long value) {
          addCriterion("create_user", value, ConditionMode.GREATER_EQUAL, "createUser", "java.lang.Long", "Float");
          return this;
      }

      public ArmsTenantSQL andCreateUserLessThan(java.lang.Long value) {
          addCriterion("create_user", value, ConditionMode.LESS_THEN, "createUser", "java.lang.Long", "Float");
          return this;
      }

      public ArmsTenantSQL andCreateUserLessThanOrEqualTo(java.lang.Long value) {
          addCriterion("create_user", value, ConditionMode.LESS_EQUAL, "createUser", "java.lang.Long", "Float");
          return this;
      }

      public ArmsTenantSQL andCreateUserBetween(java.lang.Long value1, java.lang.Long value2) {
    	  addCriterion("create_user", value1, value2, ConditionMode.BETWEEN, "createUser", "java.lang.Long", "Float");
    	  return this;
      }

      public ArmsTenantSQL andCreateUserNotBetween(java.lang.Long value1, java.lang.Long value2) {
          addCriterion("create_user", value1, value2, ConditionMode.NOT_BETWEEN, "createUser", "java.lang.Long", "Float");
          return this;
      }
        
      public ArmsTenantSQL andCreateUserIn(List<java.lang.Long> values) {
          addCriterion("create_user", values, ConditionMode.IN, "createUser", "java.lang.Long", "Float");
          return this;
      }

      public ArmsTenantSQL andCreateUserNotIn(List<java.lang.Long> values) {
          addCriterion("create_user", values, ConditionMode.NOT_IN, "createUser", "java.lang.Long", "Float");
          return this;
      }
	public ArmsTenantSQL andCreateTimeIsNull() {
		isnull("create_time");
		return this;
	}
	
	public ArmsTenantSQL andCreateTimeIsNotNull() {
		notNull("create_time");
		return this;
	}
	
	public ArmsTenantSQL andCreateTimeIsEmpty() {
		empty("create_time");
		return this;
	}

	public ArmsTenantSQL andCreateTimeIsNotEmpty() {
		notEmpty("create_time");
		return this;
	}
      public ArmsTenantSQL andCreateTimeEqualTo(java.util.Date value) {
          addCriterion("create_time", value, ConditionMode.EQUAL, "createTime", "java.util.Date", "String");
          return this;
      }

      public ArmsTenantSQL andCreateTimeNotEqualTo(java.util.Date value) {
          addCriterion("create_time", value, ConditionMode.NOT_EQUAL, "createTime", "java.util.Date", "String");
          return this;
      }

      public ArmsTenantSQL andCreateTimeGreaterThan(java.util.Date value) {
          addCriterion("create_time", value, ConditionMode.GREATER_THEN, "createTime", "java.util.Date", "String");
          return this;
      }

      public ArmsTenantSQL andCreateTimeGreaterThanOrEqualTo(java.util.Date value) {
          addCriterion("create_time", value, ConditionMode.GREATER_EQUAL, "createTime", "java.util.Date", "String");
          return this;
      }

      public ArmsTenantSQL andCreateTimeLessThan(java.util.Date value) {
          addCriterion("create_time", value, ConditionMode.LESS_THEN, "createTime", "java.util.Date", "String");
          return this;
      }

      public ArmsTenantSQL andCreateTimeLessThanOrEqualTo(java.util.Date value) {
          addCriterion("create_time", value, ConditionMode.LESS_EQUAL, "createTime", "java.util.Date", "String");
          return this;
      }

      public ArmsTenantSQL andCreateTimeBetween(java.util.Date value1, java.util.Date value2) {
    	  addCriterion("create_time", value1, value2, ConditionMode.BETWEEN, "createTime", "java.util.Date", "String");
    	  return this;
      }

      public ArmsTenantSQL andCreateTimeNotBetween(java.util.Date value1, java.util.Date value2) {
          addCriterion("create_time", value1, value2, ConditionMode.NOT_BETWEEN, "createTime", "java.util.Date", "String");
          return this;
      }
        
      public ArmsTenantSQL andCreateTimeIn(List<java.util.Date> values) {
          addCriterion("create_time", values, ConditionMode.IN, "createTime", "java.util.Date", "String");
          return this;
      }

      public ArmsTenantSQL andCreateTimeNotIn(List<java.util.Date> values) {
          addCriterion("create_time", values, ConditionMode.NOT_IN, "createTime", "java.util.Date", "String");
          return this;
      }
	public ArmsTenantSQL andUpdateUserIsNull() {
		isnull("update_user");
		return this;
	}
	
	public ArmsTenantSQL andUpdateUserIsNotNull() {
		notNull("update_user");
		return this;
	}
	
	public ArmsTenantSQL andUpdateUserIsEmpty() {
		empty("update_user");
		return this;
	}

	public ArmsTenantSQL andUpdateUserIsNotEmpty() {
		notEmpty("update_user");
		return this;
	}
      public ArmsTenantSQL andUpdateUserEqualTo(java.lang.Long value) {
          addCriterion("update_user", value, ConditionMode.EQUAL, "updateUser", "java.lang.Long", "Float");
          return this;
      }

      public ArmsTenantSQL andUpdateUserNotEqualTo(java.lang.Long value) {
          addCriterion("update_user", value, ConditionMode.NOT_EQUAL, "updateUser", "java.lang.Long", "Float");
          return this;
      }

      public ArmsTenantSQL andUpdateUserGreaterThan(java.lang.Long value) {
          addCriterion("update_user", value, ConditionMode.GREATER_THEN, "updateUser", "java.lang.Long", "Float");
          return this;
      }

      public ArmsTenantSQL andUpdateUserGreaterThanOrEqualTo(java.lang.Long value) {
          addCriterion("update_user", value, ConditionMode.GREATER_EQUAL, "updateUser", "java.lang.Long", "Float");
          return this;
      }

      public ArmsTenantSQL andUpdateUserLessThan(java.lang.Long value) {
          addCriterion("update_user", value, ConditionMode.LESS_THEN, "updateUser", "java.lang.Long", "Float");
          return this;
      }

      public ArmsTenantSQL andUpdateUserLessThanOrEqualTo(java.lang.Long value) {
          addCriterion("update_user", value, ConditionMode.LESS_EQUAL, "updateUser", "java.lang.Long", "Float");
          return this;
      }

      public ArmsTenantSQL andUpdateUserBetween(java.lang.Long value1, java.lang.Long value2) {
    	  addCriterion("update_user", value1, value2, ConditionMode.BETWEEN, "updateUser", "java.lang.Long", "Float");
    	  return this;
      }

      public ArmsTenantSQL andUpdateUserNotBetween(java.lang.Long value1, java.lang.Long value2) {
          addCriterion("update_user", value1, value2, ConditionMode.NOT_BETWEEN, "updateUser", "java.lang.Long", "Float");
          return this;
      }
        
      public ArmsTenantSQL andUpdateUserIn(List<java.lang.Long> values) {
          addCriterion("update_user", values, ConditionMode.IN, "updateUser", "java.lang.Long", "Float");
          return this;
      }

      public ArmsTenantSQL andUpdateUserNotIn(List<java.lang.Long> values) {
          addCriterion("update_user", values, ConditionMode.NOT_IN, "updateUser", "java.lang.Long", "Float");
          return this;
      }
	public ArmsTenantSQL andTenantStatusIsNull() {
		isnull("tenant_status");
		return this;
	}
	
	public ArmsTenantSQL andTenantStatusIsNotNull() {
		notNull("tenant_status");
		return this;
	}
	
	public ArmsTenantSQL andTenantStatusIsEmpty() {
		empty("tenant_status");
		return this;
	}

	public ArmsTenantSQL andTenantStatusIsNotEmpty() {
		notEmpty("tenant_status");
		return this;
	}
      public ArmsTenantSQL andTenantStatusEqualTo(java.lang.Integer value) {
          addCriterion("tenant_status", value, ConditionMode.EQUAL, "tenantStatus", "java.lang.Integer", "Float");
          return this;
      }

      public ArmsTenantSQL andTenantStatusNotEqualTo(java.lang.Integer value) {
          addCriterion("tenant_status", value, ConditionMode.NOT_EQUAL, "tenantStatus", "java.lang.Integer", "Float");
          return this;
      }

      public ArmsTenantSQL andTenantStatusGreaterThan(java.lang.Integer value) {
          addCriterion("tenant_status", value, ConditionMode.GREATER_THEN, "tenantStatus", "java.lang.Integer", "Float");
          return this;
      }

      public ArmsTenantSQL andTenantStatusGreaterThanOrEqualTo(java.lang.Integer value) {
          addCriterion("tenant_status", value, ConditionMode.GREATER_EQUAL, "tenantStatus", "java.lang.Integer", "Float");
          return this;
      }

      public ArmsTenantSQL andTenantStatusLessThan(java.lang.Integer value) {
          addCriterion("tenant_status", value, ConditionMode.LESS_THEN, "tenantStatus", "java.lang.Integer", "Float");
          return this;
      }

      public ArmsTenantSQL andTenantStatusLessThanOrEqualTo(java.lang.Integer value) {
          addCriterion("tenant_status", value, ConditionMode.LESS_EQUAL, "tenantStatus", "java.lang.Integer", "Float");
          return this;
      }

      public ArmsTenantSQL andTenantStatusBetween(java.lang.Integer value1, java.lang.Integer value2) {
    	  addCriterion("tenant_status", value1, value2, ConditionMode.BETWEEN, "tenantStatus", "java.lang.Integer", "Float");
    	  return this;
      }

      public ArmsTenantSQL andTenantStatusNotBetween(java.lang.Integer value1, java.lang.Integer value2) {
          addCriterion("tenant_status", value1, value2, ConditionMode.NOT_BETWEEN, "tenantStatus", "java.lang.Integer", "Float");
          return this;
      }
        
      public ArmsTenantSQL andTenantStatusIn(List<java.lang.Integer> values) {
          addCriterion("tenant_status", values, ConditionMode.IN, "tenantStatus", "java.lang.Integer", "Float");
          return this;
      }

      public ArmsTenantSQL andTenantStatusNotIn(List<java.lang.Integer> values) {
          addCriterion("tenant_status", values, ConditionMode.NOT_IN, "tenantStatus", "java.lang.Integer", "Float");
          return this;
      }
	public ArmsTenantSQL andIsDeletedIsNull() {
		isnull("is_deleted");
		return this;
	}
	
	public ArmsTenantSQL andIsDeletedIsNotNull() {
		notNull("is_deleted");
		return this;
	}
	
	public ArmsTenantSQL andIsDeletedIsEmpty() {
		empty("is_deleted");
		return this;
	}

	public ArmsTenantSQL andIsDeletedIsNotEmpty() {
		notEmpty("is_deleted");
		return this;
	}
      public ArmsTenantSQL andIsDeletedEqualTo(java.lang.Integer value) {
          addCriterion("is_deleted", value, ConditionMode.EQUAL, "isDeleted", "java.lang.Integer", "Float");
          return this;
      }

      public ArmsTenantSQL andIsDeletedNotEqualTo(java.lang.Integer value) {
          addCriterion("is_deleted", value, ConditionMode.NOT_EQUAL, "isDeleted", "java.lang.Integer", "Float");
          return this;
      }

      public ArmsTenantSQL andIsDeletedGreaterThan(java.lang.Integer value) {
          addCriterion("is_deleted", value, ConditionMode.GREATER_THEN, "isDeleted", "java.lang.Integer", "Float");
          return this;
      }

      public ArmsTenantSQL andIsDeletedGreaterThanOrEqualTo(java.lang.Integer value) {
          addCriterion("is_deleted", value, ConditionMode.GREATER_EQUAL, "isDeleted", "java.lang.Integer", "Float");
          return this;
      }

      public ArmsTenantSQL andIsDeletedLessThan(java.lang.Integer value) {
          addCriterion("is_deleted", value, ConditionMode.LESS_THEN, "isDeleted", "java.lang.Integer", "Float");
          return this;
      }

      public ArmsTenantSQL andIsDeletedLessThanOrEqualTo(java.lang.Integer value) {
          addCriterion("is_deleted", value, ConditionMode.LESS_EQUAL, "isDeleted", "java.lang.Integer", "Float");
          return this;
      }

      public ArmsTenantSQL andIsDeletedBetween(java.lang.Integer value1, java.lang.Integer value2) {
    	  addCriterion("is_deleted", value1, value2, ConditionMode.BETWEEN, "isDeleted", "java.lang.Integer", "Float");
    	  return this;
      }

      public ArmsTenantSQL andIsDeletedNotBetween(java.lang.Integer value1, java.lang.Integer value2) {
          addCriterion("is_deleted", value1, value2, ConditionMode.NOT_BETWEEN, "isDeleted", "java.lang.Integer", "Float");
          return this;
      }
        
      public ArmsTenantSQL andIsDeletedIn(List<java.lang.Integer> values) {
          addCriterion("is_deleted", values, ConditionMode.IN, "isDeleted", "java.lang.Integer", "Float");
          return this;
      }

      public ArmsTenantSQL andIsDeletedNotIn(List<java.lang.Integer> values) {
          addCriterion("is_deleted", values, ConditionMode.NOT_IN, "isDeleted", "java.lang.Integer", "Float");
          return this;
      }
}