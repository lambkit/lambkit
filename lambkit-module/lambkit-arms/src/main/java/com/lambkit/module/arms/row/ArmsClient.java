/**
 * Copyright (c) 2015-2017, Henry Yang 杨勇 (gismail@foxmail.com).
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.lambkit.module.arms.row;

import com.lambkit.db.RowModel;

/**
 * @author yangyong 
 * @website: www.lambkit.com
 * @email: gismail@foxmail.com
 * @date ${date}
 * @version 1.0
 * @since 1.0
 */

public class ArmsClient extends RowModel<ArmsClient> {
	public ArmsClient() {
		setTableName("arms_client");
		setPrimaryKey("id");
	}
	public java.lang.Long getId() {
		return this.get("id");
	}
	public void setId(java.lang.Long id) {
		this.set("id", id);
	}
	public java.lang.String getClientId() {
		return this.get("client_id");
	}
	public void setClientId(java.lang.String clientId) {
		this.set("client_id", clientId);
	}
	public java.lang.String getClientSecret() {
		return this.get("client_secret");
	}
	public void setClientSecret(java.lang.String clientSecret) {
		this.set("client_secret", clientSecret);
	}
	public java.lang.String getResourceIds() {
		return this.get("resource_ids");
	}
	public void setResourceIds(java.lang.String resourceIds) {
		this.set("resource_ids", resourceIds);
	}
	public java.lang.String getScope() {
		return this.get("scope");
	}
	public void setScope(java.lang.String scope) {
		this.set("scope", scope);
	}
	public java.lang.String getAuthorizedGrantTypes() {
		return this.get("authorized_grant_types");
	}
	public void setAuthorizedGrantTypes(java.lang.String authorizedGrantTypes) {
		this.set("authorized_grant_types", authorizedGrantTypes);
	}
	public java.lang.String getWebServerRedirectUri() {
		return this.get("web_server_redirect_uri");
	}
	public void setWebServerRedirectUri(java.lang.String webServerRedirectUri) {
		this.set("web_server_redirect_uri", webServerRedirectUri);
	}
	public java.lang.String getAuthorities() {
		return this.get("authorities");
	}
	public void setAuthorities(java.lang.String authorities) {
		this.set("authorities", authorities);
	}
	public java.lang.Integer getAccessTokenValidity() {
		return this.get("access_token_validity");
	}
	public void setAccessTokenValidity(java.lang.Integer accessTokenValidity) {
		this.set("access_token_validity", accessTokenValidity);
	}
	public java.lang.Integer getRefreshTokenValidity() {
		return this.get("refresh_token_validity");
	}
	public void setRefreshTokenValidity(java.lang.Integer refreshTokenValidity) {
		this.set("refresh_token_validity", refreshTokenValidity);
	}
	public java.lang.String getAdditionalInformation() {
		return this.get("additional_information");
	}
	public void setAdditionalInformation(java.lang.String additionalInformation) {
		this.set("additional_information", additionalInformation);
	}
	public java.lang.String getAutoapprove() {
		return this.get("autoapprove");
	}
	public void setAutoapprove(java.lang.String autoapprove) {
		this.set("autoapprove", autoapprove);
	}
	public java.lang.Long getCreateUser() {
		return this.get("create_user");
	}
	public void setCreateUser(java.lang.Long createUser) {
		this.set("create_user", createUser);
	}
	public java.util.Date getCreateTime() {
		return this.get("create_time");
	}
	public void setCreateTime(java.util.Date createTime) {
		this.set("create_time", createTime);
	}
	public java.lang.Long getUpdateUser() {
		return this.get("update_user");
	}
	public void setUpdateUser(java.lang.Long updateUser) {
		this.set("update_user", updateUser);
	}
	public java.lang.Integer getClientStatus() {
		return this.get("client_status");
	}
	public void setClientStatus(java.lang.Integer clientStatus) {
		this.set("client_status", clientStatus);
	}
	public java.lang.Integer getIsDeleted() {
		return this.get("is_deleted");
	}
	public void setIsDeleted(java.lang.Integer isDeleted) {
		this.set("is_deleted", isDeleted);
	}
	public java.lang.String getTenantId() {
		return this.get("tenant_id");
	}
	public void setTenantId(java.lang.String tenantId) {
		this.set("tenant_id", tenantId);
	}
}
