/**
 * Copyright (c) 2015-2017, Henry Yang 杨勇 (gismail@foxmail.com).
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.lambkit.module.arms.row;

import com.lambkit.db.RowModel;

/**
 * @author yangyong 
 * @website: www.lambkit.com
 * @email: gismail@foxmail.com
 * @date ${date}
 * @version 1.0
 * @since 1.0
 */

public class ArmsApi extends RowModel<ArmsApi> {
	public ArmsApi() {
		setTableName("arms_api");
		setPrimaryKey("api_id");
	}
	public java.lang.Long getApiId() {
		return this.get("api_id");
	}
	public void setApiId(java.lang.Long apiId) {
		this.set("api_id", apiId);
	}
	public java.lang.Integer getSystemId() {
		return this.get("system_id");
	}
	public void setSystemId(java.lang.Integer systemId) {
		this.set("system_id", systemId);
	}
	public java.lang.Long getUserid() {
		return this.get("userid");
	}
	public void setUserid(java.lang.Long userid) {
		this.set("userid", userid);
	}
	public java.lang.String getTitle() {
		return this.get("title");
	}
	public void setTitle(java.lang.String title) {
		this.set("title", title);
	}
	public java.lang.String getUri() {
		return this.get("uri");
	}
	public void setUri(java.lang.String uri) {
		this.set("uri", uri);
	}
	public java.lang.String getContent() {
		return this.get("content");
	}
	public void setContent(java.lang.String content) {
		this.set("content", content);
	}
	public java.lang.String getExam() {
		return this.get("exam");
	}
	public void setExam(java.lang.String exam) {
		this.set("exam", exam);
	}
	public java.lang.String getResult() {
		return this.get("result");
	}
	public void setResult(java.lang.String result) {
		this.set("result", result);
	}
	public java.lang.Integer getStatus() {
		return this.get("status");
	}
	public void setStatus(java.lang.Integer status) {
		this.set("status", status);
	}
	public java.util.Date getCtime() {
		return this.get("ctime");
	}
	public void setCtime(java.util.Date ctime) {
		this.set("ctime", ctime);
	}
	public java.lang.String getCatalog() {
		return this.get("catalog");
	}
	public void setCatalog(java.lang.String catalog) {
		this.set("catalog", catalog);
	}
	public java.lang.String getFlag() {
		return this.get("flag");
	}
	public void setFlag(java.lang.String flag) {
		this.set("flag", flag);
	}
	public java.lang.String getShared() {
		return this.get("shared");
	}
	public void setShared(java.lang.String shared) {
		this.set("shared", shared);
	}
	public java.lang.String getMethod() {
		return this.get("method");
	}
	public void setMethod(java.lang.String method) {
		this.set("method", method);
	}
	public java.lang.Integer getPrjid() {
		return this.get("prjid");
	}
	public void setPrjid(java.lang.Integer prjid) {
		this.set("prjid", prjid);
	}
	public java.lang.String getParams() {
		return this.get("params");
	}
	public void setParams(java.lang.String params) {
		this.set("params", params);
	}
	public java.lang.String getRets() {
		return this.get("rets");
	}
	public void setRets(java.lang.String rets) {
		this.set("rets", rets);
	}
}
