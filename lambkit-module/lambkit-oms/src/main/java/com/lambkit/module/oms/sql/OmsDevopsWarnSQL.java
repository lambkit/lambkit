/**
 * Copyright (c) 2015-2017, Henry Yang 杨勇 (gismail@foxmail.com).
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.lambkit.module.oms.sql;

import com.lambkit.db.sql.ConditionMode;
import com.lambkit.db.sql.Column;
import com.lambkit.db.sql.Columns;
import com.lambkit.db.sql.Example;

import java.io.Serializable;
import java.util.List;

/**
 * @author yangyong 
 * @website: www.lambkit.com
 * @email: gismail@foxmail.com
 * @date ${date}
 * @version 1.0
 * @since 1.0
 */

public class OmsDevopsWarnSQL extends Columns implements Serializable {
	private static final long serialVersionUID = 1L;
	
	public static OmsDevopsWarnSQL of() {
		return new OmsDevopsWarnSQL();
	}
	
	public static OmsDevopsWarnSQL by(Column column) {
		OmsDevopsWarnSQL that = new OmsDevopsWarnSQL();
		that.add(column);
        return that;
    }

    public static OmsDevopsWarnSQL by(String name, Object value) {
        return (OmsDevopsWarnSQL) create().eq(name, value);
    }
    
    public Example example() {
    	return Example.create("oms_devops_warn", this);
    }
    
    /**
     * equals
     *
     * @param name
     * @param value
     * @return
     */
    public OmsDevopsWarnSQL eq(String name, Object value) {
    	super.eq(name, value);
        return this;
    }

    /**
     * not equals !=
     *
     * @param name
     * @param value
     * @return
     */
    public OmsDevopsWarnSQL ne(String name, Object value) {
    	super.ne(name, value);
        return this;
    }


    /**
     * like
     *
     * @param name
     * @param value
     * @return
     */

    public OmsDevopsWarnSQL like(String name, Object value) {
    	super.like(name, value);
        return this;
    }
    
    public OmsDevopsWarnSQL notLike(String name, Object value) {
    	super.notLike(name, value);
        return this;
    }

    /**
     * 大于 great than
     *
     * @param name
     * @param value
     * @return
     */
    public OmsDevopsWarnSQL gt(String name, Object value) {
    	super.gt(name, value);
        return this;
    }

    /**
     * 大于等于 great or equal
     *
     * @param name
     * @param value
     * @return
     */
    public OmsDevopsWarnSQL ge(String name, Object value) {
    	super.ge(name, value);
        return this;
    }

    /**
     * 小于 less than
     *
     * @param name
     * @param value
     * @return
     */
    public OmsDevopsWarnSQL lt(String name, Object value) {
    	super.lt(name, value);
        return this;
    }

    /**
     * 小于等于 less or equal
     *
     * @param name
     * @param value
     * @return
     */
    public OmsDevopsWarnSQL le(String name, Object value) {
    	super.le(name, value);
        return this;
    }
    
    public OmsDevopsWarnSQL isnull(String name) {
    	super.isnull(name);
        return this;
    } 

    public OmsDevopsWarnSQL notNull(String name) {
    	super.notNull(name);
        return this;
    } 
    
    public OmsDevopsWarnSQL empty(String name) {
    	super.empty(name);
        return this;
    } 
    
    public OmsDevopsWarnSQL notEmpty(String name) {
    	super.notEmpty(name);
        return this;
    } 
    
    public OmsDevopsWarnSQL add(Column column) {
    	super.add(column);
    	return this;
    }
    
    /**************************/
	
	public void addCriterion(String name, Object value, ConditionMode logic, String property, String typeHandler, String valueType) {
		 if (value == null) {
			 throw new RuntimeException("Value for " + property + " cannot be null");
		 }
		 add(Column.create(name, value, logic, typeHandler, valueType));
	}
   
	public void addCriterion(String name, Object value1, Object value2, ConditionMode logic, String property, String typeHandler, String valueType) {
		 if (value1 == null || value2 == null) {
			 throw new RuntimeException("Between values for " + property + " cannot be null");
		 }
		 add(Column.create(name, value1, value2, logic, typeHandler, valueType));
	}
		 
	public OmsDevopsWarnSQL andDevopsWarnIdIsNull() {
		isnull("devops_warn_id");
		return this;
	}
	
	public OmsDevopsWarnSQL andDevopsWarnIdIsNotNull() {
		notNull("devops_warn_id");
		return this;
	}
	
	public OmsDevopsWarnSQL andDevopsWarnIdIsEmpty() {
		empty("devops_warn_id");
		return this;
	}

	public OmsDevopsWarnSQL andDevopsWarnIdIsNotEmpty() {
		notEmpty("devops_warn_id");
		return this;
	}
      public OmsDevopsWarnSQL andDevopsWarnIdEqualTo(java.lang.Long value) {
          addCriterion("devops_warn_id", value, ConditionMode.EQUAL, "devopsWarnId", "java.lang.Long", "Float");
          return this;
      }

      public OmsDevopsWarnSQL andDevopsWarnIdNotEqualTo(java.lang.Long value) {
          addCriterion("devops_warn_id", value, ConditionMode.NOT_EQUAL, "devopsWarnId", "java.lang.Long", "Float");
          return this;
      }

      public OmsDevopsWarnSQL andDevopsWarnIdGreaterThan(java.lang.Long value) {
          addCriterion("devops_warn_id", value, ConditionMode.GREATER_THEN, "devopsWarnId", "java.lang.Long", "Float");
          return this;
      }

      public OmsDevopsWarnSQL andDevopsWarnIdGreaterThanOrEqualTo(java.lang.Long value) {
          addCriterion("devops_warn_id", value, ConditionMode.GREATER_EQUAL, "devopsWarnId", "java.lang.Long", "Float");
          return this;
      }

      public OmsDevopsWarnSQL andDevopsWarnIdLessThan(java.lang.Long value) {
          addCriterion("devops_warn_id", value, ConditionMode.LESS_THEN, "devopsWarnId", "java.lang.Long", "Float");
          return this;
      }

      public OmsDevopsWarnSQL andDevopsWarnIdLessThanOrEqualTo(java.lang.Long value) {
          addCriterion("devops_warn_id", value, ConditionMode.LESS_EQUAL, "devopsWarnId", "java.lang.Long", "Float");
          return this;
      }

      public OmsDevopsWarnSQL andDevopsWarnIdBetween(java.lang.Long value1, java.lang.Long value2) {
    	  addCriterion("devops_warn_id", value1, value2, ConditionMode.BETWEEN, "devopsWarnId", "java.lang.Long", "Float");
    	  return this;
      }

      public OmsDevopsWarnSQL andDevopsWarnIdNotBetween(java.lang.Long value1, java.lang.Long value2) {
          addCriterion("devops_warn_id", value1, value2, ConditionMode.NOT_BETWEEN, "devopsWarnId", "java.lang.Long", "Float");
          return this;
      }
        
      public OmsDevopsWarnSQL andDevopsWarnIdIn(List<java.lang.Long> values) {
          addCriterion("devops_warn_id", values, ConditionMode.IN, "devopsWarnId", "java.lang.Long", "Float");
          return this;
      }

      public OmsDevopsWarnSQL andDevopsWarnIdNotIn(List<java.lang.Long> values) {
          addCriterion("devops_warn_id", values, ConditionMode.NOT_IN, "devopsWarnId", "java.lang.Long", "Float");
          return this;
      }
	public OmsDevopsWarnSQL andNameIsNull() {
		isnull("name");
		return this;
	}
	
	public OmsDevopsWarnSQL andNameIsNotNull() {
		notNull("name");
		return this;
	}
	
	public OmsDevopsWarnSQL andNameIsEmpty() {
		empty("name");
		return this;
	}

	public OmsDevopsWarnSQL andNameIsNotEmpty() {
		notEmpty("name");
		return this;
	}
       public OmsDevopsWarnSQL andNameLike(java.lang.String value) {
    	   addCriterion("name", value, ConditionMode.FUZZY, "name", "java.lang.String", "Float");
    	   return this;
      }

      public OmsDevopsWarnSQL andNameNotLike(java.lang.String value) {
          addCriterion("name", value, ConditionMode.NOT_FUZZY, "name", "java.lang.String", "Float");
          return this;
      }
      public OmsDevopsWarnSQL andNameEqualTo(java.lang.String value) {
          addCriterion("name", value, ConditionMode.EQUAL, "name", "java.lang.String", "String");
          return this;
      }

      public OmsDevopsWarnSQL andNameNotEqualTo(java.lang.String value) {
          addCriterion("name", value, ConditionMode.NOT_EQUAL, "name", "java.lang.String", "String");
          return this;
      }

      public OmsDevopsWarnSQL andNameGreaterThan(java.lang.String value) {
          addCriterion("name", value, ConditionMode.GREATER_THEN, "name", "java.lang.String", "String");
          return this;
      }

      public OmsDevopsWarnSQL andNameGreaterThanOrEqualTo(java.lang.String value) {
          addCriterion("name", value, ConditionMode.GREATER_EQUAL, "name", "java.lang.String", "String");
          return this;
      }

      public OmsDevopsWarnSQL andNameLessThan(java.lang.String value) {
          addCriterion("name", value, ConditionMode.LESS_THEN, "name", "java.lang.String", "String");
          return this;
      }

      public OmsDevopsWarnSQL andNameLessThanOrEqualTo(java.lang.String value) {
          addCriterion("name", value, ConditionMode.LESS_EQUAL, "name", "java.lang.String", "String");
          return this;
      }

      public OmsDevopsWarnSQL andNameBetween(java.lang.String value1, java.lang.String value2) {
    	  addCriterion("name", value1, value2, ConditionMode.BETWEEN, "name", "java.lang.String", "String");
    	  return this;
      }

      public OmsDevopsWarnSQL andNameNotBetween(java.lang.String value1, java.lang.String value2) {
          addCriterion("name", value1, value2, ConditionMode.NOT_BETWEEN, "name", "java.lang.String", "String");
          return this;
      }
        
      public OmsDevopsWarnSQL andNameIn(List<java.lang.String> values) {
          addCriterion("name", values, ConditionMode.IN, "name", "java.lang.String", "String");
          return this;
      }

      public OmsDevopsWarnSQL andNameNotIn(List<java.lang.String> values) {
          addCriterion("name", values, ConditionMode.NOT_IN, "name", "java.lang.String", "String");
          return this;
      }
	public OmsDevopsWarnSQL andTitleIsNull() {
		isnull("title");
		return this;
	}
	
	public OmsDevopsWarnSQL andTitleIsNotNull() {
		notNull("title");
		return this;
	}
	
	public OmsDevopsWarnSQL andTitleIsEmpty() {
		empty("title");
		return this;
	}

	public OmsDevopsWarnSQL andTitleIsNotEmpty() {
		notEmpty("title");
		return this;
	}
       public OmsDevopsWarnSQL andTitleLike(java.lang.String value) {
    	   addCriterion("title", value, ConditionMode.FUZZY, "title", "java.lang.String", "String");
    	   return this;
      }

      public OmsDevopsWarnSQL andTitleNotLike(java.lang.String value) {
          addCriterion("title", value, ConditionMode.NOT_FUZZY, "title", "java.lang.String", "String");
          return this;
      }
      public OmsDevopsWarnSQL andTitleEqualTo(java.lang.String value) {
          addCriterion("title", value, ConditionMode.EQUAL, "title", "java.lang.String", "String");
          return this;
      }

      public OmsDevopsWarnSQL andTitleNotEqualTo(java.lang.String value) {
          addCriterion("title", value, ConditionMode.NOT_EQUAL, "title", "java.lang.String", "String");
          return this;
      }

      public OmsDevopsWarnSQL andTitleGreaterThan(java.lang.String value) {
          addCriterion("title", value, ConditionMode.GREATER_THEN, "title", "java.lang.String", "String");
          return this;
      }

      public OmsDevopsWarnSQL andTitleGreaterThanOrEqualTo(java.lang.String value) {
          addCriterion("title", value, ConditionMode.GREATER_EQUAL, "title", "java.lang.String", "String");
          return this;
      }

      public OmsDevopsWarnSQL andTitleLessThan(java.lang.String value) {
          addCriterion("title", value, ConditionMode.LESS_THEN, "title", "java.lang.String", "String");
          return this;
      }

      public OmsDevopsWarnSQL andTitleLessThanOrEqualTo(java.lang.String value) {
          addCriterion("title", value, ConditionMode.LESS_EQUAL, "title", "java.lang.String", "String");
          return this;
      }

      public OmsDevopsWarnSQL andTitleBetween(java.lang.String value1, java.lang.String value2) {
    	  addCriterion("title", value1, value2, ConditionMode.BETWEEN, "title", "java.lang.String", "String");
    	  return this;
      }

      public OmsDevopsWarnSQL andTitleNotBetween(java.lang.String value1, java.lang.String value2) {
          addCriterion("title", value1, value2, ConditionMode.NOT_BETWEEN, "title", "java.lang.String", "String");
          return this;
      }
        
      public OmsDevopsWarnSQL andTitleIn(List<java.lang.String> values) {
          addCriterion("title", values, ConditionMode.IN, "title", "java.lang.String", "String");
          return this;
      }

      public OmsDevopsWarnSQL andTitleNotIn(List<java.lang.String> values) {
          addCriterion("title", values, ConditionMode.NOT_IN, "title", "java.lang.String", "String");
          return this;
      }
	public OmsDevopsWarnSQL andTypeIsNull() {
		isnull("type");
		return this;
	}
	
	public OmsDevopsWarnSQL andTypeIsNotNull() {
		notNull("type");
		return this;
	}
	
	public OmsDevopsWarnSQL andTypeIsEmpty() {
		empty("type");
		return this;
	}

	public OmsDevopsWarnSQL andTypeIsNotEmpty() {
		notEmpty("type");
		return this;
	}
       public OmsDevopsWarnSQL andTypeLike(java.lang.String value) {
    	   addCriterion("type", value, ConditionMode.FUZZY, "type", "java.lang.String", "String");
    	   return this;
      }

      public OmsDevopsWarnSQL andTypeNotLike(java.lang.String value) {
          addCriterion("type", value, ConditionMode.NOT_FUZZY, "type", "java.lang.String", "String");
          return this;
      }
      public OmsDevopsWarnSQL andTypeEqualTo(java.lang.String value) {
          addCriterion("type", value, ConditionMode.EQUAL, "type", "java.lang.String", "String");
          return this;
      }

      public OmsDevopsWarnSQL andTypeNotEqualTo(java.lang.String value) {
          addCriterion("type", value, ConditionMode.NOT_EQUAL, "type", "java.lang.String", "String");
          return this;
      }

      public OmsDevopsWarnSQL andTypeGreaterThan(java.lang.String value) {
          addCriterion("type", value, ConditionMode.GREATER_THEN, "type", "java.lang.String", "String");
          return this;
      }

      public OmsDevopsWarnSQL andTypeGreaterThanOrEqualTo(java.lang.String value) {
          addCriterion("type", value, ConditionMode.GREATER_EQUAL, "type", "java.lang.String", "String");
          return this;
      }

      public OmsDevopsWarnSQL andTypeLessThan(java.lang.String value) {
          addCriterion("type", value, ConditionMode.LESS_THEN, "type", "java.lang.String", "String");
          return this;
      }

      public OmsDevopsWarnSQL andTypeLessThanOrEqualTo(java.lang.String value) {
          addCriterion("type", value, ConditionMode.LESS_EQUAL, "type", "java.lang.String", "String");
          return this;
      }

      public OmsDevopsWarnSQL andTypeBetween(java.lang.String value1, java.lang.String value2) {
    	  addCriterion("type", value1, value2, ConditionMode.BETWEEN, "type", "java.lang.String", "String");
    	  return this;
      }

      public OmsDevopsWarnSQL andTypeNotBetween(java.lang.String value1, java.lang.String value2) {
          addCriterion("type", value1, value2, ConditionMode.NOT_BETWEEN, "type", "java.lang.String", "String");
          return this;
      }
        
      public OmsDevopsWarnSQL andTypeIn(List<java.lang.String> values) {
          addCriterion("type", values, ConditionMode.IN, "type", "java.lang.String", "String");
          return this;
      }

      public OmsDevopsWarnSQL andTypeNotIn(List<java.lang.String> values) {
          addCriterion("type", values, ConditionMode.NOT_IN, "type", "java.lang.String", "String");
          return this;
      }
	public OmsDevopsWarnSQL andCreatedIsNull() {
		isnull("created");
		return this;
	}
	
	public OmsDevopsWarnSQL andCreatedIsNotNull() {
		notNull("created");
		return this;
	}
	
	public OmsDevopsWarnSQL andCreatedIsEmpty() {
		empty("created");
		return this;
	}

	public OmsDevopsWarnSQL andCreatedIsNotEmpty() {
		notEmpty("created");
		return this;
	}
      public OmsDevopsWarnSQL andCreatedEqualTo(java.util.Date value) {
          addCriterion("created", value, ConditionMode.EQUAL, "created", "java.util.Date", "String");
          return this;
      }

      public OmsDevopsWarnSQL andCreatedNotEqualTo(java.util.Date value) {
          addCriterion("created", value, ConditionMode.NOT_EQUAL, "created", "java.util.Date", "String");
          return this;
      }

      public OmsDevopsWarnSQL andCreatedGreaterThan(java.util.Date value) {
          addCriterion("created", value, ConditionMode.GREATER_THEN, "created", "java.util.Date", "String");
          return this;
      }

      public OmsDevopsWarnSQL andCreatedGreaterThanOrEqualTo(java.util.Date value) {
          addCriterion("created", value, ConditionMode.GREATER_EQUAL, "created", "java.util.Date", "String");
          return this;
      }

      public OmsDevopsWarnSQL andCreatedLessThan(java.util.Date value) {
          addCriterion("created", value, ConditionMode.LESS_THEN, "created", "java.util.Date", "String");
          return this;
      }

      public OmsDevopsWarnSQL andCreatedLessThanOrEqualTo(java.util.Date value) {
          addCriterion("created", value, ConditionMode.LESS_EQUAL, "created", "java.util.Date", "String");
          return this;
      }

      public OmsDevopsWarnSQL andCreatedBetween(java.util.Date value1, java.util.Date value2) {
    	  addCriterion("created", value1, value2, ConditionMode.BETWEEN, "created", "java.util.Date", "String");
    	  return this;
      }

      public OmsDevopsWarnSQL andCreatedNotBetween(java.util.Date value1, java.util.Date value2) {
          addCriterion("created", value1, value2, ConditionMode.NOT_BETWEEN, "created", "java.util.Date", "String");
          return this;
      }
        
      public OmsDevopsWarnSQL andCreatedIn(List<java.util.Date> values) {
          addCriterion("created", values, ConditionMode.IN, "created", "java.util.Date", "String");
          return this;
      }

      public OmsDevopsWarnSQL andCreatedNotIn(List<java.util.Date> values) {
          addCriterion("created", values, ConditionMode.NOT_IN, "created", "java.util.Date", "String");
          return this;
      }
	public OmsDevopsWarnSQL andStimeIsNull() {
		isnull("stime");
		return this;
	}
	
	public OmsDevopsWarnSQL andStimeIsNotNull() {
		notNull("stime");
		return this;
	}
	
	public OmsDevopsWarnSQL andStimeIsEmpty() {
		empty("stime");
		return this;
	}

	public OmsDevopsWarnSQL andStimeIsNotEmpty() {
		notEmpty("stime");
		return this;
	}
      public OmsDevopsWarnSQL andStimeEqualTo(java.lang.Long value) {
          addCriterion("stime", value, ConditionMode.EQUAL, "stime", "java.lang.Long", "Float");
          return this;
      }

      public OmsDevopsWarnSQL andStimeNotEqualTo(java.lang.Long value) {
          addCriterion("stime", value, ConditionMode.NOT_EQUAL, "stime", "java.lang.Long", "Float");
          return this;
      }

      public OmsDevopsWarnSQL andStimeGreaterThan(java.lang.Long value) {
          addCriterion("stime", value, ConditionMode.GREATER_THEN, "stime", "java.lang.Long", "Float");
          return this;
      }

      public OmsDevopsWarnSQL andStimeGreaterThanOrEqualTo(java.lang.Long value) {
          addCriterion("stime", value, ConditionMode.GREATER_EQUAL, "stime", "java.lang.Long", "Float");
          return this;
      }

      public OmsDevopsWarnSQL andStimeLessThan(java.lang.Long value) {
          addCriterion("stime", value, ConditionMode.LESS_THEN, "stime", "java.lang.Long", "Float");
          return this;
      }

      public OmsDevopsWarnSQL andStimeLessThanOrEqualTo(java.lang.Long value) {
          addCriterion("stime", value, ConditionMode.LESS_EQUAL, "stime", "java.lang.Long", "Float");
          return this;
      }

      public OmsDevopsWarnSQL andStimeBetween(java.lang.Long value1, java.lang.Long value2) {
    	  addCriterion("stime", value1, value2, ConditionMode.BETWEEN, "stime", "java.lang.Long", "Float");
    	  return this;
      }

      public OmsDevopsWarnSQL andStimeNotBetween(java.lang.Long value1, java.lang.Long value2) {
          addCriterion("stime", value1, value2, ConditionMode.NOT_BETWEEN, "stime", "java.lang.Long", "Float");
          return this;
      }
        
      public OmsDevopsWarnSQL andStimeIn(List<java.lang.Long> values) {
          addCriterion("stime", values, ConditionMode.IN, "stime", "java.lang.Long", "Float");
          return this;
      }

      public OmsDevopsWarnSQL andStimeNotIn(List<java.lang.Long> values) {
          addCriterion("stime", values, ConditionMode.NOT_IN, "stime", "java.lang.Long", "Float");
          return this;
      }
	public OmsDevopsWarnSQL andEtimeIsNull() {
		isnull("etime");
		return this;
	}
	
	public OmsDevopsWarnSQL andEtimeIsNotNull() {
		notNull("etime");
		return this;
	}
	
	public OmsDevopsWarnSQL andEtimeIsEmpty() {
		empty("etime");
		return this;
	}

	public OmsDevopsWarnSQL andEtimeIsNotEmpty() {
		notEmpty("etime");
		return this;
	}
      public OmsDevopsWarnSQL andEtimeEqualTo(java.lang.Long value) {
          addCriterion("etime", value, ConditionMode.EQUAL, "etime", "java.lang.Long", "Float");
          return this;
      }

      public OmsDevopsWarnSQL andEtimeNotEqualTo(java.lang.Long value) {
          addCriterion("etime", value, ConditionMode.NOT_EQUAL, "etime", "java.lang.Long", "Float");
          return this;
      }

      public OmsDevopsWarnSQL andEtimeGreaterThan(java.lang.Long value) {
          addCriterion("etime", value, ConditionMode.GREATER_THEN, "etime", "java.lang.Long", "Float");
          return this;
      }

      public OmsDevopsWarnSQL andEtimeGreaterThanOrEqualTo(java.lang.Long value) {
          addCriterion("etime", value, ConditionMode.GREATER_EQUAL, "etime", "java.lang.Long", "Float");
          return this;
      }

      public OmsDevopsWarnSQL andEtimeLessThan(java.lang.Long value) {
          addCriterion("etime", value, ConditionMode.LESS_THEN, "etime", "java.lang.Long", "Float");
          return this;
      }

      public OmsDevopsWarnSQL andEtimeLessThanOrEqualTo(java.lang.Long value) {
          addCriterion("etime", value, ConditionMode.LESS_EQUAL, "etime", "java.lang.Long", "Float");
          return this;
      }

      public OmsDevopsWarnSQL andEtimeBetween(java.lang.Long value1, java.lang.Long value2) {
    	  addCriterion("etime", value1, value2, ConditionMode.BETWEEN, "etime", "java.lang.Long", "Float");
    	  return this;
      }

      public OmsDevopsWarnSQL andEtimeNotBetween(java.lang.Long value1, java.lang.Long value2) {
          addCriterion("etime", value1, value2, ConditionMode.NOT_BETWEEN, "etime", "java.lang.Long", "Float");
          return this;
      }
        
      public OmsDevopsWarnSQL andEtimeIn(List<java.lang.Long> values) {
          addCriterion("etime", values, ConditionMode.IN, "etime", "java.lang.Long", "Float");
          return this;
      }

      public OmsDevopsWarnSQL andEtimeNotIn(List<java.lang.Long> values) {
          addCriterion("etime", values, ConditionMode.NOT_IN, "etime", "java.lang.Long", "Float");
          return this;
      }
}