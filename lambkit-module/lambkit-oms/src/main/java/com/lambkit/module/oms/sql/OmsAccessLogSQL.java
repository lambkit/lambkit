/**
 * Copyright (c) 2015-2017, Henry Yang 杨勇 (gismail@foxmail.com).
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.lambkit.module.oms.sql;

import com.lambkit.db.sql.ConditionMode;
import com.lambkit.db.sql.Column;
import com.lambkit.db.sql.Columns;
import com.lambkit.db.sql.Example;

import java.io.Serializable;
import java.util.List;

/**
 * @author yangyong 
 * @website: www.lambkit.com
 * @email: gismail@foxmail.com
 * @date ${date}
 * @version 1.0
 * @since 1.0
 */

public class OmsAccessLogSQL extends Columns implements Serializable {
	private static final long serialVersionUID = 1L;
	
	public static OmsAccessLogSQL of() {
		return new OmsAccessLogSQL();
	}
	
	public static OmsAccessLogSQL by(Column column) {
		OmsAccessLogSQL that = new OmsAccessLogSQL();
		that.add(column);
        return that;
    }

    public static OmsAccessLogSQL by(String name, Object value) {
        return (OmsAccessLogSQL) create().eq(name, value);
    }
    
    public Example example() {
    	return Example.create("oms_access_log", this);
    }
    
    /**
     * equals
     *
     * @param name
     * @param value
     * @return
     */
    public OmsAccessLogSQL eq(String name, Object value) {
    	super.eq(name, value);
        return this;
    }

    /**
     * not equals !=
     *
     * @param name
     * @param value
     * @return
     */
    public OmsAccessLogSQL ne(String name, Object value) {
    	super.ne(name, value);
        return this;
    }


    /**
     * like
     *
     * @param name
     * @param value
     * @return
     */

    public OmsAccessLogSQL like(String name, Object value) {
    	super.like(name, value);
        return this;
    }
    
    public OmsAccessLogSQL notLike(String name, Object value) {
    	super.notLike(name, value);
        return this;
    }

    /**
     * 大于 great than
     *
     * @param name
     * @param value
     * @return
     */
    public OmsAccessLogSQL gt(String name, Object value) {
    	super.gt(name, value);
        return this;
    }

    /**
     * 大于等于 great or equal
     *
     * @param name
     * @param value
     * @return
     */
    public OmsAccessLogSQL ge(String name, Object value) {
    	super.ge(name, value);
        return this;
    }

    /**
     * 小于 less than
     *
     * @param name
     * @param value
     * @return
     */
    public OmsAccessLogSQL lt(String name, Object value) {
    	super.lt(name, value);
        return this;
    }

    /**
     * 小于等于 less or equal
     *
     * @param name
     * @param value
     * @return
     */
    public OmsAccessLogSQL le(String name, Object value) {
    	super.le(name, value);
        return this;
    }
    
    public OmsAccessLogSQL isnull(String name) {
    	super.isnull(name);
        return this;
    } 

    public OmsAccessLogSQL notNull(String name) {
    	super.notNull(name);
        return this;
    } 
    
    public OmsAccessLogSQL empty(String name) {
    	super.empty(name);
        return this;
    } 
    
    public OmsAccessLogSQL notEmpty(String name) {
    	super.notEmpty(name);
        return this;
    } 
    
    public OmsAccessLogSQL add(Column column) {
    	super.add(column);
    	return this;
    }
    
    /**************************/
	
	public void addCriterion(String name, Object value, ConditionMode logic, String property, String typeHandler, String valueType) {
		 if (value == null) {
			 throw new RuntimeException("Value for " + property + " cannot be null");
		 }
		 add(Column.create(name, value, logic, typeHandler, valueType));
	}
   
	public void addCriterion(String name, Object value1, Object value2, ConditionMode logic, String property, String typeHandler, String valueType) {
		 if (value1 == null || value2 == null) {
			 throw new RuntimeException("Between values for " + property + " cannot be null");
		 }
		 add(Column.create(name, value1, value2, logic, typeHandler, valueType));
	}
		 
	public OmsAccessLogSQL andLogIdIsNull() {
		isnull("log_id");
		return this;
	}
	
	public OmsAccessLogSQL andLogIdIsNotNull() {
		notNull("log_id");
		return this;
	}
	
	public OmsAccessLogSQL andLogIdIsEmpty() {
		empty("log_id");
		return this;
	}

	public OmsAccessLogSQL andLogIdIsNotEmpty() {
		notEmpty("log_id");
		return this;
	}
      public OmsAccessLogSQL andLogIdEqualTo(java.lang.Long value) {
          addCriterion("log_id", value, ConditionMode.EQUAL, "logId", "java.lang.Long", "Float");
          return this;
      }

      public OmsAccessLogSQL andLogIdNotEqualTo(java.lang.Long value) {
          addCriterion("log_id", value, ConditionMode.NOT_EQUAL, "logId", "java.lang.Long", "Float");
          return this;
      }

      public OmsAccessLogSQL andLogIdGreaterThan(java.lang.Long value) {
          addCriterion("log_id", value, ConditionMode.GREATER_THEN, "logId", "java.lang.Long", "Float");
          return this;
      }

      public OmsAccessLogSQL andLogIdGreaterThanOrEqualTo(java.lang.Long value) {
          addCriterion("log_id", value, ConditionMode.GREATER_EQUAL, "logId", "java.lang.Long", "Float");
          return this;
      }

      public OmsAccessLogSQL andLogIdLessThan(java.lang.Long value) {
          addCriterion("log_id", value, ConditionMode.LESS_THEN, "logId", "java.lang.Long", "Float");
          return this;
      }

      public OmsAccessLogSQL andLogIdLessThanOrEqualTo(java.lang.Long value) {
          addCriterion("log_id", value, ConditionMode.LESS_EQUAL, "logId", "java.lang.Long", "Float");
          return this;
      }

      public OmsAccessLogSQL andLogIdBetween(java.lang.Long value1, java.lang.Long value2) {
    	  addCriterion("log_id", value1, value2, ConditionMode.BETWEEN, "logId", "java.lang.Long", "Float");
    	  return this;
      }

      public OmsAccessLogSQL andLogIdNotBetween(java.lang.Long value1, java.lang.Long value2) {
          addCriterion("log_id", value1, value2, ConditionMode.NOT_BETWEEN, "logId", "java.lang.Long", "Float");
          return this;
      }
        
      public OmsAccessLogSQL andLogIdIn(List<java.lang.Long> values) {
          addCriterion("log_id", values, ConditionMode.IN, "logId", "java.lang.Long", "Float");
          return this;
      }

      public OmsAccessLogSQL andLogIdNotIn(List<java.lang.Long> values) {
          addCriterion("log_id", values, ConditionMode.NOT_IN, "logId", "java.lang.Long", "Float");
          return this;
      }
	public OmsAccessLogSQL andDescriptionIsNull() {
		isnull("description");
		return this;
	}
	
	public OmsAccessLogSQL andDescriptionIsNotNull() {
		notNull("description");
		return this;
	}
	
	public OmsAccessLogSQL andDescriptionIsEmpty() {
		empty("description");
		return this;
	}

	public OmsAccessLogSQL andDescriptionIsNotEmpty() {
		notEmpty("description");
		return this;
	}
       public OmsAccessLogSQL andDescriptionLike(java.lang.String value) {
    	   addCriterion("description", value, ConditionMode.FUZZY, "description", "java.lang.String", "Float");
    	   return this;
      }

      public OmsAccessLogSQL andDescriptionNotLike(java.lang.String value) {
          addCriterion("description", value, ConditionMode.NOT_FUZZY, "description", "java.lang.String", "Float");
          return this;
      }
      public OmsAccessLogSQL andDescriptionEqualTo(java.lang.String value) {
          addCriterion("description", value, ConditionMode.EQUAL, "description", "java.lang.String", "String");
          return this;
      }

      public OmsAccessLogSQL andDescriptionNotEqualTo(java.lang.String value) {
          addCriterion("description", value, ConditionMode.NOT_EQUAL, "description", "java.lang.String", "String");
          return this;
      }

      public OmsAccessLogSQL andDescriptionGreaterThan(java.lang.String value) {
          addCriterion("description", value, ConditionMode.GREATER_THEN, "description", "java.lang.String", "String");
          return this;
      }

      public OmsAccessLogSQL andDescriptionGreaterThanOrEqualTo(java.lang.String value) {
          addCriterion("description", value, ConditionMode.GREATER_EQUAL, "description", "java.lang.String", "String");
          return this;
      }

      public OmsAccessLogSQL andDescriptionLessThan(java.lang.String value) {
          addCriterion("description", value, ConditionMode.LESS_THEN, "description", "java.lang.String", "String");
          return this;
      }

      public OmsAccessLogSQL andDescriptionLessThanOrEqualTo(java.lang.String value) {
          addCriterion("description", value, ConditionMode.LESS_EQUAL, "description", "java.lang.String", "String");
          return this;
      }

      public OmsAccessLogSQL andDescriptionBetween(java.lang.String value1, java.lang.String value2) {
    	  addCriterion("description", value1, value2, ConditionMode.BETWEEN, "description", "java.lang.String", "String");
    	  return this;
      }

      public OmsAccessLogSQL andDescriptionNotBetween(java.lang.String value1, java.lang.String value2) {
          addCriterion("description", value1, value2, ConditionMode.NOT_BETWEEN, "description", "java.lang.String", "String");
          return this;
      }
        
      public OmsAccessLogSQL andDescriptionIn(List<java.lang.String> values) {
          addCriterion("description", values, ConditionMode.IN, "description", "java.lang.String", "String");
          return this;
      }

      public OmsAccessLogSQL andDescriptionNotIn(List<java.lang.String> values) {
          addCriterion("description", values, ConditionMode.NOT_IN, "description", "java.lang.String", "String");
          return this;
      }
	public OmsAccessLogSQL andUsernameIsNull() {
		isnull("username");
		return this;
	}
	
	public OmsAccessLogSQL andUsernameIsNotNull() {
		notNull("username");
		return this;
	}
	
	public OmsAccessLogSQL andUsernameIsEmpty() {
		empty("username");
		return this;
	}

	public OmsAccessLogSQL andUsernameIsNotEmpty() {
		notEmpty("username");
		return this;
	}
       public OmsAccessLogSQL andUsernameLike(java.lang.String value) {
    	   addCriterion("username", value, ConditionMode.FUZZY, "username", "java.lang.String", "String");
    	   return this;
      }

      public OmsAccessLogSQL andUsernameNotLike(java.lang.String value) {
          addCriterion("username", value, ConditionMode.NOT_FUZZY, "username", "java.lang.String", "String");
          return this;
      }
      public OmsAccessLogSQL andUsernameEqualTo(java.lang.String value) {
          addCriterion("username", value, ConditionMode.EQUAL, "username", "java.lang.String", "String");
          return this;
      }

      public OmsAccessLogSQL andUsernameNotEqualTo(java.lang.String value) {
          addCriterion("username", value, ConditionMode.NOT_EQUAL, "username", "java.lang.String", "String");
          return this;
      }

      public OmsAccessLogSQL andUsernameGreaterThan(java.lang.String value) {
          addCriterion("username", value, ConditionMode.GREATER_THEN, "username", "java.lang.String", "String");
          return this;
      }

      public OmsAccessLogSQL andUsernameGreaterThanOrEqualTo(java.lang.String value) {
          addCriterion("username", value, ConditionMode.GREATER_EQUAL, "username", "java.lang.String", "String");
          return this;
      }

      public OmsAccessLogSQL andUsernameLessThan(java.lang.String value) {
          addCriterion("username", value, ConditionMode.LESS_THEN, "username", "java.lang.String", "String");
          return this;
      }

      public OmsAccessLogSQL andUsernameLessThanOrEqualTo(java.lang.String value) {
          addCriterion("username", value, ConditionMode.LESS_EQUAL, "username", "java.lang.String", "String");
          return this;
      }

      public OmsAccessLogSQL andUsernameBetween(java.lang.String value1, java.lang.String value2) {
    	  addCriterion("username", value1, value2, ConditionMode.BETWEEN, "username", "java.lang.String", "String");
    	  return this;
      }

      public OmsAccessLogSQL andUsernameNotBetween(java.lang.String value1, java.lang.String value2) {
          addCriterion("username", value1, value2, ConditionMode.NOT_BETWEEN, "username", "java.lang.String", "String");
          return this;
      }
        
      public OmsAccessLogSQL andUsernameIn(List<java.lang.String> values) {
          addCriterion("username", values, ConditionMode.IN, "username", "java.lang.String", "String");
          return this;
      }

      public OmsAccessLogSQL andUsernameNotIn(List<java.lang.String> values) {
          addCriterion("username", values, ConditionMode.NOT_IN, "username", "java.lang.String", "String");
          return this;
      }
	public OmsAccessLogSQL andStartTimeIsNull() {
		isnull("start_time");
		return this;
	}
	
	public OmsAccessLogSQL andStartTimeIsNotNull() {
		notNull("start_time");
		return this;
	}
	
	public OmsAccessLogSQL andStartTimeIsEmpty() {
		empty("start_time");
		return this;
	}

	public OmsAccessLogSQL andStartTimeIsNotEmpty() {
		notEmpty("start_time");
		return this;
	}
      public OmsAccessLogSQL andStartTimeEqualTo(java.util.Date value) {
          addCriterion("start_time", value, ConditionMode.EQUAL, "startTime", "java.util.Date", "String");
          return this;
      }

      public OmsAccessLogSQL andStartTimeNotEqualTo(java.util.Date value) {
          addCriterion("start_time", value, ConditionMode.NOT_EQUAL, "startTime", "java.util.Date", "String");
          return this;
      }

      public OmsAccessLogSQL andStartTimeGreaterThan(java.util.Date value) {
          addCriterion("start_time", value, ConditionMode.GREATER_THEN, "startTime", "java.util.Date", "String");
          return this;
      }

      public OmsAccessLogSQL andStartTimeGreaterThanOrEqualTo(java.util.Date value) {
          addCriterion("start_time", value, ConditionMode.GREATER_EQUAL, "startTime", "java.util.Date", "String");
          return this;
      }

      public OmsAccessLogSQL andStartTimeLessThan(java.util.Date value) {
          addCriterion("start_time", value, ConditionMode.LESS_THEN, "startTime", "java.util.Date", "String");
          return this;
      }

      public OmsAccessLogSQL andStartTimeLessThanOrEqualTo(java.util.Date value) {
          addCriterion("start_time", value, ConditionMode.LESS_EQUAL, "startTime", "java.util.Date", "String");
          return this;
      }

      public OmsAccessLogSQL andStartTimeBetween(java.util.Date value1, java.util.Date value2) {
    	  addCriterion("start_time", value1, value2, ConditionMode.BETWEEN, "startTime", "java.util.Date", "String");
    	  return this;
      }

      public OmsAccessLogSQL andStartTimeNotBetween(java.util.Date value1, java.util.Date value2) {
          addCriterion("start_time", value1, value2, ConditionMode.NOT_BETWEEN, "startTime", "java.util.Date", "String");
          return this;
      }
        
      public OmsAccessLogSQL andStartTimeIn(List<java.util.Date> values) {
          addCriterion("start_time", values, ConditionMode.IN, "startTime", "java.util.Date", "String");
          return this;
      }

      public OmsAccessLogSQL andStartTimeNotIn(List<java.util.Date> values) {
          addCriterion("start_time", values, ConditionMode.NOT_IN, "startTime", "java.util.Date", "String");
          return this;
      }
	public OmsAccessLogSQL andSpendTimeIsNull() {
		isnull("spend_time");
		return this;
	}
	
	public OmsAccessLogSQL andSpendTimeIsNotNull() {
		notNull("spend_time");
		return this;
	}
	
	public OmsAccessLogSQL andSpendTimeIsEmpty() {
		empty("spend_time");
		return this;
	}

	public OmsAccessLogSQL andSpendTimeIsNotEmpty() {
		notEmpty("spend_time");
		return this;
	}
      public OmsAccessLogSQL andSpendTimeEqualTo(java.lang.Integer value) {
          addCriterion("spend_time", value, ConditionMode.EQUAL, "spendTime", "java.lang.Integer", "Float");
          return this;
      }

      public OmsAccessLogSQL andSpendTimeNotEqualTo(java.lang.Integer value) {
          addCriterion("spend_time", value, ConditionMode.NOT_EQUAL, "spendTime", "java.lang.Integer", "Float");
          return this;
      }

      public OmsAccessLogSQL andSpendTimeGreaterThan(java.lang.Integer value) {
          addCriterion("spend_time", value, ConditionMode.GREATER_THEN, "spendTime", "java.lang.Integer", "Float");
          return this;
      }

      public OmsAccessLogSQL andSpendTimeGreaterThanOrEqualTo(java.lang.Integer value) {
          addCriterion("spend_time", value, ConditionMode.GREATER_EQUAL, "spendTime", "java.lang.Integer", "Float");
          return this;
      }

      public OmsAccessLogSQL andSpendTimeLessThan(java.lang.Integer value) {
          addCriterion("spend_time", value, ConditionMode.LESS_THEN, "spendTime", "java.lang.Integer", "Float");
          return this;
      }

      public OmsAccessLogSQL andSpendTimeLessThanOrEqualTo(java.lang.Integer value) {
          addCriterion("spend_time", value, ConditionMode.LESS_EQUAL, "spendTime", "java.lang.Integer", "Float");
          return this;
      }

      public OmsAccessLogSQL andSpendTimeBetween(java.lang.Integer value1, java.lang.Integer value2) {
    	  addCriterion("spend_time", value1, value2, ConditionMode.BETWEEN, "spendTime", "java.lang.Integer", "Float");
    	  return this;
      }

      public OmsAccessLogSQL andSpendTimeNotBetween(java.lang.Integer value1, java.lang.Integer value2) {
          addCriterion("spend_time", value1, value2, ConditionMode.NOT_BETWEEN, "spendTime", "java.lang.Integer", "Float");
          return this;
      }
        
      public OmsAccessLogSQL andSpendTimeIn(List<java.lang.Integer> values) {
          addCriterion("spend_time", values, ConditionMode.IN, "spendTime", "java.lang.Integer", "Float");
          return this;
      }

      public OmsAccessLogSQL andSpendTimeNotIn(List<java.lang.Integer> values) {
          addCriterion("spend_time", values, ConditionMode.NOT_IN, "spendTime", "java.lang.Integer", "Float");
          return this;
      }
	public OmsAccessLogSQL andBasePathIsNull() {
		isnull("base_path");
		return this;
	}
	
	public OmsAccessLogSQL andBasePathIsNotNull() {
		notNull("base_path");
		return this;
	}
	
	public OmsAccessLogSQL andBasePathIsEmpty() {
		empty("base_path");
		return this;
	}

	public OmsAccessLogSQL andBasePathIsNotEmpty() {
		notEmpty("base_path");
		return this;
	}
       public OmsAccessLogSQL andBasePathLike(java.lang.String value) {
    	   addCriterion("base_path", value, ConditionMode.FUZZY, "basePath", "java.lang.String", "Float");
    	   return this;
      }

      public OmsAccessLogSQL andBasePathNotLike(java.lang.String value) {
          addCriterion("base_path", value, ConditionMode.NOT_FUZZY, "basePath", "java.lang.String", "Float");
          return this;
      }
      public OmsAccessLogSQL andBasePathEqualTo(java.lang.String value) {
          addCriterion("base_path", value, ConditionMode.EQUAL, "basePath", "java.lang.String", "String");
          return this;
      }

      public OmsAccessLogSQL andBasePathNotEqualTo(java.lang.String value) {
          addCriterion("base_path", value, ConditionMode.NOT_EQUAL, "basePath", "java.lang.String", "String");
          return this;
      }

      public OmsAccessLogSQL andBasePathGreaterThan(java.lang.String value) {
          addCriterion("base_path", value, ConditionMode.GREATER_THEN, "basePath", "java.lang.String", "String");
          return this;
      }

      public OmsAccessLogSQL andBasePathGreaterThanOrEqualTo(java.lang.String value) {
          addCriterion("base_path", value, ConditionMode.GREATER_EQUAL, "basePath", "java.lang.String", "String");
          return this;
      }

      public OmsAccessLogSQL andBasePathLessThan(java.lang.String value) {
          addCriterion("base_path", value, ConditionMode.LESS_THEN, "basePath", "java.lang.String", "String");
          return this;
      }

      public OmsAccessLogSQL andBasePathLessThanOrEqualTo(java.lang.String value) {
          addCriterion("base_path", value, ConditionMode.LESS_EQUAL, "basePath", "java.lang.String", "String");
          return this;
      }

      public OmsAccessLogSQL andBasePathBetween(java.lang.String value1, java.lang.String value2) {
    	  addCriterion("base_path", value1, value2, ConditionMode.BETWEEN, "basePath", "java.lang.String", "String");
    	  return this;
      }

      public OmsAccessLogSQL andBasePathNotBetween(java.lang.String value1, java.lang.String value2) {
          addCriterion("base_path", value1, value2, ConditionMode.NOT_BETWEEN, "basePath", "java.lang.String", "String");
          return this;
      }
        
      public OmsAccessLogSQL andBasePathIn(List<java.lang.String> values) {
          addCriterion("base_path", values, ConditionMode.IN, "basePath", "java.lang.String", "String");
          return this;
      }

      public OmsAccessLogSQL andBasePathNotIn(List<java.lang.String> values) {
          addCriterion("base_path", values, ConditionMode.NOT_IN, "basePath", "java.lang.String", "String");
          return this;
      }
	public OmsAccessLogSQL andUriIsNull() {
		isnull("uri");
		return this;
	}
	
	public OmsAccessLogSQL andUriIsNotNull() {
		notNull("uri");
		return this;
	}
	
	public OmsAccessLogSQL andUriIsEmpty() {
		empty("uri");
		return this;
	}

	public OmsAccessLogSQL andUriIsNotEmpty() {
		notEmpty("uri");
		return this;
	}
       public OmsAccessLogSQL andUriLike(java.lang.String value) {
    	   addCriterion("uri", value, ConditionMode.FUZZY, "uri", "java.lang.String", "String");
    	   return this;
      }

      public OmsAccessLogSQL andUriNotLike(java.lang.String value) {
          addCriterion("uri", value, ConditionMode.NOT_FUZZY, "uri", "java.lang.String", "String");
          return this;
      }
      public OmsAccessLogSQL andUriEqualTo(java.lang.String value) {
          addCriterion("uri", value, ConditionMode.EQUAL, "uri", "java.lang.String", "String");
          return this;
      }

      public OmsAccessLogSQL andUriNotEqualTo(java.lang.String value) {
          addCriterion("uri", value, ConditionMode.NOT_EQUAL, "uri", "java.lang.String", "String");
          return this;
      }

      public OmsAccessLogSQL andUriGreaterThan(java.lang.String value) {
          addCriterion("uri", value, ConditionMode.GREATER_THEN, "uri", "java.lang.String", "String");
          return this;
      }

      public OmsAccessLogSQL andUriGreaterThanOrEqualTo(java.lang.String value) {
          addCriterion("uri", value, ConditionMode.GREATER_EQUAL, "uri", "java.lang.String", "String");
          return this;
      }

      public OmsAccessLogSQL andUriLessThan(java.lang.String value) {
          addCriterion("uri", value, ConditionMode.LESS_THEN, "uri", "java.lang.String", "String");
          return this;
      }

      public OmsAccessLogSQL andUriLessThanOrEqualTo(java.lang.String value) {
          addCriterion("uri", value, ConditionMode.LESS_EQUAL, "uri", "java.lang.String", "String");
          return this;
      }

      public OmsAccessLogSQL andUriBetween(java.lang.String value1, java.lang.String value2) {
    	  addCriterion("uri", value1, value2, ConditionMode.BETWEEN, "uri", "java.lang.String", "String");
    	  return this;
      }

      public OmsAccessLogSQL andUriNotBetween(java.lang.String value1, java.lang.String value2) {
          addCriterion("uri", value1, value2, ConditionMode.NOT_BETWEEN, "uri", "java.lang.String", "String");
          return this;
      }
        
      public OmsAccessLogSQL andUriIn(List<java.lang.String> values) {
          addCriterion("uri", values, ConditionMode.IN, "uri", "java.lang.String", "String");
          return this;
      }

      public OmsAccessLogSQL andUriNotIn(List<java.lang.String> values) {
          addCriterion("uri", values, ConditionMode.NOT_IN, "uri", "java.lang.String", "String");
          return this;
      }
	public OmsAccessLogSQL andUrlIsNull() {
		isnull("url");
		return this;
	}
	
	public OmsAccessLogSQL andUrlIsNotNull() {
		notNull("url");
		return this;
	}
	
	public OmsAccessLogSQL andUrlIsEmpty() {
		empty("url");
		return this;
	}

	public OmsAccessLogSQL andUrlIsNotEmpty() {
		notEmpty("url");
		return this;
	}
       public OmsAccessLogSQL andUrlLike(java.lang.String value) {
    	   addCriterion("url", value, ConditionMode.FUZZY, "url", "java.lang.String", "String");
    	   return this;
      }

      public OmsAccessLogSQL andUrlNotLike(java.lang.String value) {
          addCriterion("url", value, ConditionMode.NOT_FUZZY, "url", "java.lang.String", "String");
          return this;
      }
      public OmsAccessLogSQL andUrlEqualTo(java.lang.String value) {
          addCriterion("url", value, ConditionMode.EQUAL, "url", "java.lang.String", "String");
          return this;
      }

      public OmsAccessLogSQL andUrlNotEqualTo(java.lang.String value) {
          addCriterion("url", value, ConditionMode.NOT_EQUAL, "url", "java.lang.String", "String");
          return this;
      }

      public OmsAccessLogSQL andUrlGreaterThan(java.lang.String value) {
          addCriterion("url", value, ConditionMode.GREATER_THEN, "url", "java.lang.String", "String");
          return this;
      }

      public OmsAccessLogSQL andUrlGreaterThanOrEqualTo(java.lang.String value) {
          addCriterion("url", value, ConditionMode.GREATER_EQUAL, "url", "java.lang.String", "String");
          return this;
      }

      public OmsAccessLogSQL andUrlLessThan(java.lang.String value) {
          addCriterion("url", value, ConditionMode.LESS_THEN, "url", "java.lang.String", "String");
          return this;
      }

      public OmsAccessLogSQL andUrlLessThanOrEqualTo(java.lang.String value) {
          addCriterion("url", value, ConditionMode.LESS_EQUAL, "url", "java.lang.String", "String");
          return this;
      }

      public OmsAccessLogSQL andUrlBetween(java.lang.String value1, java.lang.String value2) {
    	  addCriterion("url", value1, value2, ConditionMode.BETWEEN, "url", "java.lang.String", "String");
    	  return this;
      }

      public OmsAccessLogSQL andUrlNotBetween(java.lang.String value1, java.lang.String value2) {
          addCriterion("url", value1, value2, ConditionMode.NOT_BETWEEN, "url", "java.lang.String", "String");
          return this;
      }
        
      public OmsAccessLogSQL andUrlIn(List<java.lang.String> values) {
          addCriterion("url", values, ConditionMode.IN, "url", "java.lang.String", "String");
          return this;
      }

      public OmsAccessLogSQL andUrlNotIn(List<java.lang.String> values) {
          addCriterion("url", values, ConditionMode.NOT_IN, "url", "java.lang.String", "String");
          return this;
      }
	public OmsAccessLogSQL andMethodIsNull() {
		isnull("method");
		return this;
	}
	
	public OmsAccessLogSQL andMethodIsNotNull() {
		notNull("method");
		return this;
	}
	
	public OmsAccessLogSQL andMethodIsEmpty() {
		empty("method");
		return this;
	}

	public OmsAccessLogSQL andMethodIsNotEmpty() {
		notEmpty("method");
		return this;
	}
       public OmsAccessLogSQL andMethodLike(java.lang.String value) {
    	   addCriterion("method", value, ConditionMode.FUZZY, "method", "java.lang.String", "String");
    	   return this;
      }

      public OmsAccessLogSQL andMethodNotLike(java.lang.String value) {
          addCriterion("method", value, ConditionMode.NOT_FUZZY, "method", "java.lang.String", "String");
          return this;
      }
      public OmsAccessLogSQL andMethodEqualTo(java.lang.String value) {
          addCriterion("method", value, ConditionMode.EQUAL, "method", "java.lang.String", "String");
          return this;
      }

      public OmsAccessLogSQL andMethodNotEqualTo(java.lang.String value) {
          addCriterion("method", value, ConditionMode.NOT_EQUAL, "method", "java.lang.String", "String");
          return this;
      }

      public OmsAccessLogSQL andMethodGreaterThan(java.lang.String value) {
          addCriterion("method", value, ConditionMode.GREATER_THEN, "method", "java.lang.String", "String");
          return this;
      }

      public OmsAccessLogSQL andMethodGreaterThanOrEqualTo(java.lang.String value) {
          addCriterion("method", value, ConditionMode.GREATER_EQUAL, "method", "java.lang.String", "String");
          return this;
      }

      public OmsAccessLogSQL andMethodLessThan(java.lang.String value) {
          addCriterion("method", value, ConditionMode.LESS_THEN, "method", "java.lang.String", "String");
          return this;
      }

      public OmsAccessLogSQL andMethodLessThanOrEqualTo(java.lang.String value) {
          addCriterion("method", value, ConditionMode.LESS_EQUAL, "method", "java.lang.String", "String");
          return this;
      }

      public OmsAccessLogSQL andMethodBetween(java.lang.String value1, java.lang.String value2) {
    	  addCriterion("method", value1, value2, ConditionMode.BETWEEN, "method", "java.lang.String", "String");
    	  return this;
      }

      public OmsAccessLogSQL andMethodNotBetween(java.lang.String value1, java.lang.String value2) {
          addCriterion("method", value1, value2, ConditionMode.NOT_BETWEEN, "method", "java.lang.String", "String");
          return this;
      }
        
      public OmsAccessLogSQL andMethodIn(List<java.lang.String> values) {
          addCriterion("method", values, ConditionMode.IN, "method", "java.lang.String", "String");
          return this;
      }

      public OmsAccessLogSQL andMethodNotIn(List<java.lang.String> values) {
          addCriterion("method", values, ConditionMode.NOT_IN, "method", "java.lang.String", "String");
          return this;
      }
	public OmsAccessLogSQL andParameterIsNull() {
		isnull("parameter");
		return this;
	}
	
	public OmsAccessLogSQL andParameterIsNotNull() {
		notNull("parameter");
		return this;
	}
	
	public OmsAccessLogSQL andParameterIsEmpty() {
		empty("parameter");
		return this;
	}

	public OmsAccessLogSQL andParameterIsNotEmpty() {
		notEmpty("parameter");
		return this;
	}
       public OmsAccessLogSQL andParameterLike(java.lang.String value) {
    	   addCriterion("parameter", value, ConditionMode.FUZZY, "parameter", "java.lang.String", "String");
    	   return this;
      }

      public OmsAccessLogSQL andParameterNotLike(java.lang.String value) {
          addCriterion("parameter", value, ConditionMode.NOT_FUZZY, "parameter", "java.lang.String", "String");
          return this;
      }
      public OmsAccessLogSQL andParameterEqualTo(java.lang.String value) {
          addCriterion("parameter", value, ConditionMode.EQUAL, "parameter", "java.lang.String", "String");
          return this;
      }

      public OmsAccessLogSQL andParameterNotEqualTo(java.lang.String value) {
          addCriterion("parameter", value, ConditionMode.NOT_EQUAL, "parameter", "java.lang.String", "String");
          return this;
      }

      public OmsAccessLogSQL andParameterGreaterThan(java.lang.String value) {
          addCriterion("parameter", value, ConditionMode.GREATER_THEN, "parameter", "java.lang.String", "String");
          return this;
      }

      public OmsAccessLogSQL andParameterGreaterThanOrEqualTo(java.lang.String value) {
          addCriterion("parameter", value, ConditionMode.GREATER_EQUAL, "parameter", "java.lang.String", "String");
          return this;
      }

      public OmsAccessLogSQL andParameterLessThan(java.lang.String value) {
          addCriterion("parameter", value, ConditionMode.LESS_THEN, "parameter", "java.lang.String", "String");
          return this;
      }

      public OmsAccessLogSQL andParameterLessThanOrEqualTo(java.lang.String value) {
          addCriterion("parameter", value, ConditionMode.LESS_EQUAL, "parameter", "java.lang.String", "String");
          return this;
      }

      public OmsAccessLogSQL andParameterBetween(java.lang.String value1, java.lang.String value2) {
    	  addCriterion("parameter", value1, value2, ConditionMode.BETWEEN, "parameter", "java.lang.String", "String");
    	  return this;
      }

      public OmsAccessLogSQL andParameterNotBetween(java.lang.String value1, java.lang.String value2) {
          addCriterion("parameter", value1, value2, ConditionMode.NOT_BETWEEN, "parameter", "java.lang.String", "String");
          return this;
      }
        
      public OmsAccessLogSQL andParameterIn(List<java.lang.String> values) {
          addCriterion("parameter", values, ConditionMode.IN, "parameter", "java.lang.String", "String");
          return this;
      }

      public OmsAccessLogSQL andParameterNotIn(List<java.lang.String> values) {
          addCriterion("parameter", values, ConditionMode.NOT_IN, "parameter", "java.lang.String", "String");
          return this;
      }
	public OmsAccessLogSQL andUserAgentIsNull() {
		isnull("user_agent");
		return this;
	}
	
	public OmsAccessLogSQL andUserAgentIsNotNull() {
		notNull("user_agent");
		return this;
	}
	
	public OmsAccessLogSQL andUserAgentIsEmpty() {
		empty("user_agent");
		return this;
	}

	public OmsAccessLogSQL andUserAgentIsNotEmpty() {
		notEmpty("user_agent");
		return this;
	}
       public OmsAccessLogSQL andUserAgentLike(java.lang.String value) {
    	   addCriterion("user_agent", value, ConditionMode.FUZZY, "userAgent", "java.lang.String", "String");
    	   return this;
      }

      public OmsAccessLogSQL andUserAgentNotLike(java.lang.String value) {
          addCriterion("user_agent", value, ConditionMode.NOT_FUZZY, "userAgent", "java.lang.String", "String");
          return this;
      }
      public OmsAccessLogSQL andUserAgentEqualTo(java.lang.String value) {
          addCriterion("user_agent", value, ConditionMode.EQUAL, "userAgent", "java.lang.String", "String");
          return this;
      }

      public OmsAccessLogSQL andUserAgentNotEqualTo(java.lang.String value) {
          addCriterion("user_agent", value, ConditionMode.NOT_EQUAL, "userAgent", "java.lang.String", "String");
          return this;
      }

      public OmsAccessLogSQL andUserAgentGreaterThan(java.lang.String value) {
          addCriterion("user_agent", value, ConditionMode.GREATER_THEN, "userAgent", "java.lang.String", "String");
          return this;
      }

      public OmsAccessLogSQL andUserAgentGreaterThanOrEqualTo(java.lang.String value) {
          addCriterion("user_agent", value, ConditionMode.GREATER_EQUAL, "userAgent", "java.lang.String", "String");
          return this;
      }

      public OmsAccessLogSQL andUserAgentLessThan(java.lang.String value) {
          addCriterion("user_agent", value, ConditionMode.LESS_THEN, "userAgent", "java.lang.String", "String");
          return this;
      }

      public OmsAccessLogSQL andUserAgentLessThanOrEqualTo(java.lang.String value) {
          addCriterion("user_agent", value, ConditionMode.LESS_EQUAL, "userAgent", "java.lang.String", "String");
          return this;
      }

      public OmsAccessLogSQL andUserAgentBetween(java.lang.String value1, java.lang.String value2) {
    	  addCriterion("user_agent", value1, value2, ConditionMode.BETWEEN, "userAgent", "java.lang.String", "String");
    	  return this;
      }

      public OmsAccessLogSQL andUserAgentNotBetween(java.lang.String value1, java.lang.String value2) {
          addCriterion("user_agent", value1, value2, ConditionMode.NOT_BETWEEN, "userAgent", "java.lang.String", "String");
          return this;
      }
        
      public OmsAccessLogSQL andUserAgentIn(List<java.lang.String> values) {
          addCriterion("user_agent", values, ConditionMode.IN, "userAgent", "java.lang.String", "String");
          return this;
      }

      public OmsAccessLogSQL andUserAgentNotIn(List<java.lang.String> values) {
          addCriterion("user_agent", values, ConditionMode.NOT_IN, "userAgent", "java.lang.String", "String");
          return this;
      }
	public OmsAccessLogSQL andIpIsNull() {
		isnull("ip");
		return this;
	}
	
	public OmsAccessLogSQL andIpIsNotNull() {
		notNull("ip");
		return this;
	}
	
	public OmsAccessLogSQL andIpIsEmpty() {
		empty("ip");
		return this;
	}

	public OmsAccessLogSQL andIpIsNotEmpty() {
		notEmpty("ip");
		return this;
	}
       public OmsAccessLogSQL andIpLike(java.lang.String value) {
    	   addCriterion("ip", value, ConditionMode.FUZZY, "ip", "java.lang.String", "String");
    	   return this;
      }

      public OmsAccessLogSQL andIpNotLike(java.lang.String value) {
          addCriterion("ip", value, ConditionMode.NOT_FUZZY, "ip", "java.lang.String", "String");
          return this;
      }
      public OmsAccessLogSQL andIpEqualTo(java.lang.String value) {
          addCriterion("ip", value, ConditionMode.EQUAL, "ip", "java.lang.String", "String");
          return this;
      }

      public OmsAccessLogSQL andIpNotEqualTo(java.lang.String value) {
          addCriterion("ip", value, ConditionMode.NOT_EQUAL, "ip", "java.lang.String", "String");
          return this;
      }

      public OmsAccessLogSQL andIpGreaterThan(java.lang.String value) {
          addCriterion("ip", value, ConditionMode.GREATER_THEN, "ip", "java.lang.String", "String");
          return this;
      }

      public OmsAccessLogSQL andIpGreaterThanOrEqualTo(java.lang.String value) {
          addCriterion("ip", value, ConditionMode.GREATER_EQUAL, "ip", "java.lang.String", "String");
          return this;
      }

      public OmsAccessLogSQL andIpLessThan(java.lang.String value) {
          addCriterion("ip", value, ConditionMode.LESS_THEN, "ip", "java.lang.String", "String");
          return this;
      }

      public OmsAccessLogSQL andIpLessThanOrEqualTo(java.lang.String value) {
          addCriterion("ip", value, ConditionMode.LESS_EQUAL, "ip", "java.lang.String", "String");
          return this;
      }

      public OmsAccessLogSQL andIpBetween(java.lang.String value1, java.lang.String value2) {
    	  addCriterion("ip", value1, value2, ConditionMode.BETWEEN, "ip", "java.lang.String", "String");
    	  return this;
      }

      public OmsAccessLogSQL andIpNotBetween(java.lang.String value1, java.lang.String value2) {
          addCriterion("ip", value1, value2, ConditionMode.NOT_BETWEEN, "ip", "java.lang.String", "String");
          return this;
      }
        
      public OmsAccessLogSQL andIpIn(List<java.lang.String> values) {
          addCriterion("ip", values, ConditionMode.IN, "ip", "java.lang.String", "String");
          return this;
      }

      public OmsAccessLogSQL andIpNotIn(List<java.lang.String> values) {
          addCriterion("ip", values, ConditionMode.NOT_IN, "ip", "java.lang.String", "String");
          return this;
      }
	public OmsAccessLogSQL andResultIsNull() {
		isnull("result");
		return this;
	}
	
	public OmsAccessLogSQL andResultIsNotNull() {
		notNull("result");
		return this;
	}
	
	public OmsAccessLogSQL andResultIsEmpty() {
		empty("result");
		return this;
	}

	public OmsAccessLogSQL andResultIsNotEmpty() {
		notEmpty("result");
		return this;
	}
       public OmsAccessLogSQL andResultLike(java.lang.String value) {
    	   addCriterion("result", value, ConditionMode.FUZZY, "result", "java.lang.String", "String");
    	   return this;
      }

      public OmsAccessLogSQL andResultNotLike(java.lang.String value) {
          addCriterion("result", value, ConditionMode.NOT_FUZZY, "result", "java.lang.String", "String");
          return this;
      }
      public OmsAccessLogSQL andResultEqualTo(java.lang.String value) {
          addCriterion("result", value, ConditionMode.EQUAL, "result", "java.lang.String", "String");
          return this;
      }

      public OmsAccessLogSQL andResultNotEqualTo(java.lang.String value) {
          addCriterion("result", value, ConditionMode.NOT_EQUAL, "result", "java.lang.String", "String");
          return this;
      }

      public OmsAccessLogSQL andResultGreaterThan(java.lang.String value) {
          addCriterion("result", value, ConditionMode.GREATER_THEN, "result", "java.lang.String", "String");
          return this;
      }

      public OmsAccessLogSQL andResultGreaterThanOrEqualTo(java.lang.String value) {
          addCriterion("result", value, ConditionMode.GREATER_EQUAL, "result", "java.lang.String", "String");
          return this;
      }

      public OmsAccessLogSQL andResultLessThan(java.lang.String value) {
          addCriterion("result", value, ConditionMode.LESS_THEN, "result", "java.lang.String", "String");
          return this;
      }

      public OmsAccessLogSQL andResultLessThanOrEqualTo(java.lang.String value) {
          addCriterion("result", value, ConditionMode.LESS_EQUAL, "result", "java.lang.String", "String");
          return this;
      }

      public OmsAccessLogSQL andResultBetween(java.lang.String value1, java.lang.String value2) {
    	  addCriterion("result", value1, value2, ConditionMode.BETWEEN, "result", "java.lang.String", "String");
    	  return this;
      }

      public OmsAccessLogSQL andResultNotBetween(java.lang.String value1, java.lang.String value2) {
          addCriterion("result", value1, value2, ConditionMode.NOT_BETWEEN, "result", "java.lang.String", "String");
          return this;
      }
        
      public OmsAccessLogSQL andResultIn(List<java.lang.String> values) {
          addCriterion("result", values, ConditionMode.IN, "result", "java.lang.String", "String");
          return this;
      }

      public OmsAccessLogSQL andResultNotIn(List<java.lang.String> values) {
          addCriterion("result", values, ConditionMode.NOT_IN, "result", "java.lang.String", "String");
          return this;
      }
	public OmsAccessLogSQL andPermissionsIsNull() {
		isnull("permissions");
		return this;
	}
	
	public OmsAccessLogSQL andPermissionsIsNotNull() {
		notNull("permissions");
		return this;
	}
	
	public OmsAccessLogSQL andPermissionsIsEmpty() {
		empty("permissions");
		return this;
	}

	public OmsAccessLogSQL andPermissionsIsNotEmpty() {
		notEmpty("permissions");
		return this;
	}
       public OmsAccessLogSQL andPermissionsLike(java.lang.String value) {
    	   addCriterion("permissions", value, ConditionMode.FUZZY, "permissions", "java.lang.String", "String");
    	   return this;
      }

      public OmsAccessLogSQL andPermissionsNotLike(java.lang.String value) {
          addCriterion("permissions", value, ConditionMode.NOT_FUZZY, "permissions", "java.lang.String", "String");
          return this;
      }
      public OmsAccessLogSQL andPermissionsEqualTo(java.lang.String value) {
          addCriterion("permissions", value, ConditionMode.EQUAL, "permissions", "java.lang.String", "String");
          return this;
      }

      public OmsAccessLogSQL andPermissionsNotEqualTo(java.lang.String value) {
          addCriterion("permissions", value, ConditionMode.NOT_EQUAL, "permissions", "java.lang.String", "String");
          return this;
      }

      public OmsAccessLogSQL andPermissionsGreaterThan(java.lang.String value) {
          addCriterion("permissions", value, ConditionMode.GREATER_THEN, "permissions", "java.lang.String", "String");
          return this;
      }

      public OmsAccessLogSQL andPermissionsGreaterThanOrEqualTo(java.lang.String value) {
          addCriterion("permissions", value, ConditionMode.GREATER_EQUAL, "permissions", "java.lang.String", "String");
          return this;
      }

      public OmsAccessLogSQL andPermissionsLessThan(java.lang.String value) {
          addCriterion("permissions", value, ConditionMode.LESS_THEN, "permissions", "java.lang.String", "String");
          return this;
      }

      public OmsAccessLogSQL andPermissionsLessThanOrEqualTo(java.lang.String value) {
          addCriterion("permissions", value, ConditionMode.LESS_EQUAL, "permissions", "java.lang.String", "String");
          return this;
      }

      public OmsAccessLogSQL andPermissionsBetween(java.lang.String value1, java.lang.String value2) {
    	  addCriterion("permissions", value1, value2, ConditionMode.BETWEEN, "permissions", "java.lang.String", "String");
    	  return this;
      }

      public OmsAccessLogSQL andPermissionsNotBetween(java.lang.String value1, java.lang.String value2) {
          addCriterion("permissions", value1, value2, ConditionMode.NOT_BETWEEN, "permissions", "java.lang.String", "String");
          return this;
      }
        
      public OmsAccessLogSQL andPermissionsIn(List<java.lang.String> values) {
          addCriterion("permissions", values, ConditionMode.IN, "permissions", "java.lang.String", "String");
          return this;
      }

      public OmsAccessLogSQL andPermissionsNotIn(List<java.lang.String> values) {
          addCriterion("permissions", values, ConditionMode.NOT_IN, "permissions", "java.lang.String", "String");
          return this;
      }
	public OmsAccessLogSQL andSessionIdIsNull() {
		isnull("session_id");
		return this;
	}
	
	public OmsAccessLogSQL andSessionIdIsNotNull() {
		notNull("session_id");
		return this;
	}
	
	public OmsAccessLogSQL andSessionIdIsEmpty() {
		empty("session_id");
		return this;
	}

	public OmsAccessLogSQL andSessionIdIsNotEmpty() {
		notEmpty("session_id");
		return this;
	}
       public OmsAccessLogSQL andSessionIdLike(java.lang.String value) {
    	   addCriterion("session_id", value, ConditionMode.FUZZY, "sessionId", "java.lang.String", "String");
    	   return this;
      }

      public OmsAccessLogSQL andSessionIdNotLike(java.lang.String value) {
          addCriterion("session_id", value, ConditionMode.NOT_FUZZY, "sessionId", "java.lang.String", "String");
          return this;
      }
      public OmsAccessLogSQL andSessionIdEqualTo(java.lang.String value) {
          addCriterion("session_id", value, ConditionMode.EQUAL, "sessionId", "java.lang.String", "String");
          return this;
      }

      public OmsAccessLogSQL andSessionIdNotEqualTo(java.lang.String value) {
          addCriterion("session_id", value, ConditionMode.NOT_EQUAL, "sessionId", "java.lang.String", "String");
          return this;
      }

      public OmsAccessLogSQL andSessionIdGreaterThan(java.lang.String value) {
          addCriterion("session_id", value, ConditionMode.GREATER_THEN, "sessionId", "java.lang.String", "String");
          return this;
      }

      public OmsAccessLogSQL andSessionIdGreaterThanOrEqualTo(java.lang.String value) {
          addCriterion("session_id", value, ConditionMode.GREATER_EQUAL, "sessionId", "java.lang.String", "String");
          return this;
      }

      public OmsAccessLogSQL andSessionIdLessThan(java.lang.String value) {
          addCriterion("session_id", value, ConditionMode.LESS_THEN, "sessionId", "java.lang.String", "String");
          return this;
      }

      public OmsAccessLogSQL andSessionIdLessThanOrEqualTo(java.lang.String value) {
          addCriterion("session_id", value, ConditionMode.LESS_EQUAL, "sessionId", "java.lang.String", "String");
          return this;
      }

      public OmsAccessLogSQL andSessionIdBetween(java.lang.String value1, java.lang.String value2) {
    	  addCriterion("session_id", value1, value2, ConditionMode.BETWEEN, "sessionId", "java.lang.String", "String");
    	  return this;
      }

      public OmsAccessLogSQL andSessionIdNotBetween(java.lang.String value1, java.lang.String value2) {
          addCriterion("session_id", value1, value2, ConditionMode.NOT_BETWEEN, "sessionId", "java.lang.String", "String");
          return this;
      }
        
      public OmsAccessLogSQL andSessionIdIn(List<java.lang.String> values) {
          addCriterion("session_id", values, ConditionMode.IN, "sessionId", "java.lang.String", "String");
          return this;
      }

      public OmsAccessLogSQL andSessionIdNotIn(List<java.lang.String> values) {
          addCriterion("session_id", values, ConditionMode.NOT_IN, "sessionId", "java.lang.String", "String");
          return this;
      }
}