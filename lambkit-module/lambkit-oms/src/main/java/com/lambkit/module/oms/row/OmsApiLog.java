/**
 * Copyright (c) 2015-2017, Henry Yang 杨勇 (gismail@foxmail.com).
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.lambkit.module.oms.row;

import com.lambkit.db.RowModel;

/**
 * @author yangyong 
 * @website: www.lambkit.com
 * @email: gismail@foxmail.com
 * @date ${date}
 * @version 1.0
 * @since 1.0
 */

public class OmsApiLog extends RowModel<OmsApiLog> {
	public OmsApiLog() {
		setTableName("oms_api_log");
		setPrimaryKey("infocode");
	}
	public java.lang.String getInfocode() {
		return this.get("infocode");
	}
	public void setInfocode(java.lang.String infocode) {
		this.set("infocode", infocode);
	}
	public java.lang.String getNodecode() {
		return this.get("nodecode");
	}
	public void setNodecode(java.lang.String nodecode) {
		this.set("nodecode", nodecode);
	}
	public java.lang.String getTablename() {
		return this.get("tablename");
	}
	public void setTablename(java.lang.String tablename) {
		this.set("tablename", tablename);
	}
	public java.lang.String getGuid() {
		return this.get("guid");
	}
	public void setGuid(java.lang.String guid) {
		this.set("guid", guid);
	}
	public java.lang.String getMark() {
		return this.get("mark");
	}
	public void setMark(java.lang.String mark) {
		this.set("mark", mark);
	}
	public java.lang.String getType() {
		return this.get("type");
	}
	public void setType(java.lang.String type) {
		this.set("type", type);
	}
	public java.lang.Integer getStatus() {
		return this.get("status");
	}
	public void setStatus(java.lang.Integer status) {
		this.set("status", status);
	}
	public java.lang.String getMsg() {
		return this.get("msg");
	}
	public void setMsg(java.lang.String msg) {
		this.set("msg", msg);
	}
}
