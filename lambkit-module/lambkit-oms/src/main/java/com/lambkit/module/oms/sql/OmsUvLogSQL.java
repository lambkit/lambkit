/**
 * Copyright (c) 2015-2017, Henry Yang 杨勇 (gismail@foxmail.com).
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.lambkit.module.oms.sql;

import com.lambkit.db.sql.ConditionMode;
import com.lambkit.db.sql.Column;
import com.lambkit.db.sql.Columns;
import com.lambkit.db.sql.Example;

import java.io.Serializable;
import java.util.List;

/**
 * @author yangyong 
 * @website: www.lambkit.com
 * @email: gismail@foxmail.com
 * @date ${date}
 * @version 1.0
 * @since 1.0
 */

public class OmsUvLogSQL extends Columns implements Serializable {
	private static final long serialVersionUID = 1L;
	
	public static OmsUvLogSQL of() {
		return new OmsUvLogSQL();
	}
	
	public static OmsUvLogSQL by(Column column) {
		OmsUvLogSQL that = new OmsUvLogSQL();
		that.add(column);
        return that;
    }

    public static OmsUvLogSQL by(String name, Object value) {
        return (OmsUvLogSQL) create().eq(name, value);
    }
    
    public Example example() {
    	return Example.create("oms_uv_log", this);
    }
    
    /**
     * equals
     *
     * @param name
     * @param value
     * @return
     */
    public OmsUvLogSQL eq(String name, Object value) {
    	super.eq(name, value);
        return this;
    }

    /**
     * not equals !=
     *
     * @param name
     * @param value
     * @return
     */
    public OmsUvLogSQL ne(String name, Object value) {
    	super.ne(name, value);
        return this;
    }


    /**
     * like
     *
     * @param name
     * @param value
     * @return
     */

    public OmsUvLogSQL like(String name, Object value) {
    	super.like(name, value);
        return this;
    }
    
    public OmsUvLogSQL notLike(String name, Object value) {
    	super.notLike(name, value);
        return this;
    }

    /**
     * 大于 great than
     *
     * @param name
     * @param value
     * @return
     */
    public OmsUvLogSQL gt(String name, Object value) {
    	super.gt(name, value);
        return this;
    }

    /**
     * 大于等于 great or equal
     *
     * @param name
     * @param value
     * @return
     */
    public OmsUvLogSQL ge(String name, Object value) {
    	super.ge(name, value);
        return this;
    }

    /**
     * 小于 less than
     *
     * @param name
     * @param value
     * @return
     */
    public OmsUvLogSQL lt(String name, Object value) {
    	super.lt(name, value);
        return this;
    }

    /**
     * 小于等于 less or equal
     *
     * @param name
     * @param value
     * @return
     */
    public OmsUvLogSQL le(String name, Object value) {
    	super.le(name, value);
        return this;
    }
    
    public OmsUvLogSQL isnull(String name) {
    	super.isnull(name);
        return this;
    } 

    public OmsUvLogSQL notNull(String name) {
    	super.notNull(name);
        return this;
    } 
    
    public OmsUvLogSQL empty(String name) {
    	super.empty(name);
        return this;
    } 
    
    public OmsUvLogSQL notEmpty(String name) {
    	super.notEmpty(name);
        return this;
    } 
    
    public OmsUvLogSQL add(Column column) {
    	super.add(column);
    	return this;
    }
    
    /**************************/
	
	public void addCriterion(String name, Object value, ConditionMode logic, String property, String typeHandler, String valueType) {
		 if (value == null) {
			 throw new RuntimeException("Value for " + property + " cannot be null");
		 }
		 add(Column.create(name, value, logic, typeHandler, valueType));
	}
   
	public void addCriterion(String name, Object value1, Object value2, ConditionMode logic, String property, String typeHandler, String valueType) {
		 if (value1 == null || value2 == null) {
			 throw new RuntimeException("Between values for " + property + " cannot be null");
		 }
		 add(Column.create(name, value1, value2, logic, typeHandler, valueType));
	}
		 
	public OmsUvLogSQL andIdIsNull() {
		isnull("id");
		return this;
	}
	
	public OmsUvLogSQL andIdIsNotNull() {
		notNull("id");
		return this;
	}
	
	public OmsUvLogSQL andIdIsEmpty() {
		empty("id");
		return this;
	}

	public OmsUvLogSQL andIdIsNotEmpty() {
		notEmpty("id");
		return this;
	}
      public OmsUvLogSQL andIdEqualTo(java.lang.Long value) {
          addCriterion("id", value, ConditionMode.EQUAL, "id", "java.lang.Long", "Float");
          return this;
      }

      public OmsUvLogSQL andIdNotEqualTo(java.lang.Long value) {
          addCriterion("id", value, ConditionMode.NOT_EQUAL, "id", "java.lang.Long", "Float");
          return this;
      }

      public OmsUvLogSQL andIdGreaterThan(java.lang.Long value) {
          addCriterion("id", value, ConditionMode.GREATER_THEN, "id", "java.lang.Long", "Float");
          return this;
      }

      public OmsUvLogSQL andIdGreaterThanOrEqualTo(java.lang.Long value) {
          addCriterion("id", value, ConditionMode.GREATER_EQUAL, "id", "java.lang.Long", "Float");
          return this;
      }

      public OmsUvLogSQL andIdLessThan(java.lang.Long value) {
          addCriterion("id", value, ConditionMode.LESS_THEN, "id", "java.lang.Long", "Float");
          return this;
      }

      public OmsUvLogSQL andIdLessThanOrEqualTo(java.lang.Long value) {
          addCriterion("id", value, ConditionMode.LESS_EQUAL, "id", "java.lang.Long", "Float");
          return this;
      }

      public OmsUvLogSQL andIdBetween(java.lang.Long value1, java.lang.Long value2) {
    	  addCriterion("id", value1, value2, ConditionMode.BETWEEN, "id", "java.lang.Long", "Float");
    	  return this;
      }

      public OmsUvLogSQL andIdNotBetween(java.lang.Long value1, java.lang.Long value2) {
          addCriterion("id", value1, value2, ConditionMode.NOT_BETWEEN, "id", "java.lang.Long", "Float");
          return this;
      }
        
      public OmsUvLogSQL andIdIn(List<java.lang.Long> values) {
          addCriterion("id", values, ConditionMode.IN, "id", "java.lang.Long", "Float");
          return this;
      }

      public OmsUvLogSQL andIdNotIn(List<java.lang.Long> values) {
          addCriterion("id", values, ConditionMode.NOT_IN, "id", "java.lang.Long", "Float");
          return this;
      }
	public OmsUvLogSQL andSystemIdIsNull() {
		isnull("system_id");
		return this;
	}
	
	public OmsUvLogSQL andSystemIdIsNotNull() {
		notNull("system_id");
		return this;
	}
	
	public OmsUvLogSQL andSystemIdIsEmpty() {
		empty("system_id");
		return this;
	}

	public OmsUvLogSQL andSystemIdIsNotEmpty() {
		notEmpty("system_id");
		return this;
	}
      public OmsUvLogSQL andSystemIdEqualTo(java.lang.Integer value) {
          addCriterion("system_id", value, ConditionMode.EQUAL, "systemId", "java.lang.Integer", "Float");
          return this;
      }

      public OmsUvLogSQL andSystemIdNotEqualTo(java.lang.Integer value) {
          addCriterion("system_id", value, ConditionMode.NOT_EQUAL, "systemId", "java.lang.Integer", "Float");
          return this;
      }

      public OmsUvLogSQL andSystemIdGreaterThan(java.lang.Integer value) {
          addCriterion("system_id", value, ConditionMode.GREATER_THEN, "systemId", "java.lang.Integer", "Float");
          return this;
      }

      public OmsUvLogSQL andSystemIdGreaterThanOrEqualTo(java.lang.Integer value) {
          addCriterion("system_id", value, ConditionMode.GREATER_EQUAL, "systemId", "java.lang.Integer", "Float");
          return this;
      }

      public OmsUvLogSQL andSystemIdLessThan(java.lang.Integer value) {
          addCriterion("system_id", value, ConditionMode.LESS_THEN, "systemId", "java.lang.Integer", "Float");
          return this;
      }

      public OmsUvLogSQL andSystemIdLessThanOrEqualTo(java.lang.Integer value) {
          addCriterion("system_id", value, ConditionMode.LESS_EQUAL, "systemId", "java.lang.Integer", "Float");
          return this;
      }

      public OmsUvLogSQL andSystemIdBetween(java.lang.Integer value1, java.lang.Integer value2) {
    	  addCriterion("system_id", value1, value2, ConditionMode.BETWEEN, "systemId", "java.lang.Integer", "Float");
    	  return this;
      }

      public OmsUvLogSQL andSystemIdNotBetween(java.lang.Integer value1, java.lang.Integer value2) {
          addCriterion("system_id", value1, value2, ConditionMode.NOT_BETWEEN, "systemId", "java.lang.Integer", "Float");
          return this;
      }
        
      public OmsUvLogSQL andSystemIdIn(List<java.lang.Integer> values) {
          addCriterion("system_id", values, ConditionMode.IN, "systemId", "java.lang.Integer", "Float");
          return this;
      }

      public OmsUvLogSQL andSystemIdNotIn(List<java.lang.Integer> values) {
          addCriterion("system_id", values, ConditionMode.NOT_IN, "systemId", "java.lang.Integer", "Float");
          return this;
      }
	public OmsUvLogSQL andUpmsUserIdIsNull() {
		isnull("upms_user_id");
		return this;
	}
	
	public OmsUvLogSQL andUpmsUserIdIsNotNull() {
		notNull("upms_user_id");
		return this;
	}
	
	public OmsUvLogSQL andUpmsUserIdIsEmpty() {
		empty("upms_user_id");
		return this;
	}

	public OmsUvLogSQL andUpmsUserIdIsNotEmpty() {
		notEmpty("upms_user_id");
		return this;
	}
      public OmsUvLogSQL andUpmsUserIdEqualTo(java.lang.Long value) {
          addCriterion("upms_user_id", value, ConditionMode.EQUAL, "upmsUserId", "java.lang.Long", "Float");
          return this;
      }

      public OmsUvLogSQL andUpmsUserIdNotEqualTo(java.lang.Long value) {
          addCriterion("upms_user_id", value, ConditionMode.NOT_EQUAL, "upmsUserId", "java.lang.Long", "Float");
          return this;
      }

      public OmsUvLogSQL andUpmsUserIdGreaterThan(java.lang.Long value) {
          addCriterion("upms_user_id", value, ConditionMode.GREATER_THEN, "upmsUserId", "java.lang.Long", "Float");
          return this;
      }

      public OmsUvLogSQL andUpmsUserIdGreaterThanOrEqualTo(java.lang.Long value) {
          addCriterion("upms_user_id", value, ConditionMode.GREATER_EQUAL, "upmsUserId", "java.lang.Long", "Float");
          return this;
      }

      public OmsUvLogSQL andUpmsUserIdLessThan(java.lang.Long value) {
          addCriterion("upms_user_id", value, ConditionMode.LESS_THEN, "upmsUserId", "java.lang.Long", "Float");
          return this;
      }

      public OmsUvLogSQL andUpmsUserIdLessThanOrEqualTo(java.lang.Long value) {
          addCriterion("upms_user_id", value, ConditionMode.LESS_EQUAL, "upmsUserId", "java.lang.Long", "Float");
          return this;
      }

      public OmsUvLogSQL andUpmsUserIdBetween(java.lang.Long value1, java.lang.Long value2) {
    	  addCriterion("upms_user_id", value1, value2, ConditionMode.BETWEEN, "upmsUserId", "java.lang.Long", "Float");
    	  return this;
      }

      public OmsUvLogSQL andUpmsUserIdNotBetween(java.lang.Long value1, java.lang.Long value2) {
          addCriterion("upms_user_id", value1, value2, ConditionMode.NOT_BETWEEN, "upmsUserId", "java.lang.Long", "Float");
          return this;
      }
        
      public OmsUvLogSQL andUpmsUserIdIn(List<java.lang.Long> values) {
          addCriterion("upms_user_id", values, ConditionMode.IN, "upmsUserId", "java.lang.Long", "Float");
          return this;
      }

      public OmsUvLogSQL andUpmsUserIdNotIn(List<java.lang.Long> values) {
          addCriterion("upms_user_id", values, ConditionMode.NOT_IN, "upmsUserId", "java.lang.Long", "Float");
          return this;
      }
	public OmsUvLogSQL andSessionIdIsNull() {
		isnull("session_id");
		return this;
	}
	
	public OmsUvLogSQL andSessionIdIsNotNull() {
		notNull("session_id");
		return this;
	}
	
	public OmsUvLogSQL andSessionIdIsEmpty() {
		empty("session_id");
		return this;
	}

	public OmsUvLogSQL andSessionIdIsNotEmpty() {
		notEmpty("session_id");
		return this;
	}
       public OmsUvLogSQL andSessionIdLike(java.lang.String value) {
    	   addCriterion("session_id", value, ConditionMode.FUZZY, "sessionId", "java.lang.String", "Float");
    	   return this;
      }

      public OmsUvLogSQL andSessionIdNotLike(java.lang.String value) {
          addCriterion("session_id", value, ConditionMode.NOT_FUZZY, "sessionId", "java.lang.String", "Float");
          return this;
      }
      public OmsUvLogSQL andSessionIdEqualTo(java.lang.String value) {
          addCriterion("session_id", value, ConditionMode.EQUAL, "sessionId", "java.lang.String", "String");
          return this;
      }

      public OmsUvLogSQL andSessionIdNotEqualTo(java.lang.String value) {
          addCriterion("session_id", value, ConditionMode.NOT_EQUAL, "sessionId", "java.lang.String", "String");
          return this;
      }

      public OmsUvLogSQL andSessionIdGreaterThan(java.lang.String value) {
          addCriterion("session_id", value, ConditionMode.GREATER_THEN, "sessionId", "java.lang.String", "String");
          return this;
      }

      public OmsUvLogSQL andSessionIdGreaterThanOrEqualTo(java.lang.String value) {
          addCriterion("session_id", value, ConditionMode.GREATER_EQUAL, "sessionId", "java.lang.String", "String");
          return this;
      }

      public OmsUvLogSQL andSessionIdLessThan(java.lang.String value) {
          addCriterion("session_id", value, ConditionMode.LESS_THEN, "sessionId", "java.lang.String", "String");
          return this;
      }

      public OmsUvLogSQL andSessionIdLessThanOrEqualTo(java.lang.String value) {
          addCriterion("session_id", value, ConditionMode.LESS_EQUAL, "sessionId", "java.lang.String", "String");
          return this;
      }

      public OmsUvLogSQL andSessionIdBetween(java.lang.String value1, java.lang.String value2) {
    	  addCriterion("session_id", value1, value2, ConditionMode.BETWEEN, "sessionId", "java.lang.String", "String");
    	  return this;
      }

      public OmsUvLogSQL andSessionIdNotBetween(java.lang.String value1, java.lang.String value2) {
          addCriterion("session_id", value1, value2, ConditionMode.NOT_BETWEEN, "sessionId", "java.lang.String", "String");
          return this;
      }
        
      public OmsUvLogSQL andSessionIdIn(List<java.lang.String> values) {
          addCriterion("session_id", values, ConditionMode.IN, "sessionId", "java.lang.String", "String");
          return this;
      }

      public OmsUvLogSQL andSessionIdNotIn(List<java.lang.String> values) {
          addCriterion("session_id", values, ConditionMode.NOT_IN, "sessionId", "java.lang.String", "String");
          return this;
      }
	public OmsUvLogSQL andUserIdIsNull() {
		isnull("user_id");
		return this;
	}
	
	public OmsUvLogSQL andUserIdIsNotNull() {
		notNull("user_id");
		return this;
	}
	
	public OmsUvLogSQL andUserIdIsEmpty() {
		empty("user_id");
		return this;
	}

	public OmsUvLogSQL andUserIdIsNotEmpty() {
		notEmpty("user_id");
		return this;
	}
       public OmsUvLogSQL andUserIdLike(java.lang.String value) {
    	   addCriterion("user_id", value, ConditionMode.FUZZY, "userId", "java.lang.String", "String");
    	   return this;
      }

      public OmsUvLogSQL andUserIdNotLike(java.lang.String value) {
          addCriterion("user_id", value, ConditionMode.NOT_FUZZY, "userId", "java.lang.String", "String");
          return this;
      }
      public OmsUvLogSQL andUserIdEqualTo(java.lang.String value) {
          addCriterion("user_id", value, ConditionMode.EQUAL, "userId", "java.lang.String", "String");
          return this;
      }

      public OmsUvLogSQL andUserIdNotEqualTo(java.lang.String value) {
          addCriterion("user_id", value, ConditionMode.NOT_EQUAL, "userId", "java.lang.String", "String");
          return this;
      }

      public OmsUvLogSQL andUserIdGreaterThan(java.lang.String value) {
          addCriterion("user_id", value, ConditionMode.GREATER_THEN, "userId", "java.lang.String", "String");
          return this;
      }

      public OmsUvLogSQL andUserIdGreaterThanOrEqualTo(java.lang.String value) {
          addCriterion("user_id", value, ConditionMode.GREATER_EQUAL, "userId", "java.lang.String", "String");
          return this;
      }

      public OmsUvLogSQL andUserIdLessThan(java.lang.String value) {
          addCriterion("user_id", value, ConditionMode.LESS_THEN, "userId", "java.lang.String", "String");
          return this;
      }

      public OmsUvLogSQL andUserIdLessThanOrEqualTo(java.lang.String value) {
          addCriterion("user_id", value, ConditionMode.LESS_EQUAL, "userId", "java.lang.String", "String");
          return this;
      }

      public OmsUvLogSQL andUserIdBetween(java.lang.String value1, java.lang.String value2) {
    	  addCriterion("user_id", value1, value2, ConditionMode.BETWEEN, "userId", "java.lang.String", "String");
    	  return this;
      }

      public OmsUvLogSQL andUserIdNotBetween(java.lang.String value1, java.lang.String value2) {
          addCriterion("user_id", value1, value2, ConditionMode.NOT_BETWEEN, "userId", "java.lang.String", "String");
          return this;
      }
        
      public OmsUvLogSQL andUserIdIn(List<java.lang.String> values) {
          addCriterion("user_id", values, ConditionMode.IN, "userId", "java.lang.String", "String");
          return this;
      }

      public OmsUvLogSQL andUserIdNotIn(List<java.lang.String> values) {
          addCriterion("user_id", values, ConditionMode.NOT_IN, "userId", "java.lang.String", "String");
          return this;
      }
	public OmsUvLogSQL andCreateTimeIsNull() {
		isnull("create_time");
		return this;
	}
	
	public OmsUvLogSQL andCreateTimeIsNotNull() {
		notNull("create_time");
		return this;
	}
	
	public OmsUvLogSQL andCreateTimeIsEmpty() {
		empty("create_time");
		return this;
	}

	public OmsUvLogSQL andCreateTimeIsNotEmpty() {
		notEmpty("create_time");
		return this;
	}
      public OmsUvLogSQL andCreateTimeEqualTo(java.util.Date value) {
          addCriterion("create_time", value, ConditionMode.EQUAL, "createTime", "java.util.Date", "String");
          return this;
      }

      public OmsUvLogSQL andCreateTimeNotEqualTo(java.util.Date value) {
          addCriterion("create_time", value, ConditionMode.NOT_EQUAL, "createTime", "java.util.Date", "String");
          return this;
      }

      public OmsUvLogSQL andCreateTimeGreaterThan(java.util.Date value) {
          addCriterion("create_time", value, ConditionMode.GREATER_THEN, "createTime", "java.util.Date", "String");
          return this;
      }

      public OmsUvLogSQL andCreateTimeGreaterThanOrEqualTo(java.util.Date value) {
          addCriterion("create_time", value, ConditionMode.GREATER_EQUAL, "createTime", "java.util.Date", "String");
          return this;
      }

      public OmsUvLogSQL andCreateTimeLessThan(java.util.Date value) {
          addCriterion("create_time", value, ConditionMode.LESS_THEN, "createTime", "java.util.Date", "String");
          return this;
      }

      public OmsUvLogSQL andCreateTimeLessThanOrEqualTo(java.util.Date value) {
          addCriterion("create_time", value, ConditionMode.LESS_EQUAL, "createTime", "java.util.Date", "String");
          return this;
      }

      public OmsUvLogSQL andCreateTimeBetween(java.util.Date value1, java.util.Date value2) {
    	  addCriterion("create_time", value1, value2, ConditionMode.BETWEEN, "createTime", "java.util.Date", "String");
    	  return this;
      }

      public OmsUvLogSQL andCreateTimeNotBetween(java.util.Date value1, java.util.Date value2) {
          addCriterion("create_time", value1, value2, ConditionMode.NOT_BETWEEN, "createTime", "java.util.Date", "String");
          return this;
      }
        
      public OmsUvLogSQL andCreateTimeIn(List<java.util.Date> values) {
          addCriterion("create_time", values, ConditionMode.IN, "createTime", "java.util.Date", "String");
          return this;
      }

      public OmsUvLogSQL andCreateTimeNotIn(List<java.util.Date> values) {
          addCriterion("create_time", values, ConditionMode.NOT_IN, "createTime", "java.util.Date", "String");
          return this;
      }
	public OmsUvLogSQL andAreaCodeIsNull() {
		isnull("area_code");
		return this;
	}
	
	public OmsUvLogSQL andAreaCodeIsNotNull() {
		notNull("area_code");
		return this;
	}
	
	public OmsUvLogSQL andAreaCodeIsEmpty() {
		empty("area_code");
		return this;
	}

	public OmsUvLogSQL andAreaCodeIsNotEmpty() {
		notEmpty("area_code");
		return this;
	}
       public OmsUvLogSQL andAreaCodeLike(java.lang.String value) {
    	   addCriterion("area_code", value, ConditionMode.FUZZY, "areaCode", "java.lang.String", "String");
    	   return this;
      }

      public OmsUvLogSQL andAreaCodeNotLike(java.lang.String value) {
          addCriterion("area_code", value, ConditionMode.NOT_FUZZY, "areaCode", "java.lang.String", "String");
          return this;
      }
      public OmsUvLogSQL andAreaCodeEqualTo(java.lang.String value) {
          addCriterion("area_code", value, ConditionMode.EQUAL, "areaCode", "java.lang.String", "String");
          return this;
      }

      public OmsUvLogSQL andAreaCodeNotEqualTo(java.lang.String value) {
          addCriterion("area_code", value, ConditionMode.NOT_EQUAL, "areaCode", "java.lang.String", "String");
          return this;
      }

      public OmsUvLogSQL andAreaCodeGreaterThan(java.lang.String value) {
          addCriterion("area_code", value, ConditionMode.GREATER_THEN, "areaCode", "java.lang.String", "String");
          return this;
      }

      public OmsUvLogSQL andAreaCodeGreaterThanOrEqualTo(java.lang.String value) {
          addCriterion("area_code", value, ConditionMode.GREATER_EQUAL, "areaCode", "java.lang.String", "String");
          return this;
      }

      public OmsUvLogSQL andAreaCodeLessThan(java.lang.String value) {
          addCriterion("area_code", value, ConditionMode.LESS_THEN, "areaCode", "java.lang.String", "String");
          return this;
      }

      public OmsUvLogSQL andAreaCodeLessThanOrEqualTo(java.lang.String value) {
          addCriterion("area_code", value, ConditionMode.LESS_EQUAL, "areaCode", "java.lang.String", "String");
          return this;
      }

      public OmsUvLogSQL andAreaCodeBetween(java.lang.String value1, java.lang.String value2) {
    	  addCriterion("area_code", value1, value2, ConditionMode.BETWEEN, "areaCode", "java.lang.String", "String");
    	  return this;
      }

      public OmsUvLogSQL andAreaCodeNotBetween(java.lang.String value1, java.lang.String value2) {
          addCriterion("area_code", value1, value2, ConditionMode.NOT_BETWEEN, "areaCode", "java.lang.String", "String");
          return this;
      }
        
      public OmsUvLogSQL andAreaCodeIn(List<java.lang.String> values) {
          addCriterion("area_code", values, ConditionMode.IN, "areaCode", "java.lang.String", "String");
          return this;
      }

      public OmsUvLogSQL andAreaCodeNotIn(List<java.lang.String> values) {
          addCriterion("area_code", values, ConditionMode.NOT_IN, "areaCode", "java.lang.String", "String");
          return this;
      }
}