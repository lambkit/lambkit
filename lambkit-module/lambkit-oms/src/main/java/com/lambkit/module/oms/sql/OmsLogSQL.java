/**
 * Copyright (c) 2015-2017, Henry Yang 杨勇 (gismail@foxmail.com).
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.lambkit.module.oms.sql;

import com.lambkit.db.sql.ConditionMode;
import com.lambkit.db.sql.Column;
import com.lambkit.db.sql.Columns;
import com.lambkit.db.sql.Example;

import java.io.Serializable;
import java.util.List;

/**
 * @author yangyong 
 * @website: www.lambkit.com
 * @email: gismail@foxmail.com
 * @date ${date}
 * @version 1.0
 * @since 1.0
 */

public class OmsLogSQL extends Columns implements Serializable {
	private static final long serialVersionUID = 1L;
	
	public static OmsLogSQL of() {
		return new OmsLogSQL();
	}
	
	public static OmsLogSQL by(Column column) {
		OmsLogSQL that = new OmsLogSQL();
		that.add(column);
        return that;
    }

    public static OmsLogSQL by(String name, Object value) {
        return (OmsLogSQL) create().eq(name, value);
    }
    
    public Example example() {
    	return Example.create("oms_log", this);
    }
    
    /**
     * equals
     *
     * @param name
     * @param value
     * @return
     */
    public OmsLogSQL eq(String name, Object value) {
    	super.eq(name, value);
        return this;
    }

    /**
     * not equals !=
     *
     * @param name
     * @param value
     * @return
     */
    public OmsLogSQL ne(String name, Object value) {
    	super.ne(name, value);
        return this;
    }


    /**
     * like
     *
     * @param name
     * @param value
     * @return
     */

    public OmsLogSQL like(String name, Object value) {
    	super.like(name, value);
        return this;
    }
    
    public OmsLogSQL notLike(String name, Object value) {
    	super.notLike(name, value);
        return this;
    }

    /**
     * 大于 great than
     *
     * @param name
     * @param value
     * @return
     */
    public OmsLogSQL gt(String name, Object value) {
    	super.gt(name, value);
        return this;
    }

    /**
     * 大于等于 great or equal
     *
     * @param name
     * @param value
     * @return
     */
    public OmsLogSQL ge(String name, Object value) {
    	super.ge(name, value);
        return this;
    }

    /**
     * 小于 less than
     *
     * @param name
     * @param value
     * @return
     */
    public OmsLogSQL lt(String name, Object value) {
    	super.lt(name, value);
        return this;
    }

    /**
     * 小于等于 less or equal
     *
     * @param name
     * @param value
     * @return
     */
    public OmsLogSQL le(String name, Object value) {
    	super.le(name, value);
        return this;
    }
    
    public OmsLogSQL isnull(String name) {
    	super.isnull(name);
        return this;
    } 

    public OmsLogSQL notNull(String name) {
    	super.notNull(name);
        return this;
    } 
    
    public OmsLogSQL empty(String name) {
    	super.empty(name);
        return this;
    } 
    
    public OmsLogSQL notEmpty(String name) {
    	super.notEmpty(name);
        return this;
    } 
    
    public OmsLogSQL add(Column column) {
    	super.add(column);
    	return this;
    }
    
    /**************************/
	
	public void addCriterion(String name, Object value, ConditionMode logic, String property, String typeHandler, String valueType) {
		 if (value == null) {
			 throw new RuntimeException("Value for " + property + " cannot be null");
		 }
		 add(Column.create(name, value, logic, typeHandler, valueType));
	}
   
	public void addCriterion(String name, Object value1, Object value2, ConditionMode logic, String property, String typeHandler, String valueType) {
		 if (value1 == null || value2 == null) {
			 throw new RuntimeException("Between values for " + property + " cannot be null");
		 }
		 add(Column.create(name, value1, value2, logic, typeHandler, valueType));
	}
		 
	public OmsLogSQL andLogIdIsNull() {
		isnull("log_id");
		return this;
	}
	
	public OmsLogSQL andLogIdIsNotNull() {
		notNull("log_id");
		return this;
	}
	
	public OmsLogSQL andLogIdIsEmpty() {
		empty("log_id");
		return this;
	}

	public OmsLogSQL andLogIdIsNotEmpty() {
		notEmpty("log_id");
		return this;
	}
      public OmsLogSQL andLogIdEqualTo(java.lang.Integer value) {
          addCriterion("log_id", value, ConditionMode.EQUAL, "logId", "java.lang.Integer", "Float");
          return this;
      }

      public OmsLogSQL andLogIdNotEqualTo(java.lang.Integer value) {
          addCriterion("log_id", value, ConditionMode.NOT_EQUAL, "logId", "java.lang.Integer", "Float");
          return this;
      }

      public OmsLogSQL andLogIdGreaterThan(java.lang.Integer value) {
          addCriterion("log_id", value, ConditionMode.GREATER_THEN, "logId", "java.lang.Integer", "Float");
          return this;
      }

      public OmsLogSQL andLogIdGreaterThanOrEqualTo(java.lang.Integer value) {
          addCriterion("log_id", value, ConditionMode.GREATER_EQUAL, "logId", "java.lang.Integer", "Float");
          return this;
      }

      public OmsLogSQL andLogIdLessThan(java.lang.Integer value) {
          addCriterion("log_id", value, ConditionMode.LESS_THEN, "logId", "java.lang.Integer", "Float");
          return this;
      }

      public OmsLogSQL andLogIdLessThanOrEqualTo(java.lang.Integer value) {
          addCriterion("log_id", value, ConditionMode.LESS_EQUAL, "logId", "java.lang.Integer", "Float");
          return this;
      }

      public OmsLogSQL andLogIdBetween(java.lang.Integer value1, java.lang.Integer value2) {
    	  addCriterion("log_id", value1, value2, ConditionMode.BETWEEN, "logId", "java.lang.Integer", "Float");
    	  return this;
      }

      public OmsLogSQL andLogIdNotBetween(java.lang.Integer value1, java.lang.Integer value2) {
          addCriterion("log_id", value1, value2, ConditionMode.NOT_BETWEEN, "logId", "java.lang.Integer", "Float");
          return this;
      }
        
      public OmsLogSQL andLogIdIn(List<java.lang.Integer> values) {
          addCriterion("log_id", values, ConditionMode.IN, "logId", "java.lang.Integer", "Float");
          return this;
      }

      public OmsLogSQL andLogIdNotIn(List<java.lang.Integer> values) {
          addCriterion("log_id", values, ConditionMode.NOT_IN, "logId", "java.lang.Integer", "Float");
          return this;
      }
	public OmsLogSQL andDescriptionIsNull() {
		isnull("description");
		return this;
	}
	
	public OmsLogSQL andDescriptionIsNotNull() {
		notNull("description");
		return this;
	}
	
	public OmsLogSQL andDescriptionIsEmpty() {
		empty("description");
		return this;
	}

	public OmsLogSQL andDescriptionIsNotEmpty() {
		notEmpty("description");
		return this;
	}
       public OmsLogSQL andDescriptionLike(java.lang.String value) {
    	   addCriterion("description", value, ConditionMode.FUZZY, "description", "java.lang.String", "Float");
    	   return this;
      }

      public OmsLogSQL andDescriptionNotLike(java.lang.String value) {
          addCriterion("description", value, ConditionMode.NOT_FUZZY, "description", "java.lang.String", "Float");
          return this;
      }
      public OmsLogSQL andDescriptionEqualTo(java.lang.String value) {
          addCriterion("description", value, ConditionMode.EQUAL, "description", "java.lang.String", "String");
          return this;
      }

      public OmsLogSQL andDescriptionNotEqualTo(java.lang.String value) {
          addCriterion("description", value, ConditionMode.NOT_EQUAL, "description", "java.lang.String", "String");
          return this;
      }

      public OmsLogSQL andDescriptionGreaterThan(java.lang.String value) {
          addCriterion("description", value, ConditionMode.GREATER_THEN, "description", "java.lang.String", "String");
          return this;
      }

      public OmsLogSQL andDescriptionGreaterThanOrEqualTo(java.lang.String value) {
          addCriterion("description", value, ConditionMode.GREATER_EQUAL, "description", "java.lang.String", "String");
          return this;
      }

      public OmsLogSQL andDescriptionLessThan(java.lang.String value) {
          addCriterion("description", value, ConditionMode.LESS_THEN, "description", "java.lang.String", "String");
          return this;
      }

      public OmsLogSQL andDescriptionLessThanOrEqualTo(java.lang.String value) {
          addCriterion("description", value, ConditionMode.LESS_EQUAL, "description", "java.lang.String", "String");
          return this;
      }

      public OmsLogSQL andDescriptionBetween(java.lang.String value1, java.lang.String value2) {
    	  addCriterion("description", value1, value2, ConditionMode.BETWEEN, "description", "java.lang.String", "String");
    	  return this;
      }

      public OmsLogSQL andDescriptionNotBetween(java.lang.String value1, java.lang.String value2) {
          addCriterion("description", value1, value2, ConditionMode.NOT_BETWEEN, "description", "java.lang.String", "String");
          return this;
      }
        
      public OmsLogSQL andDescriptionIn(List<java.lang.String> values) {
          addCriterion("description", values, ConditionMode.IN, "description", "java.lang.String", "String");
          return this;
      }

      public OmsLogSQL andDescriptionNotIn(List<java.lang.String> values) {
          addCriterion("description", values, ConditionMode.NOT_IN, "description", "java.lang.String", "String");
          return this;
      }
	public OmsLogSQL andActionIsNull() {
		isnull("action");
		return this;
	}
	
	public OmsLogSQL andActionIsNotNull() {
		notNull("action");
		return this;
	}
	
	public OmsLogSQL andActionIsEmpty() {
		empty("action");
		return this;
	}

	public OmsLogSQL andActionIsNotEmpty() {
		notEmpty("action");
		return this;
	}
       public OmsLogSQL andActionLike(java.lang.String value) {
    	   addCriterion("action", value, ConditionMode.FUZZY, "action", "java.lang.String", "String");
    	   return this;
      }

      public OmsLogSQL andActionNotLike(java.lang.String value) {
          addCriterion("action", value, ConditionMode.NOT_FUZZY, "action", "java.lang.String", "String");
          return this;
      }
      public OmsLogSQL andActionEqualTo(java.lang.String value) {
          addCriterion("action", value, ConditionMode.EQUAL, "action", "java.lang.String", "String");
          return this;
      }

      public OmsLogSQL andActionNotEqualTo(java.lang.String value) {
          addCriterion("action", value, ConditionMode.NOT_EQUAL, "action", "java.lang.String", "String");
          return this;
      }

      public OmsLogSQL andActionGreaterThan(java.lang.String value) {
          addCriterion("action", value, ConditionMode.GREATER_THEN, "action", "java.lang.String", "String");
          return this;
      }

      public OmsLogSQL andActionGreaterThanOrEqualTo(java.lang.String value) {
          addCriterion("action", value, ConditionMode.GREATER_EQUAL, "action", "java.lang.String", "String");
          return this;
      }

      public OmsLogSQL andActionLessThan(java.lang.String value) {
          addCriterion("action", value, ConditionMode.LESS_THEN, "action", "java.lang.String", "String");
          return this;
      }

      public OmsLogSQL andActionLessThanOrEqualTo(java.lang.String value) {
          addCriterion("action", value, ConditionMode.LESS_EQUAL, "action", "java.lang.String", "String");
          return this;
      }

      public OmsLogSQL andActionBetween(java.lang.String value1, java.lang.String value2) {
    	  addCriterion("action", value1, value2, ConditionMode.BETWEEN, "action", "java.lang.String", "String");
    	  return this;
      }

      public OmsLogSQL andActionNotBetween(java.lang.String value1, java.lang.String value2) {
          addCriterion("action", value1, value2, ConditionMode.NOT_BETWEEN, "action", "java.lang.String", "String");
          return this;
      }
        
      public OmsLogSQL andActionIn(List<java.lang.String> values) {
          addCriterion("action", values, ConditionMode.IN, "action", "java.lang.String", "String");
          return this;
      }

      public OmsLogSQL andActionNotIn(List<java.lang.String> values) {
          addCriterion("action", values, ConditionMode.NOT_IN, "action", "java.lang.String", "String");
          return this;
      }
	public OmsLogSQL andFtableIsNull() {
		isnull("ftable");
		return this;
	}
	
	public OmsLogSQL andFtableIsNotNull() {
		notNull("ftable");
		return this;
	}
	
	public OmsLogSQL andFtableIsEmpty() {
		empty("ftable");
		return this;
	}

	public OmsLogSQL andFtableIsNotEmpty() {
		notEmpty("ftable");
		return this;
	}
       public OmsLogSQL andFtableLike(java.lang.String value) {
    	   addCriterion("ftable", value, ConditionMode.FUZZY, "ftable", "java.lang.String", "String");
    	   return this;
      }

      public OmsLogSQL andFtableNotLike(java.lang.String value) {
          addCriterion("ftable", value, ConditionMode.NOT_FUZZY, "ftable", "java.lang.String", "String");
          return this;
      }
      public OmsLogSQL andFtableEqualTo(java.lang.String value) {
          addCriterion("ftable", value, ConditionMode.EQUAL, "ftable", "java.lang.String", "String");
          return this;
      }

      public OmsLogSQL andFtableNotEqualTo(java.lang.String value) {
          addCriterion("ftable", value, ConditionMode.NOT_EQUAL, "ftable", "java.lang.String", "String");
          return this;
      }

      public OmsLogSQL andFtableGreaterThan(java.lang.String value) {
          addCriterion("ftable", value, ConditionMode.GREATER_THEN, "ftable", "java.lang.String", "String");
          return this;
      }

      public OmsLogSQL andFtableGreaterThanOrEqualTo(java.lang.String value) {
          addCriterion("ftable", value, ConditionMode.GREATER_EQUAL, "ftable", "java.lang.String", "String");
          return this;
      }

      public OmsLogSQL andFtableLessThan(java.lang.String value) {
          addCriterion("ftable", value, ConditionMode.LESS_THEN, "ftable", "java.lang.String", "String");
          return this;
      }

      public OmsLogSQL andFtableLessThanOrEqualTo(java.lang.String value) {
          addCriterion("ftable", value, ConditionMode.LESS_EQUAL, "ftable", "java.lang.String", "String");
          return this;
      }

      public OmsLogSQL andFtableBetween(java.lang.String value1, java.lang.String value2) {
    	  addCriterion("ftable", value1, value2, ConditionMode.BETWEEN, "ftable", "java.lang.String", "String");
    	  return this;
      }

      public OmsLogSQL andFtableNotBetween(java.lang.String value1, java.lang.String value2) {
          addCriterion("ftable", value1, value2, ConditionMode.NOT_BETWEEN, "ftable", "java.lang.String", "String");
          return this;
      }
        
      public OmsLogSQL andFtableIn(List<java.lang.String> values) {
          addCriterion("ftable", values, ConditionMode.IN, "ftable", "java.lang.String", "String");
          return this;
      }

      public OmsLogSQL andFtableNotIn(List<java.lang.String> values) {
          addCriterion("ftable", values, ConditionMode.NOT_IN, "ftable", "java.lang.String", "String");
          return this;
      }
	public OmsLogSQL andFcolumnIsNull() {
		isnull("fcolumn");
		return this;
	}
	
	public OmsLogSQL andFcolumnIsNotNull() {
		notNull("fcolumn");
		return this;
	}
	
	public OmsLogSQL andFcolumnIsEmpty() {
		empty("fcolumn");
		return this;
	}

	public OmsLogSQL andFcolumnIsNotEmpty() {
		notEmpty("fcolumn");
		return this;
	}
       public OmsLogSQL andFcolumnLike(java.lang.String value) {
    	   addCriterion("fcolumn", value, ConditionMode.FUZZY, "fcolumn", "java.lang.String", "String");
    	   return this;
      }

      public OmsLogSQL andFcolumnNotLike(java.lang.String value) {
          addCriterion("fcolumn", value, ConditionMode.NOT_FUZZY, "fcolumn", "java.lang.String", "String");
          return this;
      }
      public OmsLogSQL andFcolumnEqualTo(java.lang.String value) {
          addCriterion("fcolumn", value, ConditionMode.EQUAL, "fcolumn", "java.lang.String", "String");
          return this;
      }

      public OmsLogSQL andFcolumnNotEqualTo(java.lang.String value) {
          addCriterion("fcolumn", value, ConditionMode.NOT_EQUAL, "fcolumn", "java.lang.String", "String");
          return this;
      }

      public OmsLogSQL andFcolumnGreaterThan(java.lang.String value) {
          addCriterion("fcolumn", value, ConditionMode.GREATER_THEN, "fcolumn", "java.lang.String", "String");
          return this;
      }

      public OmsLogSQL andFcolumnGreaterThanOrEqualTo(java.lang.String value) {
          addCriterion("fcolumn", value, ConditionMode.GREATER_EQUAL, "fcolumn", "java.lang.String", "String");
          return this;
      }

      public OmsLogSQL andFcolumnLessThan(java.lang.String value) {
          addCriterion("fcolumn", value, ConditionMode.LESS_THEN, "fcolumn", "java.lang.String", "String");
          return this;
      }

      public OmsLogSQL andFcolumnLessThanOrEqualTo(java.lang.String value) {
          addCriterion("fcolumn", value, ConditionMode.LESS_EQUAL, "fcolumn", "java.lang.String", "String");
          return this;
      }

      public OmsLogSQL andFcolumnBetween(java.lang.String value1, java.lang.String value2) {
    	  addCriterion("fcolumn", value1, value2, ConditionMode.BETWEEN, "fcolumn", "java.lang.String", "String");
    	  return this;
      }

      public OmsLogSQL andFcolumnNotBetween(java.lang.String value1, java.lang.String value2) {
          addCriterion("fcolumn", value1, value2, ConditionMode.NOT_BETWEEN, "fcolumn", "java.lang.String", "String");
          return this;
      }
        
      public OmsLogSQL andFcolumnIn(List<java.lang.String> values) {
          addCriterion("fcolumn", values, ConditionMode.IN, "fcolumn", "java.lang.String", "String");
          return this;
      }

      public OmsLogSQL andFcolumnNotIn(List<java.lang.String> values) {
          addCriterion("fcolumn", values, ConditionMode.NOT_IN, "fcolumn", "java.lang.String", "String");
          return this;
      }
	public OmsLogSQL andFtimeIsNull() {
		isnull("ftime");
		return this;
	}
	
	public OmsLogSQL andFtimeIsNotNull() {
		notNull("ftime");
		return this;
	}
	
	public OmsLogSQL andFtimeIsEmpty() {
		empty("ftime");
		return this;
	}

	public OmsLogSQL andFtimeIsNotEmpty() {
		notEmpty("ftime");
		return this;
	}
      public OmsLogSQL andFtimeEqualTo(java.util.Date value) {
          addCriterion("ftime", value, ConditionMode.EQUAL, "ftime", "java.util.Date", "String");
          return this;
      }

      public OmsLogSQL andFtimeNotEqualTo(java.util.Date value) {
          addCriterion("ftime", value, ConditionMode.NOT_EQUAL, "ftime", "java.util.Date", "String");
          return this;
      }

      public OmsLogSQL andFtimeGreaterThan(java.util.Date value) {
          addCriterion("ftime", value, ConditionMode.GREATER_THEN, "ftime", "java.util.Date", "String");
          return this;
      }

      public OmsLogSQL andFtimeGreaterThanOrEqualTo(java.util.Date value) {
          addCriterion("ftime", value, ConditionMode.GREATER_EQUAL, "ftime", "java.util.Date", "String");
          return this;
      }

      public OmsLogSQL andFtimeLessThan(java.util.Date value) {
          addCriterion("ftime", value, ConditionMode.LESS_THEN, "ftime", "java.util.Date", "String");
          return this;
      }

      public OmsLogSQL andFtimeLessThanOrEqualTo(java.util.Date value) {
          addCriterion("ftime", value, ConditionMode.LESS_EQUAL, "ftime", "java.util.Date", "String");
          return this;
      }

      public OmsLogSQL andFtimeBetween(java.util.Date value1, java.util.Date value2) {
    	  addCriterion("ftime", value1, value2, ConditionMode.BETWEEN, "ftime", "java.util.Date", "String");
    	  return this;
      }

      public OmsLogSQL andFtimeNotBetween(java.util.Date value1, java.util.Date value2) {
          addCriterion("ftime", value1, value2, ConditionMode.NOT_BETWEEN, "ftime", "java.util.Date", "String");
          return this;
      }
        
      public OmsLogSQL andFtimeIn(List<java.util.Date> values) {
          addCriterion("ftime", values, ConditionMode.IN, "ftime", "java.util.Date", "String");
          return this;
      }

      public OmsLogSQL andFtimeNotIn(List<java.util.Date> values) {
          addCriterion("ftime", values, ConditionMode.NOT_IN, "ftime", "java.util.Date", "String");
          return this;
      }
	public OmsLogSQL andUsernameIsNull() {
		isnull("username");
		return this;
	}
	
	public OmsLogSQL andUsernameIsNotNull() {
		notNull("username");
		return this;
	}
	
	public OmsLogSQL andUsernameIsEmpty() {
		empty("username");
		return this;
	}

	public OmsLogSQL andUsernameIsNotEmpty() {
		notEmpty("username");
		return this;
	}
       public OmsLogSQL andUsernameLike(java.lang.String value) {
    	   addCriterion("username", value, ConditionMode.FUZZY, "username", "java.lang.String", "String");
    	   return this;
      }

      public OmsLogSQL andUsernameNotLike(java.lang.String value) {
          addCriterion("username", value, ConditionMode.NOT_FUZZY, "username", "java.lang.String", "String");
          return this;
      }
      public OmsLogSQL andUsernameEqualTo(java.lang.String value) {
          addCriterion("username", value, ConditionMode.EQUAL, "username", "java.lang.String", "String");
          return this;
      }

      public OmsLogSQL andUsernameNotEqualTo(java.lang.String value) {
          addCriterion("username", value, ConditionMode.NOT_EQUAL, "username", "java.lang.String", "String");
          return this;
      }

      public OmsLogSQL andUsernameGreaterThan(java.lang.String value) {
          addCriterion("username", value, ConditionMode.GREATER_THEN, "username", "java.lang.String", "String");
          return this;
      }

      public OmsLogSQL andUsernameGreaterThanOrEqualTo(java.lang.String value) {
          addCriterion("username", value, ConditionMode.GREATER_EQUAL, "username", "java.lang.String", "String");
          return this;
      }

      public OmsLogSQL andUsernameLessThan(java.lang.String value) {
          addCriterion("username", value, ConditionMode.LESS_THEN, "username", "java.lang.String", "String");
          return this;
      }

      public OmsLogSQL andUsernameLessThanOrEqualTo(java.lang.String value) {
          addCriterion("username", value, ConditionMode.LESS_EQUAL, "username", "java.lang.String", "String");
          return this;
      }

      public OmsLogSQL andUsernameBetween(java.lang.String value1, java.lang.String value2) {
    	  addCriterion("username", value1, value2, ConditionMode.BETWEEN, "username", "java.lang.String", "String");
    	  return this;
      }

      public OmsLogSQL andUsernameNotBetween(java.lang.String value1, java.lang.String value2) {
          addCriterion("username", value1, value2, ConditionMode.NOT_BETWEEN, "username", "java.lang.String", "String");
          return this;
      }
        
      public OmsLogSQL andUsernameIn(List<java.lang.String> values) {
          addCriterion("username", values, ConditionMode.IN, "username", "java.lang.String", "String");
          return this;
      }

      public OmsLogSQL andUsernameNotIn(List<java.lang.String> values) {
          addCriterion("username", values, ConditionMode.NOT_IN, "username", "java.lang.String", "String");
          return this;
      }
}