/**
 * Copyright (c) 2015-2017, Henry Yang 杨勇 (gismail@foxmail.com).
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.lambkit.module.oms.row;

import com.lambkit.db.RowModel;

/**
 * @author yangyong 
 * @website: www.lambkit.com
 * @email: gismail@foxmail.com
 * @date ${date}
 * @version 1.0
 * @since 1.0
 */

public class OmsDevopsWarn extends RowModel<OmsDevopsWarn> {
	public OmsDevopsWarn() {
		setTableName("oms_devops_warn");
		setPrimaryKey("devops_warn_id");
	}
	public java.lang.Long getDevopsWarnId() {
		return this.get("devops_warn_id");
	}
	public void setDevopsWarnId(java.lang.Long devopsWarnId) {
		this.set("devops_warn_id", devopsWarnId);
	}
	public java.lang.String getName() {
		return this.get("name");
	}
	public void setName(java.lang.String name) {
		this.set("name", name);
	}
	public java.lang.String getTitle() {
		return this.get("title");
	}
	public void setTitle(java.lang.String title) {
		this.set("title", title);
	}
	public java.lang.String getType() {
		return this.get("type");
	}
	public void setType(java.lang.String type) {
		this.set("type", type);
	}
	public java.util.Date getCreated() {
		return this.get("created");
	}
	public void setCreated(java.util.Date created) {
		this.set("created", created);
	}
	public java.lang.Long getStime() {
		return this.get("stime");
	}
	public void setStime(java.lang.Long stime) {
		this.set("stime", stime);
	}
	public java.lang.Long getEtime() {
		return this.get("etime");
	}
	public void setEtime(java.lang.Long etime) {
		this.set("etime", etime);
	}
}
