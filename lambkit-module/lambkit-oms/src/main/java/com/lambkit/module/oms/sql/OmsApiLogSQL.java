/**
 * Copyright (c) 2015-2017, Henry Yang 杨勇 (gismail@foxmail.com).
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.lambkit.module.oms.sql;

import com.lambkit.db.sql.ConditionMode;
import com.lambkit.db.sql.Column;
import com.lambkit.db.sql.Columns;
import com.lambkit.db.sql.Example;

import java.io.Serializable;
import java.util.List;

/**
 * @author yangyong 
 * @website: www.lambkit.com
 * @email: gismail@foxmail.com
 * @date ${date}
 * @version 1.0
 * @since 1.0
 */

public class OmsApiLogSQL extends Columns implements Serializable {
	private static final long serialVersionUID = 1L;
	
	public static OmsApiLogSQL of() {
		return new OmsApiLogSQL();
	}
	
	public static OmsApiLogSQL by(Column column) {
		OmsApiLogSQL that = new OmsApiLogSQL();
		that.add(column);
        return that;
    }

    public static OmsApiLogSQL by(String name, Object value) {
        return (OmsApiLogSQL) create().eq(name, value);
    }
    
    public Example example() {
    	return Example.create("oms_api_log", this);
    }
    
    /**
     * equals
     *
     * @param name
     * @param value
     * @return
     */
    public OmsApiLogSQL eq(String name, Object value) {
    	super.eq(name, value);
        return this;
    }

    /**
     * not equals !=
     *
     * @param name
     * @param value
     * @return
     */
    public OmsApiLogSQL ne(String name, Object value) {
    	super.ne(name, value);
        return this;
    }


    /**
     * like
     *
     * @param name
     * @param value
     * @return
     */

    public OmsApiLogSQL like(String name, Object value) {
    	super.like(name, value);
        return this;
    }
    
    public OmsApiLogSQL notLike(String name, Object value) {
    	super.notLike(name, value);
        return this;
    }

    /**
     * 大于 great than
     *
     * @param name
     * @param value
     * @return
     */
    public OmsApiLogSQL gt(String name, Object value) {
    	super.gt(name, value);
        return this;
    }

    /**
     * 大于等于 great or equal
     *
     * @param name
     * @param value
     * @return
     */
    public OmsApiLogSQL ge(String name, Object value) {
    	super.ge(name, value);
        return this;
    }

    /**
     * 小于 less than
     *
     * @param name
     * @param value
     * @return
     */
    public OmsApiLogSQL lt(String name, Object value) {
    	super.lt(name, value);
        return this;
    }

    /**
     * 小于等于 less or equal
     *
     * @param name
     * @param value
     * @return
     */
    public OmsApiLogSQL le(String name, Object value) {
    	super.le(name, value);
        return this;
    }
    
    public OmsApiLogSQL isnull(String name) {
    	super.isnull(name);
        return this;
    } 

    public OmsApiLogSQL notNull(String name) {
    	super.notNull(name);
        return this;
    } 
    
    public OmsApiLogSQL empty(String name) {
    	super.empty(name);
        return this;
    } 
    
    public OmsApiLogSQL notEmpty(String name) {
    	super.notEmpty(name);
        return this;
    } 
    
    public OmsApiLogSQL add(Column column) {
    	super.add(column);
    	return this;
    }
    
    /**************************/
	
	public void addCriterion(String name, Object value, ConditionMode logic, String property, String typeHandler, String valueType) {
		 if (value == null) {
			 throw new RuntimeException("Value for " + property + " cannot be null");
		 }
		 add(Column.create(name, value, logic, typeHandler, valueType));
	}
   
	public void addCriterion(String name, Object value1, Object value2, ConditionMode logic, String property, String typeHandler, String valueType) {
		 if (value1 == null || value2 == null) {
			 throw new RuntimeException("Between values for " + property + " cannot be null");
		 }
		 add(Column.create(name, value1, value2, logic, typeHandler, valueType));
	}
		 
	public OmsApiLogSQL andInfocodeIsNull() {
		isnull("infocode");
		return this;
	}
	
	public OmsApiLogSQL andInfocodeIsNotNull() {
		notNull("infocode");
		return this;
	}
	
	public OmsApiLogSQL andInfocodeIsEmpty() {
		empty("infocode");
		return this;
	}

	public OmsApiLogSQL andInfocodeIsNotEmpty() {
		notEmpty("infocode");
		return this;
	}
       public OmsApiLogSQL andInfocodeLike(java.lang.String value) {
    	   addCriterion("infocode", value, ConditionMode.FUZZY, "infocode", "java.lang.String", "String");
    	   return this;
      }

      public OmsApiLogSQL andInfocodeNotLike(java.lang.String value) {
          addCriterion("infocode", value, ConditionMode.NOT_FUZZY, "infocode", "java.lang.String", "String");
          return this;
      }
      public OmsApiLogSQL andInfocodeEqualTo(java.lang.String value) {
          addCriterion("infocode", value, ConditionMode.EQUAL, "infocode", "java.lang.String", "String");
          return this;
      }

      public OmsApiLogSQL andInfocodeNotEqualTo(java.lang.String value) {
          addCriterion("infocode", value, ConditionMode.NOT_EQUAL, "infocode", "java.lang.String", "String");
          return this;
      }

      public OmsApiLogSQL andInfocodeGreaterThan(java.lang.String value) {
          addCriterion("infocode", value, ConditionMode.GREATER_THEN, "infocode", "java.lang.String", "String");
          return this;
      }

      public OmsApiLogSQL andInfocodeGreaterThanOrEqualTo(java.lang.String value) {
          addCriterion("infocode", value, ConditionMode.GREATER_EQUAL, "infocode", "java.lang.String", "String");
          return this;
      }

      public OmsApiLogSQL andInfocodeLessThan(java.lang.String value) {
          addCriterion("infocode", value, ConditionMode.LESS_THEN, "infocode", "java.lang.String", "String");
          return this;
      }

      public OmsApiLogSQL andInfocodeLessThanOrEqualTo(java.lang.String value) {
          addCriterion("infocode", value, ConditionMode.LESS_EQUAL, "infocode", "java.lang.String", "String");
          return this;
      }

      public OmsApiLogSQL andInfocodeBetween(java.lang.String value1, java.lang.String value2) {
    	  addCriterion("infocode", value1, value2, ConditionMode.BETWEEN, "infocode", "java.lang.String", "String");
    	  return this;
      }

      public OmsApiLogSQL andInfocodeNotBetween(java.lang.String value1, java.lang.String value2) {
          addCriterion("infocode", value1, value2, ConditionMode.NOT_BETWEEN, "infocode", "java.lang.String", "String");
          return this;
      }
        
      public OmsApiLogSQL andInfocodeIn(List<java.lang.String> values) {
          addCriterion("infocode", values, ConditionMode.IN, "infocode", "java.lang.String", "String");
          return this;
      }

      public OmsApiLogSQL andInfocodeNotIn(List<java.lang.String> values) {
          addCriterion("infocode", values, ConditionMode.NOT_IN, "infocode", "java.lang.String", "String");
          return this;
      }
	public OmsApiLogSQL andNodecodeIsNull() {
		isnull("nodecode");
		return this;
	}
	
	public OmsApiLogSQL andNodecodeIsNotNull() {
		notNull("nodecode");
		return this;
	}
	
	public OmsApiLogSQL andNodecodeIsEmpty() {
		empty("nodecode");
		return this;
	}

	public OmsApiLogSQL andNodecodeIsNotEmpty() {
		notEmpty("nodecode");
		return this;
	}
       public OmsApiLogSQL andNodecodeLike(java.lang.String value) {
    	   addCriterion("nodecode", value, ConditionMode.FUZZY, "nodecode", "java.lang.String", "String");
    	   return this;
      }

      public OmsApiLogSQL andNodecodeNotLike(java.lang.String value) {
          addCriterion("nodecode", value, ConditionMode.NOT_FUZZY, "nodecode", "java.lang.String", "String");
          return this;
      }
      public OmsApiLogSQL andNodecodeEqualTo(java.lang.String value) {
          addCriterion("nodecode", value, ConditionMode.EQUAL, "nodecode", "java.lang.String", "String");
          return this;
      }

      public OmsApiLogSQL andNodecodeNotEqualTo(java.lang.String value) {
          addCriterion("nodecode", value, ConditionMode.NOT_EQUAL, "nodecode", "java.lang.String", "String");
          return this;
      }

      public OmsApiLogSQL andNodecodeGreaterThan(java.lang.String value) {
          addCriterion("nodecode", value, ConditionMode.GREATER_THEN, "nodecode", "java.lang.String", "String");
          return this;
      }

      public OmsApiLogSQL andNodecodeGreaterThanOrEqualTo(java.lang.String value) {
          addCriterion("nodecode", value, ConditionMode.GREATER_EQUAL, "nodecode", "java.lang.String", "String");
          return this;
      }

      public OmsApiLogSQL andNodecodeLessThan(java.lang.String value) {
          addCriterion("nodecode", value, ConditionMode.LESS_THEN, "nodecode", "java.lang.String", "String");
          return this;
      }

      public OmsApiLogSQL andNodecodeLessThanOrEqualTo(java.lang.String value) {
          addCriterion("nodecode", value, ConditionMode.LESS_EQUAL, "nodecode", "java.lang.String", "String");
          return this;
      }

      public OmsApiLogSQL andNodecodeBetween(java.lang.String value1, java.lang.String value2) {
    	  addCriterion("nodecode", value1, value2, ConditionMode.BETWEEN, "nodecode", "java.lang.String", "String");
    	  return this;
      }

      public OmsApiLogSQL andNodecodeNotBetween(java.lang.String value1, java.lang.String value2) {
          addCriterion("nodecode", value1, value2, ConditionMode.NOT_BETWEEN, "nodecode", "java.lang.String", "String");
          return this;
      }
        
      public OmsApiLogSQL andNodecodeIn(List<java.lang.String> values) {
          addCriterion("nodecode", values, ConditionMode.IN, "nodecode", "java.lang.String", "String");
          return this;
      }

      public OmsApiLogSQL andNodecodeNotIn(List<java.lang.String> values) {
          addCriterion("nodecode", values, ConditionMode.NOT_IN, "nodecode", "java.lang.String", "String");
          return this;
      }
	public OmsApiLogSQL andTablenameIsNull() {
		isnull("tablename");
		return this;
	}
	
	public OmsApiLogSQL andTablenameIsNotNull() {
		notNull("tablename");
		return this;
	}
	
	public OmsApiLogSQL andTablenameIsEmpty() {
		empty("tablename");
		return this;
	}

	public OmsApiLogSQL andTablenameIsNotEmpty() {
		notEmpty("tablename");
		return this;
	}
       public OmsApiLogSQL andTablenameLike(java.lang.String value) {
    	   addCriterion("tablename", value, ConditionMode.FUZZY, "tablename", "java.lang.String", "String");
    	   return this;
      }

      public OmsApiLogSQL andTablenameNotLike(java.lang.String value) {
          addCriterion("tablename", value, ConditionMode.NOT_FUZZY, "tablename", "java.lang.String", "String");
          return this;
      }
      public OmsApiLogSQL andTablenameEqualTo(java.lang.String value) {
          addCriterion("tablename", value, ConditionMode.EQUAL, "tablename", "java.lang.String", "String");
          return this;
      }

      public OmsApiLogSQL andTablenameNotEqualTo(java.lang.String value) {
          addCriterion("tablename", value, ConditionMode.NOT_EQUAL, "tablename", "java.lang.String", "String");
          return this;
      }

      public OmsApiLogSQL andTablenameGreaterThan(java.lang.String value) {
          addCriterion("tablename", value, ConditionMode.GREATER_THEN, "tablename", "java.lang.String", "String");
          return this;
      }

      public OmsApiLogSQL andTablenameGreaterThanOrEqualTo(java.lang.String value) {
          addCriterion("tablename", value, ConditionMode.GREATER_EQUAL, "tablename", "java.lang.String", "String");
          return this;
      }

      public OmsApiLogSQL andTablenameLessThan(java.lang.String value) {
          addCriterion("tablename", value, ConditionMode.LESS_THEN, "tablename", "java.lang.String", "String");
          return this;
      }

      public OmsApiLogSQL andTablenameLessThanOrEqualTo(java.lang.String value) {
          addCriterion("tablename", value, ConditionMode.LESS_EQUAL, "tablename", "java.lang.String", "String");
          return this;
      }

      public OmsApiLogSQL andTablenameBetween(java.lang.String value1, java.lang.String value2) {
    	  addCriterion("tablename", value1, value2, ConditionMode.BETWEEN, "tablename", "java.lang.String", "String");
    	  return this;
      }

      public OmsApiLogSQL andTablenameNotBetween(java.lang.String value1, java.lang.String value2) {
          addCriterion("tablename", value1, value2, ConditionMode.NOT_BETWEEN, "tablename", "java.lang.String", "String");
          return this;
      }
        
      public OmsApiLogSQL andTablenameIn(List<java.lang.String> values) {
          addCriterion("tablename", values, ConditionMode.IN, "tablename", "java.lang.String", "String");
          return this;
      }

      public OmsApiLogSQL andTablenameNotIn(List<java.lang.String> values) {
          addCriterion("tablename", values, ConditionMode.NOT_IN, "tablename", "java.lang.String", "String");
          return this;
      }
	public OmsApiLogSQL andGuidIsNull() {
		isnull("guid");
		return this;
	}
	
	public OmsApiLogSQL andGuidIsNotNull() {
		notNull("guid");
		return this;
	}
	
	public OmsApiLogSQL andGuidIsEmpty() {
		empty("guid");
		return this;
	}

	public OmsApiLogSQL andGuidIsNotEmpty() {
		notEmpty("guid");
		return this;
	}
       public OmsApiLogSQL andGuidLike(java.lang.String value) {
    	   addCriterion("guid", value, ConditionMode.FUZZY, "guid", "java.lang.String", "String");
    	   return this;
      }

      public OmsApiLogSQL andGuidNotLike(java.lang.String value) {
          addCriterion("guid", value, ConditionMode.NOT_FUZZY, "guid", "java.lang.String", "String");
          return this;
      }
      public OmsApiLogSQL andGuidEqualTo(java.lang.String value) {
          addCriterion("guid", value, ConditionMode.EQUAL, "guid", "java.lang.String", "String");
          return this;
      }

      public OmsApiLogSQL andGuidNotEqualTo(java.lang.String value) {
          addCriterion("guid", value, ConditionMode.NOT_EQUAL, "guid", "java.lang.String", "String");
          return this;
      }

      public OmsApiLogSQL andGuidGreaterThan(java.lang.String value) {
          addCriterion("guid", value, ConditionMode.GREATER_THEN, "guid", "java.lang.String", "String");
          return this;
      }

      public OmsApiLogSQL andGuidGreaterThanOrEqualTo(java.lang.String value) {
          addCriterion("guid", value, ConditionMode.GREATER_EQUAL, "guid", "java.lang.String", "String");
          return this;
      }

      public OmsApiLogSQL andGuidLessThan(java.lang.String value) {
          addCriterion("guid", value, ConditionMode.LESS_THEN, "guid", "java.lang.String", "String");
          return this;
      }

      public OmsApiLogSQL andGuidLessThanOrEqualTo(java.lang.String value) {
          addCriterion("guid", value, ConditionMode.LESS_EQUAL, "guid", "java.lang.String", "String");
          return this;
      }

      public OmsApiLogSQL andGuidBetween(java.lang.String value1, java.lang.String value2) {
    	  addCriterion("guid", value1, value2, ConditionMode.BETWEEN, "guid", "java.lang.String", "String");
    	  return this;
      }

      public OmsApiLogSQL andGuidNotBetween(java.lang.String value1, java.lang.String value2) {
          addCriterion("guid", value1, value2, ConditionMode.NOT_BETWEEN, "guid", "java.lang.String", "String");
          return this;
      }
        
      public OmsApiLogSQL andGuidIn(List<java.lang.String> values) {
          addCriterion("guid", values, ConditionMode.IN, "guid", "java.lang.String", "String");
          return this;
      }

      public OmsApiLogSQL andGuidNotIn(List<java.lang.String> values) {
          addCriterion("guid", values, ConditionMode.NOT_IN, "guid", "java.lang.String", "String");
          return this;
      }
	public OmsApiLogSQL andMarkIsNull() {
		isnull("mark");
		return this;
	}
	
	public OmsApiLogSQL andMarkIsNotNull() {
		notNull("mark");
		return this;
	}
	
	public OmsApiLogSQL andMarkIsEmpty() {
		empty("mark");
		return this;
	}

	public OmsApiLogSQL andMarkIsNotEmpty() {
		notEmpty("mark");
		return this;
	}
       public OmsApiLogSQL andMarkLike(java.lang.String value) {
    	   addCriterion("mark", value, ConditionMode.FUZZY, "mark", "java.lang.String", "String");
    	   return this;
      }

      public OmsApiLogSQL andMarkNotLike(java.lang.String value) {
          addCriterion("mark", value, ConditionMode.NOT_FUZZY, "mark", "java.lang.String", "String");
          return this;
      }
      public OmsApiLogSQL andMarkEqualTo(java.lang.String value) {
          addCriterion("mark", value, ConditionMode.EQUAL, "mark", "java.lang.String", "String");
          return this;
      }

      public OmsApiLogSQL andMarkNotEqualTo(java.lang.String value) {
          addCriterion("mark", value, ConditionMode.NOT_EQUAL, "mark", "java.lang.String", "String");
          return this;
      }

      public OmsApiLogSQL andMarkGreaterThan(java.lang.String value) {
          addCriterion("mark", value, ConditionMode.GREATER_THEN, "mark", "java.lang.String", "String");
          return this;
      }

      public OmsApiLogSQL andMarkGreaterThanOrEqualTo(java.lang.String value) {
          addCriterion("mark", value, ConditionMode.GREATER_EQUAL, "mark", "java.lang.String", "String");
          return this;
      }

      public OmsApiLogSQL andMarkLessThan(java.lang.String value) {
          addCriterion("mark", value, ConditionMode.LESS_THEN, "mark", "java.lang.String", "String");
          return this;
      }

      public OmsApiLogSQL andMarkLessThanOrEqualTo(java.lang.String value) {
          addCriterion("mark", value, ConditionMode.LESS_EQUAL, "mark", "java.lang.String", "String");
          return this;
      }

      public OmsApiLogSQL andMarkBetween(java.lang.String value1, java.lang.String value2) {
    	  addCriterion("mark", value1, value2, ConditionMode.BETWEEN, "mark", "java.lang.String", "String");
    	  return this;
      }

      public OmsApiLogSQL andMarkNotBetween(java.lang.String value1, java.lang.String value2) {
          addCriterion("mark", value1, value2, ConditionMode.NOT_BETWEEN, "mark", "java.lang.String", "String");
          return this;
      }
        
      public OmsApiLogSQL andMarkIn(List<java.lang.String> values) {
          addCriterion("mark", values, ConditionMode.IN, "mark", "java.lang.String", "String");
          return this;
      }

      public OmsApiLogSQL andMarkNotIn(List<java.lang.String> values) {
          addCriterion("mark", values, ConditionMode.NOT_IN, "mark", "java.lang.String", "String");
          return this;
      }
	public OmsApiLogSQL andTypeIsNull() {
		isnull("type");
		return this;
	}
	
	public OmsApiLogSQL andTypeIsNotNull() {
		notNull("type");
		return this;
	}
	
	public OmsApiLogSQL andTypeIsEmpty() {
		empty("type");
		return this;
	}

	public OmsApiLogSQL andTypeIsNotEmpty() {
		notEmpty("type");
		return this;
	}
       public OmsApiLogSQL andTypeLike(java.lang.String value) {
    	   addCriterion("type", value, ConditionMode.FUZZY, "type", "java.lang.String", "String");
    	   return this;
      }

      public OmsApiLogSQL andTypeNotLike(java.lang.String value) {
          addCriterion("type", value, ConditionMode.NOT_FUZZY, "type", "java.lang.String", "String");
          return this;
      }
      public OmsApiLogSQL andTypeEqualTo(java.lang.String value) {
          addCriterion("type", value, ConditionMode.EQUAL, "type", "java.lang.String", "String");
          return this;
      }

      public OmsApiLogSQL andTypeNotEqualTo(java.lang.String value) {
          addCriterion("type", value, ConditionMode.NOT_EQUAL, "type", "java.lang.String", "String");
          return this;
      }

      public OmsApiLogSQL andTypeGreaterThan(java.lang.String value) {
          addCriterion("type", value, ConditionMode.GREATER_THEN, "type", "java.lang.String", "String");
          return this;
      }

      public OmsApiLogSQL andTypeGreaterThanOrEqualTo(java.lang.String value) {
          addCriterion("type", value, ConditionMode.GREATER_EQUAL, "type", "java.lang.String", "String");
          return this;
      }

      public OmsApiLogSQL andTypeLessThan(java.lang.String value) {
          addCriterion("type", value, ConditionMode.LESS_THEN, "type", "java.lang.String", "String");
          return this;
      }

      public OmsApiLogSQL andTypeLessThanOrEqualTo(java.lang.String value) {
          addCriterion("type", value, ConditionMode.LESS_EQUAL, "type", "java.lang.String", "String");
          return this;
      }

      public OmsApiLogSQL andTypeBetween(java.lang.String value1, java.lang.String value2) {
    	  addCriterion("type", value1, value2, ConditionMode.BETWEEN, "type", "java.lang.String", "String");
    	  return this;
      }

      public OmsApiLogSQL andTypeNotBetween(java.lang.String value1, java.lang.String value2) {
          addCriterion("type", value1, value2, ConditionMode.NOT_BETWEEN, "type", "java.lang.String", "String");
          return this;
      }
        
      public OmsApiLogSQL andTypeIn(List<java.lang.String> values) {
          addCriterion("type", values, ConditionMode.IN, "type", "java.lang.String", "String");
          return this;
      }

      public OmsApiLogSQL andTypeNotIn(List<java.lang.String> values) {
          addCriterion("type", values, ConditionMode.NOT_IN, "type", "java.lang.String", "String");
          return this;
      }
	public OmsApiLogSQL andStatusIsNull() {
		isnull("status");
		return this;
	}
	
	public OmsApiLogSQL andStatusIsNotNull() {
		notNull("status");
		return this;
	}
	
	public OmsApiLogSQL andStatusIsEmpty() {
		empty("status");
		return this;
	}

	public OmsApiLogSQL andStatusIsNotEmpty() {
		notEmpty("status");
		return this;
	}
      public OmsApiLogSQL andStatusEqualTo(java.lang.Integer value) {
          addCriterion("status", value, ConditionMode.EQUAL, "status", "java.lang.Integer", "Float");
          return this;
      }

      public OmsApiLogSQL andStatusNotEqualTo(java.lang.Integer value) {
          addCriterion("status", value, ConditionMode.NOT_EQUAL, "status", "java.lang.Integer", "Float");
          return this;
      }

      public OmsApiLogSQL andStatusGreaterThan(java.lang.Integer value) {
          addCriterion("status", value, ConditionMode.GREATER_THEN, "status", "java.lang.Integer", "Float");
          return this;
      }

      public OmsApiLogSQL andStatusGreaterThanOrEqualTo(java.lang.Integer value) {
          addCriterion("status", value, ConditionMode.GREATER_EQUAL, "status", "java.lang.Integer", "Float");
          return this;
      }

      public OmsApiLogSQL andStatusLessThan(java.lang.Integer value) {
          addCriterion("status", value, ConditionMode.LESS_THEN, "status", "java.lang.Integer", "Float");
          return this;
      }

      public OmsApiLogSQL andStatusLessThanOrEqualTo(java.lang.Integer value) {
          addCriterion("status", value, ConditionMode.LESS_EQUAL, "status", "java.lang.Integer", "Float");
          return this;
      }

      public OmsApiLogSQL andStatusBetween(java.lang.Integer value1, java.lang.Integer value2) {
    	  addCriterion("status", value1, value2, ConditionMode.BETWEEN, "status", "java.lang.Integer", "Float");
    	  return this;
      }

      public OmsApiLogSQL andStatusNotBetween(java.lang.Integer value1, java.lang.Integer value2) {
          addCriterion("status", value1, value2, ConditionMode.NOT_BETWEEN, "status", "java.lang.Integer", "Float");
          return this;
      }
        
      public OmsApiLogSQL andStatusIn(List<java.lang.Integer> values) {
          addCriterion("status", values, ConditionMode.IN, "status", "java.lang.Integer", "Float");
          return this;
      }

      public OmsApiLogSQL andStatusNotIn(List<java.lang.Integer> values) {
          addCriterion("status", values, ConditionMode.NOT_IN, "status", "java.lang.Integer", "Float");
          return this;
      }
	public OmsApiLogSQL andMsgIsNull() {
		isnull("msg");
		return this;
	}
	
	public OmsApiLogSQL andMsgIsNotNull() {
		notNull("msg");
		return this;
	}
	
	public OmsApiLogSQL andMsgIsEmpty() {
		empty("msg");
		return this;
	}

	public OmsApiLogSQL andMsgIsNotEmpty() {
		notEmpty("msg");
		return this;
	}
       public OmsApiLogSQL andMsgLike(java.lang.String value) {
    	   addCriterion("msg", value, ConditionMode.FUZZY, "msg", "java.lang.String", "Float");
    	   return this;
      }

      public OmsApiLogSQL andMsgNotLike(java.lang.String value) {
          addCriterion("msg", value, ConditionMode.NOT_FUZZY, "msg", "java.lang.String", "Float");
          return this;
      }
      public OmsApiLogSQL andMsgEqualTo(java.lang.String value) {
          addCriterion("msg", value, ConditionMode.EQUAL, "msg", "java.lang.String", "String");
          return this;
      }

      public OmsApiLogSQL andMsgNotEqualTo(java.lang.String value) {
          addCriterion("msg", value, ConditionMode.NOT_EQUAL, "msg", "java.lang.String", "String");
          return this;
      }

      public OmsApiLogSQL andMsgGreaterThan(java.lang.String value) {
          addCriterion("msg", value, ConditionMode.GREATER_THEN, "msg", "java.lang.String", "String");
          return this;
      }

      public OmsApiLogSQL andMsgGreaterThanOrEqualTo(java.lang.String value) {
          addCriterion("msg", value, ConditionMode.GREATER_EQUAL, "msg", "java.lang.String", "String");
          return this;
      }

      public OmsApiLogSQL andMsgLessThan(java.lang.String value) {
          addCriterion("msg", value, ConditionMode.LESS_THEN, "msg", "java.lang.String", "String");
          return this;
      }

      public OmsApiLogSQL andMsgLessThanOrEqualTo(java.lang.String value) {
          addCriterion("msg", value, ConditionMode.LESS_EQUAL, "msg", "java.lang.String", "String");
          return this;
      }

      public OmsApiLogSQL andMsgBetween(java.lang.String value1, java.lang.String value2) {
    	  addCriterion("msg", value1, value2, ConditionMode.BETWEEN, "msg", "java.lang.String", "String");
    	  return this;
      }

      public OmsApiLogSQL andMsgNotBetween(java.lang.String value1, java.lang.String value2) {
          addCriterion("msg", value1, value2, ConditionMode.NOT_BETWEEN, "msg", "java.lang.String", "String");
          return this;
      }
        
      public OmsApiLogSQL andMsgIn(List<java.lang.String> values) {
          addCriterion("msg", values, ConditionMode.IN, "msg", "java.lang.String", "String");
          return this;
      }

      public OmsApiLogSQL andMsgNotIn(List<java.lang.String> values) {
          addCriterion("msg", values, ConditionMode.NOT_IN, "msg", "java.lang.String", "String");
          return this;
      }
}