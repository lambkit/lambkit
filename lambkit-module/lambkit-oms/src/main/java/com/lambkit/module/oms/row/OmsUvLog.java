/**
 * Copyright (c) 2015-2017, Henry Yang 杨勇 (gismail@foxmail.com).
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.lambkit.module.oms.row;

import com.lambkit.db.RowModel;

/**
 * @author yangyong 
 * @website: www.lambkit.com
 * @email: gismail@foxmail.com
 * @date ${date}
 * @version 1.0
 * @since 1.0
 */

public class OmsUvLog extends RowModel<OmsUvLog> {
	public OmsUvLog() {
		setTableName("oms_uv_log");
		setPrimaryKey("id");
	}
	public java.lang.Long getId() {
		return this.get("id");
	}
	public void setId(java.lang.Long id) {
		this.set("id", id);
	}
	public java.lang.Integer getSystemId() {
		return this.get("system_id");
	}
	public void setSystemId(java.lang.Integer systemId) {
		this.set("system_id", systemId);
	}
	public java.lang.Long getUpmsUserId() {
		return this.get("upms_user_id");
	}
	public void setUpmsUserId(java.lang.Long upmsUserId) {
		this.set("upms_user_id", upmsUserId);
	}
	public java.lang.String getSessionId() {
		return this.get("session_id");
	}
	public void setSessionId(java.lang.String sessionId) {
		this.set("session_id", sessionId);
	}
	public java.lang.String getUserId() {
		return this.get("user_id");
	}
	public void setUserId(java.lang.String userId) {
		this.set("user_id", userId);
	}
	public java.util.Date getCreateTime() {
		return this.get("create_time");
	}
	public void setCreateTime(java.util.Date createTime) {
		this.set("create_time", createTime);
	}
	public java.lang.String getAreaCode() {
		return this.get("area_code");
	}
	public void setAreaCode(java.lang.String areaCode) {
		this.set("area_code", areaCode);
	}
}
