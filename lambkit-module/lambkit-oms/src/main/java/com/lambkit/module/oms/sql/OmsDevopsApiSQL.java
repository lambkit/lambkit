/**
 * Copyright (c) 2015-2017, Henry Yang 杨勇 (gismail@foxmail.com).
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.lambkit.module.oms.sql;

import com.lambkit.db.sql.ConditionMode;
import com.lambkit.db.sql.Column;
import com.lambkit.db.sql.Columns;
import com.lambkit.db.sql.Example;

import java.io.Serializable;
import java.util.List;

/**
 * @author yangyong 
 * @website: www.lambkit.com
 * @email: gismail@foxmail.com
 * @date ${date}
 * @version 1.0
 * @since 1.0
 */

public class OmsDevopsApiSQL extends Columns implements Serializable {
	private static final long serialVersionUID = 1L;
	
	public static OmsDevopsApiSQL of() {
		return new OmsDevopsApiSQL();
	}
	
	public static OmsDevopsApiSQL by(Column column) {
		OmsDevopsApiSQL that = new OmsDevopsApiSQL();
		that.add(column);
        return that;
    }

    public static OmsDevopsApiSQL by(String name, Object value) {
        return (OmsDevopsApiSQL) create().eq(name, value);
    }
    
    public Example example() {
    	return Example.create("oms_devops_api", this);
    }
    
    /**
     * equals
     *
     * @param name
     * @param value
     * @return
     */
    public OmsDevopsApiSQL eq(String name, Object value) {
    	super.eq(name, value);
        return this;
    }

    /**
     * not equals !=
     *
     * @param name
     * @param value
     * @return
     */
    public OmsDevopsApiSQL ne(String name, Object value) {
    	super.ne(name, value);
        return this;
    }


    /**
     * like
     *
     * @param name
     * @param value
     * @return
     */

    public OmsDevopsApiSQL like(String name, Object value) {
    	super.like(name, value);
        return this;
    }
    
    public OmsDevopsApiSQL notLike(String name, Object value) {
    	super.notLike(name, value);
        return this;
    }

    /**
     * 大于 great than
     *
     * @param name
     * @param value
     * @return
     */
    public OmsDevopsApiSQL gt(String name, Object value) {
    	super.gt(name, value);
        return this;
    }

    /**
     * 大于等于 great or equal
     *
     * @param name
     * @param value
     * @return
     */
    public OmsDevopsApiSQL ge(String name, Object value) {
    	super.ge(name, value);
        return this;
    }

    /**
     * 小于 less than
     *
     * @param name
     * @param value
     * @return
     */
    public OmsDevopsApiSQL lt(String name, Object value) {
    	super.lt(name, value);
        return this;
    }

    /**
     * 小于等于 less or equal
     *
     * @param name
     * @param value
     * @return
     */
    public OmsDevopsApiSQL le(String name, Object value) {
    	super.le(name, value);
        return this;
    }
    
    public OmsDevopsApiSQL isnull(String name) {
    	super.isnull(name);
        return this;
    } 

    public OmsDevopsApiSQL notNull(String name) {
    	super.notNull(name);
        return this;
    } 
    
    public OmsDevopsApiSQL empty(String name) {
    	super.empty(name);
        return this;
    } 
    
    public OmsDevopsApiSQL notEmpty(String name) {
    	super.notEmpty(name);
        return this;
    } 
    
    public OmsDevopsApiSQL add(Column column) {
    	super.add(column);
    	return this;
    }
    
    /**************************/
	
	public void addCriterion(String name, Object value, ConditionMode logic, String property, String typeHandler, String valueType) {
		 if (value == null) {
			 throw new RuntimeException("Value for " + property + " cannot be null");
		 }
		 add(Column.create(name, value, logic, typeHandler, valueType));
	}
   
	public void addCriterion(String name, Object value1, Object value2, ConditionMode logic, String property, String typeHandler, String valueType) {
		 if (value1 == null || value2 == null) {
			 throw new RuntimeException("Between values for " + property + " cannot be null");
		 }
		 add(Column.create(name, value1, value2, logic, typeHandler, valueType));
	}
		 
	public OmsDevopsApiSQL andDevopsApiIdIsNull() {
		isnull("devops_api_id");
		return this;
	}
	
	public OmsDevopsApiSQL andDevopsApiIdIsNotNull() {
		notNull("devops_api_id");
		return this;
	}
	
	public OmsDevopsApiSQL andDevopsApiIdIsEmpty() {
		empty("devops_api_id");
		return this;
	}

	public OmsDevopsApiSQL andDevopsApiIdIsNotEmpty() {
		notEmpty("devops_api_id");
		return this;
	}
      public OmsDevopsApiSQL andDevopsApiIdEqualTo(java.lang.Long value) {
          addCriterion("devops_api_id", value, ConditionMode.EQUAL, "devopsApiId", "java.lang.Long", "Float");
          return this;
      }

      public OmsDevopsApiSQL andDevopsApiIdNotEqualTo(java.lang.Long value) {
          addCriterion("devops_api_id", value, ConditionMode.NOT_EQUAL, "devopsApiId", "java.lang.Long", "Float");
          return this;
      }

      public OmsDevopsApiSQL andDevopsApiIdGreaterThan(java.lang.Long value) {
          addCriterion("devops_api_id", value, ConditionMode.GREATER_THEN, "devopsApiId", "java.lang.Long", "Float");
          return this;
      }

      public OmsDevopsApiSQL andDevopsApiIdGreaterThanOrEqualTo(java.lang.Long value) {
          addCriterion("devops_api_id", value, ConditionMode.GREATER_EQUAL, "devopsApiId", "java.lang.Long", "Float");
          return this;
      }

      public OmsDevopsApiSQL andDevopsApiIdLessThan(java.lang.Long value) {
          addCriterion("devops_api_id", value, ConditionMode.LESS_THEN, "devopsApiId", "java.lang.Long", "Float");
          return this;
      }

      public OmsDevopsApiSQL andDevopsApiIdLessThanOrEqualTo(java.lang.Long value) {
          addCriterion("devops_api_id", value, ConditionMode.LESS_EQUAL, "devopsApiId", "java.lang.Long", "Float");
          return this;
      }

      public OmsDevopsApiSQL andDevopsApiIdBetween(java.lang.Long value1, java.lang.Long value2) {
    	  addCriterion("devops_api_id", value1, value2, ConditionMode.BETWEEN, "devopsApiId", "java.lang.Long", "Float");
    	  return this;
      }

      public OmsDevopsApiSQL andDevopsApiIdNotBetween(java.lang.Long value1, java.lang.Long value2) {
          addCriterion("devops_api_id", value1, value2, ConditionMode.NOT_BETWEEN, "devopsApiId", "java.lang.Long", "Float");
          return this;
      }
        
      public OmsDevopsApiSQL andDevopsApiIdIn(List<java.lang.Long> values) {
          addCriterion("devops_api_id", values, ConditionMode.IN, "devopsApiId", "java.lang.Long", "Float");
          return this;
      }

      public OmsDevopsApiSQL andDevopsApiIdNotIn(List<java.lang.Long> values) {
          addCriterion("devops_api_id", values, ConditionMode.NOT_IN, "devopsApiId", "java.lang.Long", "Float");
          return this;
      }
	public OmsDevopsApiSQL andNameIsNull() {
		isnull("name");
		return this;
	}
	
	public OmsDevopsApiSQL andNameIsNotNull() {
		notNull("name");
		return this;
	}
	
	public OmsDevopsApiSQL andNameIsEmpty() {
		empty("name");
		return this;
	}

	public OmsDevopsApiSQL andNameIsNotEmpty() {
		notEmpty("name");
		return this;
	}
       public OmsDevopsApiSQL andNameLike(java.lang.String value) {
    	   addCriterion("name", value, ConditionMode.FUZZY, "name", "java.lang.String", "Float");
    	   return this;
      }

      public OmsDevopsApiSQL andNameNotLike(java.lang.String value) {
          addCriterion("name", value, ConditionMode.NOT_FUZZY, "name", "java.lang.String", "Float");
          return this;
      }
      public OmsDevopsApiSQL andNameEqualTo(java.lang.String value) {
          addCriterion("name", value, ConditionMode.EQUAL, "name", "java.lang.String", "String");
          return this;
      }

      public OmsDevopsApiSQL andNameNotEqualTo(java.lang.String value) {
          addCriterion("name", value, ConditionMode.NOT_EQUAL, "name", "java.lang.String", "String");
          return this;
      }

      public OmsDevopsApiSQL andNameGreaterThan(java.lang.String value) {
          addCriterion("name", value, ConditionMode.GREATER_THEN, "name", "java.lang.String", "String");
          return this;
      }

      public OmsDevopsApiSQL andNameGreaterThanOrEqualTo(java.lang.String value) {
          addCriterion("name", value, ConditionMode.GREATER_EQUAL, "name", "java.lang.String", "String");
          return this;
      }

      public OmsDevopsApiSQL andNameLessThan(java.lang.String value) {
          addCriterion("name", value, ConditionMode.LESS_THEN, "name", "java.lang.String", "String");
          return this;
      }

      public OmsDevopsApiSQL andNameLessThanOrEqualTo(java.lang.String value) {
          addCriterion("name", value, ConditionMode.LESS_EQUAL, "name", "java.lang.String", "String");
          return this;
      }

      public OmsDevopsApiSQL andNameBetween(java.lang.String value1, java.lang.String value2) {
    	  addCriterion("name", value1, value2, ConditionMode.BETWEEN, "name", "java.lang.String", "String");
    	  return this;
      }

      public OmsDevopsApiSQL andNameNotBetween(java.lang.String value1, java.lang.String value2) {
          addCriterion("name", value1, value2, ConditionMode.NOT_BETWEEN, "name", "java.lang.String", "String");
          return this;
      }
        
      public OmsDevopsApiSQL andNameIn(List<java.lang.String> values) {
          addCriterion("name", values, ConditionMode.IN, "name", "java.lang.String", "String");
          return this;
      }

      public OmsDevopsApiSQL andNameNotIn(List<java.lang.String> values) {
          addCriterion("name", values, ConditionMode.NOT_IN, "name", "java.lang.String", "String");
          return this;
      }
	public OmsDevopsApiSQL andNodeIsNull() {
		isnull("node");
		return this;
	}
	
	public OmsDevopsApiSQL andNodeIsNotNull() {
		notNull("node");
		return this;
	}
	
	public OmsDevopsApiSQL andNodeIsEmpty() {
		empty("node");
		return this;
	}

	public OmsDevopsApiSQL andNodeIsNotEmpty() {
		notEmpty("node");
		return this;
	}
       public OmsDevopsApiSQL andNodeLike(java.lang.String value) {
    	   addCriterion("node", value, ConditionMode.FUZZY, "node", "java.lang.String", "String");
    	   return this;
      }

      public OmsDevopsApiSQL andNodeNotLike(java.lang.String value) {
          addCriterion("node", value, ConditionMode.NOT_FUZZY, "node", "java.lang.String", "String");
          return this;
      }
      public OmsDevopsApiSQL andNodeEqualTo(java.lang.String value) {
          addCriterion("node", value, ConditionMode.EQUAL, "node", "java.lang.String", "String");
          return this;
      }

      public OmsDevopsApiSQL andNodeNotEqualTo(java.lang.String value) {
          addCriterion("node", value, ConditionMode.NOT_EQUAL, "node", "java.lang.String", "String");
          return this;
      }

      public OmsDevopsApiSQL andNodeGreaterThan(java.lang.String value) {
          addCriterion("node", value, ConditionMode.GREATER_THEN, "node", "java.lang.String", "String");
          return this;
      }

      public OmsDevopsApiSQL andNodeGreaterThanOrEqualTo(java.lang.String value) {
          addCriterion("node", value, ConditionMode.GREATER_EQUAL, "node", "java.lang.String", "String");
          return this;
      }

      public OmsDevopsApiSQL andNodeLessThan(java.lang.String value) {
          addCriterion("node", value, ConditionMode.LESS_THEN, "node", "java.lang.String", "String");
          return this;
      }

      public OmsDevopsApiSQL andNodeLessThanOrEqualTo(java.lang.String value) {
          addCriterion("node", value, ConditionMode.LESS_EQUAL, "node", "java.lang.String", "String");
          return this;
      }

      public OmsDevopsApiSQL andNodeBetween(java.lang.String value1, java.lang.String value2) {
    	  addCriterion("node", value1, value2, ConditionMode.BETWEEN, "node", "java.lang.String", "String");
    	  return this;
      }

      public OmsDevopsApiSQL andNodeNotBetween(java.lang.String value1, java.lang.String value2) {
          addCriterion("node", value1, value2, ConditionMode.NOT_BETWEEN, "node", "java.lang.String", "String");
          return this;
      }
        
      public OmsDevopsApiSQL andNodeIn(List<java.lang.String> values) {
          addCriterion("node", values, ConditionMode.IN, "node", "java.lang.String", "String");
          return this;
      }

      public OmsDevopsApiSQL andNodeNotIn(List<java.lang.String> values) {
          addCriterion("node", values, ConditionMode.NOT_IN, "node", "java.lang.String", "String");
          return this;
      }
	public OmsDevopsApiSQL andCodeIsNull() {
		isnull("code");
		return this;
	}
	
	public OmsDevopsApiSQL andCodeIsNotNull() {
		notNull("code");
		return this;
	}
	
	public OmsDevopsApiSQL andCodeIsEmpty() {
		empty("code");
		return this;
	}

	public OmsDevopsApiSQL andCodeIsNotEmpty() {
		notEmpty("code");
		return this;
	}
       public OmsDevopsApiSQL andCodeLike(java.lang.String value) {
    	   addCriterion("code", value, ConditionMode.FUZZY, "code", "java.lang.String", "String");
    	   return this;
      }

      public OmsDevopsApiSQL andCodeNotLike(java.lang.String value) {
          addCriterion("code", value, ConditionMode.NOT_FUZZY, "code", "java.lang.String", "String");
          return this;
      }
      public OmsDevopsApiSQL andCodeEqualTo(java.lang.String value) {
          addCriterion("code", value, ConditionMode.EQUAL, "code", "java.lang.String", "String");
          return this;
      }

      public OmsDevopsApiSQL andCodeNotEqualTo(java.lang.String value) {
          addCriterion("code", value, ConditionMode.NOT_EQUAL, "code", "java.lang.String", "String");
          return this;
      }

      public OmsDevopsApiSQL andCodeGreaterThan(java.lang.String value) {
          addCriterion("code", value, ConditionMode.GREATER_THEN, "code", "java.lang.String", "String");
          return this;
      }

      public OmsDevopsApiSQL andCodeGreaterThanOrEqualTo(java.lang.String value) {
          addCriterion("code", value, ConditionMode.GREATER_EQUAL, "code", "java.lang.String", "String");
          return this;
      }

      public OmsDevopsApiSQL andCodeLessThan(java.lang.String value) {
          addCriterion("code", value, ConditionMode.LESS_THEN, "code", "java.lang.String", "String");
          return this;
      }

      public OmsDevopsApiSQL andCodeLessThanOrEqualTo(java.lang.String value) {
          addCriterion("code", value, ConditionMode.LESS_EQUAL, "code", "java.lang.String", "String");
          return this;
      }

      public OmsDevopsApiSQL andCodeBetween(java.lang.String value1, java.lang.String value2) {
    	  addCriterion("code", value1, value2, ConditionMode.BETWEEN, "code", "java.lang.String", "String");
    	  return this;
      }

      public OmsDevopsApiSQL andCodeNotBetween(java.lang.String value1, java.lang.String value2) {
          addCriterion("code", value1, value2, ConditionMode.NOT_BETWEEN, "code", "java.lang.String", "String");
          return this;
      }
        
      public OmsDevopsApiSQL andCodeIn(List<java.lang.String> values) {
          addCriterion("code", values, ConditionMode.IN, "code", "java.lang.String", "String");
          return this;
      }

      public OmsDevopsApiSQL andCodeNotIn(List<java.lang.String> values) {
          addCriterion("code", values, ConditionMode.NOT_IN, "code", "java.lang.String", "String");
          return this;
      }
	public OmsDevopsApiSQL andUrlIsNull() {
		isnull("url");
		return this;
	}
	
	public OmsDevopsApiSQL andUrlIsNotNull() {
		notNull("url");
		return this;
	}
	
	public OmsDevopsApiSQL andUrlIsEmpty() {
		empty("url");
		return this;
	}

	public OmsDevopsApiSQL andUrlIsNotEmpty() {
		notEmpty("url");
		return this;
	}
       public OmsDevopsApiSQL andUrlLike(java.lang.String value) {
    	   addCriterion("url", value, ConditionMode.FUZZY, "url", "java.lang.String", "String");
    	   return this;
      }

      public OmsDevopsApiSQL andUrlNotLike(java.lang.String value) {
          addCriterion("url", value, ConditionMode.NOT_FUZZY, "url", "java.lang.String", "String");
          return this;
      }
      public OmsDevopsApiSQL andUrlEqualTo(java.lang.String value) {
          addCriterion("url", value, ConditionMode.EQUAL, "url", "java.lang.String", "String");
          return this;
      }

      public OmsDevopsApiSQL andUrlNotEqualTo(java.lang.String value) {
          addCriterion("url", value, ConditionMode.NOT_EQUAL, "url", "java.lang.String", "String");
          return this;
      }

      public OmsDevopsApiSQL andUrlGreaterThan(java.lang.String value) {
          addCriterion("url", value, ConditionMode.GREATER_THEN, "url", "java.lang.String", "String");
          return this;
      }

      public OmsDevopsApiSQL andUrlGreaterThanOrEqualTo(java.lang.String value) {
          addCriterion("url", value, ConditionMode.GREATER_EQUAL, "url", "java.lang.String", "String");
          return this;
      }

      public OmsDevopsApiSQL andUrlLessThan(java.lang.String value) {
          addCriterion("url", value, ConditionMode.LESS_THEN, "url", "java.lang.String", "String");
          return this;
      }

      public OmsDevopsApiSQL andUrlLessThanOrEqualTo(java.lang.String value) {
          addCriterion("url", value, ConditionMode.LESS_EQUAL, "url", "java.lang.String", "String");
          return this;
      }

      public OmsDevopsApiSQL andUrlBetween(java.lang.String value1, java.lang.String value2) {
    	  addCriterion("url", value1, value2, ConditionMode.BETWEEN, "url", "java.lang.String", "String");
    	  return this;
      }

      public OmsDevopsApiSQL andUrlNotBetween(java.lang.String value1, java.lang.String value2) {
          addCriterion("url", value1, value2, ConditionMode.NOT_BETWEEN, "url", "java.lang.String", "String");
          return this;
      }
        
      public OmsDevopsApiSQL andUrlIn(List<java.lang.String> values) {
          addCriterion("url", values, ConditionMode.IN, "url", "java.lang.String", "String");
          return this;
      }

      public OmsDevopsApiSQL andUrlNotIn(List<java.lang.String> values) {
          addCriterion("url", values, ConditionMode.NOT_IN, "url", "java.lang.String", "String");
          return this;
      }
	public OmsDevopsApiSQL andTypeIsNull() {
		isnull("type");
		return this;
	}
	
	public OmsDevopsApiSQL andTypeIsNotNull() {
		notNull("type");
		return this;
	}
	
	public OmsDevopsApiSQL andTypeIsEmpty() {
		empty("type");
		return this;
	}

	public OmsDevopsApiSQL andTypeIsNotEmpty() {
		notEmpty("type");
		return this;
	}
       public OmsDevopsApiSQL andTypeLike(java.lang.String value) {
    	   addCriterion("type", value, ConditionMode.FUZZY, "type", "java.lang.String", "String");
    	   return this;
      }

      public OmsDevopsApiSQL andTypeNotLike(java.lang.String value) {
          addCriterion("type", value, ConditionMode.NOT_FUZZY, "type", "java.lang.String", "String");
          return this;
      }
      public OmsDevopsApiSQL andTypeEqualTo(java.lang.String value) {
          addCriterion("type", value, ConditionMode.EQUAL, "type", "java.lang.String", "String");
          return this;
      }

      public OmsDevopsApiSQL andTypeNotEqualTo(java.lang.String value) {
          addCriterion("type", value, ConditionMode.NOT_EQUAL, "type", "java.lang.String", "String");
          return this;
      }

      public OmsDevopsApiSQL andTypeGreaterThan(java.lang.String value) {
          addCriterion("type", value, ConditionMode.GREATER_THEN, "type", "java.lang.String", "String");
          return this;
      }

      public OmsDevopsApiSQL andTypeGreaterThanOrEqualTo(java.lang.String value) {
          addCriterion("type", value, ConditionMode.GREATER_EQUAL, "type", "java.lang.String", "String");
          return this;
      }

      public OmsDevopsApiSQL andTypeLessThan(java.lang.String value) {
          addCriterion("type", value, ConditionMode.LESS_THEN, "type", "java.lang.String", "String");
          return this;
      }

      public OmsDevopsApiSQL andTypeLessThanOrEqualTo(java.lang.String value) {
          addCriterion("type", value, ConditionMode.LESS_EQUAL, "type", "java.lang.String", "String");
          return this;
      }

      public OmsDevopsApiSQL andTypeBetween(java.lang.String value1, java.lang.String value2) {
    	  addCriterion("type", value1, value2, ConditionMode.BETWEEN, "type", "java.lang.String", "String");
    	  return this;
      }

      public OmsDevopsApiSQL andTypeNotBetween(java.lang.String value1, java.lang.String value2) {
          addCriterion("type", value1, value2, ConditionMode.NOT_BETWEEN, "type", "java.lang.String", "String");
          return this;
      }
        
      public OmsDevopsApiSQL andTypeIn(List<java.lang.String> values) {
          addCriterion("type", values, ConditionMode.IN, "type", "java.lang.String", "String");
          return this;
      }

      public OmsDevopsApiSQL andTypeNotIn(List<java.lang.String> values) {
          addCriterion("type", values, ConditionMode.NOT_IN, "type", "java.lang.String", "String");
          return this;
      }
	public OmsDevopsApiSQL andStatusIsNull() {
		isnull("status");
		return this;
	}
	
	public OmsDevopsApiSQL andStatusIsNotNull() {
		notNull("status");
		return this;
	}
	
	public OmsDevopsApiSQL andStatusIsEmpty() {
		empty("status");
		return this;
	}

	public OmsDevopsApiSQL andStatusIsNotEmpty() {
		notEmpty("status");
		return this;
	}
      public OmsDevopsApiSQL andStatusEqualTo(java.lang.Integer value) {
          addCriterion("status", value, ConditionMode.EQUAL, "status", "java.lang.Integer", "Float");
          return this;
      }

      public OmsDevopsApiSQL andStatusNotEqualTo(java.lang.Integer value) {
          addCriterion("status", value, ConditionMode.NOT_EQUAL, "status", "java.lang.Integer", "Float");
          return this;
      }

      public OmsDevopsApiSQL andStatusGreaterThan(java.lang.Integer value) {
          addCriterion("status", value, ConditionMode.GREATER_THEN, "status", "java.lang.Integer", "Float");
          return this;
      }

      public OmsDevopsApiSQL andStatusGreaterThanOrEqualTo(java.lang.Integer value) {
          addCriterion("status", value, ConditionMode.GREATER_EQUAL, "status", "java.lang.Integer", "Float");
          return this;
      }

      public OmsDevopsApiSQL andStatusLessThan(java.lang.Integer value) {
          addCriterion("status", value, ConditionMode.LESS_THEN, "status", "java.lang.Integer", "Float");
          return this;
      }

      public OmsDevopsApiSQL andStatusLessThanOrEqualTo(java.lang.Integer value) {
          addCriterion("status", value, ConditionMode.LESS_EQUAL, "status", "java.lang.Integer", "Float");
          return this;
      }

      public OmsDevopsApiSQL andStatusBetween(java.lang.Integer value1, java.lang.Integer value2) {
    	  addCriterion("status", value1, value2, ConditionMode.BETWEEN, "status", "java.lang.Integer", "Float");
    	  return this;
      }

      public OmsDevopsApiSQL andStatusNotBetween(java.lang.Integer value1, java.lang.Integer value2) {
          addCriterion("status", value1, value2, ConditionMode.NOT_BETWEEN, "status", "java.lang.Integer", "Float");
          return this;
      }
        
      public OmsDevopsApiSQL andStatusIn(List<java.lang.Integer> values) {
          addCriterion("status", values, ConditionMode.IN, "status", "java.lang.Integer", "Float");
          return this;
      }

      public OmsDevopsApiSQL andStatusNotIn(List<java.lang.Integer> values) {
          addCriterion("status", values, ConditionMode.NOT_IN, "status", "java.lang.Integer", "Float");
          return this;
      }
	public OmsDevopsApiSQL andCreatedIsNull() {
		isnull("created");
		return this;
	}
	
	public OmsDevopsApiSQL andCreatedIsNotNull() {
		notNull("created");
		return this;
	}
	
	public OmsDevopsApiSQL andCreatedIsEmpty() {
		empty("created");
		return this;
	}

	public OmsDevopsApiSQL andCreatedIsNotEmpty() {
		notEmpty("created");
		return this;
	}
      public OmsDevopsApiSQL andCreatedEqualTo(java.lang.Long value) {
          addCriterion("created", value, ConditionMode.EQUAL, "created", "java.lang.Long", "Float");
          return this;
      }

      public OmsDevopsApiSQL andCreatedNotEqualTo(java.lang.Long value) {
          addCriterion("created", value, ConditionMode.NOT_EQUAL, "created", "java.lang.Long", "Float");
          return this;
      }

      public OmsDevopsApiSQL andCreatedGreaterThan(java.lang.Long value) {
          addCriterion("created", value, ConditionMode.GREATER_THEN, "created", "java.lang.Long", "Float");
          return this;
      }

      public OmsDevopsApiSQL andCreatedGreaterThanOrEqualTo(java.lang.Long value) {
          addCriterion("created", value, ConditionMode.GREATER_EQUAL, "created", "java.lang.Long", "Float");
          return this;
      }

      public OmsDevopsApiSQL andCreatedLessThan(java.lang.Long value) {
          addCriterion("created", value, ConditionMode.LESS_THEN, "created", "java.lang.Long", "Float");
          return this;
      }

      public OmsDevopsApiSQL andCreatedLessThanOrEqualTo(java.lang.Long value) {
          addCriterion("created", value, ConditionMode.LESS_EQUAL, "created", "java.lang.Long", "Float");
          return this;
      }

      public OmsDevopsApiSQL andCreatedBetween(java.lang.Long value1, java.lang.Long value2) {
    	  addCriterion("created", value1, value2, ConditionMode.BETWEEN, "created", "java.lang.Long", "Float");
    	  return this;
      }

      public OmsDevopsApiSQL andCreatedNotBetween(java.lang.Long value1, java.lang.Long value2) {
          addCriterion("created", value1, value2, ConditionMode.NOT_BETWEEN, "created", "java.lang.Long", "Float");
          return this;
      }
        
      public OmsDevopsApiSQL andCreatedIn(List<java.lang.Long> values) {
          addCriterion("created", values, ConditionMode.IN, "created", "java.lang.Long", "Float");
          return this;
      }

      public OmsDevopsApiSQL andCreatedNotIn(List<java.lang.Long> values) {
          addCriterion("created", values, ConditionMode.NOT_IN, "created", "java.lang.Long", "Float");
          return this;
      }
}