/**
 * Copyright (c) 2015-2017, Henry Yang 杨勇 (gismail@foxmail.com).
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.lambkit.module.oms.row;

import com.lambkit.db.RowModel;

/**
 * @author yangyong 
 * @website: www.lambkit.com
 * @email: gismail@foxmail.com
 * @date ${date}
 * @version 1.0
 * @since 1.0
 */

public class OmsDevopsServer extends RowModel<OmsDevopsServer> {
	public OmsDevopsServer() {
		setTableName("oms_devops_server");
		setPrimaryKey("devops_server_id");
	}
	public java.lang.Long getDevopsServerId() {
		return this.get("devops_server_id");
	}
	public void setDevopsServerId(java.lang.Long devopsServerId) {
		this.set("devops_server_id", devopsServerId);
	}
	public java.lang.String getName() {
		return this.get("name");
	}
	public void setName(java.lang.String name) {
		this.set("name", name);
	}
	public java.lang.String getType() {
		return this.get("type");
	}
	public void setType(java.lang.String type) {
		this.set("type", type);
	}
	public java.lang.Long getCreated() {
		return this.get("created");
	}
	public void setCreated(java.lang.Long created) {
		this.set("created", created);
	}
	public java.lang.String getRecord() {
		return this.get("record");
	}
	public void setRecord(java.lang.String record) {
		this.set("record", record);
	}
	public java.lang.Float getCpu() {
		return this.get("cpu");
	}
	public void setCpu(java.lang.Float cpu) {
		this.set("cpu", cpu);
	}
	public java.lang.Float getMemory() {
		return this.get("memory");
	}
	public void setMemory(java.lang.Float memory) {
		this.set("memory", memory);
	}
	public java.lang.Float getDisk() {
		return this.get("disk");
	}
	public void setDisk(java.lang.Float disk) {
		this.set("disk", disk);
	}
}
