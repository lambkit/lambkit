package com.lambkit.test;

import cn.hutool.core.util.IdUtil;
import cn.hutool.crypto.SecureUtil;

public class PasswordKit {

    public static void main(String[] args) {
        PasswordKit kit = new PasswordKit();
        String password = "123456";
        String salt = IdUtil.simpleUUID();
        String encryptType = "md5";
        String result = kit.encrypt(password, salt, encryptType);
        System.out.println(encryptType + " password: " + password);
        System.out.println(encryptType + " salt: " + salt);
        System.out.println(encryptType + " result: " + result);
    }

    public String encrypt(String password, String salt, String type) {
        String passwordAndsalt = password + salt;
        if("sha".equals(type)) {
            return SecureUtil.sha256(passwordAndsalt);
        } else if("md5".equals(type)) {
            return SecureUtil.md5(passwordAndsalt);
        } else if("hmacSha".equals(type)) {
            return SecureUtil.hmacSha256(salt).digestHex(password);
        } else if("hmacmd5".equals(type)) {
            return SecureUtil.hmacMd5(salt).digestHex(password);
        }
        return SecureUtil.md5(passwordAndsalt);
    }
}
