package com.lambkit.test;

import cn.hutool.db.dialect.DriverNamePool;
import com.alibaba.druid.pool.DruidDataSource;
import com.lambkit.db.hutool.HutoolDb;
import com.lambkit.generator.Generator;
import com.lambkit.generator.GeneratorConfig;
import com.lambkit.generator.GeneratorType;

import java.util.HashMap;
import java.util.Map;

/**
 * @author yangyong(孤竹行)
 */
public class GeneratorKit {
    public static void main(String[] args) {
        DruidDataSource ds2 = new DruidDataSource();
        ds2.setUrl("jdbc:postgresql://localhost:5432/lambkit2");
        ds2.setUsername("postgres");
        ds2.setPassword("spFAyCBalefzzoBK");
        HutoolDb hutoolDb = new HutoolDb(ds2, DriverNamePool.DRIVER_POSTGRESQL);
        //
        GeneratorConfig config = new GeneratorConfig();
        // 模板地址
        String templatePath = "/template/jfinal";
        // 模板所在目录
        String rootPath = System.getProperty("user.dir");
        // 生成java代码的存放地址
        config.setOutRootDir(rootPath + "/lambkit-module/lambkit-upms//src/main/java");
        // 生成前端文件文件夹
        config.setWebpages("app");
        // 表格配置方式
        //config.setMgrdb("dmsMgrdb");
        // 选择一种模板语言
        config.setEngine(GeneratorConfig.TYPE_JFINAL);
        // 选择一种处理引擎
        config.setType(GeneratorType.DB);
        // 包地址
        config.setBasepackage("com.lambkit.module.upms");
        // 配置
        Map<String, Object> options = new HashMap<String, Object>();
        // 需要去掉的前缀
        options.put("tableRemovePrefixes", "lk_");

        options.put("includedTables", "upms_app," +
                "upms_app_permission," +
                "upms_app_role," +
                "upms_auth_code," +
                "upms_credit," +
                "upms_favorites," +
                //"upms_log," +
                "upms_oauth," +
                "upms_organization," +
                "upms_permission," +
                "upms_role," +
                "upms_role_permission," +
                "upms_role_rule," +
                "upms_rule," +
                "upms_system," +
                "upms_tag," +
                //"upms_user," +
                "upms_user_account," +
                "upms_user_account_role," +
                "upms_user_credit," +
                "upms_user_details," +
                "upms_user_login_channel," +
                "upms_user_oauth," +
                "upms_user_organization," +
                "upms_user_permission," +
                "upms_user_role");
        options.put("moduleName", "Upms");
        options.put("propName", "upms");

        // 不进行生成的模板
        options.put("excludedTemplate", "#(moduleName)Config.java, #(classname).java,SQL.java,Module.java");

        // 代码生成
        Generator.execute(hutoolDb, rootPath, templatePath, options, config);
    }
}
