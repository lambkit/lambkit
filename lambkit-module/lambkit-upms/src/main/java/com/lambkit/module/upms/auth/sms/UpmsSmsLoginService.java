package com.lambkit.module.upms.auth.sms;

import com.alibaba.fastjson.JSONArray;
import com.lambkit.auth.sms.AbstractSmsLoginService;
import com.lambkit.core.Attr;
import com.lambkit.core.Lambkit;
import com.lambkit.module.upms.UpmsConfig;
import com.lambkit.module.upms.UpmsConsts;
import com.lambkit.sms.huawei.HuaweiSms;
import com.lambkit.sms.nim.NimConfig;
import com.lambkit.sms.nim.NimMessageKit;

/**
 * @author yangyong(孤竹行)
 */
public class UpmsSmsLoginService extends AbstractSmsLoginService {
    @Override
    public int sendLoginVerifyCode(String phoneTo, String code) {
        UpmsConfig upmsConfig = Lambkit.config(UpmsConfig.class);
        String sms = upmsConfig.getSms();
        if("huawei".equalsIgnoreCase(sms)) {
            HuaweiSms huaweiSms = Lambkit.get(HuaweiSms.class);
            JSONArray jsonArray = new JSONArray();
            jsonArray.add(code);
            try {
                huaweiSms.send(phoneTo, jsonArray.toJSONString());
                return 200;
            } catch (Exception e) {
                throw new RuntimeException(e);
            }
        } else if ("nim".equalsIgnoreCase(sms)) {
            NimConfig config = Lambkit.config(NimConfig.class);
            Attr result = NimMessageKit.sendCode(config, phoneTo, code);
            if (result.containsKey("code")) {
                return result.getInt(code);
            }
        }
        return 0;
    }
}
