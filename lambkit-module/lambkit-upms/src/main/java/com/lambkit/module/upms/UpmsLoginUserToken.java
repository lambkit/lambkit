package com.lambkit.module.upms;

import com.lambkit.module.upms.row.UpmsUser;

import java.io.Serializable;

/**
 * @author yangyong(孤竹行)
 */
public class UpmsLoginUserToken implements Serializable {
    private String username;

    private String password;

    //0-account-password, 1-phone-verifyCode, 99-unpassword无密认证
    private int loginType = 0;
    private String passwordCodeType = null;

    private UpmsUser upmsUser;

    public UpmsLoginUserToken() {

    }

    public UpmsLoginUserToken(String username, String password) {
        this.username = username;
        this.password = password;
    }

    public UpmsLoginUserToken(String username, String password, int loginType){
        this(username, password);
        this.loginType = loginType;
    }

    public UpmsLoginUserToken(String username, String password, int loginType, String passwordCodeType){
        this(username, password, loginType);
        this.passwordCodeType = passwordCodeType;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public int getLoginType() {
        return loginType;
    }

    public void setLoginType(int loginType) {
        this.loginType = loginType;
    }

    public String getPasswordCodeType() {
        return passwordCodeType;
    }

    public void setPasswordCodeType(String passwordCodeType) {
        this.passwordCodeType = passwordCodeType;
    }

    public UpmsUser getUpmsUser() {
        return upmsUser;
    }

    public void setUpmsUser(UpmsUser upmsUser) {
        this.upmsUser = upmsUser;
    }
}
