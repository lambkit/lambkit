/**
 * Copyright (c) 2015-2017, Henry Yang 杨勇 (gismail@foxmail.com).
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.lambkit.module.upms.sql;

import com.lambkit.db.sql.ConditionMode;
import com.lambkit.db.sql.Column;
import com.lambkit.db.sql.Columns;
import com.lambkit.db.sql.Example;

import java.io.Serializable;
import java.util.List;

/**
 * @author yangyong 
 * @website: www.lambkit.com
 * @email: gismail@foxmail.com
 * @date ${date}
 * @version 1.0
 * @since 1.0
 */

public class UpmsAppSQL extends Columns implements Serializable {
	private static final long serialVersionUID = 1L;
	
	public static UpmsAppSQL of() {
		return new UpmsAppSQL();
	}
	
	public static UpmsAppSQL by(Column column) {
		UpmsAppSQL that = new UpmsAppSQL();
		that.add(column);
        return that;
    }

    public static UpmsAppSQL by(String name, Object value) {
        return (UpmsAppSQL) create().eq(name, value);
    }
    
    public Example example() {
    	return Example.create("upms_app", this);
    }
    
    /**
     * equals
     *
     * @param name
     * @param value
     * @return
     */
    public UpmsAppSQL eq(String name, Object value) {
    	super.eq(name, value);
        return this;
    }

    /**
     * not equals !=
     *
     * @param name
     * @param value
     * @return
     */
    public UpmsAppSQL ne(String name, Object value) {
    	super.ne(name, value);
        return this;
    }


    /**
     * like
     *
     * @param name
     * @param value
     * @return
     */

    public UpmsAppSQL like(String name, Object value) {
    	super.like(name, value);
        return this;
    }
    
    public UpmsAppSQL notLike(String name, Object value) {
    	super.notLike(name, value);
        return this;
    }

    /**
     * 大于 great than
     *
     * @param name
     * @param value
     * @return
     */
    public UpmsAppSQL gt(String name, Object value) {
    	super.gt(name, value);
        return this;
    }

    /**
     * 大于等于 great or equal
     *
     * @param name
     * @param value
     * @return
     */
    public UpmsAppSQL ge(String name, Object value) {
    	super.ge(name, value);
        return this;
    }

    /**
     * 小于 less than
     *
     * @param name
     * @param value
     * @return
     */
    public UpmsAppSQL lt(String name, Object value) {
    	super.lt(name, value);
        return this;
    }

    /**
     * 小于等于 less or equal
     *
     * @param name
     * @param value
     * @return
     */
    public UpmsAppSQL le(String name, Object value) {
    	super.le(name, value);
        return this;
    }
    
    public UpmsAppSQL isnull(String name) {
    	super.isnull(name);
        return this;
    } 

    public UpmsAppSQL notNull(String name) {
    	super.notNull(name);
        return this;
    } 
    
    public UpmsAppSQL empty(String name) {
    	super.empty(name);
        return this;
    } 
    
    public UpmsAppSQL notEmpty(String name) {
    	super.notEmpty(name);
        return this;
    } 
    
    public UpmsAppSQL add(Column column) {
    	super.add(column);
    	return this;
    }
    
    /**************************/
	
	public void addCriterion(String name, Object value, ConditionMode logic, String property, String typeHandler, String valueType) {
		 if (value == null) {
			 throw new RuntimeException("Value for " + property + " cannot be null");
		 }
		 add(Column.create(name, value, logic, typeHandler, valueType));
	}
   
	public void addCriterion(String name, Object value1, Object value2, ConditionMode logic, String property, String typeHandler, String valueType) {
		 if (value1 == null || value2 == null) {
			 throw new RuntimeException("Between values for " + property + " cannot be null");
		 }
		 add(Column.create(name, value1, value2, logic, typeHandler, valueType));
	}
		 
	public UpmsAppSQL andIdIsNull() {
		isnull("id");
		return this;
	}
	
	public UpmsAppSQL andIdIsNotNull() {
		notNull("id");
		return this;
	}
	
	public UpmsAppSQL andIdIsEmpty() {
		empty("id");
		return this;
	}

	public UpmsAppSQL andIdIsNotEmpty() {
		notEmpty("id");
		return this;
	}
      public UpmsAppSQL andIdEqualTo(java.lang.Integer value) {
          addCriterion("id", value, ConditionMode.EQUAL, "id", "java.lang.Integer", "Float");
          return this;
      }

      public UpmsAppSQL andIdNotEqualTo(java.lang.Integer value) {
          addCriterion("id", value, ConditionMode.NOT_EQUAL, "id", "java.lang.Integer", "Float");
          return this;
      }

      public UpmsAppSQL andIdGreaterThan(java.lang.Integer value) {
          addCriterion("id", value, ConditionMode.GREATER_THEN, "id", "java.lang.Integer", "Float");
          return this;
      }

      public UpmsAppSQL andIdGreaterThanOrEqualTo(java.lang.Integer value) {
          addCriterion("id", value, ConditionMode.GREATER_EQUAL, "id", "java.lang.Integer", "Float");
          return this;
      }

      public UpmsAppSQL andIdLessThan(java.lang.Integer value) {
          addCriterion("id", value, ConditionMode.LESS_THEN, "id", "java.lang.Integer", "Float");
          return this;
      }

      public UpmsAppSQL andIdLessThanOrEqualTo(java.lang.Integer value) {
          addCriterion("id", value, ConditionMode.LESS_EQUAL, "id", "java.lang.Integer", "Float");
          return this;
      }

      public UpmsAppSQL andIdBetween(java.lang.Integer value1, java.lang.Integer value2) {
    	  addCriterion("id", value1, value2, ConditionMode.BETWEEN, "id", "java.lang.Integer", "Float");
    	  return this;
      }

      public UpmsAppSQL andIdNotBetween(java.lang.Integer value1, java.lang.Integer value2) {
          addCriterion("id", value1, value2, ConditionMode.NOT_BETWEEN, "id", "java.lang.Integer", "Float");
          return this;
      }
        
      public UpmsAppSQL andIdIn(List<java.lang.Integer> values) {
          addCriterion("id", values, ConditionMode.IN, "id", "java.lang.Integer", "Float");
          return this;
      }

      public UpmsAppSQL andIdNotIn(List<java.lang.Integer> values) {
          addCriterion("id", values, ConditionMode.NOT_IN, "id", "java.lang.Integer", "Float");
          return this;
      }
	public UpmsAppSQL andAppNameIsNull() {
		isnull("app_name");
		return this;
	}
	
	public UpmsAppSQL andAppNameIsNotNull() {
		notNull("app_name");
		return this;
	}
	
	public UpmsAppSQL andAppNameIsEmpty() {
		empty("app_name");
		return this;
	}

	public UpmsAppSQL andAppNameIsNotEmpty() {
		notEmpty("app_name");
		return this;
	}
       public UpmsAppSQL andAppNameLike(java.lang.String value) {
    	   addCriterion("app_name", value, ConditionMode.FUZZY, "appName", "java.lang.String", "Float");
    	   return this;
      }

      public UpmsAppSQL andAppNameNotLike(java.lang.String value) {
          addCriterion("app_name", value, ConditionMode.NOT_FUZZY, "appName", "java.lang.String", "Float");
          return this;
      }
      public UpmsAppSQL andAppNameEqualTo(java.lang.String value) {
          addCriterion("app_name", value, ConditionMode.EQUAL, "appName", "java.lang.String", "String");
          return this;
      }

      public UpmsAppSQL andAppNameNotEqualTo(java.lang.String value) {
          addCriterion("app_name", value, ConditionMode.NOT_EQUAL, "appName", "java.lang.String", "String");
          return this;
      }

      public UpmsAppSQL andAppNameGreaterThan(java.lang.String value) {
          addCriterion("app_name", value, ConditionMode.GREATER_THEN, "appName", "java.lang.String", "String");
          return this;
      }

      public UpmsAppSQL andAppNameGreaterThanOrEqualTo(java.lang.String value) {
          addCriterion("app_name", value, ConditionMode.GREATER_EQUAL, "appName", "java.lang.String", "String");
          return this;
      }

      public UpmsAppSQL andAppNameLessThan(java.lang.String value) {
          addCriterion("app_name", value, ConditionMode.LESS_THEN, "appName", "java.lang.String", "String");
          return this;
      }

      public UpmsAppSQL andAppNameLessThanOrEqualTo(java.lang.String value) {
          addCriterion("app_name", value, ConditionMode.LESS_EQUAL, "appName", "java.lang.String", "String");
          return this;
      }

      public UpmsAppSQL andAppNameBetween(java.lang.String value1, java.lang.String value2) {
    	  addCriterion("app_name", value1, value2, ConditionMode.BETWEEN, "appName", "java.lang.String", "String");
    	  return this;
      }

      public UpmsAppSQL andAppNameNotBetween(java.lang.String value1, java.lang.String value2) {
          addCriterion("app_name", value1, value2, ConditionMode.NOT_BETWEEN, "appName", "java.lang.String", "String");
          return this;
      }
        
      public UpmsAppSQL andAppNameIn(List<java.lang.String> values) {
          addCriterion("app_name", values, ConditionMode.IN, "appName", "java.lang.String", "String");
          return this;
      }

      public UpmsAppSQL andAppNameNotIn(List<java.lang.String> values) {
          addCriterion("app_name", values, ConditionMode.NOT_IN, "appName", "java.lang.String", "String");
          return this;
      }
	public UpmsAppSQL andAppKeyIsNull() {
		isnull("app_key");
		return this;
	}
	
	public UpmsAppSQL andAppKeyIsNotNull() {
		notNull("app_key");
		return this;
	}
	
	public UpmsAppSQL andAppKeyIsEmpty() {
		empty("app_key");
		return this;
	}

	public UpmsAppSQL andAppKeyIsNotEmpty() {
		notEmpty("app_key");
		return this;
	}
       public UpmsAppSQL andAppKeyLike(java.lang.String value) {
    	   addCriterion("app_key", value, ConditionMode.FUZZY, "appKey", "java.lang.String", "String");
    	   return this;
      }

      public UpmsAppSQL andAppKeyNotLike(java.lang.String value) {
          addCriterion("app_key", value, ConditionMode.NOT_FUZZY, "appKey", "java.lang.String", "String");
          return this;
      }
      public UpmsAppSQL andAppKeyEqualTo(java.lang.String value) {
          addCriterion("app_key", value, ConditionMode.EQUAL, "appKey", "java.lang.String", "String");
          return this;
      }

      public UpmsAppSQL andAppKeyNotEqualTo(java.lang.String value) {
          addCriterion("app_key", value, ConditionMode.NOT_EQUAL, "appKey", "java.lang.String", "String");
          return this;
      }

      public UpmsAppSQL andAppKeyGreaterThan(java.lang.String value) {
          addCriterion("app_key", value, ConditionMode.GREATER_THEN, "appKey", "java.lang.String", "String");
          return this;
      }

      public UpmsAppSQL andAppKeyGreaterThanOrEqualTo(java.lang.String value) {
          addCriterion("app_key", value, ConditionMode.GREATER_EQUAL, "appKey", "java.lang.String", "String");
          return this;
      }

      public UpmsAppSQL andAppKeyLessThan(java.lang.String value) {
          addCriterion("app_key", value, ConditionMode.LESS_THEN, "appKey", "java.lang.String", "String");
          return this;
      }

      public UpmsAppSQL andAppKeyLessThanOrEqualTo(java.lang.String value) {
          addCriterion("app_key", value, ConditionMode.LESS_EQUAL, "appKey", "java.lang.String", "String");
          return this;
      }

      public UpmsAppSQL andAppKeyBetween(java.lang.String value1, java.lang.String value2) {
    	  addCriterion("app_key", value1, value2, ConditionMode.BETWEEN, "appKey", "java.lang.String", "String");
    	  return this;
      }

      public UpmsAppSQL andAppKeyNotBetween(java.lang.String value1, java.lang.String value2) {
          addCriterion("app_key", value1, value2, ConditionMode.NOT_BETWEEN, "appKey", "java.lang.String", "String");
          return this;
      }
        
      public UpmsAppSQL andAppKeyIn(List<java.lang.String> values) {
          addCriterion("app_key", values, ConditionMode.IN, "appKey", "java.lang.String", "String");
          return this;
      }

      public UpmsAppSQL andAppKeyNotIn(List<java.lang.String> values) {
          addCriterion("app_key", values, ConditionMode.NOT_IN, "appKey", "java.lang.String", "String");
          return this;
      }
	public UpmsAppSQL andAppSecretIsNull() {
		isnull("app_secret");
		return this;
	}
	
	public UpmsAppSQL andAppSecretIsNotNull() {
		notNull("app_secret");
		return this;
	}
	
	public UpmsAppSQL andAppSecretIsEmpty() {
		empty("app_secret");
		return this;
	}

	public UpmsAppSQL andAppSecretIsNotEmpty() {
		notEmpty("app_secret");
		return this;
	}
       public UpmsAppSQL andAppSecretLike(java.lang.String value) {
    	   addCriterion("app_secret", value, ConditionMode.FUZZY, "appSecret", "java.lang.String", "String");
    	   return this;
      }

      public UpmsAppSQL andAppSecretNotLike(java.lang.String value) {
          addCriterion("app_secret", value, ConditionMode.NOT_FUZZY, "appSecret", "java.lang.String", "String");
          return this;
      }
      public UpmsAppSQL andAppSecretEqualTo(java.lang.String value) {
          addCriterion("app_secret", value, ConditionMode.EQUAL, "appSecret", "java.lang.String", "String");
          return this;
      }

      public UpmsAppSQL andAppSecretNotEqualTo(java.lang.String value) {
          addCriterion("app_secret", value, ConditionMode.NOT_EQUAL, "appSecret", "java.lang.String", "String");
          return this;
      }

      public UpmsAppSQL andAppSecretGreaterThan(java.lang.String value) {
          addCriterion("app_secret", value, ConditionMode.GREATER_THEN, "appSecret", "java.lang.String", "String");
          return this;
      }

      public UpmsAppSQL andAppSecretGreaterThanOrEqualTo(java.lang.String value) {
          addCriterion("app_secret", value, ConditionMode.GREATER_EQUAL, "appSecret", "java.lang.String", "String");
          return this;
      }

      public UpmsAppSQL andAppSecretLessThan(java.lang.String value) {
          addCriterion("app_secret", value, ConditionMode.LESS_THEN, "appSecret", "java.lang.String", "String");
          return this;
      }

      public UpmsAppSQL andAppSecretLessThanOrEqualTo(java.lang.String value) {
          addCriterion("app_secret", value, ConditionMode.LESS_EQUAL, "appSecret", "java.lang.String", "String");
          return this;
      }

      public UpmsAppSQL andAppSecretBetween(java.lang.String value1, java.lang.String value2) {
    	  addCriterion("app_secret", value1, value2, ConditionMode.BETWEEN, "appSecret", "java.lang.String", "String");
    	  return this;
      }

      public UpmsAppSQL andAppSecretNotBetween(java.lang.String value1, java.lang.String value2) {
          addCriterion("app_secret", value1, value2, ConditionMode.NOT_BETWEEN, "appSecret", "java.lang.String", "String");
          return this;
      }
        
      public UpmsAppSQL andAppSecretIn(List<java.lang.String> values) {
          addCriterion("app_secret", values, ConditionMode.IN, "appSecret", "java.lang.String", "String");
          return this;
      }

      public UpmsAppSQL andAppSecretNotIn(List<java.lang.String> values) {
          addCriterion("app_secret", values, ConditionMode.NOT_IN, "appSecret", "java.lang.String", "String");
          return this;
      }
	public UpmsAppSQL andAccessTokenIsNull() {
		isnull("access_token");
		return this;
	}
	
	public UpmsAppSQL andAccessTokenIsNotNull() {
		notNull("access_token");
		return this;
	}
	
	public UpmsAppSQL andAccessTokenIsEmpty() {
		empty("access_token");
		return this;
	}

	public UpmsAppSQL andAccessTokenIsNotEmpty() {
		notEmpty("access_token");
		return this;
	}
       public UpmsAppSQL andAccessTokenLike(java.lang.String value) {
    	   addCriterion("access_token", value, ConditionMode.FUZZY, "accessToken", "java.lang.String", "String");
    	   return this;
      }

      public UpmsAppSQL andAccessTokenNotLike(java.lang.String value) {
          addCriterion("access_token", value, ConditionMode.NOT_FUZZY, "accessToken", "java.lang.String", "String");
          return this;
      }
      public UpmsAppSQL andAccessTokenEqualTo(java.lang.String value) {
          addCriterion("access_token", value, ConditionMode.EQUAL, "accessToken", "java.lang.String", "String");
          return this;
      }

      public UpmsAppSQL andAccessTokenNotEqualTo(java.lang.String value) {
          addCriterion("access_token", value, ConditionMode.NOT_EQUAL, "accessToken", "java.lang.String", "String");
          return this;
      }

      public UpmsAppSQL andAccessTokenGreaterThan(java.lang.String value) {
          addCriterion("access_token", value, ConditionMode.GREATER_THEN, "accessToken", "java.lang.String", "String");
          return this;
      }

      public UpmsAppSQL andAccessTokenGreaterThanOrEqualTo(java.lang.String value) {
          addCriterion("access_token", value, ConditionMode.GREATER_EQUAL, "accessToken", "java.lang.String", "String");
          return this;
      }

      public UpmsAppSQL andAccessTokenLessThan(java.lang.String value) {
          addCriterion("access_token", value, ConditionMode.LESS_THEN, "accessToken", "java.lang.String", "String");
          return this;
      }

      public UpmsAppSQL andAccessTokenLessThanOrEqualTo(java.lang.String value) {
          addCriterion("access_token", value, ConditionMode.LESS_EQUAL, "accessToken", "java.lang.String", "String");
          return this;
      }

      public UpmsAppSQL andAccessTokenBetween(java.lang.String value1, java.lang.String value2) {
    	  addCriterion("access_token", value1, value2, ConditionMode.BETWEEN, "accessToken", "java.lang.String", "String");
    	  return this;
      }

      public UpmsAppSQL andAccessTokenNotBetween(java.lang.String value1, java.lang.String value2) {
          addCriterion("access_token", value1, value2, ConditionMode.NOT_BETWEEN, "accessToken", "java.lang.String", "String");
          return this;
      }
        
      public UpmsAppSQL andAccessTokenIn(List<java.lang.String> values) {
          addCriterion("access_token", values, ConditionMode.IN, "accessToken", "java.lang.String", "String");
          return this;
      }

      public UpmsAppSQL andAccessTokenNotIn(List<java.lang.String> values) {
          addCriterion("access_token", values, ConditionMode.NOT_IN, "accessToken", "java.lang.String", "String");
          return this;
      }
	public UpmsAppSQL andStatusIsNull() {
		isnull("status");
		return this;
	}
	
	public UpmsAppSQL andStatusIsNotNull() {
		notNull("status");
		return this;
	}
	
	public UpmsAppSQL andStatusIsEmpty() {
		empty("status");
		return this;
	}

	public UpmsAppSQL andStatusIsNotEmpty() {
		notEmpty("status");
		return this;
	}
      public UpmsAppSQL andStatusEqualTo(java.lang.Integer value) {
          addCriterion("status", value, ConditionMode.EQUAL, "status", "java.lang.Integer", "Float");
          return this;
      }

      public UpmsAppSQL andStatusNotEqualTo(java.lang.Integer value) {
          addCriterion("status", value, ConditionMode.NOT_EQUAL, "status", "java.lang.Integer", "Float");
          return this;
      }

      public UpmsAppSQL andStatusGreaterThan(java.lang.Integer value) {
          addCriterion("status", value, ConditionMode.GREATER_THEN, "status", "java.lang.Integer", "Float");
          return this;
      }

      public UpmsAppSQL andStatusGreaterThanOrEqualTo(java.lang.Integer value) {
          addCriterion("status", value, ConditionMode.GREATER_EQUAL, "status", "java.lang.Integer", "Float");
          return this;
      }

      public UpmsAppSQL andStatusLessThan(java.lang.Integer value) {
          addCriterion("status", value, ConditionMode.LESS_THEN, "status", "java.lang.Integer", "Float");
          return this;
      }

      public UpmsAppSQL andStatusLessThanOrEqualTo(java.lang.Integer value) {
          addCriterion("status", value, ConditionMode.LESS_EQUAL, "status", "java.lang.Integer", "Float");
          return this;
      }

      public UpmsAppSQL andStatusBetween(java.lang.Integer value1, java.lang.Integer value2) {
    	  addCriterion("status", value1, value2, ConditionMode.BETWEEN, "status", "java.lang.Integer", "Float");
    	  return this;
      }

      public UpmsAppSQL andStatusNotBetween(java.lang.Integer value1, java.lang.Integer value2) {
          addCriterion("status", value1, value2, ConditionMode.NOT_BETWEEN, "status", "java.lang.Integer", "Float");
          return this;
      }
        
      public UpmsAppSQL andStatusIn(List<java.lang.Integer> values) {
          addCriterion("status", values, ConditionMode.IN, "status", "java.lang.Integer", "Float");
          return this;
      }

      public UpmsAppSQL andStatusNotIn(List<java.lang.Integer> values) {
          addCriterion("status", values, ConditionMode.NOT_IN, "status", "java.lang.Integer", "Float");
          return this;
      }
	public UpmsAppSQL andCreatedIsNull() {
		isnull("created");
		return this;
	}
	
	public UpmsAppSQL andCreatedIsNotNull() {
		notNull("created");
		return this;
	}
	
	public UpmsAppSQL andCreatedIsEmpty() {
		empty("created");
		return this;
	}

	public UpmsAppSQL andCreatedIsNotEmpty() {
		notEmpty("created");
		return this;
	}
      public UpmsAppSQL andCreatedEqualTo(java.util.Date value) {
          addCriterion("created", value, ConditionMode.EQUAL, "created", "java.util.Date", "String");
          return this;
      }

      public UpmsAppSQL andCreatedNotEqualTo(java.util.Date value) {
          addCriterion("created", value, ConditionMode.NOT_EQUAL, "created", "java.util.Date", "String");
          return this;
      }

      public UpmsAppSQL andCreatedGreaterThan(java.util.Date value) {
          addCriterion("created", value, ConditionMode.GREATER_THEN, "created", "java.util.Date", "String");
          return this;
      }

      public UpmsAppSQL andCreatedGreaterThanOrEqualTo(java.util.Date value) {
          addCriterion("created", value, ConditionMode.GREATER_EQUAL, "created", "java.util.Date", "String");
          return this;
      }

      public UpmsAppSQL andCreatedLessThan(java.util.Date value) {
          addCriterion("created", value, ConditionMode.LESS_THEN, "created", "java.util.Date", "String");
          return this;
      }

      public UpmsAppSQL andCreatedLessThanOrEqualTo(java.util.Date value) {
          addCriterion("created", value, ConditionMode.LESS_EQUAL, "created", "java.util.Date", "String");
          return this;
      }

      public UpmsAppSQL andCreatedBetween(java.util.Date value1, java.util.Date value2) {
    	  addCriterion("created", value1, value2, ConditionMode.BETWEEN, "created", "java.util.Date", "String");
    	  return this;
      }

      public UpmsAppSQL andCreatedNotBetween(java.util.Date value1, java.util.Date value2) {
          addCriterion("created", value1, value2, ConditionMode.NOT_BETWEEN, "created", "java.util.Date", "String");
          return this;
      }
        
      public UpmsAppSQL andCreatedIn(List<java.util.Date> values) {
          addCriterion("created", values, ConditionMode.IN, "created", "java.util.Date", "String");
          return this;
      }

      public UpmsAppSQL andCreatedNotIn(List<java.util.Date> values) {
          addCriterion("created", values, ConditionMode.NOT_IN, "created", "java.util.Date", "String");
          return this;
      }
	public UpmsAppSQL andAppIdIsNull() {
		isnull("app_id");
		return this;
	}
	
	public UpmsAppSQL andAppIdIsNotNull() {
		notNull("app_id");
		return this;
	}
	
	public UpmsAppSQL andAppIdIsEmpty() {
		empty("app_id");
		return this;
	}

	public UpmsAppSQL andAppIdIsNotEmpty() {
		notEmpty("app_id");
		return this;
	}
       public UpmsAppSQL andAppIdLike(java.lang.String value) {
    	   addCriterion("app_id", value, ConditionMode.FUZZY, "appId", "java.lang.String", "String");
    	   return this;
      }

      public UpmsAppSQL andAppIdNotLike(java.lang.String value) {
          addCriterion("app_id", value, ConditionMode.NOT_FUZZY, "appId", "java.lang.String", "String");
          return this;
      }
      public UpmsAppSQL andAppIdEqualTo(java.lang.String value) {
          addCriterion("app_id", value, ConditionMode.EQUAL, "appId", "java.lang.String", "String");
          return this;
      }

      public UpmsAppSQL andAppIdNotEqualTo(java.lang.String value) {
          addCriterion("app_id", value, ConditionMode.NOT_EQUAL, "appId", "java.lang.String", "String");
          return this;
      }

      public UpmsAppSQL andAppIdGreaterThan(java.lang.String value) {
          addCriterion("app_id", value, ConditionMode.GREATER_THEN, "appId", "java.lang.String", "String");
          return this;
      }

      public UpmsAppSQL andAppIdGreaterThanOrEqualTo(java.lang.String value) {
          addCriterion("app_id", value, ConditionMode.GREATER_EQUAL, "appId", "java.lang.String", "String");
          return this;
      }

      public UpmsAppSQL andAppIdLessThan(java.lang.String value) {
          addCriterion("app_id", value, ConditionMode.LESS_THEN, "appId", "java.lang.String", "String");
          return this;
      }

      public UpmsAppSQL andAppIdLessThanOrEqualTo(java.lang.String value) {
          addCriterion("app_id", value, ConditionMode.LESS_EQUAL, "appId", "java.lang.String", "String");
          return this;
      }

      public UpmsAppSQL andAppIdBetween(java.lang.String value1, java.lang.String value2) {
    	  addCriterion("app_id", value1, value2, ConditionMode.BETWEEN, "appId", "java.lang.String", "String");
    	  return this;
      }

      public UpmsAppSQL andAppIdNotBetween(java.lang.String value1, java.lang.String value2) {
          addCriterion("app_id", value1, value2, ConditionMode.NOT_BETWEEN, "appId", "java.lang.String", "String");
          return this;
      }
        
      public UpmsAppSQL andAppIdIn(List<java.lang.String> values) {
          addCriterion("app_id", values, ConditionMode.IN, "appId", "java.lang.String", "String");
          return this;
      }

      public UpmsAppSQL andAppIdNotIn(List<java.lang.String> values) {
          addCriterion("app_id", values, ConditionMode.NOT_IN, "appId", "java.lang.String", "String");
          return this;
      }
}