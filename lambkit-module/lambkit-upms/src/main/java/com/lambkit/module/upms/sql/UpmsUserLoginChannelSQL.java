/**
 * Copyright (c) 2015-2017, Henry Yang 杨勇 (gismail@foxmail.com).
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.lambkit.module.upms.sql;

import com.lambkit.db.sql.ConditionMode;
import com.lambkit.db.sql.Column;
import com.lambkit.db.sql.Columns;
import com.lambkit.db.sql.Example;

import java.io.Serializable;
import java.util.List;

/**
 * @author yangyong 
 * @website: www.lambkit.com
 * @email: gismail@foxmail.com
 * @date ${date}
 * @version 1.0
 * @since 1.0
 */

public class UpmsUserLoginChannelSQL extends Columns implements Serializable {
	private static final long serialVersionUID = 1L;
	
	public static UpmsUserLoginChannelSQL of() {
		return new UpmsUserLoginChannelSQL();
	}
	
	public static UpmsUserLoginChannelSQL by(Column column) {
		UpmsUserLoginChannelSQL that = new UpmsUserLoginChannelSQL();
		that.add(column);
        return that;
    }

    public static UpmsUserLoginChannelSQL by(String name, Object value) {
        return (UpmsUserLoginChannelSQL) create().eq(name, value);
    }
    
    public Example example() {
    	return Example.create("upms_user_login_channel", this);
    }
    
    /**
     * equals
     *
     * @param name
     * @param value
     * @return
     */
    public UpmsUserLoginChannelSQL eq(String name, Object value) {
    	super.eq(name, value);
        return this;
    }

    /**
     * not equals !=
     *
     * @param name
     * @param value
     * @return
     */
    public UpmsUserLoginChannelSQL ne(String name, Object value) {
    	super.ne(name, value);
        return this;
    }


    /**
     * like
     *
     * @param name
     * @param value
     * @return
     */

    public UpmsUserLoginChannelSQL like(String name, Object value) {
    	super.like(name, value);
        return this;
    }
    
    public UpmsUserLoginChannelSQL notLike(String name, Object value) {
    	super.notLike(name, value);
        return this;
    }

    /**
     * 大于 great than
     *
     * @param name
     * @param value
     * @return
     */
    public UpmsUserLoginChannelSQL gt(String name, Object value) {
    	super.gt(name, value);
        return this;
    }

    /**
     * 大于等于 great or equal
     *
     * @param name
     * @param value
     * @return
     */
    public UpmsUserLoginChannelSQL ge(String name, Object value) {
    	super.ge(name, value);
        return this;
    }

    /**
     * 小于 less than
     *
     * @param name
     * @param value
     * @return
     */
    public UpmsUserLoginChannelSQL lt(String name, Object value) {
    	super.lt(name, value);
        return this;
    }

    /**
     * 小于等于 less or equal
     *
     * @param name
     * @param value
     * @return
     */
    public UpmsUserLoginChannelSQL le(String name, Object value) {
    	super.le(name, value);
        return this;
    }
    
    public UpmsUserLoginChannelSQL isnull(String name) {
    	super.isnull(name);
        return this;
    } 

    public UpmsUserLoginChannelSQL notNull(String name) {
    	super.notNull(name);
        return this;
    } 
    
    public UpmsUserLoginChannelSQL empty(String name) {
    	super.empty(name);
        return this;
    } 
    
    public UpmsUserLoginChannelSQL notEmpty(String name) {
    	super.notEmpty(name);
        return this;
    } 
    
    public UpmsUserLoginChannelSQL add(Column column) {
    	super.add(column);
    	return this;
    }
    
    /**************************/
	
	public void addCriterion(String name, Object value, ConditionMode logic, String property, String typeHandler, String valueType) {
		 if (value == null) {
			 throw new RuntimeException("Value for " + property + " cannot be null");
		 }
		 add(Column.create(name, value, logic, typeHandler, valueType));
	}
   
	public void addCriterion(String name, Object value1, Object value2, ConditionMode logic, String property, String typeHandler, String valueType) {
		 if (value1 == null || value2 == null) {
			 throw new RuntimeException("Between values for " + property + " cannot be null");
		 }
		 add(Column.create(name, value1, value2, logic, typeHandler, valueType));
	}
		 
	public UpmsUserLoginChannelSQL andIdIsNull() {
		isnull("id");
		return this;
	}
	
	public UpmsUserLoginChannelSQL andIdIsNotNull() {
		notNull("id");
		return this;
	}
	
	public UpmsUserLoginChannelSQL andIdIsEmpty() {
		empty("id");
		return this;
	}

	public UpmsUserLoginChannelSQL andIdIsNotEmpty() {
		notEmpty("id");
		return this;
	}
      public UpmsUserLoginChannelSQL andIdEqualTo(java.lang.Long value) {
          addCriterion("id", value, ConditionMode.EQUAL, "id", "java.lang.Long", "Float");
          return this;
      }

      public UpmsUserLoginChannelSQL andIdNotEqualTo(java.lang.Long value) {
          addCriterion("id", value, ConditionMode.NOT_EQUAL, "id", "java.lang.Long", "Float");
          return this;
      }

      public UpmsUserLoginChannelSQL andIdGreaterThan(java.lang.Long value) {
          addCriterion("id", value, ConditionMode.GREATER_THEN, "id", "java.lang.Long", "Float");
          return this;
      }

      public UpmsUserLoginChannelSQL andIdGreaterThanOrEqualTo(java.lang.Long value) {
          addCriterion("id", value, ConditionMode.GREATER_EQUAL, "id", "java.lang.Long", "Float");
          return this;
      }

      public UpmsUserLoginChannelSQL andIdLessThan(java.lang.Long value) {
          addCriterion("id", value, ConditionMode.LESS_THEN, "id", "java.lang.Long", "Float");
          return this;
      }

      public UpmsUserLoginChannelSQL andIdLessThanOrEqualTo(java.lang.Long value) {
          addCriterion("id", value, ConditionMode.LESS_EQUAL, "id", "java.lang.Long", "Float");
          return this;
      }

      public UpmsUserLoginChannelSQL andIdBetween(java.lang.Long value1, java.lang.Long value2) {
    	  addCriterion("id", value1, value2, ConditionMode.BETWEEN, "id", "java.lang.Long", "Float");
    	  return this;
      }

      public UpmsUserLoginChannelSQL andIdNotBetween(java.lang.Long value1, java.lang.Long value2) {
          addCriterion("id", value1, value2, ConditionMode.NOT_BETWEEN, "id", "java.lang.Long", "Float");
          return this;
      }
        
      public UpmsUserLoginChannelSQL andIdIn(List<java.lang.Long> values) {
          addCriterion("id", values, ConditionMode.IN, "id", "java.lang.Long", "Float");
          return this;
      }

      public UpmsUserLoginChannelSQL andIdNotIn(List<java.lang.Long> values) {
          addCriterion("id", values, ConditionMode.NOT_IN, "id", "java.lang.Long", "Float");
          return this;
      }
	public UpmsUserLoginChannelSQL andUserIdIsNull() {
		isnull("user_id");
		return this;
	}
	
	public UpmsUserLoginChannelSQL andUserIdIsNotNull() {
		notNull("user_id");
		return this;
	}
	
	public UpmsUserLoginChannelSQL andUserIdIsEmpty() {
		empty("user_id");
		return this;
	}

	public UpmsUserLoginChannelSQL andUserIdIsNotEmpty() {
		notEmpty("user_id");
		return this;
	}
      public UpmsUserLoginChannelSQL andUserIdEqualTo(java.lang.Long value) {
          addCriterion("user_id", value, ConditionMode.EQUAL, "userId", "java.lang.Long", "Float");
          return this;
      }

      public UpmsUserLoginChannelSQL andUserIdNotEqualTo(java.lang.Long value) {
          addCriterion("user_id", value, ConditionMode.NOT_EQUAL, "userId", "java.lang.Long", "Float");
          return this;
      }

      public UpmsUserLoginChannelSQL andUserIdGreaterThan(java.lang.Long value) {
          addCriterion("user_id", value, ConditionMode.GREATER_THEN, "userId", "java.lang.Long", "Float");
          return this;
      }

      public UpmsUserLoginChannelSQL andUserIdGreaterThanOrEqualTo(java.lang.Long value) {
          addCriterion("user_id", value, ConditionMode.GREATER_EQUAL, "userId", "java.lang.Long", "Float");
          return this;
      }

      public UpmsUserLoginChannelSQL andUserIdLessThan(java.lang.Long value) {
          addCriterion("user_id", value, ConditionMode.LESS_THEN, "userId", "java.lang.Long", "Float");
          return this;
      }

      public UpmsUserLoginChannelSQL andUserIdLessThanOrEqualTo(java.lang.Long value) {
          addCriterion("user_id", value, ConditionMode.LESS_EQUAL, "userId", "java.lang.Long", "Float");
          return this;
      }

      public UpmsUserLoginChannelSQL andUserIdBetween(java.lang.Long value1, java.lang.Long value2) {
    	  addCriterion("user_id", value1, value2, ConditionMode.BETWEEN, "userId", "java.lang.Long", "Float");
    	  return this;
      }

      public UpmsUserLoginChannelSQL andUserIdNotBetween(java.lang.Long value1, java.lang.Long value2) {
          addCriterion("user_id", value1, value2, ConditionMode.NOT_BETWEEN, "userId", "java.lang.Long", "Float");
          return this;
      }
        
      public UpmsUserLoginChannelSQL andUserIdIn(List<java.lang.Long> values) {
          addCriterion("user_id", values, ConditionMode.IN, "userId", "java.lang.Long", "Float");
          return this;
      }

      public UpmsUserLoginChannelSQL andUserIdNotIn(List<java.lang.Long> values) {
          addCriterion("user_id", values, ConditionMode.NOT_IN, "userId", "java.lang.Long", "Float");
          return this;
      }
	public UpmsUserLoginChannelSQL andPhoneIsNull() {
		isnull("phone");
		return this;
	}
	
	public UpmsUserLoginChannelSQL andPhoneIsNotNull() {
		notNull("phone");
		return this;
	}
	
	public UpmsUserLoginChannelSQL andPhoneIsEmpty() {
		empty("phone");
		return this;
	}

	public UpmsUserLoginChannelSQL andPhoneIsNotEmpty() {
		notEmpty("phone");
		return this;
	}
       public UpmsUserLoginChannelSQL andPhoneLike(java.lang.String value) {
    	   addCriterion("phone", value, ConditionMode.FUZZY, "phone", "java.lang.String", "Float");
    	   return this;
      }

      public UpmsUserLoginChannelSQL andPhoneNotLike(java.lang.String value) {
          addCriterion("phone", value, ConditionMode.NOT_FUZZY, "phone", "java.lang.String", "Float");
          return this;
      }
      public UpmsUserLoginChannelSQL andPhoneEqualTo(java.lang.String value) {
          addCriterion("phone", value, ConditionMode.EQUAL, "phone", "java.lang.String", "String");
          return this;
      }

      public UpmsUserLoginChannelSQL andPhoneNotEqualTo(java.lang.String value) {
          addCriterion("phone", value, ConditionMode.NOT_EQUAL, "phone", "java.lang.String", "String");
          return this;
      }

      public UpmsUserLoginChannelSQL andPhoneGreaterThan(java.lang.String value) {
          addCriterion("phone", value, ConditionMode.GREATER_THEN, "phone", "java.lang.String", "String");
          return this;
      }

      public UpmsUserLoginChannelSQL andPhoneGreaterThanOrEqualTo(java.lang.String value) {
          addCriterion("phone", value, ConditionMode.GREATER_EQUAL, "phone", "java.lang.String", "String");
          return this;
      }

      public UpmsUserLoginChannelSQL andPhoneLessThan(java.lang.String value) {
          addCriterion("phone", value, ConditionMode.LESS_THEN, "phone", "java.lang.String", "String");
          return this;
      }

      public UpmsUserLoginChannelSQL andPhoneLessThanOrEqualTo(java.lang.String value) {
          addCriterion("phone", value, ConditionMode.LESS_EQUAL, "phone", "java.lang.String", "String");
          return this;
      }

      public UpmsUserLoginChannelSQL andPhoneBetween(java.lang.String value1, java.lang.String value2) {
    	  addCriterion("phone", value1, value2, ConditionMode.BETWEEN, "phone", "java.lang.String", "String");
    	  return this;
      }

      public UpmsUserLoginChannelSQL andPhoneNotBetween(java.lang.String value1, java.lang.String value2) {
          addCriterion("phone", value1, value2, ConditionMode.NOT_BETWEEN, "phone", "java.lang.String", "String");
          return this;
      }
        
      public UpmsUserLoginChannelSQL andPhoneIn(List<java.lang.String> values) {
          addCriterion("phone", values, ConditionMode.IN, "phone", "java.lang.String", "String");
          return this;
      }

      public UpmsUserLoginChannelSQL andPhoneNotIn(List<java.lang.String> values) {
          addCriterion("phone", values, ConditionMode.NOT_IN, "phone", "java.lang.String", "String");
          return this;
      }
	public UpmsUserLoginChannelSQL andTypeIsNull() {
		isnull("type");
		return this;
	}
	
	public UpmsUserLoginChannelSQL andTypeIsNotNull() {
		notNull("type");
		return this;
	}
	
	public UpmsUserLoginChannelSQL andTypeIsEmpty() {
		empty("type");
		return this;
	}

	public UpmsUserLoginChannelSQL andTypeIsNotEmpty() {
		notEmpty("type");
		return this;
	}
      public UpmsUserLoginChannelSQL andTypeEqualTo(java.lang.Integer value) {
          addCriterion("type", value, ConditionMode.EQUAL, "type", "java.lang.Integer", "Float");
          return this;
      }

      public UpmsUserLoginChannelSQL andTypeNotEqualTo(java.lang.Integer value) {
          addCriterion("type", value, ConditionMode.NOT_EQUAL, "type", "java.lang.Integer", "Float");
          return this;
      }

      public UpmsUserLoginChannelSQL andTypeGreaterThan(java.lang.Integer value) {
          addCriterion("type", value, ConditionMode.GREATER_THEN, "type", "java.lang.Integer", "Float");
          return this;
      }

      public UpmsUserLoginChannelSQL andTypeGreaterThanOrEqualTo(java.lang.Integer value) {
          addCriterion("type", value, ConditionMode.GREATER_EQUAL, "type", "java.lang.Integer", "Float");
          return this;
      }

      public UpmsUserLoginChannelSQL andTypeLessThan(java.lang.Integer value) {
          addCriterion("type", value, ConditionMode.LESS_THEN, "type", "java.lang.Integer", "Float");
          return this;
      }

      public UpmsUserLoginChannelSQL andTypeLessThanOrEqualTo(java.lang.Integer value) {
          addCriterion("type", value, ConditionMode.LESS_EQUAL, "type", "java.lang.Integer", "Float");
          return this;
      }

      public UpmsUserLoginChannelSQL andTypeBetween(java.lang.Integer value1, java.lang.Integer value2) {
    	  addCriterion("type", value1, value2, ConditionMode.BETWEEN, "type", "java.lang.Integer", "Float");
    	  return this;
      }

      public UpmsUserLoginChannelSQL andTypeNotBetween(java.lang.Integer value1, java.lang.Integer value2) {
          addCriterion("type", value1, value2, ConditionMode.NOT_BETWEEN, "type", "java.lang.Integer", "Float");
          return this;
      }
        
      public UpmsUserLoginChannelSQL andTypeIn(List<java.lang.Integer> values) {
          addCriterion("type", values, ConditionMode.IN, "type", "java.lang.Integer", "Float");
          return this;
      }

      public UpmsUserLoginChannelSQL andTypeNotIn(List<java.lang.Integer> values) {
          addCriterion("type", values, ConditionMode.NOT_IN, "type", "java.lang.Integer", "Float");
          return this;
      }
	public UpmsUserLoginChannelSQL andUserNameIsNull() {
		isnull("user_name");
		return this;
	}
	
	public UpmsUserLoginChannelSQL andUserNameIsNotNull() {
		notNull("user_name");
		return this;
	}
	
	public UpmsUserLoginChannelSQL andUserNameIsEmpty() {
		empty("user_name");
		return this;
	}

	public UpmsUserLoginChannelSQL andUserNameIsNotEmpty() {
		notEmpty("user_name");
		return this;
	}
       public UpmsUserLoginChannelSQL andUserNameLike(java.lang.String value) {
    	   addCriterion("user_name", value, ConditionMode.FUZZY, "userName", "java.lang.String", "Float");
    	   return this;
      }

      public UpmsUserLoginChannelSQL andUserNameNotLike(java.lang.String value) {
          addCriterion("user_name", value, ConditionMode.NOT_FUZZY, "userName", "java.lang.String", "Float");
          return this;
      }
      public UpmsUserLoginChannelSQL andUserNameEqualTo(java.lang.String value) {
          addCriterion("user_name", value, ConditionMode.EQUAL, "userName", "java.lang.String", "String");
          return this;
      }

      public UpmsUserLoginChannelSQL andUserNameNotEqualTo(java.lang.String value) {
          addCriterion("user_name", value, ConditionMode.NOT_EQUAL, "userName", "java.lang.String", "String");
          return this;
      }

      public UpmsUserLoginChannelSQL andUserNameGreaterThan(java.lang.String value) {
          addCriterion("user_name", value, ConditionMode.GREATER_THEN, "userName", "java.lang.String", "String");
          return this;
      }

      public UpmsUserLoginChannelSQL andUserNameGreaterThanOrEqualTo(java.lang.String value) {
          addCriterion("user_name", value, ConditionMode.GREATER_EQUAL, "userName", "java.lang.String", "String");
          return this;
      }

      public UpmsUserLoginChannelSQL andUserNameLessThan(java.lang.String value) {
          addCriterion("user_name", value, ConditionMode.LESS_THEN, "userName", "java.lang.String", "String");
          return this;
      }

      public UpmsUserLoginChannelSQL andUserNameLessThanOrEqualTo(java.lang.String value) {
          addCriterion("user_name", value, ConditionMode.LESS_EQUAL, "userName", "java.lang.String", "String");
          return this;
      }

      public UpmsUserLoginChannelSQL andUserNameBetween(java.lang.String value1, java.lang.String value2) {
    	  addCriterion("user_name", value1, value2, ConditionMode.BETWEEN, "userName", "java.lang.String", "String");
    	  return this;
      }

      public UpmsUserLoginChannelSQL andUserNameNotBetween(java.lang.String value1, java.lang.String value2) {
          addCriterion("user_name", value1, value2, ConditionMode.NOT_BETWEEN, "userName", "java.lang.String", "String");
          return this;
      }
        
      public UpmsUserLoginChannelSQL andUserNameIn(List<java.lang.String> values) {
          addCriterion("user_name", values, ConditionMode.IN, "userName", "java.lang.String", "String");
          return this;
      }

      public UpmsUserLoginChannelSQL andUserNameNotIn(List<java.lang.String> values) {
          addCriterion("user_name", values, ConditionMode.NOT_IN, "userName", "java.lang.String", "String");
          return this;
      }
	public UpmsUserLoginChannelSQL andUnionidIsNull() {
		isnull("unionid");
		return this;
	}
	
	public UpmsUserLoginChannelSQL andUnionidIsNotNull() {
		notNull("unionid");
		return this;
	}
	
	public UpmsUserLoginChannelSQL andUnionidIsEmpty() {
		empty("unionid");
		return this;
	}

	public UpmsUserLoginChannelSQL andUnionidIsNotEmpty() {
		notEmpty("unionid");
		return this;
	}
       public UpmsUserLoginChannelSQL andUnionidLike(java.lang.String value) {
    	   addCriterion("unionid", value, ConditionMode.FUZZY, "unionid", "java.lang.String", "String");
    	   return this;
      }

      public UpmsUserLoginChannelSQL andUnionidNotLike(java.lang.String value) {
          addCriterion("unionid", value, ConditionMode.NOT_FUZZY, "unionid", "java.lang.String", "String");
          return this;
      }
      public UpmsUserLoginChannelSQL andUnionidEqualTo(java.lang.String value) {
          addCriterion("unionid", value, ConditionMode.EQUAL, "unionid", "java.lang.String", "String");
          return this;
      }

      public UpmsUserLoginChannelSQL andUnionidNotEqualTo(java.lang.String value) {
          addCriterion("unionid", value, ConditionMode.NOT_EQUAL, "unionid", "java.lang.String", "String");
          return this;
      }

      public UpmsUserLoginChannelSQL andUnionidGreaterThan(java.lang.String value) {
          addCriterion("unionid", value, ConditionMode.GREATER_THEN, "unionid", "java.lang.String", "String");
          return this;
      }

      public UpmsUserLoginChannelSQL andUnionidGreaterThanOrEqualTo(java.lang.String value) {
          addCriterion("unionid", value, ConditionMode.GREATER_EQUAL, "unionid", "java.lang.String", "String");
          return this;
      }

      public UpmsUserLoginChannelSQL andUnionidLessThan(java.lang.String value) {
          addCriterion("unionid", value, ConditionMode.LESS_THEN, "unionid", "java.lang.String", "String");
          return this;
      }

      public UpmsUserLoginChannelSQL andUnionidLessThanOrEqualTo(java.lang.String value) {
          addCriterion("unionid", value, ConditionMode.LESS_EQUAL, "unionid", "java.lang.String", "String");
          return this;
      }

      public UpmsUserLoginChannelSQL andUnionidBetween(java.lang.String value1, java.lang.String value2) {
    	  addCriterion("unionid", value1, value2, ConditionMode.BETWEEN, "unionid", "java.lang.String", "String");
    	  return this;
      }

      public UpmsUserLoginChannelSQL andUnionidNotBetween(java.lang.String value1, java.lang.String value2) {
          addCriterion("unionid", value1, value2, ConditionMode.NOT_BETWEEN, "unionid", "java.lang.String", "String");
          return this;
      }
        
      public UpmsUserLoginChannelSQL andUnionidIn(List<java.lang.String> values) {
          addCriterion("unionid", values, ConditionMode.IN, "unionid", "java.lang.String", "String");
          return this;
      }

      public UpmsUserLoginChannelSQL andUnionidNotIn(List<java.lang.String> values) {
          addCriterion("unionid", values, ConditionMode.NOT_IN, "unionid", "java.lang.String", "String");
          return this;
      }
	public UpmsUserLoginChannelSQL andOpenidIsNull() {
		isnull("openid");
		return this;
	}
	
	public UpmsUserLoginChannelSQL andOpenidIsNotNull() {
		notNull("openid");
		return this;
	}
	
	public UpmsUserLoginChannelSQL andOpenidIsEmpty() {
		empty("openid");
		return this;
	}

	public UpmsUserLoginChannelSQL andOpenidIsNotEmpty() {
		notEmpty("openid");
		return this;
	}
       public UpmsUserLoginChannelSQL andOpenidLike(java.lang.String value) {
    	   addCriterion("openid", value, ConditionMode.FUZZY, "openid", "java.lang.String", "String");
    	   return this;
      }

      public UpmsUserLoginChannelSQL andOpenidNotLike(java.lang.String value) {
          addCriterion("openid", value, ConditionMode.NOT_FUZZY, "openid", "java.lang.String", "String");
          return this;
      }
      public UpmsUserLoginChannelSQL andOpenidEqualTo(java.lang.String value) {
          addCriterion("openid", value, ConditionMode.EQUAL, "openid", "java.lang.String", "String");
          return this;
      }

      public UpmsUserLoginChannelSQL andOpenidNotEqualTo(java.lang.String value) {
          addCriterion("openid", value, ConditionMode.NOT_EQUAL, "openid", "java.lang.String", "String");
          return this;
      }

      public UpmsUserLoginChannelSQL andOpenidGreaterThan(java.lang.String value) {
          addCriterion("openid", value, ConditionMode.GREATER_THEN, "openid", "java.lang.String", "String");
          return this;
      }

      public UpmsUserLoginChannelSQL andOpenidGreaterThanOrEqualTo(java.lang.String value) {
          addCriterion("openid", value, ConditionMode.GREATER_EQUAL, "openid", "java.lang.String", "String");
          return this;
      }

      public UpmsUserLoginChannelSQL andOpenidLessThan(java.lang.String value) {
          addCriterion("openid", value, ConditionMode.LESS_THEN, "openid", "java.lang.String", "String");
          return this;
      }

      public UpmsUserLoginChannelSQL andOpenidLessThanOrEqualTo(java.lang.String value) {
          addCriterion("openid", value, ConditionMode.LESS_EQUAL, "openid", "java.lang.String", "String");
          return this;
      }

      public UpmsUserLoginChannelSQL andOpenidBetween(java.lang.String value1, java.lang.String value2) {
    	  addCriterion("openid", value1, value2, ConditionMode.BETWEEN, "openid", "java.lang.String", "String");
    	  return this;
      }

      public UpmsUserLoginChannelSQL andOpenidNotBetween(java.lang.String value1, java.lang.String value2) {
          addCriterion("openid", value1, value2, ConditionMode.NOT_BETWEEN, "openid", "java.lang.String", "String");
          return this;
      }
        
      public UpmsUserLoginChannelSQL andOpenidIn(List<java.lang.String> values) {
          addCriterion("openid", values, ConditionMode.IN, "openid", "java.lang.String", "String");
          return this;
      }

      public UpmsUserLoginChannelSQL andOpenidNotIn(List<java.lang.String> values) {
          addCriterion("openid", values, ConditionMode.NOT_IN, "openid", "java.lang.String", "String");
          return this;
      }
	public UpmsUserLoginChannelSQL andCreateTimeIsNull() {
		isnull("create_time");
		return this;
	}
	
	public UpmsUserLoginChannelSQL andCreateTimeIsNotNull() {
		notNull("create_time");
		return this;
	}
	
	public UpmsUserLoginChannelSQL andCreateTimeIsEmpty() {
		empty("create_time");
		return this;
	}

	public UpmsUserLoginChannelSQL andCreateTimeIsNotEmpty() {
		notEmpty("create_time");
		return this;
	}
      public UpmsUserLoginChannelSQL andCreateTimeEqualTo(java.util.Date value) {
          addCriterion("create_time", value, ConditionMode.EQUAL, "createTime", "java.util.Date", "String");
          return this;
      }

      public UpmsUserLoginChannelSQL andCreateTimeNotEqualTo(java.util.Date value) {
          addCriterion("create_time", value, ConditionMode.NOT_EQUAL, "createTime", "java.util.Date", "String");
          return this;
      }

      public UpmsUserLoginChannelSQL andCreateTimeGreaterThan(java.util.Date value) {
          addCriterion("create_time", value, ConditionMode.GREATER_THEN, "createTime", "java.util.Date", "String");
          return this;
      }

      public UpmsUserLoginChannelSQL andCreateTimeGreaterThanOrEqualTo(java.util.Date value) {
          addCriterion("create_time", value, ConditionMode.GREATER_EQUAL, "createTime", "java.util.Date", "String");
          return this;
      }

      public UpmsUserLoginChannelSQL andCreateTimeLessThan(java.util.Date value) {
          addCriterion("create_time", value, ConditionMode.LESS_THEN, "createTime", "java.util.Date", "String");
          return this;
      }

      public UpmsUserLoginChannelSQL andCreateTimeLessThanOrEqualTo(java.util.Date value) {
          addCriterion("create_time", value, ConditionMode.LESS_EQUAL, "createTime", "java.util.Date", "String");
          return this;
      }

      public UpmsUserLoginChannelSQL andCreateTimeBetween(java.util.Date value1, java.util.Date value2) {
    	  addCriterion("create_time", value1, value2, ConditionMode.BETWEEN, "createTime", "java.util.Date", "String");
    	  return this;
      }

      public UpmsUserLoginChannelSQL andCreateTimeNotBetween(java.util.Date value1, java.util.Date value2) {
          addCriterion("create_time", value1, value2, ConditionMode.NOT_BETWEEN, "createTime", "java.util.Date", "String");
          return this;
      }
        
      public UpmsUserLoginChannelSQL andCreateTimeIn(List<java.util.Date> values) {
          addCriterion("create_time", values, ConditionMode.IN, "createTime", "java.util.Date", "String");
          return this;
      }

      public UpmsUserLoginChannelSQL andCreateTimeNotIn(List<java.util.Date> values) {
          addCriterion("create_time", values, ConditionMode.NOT_IN, "createTime", "java.util.Date", "String");
          return this;
      }
	public UpmsUserLoginChannelSQL andUpdateTimeIsNull() {
		isnull("update_time");
		return this;
	}
	
	public UpmsUserLoginChannelSQL andUpdateTimeIsNotNull() {
		notNull("update_time");
		return this;
	}
	
	public UpmsUserLoginChannelSQL andUpdateTimeIsEmpty() {
		empty("update_time");
		return this;
	}

	public UpmsUserLoginChannelSQL andUpdateTimeIsNotEmpty() {
		notEmpty("update_time");
		return this;
	}
      public UpmsUserLoginChannelSQL andUpdateTimeEqualTo(java.util.Date value) {
          addCriterion("update_time", value, ConditionMode.EQUAL, "updateTime", "java.util.Date", "String");
          return this;
      }

      public UpmsUserLoginChannelSQL andUpdateTimeNotEqualTo(java.util.Date value) {
          addCriterion("update_time", value, ConditionMode.NOT_EQUAL, "updateTime", "java.util.Date", "String");
          return this;
      }

      public UpmsUserLoginChannelSQL andUpdateTimeGreaterThan(java.util.Date value) {
          addCriterion("update_time", value, ConditionMode.GREATER_THEN, "updateTime", "java.util.Date", "String");
          return this;
      }

      public UpmsUserLoginChannelSQL andUpdateTimeGreaterThanOrEqualTo(java.util.Date value) {
          addCriterion("update_time", value, ConditionMode.GREATER_EQUAL, "updateTime", "java.util.Date", "String");
          return this;
      }

      public UpmsUserLoginChannelSQL andUpdateTimeLessThan(java.util.Date value) {
          addCriterion("update_time", value, ConditionMode.LESS_THEN, "updateTime", "java.util.Date", "String");
          return this;
      }

      public UpmsUserLoginChannelSQL andUpdateTimeLessThanOrEqualTo(java.util.Date value) {
          addCriterion("update_time", value, ConditionMode.LESS_EQUAL, "updateTime", "java.util.Date", "String");
          return this;
      }

      public UpmsUserLoginChannelSQL andUpdateTimeBetween(java.util.Date value1, java.util.Date value2) {
    	  addCriterion("update_time", value1, value2, ConditionMode.BETWEEN, "updateTime", "java.util.Date", "String");
    	  return this;
      }

      public UpmsUserLoginChannelSQL andUpdateTimeNotBetween(java.util.Date value1, java.util.Date value2) {
          addCriterion("update_time", value1, value2, ConditionMode.NOT_BETWEEN, "updateTime", "java.util.Date", "String");
          return this;
      }
        
      public UpmsUserLoginChannelSQL andUpdateTimeIn(List<java.util.Date> values) {
          addCriterion("update_time", values, ConditionMode.IN, "updateTime", "java.util.Date", "String");
          return this;
      }

      public UpmsUserLoginChannelSQL andUpdateTimeNotIn(List<java.util.Date> values) {
          addCriterion("update_time", values, ConditionMode.NOT_IN, "updateTime", "java.util.Date", "String");
          return this;
      }
	public UpmsUserLoginChannelSQL andStatusIsNull() {
		isnull("status");
		return this;
	}
	
	public UpmsUserLoginChannelSQL andStatusIsNotNull() {
		notNull("status");
		return this;
	}
	
	public UpmsUserLoginChannelSQL andStatusIsEmpty() {
		empty("status");
		return this;
	}

	public UpmsUserLoginChannelSQL andStatusIsNotEmpty() {
		notEmpty("status");
		return this;
	}
      public UpmsUserLoginChannelSQL andStatusEqualTo(java.lang.Integer value) {
          addCriterion("status", value, ConditionMode.EQUAL, "status", "java.lang.Integer", "Float");
          return this;
      }

      public UpmsUserLoginChannelSQL andStatusNotEqualTo(java.lang.Integer value) {
          addCriterion("status", value, ConditionMode.NOT_EQUAL, "status", "java.lang.Integer", "Float");
          return this;
      }

      public UpmsUserLoginChannelSQL andStatusGreaterThan(java.lang.Integer value) {
          addCriterion("status", value, ConditionMode.GREATER_THEN, "status", "java.lang.Integer", "Float");
          return this;
      }

      public UpmsUserLoginChannelSQL andStatusGreaterThanOrEqualTo(java.lang.Integer value) {
          addCriterion("status", value, ConditionMode.GREATER_EQUAL, "status", "java.lang.Integer", "Float");
          return this;
      }

      public UpmsUserLoginChannelSQL andStatusLessThan(java.lang.Integer value) {
          addCriterion("status", value, ConditionMode.LESS_THEN, "status", "java.lang.Integer", "Float");
          return this;
      }

      public UpmsUserLoginChannelSQL andStatusLessThanOrEqualTo(java.lang.Integer value) {
          addCriterion("status", value, ConditionMode.LESS_EQUAL, "status", "java.lang.Integer", "Float");
          return this;
      }

      public UpmsUserLoginChannelSQL andStatusBetween(java.lang.Integer value1, java.lang.Integer value2) {
    	  addCriterion("status", value1, value2, ConditionMode.BETWEEN, "status", "java.lang.Integer", "Float");
    	  return this;
      }

      public UpmsUserLoginChannelSQL andStatusNotBetween(java.lang.Integer value1, java.lang.Integer value2) {
          addCriterion("status", value1, value2, ConditionMode.NOT_BETWEEN, "status", "java.lang.Integer", "Float");
          return this;
      }
        
      public UpmsUserLoginChannelSQL andStatusIn(List<java.lang.Integer> values) {
          addCriterion("status", values, ConditionMode.IN, "status", "java.lang.Integer", "Float");
          return this;
      }

      public UpmsUserLoginChannelSQL andStatusNotIn(List<java.lang.Integer> values) {
          addCriterion("status", values, ConditionMode.NOT_IN, "status", "java.lang.Integer", "Float");
          return this;
      }
	public UpmsUserLoginChannelSQL andDelFlagIsNull() {
		isnull("del_flag");
		return this;
	}
	
	public UpmsUserLoginChannelSQL andDelFlagIsNotNull() {
		notNull("del_flag");
		return this;
	}
	
	public UpmsUserLoginChannelSQL andDelFlagIsEmpty() {
		empty("del_flag");
		return this;
	}

	public UpmsUserLoginChannelSQL andDelFlagIsNotEmpty() {
		notEmpty("del_flag");
		return this;
	}
      public UpmsUserLoginChannelSQL andDelFlagEqualTo(java.lang.Integer value) {
          addCriterion("del_flag", value, ConditionMode.EQUAL, "delFlag", "java.lang.Integer", "Float");
          return this;
      }

      public UpmsUserLoginChannelSQL andDelFlagNotEqualTo(java.lang.Integer value) {
          addCriterion("del_flag", value, ConditionMode.NOT_EQUAL, "delFlag", "java.lang.Integer", "Float");
          return this;
      }

      public UpmsUserLoginChannelSQL andDelFlagGreaterThan(java.lang.Integer value) {
          addCriterion("del_flag", value, ConditionMode.GREATER_THEN, "delFlag", "java.lang.Integer", "Float");
          return this;
      }

      public UpmsUserLoginChannelSQL andDelFlagGreaterThanOrEqualTo(java.lang.Integer value) {
          addCriterion("del_flag", value, ConditionMode.GREATER_EQUAL, "delFlag", "java.lang.Integer", "Float");
          return this;
      }

      public UpmsUserLoginChannelSQL andDelFlagLessThan(java.lang.Integer value) {
          addCriterion("del_flag", value, ConditionMode.LESS_THEN, "delFlag", "java.lang.Integer", "Float");
          return this;
      }

      public UpmsUserLoginChannelSQL andDelFlagLessThanOrEqualTo(java.lang.Integer value) {
          addCriterion("del_flag", value, ConditionMode.LESS_EQUAL, "delFlag", "java.lang.Integer", "Float");
          return this;
      }

      public UpmsUserLoginChannelSQL andDelFlagBetween(java.lang.Integer value1, java.lang.Integer value2) {
    	  addCriterion("del_flag", value1, value2, ConditionMode.BETWEEN, "delFlag", "java.lang.Integer", "Float");
    	  return this;
      }

      public UpmsUserLoginChannelSQL andDelFlagNotBetween(java.lang.Integer value1, java.lang.Integer value2) {
          addCriterion("del_flag", value1, value2, ConditionMode.NOT_BETWEEN, "delFlag", "java.lang.Integer", "Float");
          return this;
      }
        
      public UpmsUserLoginChannelSQL andDelFlagIn(List<java.lang.Integer> values) {
          addCriterion("del_flag", values, ConditionMode.IN, "delFlag", "java.lang.Integer", "Float");
          return this;
      }

      public UpmsUserLoginChannelSQL andDelFlagNotIn(List<java.lang.Integer> values) {
          addCriterion("del_flag", values, ConditionMode.NOT_IN, "delFlag", "java.lang.Integer", "Float");
          return this;
      }
}