/**
 * Copyright (c) 2015-2017, Henry Yang 杨勇 (gismail@foxmail.com).
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.lambkit.module.upms.row;

import com.lambkit.db.RowModel;

/**
 * @author yangyong 
 * @website: www.lambkit.com
 * @email: gismail@foxmail.com
 * @date ${date}
 * @version 1.0
 * @since 1.0
 */

public class UpmsUserAccountRole extends RowModel<UpmsUserAccountRole> {
	public UpmsUserAccountRole() {
		setTableName("upms_user_account_role");
		setPrimaryKey("id");
	}
	public java.lang.Long getId() {
		return this.get("id");
	}
	public void setId(java.lang.Long id) {
		this.set("id", id);
	}
	public java.lang.Long getUserAccountId() {
		return this.get("user_account_id");
	}
	public void setUserAccountId(java.lang.Long userAccountId) {
		this.set("user_account_id", userAccountId);
	}
	public java.lang.Long getRoleId() {
		return this.get("role_id");
	}
	public void setRoleId(java.lang.Long roleId) {
		this.set("role_id", roleId);
	}
}
