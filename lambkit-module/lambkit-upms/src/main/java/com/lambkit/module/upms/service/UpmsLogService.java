/**
 * Copyright (c) 2015-2017, Henry Yang 杨勇 (gismail@foxmail.com).
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.lambkit.module.upms.service;

import cn.hutool.json.JSONUtil;
import com.lambkit.core.Lambkit;
import com.lambkit.core.LambkitResult;
import com.lambkit.core.http.IRequest;
import com.lambkit.db.DbPool;
import com.lambkit.db.IDaoService;
import com.lambkit.db.IRowDao;
import com.lambkit.module.upms.UpmsConfig;
import com.lambkit.module.upms.row.UpmsLog;
import com.lambkit.util.RequestKit;

/**
 * @author yangyong 
 * @website: www.lambkit.com
 * @email: gismail@foxmail.com
 * @date ${date}
 * @version 1.0
 * @since 1.0
 */

public class UpmsLogService implements IDaoService<UpmsLog> {
	public IRowDao<UpmsLog> dao() {
		String dbPoolName = Lambkit.config(UpmsConfig.class).getDbPoolName();
		return DbPool.use(dbPoolName).dao(UpmsLog.class);
	}

	public IRowDao<UpmsLog> dao(String dbPoolName) {
		return DbPool.use(dbPoolName).dao(UpmsLog.class);
	}

	public UpmsLog addRegistLog(String username, LambkitResult result, IRequest request) {
		UpmsLog log = new UpmsLog();
		//log.setDescription("用户注册-" + result.getCode() + "-" + result.getMessage());
		if(result.getCode()==1) {
			log.setDescription("注册成功");
		} else {
			log.setDescription("注册失败");
		}
		log.setUri("regist");
		log.setPermissions("regist");
		log.setResult(JSONUtil.toJsonStr(result));
		toSave(log, username, request);
		return log;
	}

	public UpmsLog addLoginLog(String username, LambkitResult result, IRequest request) {
		UpmsLog log = new UpmsLog();
		//log.setDescription("用户登录-" + result.getCode() + "-" + result.getMessage());
		if(result.getCode()==1) {
			log.setDescription("登录成功");
		} else {
			log.setDescription("登录失败");
		}
		log.setUri("login");
		log.setPermissions("login");
		log.setResult(JSONUtil.toJsonStr(result));
		toSave(log, username, request);
		return log;
	}

	public UpmsLog addLogoutLog(String username, LambkitResult result, IRequest request) {
		UpmsLog log = new UpmsLog();
		//log.setDescription("用户退出-" + result.getCode() + "-" + result.getMessage());
		if(result.getCode()==1) {
			log.setDescription("退出成功");
		} else {
			log.setDescription("退出失败");
		}
		log.setUri("logout");
		log.setResult(JSONUtil.toJsonStr(result));
		log.setPermissions("logout");
		toSave(log, username, request);
		return log;
	}

	public UpmsLog addLog(String username, String desc, String uri, String result, String permissions, IRequest request) {
		UpmsLog log = new UpmsLog();
		log.setDescription(desc);
		log.setUri(uri);
		log.setPermissions(permissions);
		log.setResult(result);
		toSave(log, username, request);
		return log;
	}

	public UpmsLog addLog(String username, String desc, String uri, LambkitResult result, String permissions, IRequest request) {
		UpmsLog log = new UpmsLog();
		log.setDescription(desc + "-" + result.getCode() + "-" + result.getMessage());
		log.setUri(uri);
		log.setPermissions(permissions);
		log.setResult(JSONUtil.toJsonStr(result));
		toSave(log, username, request);
		return log;
	}

	private boolean toSave(UpmsLog log, String username, IRequest request) {
		if(request!=null) {
			log.setUrl(request.getRequestURI());
			log.setUserAgent(RequestKit.getUserAgent(request));
			log.setMethod(request.getMethod());
			log.setIp(RequestKit.getIpAddress(request));
			log.setBasePath(RequestKit.getBasePath(request));
			log.setParameter(request.getQueryString());
			request.setAttribute("upmsLogForHttp", log);
		}
		log.setUsername(username);
		//log.setStartTime(DateTimeUtils.getCurrentTimeLong());
		return dao().save(log);
	}
}
