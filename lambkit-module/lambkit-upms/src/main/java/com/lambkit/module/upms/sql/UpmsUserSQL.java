/**
 * Copyright (c) 2015-2017, Henry Yang 杨勇 (gismail@foxmail.com).
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.lambkit.module.upms.sql;

import com.lambkit.db.sql.ConditionMode;
import com.lambkit.db.sql.Column;
import com.lambkit.db.sql.Columns;
import com.lambkit.db.sql.Example;

import java.io.Serializable;
import java.util.List;

/**
 * @author yangyong 
 * @website: www.lambkit.com
 * @email: gismail@foxmail.com
 * @date ${date}
 * @version 1.0
 * @since 1.0
 */

public class UpmsUserSQL extends Columns implements Serializable {
	private static final long serialVersionUID = 1L;
	
	public static UpmsUserSQL of() {
		return new UpmsUserSQL();
	}
	
	public static UpmsUserSQL by(Column column) {
		UpmsUserSQL that = new UpmsUserSQL();
		that.add(column);
        return that;
    }

    public static UpmsUserSQL by(String name, Object value) {
        return (UpmsUserSQL) create().eq(name, value);
    }
    
    public Example example() {
    	return Example.create("upms_user", this);
    }
    
    /**
     * equals
     *
     * @param name
     * @param value
     * @return
     */
    public UpmsUserSQL eq(String name, Object value) {
    	super.eq(name, value);
        return this;
    }

    /**
     * not equals !=
     *
     * @param name
     * @param value
     * @return
     */
    public UpmsUserSQL ne(String name, Object value) {
    	super.ne(name, value);
        return this;
    }


    /**
     * like
     *
     * @param name
     * @param value
     * @return
     */

    public UpmsUserSQL like(String name, Object value) {
    	super.like(name, value);
        return this;
    }
    
    public UpmsUserSQL notLike(String name, Object value) {
    	super.notLike(name, value);
        return this;
    }

    /**
     * 大于 great than
     *
     * @param name
     * @param value
     * @return
     */
    public UpmsUserSQL gt(String name, Object value) {
    	super.gt(name, value);
        return this;
    }

    /**
     * 大于等于 great or equal
     *
     * @param name
     * @param value
     * @return
     */
    public UpmsUserSQL ge(String name, Object value) {
    	super.ge(name, value);
        return this;
    }

    /**
     * 小于 less than
     *
     * @param name
     * @param value
     * @return
     */
    public UpmsUserSQL lt(String name, Object value) {
    	super.lt(name, value);
        return this;
    }

    /**
     * 小于等于 less or equal
     *
     * @param name
     * @param value
     * @return
     */
    public UpmsUserSQL le(String name, Object value) {
    	super.le(name, value);
        return this;
    }
    
    public UpmsUserSQL isnull(String name) {
    	super.isnull(name);
        return this;
    } 

    public UpmsUserSQL notNull(String name) {
    	super.notNull(name);
        return this;
    } 
    
    public UpmsUserSQL empty(String name) {
    	super.empty(name);
        return this;
    } 
    
    public UpmsUserSQL notEmpty(String name) {
    	super.notEmpty(name);
        return this;
    } 
    
    public UpmsUserSQL add(Column column) {
    	super.add(column);
    	return this;
    }
    
    /**************************/
	
	public void addCriterion(String name, Object value, ConditionMode logic, String property, String typeHandler, String valueType) {
		 if (value == null) {
			 throw new RuntimeException("Value for " + property + " cannot be null");
		 }
		 add(Column.create(name, value, logic, typeHandler, valueType));
	}
   
	public void addCriterion(String name, Object value1, Object value2, ConditionMode logic, String property, String typeHandler, String valueType) {
		 if (value1 == null || value2 == null) {
			 throw new RuntimeException("Between values for " + property + " cannot be null");
		 }
		 add(Column.create(name, value1, value2, logic, typeHandler, valueType));
	}
		 
	public UpmsUserSQL andUserIdIsNull() {
		isnull("user_id");
		return this;
	}
	
	public UpmsUserSQL andUserIdIsNotNull() {
		notNull("user_id");
		return this;
	}
	
	public UpmsUserSQL andUserIdIsEmpty() {
		empty("user_id");
		return this;
	}

	public UpmsUserSQL andUserIdIsNotEmpty() {
		notEmpty("user_id");
		return this;
	}
      public UpmsUserSQL andUserIdEqualTo(java.lang.Long value) {
          addCriterion("user_id", value, ConditionMode.EQUAL, "userId", "java.lang.Long", "Float");
          return this;
      }

      public UpmsUserSQL andUserIdNotEqualTo(java.lang.Long value) {
          addCriterion("user_id", value, ConditionMode.NOT_EQUAL, "userId", "java.lang.Long", "Float");
          return this;
      }

      public UpmsUserSQL andUserIdGreaterThan(java.lang.Long value) {
          addCriterion("user_id", value, ConditionMode.GREATER_THEN, "userId", "java.lang.Long", "Float");
          return this;
      }

      public UpmsUserSQL andUserIdGreaterThanOrEqualTo(java.lang.Long value) {
          addCriterion("user_id", value, ConditionMode.GREATER_EQUAL, "userId", "java.lang.Long", "Float");
          return this;
      }

      public UpmsUserSQL andUserIdLessThan(java.lang.Long value) {
          addCriterion("user_id", value, ConditionMode.LESS_THEN, "userId", "java.lang.Long", "Float");
          return this;
      }

      public UpmsUserSQL andUserIdLessThanOrEqualTo(java.lang.Long value) {
          addCriterion("user_id", value, ConditionMode.LESS_EQUAL, "userId", "java.lang.Long", "Float");
          return this;
      }

      public UpmsUserSQL andUserIdBetween(java.lang.Long value1, java.lang.Long value2) {
    	  addCriterion("user_id", value1, value2, ConditionMode.BETWEEN, "userId", "java.lang.Long", "Float");
    	  return this;
      }

      public UpmsUserSQL andUserIdNotBetween(java.lang.Long value1, java.lang.Long value2) {
          addCriterion("user_id", value1, value2, ConditionMode.NOT_BETWEEN, "userId", "java.lang.Long", "Float");
          return this;
      }
        
      public UpmsUserSQL andUserIdIn(List<java.lang.Long> values) {
          addCriterion("user_id", values, ConditionMode.IN, "userId", "java.lang.Long", "Float");
          return this;
      }

      public UpmsUserSQL andUserIdNotIn(List<java.lang.Long> values) {
          addCriterion("user_id", values, ConditionMode.NOT_IN, "userId", "java.lang.Long", "Float");
          return this;
      }
	public UpmsUserSQL andUsernameIsNull() {
		isnull("username");
		return this;
	}
	
	public UpmsUserSQL andUsernameIsNotNull() {
		notNull("username");
		return this;
	}
	
	public UpmsUserSQL andUsernameIsEmpty() {
		empty("username");
		return this;
	}

	public UpmsUserSQL andUsernameIsNotEmpty() {
		notEmpty("username");
		return this;
	}
       public UpmsUserSQL andUsernameLike(java.lang.String value) {
    	   addCriterion("username", value, ConditionMode.FUZZY, "username", "java.lang.String", "Float");
    	   return this;
      }

      public UpmsUserSQL andUsernameNotLike(java.lang.String value) {
          addCriterion("username", value, ConditionMode.NOT_FUZZY, "username", "java.lang.String", "Float");
          return this;
      }
      public UpmsUserSQL andUsernameEqualTo(java.lang.String value) {
          addCriterion("username", value, ConditionMode.EQUAL, "username", "java.lang.String", "String");
          return this;
      }

      public UpmsUserSQL andUsernameNotEqualTo(java.lang.String value) {
          addCriterion("username", value, ConditionMode.NOT_EQUAL, "username", "java.lang.String", "String");
          return this;
      }

      public UpmsUserSQL andUsernameGreaterThan(java.lang.String value) {
          addCriterion("username", value, ConditionMode.GREATER_THEN, "username", "java.lang.String", "String");
          return this;
      }

      public UpmsUserSQL andUsernameGreaterThanOrEqualTo(java.lang.String value) {
          addCriterion("username", value, ConditionMode.GREATER_EQUAL, "username", "java.lang.String", "String");
          return this;
      }

      public UpmsUserSQL andUsernameLessThan(java.lang.String value) {
          addCriterion("username", value, ConditionMode.LESS_THEN, "username", "java.lang.String", "String");
          return this;
      }

      public UpmsUserSQL andUsernameLessThanOrEqualTo(java.lang.String value) {
          addCriterion("username", value, ConditionMode.LESS_EQUAL, "username", "java.lang.String", "String");
          return this;
      }

      public UpmsUserSQL andUsernameBetween(java.lang.String value1, java.lang.String value2) {
    	  addCriterion("username", value1, value2, ConditionMode.BETWEEN, "username", "java.lang.String", "String");
    	  return this;
      }

      public UpmsUserSQL andUsernameNotBetween(java.lang.String value1, java.lang.String value2) {
          addCriterion("username", value1, value2, ConditionMode.NOT_BETWEEN, "username", "java.lang.String", "String");
          return this;
      }
        
      public UpmsUserSQL andUsernameIn(List<java.lang.String> values) {
          addCriterion("username", values, ConditionMode.IN, "username", "java.lang.String", "String");
          return this;
      }

      public UpmsUserSQL andUsernameNotIn(List<java.lang.String> values) {
          addCriterion("username", values, ConditionMode.NOT_IN, "username", "java.lang.String", "String");
          return this;
      }
	public UpmsUserSQL andPasswordIsNull() {
		isnull("password");
		return this;
	}
	
	public UpmsUserSQL andPasswordIsNotNull() {
		notNull("password");
		return this;
	}
	
	public UpmsUserSQL andPasswordIsEmpty() {
		empty("password");
		return this;
	}

	public UpmsUserSQL andPasswordIsNotEmpty() {
		notEmpty("password");
		return this;
	}
       public UpmsUserSQL andPasswordLike(java.lang.String value) {
    	   addCriterion("password", value, ConditionMode.FUZZY, "password", "java.lang.String", "String");
    	   return this;
      }

      public UpmsUserSQL andPasswordNotLike(java.lang.String value) {
          addCriterion("password", value, ConditionMode.NOT_FUZZY, "password", "java.lang.String", "String");
          return this;
      }
      public UpmsUserSQL andPasswordEqualTo(java.lang.String value) {
          addCriterion("password", value, ConditionMode.EQUAL, "password", "java.lang.String", "String");
          return this;
      }

      public UpmsUserSQL andPasswordNotEqualTo(java.lang.String value) {
          addCriterion("password", value, ConditionMode.NOT_EQUAL, "password", "java.lang.String", "String");
          return this;
      }

      public UpmsUserSQL andPasswordGreaterThan(java.lang.String value) {
          addCriterion("password", value, ConditionMode.GREATER_THEN, "password", "java.lang.String", "String");
          return this;
      }

      public UpmsUserSQL andPasswordGreaterThanOrEqualTo(java.lang.String value) {
          addCriterion("password", value, ConditionMode.GREATER_EQUAL, "password", "java.lang.String", "String");
          return this;
      }

      public UpmsUserSQL andPasswordLessThan(java.lang.String value) {
          addCriterion("password", value, ConditionMode.LESS_THEN, "password", "java.lang.String", "String");
          return this;
      }

      public UpmsUserSQL andPasswordLessThanOrEqualTo(java.lang.String value) {
          addCriterion("password", value, ConditionMode.LESS_EQUAL, "password", "java.lang.String", "String");
          return this;
      }

      public UpmsUserSQL andPasswordBetween(java.lang.String value1, java.lang.String value2) {
    	  addCriterion("password", value1, value2, ConditionMode.BETWEEN, "password", "java.lang.String", "String");
    	  return this;
      }

      public UpmsUserSQL andPasswordNotBetween(java.lang.String value1, java.lang.String value2) {
          addCriterion("password", value1, value2, ConditionMode.NOT_BETWEEN, "password", "java.lang.String", "String");
          return this;
      }
        
      public UpmsUserSQL andPasswordIn(List<java.lang.String> values) {
          addCriterion("password", values, ConditionMode.IN, "password", "java.lang.String", "String");
          return this;
      }

      public UpmsUserSQL andPasswordNotIn(List<java.lang.String> values) {
          addCriterion("password", values, ConditionMode.NOT_IN, "password", "java.lang.String", "String");
          return this;
      }
	public UpmsUserSQL andSaltIsNull() {
		isnull("salt");
		return this;
	}
	
	public UpmsUserSQL andSaltIsNotNull() {
		notNull("salt");
		return this;
	}
	
	public UpmsUserSQL andSaltIsEmpty() {
		empty("salt");
		return this;
	}

	public UpmsUserSQL andSaltIsNotEmpty() {
		notEmpty("salt");
		return this;
	}
       public UpmsUserSQL andSaltLike(java.lang.String value) {
    	   addCriterion("salt", value, ConditionMode.FUZZY, "salt", "java.lang.String", "String");
    	   return this;
      }

      public UpmsUserSQL andSaltNotLike(java.lang.String value) {
          addCriterion("salt", value, ConditionMode.NOT_FUZZY, "salt", "java.lang.String", "String");
          return this;
      }
      public UpmsUserSQL andSaltEqualTo(java.lang.String value) {
          addCriterion("salt", value, ConditionMode.EQUAL, "salt", "java.lang.String", "String");
          return this;
      }

      public UpmsUserSQL andSaltNotEqualTo(java.lang.String value) {
          addCriterion("salt", value, ConditionMode.NOT_EQUAL, "salt", "java.lang.String", "String");
          return this;
      }

      public UpmsUserSQL andSaltGreaterThan(java.lang.String value) {
          addCriterion("salt", value, ConditionMode.GREATER_THEN, "salt", "java.lang.String", "String");
          return this;
      }

      public UpmsUserSQL andSaltGreaterThanOrEqualTo(java.lang.String value) {
          addCriterion("salt", value, ConditionMode.GREATER_EQUAL, "salt", "java.lang.String", "String");
          return this;
      }

      public UpmsUserSQL andSaltLessThan(java.lang.String value) {
          addCriterion("salt", value, ConditionMode.LESS_THEN, "salt", "java.lang.String", "String");
          return this;
      }

      public UpmsUserSQL andSaltLessThanOrEqualTo(java.lang.String value) {
          addCriterion("salt", value, ConditionMode.LESS_EQUAL, "salt", "java.lang.String", "String");
          return this;
      }

      public UpmsUserSQL andSaltBetween(java.lang.String value1, java.lang.String value2) {
    	  addCriterion("salt", value1, value2, ConditionMode.BETWEEN, "salt", "java.lang.String", "String");
    	  return this;
      }

      public UpmsUserSQL andSaltNotBetween(java.lang.String value1, java.lang.String value2) {
          addCriterion("salt", value1, value2, ConditionMode.NOT_BETWEEN, "salt", "java.lang.String", "String");
          return this;
      }
        
      public UpmsUserSQL andSaltIn(List<java.lang.String> values) {
          addCriterion("salt", values, ConditionMode.IN, "salt", "java.lang.String", "String");
          return this;
      }

      public UpmsUserSQL andSaltNotIn(List<java.lang.String> values) {
          addCriterion("salt", values, ConditionMode.NOT_IN, "salt", "java.lang.String", "String");
          return this;
      }
	public UpmsUserSQL andRealnameIsNull() {
		isnull("realname");
		return this;
	}
	
	public UpmsUserSQL andRealnameIsNotNull() {
		notNull("realname");
		return this;
	}
	
	public UpmsUserSQL andRealnameIsEmpty() {
		empty("realname");
		return this;
	}

	public UpmsUserSQL andRealnameIsNotEmpty() {
		notEmpty("realname");
		return this;
	}
       public UpmsUserSQL andRealnameLike(java.lang.String value) {
    	   addCriterion("realname", value, ConditionMode.FUZZY, "realname", "java.lang.String", "String");
    	   return this;
      }

      public UpmsUserSQL andRealnameNotLike(java.lang.String value) {
          addCriterion("realname", value, ConditionMode.NOT_FUZZY, "realname", "java.lang.String", "String");
          return this;
      }
      public UpmsUserSQL andRealnameEqualTo(java.lang.String value) {
          addCriterion("realname", value, ConditionMode.EQUAL, "realname", "java.lang.String", "String");
          return this;
      }

      public UpmsUserSQL andRealnameNotEqualTo(java.lang.String value) {
          addCriterion("realname", value, ConditionMode.NOT_EQUAL, "realname", "java.lang.String", "String");
          return this;
      }

      public UpmsUserSQL andRealnameGreaterThan(java.lang.String value) {
          addCriterion("realname", value, ConditionMode.GREATER_THEN, "realname", "java.lang.String", "String");
          return this;
      }

      public UpmsUserSQL andRealnameGreaterThanOrEqualTo(java.lang.String value) {
          addCriterion("realname", value, ConditionMode.GREATER_EQUAL, "realname", "java.lang.String", "String");
          return this;
      }

      public UpmsUserSQL andRealnameLessThan(java.lang.String value) {
          addCriterion("realname", value, ConditionMode.LESS_THEN, "realname", "java.lang.String", "String");
          return this;
      }

      public UpmsUserSQL andRealnameLessThanOrEqualTo(java.lang.String value) {
          addCriterion("realname", value, ConditionMode.LESS_EQUAL, "realname", "java.lang.String", "String");
          return this;
      }

      public UpmsUserSQL andRealnameBetween(java.lang.String value1, java.lang.String value2) {
    	  addCriterion("realname", value1, value2, ConditionMode.BETWEEN, "realname", "java.lang.String", "String");
    	  return this;
      }

      public UpmsUserSQL andRealnameNotBetween(java.lang.String value1, java.lang.String value2) {
          addCriterion("realname", value1, value2, ConditionMode.NOT_BETWEEN, "realname", "java.lang.String", "String");
          return this;
      }
        
      public UpmsUserSQL andRealnameIn(List<java.lang.String> values) {
          addCriterion("realname", values, ConditionMode.IN, "realname", "java.lang.String", "String");
          return this;
      }

      public UpmsUserSQL andRealnameNotIn(List<java.lang.String> values) {
          addCriterion("realname", values, ConditionMode.NOT_IN, "realname", "java.lang.String", "String");
          return this;
      }
	public UpmsUserSQL andAvatarIsNull() {
		isnull("avatar");
		return this;
	}
	
	public UpmsUserSQL andAvatarIsNotNull() {
		notNull("avatar");
		return this;
	}
	
	public UpmsUserSQL andAvatarIsEmpty() {
		empty("avatar");
		return this;
	}

	public UpmsUserSQL andAvatarIsNotEmpty() {
		notEmpty("avatar");
		return this;
	}
       public UpmsUserSQL andAvatarLike(java.lang.String value) {
    	   addCriterion("avatar", value, ConditionMode.FUZZY, "avatar", "java.lang.String", "String");
    	   return this;
      }

      public UpmsUserSQL andAvatarNotLike(java.lang.String value) {
          addCriterion("avatar", value, ConditionMode.NOT_FUZZY, "avatar", "java.lang.String", "String");
          return this;
      }
      public UpmsUserSQL andAvatarEqualTo(java.lang.String value) {
          addCriterion("avatar", value, ConditionMode.EQUAL, "avatar", "java.lang.String", "String");
          return this;
      }

      public UpmsUserSQL andAvatarNotEqualTo(java.lang.String value) {
          addCriterion("avatar", value, ConditionMode.NOT_EQUAL, "avatar", "java.lang.String", "String");
          return this;
      }

      public UpmsUserSQL andAvatarGreaterThan(java.lang.String value) {
          addCriterion("avatar", value, ConditionMode.GREATER_THEN, "avatar", "java.lang.String", "String");
          return this;
      }

      public UpmsUserSQL andAvatarGreaterThanOrEqualTo(java.lang.String value) {
          addCriterion("avatar", value, ConditionMode.GREATER_EQUAL, "avatar", "java.lang.String", "String");
          return this;
      }

      public UpmsUserSQL andAvatarLessThan(java.lang.String value) {
          addCriterion("avatar", value, ConditionMode.LESS_THEN, "avatar", "java.lang.String", "String");
          return this;
      }

      public UpmsUserSQL andAvatarLessThanOrEqualTo(java.lang.String value) {
          addCriterion("avatar", value, ConditionMode.LESS_EQUAL, "avatar", "java.lang.String", "String");
          return this;
      }

      public UpmsUserSQL andAvatarBetween(java.lang.String value1, java.lang.String value2) {
    	  addCriterion("avatar", value1, value2, ConditionMode.BETWEEN, "avatar", "java.lang.String", "String");
    	  return this;
      }

      public UpmsUserSQL andAvatarNotBetween(java.lang.String value1, java.lang.String value2) {
          addCriterion("avatar", value1, value2, ConditionMode.NOT_BETWEEN, "avatar", "java.lang.String", "String");
          return this;
      }
        
      public UpmsUserSQL andAvatarIn(List<java.lang.String> values) {
          addCriterion("avatar", values, ConditionMode.IN, "avatar", "java.lang.String", "String");
          return this;
      }

      public UpmsUserSQL andAvatarNotIn(List<java.lang.String> values) {
          addCriterion("avatar", values, ConditionMode.NOT_IN, "avatar", "java.lang.String", "String");
          return this;
      }
	public UpmsUserSQL andPhoneIsNull() {
		isnull("phone");
		return this;
	}
	
	public UpmsUserSQL andPhoneIsNotNull() {
		notNull("phone");
		return this;
	}
	
	public UpmsUserSQL andPhoneIsEmpty() {
		empty("phone");
		return this;
	}

	public UpmsUserSQL andPhoneIsNotEmpty() {
		notEmpty("phone");
		return this;
	}
       public UpmsUserSQL andPhoneLike(java.lang.String value) {
    	   addCriterion("phone", value, ConditionMode.FUZZY, "phone", "java.lang.String", "String");
    	   return this;
      }

      public UpmsUserSQL andPhoneNotLike(java.lang.String value) {
          addCriterion("phone", value, ConditionMode.NOT_FUZZY, "phone", "java.lang.String", "String");
          return this;
      }
      public UpmsUserSQL andPhoneEqualTo(java.lang.String value) {
          addCriterion("phone", value, ConditionMode.EQUAL, "phone", "java.lang.String", "String");
          return this;
      }

      public UpmsUserSQL andPhoneNotEqualTo(java.lang.String value) {
          addCriterion("phone", value, ConditionMode.NOT_EQUAL, "phone", "java.lang.String", "String");
          return this;
      }

      public UpmsUserSQL andPhoneGreaterThan(java.lang.String value) {
          addCriterion("phone", value, ConditionMode.GREATER_THEN, "phone", "java.lang.String", "String");
          return this;
      }

      public UpmsUserSQL andPhoneGreaterThanOrEqualTo(java.lang.String value) {
          addCriterion("phone", value, ConditionMode.GREATER_EQUAL, "phone", "java.lang.String", "String");
          return this;
      }

      public UpmsUserSQL andPhoneLessThan(java.lang.String value) {
          addCriterion("phone", value, ConditionMode.LESS_THEN, "phone", "java.lang.String", "String");
          return this;
      }

      public UpmsUserSQL andPhoneLessThanOrEqualTo(java.lang.String value) {
          addCriterion("phone", value, ConditionMode.LESS_EQUAL, "phone", "java.lang.String", "String");
          return this;
      }

      public UpmsUserSQL andPhoneBetween(java.lang.String value1, java.lang.String value2) {
    	  addCriterion("phone", value1, value2, ConditionMode.BETWEEN, "phone", "java.lang.String", "String");
    	  return this;
      }

      public UpmsUserSQL andPhoneNotBetween(java.lang.String value1, java.lang.String value2) {
          addCriterion("phone", value1, value2, ConditionMode.NOT_BETWEEN, "phone", "java.lang.String", "String");
          return this;
      }
        
      public UpmsUserSQL andPhoneIn(List<java.lang.String> values) {
          addCriterion("phone", values, ConditionMode.IN, "phone", "java.lang.String", "String");
          return this;
      }

      public UpmsUserSQL andPhoneNotIn(List<java.lang.String> values) {
          addCriterion("phone", values, ConditionMode.NOT_IN, "phone", "java.lang.String", "String");
          return this;
      }
	public UpmsUserSQL andEmailIsNull() {
		isnull("email");
		return this;
	}
	
	public UpmsUserSQL andEmailIsNotNull() {
		notNull("email");
		return this;
	}
	
	public UpmsUserSQL andEmailIsEmpty() {
		empty("email");
		return this;
	}

	public UpmsUserSQL andEmailIsNotEmpty() {
		notEmpty("email");
		return this;
	}
       public UpmsUserSQL andEmailLike(java.lang.String value) {
    	   addCriterion("email", value, ConditionMode.FUZZY, "email", "java.lang.String", "String");
    	   return this;
      }

      public UpmsUserSQL andEmailNotLike(java.lang.String value) {
          addCriterion("email", value, ConditionMode.NOT_FUZZY, "email", "java.lang.String", "String");
          return this;
      }
      public UpmsUserSQL andEmailEqualTo(java.lang.String value) {
          addCriterion("email", value, ConditionMode.EQUAL, "email", "java.lang.String", "String");
          return this;
      }

      public UpmsUserSQL andEmailNotEqualTo(java.lang.String value) {
          addCriterion("email", value, ConditionMode.NOT_EQUAL, "email", "java.lang.String", "String");
          return this;
      }

      public UpmsUserSQL andEmailGreaterThan(java.lang.String value) {
          addCriterion("email", value, ConditionMode.GREATER_THEN, "email", "java.lang.String", "String");
          return this;
      }

      public UpmsUserSQL andEmailGreaterThanOrEqualTo(java.lang.String value) {
          addCriterion("email", value, ConditionMode.GREATER_EQUAL, "email", "java.lang.String", "String");
          return this;
      }

      public UpmsUserSQL andEmailLessThan(java.lang.String value) {
          addCriterion("email", value, ConditionMode.LESS_THEN, "email", "java.lang.String", "String");
          return this;
      }

      public UpmsUserSQL andEmailLessThanOrEqualTo(java.lang.String value) {
          addCriterion("email", value, ConditionMode.LESS_EQUAL, "email", "java.lang.String", "String");
          return this;
      }

      public UpmsUserSQL andEmailBetween(java.lang.String value1, java.lang.String value2) {
    	  addCriterion("email", value1, value2, ConditionMode.BETWEEN, "email", "java.lang.String", "String");
    	  return this;
      }

      public UpmsUserSQL andEmailNotBetween(java.lang.String value1, java.lang.String value2) {
          addCriterion("email", value1, value2, ConditionMode.NOT_BETWEEN, "email", "java.lang.String", "String");
          return this;
      }
        
      public UpmsUserSQL andEmailIn(List<java.lang.String> values) {
          addCriterion("email", values, ConditionMode.IN, "email", "java.lang.String", "String");
          return this;
      }

      public UpmsUserSQL andEmailNotIn(List<java.lang.String> values) {
          addCriterion("email", values, ConditionMode.NOT_IN, "email", "java.lang.String", "String");
          return this;
      }
	public UpmsUserSQL andSexIsNull() {
		isnull("sex");
		return this;
	}
	
	public UpmsUserSQL andSexIsNotNull() {
		notNull("sex");
		return this;
	}
	
	public UpmsUserSQL andSexIsEmpty() {
		empty("sex");
		return this;
	}

	public UpmsUserSQL andSexIsNotEmpty() {
		notEmpty("sex");
		return this;
	}
      public UpmsUserSQL andSexEqualTo(java.lang.Integer value) {
          addCriterion("sex", value, ConditionMode.EQUAL, "sex", "java.lang.Integer", "Float");
          return this;
      }

      public UpmsUserSQL andSexNotEqualTo(java.lang.Integer value) {
          addCriterion("sex", value, ConditionMode.NOT_EQUAL, "sex", "java.lang.Integer", "Float");
          return this;
      }

      public UpmsUserSQL andSexGreaterThan(java.lang.Integer value) {
          addCriterion("sex", value, ConditionMode.GREATER_THEN, "sex", "java.lang.Integer", "Float");
          return this;
      }

      public UpmsUserSQL andSexGreaterThanOrEqualTo(java.lang.Integer value) {
          addCriterion("sex", value, ConditionMode.GREATER_EQUAL, "sex", "java.lang.Integer", "Float");
          return this;
      }

      public UpmsUserSQL andSexLessThan(java.lang.Integer value) {
          addCriterion("sex", value, ConditionMode.LESS_THEN, "sex", "java.lang.Integer", "Float");
          return this;
      }

      public UpmsUserSQL andSexLessThanOrEqualTo(java.lang.Integer value) {
          addCriterion("sex", value, ConditionMode.LESS_EQUAL, "sex", "java.lang.Integer", "Float");
          return this;
      }

      public UpmsUserSQL andSexBetween(java.lang.Integer value1, java.lang.Integer value2) {
    	  addCriterion("sex", value1, value2, ConditionMode.BETWEEN, "sex", "java.lang.Integer", "Float");
    	  return this;
      }

      public UpmsUserSQL andSexNotBetween(java.lang.Integer value1, java.lang.Integer value2) {
          addCriterion("sex", value1, value2, ConditionMode.NOT_BETWEEN, "sex", "java.lang.Integer", "Float");
          return this;
      }
        
      public UpmsUserSQL andSexIn(List<java.lang.Integer> values) {
          addCriterion("sex", values, ConditionMode.IN, "sex", "java.lang.Integer", "Float");
          return this;
      }

      public UpmsUserSQL andSexNotIn(List<java.lang.Integer> values) {
          addCriterion("sex", values, ConditionMode.NOT_IN, "sex", "java.lang.Integer", "Float");
          return this;
      }
	public UpmsUserSQL andLockedIsNull() {
		isnull("locked");
		return this;
	}
	
	public UpmsUserSQL andLockedIsNotNull() {
		notNull("locked");
		return this;
	}
	
	public UpmsUserSQL andLockedIsEmpty() {
		empty("locked");
		return this;
	}

	public UpmsUserSQL andLockedIsNotEmpty() {
		notEmpty("locked");
		return this;
	}
      public UpmsUserSQL andLockedEqualTo(java.lang.Integer value) {
          addCriterion("locked", value, ConditionMode.EQUAL, "locked", "java.lang.Integer", "Float");
          return this;
      }

      public UpmsUserSQL andLockedNotEqualTo(java.lang.Integer value) {
          addCriterion("locked", value, ConditionMode.NOT_EQUAL, "locked", "java.lang.Integer", "Float");
          return this;
      }

      public UpmsUserSQL andLockedGreaterThan(java.lang.Integer value) {
          addCriterion("locked", value, ConditionMode.GREATER_THEN, "locked", "java.lang.Integer", "Float");
          return this;
      }

      public UpmsUserSQL andLockedGreaterThanOrEqualTo(java.lang.Integer value) {
          addCriterion("locked", value, ConditionMode.GREATER_EQUAL, "locked", "java.lang.Integer", "Float");
          return this;
      }

      public UpmsUserSQL andLockedLessThan(java.lang.Integer value) {
          addCriterion("locked", value, ConditionMode.LESS_THEN, "locked", "java.lang.Integer", "Float");
          return this;
      }

      public UpmsUserSQL andLockedLessThanOrEqualTo(java.lang.Integer value) {
          addCriterion("locked", value, ConditionMode.LESS_EQUAL, "locked", "java.lang.Integer", "Float");
          return this;
      }

      public UpmsUserSQL andLockedBetween(java.lang.Integer value1, java.lang.Integer value2) {
    	  addCriterion("locked", value1, value2, ConditionMode.BETWEEN, "locked", "java.lang.Integer", "Float");
    	  return this;
      }

      public UpmsUserSQL andLockedNotBetween(java.lang.Integer value1, java.lang.Integer value2) {
          addCriterion("locked", value1, value2, ConditionMode.NOT_BETWEEN, "locked", "java.lang.Integer", "Float");
          return this;
      }
        
      public UpmsUserSQL andLockedIn(List<java.lang.Integer> values) {
          addCriterion("locked", values, ConditionMode.IN, "locked", "java.lang.Integer", "Float");
          return this;
      }

      public UpmsUserSQL andLockedNotIn(List<java.lang.Integer> values) {
          addCriterion("locked", values, ConditionMode.NOT_IN, "locked", "java.lang.Integer", "Float");
          return this;
      }
	public UpmsUserSQL andCtimeIsNull() {
		isnull("ctime");
		return this;
	}
	
	public UpmsUserSQL andCtimeIsNotNull() {
		notNull("ctime");
		return this;
	}
	
	public UpmsUserSQL andCtimeIsEmpty() {
		empty("ctime");
		return this;
	}

	public UpmsUserSQL andCtimeIsNotEmpty() {
		notEmpty("ctime");
		return this;
	}
      public UpmsUserSQL andCtimeEqualTo(java.lang.Long value) {
          addCriterion("ctime", value, ConditionMode.EQUAL, "ctime", "java.lang.Long", "Float");
          return this;
      }

      public UpmsUserSQL andCtimeNotEqualTo(java.lang.Long value) {
          addCriterion("ctime", value, ConditionMode.NOT_EQUAL, "ctime", "java.lang.Long", "Float");
          return this;
      }

      public UpmsUserSQL andCtimeGreaterThan(java.lang.Long value) {
          addCriterion("ctime", value, ConditionMode.GREATER_THEN, "ctime", "java.lang.Long", "Float");
          return this;
      }

      public UpmsUserSQL andCtimeGreaterThanOrEqualTo(java.lang.Long value) {
          addCriterion("ctime", value, ConditionMode.GREATER_EQUAL, "ctime", "java.lang.Long", "Float");
          return this;
      }

      public UpmsUserSQL andCtimeLessThan(java.lang.Long value) {
          addCriterion("ctime", value, ConditionMode.LESS_THEN, "ctime", "java.lang.Long", "Float");
          return this;
      }

      public UpmsUserSQL andCtimeLessThanOrEqualTo(java.lang.Long value) {
          addCriterion("ctime", value, ConditionMode.LESS_EQUAL, "ctime", "java.lang.Long", "Float");
          return this;
      }

      public UpmsUserSQL andCtimeBetween(java.lang.Long value1, java.lang.Long value2) {
    	  addCriterion("ctime", value1, value2, ConditionMode.BETWEEN, "ctime", "java.lang.Long", "Float");
    	  return this;
      }

      public UpmsUserSQL andCtimeNotBetween(java.lang.Long value1, java.lang.Long value2) {
          addCriterion("ctime", value1, value2, ConditionMode.NOT_BETWEEN, "ctime", "java.lang.Long", "Float");
          return this;
      }
        
      public UpmsUserSQL andCtimeIn(List<java.lang.Long> values) {
          addCriterion("ctime", values, ConditionMode.IN, "ctime", "java.lang.Long", "Float");
          return this;
      }

      public UpmsUserSQL andCtimeNotIn(List<java.lang.Long> values) {
          addCriterion("ctime", values, ConditionMode.NOT_IN, "ctime", "java.lang.Long", "Float");
          return this;
      }
	public UpmsUserSQL andPwLastTimeIsNull() {
		isnull("pw_last_time");
		return this;
	}
	
	public UpmsUserSQL andPwLastTimeIsNotNull() {
		notNull("pw_last_time");
		return this;
	}
	
	public UpmsUserSQL andPwLastTimeIsEmpty() {
		empty("pw_last_time");
		return this;
	}

	public UpmsUserSQL andPwLastTimeIsNotEmpty() {
		notEmpty("pw_last_time");
		return this;
	}
      public UpmsUserSQL andPwLastTimeEqualTo(java.util.Date value) {
          addCriterion("pw_last_time", value, ConditionMode.EQUAL, "pwLastTime", "java.util.Date", "String");
          return this;
      }

      public UpmsUserSQL andPwLastTimeNotEqualTo(java.util.Date value) {
          addCriterion("pw_last_time", value, ConditionMode.NOT_EQUAL, "pwLastTime", "java.util.Date", "String");
          return this;
      }

      public UpmsUserSQL andPwLastTimeGreaterThan(java.util.Date value) {
          addCriterion("pw_last_time", value, ConditionMode.GREATER_THEN, "pwLastTime", "java.util.Date", "String");
          return this;
      }

      public UpmsUserSQL andPwLastTimeGreaterThanOrEqualTo(java.util.Date value) {
          addCriterion("pw_last_time", value, ConditionMode.GREATER_EQUAL, "pwLastTime", "java.util.Date", "String");
          return this;
      }

      public UpmsUserSQL andPwLastTimeLessThan(java.util.Date value) {
          addCriterion("pw_last_time", value, ConditionMode.LESS_THEN, "pwLastTime", "java.util.Date", "String");
          return this;
      }

      public UpmsUserSQL andPwLastTimeLessThanOrEqualTo(java.util.Date value) {
          addCriterion("pw_last_time", value, ConditionMode.LESS_EQUAL, "pwLastTime", "java.util.Date", "String");
          return this;
      }

      public UpmsUserSQL andPwLastTimeBetween(java.util.Date value1, java.util.Date value2) {
    	  addCriterion("pw_last_time", value1, value2, ConditionMode.BETWEEN, "pwLastTime", "java.util.Date", "String");
    	  return this;
      }

      public UpmsUserSQL andPwLastTimeNotBetween(java.util.Date value1, java.util.Date value2) {
          addCriterion("pw_last_time", value1, value2, ConditionMode.NOT_BETWEEN, "pwLastTime", "java.util.Date", "String");
          return this;
      }
        
      public UpmsUserSQL andPwLastTimeIn(List<java.util.Date> values) {
          addCriterion("pw_last_time", values, ConditionMode.IN, "pwLastTime", "java.util.Date", "String");
          return this;
      }

      public UpmsUserSQL andPwLastTimeNotIn(List<java.util.Date> values) {
          addCriterion("pw_last_time", values, ConditionMode.NOT_IN, "pwLastTime", "java.util.Date", "String");
          return this;
      }
}