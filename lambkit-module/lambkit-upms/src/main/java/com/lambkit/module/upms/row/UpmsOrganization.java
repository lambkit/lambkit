/**
 * Copyright (c) 2015-2017, Henry Yang 杨勇 (gismail@foxmail.com).
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.lambkit.module.upms.row;

import com.lambkit.db.RowModel;

/**
 * @author yangyong 
 * @website: www.lambkit.com
 * @email: gismail@foxmail.com
 * @date ${date}
 * @version 1.0
 * @since 1.0
 */

public class UpmsOrganization extends RowModel<UpmsOrganization> {
	public UpmsOrganization() {
		setTableName("upms_organization");
		setPrimaryKey("organization_id");
	}
	public java.lang.Long getOrganizationId() {
		return this.get("organization_id");
	}
	public void setOrganizationId(java.lang.Long organizationId) {
		this.set("organization_id", organizationId);
	}
	public java.lang.Long getPid() {
		return this.get("pid");
	}
	public void setPid(java.lang.Long pid) {
		this.set("pid", pid);
	}
	public java.lang.String getName() {
		return this.get("name");
	}
	public void setName(java.lang.String name) {
		this.set("name", name);
	}
	public java.lang.String getDescription() {
		return this.get("description");
	}
	public void setDescription(java.lang.String description) {
		this.set("description", description);
	}
	public java.lang.Long getCtime() {
		return this.get("ctime");
	}
	public void setCtime(java.lang.Long ctime) {
		this.set("ctime", ctime);
	}
}
