package com.lambkit.module.upms.auth.cache;

import com.lambkit.LambkitConsts;
import com.lambkit.auth.cache.UserCacheBase;
import com.lambkit.core.Lambkit;
import com.lambkit.core.cache.ICache;
import com.lambkit.module.upms.UpmsConfig;
import com.lambkit.module.upms.UpmsConsts;

public class UpmsCache extends UserCacheBase {

	@Override
	public ICache getCache() {
		UpmsConfig config = Lambkit.config(UpmsConfig.class);
		if(config.getCacheType().equals(LambkitConsts.CACHE_REDIS)) {
			return Lambkit.getCache(LambkitConsts.CACHE_REDIS);
		} else if(config.getCacheType().equals(LambkitConsts.CACHE_EHCACHE)) {
			return Lambkit.getCache(LambkitConsts.CACHE_EHCACHE);
		} else {
			return Lambkit.getCache();
		}
	}

	/**
	 * // 判断是否已登录，如果已登录，则回跳，防止重复登录
	 * @param sessionId
	 * @return
	 */
	public String getSession(String sessionId) {
		return get(UpmsConsts.LAMBKIT_UPMS_SERVER_SESSION_ID + "_" + sessionId);
	}

	public String getCode(String sessionId, String userName) {
		//UUID.randomUUID().toString();
		return sessionId + "&" + userName;
	}

	public void saveSession(String sessionId, String userName, int timeOut) {
		// 全局会话sessionId列表，供会话管理, lpushOnRedis
		lpush(UpmsConsts.LAMBKIT_UPMS_SERVER_SESSION_IDS, sessionId.toString());
		// 默认验证帐号密码正确，创建code
		String code = getCode(sessionId, userName);
		// 全局会话的code, setOnRedis
		set(UpmsConsts.LAMBKIT_UPMS_SERVER_SESSION_ID + "_" + sessionId, code, timeOut);
		// code校验值, setOnRedis
		set(UpmsConsts.LAMBKIT_UPMS_SERVER_CODE + "_" + code, code, timeOut);//(int) timeout / 1000
	}

	public String getClientSession(String sessionId) {
		return get(UpmsConsts.LAMBKIT_UPMS_CLIENT_SESSION_ID + "_" + sessionId);
	}

	/**
	 * 更新code有效期
	 * @param sessionId
	 * @param cacheClientSession
	 * @param timeOut
	 */
	public void refreshClientSession(String sessionId, String cacheClientSession, int timeOut) {
		setex(UpmsConsts.LAMBKIT_UPMS_CLIENT_SESSION_ID + "_" + sessionId, cacheClientSession, timeOut);
		expire(UpmsConsts.LAMBKIT_UPMS_CLIENT_SESSION_IDS + "_" + cacheClientSession, timeOut);
	}

	/**
	 * code校验正确，创建局部会话；
	 * 保存code对应的局部会话sessionId，方便退出操作
	 * @param sessionId
	 * @param cacheClientSession
	 * @param timeOut
	 */
	public void saveClientSession(String sessionId, String cacheClientSession, int timeOut) {
		// code校验正确，创建局部会话, getRedisCache().redis()
		setex(UpmsConsts.LAMBKIT_UPMS_CLIENT_SESSION_ID + "_" + sessionId, cacheClientSession, timeOut);
		// 保存code对应的局部会话sessionId，方便退出操作, getRedisCache().redis().
		saddAndExpire(UpmsConsts.LAMBKIT_UPMS_CLIENT_SESSION_IDS + "_" + cacheClientSession, sessionId, timeOut);
	}

	public Long getClientNumber(String code) {
		return scard(UpmsConsts.LAMBKIT_UPMS_CLIENT_SESSION_IDS + "_" + code);
	}

	public String getCode(String code) {
		// 判断是否已登录，如果已登录，则回跳，防止重复登录
		return get(UpmsConsts.LAMBKIT_UPMS_SERVER_CODE + "_" + code);
	}
}
