/**
 * Copyright (c) 2015-2017, Henry Yang 杨勇 (gismail@foxmail.com).
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.lambkit.module.upms.row;

import com.lambkit.db.RowModel;

/**
 * @author yangyong 
 * @website: www.lambkit.com
 * @email: gismail@foxmail.com
 * @date ${date}
 * @version 1.0
 * @since 1.0
 */

public class UpmsRoleRule extends RowModel<UpmsRoleRule> {
	public UpmsRoleRule() {
		setTableName("upms_role_rule");
		setPrimaryKey("rule_role_id");
	}
	public java.lang.Long getRuleRoleId() {
		return this.get("rule_role_id");
	}
	public void setRuleRoleId(java.lang.Long ruleRoleId) {
		this.set("rule_role_id", ruleRoleId);
	}
	public java.lang.Long getRoleId() {
		return this.get("role_id");
	}
	public void setRoleId(java.lang.Long roleId) {
		this.set("role_id", roleId);
	}
	public java.lang.Long getRuleId() {
		return this.get("rule_id");
	}
	public void setRuleId(java.lang.Long ruleId) {
		this.set("rule_id", ruleId);
	}
	public java.util.Date getCreateTime() {
		return this.get("create_time");
	}
	public void setCreateTime(java.util.Date createTime) {
		this.set("create_time", createTime);
	}
}
