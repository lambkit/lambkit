/**
 * Copyright (c) 2015-2017, Henry Yang 杨勇 (gismail@foxmail.com).
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.lambkit.module.upms.sql;

import com.lambkit.db.sql.ConditionMode;
import com.lambkit.db.sql.Column;
import com.lambkit.db.sql.Columns;
import com.lambkit.db.sql.Example;

import java.io.Serializable;
import java.util.List;

/**
 * @author yangyong 
 * @website: www.lambkit.com
 * @email: gismail@foxmail.com
 * @date ${date}
 * @version 1.0
 * @since 1.0
 */

public class UpmsRolePermissionSQL extends Columns implements Serializable {
	private static final long serialVersionUID = 1L;
	
	public static UpmsRolePermissionSQL of() {
		return new UpmsRolePermissionSQL();
	}
	
	public static UpmsRolePermissionSQL by(Column column) {
		UpmsRolePermissionSQL that = new UpmsRolePermissionSQL();
		that.add(column);
        return that;
    }

    public static UpmsRolePermissionSQL by(String name, Object value) {
        return (UpmsRolePermissionSQL) create().eq(name, value);
    }
    
    public Example example() {
    	return Example.create("upms_role_permission", this);
    }
    
    /**
     * equals
     *
     * @param name
     * @param value
     * @return
     */
    public UpmsRolePermissionSQL eq(String name, Object value) {
    	super.eq(name, value);
        return this;
    }

    /**
     * not equals !=
     *
     * @param name
     * @param value
     * @return
     */
    public UpmsRolePermissionSQL ne(String name, Object value) {
    	super.ne(name, value);
        return this;
    }


    /**
     * like
     *
     * @param name
     * @param value
     * @return
     */

    public UpmsRolePermissionSQL like(String name, Object value) {
    	super.like(name, value);
        return this;
    }
    
    public UpmsRolePermissionSQL notLike(String name, Object value) {
    	super.notLike(name, value);
        return this;
    }

    /**
     * 大于 great than
     *
     * @param name
     * @param value
     * @return
     */
    public UpmsRolePermissionSQL gt(String name, Object value) {
    	super.gt(name, value);
        return this;
    }

    /**
     * 大于等于 great or equal
     *
     * @param name
     * @param value
     * @return
     */
    public UpmsRolePermissionSQL ge(String name, Object value) {
    	super.ge(name, value);
        return this;
    }

    /**
     * 小于 less than
     *
     * @param name
     * @param value
     * @return
     */
    public UpmsRolePermissionSQL lt(String name, Object value) {
    	super.lt(name, value);
        return this;
    }

    /**
     * 小于等于 less or equal
     *
     * @param name
     * @param value
     * @return
     */
    public UpmsRolePermissionSQL le(String name, Object value) {
    	super.le(name, value);
        return this;
    }
    
    public UpmsRolePermissionSQL isnull(String name) {
    	super.isnull(name);
        return this;
    } 

    public UpmsRolePermissionSQL notNull(String name) {
    	super.notNull(name);
        return this;
    } 
    
    public UpmsRolePermissionSQL empty(String name) {
    	super.empty(name);
        return this;
    } 
    
    public UpmsRolePermissionSQL notEmpty(String name) {
    	super.notEmpty(name);
        return this;
    } 
    
    public UpmsRolePermissionSQL add(Column column) {
    	super.add(column);
    	return this;
    }
    
    /**************************/
	
	public void addCriterion(String name, Object value, ConditionMode logic, String property, String typeHandler, String valueType) {
		 if (value == null) {
			 throw new RuntimeException("Value for " + property + " cannot be null");
		 }
		 add(Column.create(name, value, logic, typeHandler, valueType));
	}
   
	public void addCriterion(String name, Object value1, Object value2, ConditionMode logic, String property, String typeHandler, String valueType) {
		 if (value1 == null || value2 == null) {
			 throw new RuntimeException("Between values for " + property + " cannot be null");
		 }
		 add(Column.create(name, value1, value2, logic, typeHandler, valueType));
	}
		 
	public UpmsRolePermissionSQL andRolePermissionIdIsNull() {
		isnull("role_permission_id");
		return this;
	}
	
	public UpmsRolePermissionSQL andRolePermissionIdIsNotNull() {
		notNull("role_permission_id");
		return this;
	}
	
	public UpmsRolePermissionSQL andRolePermissionIdIsEmpty() {
		empty("role_permission_id");
		return this;
	}

	public UpmsRolePermissionSQL andRolePermissionIdIsNotEmpty() {
		notEmpty("role_permission_id");
		return this;
	}
      public UpmsRolePermissionSQL andRolePermissionIdEqualTo(java.lang.Long value) {
          addCriterion("role_permission_id", value, ConditionMode.EQUAL, "rolePermissionId", "java.lang.Long", "Float");
          return this;
      }

      public UpmsRolePermissionSQL andRolePermissionIdNotEqualTo(java.lang.Long value) {
          addCriterion("role_permission_id", value, ConditionMode.NOT_EQUAL, "rolePermissionId", "java.lang.Long", "Float");
          return this;
      }

      public UpmsRolePermissionSQL andRolePermissionIdGreaterThan(java.lang.Long value) {
          addCriterion("role_permission_id", value, ConditionMode.GREATER_THEN, "rolePermissionId", "java.lang.Long", "Float");
          return this;
      }

      public UpmsRolePermissionSQL andRolePermissionIdGreaterThanOrEqualTo(java.lang.Long value) {
          addCriterion("role_permission_id", value, ConditionMode.GREATER_EQUAL, "rolePermissionId", "java.lang.Long", "Float");
          return this;
      }

      public UpmsRolePermissionSQL andRolePermissionIdLessThan(java.lang.Long value) {
          addCriterion("role_permission_id", value, ConditionMode.LESS_THEN, "rolePermissionId", "java.lang.Long", "Float");
          return this;
      }

      public UpmsRolePermissionSQL andRolePermissionIdLessThanOrEqualTo(java.lang.Long value) {
          addCriterion("role_permission_id", value, ConditionMode.LESS_EQUAL, "rolePermissionId", "java.lang.Long", "Float");
          return this;
      }

      public UpmsRolePermissionSQL andRolePermissionIdBetween(java.lang.Long value1, java.lang.Long value2) {
    	  addCriterion("role_permission_id", value1, value2, ConditionMode.BETWEEN, "rolePermissionId", "java.lang.Long", "Float");
    	  return this;
      }

      public UpmsRolePermissionSQL andRolePermissionIdNotBetween(java.lang.Long value1, java.lang.Long value2) {
          addCriterion("role_permission_id", value1, value2, ConditionMode.NOT_BETWEEN, "rolePermissionId", "java.lang.Long", "Float");
          return this;
      }
        
      public UpmsRolePermissionSQL andRolePermissionIdIn(List<java.lang.Long> values) {
          addCriterion("role_permission_id", values, ConditionMode.IN, "rolePermissionId", "java.lang.Long", "Float");
          return this;
      }

      public UpmsRolePermissionSQL andRolePermissionIdNotIn(List<java.lang.Long> values) {
          addCriterion("role_permission_id", values, ConditionMode.NOT_IN, "rolePermissionId", "java.lang.Long", "Float");
          return this;
      }
	public UpmsRolePermissionSQL andRoleIdIsNull() {
		isnull("role_id");
		return this;
	}
	
	public UpmsRolePermissionSQL andRoleIdIsNotNull() {
		notNull("role_id");
		return this;
	}
	
	public UpmsRolePermissionSQL andRoleIdIsEmpty() {
		empty("role_id");
		return this;
	}

	public UpmsRolePermissionSQL andRoleIdIsNotEmpty() {
		notEmpty("role_id");
		return this;
	}
      public UpmsRolePermissionSQL andRoleIdEqualTo(java.lang.Long value) {
          addCriterion("role_id", value, ConditionMode.EQUAL, "roleId", "java.lang.Long", "Float");
          return this;
      }

      public UpmsRolePermissionSQL andRoleIdNotEqualTo(java.lang.Long value) {
          addCriterion("role_id", value, ConditionMode.NOT_EQUAL, "roleId", "java.lang.Long", "Float");
          return this;
      }

      public UpmsRolePermissionSQL andRoleIdGreaterThan(java.lang.Long value) {
          addCriterion("role_id", value, ConditionMode.GREATER_THEN, "roleId", "java.lang.Long", "Float");
          return this;
      }

      public UpmsRolePermissionSQL andRoleIdGreaterThanOrEqualTo(java.lang.Long value) {
          addCriterion("role_id", value, ConditionMode.GREATER_EQUAL, "roleId", "java.lang.Long", "Float");
          return this;
      }

      public UpmsRolePermissionSQL andRoleIdLessThan(java.lang.Long value) {
          addCriterion("role_id", value, ConditionMode.LESS_THEN, "roleId", "java.lang.Long", "Float");
          return this;
      }

      public UpmsRolePermissionSQL andRoleIdLessThanOrEqualTo(java.lang.Long value) {
          addCriterion("role_id", value, ConditionMode.LESS_EQUAL, "roleId", "java.lang.Long", "Float");
          return this;
      }

      public UpmsRolePermissionSQL andRoleIdBetween(java.lang.Long value1, java.lang.Long value2) {
    	  addCriterion("role_id", value1, value2, ConditionMode.BETWEEN, "roleId", "java.lang.Long", "Float");
    	  return this;
      }

      public UpmsRolePermissionSQL andRoleIdNotBetween(java.lang.Long value1, java.lang.Long value2) {
          addCriterion("role_id", value1, value2, ConditionMode.NOT_BETWEEN, "roleId", "java.lang.Long", "Float");
          return this;
      }
        
      public UpmsRolePermissionSQL andRoleIdIn(List<java.lang.Long> values) {
          addCriterion("role_id", values, ConditionMode.IN, "roleId", "java.lang.Long", "Float");
          return this;
      }

      public UpmsRolePermissionSQL andRoleIdNotIn(List<java.lang.Long> values) {
          addCriterion("role_id", values, ConditionMode.NOT_IN, "roleId", "java.lang.Long", "Float");
          return this;
      }
	public UpmsRolePermissionSQL andPermissionIdIsNull() {
		isnull("permission_id");
		return this;
	}
	
	public UpmsRolePermissionSQL andPermissionIdIsNotNull() {
		notNull("permission_id");
		return this;
	}
	
	public UpmsRolePermissionSQL andPermissionIdIsEmpty() {
		empty("permission_id");
		return this;
	}

	public UpmsRolePermissionSQL andPermissionIdIsNotEmpty() {
		notEmpty("permission_id");
		return this;
	}
      public UpmsRolePermissionSQL andPermissionIdEqualTo(java.lang.Long value) {
          addCriterion("permission_id", value, ConditionMode.EQUAL, "permissionId", "java.lang.Long", "Float");
          return this;
      }

      public UpmsRolePermissionSQL andPermissionIdNotEqualTo(java.lang.Long value) {
          addCriterion("permission_id", value, ConditionMode.NOT_EQUAL, "permissionId", "java.lang.Long", "Float");
          return this;
      }

      public UpmsRolePermissionSQL andPermissionIdGreaterThan(java.lang.Long value) {
          addCriterion("permission_id", value, ConditionMode.GREATER_THEN, "permissionId", "java.lang.Long", "Float");
          return this;
      }

      public UpmsRolePermissionSQL andPermissionIdGreaterThanOrEqualTo(java.lang.Long value) {
          addCriterion("permission_id", value, ConditionMode.GREATER_EQUAL, "permissionId", "java.lang.Long", "Float");
          return this;
      }

      public UpmsRolePermissionSQL andPermissionIdLessThan(java.lang.Long value) {
          addCriterion("permission_id", value, ConditionMode.LESS_THEN, "permissionId", "java.lang.Long", "Float");
          return this;
      }

      public UpmsRolePermissionSQL andPermissionIdLessThanOrEqualTo(java.lang.Long value) {
          addCriterion("permission_id", value, ConditionMode.LESS_EQUAL, "permissionId", "java.lang.Long", "Float");
          return this;
      }

      public UpmsRolePermissionSQL andPermissionIdBetween(java.lang.Long value1, java.lang.Long value2) {
    	  addCriterion("permission_id", value1, value2, ConditionMode.BETWEEN, "permissionId", "java.lang.Long", "Float");
    	  return this;
      }

      public UpmsRolePermissionSQL andPermissionIdNotBetween(java.lang.Long value1, java.lang.Long value2) {
          addCriterion("permission_id", value1, value2, ConditionMode.NOT_BETWEEN, "permissionId", "java.lang.Long", "Float");
          return this;
      }
        
      public UpmsRolePermissionSQL andPermissionIdIn(List<java.lang.Long> values) {
          addCriterion("permission_id", values, ConditionMode.IN, "permissionId", "java.lang.Long", "Float");
          return this;
      }

      public UpmsRolePermissionSQL andPermissionIdNotIn(List<java.lang.Long> values) {
          addCriterion("permission_id", values, ConditionMode.NOT_IN, "permissionId", "java.lang.Long", "Float");
          return this;
      }
}