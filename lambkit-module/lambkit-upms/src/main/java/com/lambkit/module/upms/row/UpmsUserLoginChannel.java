/**
 * Copyright (c) 2015-2017, Henry Yang 杨勇 (gismail@foxmail.com).
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.lambkit.module.upms.row;

import com.lambkit.db.RowModel;

/**
 * @author yangyong 
 * @website: www.lambkit.com
 * @email: gismail@foxmail.com
 * @date ${date}
 * @version 1.0
 * @since 1.0
 */

public class UpmsUserLoginChannel extends RowModel<UpmsUserLoginChannel> {
	public UpmsUserLoginChannel() {
		setTableName("upms_user_login_channel");
		setPrimaryKey("id");
	}
	public java.lang.Long getId() {
		return this.get("id");
	}
	public void setId(java.lang.Long id) {
		this.set("id", id);
	}
	public java.lang.Long getUserId() {
		return this.get("user_id");
	}
	public void setUserId(java.lang.Long userId) {
		this.set("user_id", userId);
	}
	public java.lang.String getPhone() {
		return this.get("phone");
	}
	public void setPhone(java.lang.String phone) {
		this.set("phone", phone);
	}
	public java.lang.Integer getType() {
		return this.get("type");
	}
	public void setType(java.lang.Integer type) {
		this.set("type", type);
	}
	public java.lang.String getUserName() {
		return this.get("user_name");
	}
	public void setUserName(java.lang.String userName) {
		this.set("user_name", userName);
	}
	public java.lang.String getUnionid() {
		return this.get("unionid");
	}
	public void setUnionid(java.lang.String unionid) {
		this.set("unionid", unionid);
	}
	public java.lang.String getOpenid() {
		return this.get("openid");
	}
	public void setOpenid(java.lang.String openid) {
		this.set("openid", openid);
	}
	public java.util.Date getCreateTime() {
		return this.get("create_time");
	}
	public void setCreateTime(java.util.Date createTime) {
		this.set("create_time", createTime);
	}
	public java.util.Date getUpdateTime() {
		return this.get("update_time");
	}
	public void setUpdateTime(java.util.Date updateTime) {
		this.set("update_time", updateTime);
	}
	public java.lang.Integer getStatus() {
		return this.get("status");
	}
	public void setStatus(java.lang.Integer status) {
		this.set("status", status);
	}
	public java.lang.Integer getDelFlag() {
		return this.get("del_flag");
	}
	public void setDelFlag(java.lang.Integer delFlag) {
		this.set("del_flag", delFlag);
	}
}
