/**
 * Copyright (c) 2015-2017, Henry Yang 杨勇 (gismail@foxmail.com).
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.lambkit.module.upms;

import com.lambkit.core.Lambkit;
import com.lambkit.core.LifecycleException;
import com.lambkit.module.LambkitModule;
import com.lambkit.module.upms.kernel.satoken.SaTokenPlugin;
import com.lambkit.module.upms.kernel.satoken.SaTokenUpmsKernel;

/**
 * @author yangyong(孤竹行)
 */
public class UpmsModule extends LambkitModule {
    @Override
    public void init() throws LifecycleException {
        super.init();
        UpmsConfig upmsConfig = Lambkit.config(UpmsConfig.class);
        if("satoken".equalsIgnoreCase(upmsConfig.getKernel())) {
            getAppContext().addPlugin(new SaTokenPlugin());
        }
        Upms.regist("satoken", new SaTokenUpmsKernel());
    }
}