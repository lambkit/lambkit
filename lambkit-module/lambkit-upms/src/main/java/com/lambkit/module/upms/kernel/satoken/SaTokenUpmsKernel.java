package com.lambkit.module.upms.kernel.satoken;

import com.lambkit.auth.service.AuthKernelService;
import com.lambkit.core.http.IController;
import com.lambkit.module.upms.auth.service.AbstractUpmsKernel;
import com.lambkit.module.upms.row.UpmsUser;

import java.lang.reflect.Method;

/**
 * @author yangyong(孤竹行)
 */
public class SaTokenUpmsKernel extends AbstractUpmsKernel {

    private SaTokenKernelService saTokenKernelService;

    public SaTokenUpmsKernel() {
        saTokenKernelService = new SaTokenKernelService();
    }

    @Override
    public String getName() {
        return "satoken";
    }

    @Override
    public AuthKernelService getAuthKernelService() {
        return saTokenKernelService;
    }

    @Override
    public int validation(Class clazz, Method method, IController controller, UpmsUser upmsUser) {
        return 1;
    }
}
