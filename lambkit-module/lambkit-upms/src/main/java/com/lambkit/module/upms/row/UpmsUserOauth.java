/**
 * Copyright (c) 2015-2017, Henry Yang 杨勇 (gismail@foxmail.com).
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.lambkit.module.upms.row;

import com.lambkit.db.RowModel;

/**
 * @author yangyong 
 * @website: www.lambkit.com
 * @email: gismail@foxmail.com
 * @date ${date}
 * @version 1.0
 * @since 1.0
 */

public class UpmsUserOauth extends RowModel<UpmsUserOauth> {
	public UpmsUserOauth() {
		setTableName("upms_user_oauth");
		setPrimaryKey("user_oauth_id");
	}
	public java.lang.Long getUserOauthId() {
		return this.get("user_oauth_id");
	}
	public void setUserOauthId(java.lang.Long userOauthId) {
		this.set("user_oauth_id", userOauthId);
	}
	public java.lang.Long getUserId() {
		return this.get("user_id");
	}
	public void setUserId(java.lang.Long userId) {
		this.set("user_id", userId);
	}
	public java.lang.Long getOauthId() {
		return this.get("oauth_id");
	}
	public void setOauthId(java.lang.Long oauthId) {
		this.set("oauth_id", oauthId);
	}
	public byte[] getOpenId() {
		return this.get("open_id");
	}
	public void setOpenId(byte[] openId) {
		this.set("open_id", openId);
	}
	public java.lang.Integer getStatus() {
		return this.get("status");
	}
	public void setStatus(java.lang.Integer status) {
		this.set("status", status);
	}
	public java.util.Date getCreateTime() {
		return this.get("create_time");
	}
	public void setCreateTime(java.util.Date createTime) {
		this.set("create_time", createTime);
	}
}
