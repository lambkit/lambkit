/**
 * Copyright (c) 2015-2017, Henry Yang 杨勇 (gismail@foxmail.com).
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.lambkit.module.upms.row;

import com.lambkit.db.RowModel;

/**
 * @author yangyong 
 * @website: www.lambkit.com
 * @email: gismail@foxmail.com
 * @date ${date}
 * @version 1.0
 * @since 1.0
 */

public class UpmsApp extends RowModel<UpmsApp> {
	public UpmsApp() {
		setTableName("upms_app");
		setPrimaryKey("id");
	}
	public java.lang.Integer getId() {
		return this.get("id");
	}
	public void setId(java.lang.Integer id) {
		this.set("id", id);
	}
	public java.lang.String getAppName() {
		return this.get("app_name");
	}
	public void setAppName(java.lang.String appName) {
		this.set("app_name", appName);
	}
	public java.lang.String getAppKey() {
		return this.get("app_key");
	}
	public void setAppKey(java.lang.String appKey) {
		this.set("app_key", appKey);
	}
	public java.lang.String getAppSecret() {
		return this.get("app_secret");
	}
	public void setAppSecret(java.lang.String appSecret) {
		this.set("app_secret", appSecret);
	}
	public java.lang.String getAccessToken() {
		return this.get("access_token");
	}
	public void setAccessToken(java.lang.String accessToken) {
		this.set("access_token", accessToken);
	}
	public java.lang.Integer getStatus() {
		return this.get("status");
	}
	public void setStatus(java.lang.Integer status) {
		this.set("status", status);
	}
	public java.util.Date getCreated() {
		return this.get("created");
	}
	public void setCreated(java.util.Date created) {
		this.set("created", created);
	}
	public java.lang.String getAppId() {
		return this.get("app_id");
	}
	public void setAppId(java.lang.String appId) {
		this.set("app_id", appId);
	}
}
