package com.lambkit.module.upms.auth.service;


import com.lambkit.auth.AuthUser;
import com.lambkit.auth.service.AuthDataService;
import com.lambkit.auth.service.AuthKernelService;
import com.lambkit.core.Lambkit;
import com.lambkit.module.upms.UpmsApiService;
import com.lambkit.module.upms.row.UpmsUser;
import com.lambkit.web.HttpContextHolder;

/**
 * @author yangyong(孤竹行)
 */
public abstract class AbstractAuthKernelService implements AuthKernelService {

    protected AuthDataService getAuthDataService() {
        return Lambkit.get(UpmsDataService.class);
    }

    @Override
    public void clearCachedAuth(String username) {

    }

    @Override
    public Boolean authenticated() {
        UpmsSessionService sessionService = Lambkit.get(UpmsSessionService.class);
        String sessionId = sessionService.getId(HttpContextHolder.get().getRequest());
        AuthDataService dataService = getAuthDataService();
        AuthUser authUser = dataService.getCache().validate(sessionId);
        if(authUser!=null) {
            return true;
        }
        return false;
    }

    @Override
    public Boolean notAuthenticated() {
        return !authenticated();
    }

    @Override
    public Boolean user() {
        return authenticated();
    }

    @Override
    public Boolean guest() {
        return notAuthenticated();
    }

    @Override
    public Boolean isGuestRule(String permission) {
        UpmsApiService upmsApiService = Lambkit.get(UpmsApiService.class);
        return upmsApiService.isGuestRule(permission);
    }

    private String getSessionId() {
        UpmsSessionService sessionService = Lambkit.get(UpmsSessionService.class);
        String sessionId = sessionService.getId(HttpContextHolder.get().getRequest());
        return sessionId;
    }


    private String getUserName() {
        return authenticate(getSessionId());
    }

    private UpmsUser getUpmsUser() {
        String username = getUserName();
        return Lambkit.get(UpmsApiService.class).selectUpmsUserByUsername(username);
    }
    @Override
    public Boolean hasRole(Long roleid) {
        UpmsUser upmsUserModel = getUpmsUser();
        if(upmsUserModel==null) {
            return false;
        }
        return getAuthDataService().hasRole(upmsUserModel.getUserId(), roleid);
    }

    @Override
    public Boolean hasRole(String roleName) {
        UpmsUser upmsUserModel = getUpmsUser();
        if(upmsUserModel==null) {
            return false;
        }
        return getAuthDataService().hasRole(upmsUserModel.getUserId(), roleName);
    }

    @Override
    public Boolean lacksRole(String roleName) {
        return getAuthDataService().lacksRole(roleName);
    }

    @Override
    public Boolean hasAnyRoles(String roleNames) {
        UpmsUser upmsUserModel = getUpmsUser();
        if(upmsUserModel==null) {
            return false;
        }
        return getAuthDataService().hasAnyRoles(upmsUserModel.getUserId(), roleNames);
    }

    @Override
    public Boolean hasAllRoles(String roleNames) {
        UpmsUser upmsUserModel = getUpmsUser();
        if(upmsUserModel==null) {
            return false;
        }
        return getAuthDataService().hasAllRoles(upmsUserModel.getUserId(), roleNames);
    }

    @Override
    public Boolean hasRule(Long ruleid) {
        UpmsUser upmsUserModel = getUpmsUser();
        if(upmsUserModel==null) {
            return false;
        }
        return getAuthDataService().hasRule(upmsUserModel.getUserId(), ruleid);
    }

    @Override
    public Boolean hasRule(String permission) {
        UpmsUser upmsUserModel = getUpmsUser();
        if(upmsUserModel==null) {
            return false;
        }
        return getAuthDataService().hasRule(upmsUserModel.getUserId(), permission);
    }

    @Override
    public Boolean lacksRule(String permission) {
        return getAuthDataService().lacksRule(permission);
    }

    @Override
    public Boolean hasAnyRules(String permissions) {
        UpmsUser upmsUserModel = getUpmsUser();
        if(upmsUserModel==null) {
            return false;
        }
        return getAuthDataService().hasAnyRules(upmsUserModel.getUserId(), permissions);
    }

    @Override
    public Boolean hasAllRules(String permissions) {
        UpmsUser upmsUserModel = getUpmsUser();
        if(upmsUserModel==null) {
            return false;
        }
        return getAuthDataService().hasAllRules(upmsUserModel.getUserId(), permissions);
    }
}
