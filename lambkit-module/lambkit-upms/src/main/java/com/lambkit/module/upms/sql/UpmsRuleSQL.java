/**
 * Copyright (c) 2015-2017, Henry Yang 杨勇 (gismail@foxmail.com).
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.lambkit.module.upms.sql;

import com.lambkit.db.sql.ConditionMode;
import com.lambkit.db.sql.Column;
import com.lambkit.db.sql.Columns;
import com.lambkit.db.sql.Example;

import java.io.Serializable;
import java.util.List;

/**
 * @author yangyong 
 * @website: www.lambkit.com
 * @email: gismail@foxmail.com
 * @date ${date}
 * @version 1.0
 * @since 1.0
 */

public class UpmsRuleSQL extends Columns implements Serializable {
	private static final long serialVersionUID = 1L;
	
	public static UpmsRuleSQL of() {
		return new UpmsRuleSQL();
	}
	
	public static UpmsRuleSQL by(Column column) {
		UpmsRuleSQL that = new UpmsRuleSQL();
		that.add(column);
        return that;
    }

    public static UpmsRuleSQL by(String name, Object value) {
        return (UpmsRuleSQL) create().eq(name, value);
    }
    
    public Example example() {
    	return Example.create("upms_rule", this);
    }
    
    /**
     * equals
     *
     * @param name
     * @param value
     * @return
     */
    public UpmsRuleSQL eq(String name, Object value) {
    	super.eq(name, value);
        return this;
    }

    /**
     * not equals !=
     *
     * @param name
     * @param value
     * @return
     */
    public UpmsRuleSQL ne(String name, Object value) {
    	super.ne(name, value);
        return this;
    }


    /**
     * like
     *
     * @param name
     * @param value
     * @return
     */

    public UpmsRuleSQL like(String name, Object value) {
    	super.like(name, value);
        return this;
    }
    
    public UpmsRuleSQL notLike(String name, Object value) {
    	super.notLike(name, value);
        return this;
    }

    /**
     * 大于 great than
     *
     * @param name
     * @param value
     * @return
     */
    public UpmsRuleSQL gt(String name, Object value) {
    	super.gt(name, value);
        return this;
    }

    /**
     * 大于等于 great or equal
     *
     * @param name
     * @param value
     * @return
     */
    public UpmsRuleSQL ge(String name, Object value) {
    	super.ge(name, value);
        return this;
    }

    /**
     * 小于 less than
     *
     * @param name
     * @param value
     * @return
     */
    public UpmsRuleSQL lt(String name, Object value) {
    	super.lt(name, value);
        return this;
    }

    /**
     * 小于等于 less or equal
     *
     * @param name
     * @param value
     * @return
     */
    public UpmsRuleSQL le(String name, Object value) {
    	super.le(name, value);
        return this;
    }
    
    public UpmsRuleSQL isnull(String name) {
    	super.isnull(name);
        return this;
    } 

    public UpmsRuleSQL notNull(String name) {
    	super.notNull(name);
        return this;
    } 
    
    public UpmsRuleSQL empty(String name) {
    	super.empty(name);
        return this;
    } 
    
    public UpmsRuleSQL notEmpty(String name) {
    	super.notEmpty(name);
        return this;
    } 
    
    public UpmsRuleSQL add(Column column) {
    	super.add(column);
    	return this;
    }
    
    /**************************/
	
	public void addCriterion(String name, Object value, ConditionMode logic, String property, String typeHandler, String valueType) {
		 if (value == null) {
			 throw new RuntimeException("Value for " + property + " cannot be null");
		 }
		 add(Column.create(name, value, logic, typeHandler, valueType));
	}
   
	public void addCriterion(String name, Object value1, Object value2, ConditionMode logic, String property, String typeHandler, String valueType) {
		 if (value1 == null || value2 == null) {
			 throw new RuntimeException("Between values for " + property + " cannot be null");
		 }
		 add(Column.create(name, value1, value2, logic, typeHandler, valueType));
	}
		 
	public UpmsRuleSQL andIdIsNull() {
		isnull("id");
		return this;
	}
	
	public UpmsRuleSQL andIdIsNotNull() {
		notNull("id");
		return this;
	}
	
	public UpmsRuleSQL andIdIsEmpty() {
		empty("id");
		return this;
	}

	public UpmsRuleSQL andIdIsNotEmpty() {
		notEmpty("id");
		return this;
	}
      public UpmsRuleSQL andIdEqualTo(java.lang.Long value) {
          addCriterion("id", value, ConditionMode.EQUAL, "id", "java.lang.Long", "Float");
          return this;
      }

      public UpmsRuleSQL andIdNotEqualTo(java.lang.Long value) {
          addCriterion("id", value, ConditionMode.NOT_EQUAL, "id", "java.lang.Long", "Float");
          return this;
      }

      public UpmsRuleSQL andIdGreaterThan(java.lang.Long value) {
          addCriterion("id", value, ConditionMode.GREATER_THEN, "id", "java.lang.Long", "Float");
          return this;
      }

      public UpmsRuleSQL andIdGreaterThanOrEqualTo(java.lang.Long value) {
          addCriterion("id", value, ConditionMode.GREATER_EQUAL, "id", "java.lang.Long", "Float");
          return this;
      }

      public UpmsRuleSQL andIdLessThan(java.lang.Long value) {
          addCriterion("id", value, ConditionMode.LESS_THEN, "id", "java.lang.Long", "Float");
          return this;
      }

      public UpmsRuleSQL andIdLessThanOrEqualTo(java.lang.Long value) {
          addCriterion("id", value, ConditionMode.LESS_EQUAL, "id", "java.lang.Long", "Float");
          return this;
      }

      public UpmsRuleSQL andIdBetween(java.lang.Long value1, java.lang.Long value2) {
    	  addCriterion("id", value1, value2, ConditionMode.BETWEEN, "id", "java.lang.Long", "Float");
    	  return this;
      }

      public UpmsRuleSQL andIdNotBetween(java.lang.Long value1, java.lang.Long value2) {
          addCriterion("id", value1, value2, ConditionMode.NOT_BETWEEN, "id", "java.lang.Long", "Float");
          return this;
      }
        
      public UpmsRuleSQL andIdIn(List<java.lang.Long> values) {
          addCriterion("id", values, ConditionMode.IN, "id", "java.lang.Long", "Float");
          return this;
      }

      public UpmsRuleSQL andIdNotIn(List<java.lang.Long> values) {
          addCriterion("id", values, ConditionMode.NOT_IN, "id", "java.lang.Long", "Float");
          return this;
      }
	public UpmsRuleSQL andRuleNameIsNull() {
		isnull("rule_name");
		return this;
	}
	
	public UpmsRuleSQL andRuleNameIsNotNull() {
		notNull("rule_name");
		return this;
	}
	
	public UpmsRuleSQL andRuleNameIsEmpty() {
		empty("rule_name");
		return this;
	}

	public UpmsRuleSQL andRuleNameIsNotEmpty() {
		notEmpty("rule_name");
		return this;
	}
       public UpmsRuleSQL andRuleNameLike(java.lang.String value) {
    	   addCriterion("rule_name", value, ConditionMode.FUZZY, "ruleName", "java.lang.String", "Float");
    	   return this;
      }

      public UpmsRuleSQL andRuleNameNotLike(java.lang.String value) {
          addCriterion("rule_name", value, ConditionMode.NOT_FUZZY, "ruleName", "java.lang.String", "Float");
          return this;
      }
      public UpmsRuleSQL andRuleNameEqualTo(java.lang.String value) {
          addCriterion("rule_name", value, ConditionMode.EQUAL, "ruleName", "java.lang.String", "String");
          return this;
      }

      public UpmsRuleSQL andRuleNameNotEqualTo(java.lang.String value) {
          addCriterion("rule_name", value, ConditionMode.NOT_EQUAL, "ruleName", "java.lang.String", "String");
          return this;
      }

      public UpmsRuleSQL andRuleNameGreaterThan(java.lang.String value) {
          addCriterion("rule_name", value, ConditionMode.GREATER_THEN, "ruleName", "java.lang.String", "String");
          return this;
      }

      public UpmsRuleSQL andRuleNameGreaterThanOrEqualTo(java.lang.String value) {
          addCriterion("rule_name", value, ConditionMode.GREATER_EQUAL, "ruleName", "java.lang.String", "String");
          return this;
      }

      public UpmsRuleSQL andRuleNameLessThan(java.lang.String value) {
          addCriterion("rule_name", value, ConditionMode.LESS_THEN, "ruleName", "java.lang.String", "String");
          return this;
      }

      public UpmsRuleSQL andRuleNameLessThanOrEqualTo(java.lang.String value) {
          addCriterion("rule_name", value, ConditionMode.LESS_EQUAL, "ruleName", "java.lang.String", "String");
          return this;
      }

      public UpmsRuleSQL andRuleNameBetween(java.lang.String value1, java.lang.String value2) {
    	  addCriterion("rule_name", value1, value2, ConditionMode.BETWEEN, "ruleName", "java.lang.String", "String");
    	  return this;
      }

      public UpmsRuleSQL andRuleNameNotBetween(java.lang.String value1, java.lang.String value2) {
          addCriterion("rule_name", value1, value2, ConditionMode.NOT_BETWEEN, "ruleName", "java.lang.String", "String");
          return this;
      }
        
      public UpmsRuleSQL andRuleNameIn(List<java.lang.String> values) {
          addCriterion("rule_name", values, ConditionMode.IN, "ruleName", "java.lang.String", "String");
          return this;
      }

      public UpmsRuleSQL andRuleNameNotIn(List<java.lang.String> values) {
          addCriterion("rule_name", values, ConditionMode.NOT_IN, "ruleName", "java.lang.String", "String");
          return this;
      }
	public UpmsRuleSQL andRuleCodeIsNull() {
		isnull("rule_code");
		return this;
	}
	
	public UpmsRuleSQL andRuleCodeIsNotNull() {
		notNull("rule_code");
		return this;
	}
	
	public UpmsRuleSQL andRuleCodeIsEmpty() {
		empty("rule_code");
		return this;
	}

	public UpmsRuleSQL andRuleCodeIsNotEmpty() {
		notEmpty("rule_code");
		return this;
	}
       public UpmsRuleSQL andRuleCodeLike(java.lang.String value) {
    	   addCriterion("rule_code", value, ConditionMode.FUZZY, "ruleCode", "java.lang.String", "String");
    	   return this;
      }

      public UpmsRuleSQL andRuleCodeNotLike(java.lang.String value) {
          addCriterion("rule_code", value, ConditionMode.NOT_FUZZY, "ruleCode", "java.lang.String", "String");
          return this;
      }
      public UpmsRuleSQL andRuleCodeEqualTo(java.lang.String value) {
          addCriterion("rule_code", value, ConditionMode.EQUAL, "ruleCode", "java.lang.String", "String");
          return this;
      }

      public UpmsRuleSQL andRuleCodeNotEqualTo(java.lang.String value) {
          addCriterion("rule_code", value, ConditionMode.NOT_EQUAL, "ruleCode", "java.lang.String", "String");
          return this;
      }

      public UpmsRuleSQL andRuleCodeGreaterThan(java.lang.String value) {
          addCriterion("rule_code", value, ConditionMode.GREATER_THEN, "ruleCode", "java.lang.String", "String");
          return this;
      }

      public UpmsRuleSQL andRuleCodeGreaterThanOrEqualTo(java.lang.String value) {
          addCriterion("rule_code", value, ConditionMode.GREATER_EQUAL, "ruleCode", "java.lang.String", "String");
          return this;
      }

      public UpmsRuleSQL andRuleCodeLessThan(java.lang.String value) {
          addCriterion("rule_code", value, ConditionMode.LESS_THEN, "ruleCode", "java.lang.String", "String");
          return this;
      }

      public UpmsRuleSQL andRuleCodeLessThanOrEqualTo(java.lang.String value) {
          addCriterion("rule_code", value, ConditionMode.LESS_EQUAL, "ruleCode", "java.lang.String", "String");
          return this;
      }

      public UpmsRuleSQL andRuleCodeBetween(java.lang.String value1, java.lang.String value2) {
    	  addCriterion("rule_code", value1, value2, ConditionMode.BETWEEN, "ruleCode", "java.lang.String", "String");
    	  return this;
      }

      public UpmsRuleSQL andRuleCodeNotBetween(java.lang.String value1, java.lang.String value2) {
          addCriterion("rule_code", value1, value2, ConditionMode.NOT_BETWEEN, "ruleCode", "java.lang.String", "String");
          return this;
      }
        
      public UpmsRuleSQL andRuleCodeIn(List<java.lang.String> values) {
          addCriterion("rule_code", values, ConditionMode.IN, "ruleCode", "java.lang.String", "String");
          return this;
      }

      public UpmsRuleSQL andRuleCodeNotIn(List<java.lang.String> values) {
          addCriterion("rule_code", values, ConditionMode.NOT_IN, "ruleCode", "java.lang.String", "String");
          return this;
      }
	public UpmsRuleSQL andCreateTimeIsNull() {
		isnull("create_time");
		return this;
	}
	
	public UpmsRuleSQL andCreateTimeIsNotNull() {
		notNull("create_time");
		return this;
	}
	
	public UpmsRuleSQL andCreateTimeIsEmpty() {
		empty("create_time");
		return this;
	}

	public UpmsRuleSQL andCreateTimeIsNotEmpty() {
		notEmpty("create_time");
		return this;
	}
      public UpmsRuleSQL andCreateTimeEqualTo(java.util.Date value) {
          addCriterion("create_time", value, ConditionMode.EQUAL, "createTime", "java.util.Date", "String");
          return this;
      }

      public UpmsRuleSQL andCreateTimeNotEqualTo(java.util.Date value) {
          addCriterion("create_time", value, ConditionMode.NOT_EQUAL, "createTime", "java.util.Date", "String");
          return this;
      }

      public UpmsRuleSQL andCreateTimeGreaterThan(java.util.Date value) {
          addCriterion("create_time", value, ConditionMode.GREATER_THEN, "createTime", "java.util.Date", "String");
          return this;
      }

      public UpmsRuleSQL andCreateTimeGreaterThanOrEqualTo(java.util.Date value) {
          addCriterion("create_time", value, ConditionMode.GREATER_EQUAL, "createTime", "java.util.Date", "String");
          return this;
      }

      public UpmsRuleSQL andCreateTimeLessThan(java.util.Date value) {
          addCriterion("create_time", value, ConditionMode.LESS_THEN, "createTime", "java.util.Date", "String");
          return this;
      }

      public UpmsRuleSQL andCreateTimeLessThanOrEqualTo(java.util.Date value) {
          addCriterion("create_time", value, ConditionMode.LESS_EQUAL, "createTime", "java.util.Date", "String");
          return this;
      }

      public UpmsRuleSQL andCreateTimeBetween(java.util.Date value1, java.util.Date value2) {
    	  addCriterion("create_time", value1, value2, ConditionMode.BETWEEN, "createTime", "java.util.Date", "String");
    	  return this;
      }

      public UpmsRuleSQL andCreateTimeNotBetween(java.util.Date value1, java.util.Date value2) {
          addCriterion("create_time", value1, value2, ConditionMode.NOT_BETWEEN, "createTime", "java.util.Date", "String");
          return this;
      }
        
      public UpmsRuleSQL andCreateTimeIn(List<java.util.Date> values) {
          addCriterion("create_time", values, ConditionMode.IN, "createTime", "java.util.Date", "String");
          return this;
      }

      public UpmsRuleSQL andCreateTimeNotIn(List<java.util.Date> values) {
          addCriterion("create_time", values, ConditionMode.NOT_IN, "createTime", "java.util.Date", "String");
          return this;
      }
	public UpmsRuleSQL andUpdateTimeIsNull() {
		isnull("update_time");
		return this;
	}
	
	public UpmsRuleSQL andUpdateTimeIsNotNull() {
		notNull("update_time");
		return this;
	}
	
	public UpmsRuleSQL andUpdateTimeIsEmpty() {
		empty("update_time");
		return this;
	}

	public UpmsRuleSQL andUpdateTimeIsNotEmpty() {
		notEmpty("update_time");
		return this;
	}
      public UpmsRuleSQL andUpdateTimeEqualTo(java.util.Date value) {
          addCriterion("update_time", value, ConditionMode.EQUAL, "updateTime", "java.util.Date", "String");
          return this;
      }

      public UpmsRuleSQL andUpdateTimeNotEqualTo(java.util.Date value) {
          addCriterion("update_time", value, ConditionMode.NOT_EQUAL, "updateTime", "java.util.Date", "String");
          return this;
      }

      public UpmsRuleSQL andUpdateTimeGreaterThan(java.util.Date value) {
          addCriterion("update_time", value, ConditionMode.GREATER_THEN, "updateTime", "java.util.Date", "String");
          return this;
      }

      public UpmsRuleSQL andUpdateTimeGreaterThanOrEqualTo(java.util.Date value) {
          addCriterion("update_time", value, ConditionMode.GREATER_EQUAL, "updateTime", "java.util.Date", "String");
          return this;
      }

      public UpmsRuleSQL andUpdateTimeLessThan(java.util.Date value) {
          addCriterion("update_time", value, ConditionMode.LESS_THEN, "updateTime", "java.util.Date", "String");
          return this;
      }

      public UpmsRuleSQL andUpdateTimeLessThanOrEqualTo(java.util.Date value) {
          addCriterion("update_time", value, ConditionMode.LESS_EQUAL, "updateTime", "java.util.Date", "String");
          return this;
      }

      public UpmsRuleSQL andUpdateTimeBetween(java.util.Date value1, java.util.Date value2) {
    	  addCriterion("update_time", value1, value2, ConditionMode.BETWEEN, "updateTime", "java.util.Date", "String");
    	  return this;
      }

      public UpmsRuleSQL andUpdateTimeNotBetween(java.util.Date value1, java.util.Date value2) {
          addCriterion("update_time", value1, value2, ConditionMode.NOT_BETWEEN, "updateTime", "java.util.Date", "String");
          return this;
      }
        
      public UpmsRuleSQL andUpdateTimeIn(List<java.util.Date> values) {
          addCriterion("update_time", values, ConditionMode.IN, "updateTime", "java.util.Date", "String");
          return this;
      }

      public UpmsRuleSQL andUpdateTimeNotIn(List<java.util.Date> values) {
          addCriterion("update_time", values, ConditionMode.NOT_IN, "updateTime", "java.util.Date", "String");
          return this;
      }
	public UpmsRuleSQL andRemarksIsNull() {
		isnull("remarks");
		return this;
	}
	
	public UpmsRuleSQL andRemarksIsNotNull() {
		notNull("remarks");
		return this;
	}
	
	public UpmsRuleSQL andRemarksIsEmpty() {
		empty("remarks");
		return this;
	}

	public UpmsRuleSQL andRemarksIsNotEmpty() {
		notEmpty("remarks");
		return this;
	}
       public UpmsRuleSQL andRemarksLike(java.lang.String value) {
    	   addCriterion("remarks", value, ConditionMode.FUZZY, "remarks", "java.lang.String", "String");
    	   return this;
      }

      public UpmsRuleSQL andRemarksNotLike(java.lang.String value) {
          addCriterion("remarks", value, ConditionMode.NOT_FUZZY, "remarks", "java.lang.String", "String");
          return this;
      }
      public UpmsRuleSQL andRemarksEqualTo(java.lang.String value) {
          addCriterion("remarks", value, ConditionMode.EQUAL, "remarks", "java.lang.String", "String");
          return this;
      }

      public UpmsRuleSQL andRemarksNotEqualTo(java.lang.String value) {
          addCriterion("remarks", value, ConditionMode.NOT_EQUAL, "remarks", "java.lang.String", "String");
          return this;
      }

      public UpmsRuleSQL andRemarksGreaterThan(java.lang.String value) {
          addCriterion("remarks", value, ConditionMode.GREATER_THEN, "remarks", "java.lang.String", "String");
          return this;
      }

      public UpmsRuleSQL andRemarksGreaterThanOrEqualTo(java.lang.String value) {
          addCriterion("remarks", value, ConditionMode.GREATER_EQUAL, "remarks", "java.lang.String", "String");
          return this;
      }

      public UpmsRuleSQL andRemarksLessThan(java.lang.String value) {
          addCriterion("remarks", value, ConditionMode.LESS_THEN, "remarks", "java.lang.String", "String");
          return this;
      }

      public UpmsRuleSQL andRemarksLessThanOrEqualTo(java.lang.String value) {
          addCriterion("remarks", value, ConditionMode.LESS_EQUAL, "remarks", "java.lang.String", "String");
          return this;
      }

      public UpmsRuleSQL andRemarksBetween(java.lang.String value1, java.lang.String value2) {
    	  addCriterion("remarks", value1, value2, ConditionMode.BETWEEN, "remarks", "java.lang.String", "String");
    	  return this;
      }

      public UpmsRuleSQL andRemarksNotBetween(java.lang.String value1, java.lang.String value2) {
          addCriterion("remarks", value1, value2, ConditionMode.NOT_BETWEEN, "remarks", "java.lang.String", "String");
          return this;
      }
        
      public UpmsRuleSQL andRemarksIn(List<java.lang.String> values) {
          addCriterion("remarks", values, ConditionMode.IN, "remarks", "java.lang.String", "String");
          return this;
      }

      public UpmsRuleSQL andRemarksNotIn(List<java.lang.String> values) {
          addCriterion("remarks", values, ConditionMode.NOT_IN, "remarks", "java.lang.String", "String");
          return this;
      }
	public UpmsRuleSQL andRuleTypeIsNull() {
		isnull("rule_type");
		return this;
	}
	
	public UpmsRuleSQL andRuleTypeIsNotNull() {
		notNull("rule_type");
		return this;
	}
	
	public UpmsRuleSQL andRuleTypeIsEmpty() {
		empty("rule_type");
		return this;
	}

	public UpmsRuleSQL andRuleTypeIsNotEmpty() {
		notEmpty("rule_type");
		return this;
	}
      public UpmsRuleSQL andRuleTypeEqualTo(java.lang.Integer value) {
          addCriterion("rule_type", value, ConditionMode.EQUAL, "ruleType", "java.lang.Integer", "Float");
          return this;
      }

      public UpmsRuleSQL andRuleTypeNotEqualTo(java.lang.Integer value) {
          addCriterion("rule_type", value, ConditionMode.NOT_EQUAL, "ruleType", "java.lang.Integer", "Float");
          return this;
      }

      public UpmsRuleSQL andRuleTypeGreaterThan(java.lang.Integer value) {
          addCriterion("rule_type", value, ConditionMode.GREATER_THEN, "ruleType", "java.lang.Integer", "Float");
          return this;
      }

      public UpmsRuleSQL andRuleTypeGreaterThanOrEqualTo(java.lang.Integer value) {
          addCriterion("rule_type", value, ConditionMode.GREATER_EQUAL, "ruleType", "java.lang.Integer", "Float");
          return this;
      }

      public UpmsRuleSQL andRuleTypeLessThan(java.lang.Integer value) {
          addCriterion("rule_type", value, ConditionMode.LESS_THEN, "ruleType", "java.lang.Integer", "Float");
          return this;
      }

      public UpmsRuleSQL andRuleTypeLessThanOrEqualTo(java.lang.Integer value) {
          addCriterion("rule_type", value, ConditionMode.LESS_EQUAL, "ruleType", "java.lang.Integer", "Float");
          return this;
      }

      public UpmsRuleSQL andRuleTypeBetween(java.lang.Integer value1, java.lang.Integer value2) {
    	  addCriterion("rule_type", value1, value2, ConditionMode.BETWEEN, "ruleType", "java.lang.Integer", "Float");
    	  return this;
      }

      public UpmsRuleSQL andRuleTypeNotBetween(java.lang.Integer value1, java.lang.Integer value2) {
          addCriterion("rule_type", value1, value2, ConditionMode.NOT_BETWEEN, "ruleType", "java.lang.Integer", "Float");
          return this;
      }
        
      public UpmsRuleSQL andRuleTypeIn(List<java.lang.Integer> values) {
          addCriterion("rule_type", values, ConditionMode.IN, "ruleType", "java.lang.Integer", "Float");
          return this;
      }

      public UpmsRuleSQL andRuleTypeNotIn(List<java.lang.Integer> values) {
          addCriterion("rule_type", values, ConditionMode.NOT_IN, "ruleType", "java.lang.Integer", "Float");
          return this;
      }
	public UpmsRuleSQL andRuleContentIsNull() {
		isnull("rule_content");
		return this;
	}
	
	public UpmsRuleSQL andRuleContentIsNotNull() {
		notNull("rule_content");
		return this;
	}
	
	public UpmsRuleSQL andRuleContentIsEmpty() {
		empty("rule_content");
		return this;
	}

	public UpmsRuleSQL andRuleContentIsNotEmpty() {
		notEmpty("rule_content");
		return this;
	}
       public UpmsRuleSQL andRuleContentLike(java.lang.String value) {
    	   addCriterion("rule_content", value, ConditionMode.FUZZY, "ruleContent", "java.lang.String", "Float");
    	   return this;
      }

      public UpmsRuleSQL andRuleContentNotLike(java.lang.String value) {
          addCriterion("rule_content", value, ConditionMode.NOT_FUZZY, "ruleContent", "java.lang.String", "Float");
          return this;
      }
      public UpmsRuleSQL andRuleContentEqualTo(java.lang.String value) {
          addCriterion("rule_content", value, ConditionMode.EQUAL, "ruleContent", "java.lang.String", "String");
          return this;
      }

      public UpmsRuleSQL andRuleContentNotEqualTo(java.lang.String value) {
          addCriterion("rule_content", value, ConditionMode.NOT_EQUAL, "ruleContent", "java.lang.String", "String");
          return this;
      }

      public UpmsRuleSQL andRuleContentGreaterThan(java.lang.String value) {
          addCriterion("rule_content", value, ConditionMode.GREATER_THEN, "ruleContent", "java.lang.String", "String");
          return this;
      }

      public UpmsRuleSQL andRuleContentGreaterThanOrEqualTo(java.lang.String value) {
          addCriterion("rule_content", value, ConditionMode.GREATER_EQUAL, "ruleContent", "java.lang.String", "String");
          return this;
      }

      public UpmsRuleSQL andRuleContentLessThan(java.lang.String value) {
          addCriterion("rule_content", value, ConditionMode.LESS_THEN, "ruleContent", "java.lang.String", "String");
          return this;
      }

      public UpmsRuleSQL andRuleContentLessThanOrEqualTo(java.lang.String value) {
          addCriterion("rule_content", value, ConditionMode.LESS_EQUAL, "ruleContent", "java.lang.String", "String");
          return this;
      }

      public UpmsRuleSQL andRuleContentBetween(java.lang.String value1, java.lang.String value2) {
    	  addCriterion("rule_content", value1, value2, ConditionMode.BETWEEN, "ruleContent", "java.lang.String", "String");
    	  return this;
      }

      public UpmsRuleSQL andRuleContentNotBetween(java.lang.String value1, java.lang.String value2) {
          addCriterion("rule_content", value1, value2, ConditionMode.NOT_BETWEEN, "ruleContent", "java.lang.String", "String");
          return this;
      }
        
      public UpmsRuleSQL andRuleContentIn(List<java.lang.String> values) {
          addCriterion("rule_content", values, ConditionMode.IN, "ruleContent", "java.lang.String", "String");
          return this;
      }

      public UpmsRuleSQL andRuleContentNotIn(List<java.lang.String> values) {
          addCriterion("rule_content", values, ConditionMode.NOT_IN, "ruleContent", "java.lang.String", "String");
          return this;
      }
	public UpmsRuleSQL andUrlIsNull() {
		isnull("url");
		return this;
	}
	
	public UpmsRuleSQL andUrlIsNotNull() {
		notNull("url");
		return this;
	}
	
	public UpmsRuleSQL andUrlIsEmpty() {
		empty("url");
		return this;
	}

	public UpmsRuleSQL andUrlIsNotEmpty() {
		notEmpty("url");
		return this;
	}
       public UpmsRuleSQL andUrlLike(java.lang.String value) {
    	   addCriterion("url", value, ConditionMode.FUZZY, "url", "java.lang.String", "String");
    	   return this;
      }

      public UpmsRuleSQL andUrlNotLike(java.lang.String value) {
          addCriterion("url", value, ConditionMode.NOT_FUZZY, "url", "java.lang.String", "String");
          return this;
      }
      public UpmsRuleSQL andUrlEqualTo(java.lang.String value) {
          addCriterion("url", value, ConditionMode.EQUAL, "url", "java.lang.String", "String");
          return this;
      }

      public UpmsRuleSQL andUrlNotEqualTo(java.lang.String value) {
          addCriterion("url", value, ConditionMode.NOT_EQUAL, "url", "java.lang.String", "String");
          return this;
      }

      public UpmsRuleSQL andUrlGreaterThan(java.lang.String value) {
          addCriterion("url", value, ConditionMode.GREATER_THEN, "url", "java.lang.String", "String");
          return this;
      }

      public UpmsRuleSQL andUrlGreaterThanOrEqualTo(java.lang.String value) {
          addCriterion("url", value, ConditionMode.GREATER_EQUAL, "url", "java.lang.String", "String");
          return this;
      }

      public UpmsRuleSQL andUrlLessThan(java.lang.String value) {
          addCriterion("url", value, ConditionMode.LESS_THEN, "url", "java.lang.String", "String");
          return this;
      }

      public UpmsRuleSQL andUrlLessThanOrEqualTo(java.lang.String value) {
          addCriterion("url", value, ConditionMode.LESS_EQUAL, "url", "java.lang.String", "String");
          return this;
      }

      public UpmsRuleSQL andUrlBetween(java.lang.String value1, java.lang.String value2) {
    	  addCriterion("url", value1, value2, ConditionMode.BETWEEN, "url", "java.lang.String", "String");
    	  return this;
      }

      public UpmsRuleSQL andUrlNotBetween(java.lang.String value1, java.lang.String value2) {
          addCriterion("url", value1, value2, ConditionMode.NOT_BETWEEN, "url", "java.lang.String", "String");
          return this;
      }
        
      public UpmsRuleSQL andUrlIn(List<java.lang.String> values) {
          addCriterion("url", values, ConditionMode.IN, "url", "java.lang.String", "String");
          return this;
      }

      public UpmsRuleSQL andUrlNotIn(List<java.lang.String> values) {
          addCriterion("url", values, ConditionMode.NOT_IN, "url", "java.lang.String", "String");
          return this;
      }
}