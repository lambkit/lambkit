/**
 * Copyright (c) 2015-2017, Henry Yang 杨勇 (gismail@foxmail.com).
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.lambkit.module.upms.sql;

import com.lambkit.db.sql.ConditionMode;
import com.lambkit.db.sql.Column;
import com.lambkit.db.sql.Columns;
import com.lambkit.db.sql.Example;

import java.io.Serializable;
import java.util.List;

/**
 * @author yangyong 
 * @website: www.lambkit.com
 * @email: gismail@foxmail.com
 * @date ${date}
 * @version 1.0
 * @since 1.0
 */

public class UpmsAppPermissionSQL extends Columns implements Serializable {
	private static final long serialVersionUID = 1L;
	
	public static UpmsAppPermissionSQL of() {
		return new UpmsAppPermissionSQL();
	}
	
	public static UpmsAppPermissionSQL by(Column column) {
		UpmsAppPermissionSQL that = new UpmsAppPermissionSQL();
		that.add(column);
        return that;
    }

    public static UpmsAppPermissionSQL by(String name, Object value) {
        return (UpmsAppPermissionSQL) create().eq(name, value);
    }
    
    public Example example() {
    	return Example.create("upms_app_permission", this);
    }
    
    /**
     * equals
     *
     * @param name
     * @param value
     * @return
     */
    public UpmsAppPermissionSQL eq(String name, Object value) {
    	super.eq(name, value);
        return this;
    }

    /**
     * not equals !=
     *
     * @param name
     * @param value
     * @return
     */
    public UpmsAppPermissionSQL ne(String name, Object value) {
    	super.ne(name, value);
        return this;
    }


    /**
     * like
     *
     * @param name
     * @param value
     * @return
     */

    public UpmsAppPermissionSQL like(String name, Object value) {
    	super.like(name, value);
        return this;
    }
    
    public UpmsAppPermissionSQL notLike(String name, Object value) {
    	super.notLike(name, value);
        return this;
    }

    /**
     * 大于 great than
     *
     * @param name
     * @param value
     * @return
     */
    public UpmsAppPermissionSQL gt(String name, Object value) {
    	super.gt(name, value);
        return this;
    }

    /**
     * 大于等于 great or equal
     *
     * @param name
     * @param value
     * @return
     */
    public UpmsAppPermissionSQL ge(String name, Object value) {
    	super.ge(name, value);
        return this;
    }

    /**
     * 小于 less than
     *
     * @param name
     * @param value
     * @return
     */
    public UpmsAppPermissionSQL lt(String name, Object value) {
    	super.lt(name, value);
        return this;
    }

    /**
     * 小于等于 less or equal
     *
     * @param name
     * @param value
     * @return
     */
    public UpmsAppPermissionSQL le(String name, Object value) {
    	super.le(name, value);
        return this;
    }
    
    public UpmsAppPermissionSQL isnull(String name) {
    	super.isnull(name);
        return this;
    } 

    public UpmsAppPermissionSQL notNull(String name) {
    	super.notNull(name);
        return this;
    } 
    
    public UpmsAppPermissionSQL empty(String name) {
    	super.empty(name);
        return this;
    } 
    
    public UpmsAppPermissionSQL notEmpty(String name) {
    	super.notEmpty(name);
        return this;
    } 
    
    public UpmsAppPermissionSQL add(Column column) {
    	super.add(column);
    	return this;
    }
    
    /**************************/
	
	public void addCriterion(String name, Object value, ConditionMode logic, String property, String typeHandler, String valueType) {
		 if (value == null) {
			 throw new RuntimeException("Value for " + property + " cannot be null");
		 }
		 add(Column.create(name, value, logic, typeHandler, valueType));
	}
   
	public void addCriterion(String name, Object value1, Object value2, ConditionMode logic, String property, String typeHandler, String valueType) {
		 if (value1 == null || value2 == null) {
			 throw new RuntimeException("Between values for " + property + " cannot be null");
		 }
		 add(Column.create(name, value1, value2, logic, typeHandler, valueType));
	}
		 
	public UpmsAppPermissionSQL andAppPermissionIdIsNull() {
		isnull("app_permission_id");
		return this;
	}
	
	public UpmsAppPermissionSQL andAppPermissionIdIsNotNull() {
		notNull("app_permission_id");
		return this;
	}
	
	public UpmsAppPermissionSQL andAppPermissionIdIsEmpty() {
		empty("app_permission_id");
		return this;
	}

	public UpmsAppPermissionSQL andAppPermissionIdIsNotEmpty() {
		notEmpty("app_permission_id");
		return this;
	}
      public UpmsAppPermissionSQL andAppPermissionIdEqualTo(java.lang.Long value) {
          addCriterion("app_permission_id", value, ConditionMode.EQUAL, "appPermissionId", "java.lang.Long", "Float");
          return this;
      }

      public UpmsAppPermissionSQL andAppPermissionIdNotEqualTo(java.lang.Long value) {
          addCriterion("app_permission_id", value, ConditionMode.NOT_EQUAL, "appPermissionId", "java.lang.Long", "Float");
          return this;
      }

      public UpmsAppPermissionSQL andAppPermissionIdGreaterThan(java.lang.Long value) {
          addCriterion("app_permission_id", value, ConditionMode.GREATER_THEN, "appPermissionId", "java.lang.Long", "Float");
          return this;
      }

      public UpmsAppPermissionSQL andAppPermissionIdGreaterThanOrEqualTo(java.lang.Long value) {
          addCriterion("app_permission_id", value, ConditionMode.GREATER_EQUAL, "appPermissionId", "java.lang.Long", "Float");
          return this;
      }

      public UpmsAppPermissionSQL andAppPermissionIdLessThan(java.lang.Long value) {
          addCriterion("app_permission_id", value, ConditionMode.LESS_THEN, "appPermissionId", "java.lang.Long", "Float");
          return this;
      }

      public UpmsAppPermissionSQL andAppPermissionIdLessThanOrEqualTo(java.lang.Long value) {
          addCriterion("app_permission_id", value, ConditionMode.LESS_EQUAL, "appPermissionId", "java.lang.Long", "Float");
          return this;
      }

      public UpmsAppPermissionSQL andAppPermissionIdBetween(java.lang.Long value1, java.lang.Long value2) {
    	  addCriterion("app_permission_id", value1, value2, ConditionMode.BETWEEN, "appPermissionId", "java.lang.Long", "Float");
    	  return this;
      }

      public UpmsAppPermissionSQL andAppPermissionIdNotBetween(java.lang.Long value1, java.lang.Long value2) {
          addCriterion("app_permission_id", value1, value2, ConditionMode.NOT_BETWEEN, "appPermissionId", "java.lang.Long", "Float");
          return this;
      }
        
      public UpmsAppPermissionSQL andAppPermissionIdIn(List<java.lang.Long> values) {
          addCriterion("app_permission_id", values, ConditionMode.IN, "appPermissionId", "java.lang.Long", "Float");
          return this;
      }

      public UpmsAppPermissionSQL andAppPermissionIdNotIn(List<java.lang.Long> values) {
          addCriterion("app_permission_id", values, ConditionMode.NOT_IN, "appPermissionId", "java.lang.Long", "Float");
          return this;
      }
	public UpmsAppPermissionSQL andAppIdIsNull() {
		isnull("app_id");
		return this;
	}
	
	public UpmsAppPermissionSQL andAppIdIsNotNull() {
		notNull("app_id");
		return this;
	}
	
	public UpmsAppPermissionSQL andAppIdIsEmpty() {
		empty("app_id");
		return this;
	}

	public UpmsAppPermissionSQL andAppIdIsNotEmpty() {
		notEmpty("app_id");
		return this;
	}
      public UpmsAppPermissionSQL andAppIdEqualTo(java.lang.Long value) {
          addCriterion("app_id", value, ConditionMode.EQUAL, "appId", "java.lang.Long", "Float");
          return this;
      }

      public UpmsAppPermissionSQL andAppIdNotEqualTo(java.lang.Long value) {
          addCriterion("app_id", value, ConditionMode.NOT_EQUAL, "appId", "java.lang.Long", "Float");
          return this;
      }

      public UpmsAppPermissionSQL andAppIdGreaterThan(java.lang.Long value) {
          addCriterion("app_id", value, ConditionMode.GREATER_THEN, "appId", "java.lang.Long", "Float");
          return this;
      }

      public UpmsAppPermissionSQL andAppIdGreaterThanOrEqualTo(java.lang.Long value) {
          addCriterion("app_id", value, ConditionMode.GREATER_EQUAL, "appId", "java.lang.Long", "Float");
          return this;
      }

      public UpmsAppPermissionSQL andAppIdLessThan(java.lang.Long value) {
          addCriterion("app_id", value, ConditionMode.LESS_THEN, "appId", "java.lang.Long", "Float");
          return this;
      }

      public UpmsAppPermissionSQL andAppIdLessThanOrEqualTo(java.lang.Long value) {
          addCriterion("app_id", value, ConditionMode.LESS_EQUAL, "appId", "java.lang.Long", "Float");
          return this;
      }

      public UpmsAppPermissionSQL andAppIdBetween(java.lang.Long value1, java.lang.Long value2) {
    	  addCriterion("app_id", value1, value2, ConditionMode.BETWEEN, "appId", "java.lang.Long", "Float");
    	  return this;
      }

      public UpmsAppPermissionSQL andAppIdNotBetween(java.lang.Long value1, java.lang.Long value2) {
          addCriterion("app_id", value1, value2, ConditionMode.NOT_BETWEEN, "appId", "java.lang.Long", "Float");
          return this;
      }
        
      public UpmsAppPermissionSQL andAppIdIn(List<java.lang.Long> values) {
          addCriterion("app_id", values, ConditionMode.IN, "appId", "java.lang.Long", "Float");
          return this;
      }

      public UpmsAppPermissionSQL andAppIdNotIn(List<java.lang.Long> values) {
          addCriterion("app_id", values, ConditionMode.NOT_IN, "appId", "java.lang.Long", "Float");
          return this;
      }
	public UpmsAppPermissionSQL andPermissionIdIsNull() {
		isnull("permission_id");
		return this;
	}
	
	public UpmsAppPermissionSQL andPermissionIdIsNotNull() {
		notNull("permission_id");
		return this;
	}
	
	public UpmsAppPermissionSQL andPermissionIdIsEmpty() {
		empty("permission_id");
		return this;
	}

	public UpmsAppPermissionSQL andPermissionIdIsNotEmpty() {
		notEmpty("permission_id");
		return this;
	}
      public UpmsAppPermissionSQL andPermissionIdEqualTo(java.lang.Long value) {
          addCriterion("permission_id", value, ConditionMode.EQUAL, "permissionId", "java.lang.Long", "Float");
          return this;
      }

      public UpmsAppPermissionSQL andPermissionIdNotEqualTo(java.lang.Long value) {
          addCriterion("permission_id", value, ConditionMode.NOT_EQUAL, "permissionId", "java.lang.Long", "Float");
          return this;
      }

      public UpmsAppPermissionSQL andPermissionIdGreaterThan(java.lang.Long value) {
          addCriterion("permission_id", value, ConditionMode.GREATER_THEN, "permissionId", "java.lang.Long", "Float");
          return this;
      }

      public UpmsAppPermissionSQL andPermissionIdGreaterThanOrEqualTo(java.lang.Long value) {
          addCriterion("permission_id", value, ConditionMode.GREATER_EQUAL, "permissionId", "java.lang.Long", "Float");
          return this;
      }

      public UpmsAppPermissionSQL andPermissionIdLessThan(java.lang.Long value) {
          addCriterion("permission_id", value, ConditionMode.LESS_THEN, "permissionId", "java.lang.Long", "Float");
          return this;
      }

      public UpmsAppPermissionSQL andPermissionIdLessThanOrEqualTo(java.lang.Long value) {
          addCriterion("permission_id", value, ConditionMode.LESS_EQUAL, "permissionId", "java.lang.Long", "Float");
          return this;
      }

      public UpmsAppPermissionSQL andPermissionIdBetween(java.lang.Long value1, java.lang.Long value2) {
    	  addCriterion("permission_id", value1, value2, ConditionMode.BETWEEN, "permissionId", "java.lang.Long", "Float");
    	  return this;
      }

      public UpmsAppPermissionSQL andPermissionIdNotBetween(java.lang.Long value1, java.lang.Long value2) {
          addCriterion("permission_id", value1, value2, ConditionMode.NOT_BETWEEN, "permissionId", "java.lang.Long", "Float");
          return this;
      }
        
      public UpmsAppPermissionSQL andPermissionIdIn(List<java.lang.Long> values) {
          addCriterion("permission_id", values, ConditionMode.IN, "permissionId", "java.lang.Long", "Float");
          return this;
      }

      public UpmsAppPermissionSQL andPermissionIdNotIn(List<java.lang.Long> values) {
          addCriterion("permission_id", values, ConditionMode.NOT_IN, "permissionId", "java.lang.Long", "Float");
          return this;
      }
	public UpmsAppPermissionSQL andTypeIsNull() {
		isnull("type");
		return this;
	}
	
	public UpmsAppPermissionSQL andTypeIsNotNull() {
		notNull("type");
		return this;
	}
	
	public UpmsAppPermissionSQL andTypeIsEmpty() {
		empty("type");
		return this;
	}

	public UpmsAppPermissionSQL andTypeIsNotEmpty() {
		notEmpty("type");
		return this;
	}
      public UpmsAppPermissionSQL andTypeEqualTo(java.lang.Integer value) {
          addCriterion("type", value, ConditionMode.EQUAL, "type", "java.lang.Integer", "Float");
          return this;
      }

      public UpmsAppPermissionSQL andTypeNotEqualTo(java.lang.Integer value) {
          addCriterion("type", value, ConditionMode.NOT_EQUAL, "type", "java.lang.Integer", "Float");
          return this;
      }

      public UpmsAppPermissionSQL andTypeGreaterThan(java.lang.Integer value) {
          addCriterion("type", value, ConditionMode.GREATER_THEN, "type", "java.lang.Integer", "Float");
          return this;
      }

      public UpmsAppPermissionSQL andTypeGreaterThanOrEqualTo(java.lang.Integer value) {
          addCriterion("type", value, ConditionMode.GREATER_EQUAL, "type", "java.lang.Integer", "Float");
          return this;
      }

      public UpmsAppPermissionSQL andTypeLessThan(java.lang.Integer value) {
          addCriterion("type", value, ConditionMode.LESS_THEN, "type", "java.lang.Integer", "Float");
          return this;
      }

      public UpmsAppPermissionSQL andTypeLessThanOrEqualTo(java.lang.Integer value) {
          addCriterion("type", value, ConditionMode.LESS_EQUAL, "type", "java.lang.Integer", "Float");
          return this;
      }

      public UpmsAppPermissionSQL andTypeBetween(java.lang.Integer value1, java.lang.Integer value2) {
    	  addCriterion("type", value1, value2, ConditionMode.BETWEEN, "type", "java.lang.Integer", "Float");
    	  return this;
      }

      public UpmsAppPermissionSQL andTypeNotBetween(java.lang.Integer value1, java.lang.Integer value2) {
          addCriterion("type", value1, value2, ConditionMode.NOT_BETWEEN, "type", "java.lang.Integer", "Float");
          return this;
      }
        
      public UpmsAppPermissionSQL andTypeIn(List<java.lang.Integer> values) {
          addCriterion("type", values, ConditionMode.IN, "type", "java.lang.Integer", "Float");
          return this;
      }

      public UpmsAppPermissionSQL andTypeNotIn(List<java.lang.Integer> values) {
          addCriterion("type", values, ConditionMode.NOT_IN, "type", "java.lang.Integer", "Float");
          return this;
      }
}