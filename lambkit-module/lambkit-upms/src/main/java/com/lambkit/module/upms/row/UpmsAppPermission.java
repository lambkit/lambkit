/**
 * Copyright (c) 2015-2017, Henry Yang 杨勇 (gismail@foxmail.com).
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.lambkit.module.upms.row;

import com.lambkit.db.RowModel;

/**
 * @author yangyong 
 * @website: www.lambkit.com
 * @email: gismail@foxmail.com
 * @date ${date}
 * @version 1.0
 * @since 1.0
 */

public class UpmsAppPermission extends RowModel<UpmsAppPermission> {
	public UpmsAppPermission() {
		setTableName("upms_app_permission");
		setPrimaryKey("app_permission_id");
	}
	public java.lang.Long getAppPermissionId() {
		return this.get("app_permission_id");
	}
	public void setAppPermissionId(java.lang.Long appPermissionId) {
		this.set("app_permission_id", appPermissionId);
	}
	public java.lang.Long getAppId() {
		return this.get("app_id");
	}
	public void setAppId(java.lang.Long appId) {
		this.set("app_id", appId);
	}
	public java.lang.Long getPermissionId() {
		return this.get("permission_id");
	}
	public void setPermissionId(java.lang.Long permissionId) {
		this.set("permission_id", permissionId);
	}
	public java.lang.Integer getType() {
		return this.get("type");
	}
	public void setType(java.lang.Integer type) {
		this.set("type", type);
	}
}
