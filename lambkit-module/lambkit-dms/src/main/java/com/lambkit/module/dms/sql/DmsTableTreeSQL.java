/**
 * Copyright (c) 2015-2017, Henry Yang 杨勇 (gismail@foxmail.com).
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.lambkit.module.dms.sql;

import com.lambkit.db.sql.ConditionMode;
import com.lambkit.db.sql.Column;
import com.lambkit.db.sql.Columns;
import com.lambkit.db.sql.Example;

import java.io.Serializable;
import java.util.List;

/**
 * @author yangyong 
 * @website: www.lambkit.com
 * @email: gismail@foxmail.com
 * @date ${date}
 * @version 1.0
 * @since 1.0
 */

public class DmsTableTreeSQL extends Columns implements Serializable {
	private static final long serialVersionUID = 1L;
	
	public static DmsTableTreeSQL of() {
		return new DmsTableTreeSQL();
	}
	
	public static DmsTableTreeSQL by(Column column) {
		DmsTableTreeSQL that = new DmsTableTreeSQL();
		that.add(column);
        return that;
    }

    public static DmsTableTreeSQL by(String name, Object value) {
        return (DmsTableTreeSQL) create().eq(name, value);
    }
    
    public Example example() {
    	return Example.create("dms_table_tree", this);
    }
    
    /**
     * equals
     *
     * @param name
     * @param value
     * @return
     */
    public DmsTableTreeSQL eq(String name, Object value) {
    	super.eq(name, value);
        return this;
    }

    /**
     * not equals !=
     *
     * @param name
     * @param value
     * @return
     */
    public DmsTableTreeSQL ne(String name, Object value) {
    	super.ne(name, value);
        return this;
    }


    /**
     * like
     *
     * @param name
     * @param value
     * @return
     */

    public DmsTableTreeSQL like(String name, Object value) {
    	super.like(name, value);
        return this;
    }
    
    public DmsTableTreeSQL notLike(String name, Object value) {
    	super.notLike(name, value);
        return this;
    }

    /**
     * 大于 great than
     *
     * @param name
     * @param value
     * @return
     */
    public DmsTableTreeSQL gt(String name, Object value) {
    	super.gt(name, value);
        return this;
    }

    /**
     * 大于等于 great or equal
     *
     * @param name
     * @param value
     * @return
     */
    public DmsTableTreeSQL ge(String name, Object value) {
    	super.ge(name, value);
        return this;
    }

    /**
     * 小于 less than
     *
     * @param name
     * @param value
     * @return
     */
    public DmsTableTreeSQL lt(String name, Object value) {
    	super.lt(name, value);
        return this;
    }

    /**
     * 小于等于 less or equal
     *
     * @param name
     * @param value
     * @return
     */
    public DmsTableTreeSQL le(String name, Object value) {
    	super.le(name, value);
        return this;
    }
    
    public DmsTableTreeSQL isnull(String name) {
    	super.isnull(name);
        return this;
    } 

    public DmsTableTreeSQL notNull(String name) {
    	super.notNull(name);
        return this;
    } 
    
    public DmsTableTreeSQL empty(String name) {
    	super.empty(name);
        return this;
    } 
    
    public DmsTableTreeSQL notEmpty(String name) {
    	super.notEmpty(name);
        return this;
    } 
    
    public DmsTableTreeSQL add(Column column) {
    	super.add(column);
    	return this;
    }
    
    /**************************/
	
	public void addCriterion(String name, Object value, ConditionMode logic, String property, String typeHandler, String valueType) {
		 if (value == null) {
			 throw new RuntimeException("Value for " + property + " cannot be null");
		 }
		 add(Column.create(name, value, logic, typeHandler, valueType));
	}
   
	public void addCriterion(String name, Object value1, Object value2, ConditionMode logic, String property, String typeHandler, String valueType) {
		 if (value1 == null || value2 == null) {
			 throw new RuntimeException("Between values for " + property + " cannot be null");
		 }
		 add(Column.create(name, value1, value2, logic, typeHandler, valueType));
	}
		 
	public DmsTableTreeSQL andTtidIsNull() {
		isnull("ttid");
		return this;
	}
	
	public DmsTableTreeSQL andTtidIsNotNull() {
		notNull("ttid");
		return this;
	}
	
	public DmsTableTreeSQL andTtidIsEmpty() {
		empty("ttid");
		return this;
	}

	public DmsTableTreeSQL andTtidIsNotEmpty() {
		notEmpty("ttid");
		return this;
	}
      public DmsTableTreeSQL andTtidEqualTo(java.lang.Long value) {
          addCriterion("ttid", value, ConditionMode.EQUAL, "ttid", "java.lang.Long", "Float");
          return this;
      }

      public DmsTableTreeSQL andTtidNotEqualTo(java.lang.Long value) {
          addCriterion("ttid", value, ConditionMode.NOT_EQUAL, "ttid", "java.lang.Long", "Float");
          return this;
      }

      public DmsTableTreeSQL andTtidGreaterThan(java.lang.Long value) {
          addCriterion("ttid", value, ConditionMode.GREATER_THEN, "ttid", "java.lang.Long", "Float");
          return this;
      }

      public DmsTableTreeSQL andTtidGreaterThanOrEqualTo(java.lang.Long value) {
          addCriterion("ttid", value, ConditionMode.GREATER_EQUAL, "ttid", "java.lang.Long", "Float");
          return this;
      }

      public DmsTableTreeSQL andTtidLessThan(java.lang.Long value) {
          addCriterion("ttid", value, ConditionMode.LESS_THEN, "ttid", "java.lang.Long", "Float");
          return this;
      }

      public DmsTableTreeSQL andTtidLessThanOrEqualTo(java.lang.Long value) {
          addCriterion("ttid", value, ConditionMode.LESS_EQUAL, "ttid", "java.lang.Long", "Float");
          return this;
      }

      public DmsTableTreeSQL andTtidBetween(java.lang.Long value1, java.lang.Long value2) {
    	  addCriterion("ttid", value1, value2, ConditionMode.BETWEEN, "ttid", "java.lang.Long", "Float");
    	  return this;
      }

      public DmsTableTreeSQL andTtidNotBetween(java.lang.Long value1, java.lang.Long value2) {
          addCriterion("ttid", value1, value2, ConditionMode.NOT_BETWEEN, "ttid", "java.lang.Long", "Float");
          return this;
      }
        
      public DmsTableTreeSQL andTtidIn(List<java.lang.Long> values) {
          addCriterion("ttid", values, ConditionMode.IN, "ttid", "java.lang.Long", "Float");
          return this;
      }

      public DmsTableTreeSQL andTtidNotIn(List<java.lang.Long> values) {
          addCriterion("ttid", values, ConditionMode.NOT_IN, "ttid", "java.lang.Long", "Float");
          return this;
      }
	public DmsTableTreeSQL andModelIdNameIsNull() {
		isnull("model_id_name");
		return this;
	}
	
	public DmsTableTreeSQL andModelIdNameIsNotNull() {
		notNull("model_id_name");
		return this;
	}
	
	public DmsTableTreeSQL andModelIdNameIsEmpty() {
		empty("model_id_name");
		return this;
	}

	public DmsTableTreeSQL andModelIdNameIsNotEmpty() {
		notEmpty("model_id_name");
		return this;
	}
       public DmsTableTreeSQL andModelIdNameLike(java.lang.String value) {
    	   addCriterion("model_id_name", value, ConditionMode.FUZZY, "modelIdName", "java.lang.String", "Float");
    	   return this;
      }

      public DmsTableTreeSQL andModelIdNameNotLike(java.lang.String value) {
          addCriterion("model_id_name", value, ConditionMode.NOT_FUZZY, "modelIdName", "java.lang.String", "Float");
          return this;
      }
      public DmsTableTreeSQL andModelIdNameEqualTo(java.lang.String value) {
          addCriterion("model_id_name", value, ConditionMode.EQUAL, "modelIdName", "java.lang.String", "String");
          return this;
      }

      public DmsTableTreeSQL andModelIdNameNotEqualTo(java.lang.String value) {
          addCriterion("model_id_name", value, ConditionMode.NOT_EQUAL, "modelIdName", "java.lang.String", "String");
          return this;
      }

      public DmsTableTreeSQL andModelIdNameGreaterThan(java.lang.String value) {
          addCriterion("model_id_name", value, ConditionMode.GREATER_THEN, "modelIdName", "java.lang.String", "String");
          return this;
      }

      public DmsTableTreeSQL andModelIdNameGreaterThanOrEqualTo(java.lang.String value) {
          addCriterion("model_id_name", value, ConditionMode.GREATER_EQUAL, "modelIdName", "java.lang.String", "String");
          return this;
      }

      public DmsTableTreeSQL andModelIdNameLessThan(java.lang.String value) {
          addCriterion("model_id_name", value, ConditionMode.LESS_THEN, "modelIdName", "java.lang.String", "String");
          return this;
      }

      public DmsTableTreeSQL andModelIdNameLessThanOrEqualTo(java.lang.String value) {
          addCriterion("model_id_name", value, ConditionMode.LESS_EQUAL, "modelIdName", "java.lang.String", "String");
          return this;
      }

      public DmsTableTreeSQL andModelIdNameBetween(java.lang.String value1, java.lang.String value2) {
    	  addCriterion("model_id_name", value1, value2, ConditionMode.BETWEEN, "modelIdName", "java.lang.String", "String");
    	  return this;
      }

      public DmsTableTreeSQL andModelIdNameNotBetween(java.lang.String value1, java.lang.String value2) {
          addCriterion("model_id_name", value1, value2, ConditionMode.NOT_BETWEEN, "modelIdName", "java.lang.String", "String");
          return this;
      }
        
      public DmsTableTreeSQL andModelIdNameIn(List<java.lang.String> values) {
          addCriterion("model_id_name", values, ConditionMode.IN, "modelIdName", "java.lang.String", "String");
          return this;
      }

      public DmsTableTreeSQL andModelIdNameNotIn(List<java.lang.String> values) {
          addCriterion("model_id_name", values, ConditionMode.NOT_IN, "modelIdName", "java.lang.String", "String");
          return this;
      }
	public DmsTableTreeSQL andParentIdNameIsNull() {
		isnull("parent_id_name");
		return this;
	}
	
	public DmsTableTreeSQL andParentIdNameIsNotNull() {
		notNull("parent_id_name");
		return this;
	}
	
	public DmsTableTreeSQL andParentIdNameIsEmpty() {
		empty("parent_id_name");
		return this;
	}

	public DmsTableTreeSQL andParentIdNameIsNotEmpty() {
		notEmpty("parent_id_name");
		return this;
	}
       public DmsTableTreeSQL andParentIdNameLike(java.lang.String value) {
    	   addCriterion("parent_id_name", value, ConditionMode.FUZZY, "parentIdName", "java.lang.String", "String");
    	   return this;
      }

      public DmsTableTreeSQL andParentIdNameNotLike(java.lang.String value) {
          addCriterion("parent_id_name", value, ConditionMode.NOT_FUZZY, "parentIdName", "java.lang.String", "String");
          return this;
      }
      public DmsTableTreeSQL andParentIdNameEqualTo(java.lang.String value) {
          addCriterion("parent_id_name", value, ConditionMode.EQUAL, "parentIdName", "java.lang.String", "String");
          return this;
      }

      public DmsTableTreeSQL andParentIdNameNotEqualTo(java.lang.String value) {
          addCriterion("parent_id_name", value, ConditionMode.NOT_EQUAL, "parentIdName", "java.lang.String", "String");
          return this;
      }

      public DmsTableTreeSQL andParentIdNameGreaterThan(java.lang.String value) {
          addCriterion("parent_id_name", value, ConditionMode.GREATER_THEN, "parentIdName", "java.lang.String", "String");
          return this;
      }

      public DmsTableTreeSQL andParentIdNameGreaterThanOrEqualTo(java.lang.String value) {
          addCriterion("parent_id_name", value, ConditionMode.GREATER_EQUAL, "parentIdName", "java.lang.String", "String");
          return this;
      }

      public DmsTableTreeSQL andParentIdNameLessThan(java.lang.String value) {
          addCriterion("parent_id_name", value, ConditionMode.LESS_THEN, "parentIdName", "java.lang.String", "String");
          return this;
      }

      public DmsTableTreeSQL andParentIdNameLessThanOrEqualTo(java.lang.String value) {
          addCriterion("parent_id_name", value, ConditionMode.LESS_EQUAL, "parentIdName", "java.lang.String", "String");
          return this;
      }

      public DmsTableTreeSQL andParentIdNameBetween(java.lang.String value1, java.lang.String value2) {
    	  addCriterion("parent_id_name", value1, value2, ConditionMode.BETWEEN, "parentIdName", "java.lang.String", "String");
    	  return this;
      }

      public DmsTableTreeSQL andParentIdNameNotBetween(java.lang.String value1, java.lang.String value2) {
          addCriterion("parent_id_name", value1, value2, ConditionMode.NOT_BETWEEN, "parentIdName", "java.lang.String", "String");
          return this;
      }
        
      public DmsTableTreeSQL andParentIdNameIn(List<java.lang.String> values) {
          addCriterion("parent_id_name", values, ConditionMode.IN, "parentIdName", "java.lang.String", "String");
          return this;
      }

      public DmsTableTreeSQL andParentIdNameNotIn(List<java.lang.String> values) {
          addCriterion("parent_id_name", values, ConditionMode.NOT_IN, "parentIdName", "java.lang.String", "String");
          return this;
      }
	public DmsTableTreeSQL andPathNameIsNull() {
		isnull("path_name");
		return this;
	}
	
	public DmsTableTreeSQL andPathNameIsNotNull() {
		notNull("path_name");
		return this;
	}
	
	public DmsTableTreeSQL andPathNameIsEmpty() {
		empty("path_name");
		return this;
	}

	public DmsTableTreeSQL andPathNameIsNotEmpty() {
		notEmpty("path_name");
		return this;
	}
       public DmsTableTreeSQL andPathNameLike(java.lang.String value) {
    	   addCriterion("path_name", value, ConditionMode.FUZZY, "pathName", "java.lang.String", "String");
    	   return this;
      }

      public DmsTableTreeSQL andPathNameNotLike(java.lang.String value) {
          addCriterion("path_name", value, ConditionMode.NOT_FUZZY, "pathName", "java.lang.String", "String");
          return this;
      }
      public DmsTableTreeSQL andPathNameEqualTo(java.lang.String value) {
          addCriterion("path_name", value, ConditionMode.EQUAL, "pathName", "java.lang.String", "String");
          return this;
      }

      public DmsTableTreeSQL andPathNameNotEqualTo(java.lang.String value) {
          addCriterion("path_name", value, ConditionMode.NOT_EQUAL, "pathName", "java.lang.String", "String");
          return this;
      }

      public DmsTableTreeSQL andPathNameGreaterThan(java.lang.String value) {
          addCriterion("path_name", value, ConditionMode.GREATER_THEN, "pathName", "java.lang.String", "String");
          return this;
      }

      public DmsTableTreeSQL andPathNameGreaterThanOrEqualTo(java.lang.String value) {
          addCriterion("path_name", value, ConditionMode.GREATER_EQUAL, "pathName", "java.lang.String", "String");
          return this;
      }

      public DmsTableTreeSQL andPathNameLessThan(java.lang.String value) {
          addCriterion("path_name", value, ConditionMode.LESS_THEN, "pathName", "java.lang.String", "String");
          return this;
      }

      public DmsTableTreeSQL andPathNameLessThanOrEqualTo(java.lang.String value) {
          addCriterion("path_name", value, ConditionMode.LESS_EQUAL, "pathName", "java.lang.String", "String");
          return this;
      }

      public DmsTableTreeSQL andPathNameBetween(java.lang.String value1, java.lang.String value2) {
    	  addCriterion("path_name", value1, value2, ConditionMode.BETWEEN, "pathName", "java.lang.String", "String");
    	  return this;
      }

      public DmsTableTreeSQL andPathNameNotBetween(java.lang.String value1, java.lang.String value2) {
          addCriterion("path_name", value1, value2, ConditionMode.NOT_BETWEEN, "pathName", "java.lang.String", "String");
          return this;
      }
        
      public DmsTableTreeSQL andPathNameIn(List<java.lang.String> values) {
          addCriterion("path_name", values, ConditionMode.IN, "pathName", "java.lang.String", "String");
          return this;
      }

      public DmsTableTreeSQL andPathNameNotIn(List<java.lang.String> values) {
          addCriterion("path_name", values, ConditionMode.NOT_IN, "pathName", "java.lang.String", "String");
          return this;
      }
	public DmsTableTreeSQL andLevelNameIsNull() {
		isnull("level_name");
		return this;
	}
	
	public DmsTableTreeSQL andLevelNameIsNotNull() {
		notNull("level_name");
		return this;
	}
	
	public DmsTableTreeSQL andLevelNameIsEmpty() {
		empty("level_name");
		return this;
	}

	public DmsTableTreeSQL andLevelNameIsNotEmpty() {
		notEmpty("level_name");
		return this;
	}
       public DmsTableTreeSQL andLevelNameLike(java.lang.String value) {
    	   addCriterion("level_name", value, ConditionMode.FUZZY, "levelName", "java.lang.String", "String");
    	   return this;
      }

      public DmsTableTreeSQL andLevelNameNotLike(java.lang.String value) {
          addCriterion("level_name", value, ConditionMode.NOT_FUZZY, "levelName", "java.lang.String", "String");
          return this;
      }
      public DmsTableTreeSQL andLevelNameEqualTo(java.lang.String value) {
          addCriterion("level_name", value, ConditionMode.EQUAL, "levelName", "java.lang.String", "String");
          return this;
      }

      public DmsTableTreeSQL andLevelNameNotEqualTo(java.lang.String value) {
          addCriterion("level_name", value, ConditionMode.NOT_EQUAL, "levelName", "java.lang.String", "String");
          return this;
      }

      public DmsTableTreeSQL andLevelNameGreaterThan(java.lang.String value) {
          addCriterion("level_name", value, ConditionMode.GREATER_THEN, "levelName", "java.lang.String", "String");
          return this;
      }

      public DmsTableTreeSQL andLevelNameGreaterThanOrEqualTo(java.lang.String value) {
          addCriterion("level_name", value, ConditionMode.GREATER_EQUAL, "levelName", "java.lang.String", "String");
          return this;
      }

      public DmsTableTreeSQL andLevelNameLessThan(java.lang.String value) {
          addCriterion("level_name", value, ConditionMode.LESS_THEN, "levelName", "java.lang.String", "String");
          return this;
      }

      public DmsTableTreeSQL andLevelNameLessThanOrEqualTo(java.lang.String value) {
          addCriterion("level_name", value, ConditionMode.LESS_EQUAL, "levelName", "java.lang.String", "String");
          return this;
      }

      public DmsTableTreeSQL andLevelNameBetween(java.lang.String value1, java.lang.String value2) {
    	  addCriterion("level_name", value1, value2, ConditionMode.BETWEEN, "levelName", "java.lang.String", "String");
    	  return this;
      }

      public DmsTableTreeSQL andLevelNameNotBetween(java.lang.String value1, java.lang.String value2) {
          addCriterion("level_name", value1, value2, ConditionMode.NOT_BETWEEN, "levelName", "java.lang.String", "String");
          return this;
      }
        
      public DmsTableTreeSQL andLevelNameIn(List<java.lang.String> values) {
          addCriterion("level_name", values, ConditionMode.IN, "levelName", "java.lang.String", "String");
          return this;
      }

      public DmsTableTreeSQL andLevelNameNotIn(List<java.lang.String> values) {
          addCriterion("level_name", values, ConditionMode.NOT_IN, "levelName", "java.lang.String", "String");
          return this;
      }
	public DmsTableTreeSQL andLeafNameIsNull() {
		isnull("leaf_name");
		return this;
	}
	
	public DmsTableTreeSQL andLeafNameIsNotNull() {
		notNull("leaf_name");
		return this;
	}
	
	public DmsTableTreeSQL andLeafNameIsEmpty() {
		empty("leaf_name");
		return this;
	}

	public DmsTableTreeSQL andLeafNameIsNotEmpty() {
		notEmpty("leaf_name");
		return this;
	}
       public DmsTableTreeSQL andLeafNameLike(java.lang.String value) {
    	   addCriterion("leaf_name", value, ConditionMode.FUZZY, "leafName", "java.lang.String", "String");
    	   return this;
      }

      public DmsTableTreeSQL andLeafNameNotLike(java.lang.String value) {
          addCriterion("leaf_name", value, ConditionMode.NOT_FUZZY, "leafName", "java.lang.String", "String");
          return this;
      }
      public DmsTableTreeSQL andLeafNameEqualTo(java.lang.String value) {
          addCriterion("leaf_name", value, ConditionMode.EQUAL, "leafName", "java.lang.String", "String");
          return this;
      }

      public DmsTableTreeSQL andLeafNameNotEqualTo(java.lang.String value) {
          addCriterion("leaf_name", value, ConditionMode.NOT_EQUAL, "leafName", "java.lang.String", "String");
          return this;
      }

      public DmsTableTreeSQL andLeafNameGreaterThan(java.lang.String value) {
          addCriterion("leaf_name", value, ConditionMode.GREATER_THEN, "leafName", "java.lang.String", "String");
          return this;
      }

      public DmsTableTreeSQL andLeafNameGreaterThanOrEqualTo(java.lang.String value) {
          addCriterion("leaf_name", value, ConditionMode.GREATER_EQUAL, "leafName", "java.lang.String", "String");
          return this;
      }

      public DmsTableTreeSQL andLeafNameLessThan(java.lang.String value) {
          addCriterion("leaf_name", value, ConditionMode.LESS_THEN, "leafName", "java.lang.String", "String");
          return this;
      }

      public DmsTableTreeSQL andLeafNameLessThanOrEqualTo(java.lang.String value) {
          addCriterion("leaf_name", value, ConditionMode.LESS_EQUAL, "leafName", "java.lang.String", "String");
          return this;
      }

      public DmsTableTreeSQL andLeafNameBetween(java.lang.String value1, java.lang.String value2) {
    	  addCriterion("leaf_name", value1, value2, ConditionMode.BETWEEN, "leafName", "java.lang.String", "String");
    	  return this;
      }

      public DmsTableTreeSQL andLeafNameNotBetween(java.lang.String value1, java.lang.String value2) {
          addCriterion("leaf_name", value1, value2, ConditionMode.NOT_BETWEEN, "leafName", "java.lang.String", "String");
          return this;
      }
        
      public DmsTableTreeSQL andLeafNameIn(List<java.lang.String> values) {
          addCriterion("leaf_name", values, ConditionMode.IN, "leafName", "java.lang.String", "String");
          return this;
      }

      public DmsTableTreeSQL andLeafNameNotIn(List<java.lang.String> values) {
          addCriterion("leaf_name", values, ConditionMode.NOT_IN, "leafName", "java.lang.String", "String");
          return this;
      }
	public DmsTableTreeSQL andExtTableIsNull() {
		isnull("ext_table");
		return this;
	}
	
	public DmsTableTreeSQL andExtTableIsNotNull() {
		notNull("ext_table");
		return this;
	}
	
	public DmsTableTreeSQL andExtTableIsEmpty() {
		empty("ext_table");
		return this;
	}

	public DmsTableTreeSQL andExtTableIsNotEmpty() {
		notEmpty("ext_table");
		return this;
	}
       public DmsTableTreeSQL andExtTableLike(java.lang.String value) {
    	   addCriterion("ext_table", value, ConditionMode.FUZZY, "extTable", "java.lang.String", "String");
    	   return this;
      }

      public DmsTableTreeSQL andExtTableNotLike(java.lang.String value) {
          addCriterion("ext_table", value, ConditionMode.NOT_FUZZY, "extTable", "java.lang.String", "String");
          return this;
      }
      public DmsTableTreeSQL andExtTableEqualTo(java.lang.String value) {
          addCriterion("ext_table", value, ConditionMode.EQUAL, "extTable", "java.lang.String", "String");
          return this;
      }

      public DmsTableTreeSQL andExtTableNotEqualTo(java.lang.String value) {
          addCriterion("ext_table", value, ConditionMode.NOT_EQUAL, "extTable", "java.lang.String", "String");
          return this;
      }

      public DmsTableTreeSQL andExtTableGreaterThan(java.lang.String value) {
          addCriterion("ext_table", value, ConditionMode.GREATER_THEN, "extTable", "java.lang.String", "String");
          return this;
      }

      public DmsTableTreeSQL andExtTableGreaterThanOrEqualTo(java.lang.String value) {
          addCriterion("ext_table", value, ConditionMode.GREATER_EQUAL, "extTable", "java.lang.String", "String");
          return this;
      }

      public DmsTableTreeSQL andExtTableLessThan(java.lang.String value) {
          addCriterion("ext_table", value, ConditionMode.LESS_THEN, "extTable", "java.lang.String", "String");
          return this;
      }

      public DmsTableTreeSQL andExtTableLessThanOrEqualTo(java.lang.String value) {
          addCriterion("ext_table", value, ConditionMode.LESS_EQUAL, "extTable", "java.lang.String", "String");
          return this;
      }

      public DmsTableTreeSQL andExtTableBetween(java.lang.String value1, java.lang.String value2) {
    	  addCriterion("ext_table", value1, value2, ConditionMode.BETWEEN, "extTable", "java.lang.String", "String");
    	  return this;
      }

      public DmsTableTreeSQL andExtTableNotBetween(java.lang.String value1, java.lang.String value2) {
          addCriterion("ext_table", value1, value2, ConditionMode.NOT_BETWEEN, "extTable", "java.lang.String", "String");
          return this;
      }
        
      public DmsTableTreeSQL andExtTableIn(List<java.lang.String> values) {
          addCriterion("ext_table", values, ConditionMode.IN, "extTable", "java.lang.String", "String");
          return this;
      }

      public DmsTableTreeSQL andExtTableNotIn(List<java.lang.String> values) {
          addCriterion("ext_table", values, ConditionMode.NOT_IN, "extTable", "java.lang.String", "String");
          return this;
      }
	public DmsTableTreeSQL andExtModelIdNameIsNull() {
		isnull("ext_model_id_name");
		return this;
	}
	
	public DmsTableTreeSQL andExtModelIdNameIsNotNull() {
		notNull("ext_model_id_name");
		return this;
	}
	
	public DmsTableTreeSQL andExtModelIdNameIsEmpty() {
		empty("ext_model_id_name");
		return this;
	}

	public DmsTableTreeSQL andExtModelIdNameIsNotEmpty() {
		notEmpty("ext_model_id_name");
		return this;
	}
       public DmsTableTreeSQL andExtModelIdNameLike(java.lang.String value) {
    	   addCriterion("ext_model_id_name", value, ConditionMode.FUZZY, "extModelIdName", "java.lang.String", "String");
    	   return this;
      }

      public DmsTableTreeSQL andExtModelIdNameNotLike(java.lang.String value) {
          addCriterion("ext_model_id_name", value, ConditionMode.NOT_FUZZY, "extModelIdName", "java.lang.String", "String");
          return this;
      }
      public DmsTableTreeSQL andExtModelIdNameEqualTo(java.lang.String value) {
          addCriterion("ext_model_id_name", value, ConditionMode.EQUAL, "extModelIdName", "java.lang.String", "String");
          return this;
      }

      public DmsTableTreeSQL andExtModelIdNameNotEqualTo(java.lang.String value) {
          addCriterion("ext_model_id_name", value, ConditionMode.NOT_EQUAL, "extModelIdName", "java.lang.String", "String");
          return this;
      }

      public DmsTableTreeSQL andExtModelIdNameGreaterThan(java.lang.String value) {
          addCriterion("ext_model_id_name", value, ConditionMode.GREATER_THEN, "extModelIdName", "java.lang.String", "String");
          return this;
      }

      public DmsTableTreeSQL andExtModelIdNameGreaterThanOrEqualTo(java.lang.String value) {
          addCriterion("ext_model_id_name", value, ConditionMode.GREATER_EQUAL, "extModelIdName", "java.lang.String", "String");
          return this;
      }

      public DmsTableTreeSQL andExtModelIdNameLessThan(java.lang.String value) {
          addCriterion("ext_model_id_name", value, ConditionMode.LESS_THEN, "extModelIdName", "java.lang.String", "String");
          return this;
      }

      public DmsTableTreeSQL andExtModelIdNameLessThanOrEqualTo(java.lang.String value) {
          addCriterion("ext_model_id_name", value, ConditionMode.LESS_EQUAL, "extModelIdName", "java.lang.String", "String");
          return this;
      }

      public DmsTableTreeSQL andExtModelIdNameBetween(java.lang.String value1, java.lang.String value2) {
    	  addCriterion("ext_model_id_name", value1, value2, ConditionMode.BETWEEN, "extModelIdName", "java.lang.String", "String");
    	  return this;
      }

      public DmsTableTreeSQL andExtModelIdNameNotBetween(java.lang.String value1, java.lang.String value2) {
          addCriterion("ext_model_id_name", value1, value2, ConditionMode.NOT_BETWEEN, "extModelIdName", "java.lang.String", "String");
          return this;
      }
        
      public DmsTableTreeSQL andExtModelIdNameIn(List<java.lang.String> values) {
          addCriterion("ext_model_id_name", values, ConditionMode.IN, "extModelIdName", "java.lang.String", "String");
          return this;
      }

      public DmsTableTreeSQL andExtModelIdNameNotIn(List<java.lang.String> values) {
          addCriterion("ext_model_id_name", values, ConditionMode.NOT_IN, "extModelIdName", "java.lang.String", "String");
          return this;
      }
	public DmsTableTreeSQL andExtParentIdNameIsNull() {
		isnull("ext_parent_id_name");
		return this;
	}
	
	public DmsTableTreeSQL andExtParentIdNameIsNotNull() {
		notNull("ext_parent_id_name");
		return this;
	}
	
	public DmsTableTreeSQL andExtParentIdNameIsEmpty() {
		empty("ext_parent_id_name");
		return this;
	}

	public DmsTableTreeSQL andExtParentIdNameIsNotEmpty() {
		notEmpty("ext_parent_id_name");
		return this;
	}
       public DmsTableTreeSQL andExtParentIdNameLike(java.lang.String value) {
    	   addCriterion("ext_parent_id_name", value, ConditionMode.FUZZY, "extParentIdName", "java.lang.String", "String");
    	   return this;
      }

      public DmsTableTreeSQL andExtParentIdNameNotLike(java.lang.String value) {
          addCriterion("ext_parent_id_name", value, ConditionMode.NOT_FUZZY, "extParentIdName", "java.lang.String", "String");
          return this;
      }
      public DmsTableTreeSQL andExtParentIdNameEqualTo(java.lang.String value) {
          addCriterion("ext_parent_id_name", value, ConditionMode.EQUAL, "extParentIdName", "java.lang.String", "String");
          return this;
      }

      public DmsTableTreeSQL andExtParentIdNameNotEqualTo(java.lang.String value) {
          addCriterion("ext_parent_id_name", value, ConditionMode.NOT_EQUAL, "extParentIdName", "java.lang.String", "String");
          return this;
      }

      public DmsTableTreeSQL andExtParentIdNameGreaterThan(java.lang.String value) {
          addCriterion("ext_parent_id_name", value, ConditionMode.GREATER_THEN, "extParentIdName", "java.lang.String", "String");
          return this;
      }

      public DmsTableTreeSQL andExtParentIdNameGreaterThanOrEqualTo(java.lang.String value) {
          addCriterion("ext_parent_id_name", value, ConditionMode.GREATER_EQUAL, "extParentIdName", "java.lang.String", "String");
          return this;
      }

      public DmsTableTreeSQL andExtParentIdNameLessThan(java.lang.String value) {
          addCriterion("ext_parent_id_name", value, ConditionMode.LESS_THEN, "extParentIdName", "java.lang.String", "String");
          return this;
      }

      public DmsTableTreeSQL andExtParentIdNameLessThanOrEqualTo(java.lang.String value) {
          addCriterion("ext_parent_id_name", value, ConditionMode.LESS_EQUAL, "extParentIdName", "java.lang.String", "String");
          return this;
      }

      public DmsTableTreeSQL andExtParentIdNameBetween(java.lang.String value1, java.lang.String value2) {
    	  addCriterion("ext_parent_id_name", value1, value2, ConditionMode.BETWEEN, "extParentIdName", "java.lang.String", "String");
    	  return this;
      }

      public DmsTableTreeSQL andExtParentIdNameNotBetween(java.lang.String value1, java.lang.String value2) {
          addCriterion("ext_parent_id_name", value1, value2, ConditionMode.NOT_BETWEEN, "extParentIdName", "java.lang.String", "String");
          return this;
      }
        
      public DmsTableTreeSQL andExtParentIdNameIn(List<java.lang.String> values) {
          addCriterion("ext_parent_id_name", values, ConditionMode.IN, "extParentIdName", "java.lang.String", "String");
          return this;
      }

      public DmsTableTreeSQL andExtParentIdNameNotIn(List<java.lang.String> values) {
          addCriterion("ext_parent_id_name", values, ConditionMode.NOT_IN, "extParentIdName", "java.lang.String", "String");
          return this;
      }
	public DmsTableTreeSQL andExtPathNameIsNull() {
		isnull("ext_path_name");
		return this;
	}
	
	public DmsTableTreeSQL andExtPathNameIsNotNull() {
		notNull("ext_path_name");
		return this;
	}
	
	public DmsTableTreeSQL andExtPathNameIsEmpty() {
		empty("ext_path_name");
		return this;
	}

	public DmsTableTreeSQL andExtPathNameIsNotEmpty() {
		notEmpty("ext_path_name");
		return this;
	}
       public DmsTableTreeSQL andExtPathNameLike(java.lang.String value) {
    	   addCriterion("ext_path_name", value, ConditionMode.FUZZY, "extPathName", "java.lang.String", "String");
    	   return this;
      }

      public DmsTableTreeSQL andExtPathNameNotLike(java.lang.String value) {
          addCriterion("ext_path_name", value, ConditionMode.NOT_FUZZY, "extPathName", "java.lang.String", "String");
          return this;
      }
      public DmsTableTreeSQL andExtPathNameEqualTo(java.lang.String value) {
          addCriterion("ext_path_name", value, ConditionMode.EQUAL, "extPathName", "java.lang.String", "String");
          return this;
      }

      public DmsTableTreeSQL andExtPathNameNotEqualTo(java.lang.String value) {
          addCriterion("ext_path_name", value, ConditionMode.NOT_EQUAL, "extPathName", "java.lang.String", "String");
          return this;
      }

      public DmsTableTreeSQL andExtPathNameGreaterThan(java.lang.String value) {
          addCriterion("ext_path_name", value, ConditionMode.GREATER_THEN, "extPathName", "java.lang.String", "String");
          return this;
      }

      public DmsTableTreeSQL andExtPathNameGreaterThanOrEqualTo(java.lang.String value) {
          addCriterion("ext_path_name", value, ConditionMode.GREATER_EQUAL, "extPathName", "java.lang.String", "String");
          return this;
      }

      public DmsTableTreeSQL andExtPathNameLessThan(java.lang.String value) {
          addCriterion("ext_path_name", value, ConditionMode.LESS_THEN, "extPathName", "java.lang.String", "String");
          return this;
      }

      public DmsTableTreeSQL andExtPathNameLessThanOrEqualTo(java.lang.String value) {
          addCriterion("ext_path_name", value, ConditionMode.LESS_EQUAL, "extPathName", "java.lang.String", "String");
          return this;
      }

      public DmsTableTreeSQL andExtPathNameBetween(java.lang.String value1, java.lang.String value2) {
    	  addCriterion("ext_path_name", value1, value2, ConditionMode.BETWEEN, "extPathName", "java.lang.String", "String");
    	  return this;
      }

      public DmsTableTreeSQL andExtPathNameNotBetween(java.lang.String value1, java.lang.String value2) {
          addCriterion("ext_path_name", value1, value2, ConditionMode.NOT_BETWEEN, "extPathName", "java.lang.String", "String");
          return this;
      }
        
      public DmsTableTreeSQL andExtPathNameIn(List<java.lang.String> values) {
          addCriterion("ext_path_name", values, ConditionMode.IN, "extPathName", "java.lang.String", "String");
          return this;
      }

      public DmsTableTreeSQL andExtPathNameNotIn(List<java.lang.String> values) {
          addCriterion("ext_path_name", values, ConditionMode.NOT_IN, "extPathName", "java.lang.String", "String");
          return this;
      }
	public DmsTableTreeSQL andExtLevelNameIsNull() {
		isnull("ext_level_name");
		return this;
	}
	
	public DmsTableTreeSQL andExtLevelNameIsNotNull() {
		notNull("ext_level_name");
		return this;
	}
	
	public DmsTableTreeSQL andExtLevelNameIsEmpty() {
		empty("ext_level_name");
		return this;
	}

	public DmsTableTreeSQL andExtLevelNameIsNotEmpty() {
		notEmpty("ext_level_name");
		return this;
	}
       public DmsTableTreeSQL andExtLevelNameLike(java.lang.String value) {
    	   addCriterion("ext_level_name", value, ConditionMode.FUZZY, "extLevelName", "java.lang.String", "String");
    	   return this;
      }

      public DmsTableTreeSQL andExtLevelNameNotLike(java.lang.String value) {
          addCriterion("ext_level_name", value, ConditionMode.NOT_FUZZY, "extLevelName", "java.lang.String", "String");
          return this;
      }
      public DmsTableTreeSQL andExtLevelNameEqualTo(java.lang.String value) {
          addCriterion("ext_level_name", value, ConditionMode.EQUAL, "extLevelName", "java.lang.String", "String");
          return this;
      }

      public DmsTableTreeSQL andExtLevelNameNotEqualTo(java.lang.String value) {
          addCriterion("ext_level_name", value, ConditionMode.NOT_EQUAL, "extLevelName", "java.lang.String", "String");
          return this;
      }

      public DmsTableTreeSQL andExtLevelNameGreaterThan(java.lang.String value) {
          addCriterion("ext_level_name", value, ConditionMode.GREATER_THEN, "extLevelName", "java.lang.String", "String");
          return this;
      }

      public DmsTableTreeSQL andExtLevelNameGreaterThanOrEqualTo(java.lang.String value) {
          addCriterion("ext_level_name", value, ConditionMode.GREATER_EQUAL, "extLevelName", "java.lang.String", "String");
          return this;
      }

      public DmsTableTreeSQL andExtLevelNameLessThan(java.lang.String value) {
          addCriterion("ext_level_name", value, ConditionMode.LESS_THEN, "extLevelName", "java.lang.String", "String");
          return this;
      }

      public DmsTableTreeSQL andExtLevelNameLessThanOrEqualTo(java.lang.String value) {
          addCriterion("ext_level_name", value, ConditionMode.LESS_EQUAL, "extLevelName", "java.lang.String", "String");
          return this;
      }

      public DmsTableTreeSQL andExtLevelNameBetween(java.lang.String value1, java.lang.String value2) {
    	  addCriterion("ext_level_name", value1, value2, ConditionMode.BETWEEN, "extLevelName", "java.lang.String", "String");
    	  return this;
      }

      public DmsTableTreeSQL andExtLevelNameNotBetween(java.lang.String value1, java.lang.String value2) {
          addCriterion("ext_level_name", value1, value2, ConditionMode.NOT_BETWEEN, "extLevelName", "java.lang.String", "String");
          return this;
      }
        
      public DmsTableTreeSQL andExtLevelNameIn(List<java.lang.String> values) {
          addCriterion("ext_level_name", values, ConditionMode.IN, "extLevelName", "java.lang.String", "String");
          return this;
      }

      public DmsTableTreeSQL andExtLevelNameNotIn(List<java.lang.String> values) {
          addCriterion("ext_level_name", values, ConditionMode.NOT_IN, "extLevelName", "java.lang.String", "String");
          return this;
      }
	public DmsTableTreeSQL andExtLeafNameIsNull() {
		isnull("ext_leaf_name");
		return this;
	}
	
	public DmsTableTreeSQL andExtLeafNameIsNotNull() {
		notNull("ext_leaf_name");
		return this;
	}
	
	public DmsTableTreeSQL andExtLeafNameIsEmpty() {
		empty("ext_leaf_name");
		return this;
	}

	public DmsTableTreeSQL andExtLeafNameIsNotEmpty() {
		notEmpty("ext_leaf_name");
		return this;
	}
       public DmsTableTreeSQL andExtLeafNameLike(java.lang.String value) {
    	   addCriterion("ext_leaf_name", value, ConditionMode.FUZZY, "extLeafName", "java.lang.String", "String");
    	   return this;
      }

      public DmsTableTreeSQL andExtLeafNameNotLike(java.lang.String value) {
          addCriterion("ext_leaf_name", value, ConditionMode.NOT_FUZZY, "extLeafName", "java.lang.String", "String");
          return this;
      }
      public DmsTableTreeSQL andExtLeafNameEqualTo(java.lang.String value) {
          addCriterion("ext_leaf_name", value, ConditionMode.EQUAL, "extLeafName", "java.lang.String", "String");
          return this;
      }

      public DmsTableTreeSQL andExtLeafNameNotEqualTo(java.lang.String value) {
          addCriterion("ext_leaf_name", value, ConditionMode.NOT_EQUAL, "extLeafName", "java.lang.String", "String");
          return this;
      }

      public DmsTableTreeSQL andExtLeafNameGreaterThan(java.lang.String value) {
          addCriterion("ext_leaf_name", value, ConditionMode.GREATER_THEN, "extLeafName", "java.lang.String", "String");
          return this;
      }

      public DmsTableTreeSQL andExtLeafNameGreaterThanOrEqualTo(java.lang.String value) {
          addCriterion("ext_leaf_name", value, ConditionMode.GREATER_EQUAL, "extLeafName", "java.lang.String", "String");
          return this;
      }

      public DmsTableTreeSQL andExtLeafNameLessThan(java.lang.String value) {
          addCriterion("ext_leaf_name", value, ConditionMode.LESS_THEN, "extLeafName", "java.lang.String", "String");
          return this;
      }

      public DmsTableTreeSQL andExtLeafNameLessThanOrEqualTo(java.lang.String value) {
          addCriterion("ext_leaf_name", value, ConditionMode.LESS_EQUAL, "extLeafName", "java.lang.String", "String");
          return this;
      }

      public DmsTableTreeSQL andExtLeafNameBetween(java.lang.String value1, java.lang.String value2) {
    	  addCriterion("ext_leaf_name", value1, value2, ConditionMode.BETWEEN, "extLeafName", "java.lang.String", "String");
    	  return this;
      }

      public DmsTableTreeSQL andExtLeafNameNotBetween(java.lang.String value1, java.lang.String value2) {
          addCriterion("ext_leaf_name", value1, value2, ConditionMode.NOT_BETWEEN, "extLeafName", "java.lang.String", "String");
          return this;
      }
        
      public DmsTableTreeSQL andExtLeafNameIn(List<java.lang.String> values) {
          addCriterion("ext_leaf_name", values, ConditionMode.IN, "extLeafName", "java.lang.String", "String");
          return this;
      }

      public DmsTableTreeSQL andExtLeafNameNotIn(List<java.lang.String> values) {
          addCriterion("ext_leaf_name", values, ConditionMode.NOT_IN, "extLeafName", "java.lang.String", "String");
          return this;
      }
	public DmsTableTreeSQL andStatusIsNull() {
		isnull("status");
		return this;
	}
	
	public DmsTableTreeSQL andStatusIsNotNull() {
		notNull("status");
		return this;
	}
	
	public DmsTableTreeSQL andStatusIsEmpty() {
		empty("status");
		return this;
	}

	public DmsTableTreeSQL andStatusIsNotEmpty() {
		notEmpty("status");
		return this;
	}
      public DmsTableTreeSQL andStatusEqualTo(java.lang.Integer value) {
          addCriterion("status", value, ConditionMode.EQUAL, "status", "java.lang.Integer", "Float");
          return this;
      }

      public DmsTableTreeSQL andStatusNotEqualTo(java.lang.Integer value) {
          addCriterion("status", value, ConditionMode.NOT_EQUAL, "status", "java.lang.Integer", "Float");
          return this;
      }

      public DmsTableTreeSQL andStatusGreaterThan(java.lang.Integer value) {
          addCriterion("status", value, ConditionMode.GREATER_THEN, "status", "java.lang.Integer", "Float");
          return this;
      }

      public DmsTableTreeSQL andStatusGreaterThanOrEqualTo(java.lang.Integer value) {
          addCriterion("status", value, ConditionMode.GREATER_EQUAL, "status", "java.lang.Integer", "Float");
          return this;
      }

      public DmsTableTreeSQL andStatusLessThan(java.lang.Integer value) {
          addCriterion("status", value, ConditionMode.LESS_THEN, "status", "java.lang.Integer", "Float");
          return this;
      }

      public DmsTableTreeSQL andStatusLessThanOrEqualTo(java.lang.Integer value) {
          addCriterion("status", value, ConditionMode.LESS_EQUAL, "status", "java.lang.Integer", "Float");
          return this;
      }

      public DmsTableTreeSQL andStatusBetween(java.lang.Integer value1, java.lang.Integer value2) {
    	  addCriterion("status", value1, value2, ConditionMode.BETWEEN, "status", "java.lang.Integer", "Float");
    	  return this;
      }

      public DmsTableTreeSQL andStatusNotBetween(java.lang.Integer value1, java.lang.Integer value2) {
          addCriterion("status", value1, value2, ConditionMode.NOT_BETWEEN, "status", "java.lang.Integer", "Float");
          return this;
      }
        
      public DmsTableTreeSQL andStatusIn(List<java.lang.Integer> values) {
          addCriterion("status", values, ConditionMode.IN, "status", "java.lang.Integer", "Float");
          return this;
      }

      public DmsTableTreeSQL andStatusNotIn(List<java.lang.Integer> values) {
          addCriterion("status", values, ConditionMode.NOT_IN, "status", "java.lang.Integer", "Float");
          return this;
      }
	public DmsTableTreeSQL andTopNameIsNull() {
		isnull("top_name");
		return this;
	}
	
	public DmsTableTreeSQL andTopNameIsNotNull() {
		notNull("top_name");
		return this;
	}
	
	public DmsTableTreeSQL andTopNameIsEmpty() {
		empty("top_name");
		return this;
	}

	public DmsTableTreeSQL andTopNameIsNotEmpty() {
		notEmpty("top_name");
		return this;
	}
       public DmsTableTreeSQL andTopNameLike(java.lang.String value) {
    	   addCriterion("top_name", value, ConditionMode.FUZZY, "topName", "java.lang.String", "Float");
    	   return this;
      }

      public DmsTableTreeSQL andTopNameNotLike(java.lang.String value) {
          addCriterion("top_name", value, ConditionMode.NOT_FUZZY, "topName", "java.lang.String", "Float");
          return this;
      }
      public DmsTableTreeSQL andTopNameEqualTo(java.lang.String value) {
          addCriterion("top_name", value, ConditionMode.EQUAL, "topName", "java.lang.String", "String");
          return this;
      }

      public DmsTableTreeSQL andTopNameNotEqualTo(java.lang.String value) {
          addCriterion("top_name", value, ConditionMode.NOT_EQUAL, "topName", "java.lang.String", "String");
          return this;
      }

      public DmsTableTreeSQL andTopNameGreaterThan(java.lang.String value) {
          addCriterion("top_name", value, ConditionMode.GREATER_THEN, "topName", "java.lang.String", "String");
          return this;
      }

      public DmsTableTreeSQL andTopNameGreaterThanOrEqualTo(java.lang.String value) {
          addCriterion("top_name", value, ConditionMode.GREATER_EQUAL, "topName", "java.lang.String", "String");
          return this;
      }

      public DmsTableTreeSQL andTopNameLessThan(java.lang.String value) {
          addCriterion("top_name", value, ConditionMode.LESS_THEN, "topName", "java.lang.String", "String");
          return this;
      }

      public DmsTableTreeSQL andTopNameLessThanOrEqualTo(java.lang.String value) {
          addCriterion("top_name", value, ConditionMode.LESS_EQUAL, "topName", "java.lang.String", "String");
          return this;
      }

      public DmsTableTreeSQL andTopNameBetween(java.lang.String value1, java.lang.String value2) {
    	  addCriterion("top_name", value1, value2, ConditionMode.BETWEEN, "topName", "java.lang.String", "String");
    	  return this;
      }

      public DmsTableTreeSQL andTopNameNotBetween(java.lang.String value1, java.lang.String value2) {
          addCriterion("top_name", value1, value2, ConditionMode.NOT_BETWEEN, "topName", "java.lang.String", "String");
          return this;
      }
        
      public DmsTableTreeSQL andTopNameIn(List<java.lang.String> values) {
          addCriterion("top_name", values, ConditionMode.IN, "topName", "java.lang.String", "String");
          return this;
      }

      public DmsTableTreeSQL andTopNameNotIn(List<java.lang.String> values) {
          addCriterion("top_name", values, ConditionMode.NOT_IN, "topName", "java.lang.String", "String");
          return this;
      }
	public DmsTableTreeSQL andExtTopNameIsNull() {
		isnull("ext_top_name");
		return this;
	}
	
	public DmsTableTreeSQL andExtTopNameIsNotNull() {
		notNull("ext_top_name");
		return this;
	}
	
	public DmsTableTreeSQL andExtTopNameIsEmpty() {
		empty("ext_top_name");
		return this;
	}

	public DmsTableTreeSQL andExtTopNameIsNotEmpty() {
		notEmpty("ext_top_name");
		return this;
	}
       public DmsTableTreeSQL andExtTopNameLike(java.lang.String value) {
    	   addCriterion("ext_top_name", value, ConditionMode.FUZZY, "extTopName", "java.lang.String", "String");
    	   return this;
      }

      public DmsTableTreeSQL andExtTopNameNotLike(java.lang.String value) {
          addCriterion("ext_top_name", value, ConditionMode.NOT_FUZZY, "extTopName", "java.lang.String", "String");
          return this;
      }
      public DmsTableTreeSQL andExtTopNameEqualTo(java.lang.String value) {
          addCriterion("ext_top_name", value, ConditionMode.EQUAL, "extTopName", "java.lang.String", "String");
          return this;
      }

      public DmsTableTreeSQL andExtTopNameNotEqualTo(java.lang.String value) {
          addCriterion("ext_top_name", value, ConditionMode.NOT_EQUAL, "extTopName", "java.lang.String", "String");
          return this;
      }

      public DmsTableTreeSQL andExtTopNameGreaterThan(java.lang.String value) {
          addCriterion("ext_top_name", value, ConditionMode.GREATER_THEN, "extTopName", "java.lang.String", "String");
          return this;
      }

      public DmsTableTreeSQL andExtTopNameGreaterThanOrEqualTo(java.lang.String value) {
          addCriterion("ext_top_name", value, ConditionMode.GREATER_EQUAL, "extTopName", "java.lang.String", "String");
          return this;
      }

      public DmsTableTreeSQL andExtTopNameLessThan(java.lang.String value) {
          addCriterion("ext_top_name", value, ConditionMode.LESS_THEN, "extTopName", "java.lang.String", "String");
          return this;
      }

      public DmsTableTreeSQL andExtTopNameLessThanOrEqualTo(java.lang.String value) {
          addCriterion("ext_top_name", value, ConditionMode.LESS_EQUAL, "extTopName", "java.lang.String", "String");
          return this;
      }

      public DmsTableTreeSQL andExtTopNameBetween(java.lang.String value1, java.lang.String value2) {
    	  addCriterion("ext_top_name", value1, value2, ConditionMode.BETWEEN, "extTopName", "java.lang.String", "String");
    	  return this;
      }

      public DmsTableTreeSQL andExtTopNameNotBetween(java.lang.String value1, java.lang.String value2) {
          addCriterion("ext_top_name", value1, value2, ConditionMode.NOT_BETWEEN, "extTopName", "java.lang.String", "String");
          return this;
      }
        
      public DmsTableTreeSQL andExtTopNameIn(List<java.lang.String> values) {
          addCriterion("ext_top_name", values, ConditionMode.IN, "extTopName", "java.lang.String", "String");
          return this;
      }

      public DmsTableTreeSQL andExtTopNameNotIn(List<java.lang.String> values) {
          addCriterion("ext_top_name", values, ConditionMode.NOT_IN, "extTopName", "java.lang.String", "String");
          return this;
      }
	public DmsTableTreeSQL andTopValueIsNull() {
		isnull("top_value");
		return this;
	}
	
	public DmsTableTreeSQL andTopValueIsNotNull() {
		notNull("top_value");
		return this;
	}
	
	public DmsTableTreeSQL andTopValueIsEmpty() {
		empty("top_value");
		return this;
	}

	public DmsTableTreeSQL andTopValueIsNotEmpty() {
		notEmpty("top_value");
		return this;
	}
       public DmsTableTreeSQL andTopValueLike(java.lang.String value) {
    	   addCriterion("top_value", value, ConditionMode.FUZZY, "topValue", "java.lang.String", "String");
    	   return this;
      }

      public DmsTableTreeSQL andTopValueNotLike(java.lang.String value) {
          addCriterion("top_value", value, ConditionMode.NOT_FUZZY, "topValue", "java.lang.String", "String");
          return this;
      }
      public DmsTableTreeSQL andTopValueEqualTo(java.lang.String value) {
          addCriterion("top_value", value, ConditionMode.EQUAL, "topValue", "java.lang.String", "String");
          return this;
      }

      public DmsTableTreeSQL andTopValueNotEqualTo(java.lang.String value) {
          addCriterion("top_value", value, ConditionMode.NOT_EQUAL, "topValue", "java.lang.String", "String");
          return this;
      }

      public DmsTableTreeSQL andTopValueGreaterThan(java.lang.String value) {
          addCriterion("top_value", value, ConditionMode.GREATER_THEN, "topValue", "java.lang.String", "String");
          return this;
      }

      public DmsTableTreeSQL andTopValueGreaterThanOrEqualTo(java.lang.String value) {
          addCriterion("top_value", value, ConditionMode.GREATER_EQUAL, "topValue", "java.lang.String", "String");
          return this;
      }

      public DmsTableTreeSQL andTopValueLessThan(java.lang.String value) {
          addCriterion("top_value", value, ConditionMode.LESS_THEN, "topValue", "java.lang.String", "String");
          return this;
      }

      public DmsTableTreeSQL andTopValueLessThanOrEqualTo(java.lang.String value) {
          addCriterion("top_value", value, ConditionMode.LESS_EQUAL, "topValue", "java.lang.String", "String");
          return this;
      }

      public DmsTableTreeSQL andTopValueBetween(java.lang.String value1, java.lang.String value2) {
    	  addCriterion("top_value", value1, value2, ConditionMode.BETWEEN, "topValue", "java.lang.String", "String");
    	  return this;
      }

      public DmsTableTreeSQL andTopValueNotBetween(java.lang.String value1, java.lang.String value2) {
          addCriterion("top_value", value1, value2, ConditionMode.NOT_BETWEEN, "topValue", "java.lang.String", "String");
          return this;
      }
        
      public DmsTableTreeSQL andTopValueIn(List<java.lang.String> values) {
          addCriterion("top_value", values, ConditionMode.IN, "topValue", "java.lang.String", "String");
          return this;
      }

      public DmsTableTreeSQL andTopValueNotIn(List<java.lang.String> values) {
          addCriterion("top_value", values, ConditionMode.NOT_IN, "topValue", "java.lang.String", "String");
          return this;
      }
	public DmsTableTreeSQL andLeafValueIsNull() {
		isnull("leaf_value");
		return this;
	}
	
	public DmsTableTreeSQL andLeafValueIsNotNull() {
		notNull("leaf_value");
		return this;
	}
	
	public DmsTableTreeSQL andLeafValueIsEmpty() {
		empty("leaf_value");
		return this;
	}

	public DmsTableTreeSQL andLeafValueIsNotEmpty() {
		notEmpty("leaf_value");
		return this;
	}
       public DmsTableTreeSQL andLeafValueLike(java.lang.String value) {
    	   addCriterion("leaf_value", value, ConditionMode.FUZZY, "leafValue", "java.lang.String", "String");
    	   return this;
      }

      public DmsTableTreeSQL andLeafValueNotLike(java.lang.String value) {
          addCriterion("leaf_value", value, ConditionMode.NOT_FUZZY, "leafValue", "java.lang.String", "String");
          return this;
      }
      public DmsTableTreeSQL andLeafValueEqualTo(java.lang.String value) {
          addCriterion("leaf_value", value, ConditionMode.EQUAL, "leafValue", "java.lang.String", "String");
          return this;
      }

      public DmsTableTreeSQL andLeafValueNotEqualTo(java.lang.String value) {
          addCriterion("leaf_value", value, ConditionMode.NOT_EQUAL, "leafValue", "java.lang.String", "String");
          return this;
      }

      public DmsTableTreeSQL andLeafValueGreaterThan(java.lang.String value) {
          addCriterion("leaf_value", value, ConditionMode.GREATER_THEN, "leafValue", "java.lang.String", "String");
          return this;
      }

      public DmsTableTreeSQL andLeafValueGreaterThanOrEqualTo(java.lang.String value) {
          addCriterion("leaf_value", value, ConditionMode.GREATER_EQUAL, "leafValue", "java.lang.String", "String");
          return this;
      }

      public DmsTableTreeSQL andLeafValueLessThan(java.lang.String value) {
          addCriterion("leaf_value", value, ConditionMode.LESS_THEN, "leafValue", "java.lang.String", "String");
          return this;
      }

      public DmsTableTreeSQL andLeafValueLessThanOrEqualTo(java.lang.String value) {
          addCriterion("leaf_value", value, ConditionMode.LESS_EQUAL, "leafValue", "java.lang.String", "String");
          return this;
      }

      public DmsTableTreeSQL andLeafValueBetween(java.lang.String value1, java.lang.String value2) {
    	  addCriterion("leaf_value", value1, value2, ConditionMode.BETWEEN, "leafValue", "java.lang.String", "String");
    	  return this;
      }

      public DmsTableTreeSQL andLeafValueNotBetween(java.lang.String value1, java.lang.String value2) {
          addCriterion("leaf_value", value1, value2, ConditionMode.NOT_BETWEEN, "leafValue", "java.lang.String", "String");
          return this;
      }
        
      public DmsTableTreeSQL andLeafValueIn(List<java.lang.String> values) {
          addCriterion("leaf_value", values, ConditionMode.IN, "leafValue", "java.lang.String", "String");
          return this;
      }

      public DmsTableTreeSQL andLeafValueNotIn(List<java.lang.String> values) {
          addCriterion("leaf_value", values, ConditionMode.NOT_IN, "leafValue", "java.lang.String", "String");
          return this;
      }
}