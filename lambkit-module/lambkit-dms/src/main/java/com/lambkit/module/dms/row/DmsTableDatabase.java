/**
 * Copyright (c) 2015-2017, Henry Yang 杨勇 (gismail@foxmail.com).
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.lambkit.module.dms.row;

import com.lambkit.db.RowModel;

/**
 * @author yangyong 
 * @website: www.lambkit.com
 * @email: gismail@foxmail.com
 * @date ${date}
 * @version 1.0
 * @since 1.0
 */

public class DmsTableDatabase extends RowModel<DmsTableDatabase> {
	public DmsTableDatabase() {
		setTableName("dms_table_database");
		setPrimaryKey("id");
	}
	public java.lang.Integer getId() {
		return this.get("id");
	}
	public void setId(java.lang.Integer id) {
		this.set("id", id);
	}
	public java.lang.Integer getDbid() {
		return this.get("dbid");
	}
	public void setDbid(java.lang.Integer dbid) {
		this.set("dbid", dbid);
	}
	public java.lang.Integer getTbid() {
		return this.get("tbid");
	}
	public void setTbid(java.lang.Integer tbid) {
		this.set("tbid", tbid);
	}
}
