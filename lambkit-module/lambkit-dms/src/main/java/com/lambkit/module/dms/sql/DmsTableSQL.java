/**
 * Copyright (c) 2015-2017, Henry Yang 杨勇 (gismail@foxmail.com).
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.lambkit.module.dms.sql;

import com.lambkit.db.sql.ConditionMode;
import com.lambkit.db.sql.Column;
import com.lambkit.db.sql.Columns;
import com.lambkit.db.sql.Example;

import java.io.Serializable;
import java.util.List;

/**
 * @author yangyong 
 * @website: www.lambkit.com
 * @email: gismail@foxmail.com
 * @date ${date}
 * @version 1.0
 * @since 1.0
 */

public class DmsTableSQL extends Columns implements Serializable {
	private static final long serialVersionUID = 1L;
	
	public static DmsTableSQL of() {
		return new DmsTableSQL();
	}
	
	public static DmsTableSQL by(Column column) {
		DmsTableSQL that = new DmsTableSQL();
		that.add(column);
        return that;
    }

    public static DmsTableSQL by(String name, Object value) {
        return (DmsTableSQL) create().eq(name, value);
    }
    
    public Example example() {
    	return Example.create("dms_table", this);
    }
    
    /**
     * equals
     *
     * @param name
     * @param value
     * @return
     */
    public DmsTableSQL eq(String name, Object value) {
    	super.eq(name, value);
        return this;
    }

    /**
     * not equals !=
     *
     * @param name
     * @param value
     * @return
     */
    public DmsTableSQL ne(String name, Object value) {
    	super.ne(name, value);
        return this;
    }


    /**
     * like
     *
     * @param name
     * @param value
     * @return
     */

    public DmsTableSQL like(String name, Object value) {
    	super.like(name, value);
        return this;
    }
    
    public DmsTableSQL notLike(String name, Object value) {
    	super.notLike(name, value);
        return this;
    }

    /**
     * 大于 great than
     *
     * @param name
     * @param value
     * @return
     */
    public DmsTableSQL gt(String name, Object value) {
    	super.gt(name, value);
        return this;
    }

    /**
     * 大于等于 great or equal
     *
     * @param name
     * @param value
     * @return
     */
    public DmsTableSQL ge(String name, Object value) {
    	super.ge(name, value);
        return this;
    }

    /**
     * 小于 less than
     *
     * @param name
     * @param value
     * @return
     */
    public DmsTableSQL lt(String name, Object value) {
    	super.lt(name, value);
        return this;
    }

    /**
     * 小于等于 less or equal
     *
     * @param name
     * @param value
     * @return
     */
    public DmsTableSQL le(String name, Object value) {
    	super.le(name, value);
        return this;
    }
    
    public DmsTableSQL isnull(String name) {
    	super.isnull(name);
        return this;
    } 

    public DmsTableSQL notNull(String name) {
    	super.notNull(name);
        return this;
    } 
    
    public DmsTableSQL empty(String name) {
    	super.empty(name);
        return this;
    } 
    
    public DmsTableSQL notEmpty(String name) {
    	super.notEmpty(name);
        return this;
    } 
    
    public DmsTableSQL add(Column column) {
    	super.add(column);
    	return this;
    }
    
    /**************************/
	
	public void addCriterion(String name, Object value, ConditionMode logic, String property, String typeHandler, String valueType) {
		 if (value == null) {
			 throw new RuntimeException("Value for " + property + " cannot be null");
		 }
		 add(Column.create(name, value, logic, typeHandler, valueType));
	}
   
	public void addCriterion(String name, Object value1, Object value2, ConditionMode logic, String property, String typeHandler, String valueType) {
		 if (value1 == null || value2 == null) {
			 throw new RuntimeException("Between values for " + property + " cannot be null");
		 }
		 add(Column.create(name, value1, value2, logic, typeHandler, valueType));
	}
		 
	public DmsTableSQL andTbidIsNull() {
		isnull("tbid");
		return this;
	}
	
	public DmsTableSQL andTbidIsNotNull() {
		notNull("tbid");
		return this;
	}
	
	public DmsTableSQL andTbidIsEmpty() {
		empty("tbid");
		return this;
	}

	public DmsTableSQL andTbidIsNotEmpty() {
		notEmpty("tbid");
		return this;
	}
      public DmsTableSQL andTbidEqualTo(java.lang.Integer value) {
          addCriterion("tbid", value, ConditionMode.EQUAL, "tbid", "java.lang.Integer", "Float");
          return this;
      }

      public DmsTableSQL andTbidNotEqualTo(java.lang.Integer value) {
          addCriterion("tbid", value, ConditionMode.NOT_EQUAL, "tbid", "java.lang.Integer", "Float");
          return this;
      }

      public DmsTableSQL andTbidGreaterThan(java.lang.Integer value) {
          addCriterion("tbid", value, ConditionMode.GREATER_THEN, "tbid", "java.lang.Integer", "Float");
          return this;
      }

      public DmsTableSQL andTbidGreaterThanOrEqualTo(java.lang.Integer value) {
          addCriterion("tbid", value, ConditionMode.GREATER_EQUAL, "tbid", "java.lang.Integer", "Float");
          return this;
      }

      public DmsTableSQL andTbidLessThan(java.lang.Integer value) {
          addCriterion("tbid", value, ConditionMode.LESS_THEN, "tbid", "java.lang.Integer", "Float");
          return this;
      }

      public DmsTableSQL andTbidLessThanOrEqualTo(java.lang.Integer value) {
          addCriterion("tbid", value, ConditionMode.LESS_EQUAL, "tbid", "java.lang.Integer", "Float");
          return this;
      }

      public DmsTableSQL andTbidBetween(java.lang.Integer value1, java.lang.Integer value2) {
    	  addCriterion("tbid", value1, value2, ConditionMode.BETWEEN, "tbid", "java.lang.Integer", "Float");
    	  return this;
      }

      public DmsTableSQL andTbidNotBetween(java.lang.Integer value1, java.lang.Integer value2) {
          addCriterion("tbid", value1, value2, ConditionMode.NOT_BETWEEN, "tbid", "java.lang.Integer", "Float");
          return this;
      }
        
      public DmsTableSQL andTbidIn(List<java.lang.Integer> values) {
          addCriterion("tbid", values, ConditionMode.IN, "tbid", "java.lang.Integer", "Float");
          return this;
      }

      public DmsTableSQL andTbidNotIn(List<java.lang.Integer> values) {
          addCriterion("tbid", values, ConditionMode.NOT_IN, "tbid", "java.lang.Integer", "Float");
          return this;
      }
	public DmsTableSQL andNameIsNull() {
		isnull("name");
		return this;
	}
	
	public DmsTableSQL andNameIsNotNull() {
		notNull("name");
		return this;
	}
	
	public DmsTableSQL andNameIsEmpty() {
		empty("name");
		return this;
	}

	public DmsTableSQL andNameIsNotEmpty() {
		notEmpty("name");
		return this;
	}
       public DmsTableSQL andNameLike(java.lang.String value) {
    	   addCriterion("name", value, ConditionMode.FUZZY, "name", "java.lang.String", "Float");
    	   return this;
      }

      public DmsTableSQL andNameNotLike(java.lang.String value) {
          addCriterion("name", value, ConditionMode.NOT_FUZZY, "name", "java.lang.String", "Float");
          return this;
      }
      public DmsTableSQL andNameEqualTo(java.lang.String value) {
          addCriterion("name", value, ConditionMode.EQUAL, "name", "java.lang.String", "String");
          return this;
      }

      public DmsTableSQL andNameNotEqualTo(java.lang.String value) {
          addCriterion("name", value, ConditionMode.NOT_EQUAL, "name", "java.lang.String", "String");
          return this;
      }

      public DmsTableSQL andNameGreaterThan(java.lang.String value) {
          addCriterion("name", value, ConditionMode.GREATER_THEN, "name", "java.lang.String", "String");
          return this;
      }

      public DmsTableSQL andNameGreaterThanOrEqualTo(java.lang.String value) {
          addCriterion("name", value, ConditionMode.GREATER_EQUAL, "name", "java.lang.String", "String");
          return this;
      }

      public DmsTableSQL andNameLessThan(java.lang.String value) {
          addCriterion("name", value, ConditionMode.LESS_THEN, "name", "java.lang.String", "String");
          return this;
      }

      public DmsTableSQL andNameLessThanOrEqualTo(java.lang.String value) {
          addCriterion("name", value, ConditionMode.LESS_EQUAL, "name", "java.lang.String", "String");
          return this;
      }

      public DmsTableSQL andNameBetween(java.lang.String value1, java.lang.String value2) {
    	  addCriterion("name", value1, value2, ConditionMode.BETWEEN, "name", "java.lang.String", "String");
    	  return this;
      }

      public DmsTableSQL andNameNotBetween(java.lang.String value1, java.lang.String value2) {
          addCriterion("name", value1, value2, ConditionMode.NOT_BETWEEN, "name", "java.lang.String", "String");
          return this;
      }
        
      public DmsTableSQL andNameIn(List<java.lang.String> values) {
          addCriterion("name", values, ConditionMode.IN, "name", "java.lang.String", "String");
          return this;
      }

      public DmsTableSQL andNameNotIn(List<java.lang.String> values) {
          addCriterion("name", values, ConditionMode.NOT_IN, "name", "java.lang.String", "String");
          return this;
      }
	public DmsTableSQL andTitleIsNull() {
		isnull("title");
		return this;
	}
	
	public DmsTableSQL andTitleIsNotNull() {
		notNull("title");
		return this;
	}
	
	public DmsTableSQL andTitleIsEmpty() {
		empty("title");
		return this;
	}

	public DmsTableSQL andTitleIsNotEmpty() {
		notEmpty("title");
		return this;
	}
       public DmsTableSQL andTitleLike(java.lang.String value) {
    	   addCriterion("title", value, ConditionMode.FUZZY, "title", "java.lang.String", "String");
    	   return this;
      }

      public DmsTableSQL andTitleNotLike(java.lang.String value) {
          addCriterion("title", value, ConditionMode.NOT_FUZZY, "title", "java.lang.String", "String");
          return this;
      }
      public DmsTableSQL andTitleEqualTo(java.lang.String value) {
          addCriterion("title", value, ConditionMode.EQUAL, "title", "java.lang.String", "String");
          return this;
      }

      public DmsTableSQL andTitleNotEqualTo(java.lang.String value) {
          addCriterion("title", value, ConditionMode.NOT_EQUAL, "title", "java.lang.String", "String");
          return this;
      }

      public DmsTableSQL andTitleGreaterThan(java.lang.String value) {
          addCriterion("title", value, ConditionMode.GREATER_THEN, "title", "java.lang.String", "String");
          return this;
      }

      public DmsTableSQL andTitleGreaterThanOrEqualTo(java.lang.String value) {
          addCriterion("title", value, ConditionMode.GREATER_EQUAL, "title", "java.lang.String", "String");
          return this;
      }

      public DmsTableSQL andTitleLessThan(java.lang.String value) {
          addCriterion("title", value, ConditionMode.LESS_THEN, "title", "java.lang.String", "String");
          return this;
      }

      public DmsTableSQL andTitleLessThanOrEqualTo(java.lang.String value) {
          addCriterion("title", value, ConditionMode.LESS_EQUAL, "title", "java.lang.String", "String");
          return this;
      }

      public DmsTableSQL andTitleBetween(java.lang.String value1, java.lang.String value2) {
    	  addCriterion("title", value1, value2, ConditionMode.BETWEEN, "title", "java.lang.String", "String");
    	  return this;
      }

      public DmsTableSQL andTitleNotBetween(java.lang.String value1, java.lang.String value2) {
          addCriterion("title", value1, value2, ConditionMode.NOT_BETWEEN, "title", "java.lang.String", "String");
          return this;
      }
        
      public DmsTableSQL andTitleIn(List<java.lang.String> values) {
          addCriterion("title", values, ConditionMode.IN, "title", "java.lang.String", "String");
          return this;
      }

      public DmsTableSQL andTitleNotIn(List<java.lang.String> values) {
          addCriterion("title", values, ConditionMode.NOT_IN, "title", "java.lang.String", "String");
          return this;
      }
	public DmsTableSQL andAliasIsNull() {
		isnull("alias");
		return this;
	}
	
	public DmsTableSQL andAliasIsNotNull() {
		notNull("alias");
		return this;
	}
	
	public DmsTableSQL andAliasIsEmpty() {
		empty("alias");
		return this;
	}

	public DmsTableSQL andAliasIsNotEmpty() {
		notEmpty("alias");
		return this;
	}
       public DmsTableSQL andAliasLike(java.lang.String value) {
    	   addCriterion("alias", value, ConditionMode.FUZZY, "alias", "java.lang.String", "String");
    	   return this;
      }

      public DmsTableSQL andAliasNotLike(java.lang.String value) {
          addCriterion("alias", value, ConditionMode.NOT_FUZZY, "alias", "java.lang.String", "String");
          return this;
      }
      public DmsTableSQL andAliasEqualTo(java.lang.String value) {
          addCriterion("alias", value, ConditionMode.EQUAL, "alias", "java.lang.String", "String");
          return this;
      }

      public DmsTableSQL andAliasNotEqualTo(java.lang.String value) {
          addCriterion("alias", value, ConditionMode.NOT_EQUAL, "alias", "java.lang.String", "String");
          return this;
      }

      public DmsTableSQL andAliasGreaterThan(java.lang.String value) {
          addCriterion("alias", value, ConditionMode.GREATER_THEN, "alias", "java.lang.String", "String");
          return this;
      }

      public DmsTableSQL andAliasGreaterThanOrEqualTo(java.lang.String value) {
          addCriterion("alias", value, ConditionMode.GREATER_EQUAL, "alias", "java.lang.String", "String");
          return this;
      }

      public DmsTableSQL andAliasLessThan(java.lang.String value) {
          addCriterion("alias", value, ConditionMode.LESS_THEN, "alias", "java.lang.String", "String");
          return this;
      }

      public DmsTableSQL andAliasLessThanOrEqualTo(java.lang.String value) {
          addCriterion("alias", value, ConditionMode.LESS_EQUAL, "alias", "java.lang.String", "String");
          return this;
      }

      public DmsTableSQL andAliasBetween(java.lang.String value1, java.lang.String value2) {
    	  addCriterion("alias", value1, value2, ConditionMode.BETWEEN, "alias", "java.lang.String", "String");
    	  return this;
      }

      public DmsTableSQL andAliasNotBetween(java.lang.String value1, java.lang.String value2) {
          addCriterion("alias", value1, value2, ConditionMode.NOT_BETWEEN, "alias", "java.lang.String", "String");
          return this;
      }
        
      public DmsTableSQL andAliasIn(List<java.lang.String> values) {
          addCriterion("alias", values, ConditionMode.IN, "alias", "java.lang.String", "String");
          return this;
      }

      public DmsTableSQL andAliasNotIn(List<java.lang.String> values) {
          addCriterion("alias", values, ConditionMode.NOT_IN, "alias", "java.lang.String", "String");
          return this;
      }
	public DmsTableSQL andOrdersIsNull() {
		isnull("orders");
		return this;
	}
	
	public DmsTableSQL andOrdersIsNotNull() {
		notNull("orders");
		return this;
	}
	
	public DmsTableSQL andOrdersIsEmpty() {
		empty("orders");
		return this;
	}

	public DmsTableSQL andOrdersIsNotEmpty() {
		notEmpty("orders");
		return this;
	}
      public DmsTableSQL andOrdersEqualTo(java.lang.Integer value) {
          addCriterion("orders", value, ConditionMode.EQUAL, "orders", "java.lang.Integer", "Float");
          return this;
      }

      public DmsTableSQL andOrdersNotEqualTo(java.lang.Integer value) {
          addCriterion("orders", value, ConditionMode.NOT_EQUAL, "orders", "java.lang.Integer", "Float");
          return this;
      }

      public DmsTableSQL andOrdersGreaterThan(java.lang.Integer value) {
          addCriterion("orders", value, ConditionMode.GREATER_THEN, "orders", "java.lang.Integer", "Float");
          return this;
      }

      public DmsTableSQL andOrdersGreaterThanOrEqualTo(java.lang.Integer value) {
          addCriterion("orders", value, ConditionMode.GREATER_EQUAL, "orders", "java.lang.Integer", "Float");
          return this;
      }

      public DmsTableSQL andOrdersLessThan(java.lang.Integer value) {
          addCriterion("orders", value, ConditionMode.LESS_THEN, "orders", "java.lang.Integer", "Float");
          return this;
      }

      public DmsTableSQL andOrdersLessThanOrEqualTo(java.lang.Integer value) {
          addCriterion("orders", value, ConditionMode.LESS_EQUAL, "orders", "java.lang.Integer", "Float");
          return this;
      }

      public DmsTableSQL andOrdersBetween(java.lang.Integer value1, java.lang.Integer value2) {
    	  addCriterion("orders", value1, value2, ConditionMode.BETWEEN, "orders", "java.lang.Integer", "Float");
    	  return this;
      }

      public DmsTableSQL andOrdersNotBetween(java.lang.Integer value1, java.lang.Integer value2) {
          addCriterion("orders", value1, value2, ConditionMode.NOT_BETWEEN, "orders", "java.lang.Integer", "Float");
          return this;
      }
        
      public DmsTableSQL andOrdersIn(List<java.lang.Integer> values) {
          addCriterion("orders", values, ConditionMode.IN, "orders", "java.lang.Integer", "Float");
          return this;
      }

      public DmsTableSQL andOrdersNotIn(List<java.lang.Integer> values) {
          addCriterion("orders", values, ConditionMode.NOT_IN, "orders", "java.lang.Integer", "Float");
          return this;
      }
	public DmsTableSQL andCtimeIsNull() {
		isnull("ctime");
		return this;
	}
	
	public DmsTableSQL andCtimeIsNotNull() {
		notNull("ctime");
		return this;
	}
	
	public DmsTableSQL andCtimeIsEmpty() {
		empty("ctime");
		return this;
	}

	public DmsTableSQL andCtimeIsNotEmpty() {
		notEmpty("ctime");
		return this;
	}
      public DmsTableSQL andCtimeEqualTo(java.util.Date value) {
          addCriterion("ctime", value, ConditionMode.EQUAL, "ctime", "java.util.Date", "String");
          return this;
      }

      public DmsTableSQL andCtimeNotEqualTo(java.util.Date value) {
          addCriterion("ctime", value, ConditionMode.NOT_EQUAL, "ctime", "java.util.Date", "String");
          return this;
      }

      public DmsTableSQL andCtimeGreaterThan(java.util.Date value) {
          addCriterion("ctime", value, ConditionMode.GREATER_THEN, "ctime", "java.util.Date", "String");
          return this;
      }

      public DmsTableSQL andCtimeGreaterThanOrEqualTo(java.util.Date value) {
          addCriterion("ctime", value, ConditionMode.GREATER_EQUAL, "ctime", "java.util.Date", "String");
          return this;
      }

      public DmsTableSQL andCtimeLessThan(java.util.Date value) {
          addCriterion("ctime", value, ConditionMode.LESS_THEN, "ctime", "java.util.Date", "String");
          return this;
      }

      public DmsTableSQL andCtimeLessThanOrEqualTo(java.util.Date value) {
          addCriterion("ctime", value, ConditionMode.LESS_EQUAL, "ctime", "java.util.Date", "String");
          return this;
      }

      public DmsTableSQL andCtimeBetween(java.util.Date value1, java.util.Date value2) {
    	  addCriterion("ctime", value1, value2, ConditionMode.BETWEEN, "ctime", "java.util.Date", "String");
    	  return this;
      }

      public DmsTableSQL andCtimeNotBetween(java.util.Date value1, java.util.Date value2) {
          addCriterion("ctime", value1, value2, ConditionMode.NOT_BETWEEN, "ctime", "java.util.Date", "String");
          return this;
      }
        
      public DmsTableSQL andCtimeIn(List<java.util.Date> values) {
          addCriterion("ctime", values, ConditionMode.IN, "ctime", "java.util.Date", "String");
          return this;
      }

      public DmsTableSQL andCtimeNotIn(List<java.util.Date> values) {
          addCriterion("ctime", values, ConditionMode.NOT_IN, "ctime", "java.util.Date", "String");
          return this;
      }
	public DmsTableSQL andCatalogIsNull() {
		isnull("catalog");
		return this;
	}
	
	public DmsTableSQL andCatalogIsNotNull() {
		notNull("catalog");
		return this;
	}
	
	public DmsTableSQL andCatalogIsEmpty() {
		empty("catalog");
		return this;
	}

	public DmsTableSQL andCatalogIsNotEmpty() {
		notEmpty("catalog");
		return this;
	}
       public DmsTableSQL andCatalogLike(java.lang.String value) {
    	   addCriterion("catalog", value, ConditionMode.FUZZY, "catalog", "java.lang.String", "String");
    	   return this;
      }

      public DmsTableSQL andCatalogNotLike(java.lang.String value) {
          addCriterion("catalog", value, ConditionMode.NOT_FUZZY, "catalog", "java.lang.String", "String");
          return this;
      }
      public DmsTableSQL andCatalogEqualTo(java.lang.String value) {
          addCriterion("catalog", value, ConditionMode.EQUAL, "catalog", "java.lang.String", "String");
          return this;
      }

      public DmsTableSQL andCatalogNotEqualTo(java.lang.String value) {
          addCriterion("catalog", value, ConditionMode.NOT_EQUAL, "catalog", "java.lang.String", "String");
          return this;
      }

      public DmsTableSQL andCatalogGreaterThan(java.lang.String value) {
          addCriterion("catalog", value, ConditionMode.GREATER_THEN, "catalog", "java.lang.String", "String");
          return this;
      }

      public DmsTableSQL andCatalogGreaterThanOrEqualTo(java.lang.String value) {
          addCriterion("catalog", value, ConditionMode.GREATER_EQUAL, "catalog", "java.lang.String", "String");
          return this;
      }

      public DmsTableSQL andCatalogLessThan(java.lang.String value) {
          addCriterion("catalog", value, ConditionMode.LESS_THEN, "catalog", "java.lang.String", "String");
          return this;
      }

      public DmsTableSQL andCatalogLessThanOrEqualTo(java.lang.String value) {
          addCriterion("catalog", value, ConditionMode.LESS_EQUAL, "catalog", "java.lang.String", "String");
          return this;
      }

      public DmsTableSQL andCatalogBetween(java.lang.String value1, java.lang.String value2) {
    	  addCriterion("catalog", value1, value2, ConditionMode.BETWEEN, "catalog", "java.lang.String", "String");
    	  return this;
      }

      public DmsTableSQL andCatalogNotBetween(java.lang.String value1, java.lang.String value2) {
          addCriterion("catalog", value1, value2, ConditionMode.NOT_BETWEEN, "catalog", "java.lang.String", "String");
          return this;
      }
        
      public DmsTableSQL andCatalogIn(List<java.lang.String> values) {
          addCriterion("catalog", values, ConditionMode.IN, "catalog", "java.lang.String", "String");
          return this;
      }

      public DmsTableSQL andCatalogNotIn(List<java.lang.String> values) {
          addCriterion("catalog", values, ConditionMode.NOT_IN, "catalog", "java.lang.String", "String");
          return this;
      }
	public DmsTableSQL andTagIsNull() {
		isnull("tag");
		return this;
	}
	
	public DmsTableSQL andTagIsNotNull() {
		notNull("tag");
		return this;
	}
	
	public DmsTableSQL andTagIsEmpty() {
		empty("tag");
		return this;
	}

	public DmsTableSQL andTagIsNotEmpty() {
		notEmpty("tag");
		return this;
	}
       public DmsTableSQL andTagLike(java.lang.String value) {
    	   addCriterion("tag", value, ConditionMode.FUZZY, "tag", "java.lang.String", "String");
    	   return this;
      }

      public DmsTableSQL andTagNotLike(java.lang.String value) {
          addCriterion("tag", value, ConditionMode.NOT_FUZZY, "tag", "java.lang.String", "String");
          return this;
      }
      public DmsTableSQL andTagEqualTo(java.lang.String value) {
          addCriterion("tag", value, ConditionMode.EQUAL, "tag", "java.lang.String", "String");
          return this;
      }

      public DmsTableSQL andTagNotEqualTo(java.lang.String value) {
          addCriterion("tag", value, ConditionMode.NOT_EQUAL, "tag", "java.lang.String", "String");
          return this;
      }

      public DmsTableSQL andTagGreaterThan(java.lang.String value) {
          addCriterion("tag", value, ConditionMode.GREATER_THEN, "tag", "java.lang.String", "String");
          return this;
      }

      public DmsTableSQL andTagGreaterThanOrEqualTo(java.lang.String value) {
          addCriterion("tag", value, ConditionMode.GREATER_EQUAL, "tag", "java.lang.String", "String");
          return this;
      }

      public DmsTableSQL andTagLessThan(java.lang.String value) {
          addCriterion("tag", value, ConditionMode.LESS_THEN, "tag", "java.lang.String", "String");
          return this;
      }

      public DmsTableSQL andTagLessThanOrEqualTo(java.lang.String value) {
          addCriterion("tag", value, ConditionMode.LESS_EQUAL, "tag", "java.lang.String", "String");
          return this;
      }

      public DmsTableSQL andTagBetween(java.lang.String value1, java.lang.String value2) {
    	  addCriterion("tag", value1, value2, ConditionMode.BETWEEN, "tag", "java.lang.String", "String");
    	  return this;
      }

      public DmsTableSQL andTagNotBetween(java.lang.String value1, java.lang.String value2) {
          addCriterion("tag", value1, value2, ConditionMode.NOT_BETWEEN, "tag", "java.lang.String", "String");
          return this;
      }
        
      public DmsTableSQL andTagIn(List<java.lang.String> values) {
          addCriterion("tag", values, ConditionMode.IN, "tag", "java.lang.String", "String");
          return this;
      }

      public DmsTableSQL andTagNotIn(List<java.lang.String> values) {
          addCriterion("tag", values, ConditionMode.NOT_IN, "tag", "java.lang.String", "String");
          return this;
      }
	public DmsTableSQL andStatusIsNull() {
		isnull("status");
		return this;
	}
	
	public DmsTableSQL andStatusIsNotNull() {
		notNull("status");
		return this;
	}
	
	public DmsTableSQL andStatusIsEmpty() {
		empty("status");
		return this;
	}

	public DmsTableSQL andStatusIsNotEmpty() {
		notEmpty("status");
		return this;
	}
       public DmsTableSQL andStatusLike(java.lang.String value) {
    	   addCriterion("status", value, ConditionMode.FUZZY, "status", "java.lang.String", "String");
    	   return this;
      }

      public DmsTableSQL andStatusNotLike(java.lang.String value) {
          addCriterion("status", value, ConditionMode.NOT_FUZZY, "status", "java.lang.String", "String");
          return this;
      }
      public DmsTableSQL andStatusEqualTo(java.lang.String value) {
          addCriterion("status", value, ConditionMode.EQUAL, "status", "java.lang.String", "String");
          return this;
      }

      public DmsTableSQL andStatusNotEqualTo(java.lang.String value) {
          addCriterion("status", value, ConditionMode.NOT_EQUAL, "status", "java.lang.String", "String");
          return this;
      }

      public DmsTableSQL andStatusGreaterThan(java.lang.String value) {
          addCriterion("status", value, ConditionMode.GREATER_THEN, "status", "java.lang.String", "String");
          return this;
      }

      public DmsTableSQL andStatusGreaterThanOrEqualTo(java.lang.String value) {
          addCriterion("status", value, ConditionMode.GREATER_EQUAL, "status", "java.lang.String", "String");
          return this;
      }

      public DmsTableSQL andStatusLessThan(java.lang.String value) {
          addCriterion("status", value, ConditionMode.LESS_THEN, "status", "java.lang.String", "String");
          return this;
      }

      public DmsTableSQL andStatusLessThanOrEqualTo(java.lang.String value) {
          addCriterion("status", value, ConditionMode.LESS_EQUAL, "status", "java.lang.String", "String");
          return this;
      }

      public DmsTableSQL andStatusBetween(java.lang.String value1, java.lang.String value2) {
    	  addCriterion("status", value1, value2, ConditionMode.BETWEEN, "status", "java.lang.String", "String");
    	  return this;
      }

      public DmsTableSQL andStatusNotBetween(java.lang.String value1, java.lang.String value2) {
          addCriterion("status", value1, value2, ConditionMode.NOT_BETWEEN, "status", "java.lang.String", "String");
          return this;
      }
        
      public DmsTableSQL andStatusIn(List<java.lang.String> values) {
          addCriterion("status", values, ConditionMode.IN, "status", "java.lang.String", "String");
          return this;
      }

      public DmsTableSQL andStatusNotIn(List<java.lang.String> values) {
          addCriterion("status", values, ConditionMode.NOT_IN, "status", "java.lang.String", "String");
          return this;
      }
	public DmsTableSQL andGuidIsNull() {
		isnull("guid");
		return this;
	}
	
	public DmsTableSQL andGuidIsNotNull() {
		notNull("guid");
		return this;
	}
	
	public DmsTableSQL andGuidIsEmpty() {
		empty("guid");
		return this;
	}

	public DmsTableSQL andGuidIsNotEmpty() {
		notEmpty("guid");
		return this;
	}
       public DmsTableSQL andGuidLike(java.lang.String value) {
    	   addCriterion("guid", value, ConditionMode.FUZZY, "guid", "java.lang.String", "String");
    	   return this;
      }

      public DmsTableSQL andGuidNotLike(java.lang.String value) {
          addCriterion("guid", value, ConditionMode.NOT_FUZZY, "guid", "java.lang.String", "String");
          return this;
      }
      public DmsTableSQL andGuidEqualTo(java.lang.String value) {
          addCriterion("guid", value, ConditionMode.EQUAL, "guid", "java.lang.String", "String");
          return this;
      }

      public DmsTableSQL andGuidNotEqualTo(java.lang.String value) {
          addCriterion("guid", value, ConditionMode.NOT_EQUAL, "guid", "java.lang.String", "String");
          return this;
      }

      public DmsTableSQL andGuidGreaterThan(java.lang.String value) {
          addCriterion("guid", value, ConditionMode.GREATER_THEN, "guid", "java.lang.String", "String");
          return this;
      }

      public DmsTableSQL andGuidGreaterThanOrEqualTo(java.lang.String value) {
          addCriterion("guid", value, ConditionMode.GREATER_EQUAL, "guid", "java.lang.String", "String");
          return this;
      }

      public DmsTableSQL andGuidLessThan(java.lang.String value) {
          addCriterion("guid", value, ConditionMode.LESS_THEN, "guid", "java.lang.String", "String");
          return this;
      }

      public DmsTableSQL andGuidLessThanOrEqualTo(java.lang.String value) {
          addCriterion("guid", value, ConditionMode.LESS_EQUAL, "guid", "java.lang.String", "String");
          return this;
      }

      public DmsTableSQL andGuidBetween(java.lang.String value1, java.lang.String value2) {
    	  addCriterion("guid", value1, value2, ConditionMode.BETWEEN, "guid", "java.lang.String", "String");
    	  return this;
      }

      public DmsTableSQL andGuidNotBetween(java.lang.String value1, java.lang.String value2) {
          addCriterion("guid", value1, value2, ConditionMode.NOT_BETWEEN, "guid", "java.lang.String", "String");
          return this;
      }
        
      public DmsTableSQL andGuidIn(List<java.lang.String> values) {
          addCriterion("guid", values, ConditionMode.IN, "guid", "java.lang.String", "String");
          return this;
      }

      public DmsTableSQL andGuidNotIn(List<java.lang.String> values) {
          addCriterion("guid", values, ConditionMode.NOT_IN, "guid", "java.lang.String", "String");
          return this;
      }
}