/**
 * Copyright (c) 2015-2017, Henry Yang 杨勇 (gismail@foxmail.com).
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.lambkit.module.dms.row;

import com.lambkit.db.RowModel;

/**
 * @author yangyong 
 * @website: www.lambkit.com
 * @email: gismail@foxmail.com
 * @date ${date}
 * @version 1.0
 * @since 1.0
 */

public class DmsTableTree extends RowModel<DmsTableTree> {
	public DmsTableTree() {
		setTableName("dms_table_tree");
		setPrimaryKey("ttid");
	}
	public java.lang.Long getTtid() {
		return this.get("ttid");
	}
	public void setTtid(java.lang.Long ttid) {
		this.set("ttid", ttid);
	}
	public java.lang.String getModelIdName() {
		return this.get("model_id_name");
	}
	public void setModelIdName(java.lang.String modelIdName) {
		this.set("model_id_name", modelIdName);
	}
	public java.lang.String getParentIdName() {
		return this.get("parent_id_name");
	}
	public void setParentIdName(java.lang.String parentIdName) {
		this.set("parent_id_name", parentIdName);
	}
	public java.lang.String getPathName() {
		return this.get("path_name");
	}
	public void setPathName(java.lang.String pathName) {
		this.set("path_name", pathName);
	}
	public java.lang.String getLevelName() {
		return this.get("level_name");
	}
	public void setLevelName(java.lang.String levelName) {
		this.set("level_name", levelName);
	}
	public java.lang.String getLeafName() {
		return this.get("leaf_name");
	}
	public void setLeafName(java.lang.String leafName) {
		this.set("leaf_name", leafName);
	}
	public java.lang.String getExtTable() {
		return this.get("ext_table");
	}
	public void setExtTable(java.lang.String extTable) {
		this.set("ext_table", extTable);
	}
	public java.lang.String getExtModelIdName() {
		return this.get("ext_model_id_name");
	}
	public void setExtModelIdName(java.lang.String extModelIdName) {
		this.set("ext_model_id_name", extModelIdName);
	}
	public java.lang.String getExtParentIdName() {
		return this.get("ext_parent_id_name");
	}
	public void setExtParentIdName(java.lang.String extParentIdName) {
		this.set("ext_parent_id_name", extParentIdName);
	}
	public java.lang.String getExtPathName() {
		return this.get("ext_path_name");
	}
	public void setExtPathName(java.lang.String extPathName) {
		this.set("ext_path_name", extPathName);
	}
	public java.lang.String getExtLevelName() {
		return this.get("ext_level_name");
	}
	public void setExtLevelName(java.lang.String extLevelName) {
		this.set("ext_level_name", extLevelName);
	}
	public java.lang.String getExtLeafName() {
		return this.get("ext_leaf_name");
	}
	public void setExtLeafName(java.lang.String extLeafName) {
		this.set("ext_leaf_name", extLeafName);
	}
	public java.lang.Integer getStatus() {
		return this.get("status");
	}
	public void setStatus(java.lang.Integer status) {
		this.set("status", status);
	}
	public java.lang.String getTopName() {
		return this.get("top_name");
	}
	public void setTopName(java.lang.String topName) {
		this.set("top_name", topName);
	}
	public java.lang.String getExtTopName() {
		return this.get("ext_top_name");
	}
	public void setExtTopName(java.lang.String extTopName) {
		this.set("ext_top_name", extTopName);
	}
	public java.lang.String getTopValue() {
		return this.get("top_value");
	}
	public void setTopValue(java.lang.String topValue) {
		this.set("top_value", topValue);
	}
	public java.lang.String getLeafValue() {
		return this.get("leaf_value");
	}
	public void setLeafValue(java.lang.String leafValue) {
		this.set("leaf_value", leafValue);
	}
}
