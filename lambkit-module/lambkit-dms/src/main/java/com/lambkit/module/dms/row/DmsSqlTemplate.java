/**
 * Copyright (c) 2015-2017, Henry Yang 杨勇 (gismail@foxmail.com).
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.lambkit.module.dms.row;

import com.lambkit.db.RowModel;

/**
 * @author yangyong 
 * @website: www.lambkit.com
 * @email: gismail@foxmail.com
 * @date ${date}
 * @version 1.0
 * @since 1.0
 */

public class DmsSqlTemplate extends RowModel<DmsSqlTemplate> {
	public DmsSqlTemplate() {
		setTableName("dms_sql_template");
		setPrimaryKey("name");
	}
	public java.lang.String getName() {
		return this.get("name");
	}
	public void setName(java.lang.String name) {
		this.set("name", name);
	}
	public java.lang.String getTitle() {
		return this.get("title");
	}
	public void setTitle(java.lang.String title) {
		this.set("title", title);
	}
	public java.lang.String getContent() {
		return this.get("content");
	}
	public void setContent(java.lang.String content) {
		this.set("content", content);
	}
	public java.lang.Integer getUserid() {
		return this.get("userid");
	}
	public void setUserid(java.lang.Integer userid) {
		this.set("userid", userid);
	}
	public java.lang.Integer getAppid() {
		return this.get("appid");
	}
	public void setAppid(java.lang.Integer appid) {
		this.set("appid", appid);
	}
	public java.lang.Integer getStatus() {
		return this.get("status");
	}
	public void setStatus(java.lang.Integer status) {
		this.set("status", status);
	}
	public java.lang.String getRuletype() {
		return this.get("ruletype");
	}
	public void setRuletype(java.lang.String ruletype) {
		this.set("ruletype", ruletype);
	}
	public java.lang.String getSqltype() {
		return this.get("sqltype");
	}
	public void setSqltype(java.lang.String sqltype) {
		this.set("sqltype", sqltype);
	}
	public java.lang.String getCatalog() {
		return this.get("catalog");
	}
	public void setCatalog(java.lang.String catalog) {
		this.set("catalog", catalog);
	}
	public java.lang.String getDbtype() {
		return this.get("dbtype");
	}
	public void setDbtype(java.lang.String dbtype) {
		this.set("dbtype", dbtype);
	}
	public java.lang.String getDbname() {
		return this.get("dbname");
	}
	public void setDbname(java.lang.String dbname) {
		this.set("dbname", dbname);
	}
}
