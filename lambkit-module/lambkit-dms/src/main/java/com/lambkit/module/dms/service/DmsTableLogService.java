/**
 * Copyright (c) 2015-2017, Henry Yang 杨勇 (gismail@foxmail.com).
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.lambkit.module.dms.service;

import com.lambkit.core.Lambkit;
import com.lambkit.db.DbPool;
import com.lambkit.db.IDaoService;
import com.lambkit.db.IRowDao;
import com.lambkit.module.dms.DmsConfig;
import com.lambkit.module.dms.row.DmsTableLog;

/**
 * @author yangyong 
 * @website: www.lambkit.com
 * @email: gismail@foxmail.com
 * @date ${date}
 * @version 1.0
 * @since 1.0
 */

public class DmsTableLogService implements IDaoService<DmsTableLog> {
	public IRowDao<DmsTableLog> dao() {
		String dbPoolName = Lambkit.config(DmsConfig.class).getDbPoolName();
		return DbPool.use(dbPoolName).dao(DmsTableLog.class);
	}

	public IRowDao<DmsTableLog> dao(String dbPoolName) {
		return DbPool.use(dbPoolName).dao(DmsTableLog.class);
	}
}
