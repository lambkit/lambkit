/**
 * Copyright (c) 2015-2017, Henry Yang 杨勇 (gismail@foxmail.com).
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.lambkit.module.dms.sql;

import com.lambkit.db.sql.ConditionMode;
import com.lambkit.db.sql.Column;
import com.lambkit.db.sql.Columns;
import com.lambkit.db.sql.Example;

import java.io.Serializable;
import java.util.List;

/**
 * @author yangyong 
 * @website: www.lambkit.com
 * @email: gismail@foxmail.com
 * @date ${date}
 * @version 1.0
 * @since 1.0
 */

public class DmsFormSelectSQL extends Columns implements Serializable {
	private static final long serialVersionUID = 1L;
	
	public static DmsFormSelectSQL of() {
		return new DmsFormSelectSQL();
	}
	
	public static DmsFormSelectSQL by(Column column) {
		DmsFormSelectSQL that = new DmsFormSelectSQL();
		that.add(column);
        return that;
    }

    public static DmsFormSelectSQL by(String name, Object value) {
        return (DmsFormSelectSQL) create().eq(name, value);
    }
    
    public Example example() {
    	return Example.create("dms_form_select", this);
    }
    
    /**
     * equals
     *
     * @param name
     * @param value
     * @return
     */
    public DmsFormSelectSQL eq(String name, Object value) {
    	super.eq(name, value);
        return this;
    }

    /**
     * not equals !=
     *
     * @param name
     * @param value
     * @return
     */
    public DmsFormSelectSQL ne(String name, Object value) {
    	super.ne(name, value);
        return this;
    }


    /**
     * like
     *
     * @param name
     * @param value
     * @return
     */

    public DmsFormSelectSQL like(String name, Object value) {
    	super.like(name, value);
        return this;
    }
    
    public DmsFormSelectSQL notLike(String name, Object value) {
    	super.notLike(name, value);
        return this;
    }

    /**
     * 大于 great than
     *
     * @param name
     * @param value
     * @return
     */
    public DmsFormSelectSQL gt(String name, Object value) {
    	super.gt(name, value);
        return this;
    }

    /**
     * 大于等于 great or equal
     *
     * @param name
     * @param value
     * @return
     */
    public DmsFormSelectSQL ge(String name, Object value) {
    	super.ge(name, value);
        return this;
    }

    /**
     * 小于 less than
     *
     * @param name
     * @param value
     * @return
     */
    public DmsFormSelectSQL lt(String name, Object value) {
    	super.lt(name, value);
        return this;
    }

    /**
     * 小于等于 less or equal
     *
     * @param name
     * @param value
     * @return
     */
    public DmsFormSelectSQL le(String name, Object value) {
    	super.le(name, value);
        return this;
    }
    
    public DmsFormSelectSQL isnull(String name) {
    	super.isnull(name);
        return this;
    } 

    public DmsFormSelectSQL notNull(String name) {
    	super.notNull(name);
        return this;
    } 
    
    public DmsFormSelectSQL empty(String name) {
    	super.empty(name);
        return this;
    } 
    
    public DmsFormSelectSQL notEmpty(String name) {
    	super.notEmpty(name);
        return this;
    } 
    
    public DmsFormSelectSQL add(Column column) {
    	super.add(column);
    	return this;
    }
    
    /**************************/
	
	public void addCriterion(String name, Object value, ConditionMode logic, String property, String typeHandler, String valueType) {
		 if (value == null) {
			 throw new RuntimeException("Value for " + property + " cannot be null");
		 }
		 add(Column.create(name, value, logic, typeHandler, valueType));
	}
   
	public void addCriterion(String name, Object value1, Object value2, ConditionMode logic, String property, String typeHandler, String valueType) {
		 if (value1 == null || value2 == null) {
			 throw new RuntimeException("Between values for " + property + " cannot be null");
		 }
		 add(Column.create(name, value1, value2, logic, typeHandler, valueType));
	}
		 
	public DmsFormSelectSQL andFormSelectIdIsNull() {
		isnull("form_select_id");
		return this;
	}
	
	public DmsFormSelectSQL andFormSelectIdIsNotNull() {
		notNull("form_select_id");
		return this;
	}
	
	public DmsFormSelectSQL andFormSelectIdIsEmpty() {
		empty("form_select_id");
		return this;
	}

	public DmsFormSelectSQL andFormSelectIdIsNotEmpty() {
		notEmpty("form_select_id");
		return this;
	}
      public DmsFormSelectSQL andFormSelectIdEqualTo(java.lang.Long value) {
          addCriterion("form_select_id", value, ConditionMode.EQUAL, "formSelectId", "java.lang.Long", "Float");
          return this;
      }

      public DmsFormSelectSQL andFormSelectIdNotEqualTo(java.lang.Long value) {
          addCriterion("form_select_id", value, ConditionMode.NOT_EQUAL, "formSelectId", "java.lang.Long", "Float");
          return this;
      }

      public DmsFormSelectSQL andFormSelectIdGreaterThan(java.lang.Long value) {
          addCriterion("form_select_id", value, ConditionMode.GREATER_THEN, "formSelectId", "java.lang.Long", "Float");
          return this;
      }

      public DmsFormSelectSQL andFormSelectIdGreaterThanOrEqualTo(java.lang.Long value) {
          addCriterion("form_select_id", value, ConditionMode.GREATER_EQUAL, "formSelectId", "java.lang.Long", "Float");
          return this;
      }

      public DmsFormSelectSQL andFormSelectIdLessThan(java.lang.Long value) {
          addCriterion("form_select_id", value, ConditionMode.LESS_THEN, "formSelectId", "java.lang.Long", "Float");
          return this;
      }

      public DmsFormSelectSQL andFormSelectIdLessThanOrEqualTo(java.lang.Long value) {
          addCriterion("form_select_id", value, ConditionMode.LESS_EQUAL, "formSelectId", "java.lang.Long", "Float");
          return this;
      }

      public DmsFormSelectSQL andFormSelectIdBetween(java.lang.Long value1, java.lang.Long value2) {
    	  addCriterion("form_select_id", value1, value2, ConditionMode.BETWEEN, "formSelectId", "java.lang.Long", "Float");
    	  return this;
      }

      public DmsFormSelectSQL andFormSelectIdNotBetween(java.lang.Long value1, java.lang.Long value2) {
          addCriterion("form_select_id", value1, value2, ConditionMode.NOT_BETWEEN, "formSelectId", "java.lang.Long", "Float");
          return this;
      }
        
      public DmsFormSelectSQL andFormSelectIdIn(List<java.lang.Long> values) {
          addCriterion("form_select_id", values, ConditionMode.IN, "formSelectId", "java.lang.Long", "Float");
          return this;
      }

      public DmsFormSelectSQL andFormSelectIdNotIn(List<java.lang.Long> values) {
          addCriterion("form_select_id", values, ConditionMode.NOT_IN, "formSelectId", "java.lang.Long", "Float");
          return this;
      }
	public DmsFormSelectSQL andSelectTypeIsNull() {
		isnull("select_type");
		return this;
	}
	
	public DmsFormSelectSQL andSelectTypeIsNotNull() {
		notNull("select_type");
		return this;
	}
	
	public DmsFormSelectSQL andSelectTypeIsEmpty() {
		empty("select_type");
		return this;
	}

	public DmsFormSelectSQL andSelectTypeIsNotEmpty() {
		notEmpty("select_type");
		return this;
	}
       public DmsFormSelectSQL andSelectTypeLike(java.lang.String value) {
    	   addCriterion("select_type", value, ConditionMode.FUZZY, "selectType", "java.lang.String", "Float");
    	   return this;
      }

      public DmsFormSelectSQL andSelectTypeNotLike(java.lang.String value) {
          addCriterion("select_type", value, ConditionMode.NOT_FUZZY, "selectType", "java.lang.String", "Float");
          return this;
      }
      public DmsFormSelectSQL andSelectTypeEqualTo(java.lang.String value) {
          addCriterion("select_type", value, ConditionMode.EQUAL, "selectType", "java.lang.String", "String");
          return this;
      }

      public DmsFormSelectSQL andSelectTypeNotEqualTo(java.lang.String value) {
          addCriterion("select_type", value, ConditionMode.NOT_EQUAL, "selectType", "java.lang.String", "String");
          return this;
      }

      public DmsFormSelectSQL andSelectTypeGreaterThan(java.lang.String value) {
          addCriterion("select_type", value, ConditionMode.GREATER_THEN, "selectType", "java.lang.String", "String");
          return this;
      }

      public DmsFormSelectSQL andSelectTypeGreaterThanOrEqualTo(java.lang.String value) {
          addCriterion("select_type", value, ConditionMode.GREATER_EQUAL, "selectType", "java.lang.String", "String");
          return this;
      }

      public DmsFormSelectSQL andSelectTypeLessThan(java.lang.String value) {
          addCriterion("select_type", value, ConditionMode.LESS_THEN, "selectType", "java.lang.String", "String");
          return this;
      }

      public DmsFormSelectSQL andSelectTypeLessThanOrEqualTo(java.lang.String value) {
          addCriterion("select_type", value, ConditionMode.LESS_EQUAL, "selectType", "java.lang.String", "String");
          return this;
      }

      public DmsFormSelectSQL andSelectTypeBetween(java.lang.String value1, java.lang.String value2) {
    	  addCriterion("select_type", value1, value2, ConditionMode.BETWEEN, "selectType", "java.lang.String", "String");
    	  return this;
      }

      public DmsFormSelectSQL andSelectTypeNotBetween(java.lang.String value1, java.lang.String value2) {
          addCriterion("select_type", value1, value2, ConditionMode.NOT_BETWEEN, "selectType", "java.lang.String", "String");
          return this;
      }
        
      public DmsFormSelectSQL andSelectTypeIn(List<java.lang.String> values) {
          addCriterion("select_type", values, ConditionMode.IN, "selectType", "java.lang.String", "String");
          return this;
      }

      public DmsFormSelectSQL andSelectTypeNotIn(List<java.lang.String> values) {
          addCriterion("select_type", values, ConditionMode.NOT_IN, "selectType", "java.lang.String", "String");
          return this;
      }
	public DmsFormSelectSQL andLabelIsNull() {
		isnull("label");
		return this;
	}
	
	public DmsFormSelectSQL andLabelIsNotNull() {
		notNull("label");
		return this;
	}
	
	public DmsFormSelectSQL andLabelIsEmpty() {
		empty("label");
		return this;
	}

	public DmsFormSelectSQL andLabelIsNotEmpty() {
		notEmpty("label");
		return this;
	}
       public DmsFormSelectSQL andLabelLike(java.lang.String value) {
    	   addCriterion("label", value, ConditionMode.FUZZY, "label", "java.lang.String", "String");
    	   return this;
      }

      public DmsFormSelectSQL andLabelNotLike(java.lang.String value) {
          addCriterion("label", value, ConditionMode.NOT_FUZZY, "label", "java.lang.String", "String");
          return this;
      }
      public DmsFormSelectSQL andLabelEqualTo(java.lang.String value) {
          addCriterion("label", value, ConditionMode.EQUAL, "label", "java.lang.String", "String");
          return this;
      }

      public DmsFormSelectSQL andLabelNotEqualTo(java.lang.String value) {
          addCriterion("label", value, ConditionMode.NOT_EQUAL, "label", "java.lang.String", "String");
          return this;
      }

      public DmsFormSelectSQL andLabelGreaterThan(java.lang.String value) {
          addCriterion("label", value, ConditionMode.GREATER_THEN, "label", "java.lang.String", "String");
          return this;
      }

      public DmsFormSelectSQL andLabelGreaterThanOrEqualTo(java.lang.String value) {
          addCriterion("label", value, ConditionMode.GREATER_EQUAL, "label", "java.lang.String", "String");
          return this;
      }

      public DmsFormSelectSQL andLabelLessThan(java.lang.String value) {
          addCriterion("label", value, ConditionMode.LESS_THEN, "label", "java.lang.String", "String");
          return this;
      }

      public DmsFormSelectSQL andLabelLessThanOrEqualTo(java.lang.String value) {
          addCriterion("label", value, ConditionMode.LESS_EQUAL, "label", "java.lang.String", "String");
          return this;
      }

      public DmsFormSelectSQL andLabelBetween(java.lang.String value1, java.lang.String value2) {
    	  addCriterion("label", value1, value2, ConditionMode.BETWEEN, "label", "java.lang.String", "String");
    	  return this;
      }

      public DmsFormSelectSQL andLabelNotBetween(java.lang.String value1, java.lang.String value2) {
          addCriterion("label", value1, value2, ConditionMode.NOT_BETWEEN, "label", "java.lang.String", "String");
          return this;
      }
        
      public DmsFormSelectSQL andLabelIn(List<java.lang.String> values) {
          addCriterion("label", values, ConditionMode.IN, "label", "java.lang.String", "String");
          return this;
      }

      public DmsFormSelectSQL andLabelNotIn(List<java.lang.String> values) {
          addCriterion("label", values, ConditionMode.NOT_IN, "label", "java.lang.String", "String");
          return this;
      }
	public DmsFormSelectSQL andRequiredIsNull() {
		isnull("required");
		return this;
	}
	
	public DmsFormSelectSQL andRequiredIsNotNull() {
		notNull("required");
		return this;
	}
	
	public DmsFormSelectSQL andRequiredIsEmpty() {
		empty("required");
		return this;
	}

	public DmsFormSelectSQL andRequiredIsNotEmpty() {
		notEmpty("required");
		return this;
	}
      public DmsFormSelectSQL andRequiredEqualTo(java.lang.Integer value) {
          addCriterion("required", value, ConditionMode.EQUAL, "required", "java.lang.Integer", "Float");
          return this;
      }

      public DmsFormSelectSQL andRequiredNotEqualTo(java.lang.Integer value) {
          addCriterion("required", value, ConditionMode.NOT_EQUAL, "required", "java.lang.Integer", "Float");
          return this;
      }

      public DmsFormSelectSQL andRequiredGreaterThan(java.lang.Integer value) {
          addCriterion("required", value, ConditionMode.GREATER_THEN, "required", "java.lang.Integer", "Float");
          return this;
      }

      public DmsFormSelectSQL andRequiredGreaterThanOrEqualTo(java.lang.Integer value) {
          addCriterion("required", value, ConditionMode.GREATER_EQUAL, "required", "java.lang.Integer", "Float");
          return this;
      }

      public DmsFormSelectSQL andRequiredLessThan(java.lang.Integer value) {
          addCriterion("required", value, ConditionMode.LESS_THEN, "required", "java.lang.Integer", "Float");
          return this;
      }

      public DmsFormSelectSQL andRequiredLessThanOrEqualTo(java.lang.Integer value) {
          addCriterion("required", value, ConditionMode.LESS_EQUAL, "required", "java.lang.Integer", "Float");
          return this;
      }

      public DmsFormSelectSQL andRequiredBetween(java.lang.Integer value1, java.lang.Integer value2) {
    	  addCriterion("required", value1, value2, ConditionMode.BETWEEN, "required", "java.lang.Integer", "Float");
    	  return this;
      }

      public DmsFormSelectSQL andRequiredNotBetween(java.lang.Integer value1, java.lang.Integer value2) {
          addCriterion("required", value1, value2, ConditionMode.NOT_BETWEEN, "required", "java.lang.Integer", "Float");
          return this;
      }
        
      public DmsFormSelectSQL andRequiredIn(List<java.lang.Integer> values) {
          addCriterion("required", values, ConditionMode.IN, "required", "java.lang.Integer", "Float");
          return this;
      }

      public DmsFormSelectSQL andRequiredNotIn(List<java.lang.Integer> values) {
          addCriterion("required", values, ConditionMode.NOT_IN, "required", "java.lang.Integer", "Float");
          return this;
      }
	public DmsFormSelectSQL andHintIsNull() {
		isnull("hint");
		return this;
	}
	
	public DmsFormSelectSQL andHintIsNotNull() {
		notNull("hint");
		return this;
	}
	
	public DmsFormSelectSQL andHintIsEmpty() {
		empty("hint");
		return this;
	}

	public DmsFormSelectSQL andHintIsNotEmpty() {
		notEmpty("hint");
		return this;
	}
       public DmsFormSelectSQL andHintLike(java.lang.String value) {
    	   addCriterion("hint", value, ConditionMode.FUZZY, "hint", "java.lang.String", "Float");
    	   return this;
      }

      public DmsFormSelectSQL andHintNotLike(java.lang.String value) {
          addCriterion("hint", value, ConditionMode.NOT_FUZZY, "hint", "java.lang.String", "Float");
          return this;
      }
      public DmsFormSelectSQL andHintEqualTo(java.lang.String value) {
          addCriterion("hint", value, ConditionMode.EQUAL, "hint", "java.lang.String", "String");
          return this;
      }

      public DmsFormSelectSQL andHintNotEqualTo(java.lang.String value) {
          addCriterion("hint", value, ConditionMode.NOT_EQUAL, "hint", "java.lang.String", "String");
          return this;
      }

      public DmsFormSelectSQL andHintGreaterThan(java.lang.String value) {
          addCriterion("hint", value, ConditionMode.GREATER_THEN, "hint", "java.lang.String", "String");
          return this;
      }

      public DmsFormSelectSQL andHintGreaterThanOrEqualTo(java.lang.String value) {
          addCriterion("hint", value, ConditionMode.GREATER_EQUAL, "hint", "java.lang.String", "String");
          return this;
      }

      public DmsFormSelectSQL andHintLessThan(java.lang.String value) {
          addCriterion("hint", value, ConditionMode.LESS_THEN, "hint", "java.lang.String", "String");
          return this;
      }

      public DmsFormSelectSQL andHintLessThanOrEqualTo(java.lang.String value) {
          addCriterion("hint", value, ConditionMode.LESS_EQUAL, "hint", "java.lang.String", "String");
          return this;
      }

      public DmsFormSelectSQL andHintBetween(java.lang.String value1, java.lang.String value2) {
    	  addCriterion("hint", value1, value2, ConditionMode.BETWEEN, "hint", "java.lang.String", "String");
    	  return this;
      }

      public DmsFormSelectSQL andHintNotBetween(java.lang.String value1, java.lang.String value2) {
          addCriterion("hint", value1, value2, ConditionMode.NOT_BETWEEN, "hint", "java.lang.String", "String");
          return this;
      }
        
      public DmsFormSelectSQL andHintIn(List<java.lang.String> values) {
          addCriterion("hint", values, ConditionMode.IN, "hint", "java.lang.String", "String");
          return this;
      }

      public DmsFormSelectSQL andHintNotIn(List<java.lang.String> values) {
          addCriterion("hint", values, ConditionMode.NOT_IN, "hint", "java.lang.String", "String");
          return this;
      }
	public DmsFormSelectSQL andPlaceholderIsNull() {
		isnull("placeholder");
		return this;
	}
	
	public DmsFormSelectSQL andPlaceholderIsNotNull() {
		notNull("placeholder");
		return this;
	}
	
	public DmsFormSelectSQL andPlaceholderIsEmpty() {
		empty("placeholder");
		return this;
	}

	public DmsFormSelectSQL andPlaceholderIsNotEmpty() {
		notEmpty("placeholder");
		return this;
	}
       public DmsFormSelectSQL andPlaceholderLike(java.lang.String value) {
    	   addCriterion("placeholder", value, ConditionMode.FUZZY, "placeholder", "java.lang.String", "String");
    	   return this;
      }

      public DmsFormSelectSQL andPlaceholderNotLike(java.lang.String value) {
          addCriterion("placeholder", value, ConditionMode.NOT_FUZZY, "placeholder", "java.lang.String", "String");
          return this;
      }
      public DmsFormSelectSQL andPlaceholderEqualTo(java.lang.String value) {
          addCriterion("placeholder", value, ConditionMode.EQUAL, "placeholder", "java.lang.String", "String");
          return this;
      }

      public DmsFormSelectSQL andPlaceholderNotEqualTo(java.lang.String value) {
          addCriterion("placeholder", value, ConditionMode.NOT_EQUAL, "placeholder", "java.lang.String", "String");
          return this;
      }

      public DmsFormSelectSQL andPlaceholderGreaterThan(java.lang.String value) {
          addCriterion("placeholder", value, ConditionMode.GREATER_THEN, "placeholder", "java.lang.String", "String");
          return this;
      }

      public DmsFormSelectSQL andPlaceholderGreaterThanOrEqualTo(java.lang.String value) {
          addCriterion("placeholder", value, ConditionMode.GREATER_EQUAL, "placeholder", "java.lang.String", "String");
          return this;
      }

      public DmsFormSelectSQL andPlaceholderLessThan(java.lang.String value) {
          addCriterion("placeholder", value, ConditionMode.LESS_THEN, "placeholder", "java.lang.String", "String");
          return this;
      }

      public DmsFormSelectSQL andPlaceholderLessThanOrEqualTo(java.lang.String value) {
          addCriterion("placeholder", value, ConditionMode.LESS_EQUAL, "placeholder", "java.lang.String", "String");
          return this;
      }

      public DmsFormSelectSQL andPlaceholderBetween(java.lang.String value1, java.lang.String value2) {
    	  addCriterion("placeholder", value1, value2, ConditionMode.BETWEEN, "placeholder", "java.lang.String", "String");
    	  return this;
      }

      public DmsFormSelectSQL andPlaceholderNotBetween(java.lang.String value1, java.lang.String value2) {
          addCriterion("placeholder", value1, value2, ConditionMode.NOT_BETWEEN, "placeholder", "java.lang.String", "String");
          return this;
      }
        
      public DmsFormSelectSQL andPlaceholderIn(List<java.lang.String> values) {
          addCriterion("placeholder", values, ConditionMode.IN, "placeholder", "java.lang.String", "String");
          return this;
      }

      public DmsFormSelectSQL andPlaceholderNotIn(List<java.lang.String> values) {
          addCriterion("placeholder", values, ConditionMode.NOT_IN, "placeholder", "java.lang.String", "String");
          return this;
      }
	public DmsFormSelectSQL andDisabledIsNull() {
		isnull("disabled");
		return this;
	}
	
	public DmsFormSelectSQL andDisabledIsNotNull() {
		notNull("disabled");
		return this;
	}
	
	public DmsFormSelectSQL andDisabledIsEmpty() {
		empty("disabled");
		return this;
	}

	public DmsFormSelectSQL andDisabledIsNotEmpty() {
		notEmpty("disabled");
		return this;
	}
      public DmsFormSelectSQL andDisabledEqualTo(java.lang.Integer value) {
          addCriterion("disabled", value, ConditionMode.EQUAL, "disabled", "java.lang.Integer", "Float");
          return this;
      }

      public DmsFormSelectSQL andDisabledNotEqualTo(java.lang.Integer value) {
          addCriterion("disabled", value, ConditionMode.NOT_EQUAL, "disabled", "java.lang.Integer", "Float");
          return this;
      }

      public DmsFormSelectSQL andDisabledGreaterThan(java.lang.Integer value) {
          addCriterion("disabled", value, ConditionMode.GREATER_THEN, "disabled", "java.lang.Integer", "Float");
          return this;
      }

      public DmsFormSelectSQL andDisabledGreaterThanOrEqualTo(java.lang.Integer value) {
          addCriterion("disabled", value, ConditionMode.GREATER_EQUAL, "disabled", "java.lang.Integer", "Float");
          return this;
      }

      public DmsFormSelectSQL andDisabledLessThan(java.lang.Integer value) {
          addCriterion("disabled", value, ConditionMode.LESS_THEN, "disabled", "java.lang.Integer", "Float");
          return this;
      }

      public DmsFormSelectSQL andDisabledLessThanOrEqualTo(java.lang.Integer value) {
          addCriterion("disabled", value, ConditionMode.LESS_EQUAL, "disabled", "java.lang.Integer", "Float");
          return this;
      }

      public DmsFormSelectSQL andDisabledBetween(java.lang.Integer value1, java.lang.Integer value2) {
    	  addCriterion("disabled", value1, value2, ConditionMode.BETWEEN, "disabled", "java.lang.Integer", "Float");
    	  return this;
      }

      public DmsFormSelectSQL andDisabledNotBetween(java.lang.Integer value1, java.lang.Integer value2) {
          addCriterion("disabled", value1, value2, ConditionMode.NOT_BETWEEN, "disabled", "java.lang.Integer", "Float");
          return this;
      }
        
      public DmsFormSelectSQL andDisabledIn(List<java.lang.Integer> values) {
          addCriterion("disabled", values, ConditionMode.IN, "disabled", "java.lang.Integer", "Float");
          return this;
      }

      public DmsFormSelectSQL andDisabledNotIn(List<java.lang.Integer> values) {
          addCriterion("disabled", values, ConditionMode.NOT_IN, "disabled", "java.lang.Integer", "Float");
          return this;
      }
	public DmsFormSelectSQL andReadonlyIsNull() {
		isnull("readonly");
		return this;
	}
	
	public DmsFormSelectSQL andReadonlyIsNotNull() {
		notNull("readonly");
		return this;
	}
	
	public DmsFormSelectSQL andReadonlyIsEmpty() {
		empty("readonly");
		return this;
	}

	public DmsFormSelectSQL andReadonlyIsNotEmpty() {
		notEmpty("readonly");
		return this;
	}
      public DmsFormSelectSQL andReadonlyEqualTo(java.lang.Integer value) {
          addCriterion("readonly", value, ConditionMode.EQUAL, "readonly", "java.lang.Integer", "Float");
          return this;
      }

      public DmsFormSelectSQL andReadonlyNotEqualTo(java.lang.Integer value) {
          addCriterion("readonly", value, ConditionMode.NOT_EQUAL, "readonly", "java.lang.Integer", "Float");
          return this;
      }

      public DmsFormSelectSQL andReadonlyGreaterThan(java.lang.Integer value) {
          addCriterion("readonly", value, ConditionMode.GREATER_THEN, "readonly", "java.lang.Integer", "Float");
          return this;
      }

      public DmsFormSelectSQL andReadonlyGreaterThanOrEqualTo(java.lang.Integer value) {
          addCriterion("readonly", value, ConditionMode.GREATER_EQUAL, "readonly", "java.lang.Integer", "Float");
          return this;
      }

      public DmsFormSelectSQL andReadonlyLessThan(java.lang.Integer value) {
          addCriterion("readonly", value, ConditionMode.LESS_THEN, "readonly", "java.lang.Integer", "Float");
          return this;
      }

      public DmsFormSelectSQL andReadonlyLessThanOrEqualTo(java.lang.Integer value) {
          addCriterion("readonly", value, ConditionMode.LESS_EQUAL, "readonly", "java.lang.Integer", "Float");
          return this;
      }

      public DmsFormSelectSQL andReadonlyBetween(java.lang.Integer value1, java.lang.Integer value2) {
    	  addCriterion("readonly", value1, value2, ConditionMode.BETWEEN, "readonly", "java.lang.Integer", "Float");
    	  return this;
      }

      public DmsFormSelectSQL andReadonlyNotBetween(java.lang.Integer value1, java.lang.Integer value2) {
          addCriterion("readonly", value1, value2, ConditionMode.NOT_BETWEEN, "readonly", "java.lang.Integer", "Float");
          return this;
      }
        
      public DmsFormSelectSQL andReadonlyIn(List<java.lang.Integer> values) {
          addCriterion("readonly", values, ConditionMode.IN, "readonly", "java.lang.Integer", "Float");
          return this;
      }

      public DmsFormSelectSQL andReadonlyNotIn(List<java.lang.Integer> values) {
          addCriterion("readonly", values, ConditionMode.NOT_IN, "readonly", "java.lang.Integer", "Float");
          return this;
      }
	public DmsFormSelectSQL andFldidIsNull() {
		isnull("fldid");
		return this;
	}
	
	public DmsFormSelectSQL andFldidIsNotNull() {
		notNull("fldid");
		return this;
	}
	
	public DmsFormSelectSQL andFldidIsEmpty() {
		empty("fldid");
		return this;
	}

	public DmsFormSelectSQL andFldidIsNotEmpty() {
		notEmpty("fldid");
		return this;
	}
      public DmsFormSelectSQL andFldidEqualTo(java.lang.Integer value) {
          addCriterion("fldid", value, ConditionMode.EQUAL, "fldid", "java.lang.Integer", "Float");
          return this;
      }

      public DmsFormSelectSQL andFldidNotEqualTo(java.lang.Integer value) {
          addCriterion("fldid", value, ConditionMode.NOT_EQUAL, "fldid", "java.lang.Integer", "Float");
          return this;
      }

      public DmsFormSelectSQL andFldidGreaterThan(java.lang.Integer value) {
          addCriterion("fldid", value, ConditionMode.GREATER_THEN, "fldid", "java.lang.Integer", "Float");
          return this;
      }

      public DmsFormSelectSQL andFldidGreaterThanOrEqualTo(java.lang.Integer value) {
          addCriterion("fldid", value, ConditionMode.GREATER_EQUAL, "fldid", "java.lang.Integer", "Float");
          return this;
      }

      public DmsFormSelectSQL andFldidLessThan(java.lang.Integer value) {
          addCriterion("fldid", value, ConditionMode.LESS_THEN, "fldid", "java.lang.Integer", "Float");
          return this;
      }

      public DmsFormSelectSQL andFldidLessThanOrEqualTo(java.lang.Integer value) {
          addCriterion("fldid", value, ConditionMode.LESS_EQUAL, "fldid", "java.lang.Integer", "Float");
          return this;
      }

      public DmsFormSelectSQL andFldidBetween(java.lang.Integer value1, java.lang.Integer value2) {
    	  addCriterion("fldid", value1, value2, ConditionMode.BETWEEN, "fldid", "java.lang.Integer", "Float");
    	  return this;
      }

      public DmsFormSelectSQL andFldidNotBetween(java.lang.Integer value1, java.lang.Integer value2) {
          addCriterion("fldid", value1, value2, ConditionMode.NOT_BETWEEN, "fldid", "java.lang.Integer", "Float");
          return this;
      }
        
      public DmsFormSelectSQL andFldidIn(List<java.lang.Integer> values) {
          addCriterion("fldid", values, ConditionMode.IN, "fldid", "java.lang.Integer", "Float");
          return this;
      }

      public DmsFormSelectSQL andFldidNotIn(List<java.lang.Integer> values) {
          addCriterion("fldid", values, ConditionMode.NOT_IN, "fldid", "java.lang.Integer", "Float");
          return this;
      }
	public DmsFormSelectSQL andFldnameIsNull() {
		isnull("fldname");
		return this;
	}
	
	public DmsFormSelectSQL andFldnameIsNotNull() {
		notNull("fldname");
		return this;
	}
	
	public DmsFormSelectSQL andFldnameIsEmpty() {
		empty("fldname");
		return this;
	}

	public DmsFormSelectSQL andFldnameIsNotEmpty() {
		notEmpty("fldname");
		return this;
	}
       public DmsFormSelectSQL andFldnameLike(java.lang.String value) {
    	   addCriterion("fldname", value, ConditionMode.FUZZY, "fldname", "java.lang.String", "Float");
    	   return this;
      }

      public DmsFormSelectSQL andFldnameNotLike(java.lang.String value) {
          addCriterion("fldname", value, ConditionMode.NOT_FUZZY, "fldname", "java.lang.String", "Float");
          return this;
      }
      public DmsFormSelectSQL andFldnameEqualTo(java.lang.String value) {
          addCriterion("fldname", value, ConditionMode.EQUAL, "fldname", "java.lang.String", "String");
          return this;
      }

      public DmsFormSelectSQL andFldnameNotEqualTo(java.lang.String value) {
          addCriterion("fldname", value, ConditionMode.NOT_EQUAL, "fldname", "java.lang.String", "String");
          return this;
      }

      public DmsFormSelectSQL andFldnameGreaterThan(java.lang.String value) {
          addCriterion("fldname", value, ConditionMode.GREATER_THEN, "fldname", "java.lang.String", "String");
          return this;
      }

      public DmsFormSelectSQL andFldnameGreaterThanOrEqualTo(java.lang.String value) {
          addCriterion("fldname", value, ConditionMode.GREATER_EQUAL, "fldname", "java.lang.String", "String");
          return this;
      }

      public DmsFormSelectSQL andFldnameLessThan(java.lang.String value) {
          addCriterion("fldname", value, ConditionMode.LESS_THEN, "fldname", "java.lang.String", "String");
          return this;
      }

      public DmsFormSelectSQL andFldnameLessThanOrEqualTo(java.lang.String value) {
          addCriterion("fldname", value, ConditionMode.LESS_EQUAL, "fldname", "java.lang.String", "String");
          return this;
      }

      public DmsFormSelectSQL andFldnameBetween(java.lang.String value1, java.lang.String value2) {
    	  addCriterion("fldname", value1, value2, ConditionMode.BETWEEN, "fldname", "java.lang.String", "String");
    	  return this;
      }

      public DmsFormSelectSQL andFldnameNotBetween(java.lang.String value1, java.lang.String value2) {
          addCriterion("fldname", value1, value2, ConditionMode.NOT_BETWEEN, "fldname", "java.lang.String", "String");
          return this;
      }
        
      public DmsFormSelectSQL andFldnameIn(List<java.lang.String> values) {
          addCriterion("fldname", values, ConditionMode.IN, "fldname", "java.lang.String", "String");
          return this;
      }

      public DmsFormSelectSQL andFldnameNotIn(List<java.lang.String> values) {
          addCriterion("fldname", values, ConditionMode.NOT_IN, "fldname", "java.lang.String", "String");
          return this;
      }
	public DmsFormSelectSQL andStatusIsNull() {
		isnull("status");
		return this;
	}
	
	public DmsFormSelectSQL andStatusIsNotNull() {
		notNull("status");
		return this;
	}
	
	public DmsFormSelectSQL andStatusIsEmpty() {
		empty("status");
		return this;
	}

	public DmsFormSelectSQL andStatusIsNotEmpty() {
		notEmpty("status");
		return this;
	}
      public DmsFormSelectSQL andStatusEqualTo(java.lang.Integer value) {
          addCriterion("status", value, ConditionMode.EQUAL, "status", "java.lang.Integer", "Float");
          return this;
      }

      public DmsFormSelectSQL andStatusNotEqualTo(java.lang.Integer value) {
          addCriterion("status", value, ConditionMode.NOT_EQUAL, "status", "java.lang.Integer", "Float");
          return this;
      }

      public DmsFormSelectSQL andStatusGreaterThan(java.lang.Integer value) {
          addCriterion("status", value, ConditionMode.GREATER_THEN, "status", "java.lang.Integer", "Float");
          return this;
      }

      public DmsFormSelectSQL andStatusGreaterThanOrEqualTo(java.lang.Integer value) {
          addCriterion("status", value, ConditionMode.GREATER_EQUAL, "status", "java.lang.Integer", "Float");
          return this;
      }

      public DmsFormSelectSQL andStatusLessThan(java.lang.Integer value) {
          addCriterion("status", value, ConditionMode.LESS_THEN, "status", "java.lang.Integer", "Float");
          return this;
      }

      public DmsFormSelectSQL andStatusLessThanOrEqualTo(java.lang.Integer value) {
          addCriterion("status", value, ConditionMode.LESS_EQUAL, "status", "java.lang.Integer", "Float");
          return this;
      }

      public DmsFormSelectSQL andStatusBetween(java.lang.Integer value1, java.lang.Integer value2) {
    	  addCriterion("status", value1, value2, ConditionMode.BETWEEN, "status", "java.lang.Integer", "Float");
    	  return this;
      }

      public DmsFormSelectSQL andStatusNotBetween(java.lang.Integer value1, java.lang.Integer value2) {
          addCriterion("status", value1, value2, ConditionMode.NOT_BETWEEN, "status", "java.lang.Integer", "Float");
          return this;
      }
        
      public DmsFormSelectSQL andStatusIn(List<java.lang.Integer> values) {
          addCriterion("status", values, ConditionMode.IN, "status", "java.lang.Integer", "Float");
          return this;
      }

      public DmsFormSelectSQL andStatusNotIn(List<java.lang.Integer> values) {
          addCriterion("status", values, ConditionMode.NOT_IN, "status", "java.lang.Integer", "Float");
          return this;
      }
	public DmsFormSelectSQL andDefalutValueIsNull() {
		isnull("defalut_value");
		return this;
	}
	
	public DmsFormSelectSQL andDefalutValueIsNotNull() {
		notNull("defalut_value");
		return this;
	}
	
	public DmsFormSelectSQL andDefalutValueIsEmpty() {
		empty("defalut_value");
		return this;
	}

	public DmsFormSelectSQL andDefalutValueIsNotEmpty() {
		notEmpty("defalut_value");
		return this;
	}
       public DmsFormSelectSQL andDefalutValueLike(java.lang.String value) {
    	   addCriterion("defalut_value", value, ConditionMode.FUZZY, "defalutValue", "java.lang.String", "Float");
    	   return this;
      }

      public DmsFormSelectSQL andDefalutValueNotLike(java.lang.String value) {
          addCriterion("defalut_value", value, ConditionMode.NOT_FUZZY, "defalutValue", "java.lang.String", "Float");
          return this;
      }
      public DmsFormSelectSQL andDefalutValueEqualTo(java.lang.String value) {
          addCriterion("defalut_value", value, ConditionMode.EQUAL, "defalutValue", "java.lang.String", "String");
          return this;
      }

      public DmsFormSelectSQL andDefalutValueNotEqualTo(java.lang.String value) {
          addCriterion("defalut_value", value, ConditionMode.NOT_EQUAL, "defalutValue", "java.lang.String", "String");
          return this;
      }

      public DmsFormSelectSQL andDefalutValueGreaterThan(java.lang.String value) {
          addCriterion("defalut_value", value, ConditionMode.GREATER_THEN, "defalutValue", "java.lang.String", "String");
          return this;
      }

      public DmsFormSelectSQL andDefalutValueGreaterThanOrEqualTo(java.lang.String value) {
          addCriterion("defalut_value", value, ConditionMode.GREATER_EQUAL, "defalutValue", "java.lang.String", "String");
          return this;
      }

      public DmsFormSelectSQL andDefalutValueLessThan(java.lang.String value) {
          addCriterion("defalut_value", value, ConditionMode.LESS_THEN, "defalutValue", "java.lang.String", "String");
          return this;
      }

      public DmsFormSelectSQL andDefalutValueLessThanOrEqualTo(java.lang.String value) {
          addCriterion("defalut_value", value, ConditionMode.LESS_EQUAL, "defalutValue", "java.lang.String", "String");
          return this;
      }

      public DmsFormSelectSQL andDefalutValueBetween(java.lang.String value1, java.lang.String value2) {
    	  addCriterion("defalut_value", value1, value2, ConditionMode.BETWEEN, "defalutValue", "java.lang.String", "String");
    	  return this;
      }

      public DmsFormSelectSQL andDefalutValueNotBetween(java.lang.String value1, java.lang.String value2) {
          addCriterion("defalut_value", value1, value2, ConditionMode.NOT_BETWEEN, "defalutValue", "java.lang.String", "String");
          return this;
      }
        
      public DmsFormSelectSQL andDefalutValueIn(List<java.lang.String> values) {
          addCriterion("defalut_value", values, ConditionMode.IN, "defalutValue", "java.lang.String", "String");
          return this;
      }

      public DmsFormSelectSQL andDefalutValueNotIn(List<java.lang.String> values) {
          addCriterion("defalut_value", values, ConditionMode.NOT_IN, "defalutValue", "java.lang.String", "String");
          return this;
      }
	public DmsFormSelectSQL andDictValueKeyIsNull() {
		isnull("dict_value_key");
		return this;
	}
	
	public DmsFormSelectSQL andDictValueKeyIsNotNull() {
		notNull("dict_value_key");
		return this;
	}
	
	public DmsFormSelectSQL andDictValueKeyIsEmpty() {
		empty("dict_value_key");
		return this;
	}

	public DmsFormSelectSQL andDictValueKeyIsNotEmpty() {
		notEmpty("dict_value_key");
		return this;
	}
       public DmsFormSelectSQL andDictValueKeyLike(java.lang.String value) {
    	   addCriterion("dict_value_key", value, ConditionMode.FUZZY, "dictValueKey", "java.lang.String", "String");
    	   return this;
      }

      public DmsFormSelectSQL andDictValueKeyNotLike(java.lang.String value) {
          addCriterion("dict_value_key", value, ConditionMode.NOT_FUZZY, "dictValueKey", "java.lang.String", "String");
          return this;
      }
      public DmsFormSelectSQL andDictValueKeyEqualTo(java.lang.String value) {
          addCriterion("dict_value_key", value, ConditionMode.EQUAL, "dictValueKey", "java.lang.String", "String");
          return this;
      }

      public DmsFormSelectSQL andDictValueKeyNotEqualTo(java.lang.String value) {
          addCriterion("dict_value_key", value, ConditionMode.NOT_EQUAL, "dictValueKey", "java.lang.String", "String");
          return this;
      }

      public DmsFormSelectSQL andDictValueKeyGreaterThan(java.lang.String value) {
          addCriterion("dict_value_key", value, ConditionMode.GREATER_THEN, "dictValueKey", "java.lang.String", "String");
          return this;
      }

      public DmsFormSelectSQL andDictValueKeyGreaterThanOrEqualTo(java.lang.String value) {
          addCriterion("dict_value_key", value, ConditionMode.GREATER_EQUAL, "dictValueKey", "java.lang.String", "String");
          return this;
      }

      public DmsFormSelectSQL andDictValueKeyLessThan(java.lang.String value) {
          addCriterion("dict_value_key", value, ConditionMode.LESS_THEN, "dictValueKey", "java.lang.String", "String");
          return this;
      }

      public DmsFormSelectSQL andDictValueKeyLessThanOrEqualTo(java.lang.String value) {
          addCriterion("dict_value_key", value, ConditionMode.LESS_EQUAL, "dictValueKey", "java.lang.String", "String");
          return this;
      }

      public DmsFormSelectSQL andDictValueKeyBetween(java.lang.String value1, java.lang.String value2) {
    	  addCriterion("dict_value_key", value1, value2, ConditionMode.BETWEEN, "dictValueKey", "java.lang.String", "String");
    	  return this;
      }

      public DmsFormSelectSQL andDictValueKeyNotBetween(java.lang.String value1, java.lang.String value2) {
          addCriterion("dict_value_key", value1, value2, ConditionMode.NOT_BETWEEN, "dictValueKey", "java.lang.String", "String");
          return this;
      }
        
      public DmsFormSelectSQL andDictValueKeyIn(List<java.lang.String> values) {
          addCriterion("dict_value_key", values, ConditionMode.IN, "dictValueKey", "java.lang.String", "String");
          return this;
      }

      public DmsFormSelectSQL andDictValueKeyNotIn(List<java.lang.String> values) {
          addCriterion("dict_value_key", values, ConditionMode.NOT_IN, "dictValueKey", "java.lang.String", "String");
          return this;
      }
	public DmsFormSelectSQL andSwitchOnValueIsNull() {
		isnull("switch_on_value");
		return this;
	}
	
	public DmsFormSelectSQL andSwitchOnValueIsNotNull() {
		notNull("switch_on_value");
		return this;
	}
	
	public DmsFormSelectSQL andSwitchOnValueIsEmpty() {
		empty("switch_on_value");
		return this;
	}

	public DmsFormSelectSQL andSwitchOnValueIsNotEmpty() {
		notEmpty("switch_on_value");
		return this;
	}
       public DmsFormSelectSQL andSwitchOnValueLike(java.lang.String value) {
    	   addCriterion("switch_on_value", value, ConditionMode.FUZZY, "switchOnValue", "java.lang.String", "String");
    	   return this;
      }

      public DmsFormSelectSQL andSwitchOnValueNotLike(java.lang.String value) {
          addCriterion("switch_on_value", value, ConditionMode.NOT_FUZZY, "switchOnValue", "java.lang.String", "String");
          return this;
      }
      public DmsFormSelectSQL andSwitchOnValueEqualTo(java.lang.String value) {
          addCriterion("switch_on_value", value, ConditionMode.EQUAL, "switchOnValue", "java.lang.String", "String");
          return this;
      }

      public DmsFormSelectSQL andSwitchOnValueNotEqualTo(java.lang.String value) {
          addCriterion("switch_on_value", value, ConditionMode.NOT_EQUAL, "switchOnValue", "java.lang.String", "String");
          return this;
      }

      public DmsFormSelectSQL andSwitchOnValueGreaterThan(java.lang.String value) {
          addCriterion("switch_on_value", value, ConditionMode.GREATER_THEN, "switchOnValue", "java.lang.String", "String");
          return this;
      }

      public DmsFormSelectSQL andSwitchOnValueGreaterThanOrEqualTo(java.lang.String value) {
          addCriterion("switch_on_value", value, ConditionMode.GREATER_EQUAL, "switchOnValue", "java.lang.String", "String");
          return this;
      }

      public DmsFormSelectSQL andSwitchOnValueLessThan(java.lang.String value) {
          addCriterion("switch_on_value", value, ConditionMode.LESS_THEN, "switchOnValue", "java.lang.String", "String");
          return this;
      }

      public DmsFormSelectSQL andSwitchOnValueLessThanOrEqualTo(java.lang.String value) {
          addCriterion("switch_on_value", value, ConditionMode.LESS_EQUAL, "switchOnValue", "java.lang.String", "String");
          return this;
      }

      public DmsFormSelectSQL andSwitchOnValueBetween(java.lang.String value1, java.lang.String value2) {
    	  addCriterion("switch_on_value", value1, value2, ConditionMode.BETWEEN, "switchOnValue", "java.lang.String", "String");
    	  return this;
      }

      public DmsFormSelectSQL andSwitchOnValueNotBetween(java.lang.String value1, java.lang.String value2) {
          addCriterion("switch_on_value", value1, value2, ConditionMode.NOT_BETWEEN, "switchOnValue", "java.lang.String", "String");
          return this;
      }
        
      public DmsFormSelectSQL andSwitchOnValueIn(List<java.lang.String> values) {
          addCriterion("switch_on_value", values, ConditionMode.IN, "switchOnValue", "java.lang.String", "String");
          return this;
      }

      public DmsFormSelectSQL andSwitchOnValueNotIn(List<java.lang.String> values) {
          addCriterion("switch_on_value", values, ConditionMode.NOT_IN, "switchOnValue", "java.lang.String", "String");
          return this;
      }
	public DmsFormSelectSQL andSwitchOffValueIsNull() {
		isnull("switch_off_value");
		return this;
	}
	
	public DmsFormSelectSQL andSwitchOffValueIsNotNull() {
		notNull("switch_off_value");
		return this;
	}
	
	public DmsFormSelectSQL andSwitchOffValueIsEmpty() {
		empty("switch_off_value");
		return this;
	}

	public DmsFormSelectSQL andSwitchOffValueIsNotEmpty() {
		notEmpty("switch_off_value");
		return this;
	}
       public DmsFormSelectSQL andSwitchOffValueLike(java.lang.String value) {
    	   addCriterion("switch_off_value", value, ConditionMode.FUZZY, "switchOffValue", "java.lang.String", "String");
    	   return this;
      }

      public DmsFormSelectSQL andSwitchOffValueNotLike(java.lang.String value) {
          addCriterion("switch_off_value", value, ConditionMode.NOT_FUZZY, "switchOffValue", "java.lang.String", "String");
          return this;
      }
      public DmsFormSelectSQL andSwitchOffValueEqualTo(java.lang.String value) {
          addCriterion("switch_off_value", value, ConditionMode.EQUAL, "switchOffValue", "java.lang.String", "String");
          return this;
      }

      public DmsFormSelectSQL andSwitchOffValueNotEqualTo(java.lang.String value) {
          addCriterion("switch_off_value", value, ConditionMode.NOT_EQUAL, "switchOffValue", "java.lang.String", "String");
          return this;
      }

      public DmsFormSelectSQL andSwitchOffValueGreaterThan(java.lang.String value) {
          addCriterion("switch_off_value", value, ConditionMode.GREATER_THEN, "switchOffValue", "java.lang.String", "String");
          return this;
      }

      public DmsFormSelectSQL andSwitchOffValueGreaterThanOrEqualTo(java.lang.String value) {
          addCriterion("switch_off_value", value, ConditionMode.GREATER_EQUAL, "switchOffValue", "java.lang.String", "String");
          return this;
      }

      public DmsFormSelectSQL andSwitchOffValueLessThan(java.lang.String value) {
          addCriterion("switch_off_value", value, ConditionMode.LESS_THEN, "switchOffValue", "java.lang.String", "String");
          return this;
      }

      public DmsFormSelectSQL andSwitchOffValueLessThanOrEqualTo(java.lang.String value) {
          addCriterion("switch_off_value", value, ConditionMode.LESS_EQUAL, "switchOffValue", "java.lang.String", "String");
          return this;
      }

      public DmsFormSelectSQL andSwitchOffValueBetween(java.lang.String value1, java.lang.String value2) {
    	  addCriterion("switch_off_value", value1, value2, ConditionMode.BETWEEN, "switchOffValue", "java.lang.String", "String");
    	  return this;
      }

      public DmsFormSelectSQL andSwitchOffValueNotBetween(java.lang.String value1, java.lang.String value2) {
          addCriterion("switch_off_value", value1, value2, ConditionMode.NOT_BETWEEN, "switchOffValue", "java.lang.String", "String");
          return this;
      }
        
      public DmsFormSelectSQL andSwitchOffValueIn(List<java.lang.String> values) {
          addCriterion("switch_off_value", values, ConditionMode.IN, "switchOffValue", "java.lang.String", "String");
          return this;
      }

      public DmsFormSelectSQL andSwitchOffValueNotIn(List<java.lang.String> values) {
          addCriterion("switch_off_value", values, ConditionMode.NOT_IN, "switchOffValue", "java.lang.String", "String");
          return this;
      }
	public DmsFormSelectSQL andMultiMinNumIsNull() {
		isnull("multi_min_num");
		return this;
	}
	
	public DmsFormSelectSQL andMultiMinNumIsNotNull() {
		notNull("multi_min_num");
		return this;
	}
	
	public DmsFormSelectSQL andMultiMinNumIsEmpty() {
		empty("multi_min_num");
		return this;
	}

	public DmsFormSelectSQL andMultiMinNumIsNotEmpty() {
		notEmpty("multi_min_num");
		return this;
	}
      public DmsFormSelectSQL andMultiMinNumEqualTo(java.lang.Integer value) {
          addCriterion("multi_min_num", value, ConditionMode.EQUAL, "multiMinNum", "java.lang.Integer", "Float");
          return this;
      }

      public DmsFormSelectSQL andMultiMinNumNotEqualTo(java.lang.Integer value) {
          addCriterion("multi_min_num", value, ConditionMode.NOT_EQUAL, "multiMinNum", "java.lang.Integer", "Float");
          return this;
      }

      public DmsFormSelectSQL andMultiMinNumGreaterThan(java.lang.Integer value) {
          addCriterion("multi_min_num", value, ConditionMode.GREATER_THEN, "multiMinNum", "java.lang.Integer", "Float");
          return this;
      }

      public DmsFormSelectSQL andMultiMinNumGreaterThanOrEqualTo(java.lang.Integer value) {
          addCriterion("multi_min_num", value, ConditionMode.GREATER_EQUAL, "multiMinNum", "java.lang.Integer", "Float");
          return this;
      }

      public DmsFormSelectSQL andMultiMinNumLessThan(java.lang.Integer value) {
          addCriterion("multi_min_num", value, ConditionMode.LESS_THEN, "multiMinNum", "java.lang.Integer", "Float");
          return this;
      }

      public DmsFormSelectSQL andMultiMinNumLessThanOrEqualTo(java.lang.Integer value) {
          addCriterion("multi_min_num", value, ConditionMode.LESS_EQUAL, "multiMinNum", "java.lang.Integer", "Float");
          return this;
      }

      public DmsFormSelectSQL andMultiMinNumBetween(java.lang.Integer value1, java.lang.Integer value2) {
    	  addCriterion("multi_min_num", value1, value2, ConditionMode.BETWEEN, "multiMinNum", "java.lang.Integer", "Float");
    	  return this;
      }

      public DmsFormSelectSQL andMultiMinNumNotBetween(java.lang.Integer value1, java.lang.Integer value2) {
          addCriterion("multi_min_num", value1, value2, ConditionMode.NOT_BETWEEN, "multiMinNum", "java.lang.Integer", "Float");
          return this;
      }
        
      public DmsFormSelectSQL andMultiMinNumIn(List<java.lang.Integer> values) {
          addCriterion("multi_min_num", values, ConditionMode.IN, "multiMinNum", "java.lang.Integer", "Float");
          return this;
      }

      public DmsFormSelectSQL andMultiMinNumNotIn(List<java.lang.Integer> values) {
          addCriterion("multi_min_num", values, ConditionMode.NOT_IN, "multiMinNum", "java.lang.Integer", "Float");
          return this;
      }
	public DmsFormSelectSQL andMultiMaxNumIsNull() {
		isnull("multi_max_num");
		return this;
	}
	
	public DmsFormSelectSQL andMultiMaxNumIsNotNull() {
		notNull("multi_max_num");
		return this;
	}
	
	public DmsFormSelectSQL andMultiMaxNumIsEmpty() {
		empty("multi_max_num");
		return this;
	}

	public DmsFormSelectSQL andMultiMaxNumIsNotEmpty() {
		notEmpty("multi_max_num");
		return this;
	}
      public DmsFormSelectSQL andMultiMaxNumEqualTo(java.lang.Integer value) {
          addCriterion("multi_max_num", value, ConditionMode.EQUAL, "multiMaxNum", "java.lang.Integer", "Float");
          return this;
      }

      public DmsFormSelectSQL andMultiMaxNumNotEqualTo(java.lang.Integer value) {
          addCriterion("multi_max_num", value, ConditionMode.NOT_EQUAL, "multiMaxNum", "java.lang.Integer", "Float");
          return this;
      }

      public DmsFormSelectSQL andMultiMaxNumGreaterThan(java.lang.Integer value) {
          addCriterion("multi_max_num", value, ConditionMode.GREATER_THEN, "multiMaxNum", "java.lang.Integer", "Float");
          return this;
      }

      public DmsFormSelectSQL andMultiMaxNumGreaterThanOrEqualTo(java.lang.Integer value) {
          addCriterion("multi_max_num", value, ConditionMode.GREATER_EQUAL, "multiMaxNum", "java.lang.Integer", "Float");
          return this;
      }

      public DmsFormSelectSQL andMultiMaxNumLessThan(java.lang.Integer value) {
          addCriterion("multi_max_num", value, ConditionMode.LESS_THEN, "multiMaxNum", "java.lang.Integer", "Float");
          return this;
      }

      public DmsFormSelectSQL andMultiMaxNumLessThanOrEqualTo(java.lang.Integer value) {
          addCriterion("multi_max_num", value, ConditionMode.LESS_EQUAL, "multiMaxNum", "java.lang.Integer", "Float");
          return this;
      }

      public DmsFormSelectSQL andMultiMaxNumBetween(java.lang.Integer value1, java.lang.Integer value2) {
    	  addCriterion("multi_max_num", value1, value2, ConditionMode.BETWEEN, "multiMaxNum", "java.lang.Integer", "Float");
    	  return this;
      }

      public DmsFormSelectSQL andMultiMaxNumNotBetween(java.lang.Integer value1, java.lang.Integer value2) {
          addCriterion("multi_max_num", value1, value2, ConditionMode.NOT_BETWEEN, "multiMaxNum", "java.lang.Integer", "Float");
          return this;
      }
        
      public DmsFormSelectSQL andMultiMaxNumIn(List<java.lang.Integer> values) {
          addCriterion("multi_max_num", values, ConditionMode.IN, "multiMaxNum", "java.lang.Integer", "Float");
          return this;
      }

      public DmsFormSelectSQL andMultiMaxNumNotIn(List<java.lang.Integer> values) {
          addCriterion("multi_max_num", values, ConditionMode.NOT_IN, "multiMaxNum", "java.lang.Integer", "Float");
          return this;
      }
}