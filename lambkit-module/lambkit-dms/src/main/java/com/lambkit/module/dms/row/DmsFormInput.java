/**
 * Copyright (c) 2015-2017, Henry Yang 杨勇 (gismail@foxmail.com).
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.lambkit.module.dms.row;

import com.lambkit.db.RowModel;

/**
 * @author yangyong 
 * @website: www.lambkit.com
 * @email: gismail@foxmail.com
 * @date ${date}
 * @version 1.0
 * @since 1.0
 */

public class DmsFormInput extends RowModel<DmsFormInput> {
	public DmsFormInput() {
		setTableName("dms_form_input");
		setPrimaryKey("form_input_id");
	}
	public java.lang.Long getFormInputId() {
		return this.get("form_input_id");
	}
	public void setFormInputId(java.lang.Long formInputId) {
		this.set("form_input_id", formInputId);
	}
	public java.lang.String getInputType() {
		return this.get("input_type");
	}
	public void setInputType(java.lang.String inputType) {
		this.set("input_type", inputType);
	}
	public java.lang.String getLabel() {
		return this.get("label");
	}
	public void setLabel(java.lang.String label) {
		this.set("label", label);
	}
	public java.lang.Integer getRequired() {
		return this.get("required");
	}
	public void setRequired(java.lang.Integer required) {
		this.set("required", required);
	}
	public java.lang.String getHint() {
		return this.get("hint");
	}
	public void setHint(java.lang.String hint) {
		this.set("hint", hint);
	}
	public java.lang.String getPlaceholder() {
		return this.get("placeholder");
	}
	public void setPlaceholder(java.lang.String placeholder) {
		this.set("placeholder", placeholder);
	}
	public java.lang.Integer getMinLength() {
		return this.get("min_length");
	}
	public void setMinLength(java.lang.Integer minLength) {
		this.set("min_length", minLength);
	}
	public java.lang.Integer getMaxLength() {
		return this.get("max_length");
	}
	public void setMaxLength(java.lang.Integer maxLength) {
		this.set("max_length", maxLength);
	}
	public java.lang.Integer getDisabled() {
		return this.get("disabled");
	}
	public void setDisabled(java.lang.Integer disabled) {
		this.set("disabled", disabled);
	}
	public java.lang.Integer getReadonly() {
		return this.get("readonly");
	}
	public void setReadonly(java.lang.Integer readonly) {
		this.set("readonly", readonly);
	}
	public java.lang.Integer getTextCol() {
		return this.get("text_col");
	}
	public void setTextCol(java.lang.Integer textCol) {
		this.set("text_col", textCol);
	}
	public java.lang.Integer getTextRow() {
		return this.get("text_row");
	}
	public void setTextRow(java.lang.Integer textRow) {
		this.set("text_row", textRow);
	}
	public java.lang.Integer getFldid() {
		return this.get("fldid");
	}
	public void setFldid(java.lang.Integer fldid) {
		this.set("fldid", fldid);
	}
	public java.lang.String getFldname() {
		return this.get("fldname");
	}
	public void setFldname(java.lang.String fldname) {
		this.set("fldname", fldname);
	}
	public java.lang.Integer getStatus() {
		return this.get("status");
	}
	public void setStatus(java.lang.Integer status) {
		this.set("status", status);
	}
	public java.lang.String getDefalutValue() {
		return this.get("defalut_value");
	}
	public void setDefalutValue(java.lang.String defalutValue) {
		this.set("defalut_value", defalutValue);
	}
	public java.lang.Integer getPrecision() {
		return this.get("precision");
	}
	public void setPrecision(java.lang.Integer precision) {
		this.set("precision", precision);
	}
	public java.lang.Double getMinValue() {
		return this.get("min_value");
	}
	public void setMinValue(java.lang.Double minValue) {
		this.set("min_value", minValue);
	}
	public java.lang.Double getMaxValue() {
		return this.get("max_value");
	}
	public void setMaxValue(java.lang.Double maxValue) {
		this.set("max_value", maxValue);
	}
	public java.lang.Float getStepValue() {
		return this.get("step_value");
	}
	public void setStepValue(java.lang.Float stepValue) {
		this.set("step_value", stepValue);
	}
}
