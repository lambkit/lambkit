/**
 * Copyright (c) 2015-2017, Henry Yang 杨勇 (gismail@foxmail.com).
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.lambkit.module.dms.sql;

import com.lambkit.db.sql.ConditionMode;
import com.lambkit.db.sql.Column;
import com.lambkit.db.sql.Columns;
import com.lambkit.db.sql.Example;

import java.io.Serializable;
import java.util.List;

/**
 * @author yangyong 
 * @website: www.lambkit.com
 * @email: gismail@foxmail.com
 * @date ${date}
 * @version 1.0
 * @since 1.0
 */

public class DmsTableLogSQL extends Columns implements Serializable {
	private static final long serialVersionUID = 1L;
	
	public static DmsTableLogSQL of() {
		return new DmsTableLogSQL();
	}
	
	public static DmsTableLogSQL by(Column column) {
		DmsTableLogSQL that = new DmsTableLogSQL();
		that.add(column);
        return that;
    }

    public static DmsTableLogSQL by(String name, Object value) {
        return (DmsTableLogSQL) create().eq(name, value);
    }
    
    public Example example() {
    	return Example.create("dms_table_log", this);
    }
    
    /**
     * equals
     *
     * @param name
     * @param value
     * @return
     */
    public DmsTableLogSQL eq(String name, Object value) {
    	super.eq(name, value);
        return this;
    }

    /**
     * not equals !=
     *
     * @param name
     * @param value
     * @return
     */
    public DmsTableLogSQL ne(String name, Object value) {
    	super.ne(name, value);
        return this;
    }


    /**
     * like
     *
     * @param name
     * @param value
     * @return
     */

    public DmsTableLogSQL like(String name, Object value) {
    	super.like(name, value);
        return this;
    }
    
    public DmsTableLogSQL notLike(String name, Object value) {
    	super.notLike(name, value);
        return this;
    }

    /**
     * 大于 great than
     *
     * @param name
     * @param value
     * @return
     */
    public DmsTableLogSQL gt(String name, Object value) {
    	super.gt(name, value);
        return this;
    }

    /**
     * 大于等于 great or equal
     *
     * @param name
     * @param value
     * @return
     */
    public DmsTableLogSQL ge(String name, Object value) {
    	super.ge(name, value);
        return this;
    }

    /**
     * 小于 less than
     *
     * @param name
     * @param value
     * @return
     */
    public DmsTableLogSQL lt(String name, Object value) {
    	super.lt(name, value);
        return this;
    }

    /**
     * 小于等于 less or equal
     *
     * @param name
     * @param value
     * @return
     */
    public DmsTableLogSQL le(String name, Object value) {
    	super.le(name, value);
        return this;
    }
    
    public DmsTableLogSQL isnull(String name) {
    	super.isnull(name);
        return this;
    } 

    public DmsTableLogSQL notNull(String name) {
    	super.notNull(name);
        return this;
    } 
    
    public DmsTableLogSQL empty(String name) {
    	super.empty(name);
        return this;
    } 
    
    public DmsTableLogSQL notEmpty(String name) {
    	super.notEmpty(name);
        return this;
    } 
    
    public DmsTableLogSQL add(Column column) {
    	super.add(column);
    	return this;
    }
    
    /**************************/
	
	public void addCriterion(String name, Object value, ConditionMode logic, String property, String typeHandler, String valueType) {
		 if (value == null) {
			 throw new RuntimeException("Value for " + property + " cannot be null");
		 }
		 add(Column.create(name, value, logic, typeHandler, valueType));
	}
   
	public void addCriterion(String name, Object value1, Object value2, ConditionMode logic, String property, String typeHandler, String valueType) {
		 if (value1 == null || value2 == null) {
			 throw new RuntimeException("Between values for " + property + " cannot be null");
		 }
		 add(Column.create(name, value1, value2, logic, typeHandler, valueType));
	}
		 
	public DmsTableLogSQL andLogIdIsNull() {
		isnull("log_id");
		return this;
	}
	
	public DmsTableLogSQL andLogIdIsNotNull() {
		notNull("log_id");
		return this;
	}
	
	public DmsTableLogSQL andLogIdIsEmpty() {
		empty("log_id");
		return this;
	}

	public DmsTableLogSQL andLogIdIsNotEmpty() {
		notEmpty("log_id");
		return this;
	}
      public DmsTableLogSQL andLogIdEqualTo(java.lang.Integer value) {
          addCriterion("log_id", value, ConditionMode.EQUAL, "logId", "java.lang.Integer", "Float");
          return this;
      }

      public DmsTableLogSQL andLogIdNotEqualTo(java.lang.Integer value) {
          addCriterion("log_id", value, ConditionMode.NOT_EQUAL, "logId", "java.lang.Integer", "Float");
          return this;
      }

      public DmsTableLogSQL andLogIdGreaterThan(java.lang.Integer value) {
          addCriterion("log_id", value, ConditionMode.GREATER_THEN, "logId", "java.lang.Integer", "Float");
          return this;
      }

      public DmsTableLogSQL andLogIdGreaterThanOrEqualTo(java.lang.Integer value) {
          addCriterion("log_id", value, ConditionMode.GREATER_EQUAL, "logId", "java.lang.Integer", "Float");
          return this;
      }

      public DmsTableLogSQL andLogIdLessThan(java.lang.Integer value) {
          addCriterion("log_id", value, ConditionMode.LESS_THEN, "logId", "java.lang.Integer", "Float");
          return this;
      }

      public DmsTableLogSQL andLogIdLessThanOrEqualTo(java.lang.Integer value) {
          addCriterion("log_id", value, ConditionMode.LESS_EQUAL, "logId", "java.lang.Integer", "Float");
          return this;
      }

      public DmsTableLogSQL andLogIdBetween(java.lang.Integer value1, java.lang.Integer value2) {
    	  addCriterion("log_id", value1, value2, ConditionMode.BETWEEN, "logId", "java.lang.Integer", "Float");
    	  return this;
      }

      public DmsTableLogSQL andLogIdNotBetween(java.lang.Integer value1, java.lang.Integer value2) {
          addCriterion("log_id", value1, value2, ConditionMode.NOT_BETWEEN, "logId", "java.lang.Integer", "Float");
          return this;
      }
        
      public DmsTableLogSQL andLogIdIn(List<java.lang.Integer> values) {
          addCriterion("log_id", values, ConditionMode.IN, "logId", "java.lang.Integer", "Float");
          return this;
      }

      public DmsTableLogSQL andLogIdNotIn(List<java.lang.Integer> values) {
          addCriterion("log_id", values, ConditionMode.NOT_IN, "logId", "java.lang.Integer", "Float");
          return this;
      }
	public DmsTableLogSQL andDescriptionIsNull() {
		isnull("description");
		return this;
	}
	
	public DmsTableLogSQL andDescriptionIsNotNull() {
		notNull("description");
		return this;
	}
	
	public DmsTableLogSQL andDescriptionIsEmpty() {
		empty("description");
		return this;
	}

	public DmsTableLogSQL andDescriptionIsNotEmpty() {
		notEmpty("description");
		return this;
	}
       public DmsTableLogSQL andDescriptionLike(java.lang.String value) {
    	   addCriterion("description", value, ConditionMode.FUZZY, "description", "java.lang.String", "Float");
    	   return this;
      }

      public DmsTableLogSQL andDescriptionNotLike(java.lang.String value) {
          addCriterion("description", value, ConditionMode.NOT_FUZZY, "description", "java.lang.String", "Float");
          return this;
      }
      public DmsTableLogSQL andDescriptionEqualTo(java.lang.String value) {
          addCriterion("description", value, ConditionMode.EQUAL, "description", "java.lang.String", "String");
          return this;
      }

      public DmsTableLogSQL andDescriptionNotEqualTo(java.lang.String value) {
          addCriterion("description", value, ConditionMode.NOT_EQUAL, "description", "java.lang.String", "String");
          return this;
      }

      public DmsTableLogSQL andDescriptionGreaterThan(java.lang.String value) {
          addCriterion("description", value, ConditionMode.GREATER_THEN, "description", "java.lang.String", "String");
          return this;
      }

      public DmsTableLogSQL andDescriptionGreaterThanOrEqualTo(java.lang.String value) {
          addCriterion("description", value, ConditionMode.GREATER_EQUAL, "description", "java.lang.String", "String");
          return this;
      }

      public DmsTableLogSQL andDescriptionLessThan(java.lang.String value) {
          addCriterion("description", value, ConditionMode.LESS_THEN, "description", "java.lang.String", "String");
          return this;
      }

      public DmsTableLogSQL andDescriptionLessThanOrEqualTo(java.lang.String value) {
          addCriterion("description", value, ConditionMode.LESS_EQUAL, "description", "java.lang.String", "String");
          return this;
      }

      public DmsTableLogSQL andDescriptionBetween(java.lang.String value1, java.lang.String value2) {
    	  addCriterion("description", value1, value2, ConditionMode.BETWEEN, "description", "java.lang.String", "String");
    	  return this;
      }

      public DmsTableLogSQL andDescriptionNotBetween(java.lang.String value1, java.lang.String value2) {
          addCriterion("description", value1, value2, ConditionMode.NOT_BETWEEN, "description", "java.lang.String", "String");
          return this;
      }
        
      public DmsTableLogSQL andDescriptionIn(List<java.lang.String> values) {
          addCriterion("description", values, ConditionMode.IN, "description", "java.lang.String", "String");
          return this;
      }

      public DmsTableLogSQL andDescriptionNotIn(List<java.lang.String> values) {
          addCriterion("description", values, ConditionMode.NOT_IN, "description", "java.lang.String", "String");
          return this;
      }
	public DmsTableLogSQL andActionIsNull() {
		isnull("action");
		return this;
	}
	
	public DmsTableLogSQL andActionIsNotNull() {
		notNull("action");
		return this;
	}
	
	public DmsTableLogSQL andActionIsEmpty() {
		empty("action");
		return this;
	}

	public DmsTableLogSQL andActionIsNotEmpty() {
		notEmpty("action");
		return this;
	}
       public DmsTableLogSQL andActionLike(java.lang.String value) {
    	   addCriterion("action", value, ConditionMode.FUZZY, "action", "java.lang.String", "String");
    	   return this;
      }

      public DmsTableLogSQL andActionNotLike(java.lang.String value) {
          addCriterion("action", value, ConditionMode.NOT_FUZZY, "action", "java.lang.String", "String");
          return this;
      }
      public DmsTableLogSQL andActionEqualTo(java.lang.String value) {
          addCriterion("action", value, ConditionMode.EQUAL, "action", "java.lang.String", "String");
          return this;
      }

      public DmsTableLogSQL andActionNotEqualTo(java.lang.String value) {
          addCriterion("action", value, ConditionMode.NOT_EQUAL, "action", "java.lang.String", "String");
          return this;
      }

      public DmsTableLogSQL andActionGreaterThan(java.lang.String value) {
          addCriterion("action", value, ConditionMode.GREATER_THEN, "action", "java.lang.String", "String");
          return this;
      }

      public DmsTableLogSQL andActionGreaterThanOrEqualTo(java.lang.String value) {
          addCriterion("action", value, ConditionMode.GREATER_EQUAL, "action", "java.lang.String", "String");
          return this;
      }

      public DmsTableLogSQL andActionLessThan(java.lang.String value) {
          addCriterion("action", value, ConditionMode.LESS_THEN, "action", "java.lang.String", "String");
          return this;
      }

      public DmsTableLogSQL andActionLessThanOrEqualTo(java.lang.String value) {
          addCriterion("action", value, ConditionMode.LESS_EQUAL, "action", "java.lang.String", "String");
          return this;
      }

      public DmsTableLogSQL andActionBetween(java.lang.String value1, java.lang.String value2) {
    	  addCriterion("action", value1, value2, ConditionMode.BETWEEN, "action", "java.lang.String", "String");
    	  return this;
      }

      public DmsTableLogSQL andActionNotBetween(java.lang.String value1, java.lang.String value2) {
          addCriterion("action", value1, value2, ConditionMode.NOT_BETWEEN, "action", "java.lang.String", "String");
          return this;
      }
        
      public DmsTableLogSQL andActionIn(List<java.lang.String> values) {
          addCriterion("action", values, ConditionMode.IN, "action", "java.lang.String", "String");
          return this;
      }

      public DmsTableLogSQL andActionNotIn(List<java.lang.String> values) {
          addCriterion("action", values, ConditionMode.NOT_IN, "action", "java.lang.String", "String");
          return this;
      }
	public DmsTableLogSQL andFtableIsNull() {
		isnull("ftable");
		return this;
	}
	
	public DmsTableLogSQL andFtableIsNotNull() {
		notNull("ftable");
		return this;
	}
	
	public DmsTableLogSQL andFtableIsEmpty() {
		empty("ftable");
		return this;
	}

	public DmsTableLogSQL andFtableIsNotEmpty() {
		notEmpty("ftable");
		return this;
	}
       public DmsTableLogSQL andFtableLike(java.lang.String value) {
    	   addCriterion("ftable", value, ConditionMode.FUZZY, "ftable", "java.lang.String", "String");
    	   return this;
      }

      public DmsTableLogSQL andFtableNotLike(java.lang.String value) {
          addCriterion("ftable", value, ConditionMode.NOT_FUZZY, "ftable", "java.lang.String", "String");
          return this;
      }
      public DmsTableLogSQL andFtableEqualTo(java.lang.String value) {
          addCriterion("ftable", value, ConditionMode.EQUAL, "ftable", "java.lang.String", "String");
          return this;
      }

      public DmsTableLogSQL andFtableNotEqualTo(java.lang.String value) {
          addCriterion("ftable", value, ConditionMode.NOT_EQUAL, "ftable", "java.lang.String", "String");
          return this;
      }

      public DmsTableLogSQL andFtableGreaterThan(java.lang.String value) {
          addCriterion("ftable", value, ConditionMode.GREATER_THEN, "ftable", "java.lang.String", "String");
          return this;
      }

      public DmsTableLogSQL andFtableGreaterThanOrEqualTo(java.lang.String value) {
          addCriterion("ftable", value, ConditionMode.GREATER_EQUAL, "ftable", "java.lang.String", "String");
          return this;
      }

      public DmsTableLogSQL andFtableLessThan(java.lang.String value) {
          addCriterion("ftable", value, ConditionMode.LESS_THEN, "ftable", "java.lang.String", "String");
          return this;
      }

      public DmsTableLogSQL andFtableLessThanOrEqualTo(java.lang.String value) {
          addCriterion("ftable", value, ConditionMode.LESS_EQUAL, "ftable", "java.lang.String", "String");
          return this;
      }

      public DmsTableLogSQL andFtableBetween(java.lang.String value1, java.lang.String value2) {
    	  addCriterion("ftable", value1, value2, ConditionMode.BETWEEN, "ftable", "java.lang.String", "String");
    	  return this;
      }

      public DmsTableLogSQL andFtableNotBetween(java.lang.String value1, java.lang.String value2) {
          addCriterion("ftable", value1, value2, ConditionMode.NOT_BETWEEN, "ftable", "java.lang.String", "String");
          return this;
      }
        
      public DmsTableLogSQL andFtableIn(List<java.lang.String> values) {
          addCriterion("ftable", values, ConditionMode.IN, "ftable", "java.lang.String", "String");
          return this;
      }

      public DmsTableLogSQL andFtableNotIn(List<java.lang.String> values) {
          addCriterion("ftable", values, ConditionMode.NOT_IN, "ftable", "java.lang.String", "String");
          return this;
      }
	public DmsTableLogSQL andFcolumnIsNull() {
		isnull("fcolumn");
		return this;
	}
	
	public DmsTableLogSQL andFcolumnIsNotNull() {
		notNull("fcolumn");
		return this;
	}
	
	public DmsTableLogSQL andFcolumnIsEmpty() {
		empty("fcolumn");
		return this;
	}

	public DmsTableLogSQL andFcolumnIsNotEmpty() {
		notEmpty("fcolumn");
		return this;
	}
       public DmsTableLogSQL andFcolumnLike(java.lang.String value) {
    	   addCriterion("fcolumn", value, ConditionMode.FUZZY, "fcolumn", "java.lang.String", "String");
    	   return this;
      }

      public DmsTableLogSQL andFcolumnNotLike(java.lang.String value) {
          addCriterion("fcolumn", value, ConditionMode.NOT_FUZZY, "fcolumn", "java.lang.String", "String");
          return this;
      }
      public DmsTableLogSQL andFcolumnEqualTo(java.lang.String value) {
          addCriterion("fcolumn", value, ConditionMode.EQUAL, "fcolumn", "java.lang.String", "String");
          return this;
      }

      public DmsTableLogSQL andFcolumnNotEqualTo(java.lang.String value) {
          addCriterion("fcolumn", value, ConditionMode.NOT_EQUAL, "fcolumn", "java.lang.String", "String");
          return this;
      }

      public DmsTableLogSQL andFcolumnGreaterThan(java.lang.String value) {
          addCriterion("fcolumn", value, ConditionMode.GREATER_THEN, "fcolumn", "java.lang.String", "String");
          return this;
      }

      public DmsTableLogSQL andFcolumnGreaterThanOrEqualTo(java.lang.String value) {
          addCriterion("fcolumn", value, ConditionMode.GREATER_EQUAL, "fcolumn", "java.lang.String", "String");
          return this;
      }

      public DmsTableLogSQL andFcolumnLessThan(java.lang.String value) {
          addCriterion("fcolumn", value, ConditionMode.LESS_THEN, "fcolumn", "java.lang.String", "String");
          return this;
      }

      public DmsTableLogSQL andFcolumnLessThanOrEqualTo(java.lang.String value) {
          addCriterion("fcolumn", value, ConditionMode.LESS_EQUAL, "fcolumn", "java.lang.String", "String");
          return this;
      }

      public DmsTableLogSQL andFcolumnBetween(java.lang.String value1, java.lang.String value2) {
    	  addCriterion("fcolumn", value1, value2, ConditionMode.BETWEEN, "fcolumn", "java.lang.String", "String");
    	  return this;
      }

      public DmsTableLogSQL andFcolumnNotBetween(java.lang.String value1, java.lang.String value2) {
          addCriterion("fcolumn", value1, value2, ConditionMode.NOT_BETWEEN, "fcolumn", "java.lang.String", "String");
          return this;
      }
        
      public DmsTableLogSQL andFcolumnIn(List<java.lang.String> values) {
          addCriterion("fcolumn", values, ConditionMode.IN, "fcolumn", "java.lang.String", "String");
          return this;
      }

      public DmsTableLogSQL andFcolumnNotIn(List<java.lang.String> values) {
          addCriterion("fcolumn", values, ConditionMode.NOT_IN, "fcolumn", "java.lang.String", "String");
          return this;
      }
	public DmsTableLogSQL andFtimeIsNull() {
		isnull("ftime");
		return this;
	}
	
	public DmsTableLogSQL andFtimeIsNotNull() {
		notNull("ftime");
		return this;
	}
	
	public DmsTableLogSQL andFtimeIsEmpty() {
		empty("ftime");
		return this;
	}

	public DmsTableLogSQL andFtimeIsNotEmpty() {
		notEmpty("ftime");
		return this;
	}
      public DmsTableLogSQL andFtimeEqualTo(java.util.Date value) {
          addCriterion("ftime", value, ConditionMode.EQUAL, "ftime", "java.util.Date", "String");
          return this;
      }

      public DmsTableLogSQL andFtimeNotEqualTo(java.util.Date value) {
          addCriterion("ftime", value, ConditionMode.NOT_EQUAL, "ftime", "java.util.Date", "String");
          return this;
      }

      public DmsTableLogSQL andFtimeGreaterThan(java.util.Date value) {
          addCriterion("ftime", value, ConditionMode.GREATER_THEN, "ftime", "java.util.Date", "String");
          return this;
      }

      public DmsTableLogSQL andFtimeGreaterThanOrEqualTo(java.util.Date value) {
          addCriterion("ftime", value, ConditionMode.GREATER_EQUAL, "ftime", "java.util.Date", "String");
          return this;
      }

      public DmsTableLogSQL andFtimeLessThan(java.util.Date value) {
          addCriterion("ftime", value, ConditionMode.LESS_THEN, "ftime", "java.util.Date", "String");
          return this;
      }

      public DmsTableLogSQL andFtimeLessThanOrEqualTo(java.util.Date value) {
          addCriterion("ftime", value, ConditionMode.LESS_EQUAL, "ftime", "java.util.Date", "String");
          return this;
      }

      public DmsTableLogSQL andFtimeBetween(java.util.Date value1, java.util.Date value2) {
    	  addCriterion("ftime", value1, value2, ConditionMode.BETWEEN, "ftime", "java.util.Date", "String");
    	  return this;
      }

      public DmsTableLogSQL andFtimeNotBetween(java.util.Date value1, java.util.Date value2) {
          addCriterion("ftime", value1, value2, ConditionMode.NOT_BETWEEN, "ftime", "java.util.Date", "String");
          return this;
      }
        
      public DmsTableLogSQL andFtimeIn(List<java.util.Date> values) {
          addCriterion("ftime", values, ConditionMode.IN, "ftime", "java.util.Date", "String");
          return this;
      }

      public DmsTableLogSQL andFtimeNotIn(List<java.util.Date> values) {
          addCriterion("ftime", values, ConditionMode.NOT_IN, "ftime", "java.util.Date", "String");
          return this;
      }
	public DmsTableLogSQL andUsernameIsNull() {
		isnull("username");
		return this;
	}
	
	public DmsTableLogSQL andUsernameIsNotNull() {
		notNull("username");
		return this;
	}
	
	public DmsTableLogSQL andUsernameIsEmpty() {
		empty("username");
		return this;
	}

	public DmsTableLogSQL andUsernameIsNotEmpty() {
		notEmpty("username");
		return this;
	}
       public DmsTableLogSQL andUsernameLike(java.lang.String value) {
    	   addCriterion("username", value, ConditionMode.FUZZY, "username", "java.lang.String", "String");
    	   return this;
      }

      public DmsTableLogSQL andUsernameNotLike(java.lang.String value) {
          addCriterion("username", value, ConditionMode.NOT_FUZZY, "username", "java.lang.String", "String");
          return this;
      }
      public DmsTableLogSQL andUsernameEqualTo(java.lang.String value) {
          addCriterion("username", value, ConditionMode.EQUAL, "username", "java.lang.String", "String");
          return this;
      }

      public DmsTableLogSQL andUsernameNotEqualTo(java.lang.String value) {
          addCriterion("username", value, ConditionMode.NOT_EQUAL, "username", "java.lang.String", "String");
          return this;
      }

      public DmsTableLogSQL andUsernameGreaterThan(java.lang.String value) {
          addCriterion("username", value, ConditionMode.GREATER_THEN, "username", "java.lang.String", "String");
          return this;
      }

      public DmsTableLogSQL andUsernameGreaterThanOrEqualTo(java.lang.String value) {
          addCriterion("username", value, ConditionMode.GREATER_EQUAL, "username", "java.lang.String", "String");
          return this;
      }

      public DmsTableLogSQL andUsernameLessThan(java.lang.String value) {
          addCriterion("username", value, ConditionMode.LESS_THEN, "username", "java.lang.String", "String");
          return this;
      }

      public DmsTableLogSQL andUsernameLessThanOrEqualTo(java.lang.String value) {
          addCriterion("username", value, ConditionMode.LESS_EQUAL, "username", "java.lang.String", "String");
          return this;
      }

      public DmsTableLogSQL andUsernameBetween(java.lang.String value1, java.lang.String value2) {
    	  addCriterion("username", value1, value2, ConditionMode.BETWEEN, "username", "java.lang.String", "String");
    	  return this;
      }

      public DmsTableLogSQL andUsernameNotBetween(java.lang.String value1, java.lang.String value2) {
          addCriterion("username", value1, value2, ConditionMode.NOT_BETWEEN, "username", "java.lang.String", "String");
          return this;
      }
        
      public DmsTableLogSQL andUsernameIn(List<java.lang.String> values) {
          addCriterion("username", values, ConditionMode.IN, "username", "java.lang.String", "String");
          return this;
      }

      public DmsTableLogSQL andUsernameNotIn(List<java.lang.String> values) {
          addCriterion("username", values, ConditionMode.NOT_IN, "username", "java.lang.String", "String");
          return this;
      }
}