/**
 * Copyright (c) 2015-2017, Henry Yang 杨勇 (gismail@foxmail.com).
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.lambkit.module.dms.sql;

import com.lambkit.db.sql.ConditionMode;
import com.lambkit.db.sql.Column;
import com.lambkit.db.sql.Columns;
import com.lambkit.db.sql.Example;

import java.io.Serializable;
import java.util.List;

/**
 * @author yangyong 
 * @website: www.lambkit.com
 * @email: gismail@foxmail.com
 * @date ${date}
 * @version 1.0
 * @since 1.0
 */

public class DmsTableEntitySQL extends Columns implements Serializable {
	private static final long serialVersionUID = 1L;
	
	public static DmsTableEntitySQL of() {
		return new DmsTableEntitySQL();
	}
	
	public static DmsTableEntitySQL by(Column column) {
		DmsTableEntitySQL that = new DmsTableEntitySQL();
		that.add(column);
        return that;
    }

    public static DmsTableEntitySQL by(String name, Object value) {
        return (DmsTableEntitySQL) create().eq(name, value);
    }
    
    public Example example() {
    	return Example.create("dms_table_entity", this);
    }
    
    /**
     * equals
     *
     * @param name
     * @param value
     * @return
     */
    public DmsTableEntitySQL eq(String name, Object value) {
    	super.eq(name, value);
        return this;
    }

    /**
     * not equals !=
     *
     * @param name
     * @param value
     * @return
     */
    public DmsTableEntitySQL ne(String name, Object value) {
    	super.ne(name, value);
        return this;
    }


    /**
     * like
     *
     * @param name
     * @param value
     * @return
     */

    public DmsTableEntitySQL like(String name, Object value) {
    	super.like(name, value);
        return this;
    }
    
    public DmsTableEntitySQL notLike(String name, Object value) {
    	super.notLike(name, value);
        return this;
    }

    /**
     * 大于 great than
     *
     * @param name
     * @param value
     * @return
     */
    public DmsTableEntitySQL gt(String name, Object value) {
    	super.gt(name, value);
        return this;
    }

    /**
     * 大于等于 great or equal
     *
     * @param name
     * @param value
     * @return
     */
    public DmsTableEntitySQL ge(String name, Object value) {
    	super.ge(name, value);
        return this;
    }

    /**
     * 小于 less than
     *
     * @param name
     * @param value
     * @return
     */
    public DmsTableEntitySQL lt(String name, Object value) {
    	super.lt(name, value);
        return this;
    }

    /**
     * 小于等于 less or equal
     *
     * @param name
     * @param value
     * @return
     */
    public DmsTableEntitySQL le(String name, Object value) {
    	super.le(name, value);
        return this;
    }
    
    public DmsTableEntitySQL isnull(String name) {
    	super.isnull(name);
        return this;
    } 

    public DmsTableEntitySQL notNull(String name) {
    	super.notNull(name);
        return this;
    } 
    
    public DmsTableEntitySQL empty(String name) {
    	super.empty(name);
        return this;
    } 
    
    public DmsTableEntitySQL notEmpty(String name) {
    	super.notEmpty(name);
        return this;
    } 
    
    public DmsTableEntitySQL add(Column column) {
    	super.add(column);
    	return this;
    }
    
    /**************************/
	
	public void addCriterion(String name, Object value, ConditionMode logic, String property, String typeHandler, String valueType) {
		 if (value == null) {
			 throw new RuntimeException("Value for " + property + " cannot be null");
		 }
		 add(Column.create(name, value, logic, typeHandler, valueType));
	}
   
	public void addCriterion(String name, Object value1, Object value2, ConditionMode logic, String property, String typeHandler, String valueType) {
		 if (value1 == null || value2 == null) {
			 throw new RuntimeException("Between values for " + property + " cannot be null");
		 }
		 add(Column.create(name, value1, value2, logic, typeHandler, valueType));
	}
		 
	public DmsTableEntitySQL andTeidIsNull() {
		isnull("teid");
		return this;
	}
	
	public DmsTableEntitySQL andTeidIsNotNull() {
		notNull("teid");
		return this;
	}
	
	public DmsTableEntitySQL andTeidIsEmpty() {
		empty("teid");
		return this;
	}

	public DmsTableEntitySQL andTeidIsNotEmpty() {
		notEmpty("teid");
		return this;
	}
      public DmsTableEntitySQL andTeidEqualTo(java.lang.Integer value) {
          addCriterion("teid", value, ConditionMode.EQUAL, "teid", "java.lang.Integer", "Float");
          return this;
      }

      public DmsTableEntitySQL andTeidNotEqualTo(java.lang.Integer value) {
          addCriterion("teid", value, ConditionMode.NOT_EQUAL, "teid", "java.lang.Integer", "Float");
          return this;
      }

      public DmsTableEntitySQL andTeidGreaterThan(java.lang.Integer value) {
          addCriterion("teid", value, ConditionMode.GREATER_THEN, "teid", "java.lang.Integer", "Float");
          return this;
      }

      public DmsTableEntitySQL andTeidGreaterThanOrEqualTo(java.lang.Integer value) {
          addCriterion("teid", value, ConditionMode.GREATER_EQUAL, "teid", "java.lang.Integer", "Float");
          return this;
      }

      public DmsTableEntitySQL andTeidLessThan(java.lang.Integer value) {
          addCriterion("teid", value, ConditionMode.LESS_THEN, "teid", "java.lang.Integer", "Float");
          return this;
      }

      public DmsTableEntitySQL andTeidLessThanOrEqualTo(java.lang.Integer value) {
          addCriterion("teid", value, ConditionMode.LESS_EQUAL, "teid", "java.lang.Integer", "Float");
          return this;
      }

      public DmsTableEntitySQL andTeidBetween(java.lang.Integer value1, java.lang.Integer value2) {
    	  addCriterion("teid", value1, value2, ConditionMode.BETWEEN, "teid", "java.lang.Integer", "Float");
    	  return this;
      }

      public DmsTableEntitySQL andTeidNotBetween(java.lang.Integer value1, java.lang.Integer value2) {
          addCriterion("teid", value1, value2, ConditionMode.NOT_BETWEEN, "teid", "java.lang.Integer", "Float");
          return this;
      }
        
      public DmsTableEntitySQL andTeidIn(List<java.lang.Integer> values) {
          addCriterion("teid", values, ConditionMode.IN, "teid", "java.lang.Integer", "Float");
          return this;
      }

      public DmsTableEntitySQL andTeidNotIn(List<java.lang.Integer> values) {
          addCriterion("teid", values, ConditionMode.NOT_IN, "teid", "java.lang.Integer", "Float");
          return this;
      }
	public DmsTableEntitySQL andNameIsNull() {
		isnull("name");
		return this;
	}
	
	public DmsTableEntitySQL andNameIsNotNull() {
		notNull("name");
		return this;
	}
	
	public DmsTableEntitySQL andNameIsEmpty() {
		empty("name");
		return this;
	}

	public DmsTableEntitySQL andNameIsNotEmpty() {
		notEmpty("name");
		return this;
	}
       public DmsTableEntitySQL andNameLike(java.lang.String value) {
    	   addCriterion("name", value, ConditionMode.FUZZY, "name", "java.lang.String", "Float");
    	   return this;
      }

      public DmsTableEntitySQL andNameNotLike(java.lang.String value) {
          addCriterion("name", value, ConditionMode.NOT_FUZZY, "name", "java.lang.String", "Float");
          return this;
      }
      public DmsTableEntitySQL andNameEqualTo(java.lang.String value) {
          addCriterion("name", value, ConditionMode.EQUAL, "name", "java.lang.String", "String");
          return this;
      }

      public DmsTableEntitySQL andNameNotEqualTo(java.lang.String value) {
          addCriterion("name", value, ConditionMode.NOT_EQUAL, "name", "java.lang.String", "String");
          return this;
      }

      public DmsTableEntitySQL andNameGreaterThan(java.lang.String value) {
          addCriterion("name", value, ConditionMode.GREATER_THEN, "name", "java.lang.String", "String");
          return this;
      }

      public DmsTableEntitySQL andNameGreaterThanOrEqualTo(java.lang.String value) {
          addCriterion("name", value, ConditionMode.GREATER_EQUAL, "name", "java.lang.String", "String");
          return this;
      }

      public DmsTableEntitySQL andNameLessThan(java.lang.String value) {
          addCriterion("name", value, ConditionMode.LESS_THEN, "name", "java.lang.String", "String");
          return this;
      }

      public DmsTableEntitySQL andNameLessThanOrEqualTo(java.lang.String value) {
          addCriterion("name", value, ConditionMode.LESS_EQUAL, "name", "java.lang.String", "String");
          return this;
      }

      public DmsTableEntitySQL andNameBetween(java.lang.String value1, java.lang.String value2) {
    	  addCriterion("name", value1, value2, ConditionMode.BETWEEN, "name", "java.lang.String", "String");
    	  return this;
      }

      public DmsTableEntitySQL andNameNotBetween(java.lang.String value1, java.lang.String value2) {
          addCriterion("name", value1, value2, ConditionMode.NOT_BETWEEN, "name", "java.lang.String", "String");
          return this;
      }
        
      public DmsTableEntitySQL andNameIn(List<java.lang.String> values) {
          addCriterion("name", values, ConditionMode.IN, "name", "java.lang.String", "String");
          return this;
      }

      public DmsTableEntitySQL andNameNotIn(List<java.lang.String> values) {
          addCriterion("name", values, ConditionMode.NOT_IN, "name", "java.lang.String", "String");
          return this;
      }
	public DmsTableEntitySQL andEntityTypeIsNull() {
		isnull("entity_type");
		return this;
	}
	
	public DmsTableEntitySQL andEntityTypeIsNotNull() {
		notNull("entity_type");
		return this;
	}
	
	public DmsTableEntitySQL andEntityTypeIsEmpty() {
		empty("entity_type");
		return this;
	}

	public DmsTableEntitySQL andEntityTypeIsNotEmpty() {
		notEmpty("entity_type");
		return this;
	}
       public DmsTableEntitySQL andEntityTypeLike(java.lang.String value) {
    	   addCriterion("entity_type", value, ConditionMode.FUZZY, "entityType", "java.lang.String", "String");
    	   return this;
      }

      public DmsTableEntitySQL andEntityTypeNotLike(java.lang.String value) {
          addCriterion("entity_type", value, ConditionMode.NOT_FUZZY, "entityType", "java.lang.String", "String");
          return this;
      }
      public DmsTableEntitySQL andEntityTypeEqualTo(java.lang.String value) {
          addCriterion("entity_type", value, ConditionMode.EQUAL, "entityType", "java.lang.String", "String");
          return this;
      }

      public DmsTableEntitySQL andEntityTypeNotEqualTo(java.lang.String value) {
          addCriterion("entity_type", value, ConditionMode.NOT_EQUAL, "entityType", "java.lang.String", "String");
          return this;
      }

      public DmsTableEntitySQL andEntityTypeGreaterThan(java.lang.String value) {
          addCriterion("entity_type", value, ConditionMode.GREATER_THEN, "entityType", "java.lang.String", "String");
          return this;
      }

      public DmsTableEntitySQL andEntityTypeGreaterThanOrEqualTo(java.lang.String value) {
          addCriterion("entity_type", value, ConditionMode.GREATER_EQUAL, "entityType", "java.lang.String", "String");
          return this;
      }

      public DmsTableEntitySQL andEntityTypeLessThan(java.lang.String value) {
          addCriterion("entity_type", value, ConditionMode.LESS_THEN, "entityType", "java.lang.String", "String");
          return this;
      }

      public DmsTableEntitySQL andEntityTypeLessThanOrEqualTo(java.lang.String value) {
          addCriterion("entity_type", value, ConditionMode.LESS_EQUAL, "entityType", "java.lang.String", "String");
          return this;
      }

      public DmsTableEntitySQL andEntityTypeBetween(java.lang.String value1, java.lang.String value2) {
    	  addCriterion("entity_type", value1, value2, ConditionMode.BETWEEN, "entityType", "java.lang.String", "String");
    	  return this;
      }

      public DmsTableEntitySQL andEntityTypeNotBetween(java.lang.String value1, java.lang.String value2) {
          addCriterion("entity_type", value1, value2, ConditionMode.NOT_BETWEEN, "entityType", "java.lang.String", "String");
          return this;
      }
        
      public DmsTableEntitySQL andEntityTypeIn(List<java.lang.String> values) {
          addCriterion("entity_type", values, ConditionMode.IN, "entityType", "java.lang.String", "String");
          return this;
      }

      public DmsTableEntitySQL andEntityTypeNotIn(List<java.lang.String> values) {
          addCriterion("entity_type", values, ConditionMode.NOT_IN, "entityType", "java.lang.String", "String");
          return this;
      }
	public DmsTableEntitySQL andDescriptionIsNull() {
		isnull("description");
		return this;
	}
	
	public DmsTableEntitySQL andDescriptionIsNotNull() {
		notNull("description");
		return this;
	}
	
	public DmsTableEntitySQL andDescriptionIsEmpty() {
		empty("description");
		return this;
	}

	public DmsTableEntitySQL andDescriptionIsNotEmpty() {
		notEmpty("description");
		return this;
	}
       public DmsTableEntitySQL andDescriptionLike(java.lang.String value) {
    	   addCriterion("description", value, ConditionMode.FUZZY, "description", "java.lang.String", "String");
    	   return this;
      }

      public DmsTableEntitySQL andDescriptionNotLike(java.lang.String value) {
          addCriterion("description", value, ConditionMode.NOT_FUZZY, "description", "java.lang.String", "String");
          return this;
      }
      public DmsTableEntitySQL andDescriptionEqualTo(java.lang.String value) {
          addCriterion("description", value, ConditionMode.EQUAL, "description", "java.lang.String", "String");
          return this;
      }

      public DmsTableEntitySQL andDescriptionNotEqualTo(java.lang.String value) {
          addCriterion("description", value, ConditionMode.NOT_EQUAL, "description", "java.lang.String", "String");
          return this;
      }

      public DmsTableEntitySQL andDescriptionGreaterThan(java.lang.String value) {
          addCriterion("description", value, ConditionMode.GREATER_THEN, "description", "java.lang.String", "String");
          return this;
      }

      public DmsTableEntitySQL andDescriptionGreaterThanOrEqualTo(java.lang.String value) {
          addCriterion("description", value, ConditionMode.GREATER_EQUAL, "description", "java.lang.String", "String");
          return this;
      }

      public DmsTableEntitySQL andDescriptionLessThan(java.lang.String value) {
          addCriterion("description", value, ConditionMode.LESS_THEN, "description", "java.lang.String", "String");
          return this;
      }

      public DmsTableEntitySQL andDescriptionLessThanOrEqualTo(java.lang.String value) {
          addCriterion("description", value, ConditionMode.LESS_EQUAL, "description", "java.lang.String", "String");
          return this;
      }

      public DmsTableEntitySQL andDescriptionBetween(java.lang.String value1, java.lang.String value2) {
    	  addCriterion("description", value1, value2, ConditionMode.BETWEEN, "description", "java.lang.String", "String");
    	  return this;
      }

      public DmsTableEntitySQL andDescriptionNotBetween(java.lang.String value1, java.lang.String value2) {
          addCriterion("description", value1, value2, ConditionMode.NOT_BETWEEN, "description", "java.lang.String", "String");
          return this;
      }
        
      public DmsTableEntitySQL andDescriptionIn(List<java.lang.String> values) {
          addCriterion("description", values, ConditionMode.IN, "description", "java.lang.String", "String");
          return this;
      }

      public DmsTableEntitySQL andDescriptionNotIn(List<java.lang.String> values) {
          addCriterion("description", values, ConditionMode.NOT_IN, "description", "java.lang.String", "String");
          return this;
      }
}