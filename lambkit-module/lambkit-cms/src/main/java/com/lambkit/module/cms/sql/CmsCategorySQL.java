/**
 * Copyright (c) 2015-2017, Henry Yang 杨勇 (gismail@foxmail.com).
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.lambkit.module.cms.sql;

import com.lambkit.db.sql.ConditionMode;
import com.lambkit.db.sql.Column;
import com.lambkit.db.sql.Columns;
import com.lambkit.db.sql.Example;

import java.io.Serializable;
import java.util.List;

/**
 * @author yangyong 
 * @website: www.lambkit.com
 * @email: gismail@foxmail.com
 * @date ${date}
 * @version 1.0
 * @since 1.0
 */

public class CmsCategorySQL extends Columns implements Serializable {
	private static final long serialVersionUID = 1L;
	
	public static CmsCategorySQL of() {
		return new CmsCategorySQL();
	}
	
	public static CmsCategorySQL by(Column column) {
		CmsCategorySQL that = new CmsCategorySQL();
		that.add(column);
        return that;
    }

    public static CmsCategorySQL by(String name, Object value) {
        return (CmsCategorySQL) create().eq(name, value);
    }
    
    public Example example() {
    	return Example.create("cms_category", this);
    }
    
    /**
     * equals
     *
     * @param name
     * @param value
     * @return
     */
    public CmsCategorySQL eq(String name, Object value) {
    	super.eq(name, value);
        return this;
    }

    /**
     * not equals !=
     *
     * @param name
     * @param value
     * @return
     */
    public CmsCategorySQL ne(String name, Object value) {
    	super.ne(name, value);
        return this;
    }


    /**
     * like
     *
     * @param name
     * @param value
     * @return
     */

    public CmsCategorySQL like(String name, Object value) {
    	super.like(name, value);
        return this;
    }
    
    public CmsCategorySQL notLike(String name, Object value) {
    	super.notLike(name, value);
        return this;
    }

    /**
     * 大于 great than
     *
     * @param name
     * @param value
     * @return
     */
    public CmsCategorySQL gt(String name, Object value) {
    	super.gt(name, value);
        return this;
    }

    /**
     * 大于等于 great or equal
     *
     * @param name
     * @param value
     * @return
     */
    public CmsCategorySQL ge(String name, Object value) {
    	super.ge(name, value);
        return this;
    }

    /**
     * 小于 less than
     *
     * @param name
     * @param value
     * @return
     */
    public CmsCategorySQL lt(String name, Object value) {
    	super.lt(name, value);
        return this;
    }

    /**
     * 小于等于 less or equal
     *
     * @param name
     * @param value
     * @return
     */
    public CmsCategorySQL le(String name, Object value) {
    	super.le(name, value);
        return this;
    }
    
    public CmsCategorySQL isnull(String name) {
    	super.isnull(name);
        return this;
    } 

    public CmsCategorySQL notNull(String name) {
    	super.notNull(name);
        return this;
    } 
    
    public CmsCategorySQL empty(String name) {
    	super.empty(name);
        return this;
    } 
    
    public CmsCategorySQL notEmpty(String name) {
    	super.notEmpty(name);
        return this;
    } 
    
    public CmsCategorySQL add(Column column) {
    	super.add(column);
    	return this;
    }
    
    /**************************/
	
	public void addCriterion(String name, Object value, ConditionMode logic, String property, String typeHandler, String valueType) {
		 if (value == null) {
			 throw new RuntimeException("Value for " + property + " cannot be null");
		 }
		 add(Column.create(name, value, logic, typeHandler, valueType));
	}
   
	public void addCriterion(String name, Object value1, Object value2, ConditionMode logic, String property, String typeHandler, String valueType) {
		 if (value1 == null || value2 == null) {
			 throw new RuntimeException("Between values for " + property + " cannot be null");
		 }
		 add(Column.create(name, value1, value2, logic, typeHandler, valueType));
	}
		 
	public CmsCategorySQL andCategoryIdIsNull() {
		isnull("category_id");
		return this;
	}
	
	public CmsCategorySQL andCategoryIdIsNotNull() {
		notNull("category_id");
		return this;
	}
	
	public CmsCategorySQL andCategoryIdIsEmpty() {
		empty("category_id");
		return this;
	}

	public CmsCategorySQL andCategoryIdIsNotEmpty() {
		notEmpty("category_id");
		return this;
	}
      public CmsCategorySQL andCategoryIdEqualTo(java.lang.Long value) {
          addCriterion("category_id", value, ConditionMode.EQUAL, "categoryId", "java.lang.Long", "Float");
          return this;
      }

      public CmsCategorySQL andCategoryIdNotEqualTo(java.lang.Long value) {
          addCriterion("category_id", value, ConditionMode.NOT_EQUAL, "categoryId", "java.lang.Long", "Float");
          return this;
      }

      public CmsCategorySQL andCategoryIdGreaterThan(java.lang.Long value) {
          addCriterion("category_id", value, ConditionMode.GREATER_THEN, "categoryId", "java.lang.Long", "Float");
          return this;
      }

      public CmsCategorySQL andCategoryIdGreaterThanOrEqualTo(java.lang.Long value) {
          addCriterion("category_id", value, ConditionMode.GREATER_EQUAL, "categoryId", "java.lang.Long", "Float");
          return this;
      }

      public CmsCategorySQL andCategoryIdLessThan(java.lang.Long value) {
          addCriterion("category_id", value, ConditionMode.LESS_THEN, "categoryId", "java.lang.Long", "Float");
          return this;
      }

      public CmsCategorySQL andCategoryIdLessThanOrEqualTo(java.lang.Long value) {
          addCriterion("category_id", value, ConditionMode.LESS_EQUAL, "categoryId", "java.lang.Long", "Float");
          return this;
      }

      public CmsCategorySQL andCategoryIdBetween(java.lang.Long value1, java.lang.Long value2) {
    	  addCriterion("category_id", value1, value2, ConditionMode.BETWEEN, "categoryId", "java.lang.Long", "Float");
    	  return this;
      }

      public CmsCategorySQL andCategoryIdNotBetween(java.lang.Long value1, java.lang.Long value2) {
          addCriterion("category_id", value1, value2, ConditionMode.NOT_BETWEEN, "categoryId", "java.lang.Long", "Float");
          return this;
      }
        
      public CmsCategorySQL andCategoryIdIn(List<java.lang.Long> values) {
          addCriterion("category_id", values, ConditionMode.IN, "categoryId", "java.lang.Long", "Float");
          return this;
      }

      public CmsCategorySQL andCategoryIdNotIn(List<java.lang.Long> values) {
          addCriterion("category_id", values, ConditionMode.NOT_IN, "categoryId", "java.lang.Long", "Float");
          return this;
      }
	public CmsCategorySQL andPidIsNull() {
		isnull("pid");
		return this;
	}
	
	public CmsCategorySQL andPidIsNotNull() {
		notNull("pid");
		return this;
	}
	
	public CmsCategorySQL andPidIsEmpty() {
		empty("pid");
		return this;
	}

	public CmsCategorySQL andPidIsNotEmpty() {
		notEmpty("pid");
		return this;
	}
      public CmsCategorySQL andPidEqualTo(java.lang.Long value) {
          addCriterion("pid", value, ConditionMode.EQUAL, "pid", "java.lang.Long", "Float");
          return this;
      }

      public CmsCategorySQL andPidNotEqualTo(java.lang.Long value) {
          addCriterion("pid", value, ConditionMode.NOT_EQUAL, "pid", "java.lang.Long", "Float");
          return this;
      }

      public CmsCategorySQL andPidGreaterThan(java.lang.Long value) {
          addCriterion("pid", value, ConditionMode.GREATER_THEN, "pid", "java.lang.Long", "Float");
          return this;
      }

      public CmsCategorySQL andPidGreaterThanOrEqualTo(java.lang.Long value) {
          addCriterion("pid", value, ConditionMode.GREATER_EQUAL, "pid", "java.lang.Long", "Float");
          return this;
      }

      public CmsCategorySQL andPidLessThan(java.lang.Long value) {
          addCriterion("pid", value, ConditionMode.LESS_THEN, "pid", "java.lang.Long", "Float");
          return this;
      }

      public CmsCategorySQL andPidLessThanOrEqualTo(java.lang.Long value) {
          addCriterion("pid", value, ConditionMode.LESS_EQUAL, "pid", "java.lang.Long", "Float");
          return this;
      }

      public CmsCategorySQL andPidBetween(java.lang.Long value1, java.lang.Long value2) {
    	  addCriterion("pid", value1, value2, ConditionMode.BETWEEN, "pid", "java.lang.Long", "Float");
    	  return this;
      }

      public CmsCategorySQL andPidNotBetween(java.lang.Long value1, java.lang.Long value2) {
          addCriterion("pid", value1, value2, ConditionMode.NOT_BETWEEN, "pid", "java.lang.Long", "Float");
          return this;
      }
        
      public CmsCategorySQL andPidIn(List<java.lang.Long> values) {
          addCriterion("pid", values, ConditionMode.IN, "pid", "java.lang.Long", "Float");
          return this;
      }

      public CmsCategorySQL andPidNotIn(List<java.lang.Long> values) {
          addCriterion("pid", values, ConditionMode.NOT_IN, "pid", "java.lang.Long", "Float");
          return this;
      }
	public CmsCategorySQL andLevelIsNull() {
		isnull("level");
		return this;
	}
	
	public CmsCategorySQL andLevelIsNotNull() {
		notNull("level");
		return this;
	}
	
	public CmsCategorySQL andLevelIsEmpty() {
		empty("level");
		return this;
	}

	public CmsCategorySQL andLevelIsNotEmpty() {
		notEmpty("level");
		return this;
	}
      public CmsCategorySQL andLevelEqualTo(java.lang.Integer value) {
          addCriterion("level", value, ConditionMode.EQUAL, "level", "java.lang.Integer", "Float");
          return this;
      }

      public CmsCategorySQL andLevelNotEqualTo(java.lang.Integer value) {
          addCriterion("level", value, ConditionMode.NOT_EQUAL, "level", "java.lang.Integer", "Float");
          return this;
      }

      public CmsCategorySQL andLevelGreaterThan(java.lang.Integer value) {
          addCriterion("level", value, ConditionMode.GREATER_THEN, "level", "java.lang.Integer", "Float");
          return this;
      }

      public CmsCategorySQL andLevelGreaterThanOrEqualTo(java.lang.Integer value) {
          addCriterion("level", value, ConditionMode.GREATER_EQUAL, "level", "java.lang.Integer", "Float");
          return this;
      }

      public CmsCategorySQL andLevelLessThan(java.lang.Integer value) {
          addCriterion("level", value, ConditionMode.LESS_THEN, "level", "java.lang.Integer", "Float");
          return this;
      }

      public CmsCategorySQL andLevelLessThanOrEqualTo(java.lang.Integer value) {
          addCriterion("level", value, ConditionMode.LESS_EQUAL, "level", "java.lang.Integer", "Float");
          return this;
      }

      public CmsCategorySQL andLevelBetween(java.lang.Integer value1, java.lang.Integer value2) {
    	  addCriterion("level", value1, value2, ConditionMode.BETWEEN, "level", "java.lang.Integer", "Float");
    	  return this;
      }

      public CmsCategorySQL andLevelNotBetween(java.lang.Integer value1, java.lang.Integer value2) {
          addCriterion("level", value1, value2, ConditionMode.NOT_BETWEEN, "level", "java.lang.Integer", "Float");
          return this;
      }
        
      public CmsCategorySQL andLevelIn(List<java.lang.Integer> values) {
          addCriterion("level", values, ConditionMode.IN, "level", "java.lang.Integer", "Float");
          return this;
      }

      public CmsCategorySQL andLevelNotIn(List<java.lang.Integer> values) {
          addCriterion("level", values, ConditionMode.NOT_IN, "level", "java.lang.Integer", "Float");
          return this;
      }
	public CmsCategorySQL andPathIsNull() {
		isnull("path");
		return this;
	}
	
	public CmsCategorySQL andPathIsNotNull() {
		notNull("path");
		return this;
	}
	
	public CmsCategorySQL andPathIsEmpty() {
		empty("path");
		return this;
	}

	public CmsCategorySQL andPathIsNotEmpty() {
		notEmpty("path");
		return this;
	}
       public CmsCategorySQL andPathLike(java.lang.String value) {
    	   addCriterion("path", value, ConditionMode.FUZZY, "path", "java.lang.String", "Float");
    	   return this;
      }

      public CmsCategorySQL andPathNotLike(java.lang.String value) {
          addCriterion("path", value, ConditionMode.NOT_FUZZY, "path", "java.lang.String", "Float");
          return this;
      }
      public CmsCategorySQL andPathEqualTo(java.lang.String value) {
          addCriterion("path", value, ConditionMode.EQUAL, "path", "java.lang.String", "String");
          return this;
      }

      public CmsCategorySQL andPathNotEqualTo(java.lang.String value) {
          addCriterion("path", value, ConditionMode.NOT_EQUAL, "path", "java.lang.String", "String");
          return this;
      }

      public CmsCategorySQL andPathGreaterThan(java.lang.String value) {
          addCriterion("path", value, ConditionMode.GREATER_THEN, "path", "java.lang.String", "String");
          return this;
      }

      public CmsCategorySQL andPathGreaterThanOrEqualTo(java.lang.String value) {
          addCriterion("path", value, ConditionMode.GREATER_EQUAL, "path", "java.lang.String", "String");
          return this;
      }

      public CmsCategorySQL andPathLessThan(java.lang.String value) {
          addCriterion("path", value, ConditionMode.LESS_THEN, "path", "java.lang.String", "String");
          return this;
      }

      public CmsCategorySQL andPathLessThanOrEqualTo(java.lang.String value) {
          addCriterion("path", value, ConditionMode.LESS_EQUAL, "path", "java.lang.String", "String");
          return this;
      }

      public CmsCategorySQL andPathBetween(java.lang.String value1, java.lang.String value2) {
    	  addCriterion("path", value1, value2, ConditionMode.BETWEEN, "path", "java.lang.String", "String");
    	  return this;
      }

      public CmsCategorySQL andPathNotBetween(java.lang.String value1, java.lang.String value2) {
          addCriterion("path", value1, value2, ConditionMode.NOT_BETWEEN, "path", "java.lang.String", "String");
          return this;
      }
        
      public CmsCategorySQL andPathIn(List<java.lang.String> values) {
          addCriterion("path", values, ConditionMode.IN, "path", "java.lang.String", "String");
          return this;
      }

      public CmsCategorySQL andPathNotIn(List<java.lang.String> values) {
          addCriterion("path", values, ConditionMode.NOT_IN, "path", "java.lang.String", "String");
          return this;
      }
	public CmsCategorySQL andNameIsNull() {
		isnull("name");
		return this;
	}
	
	public CmsCategorySQL andNameIsNotNull() {
		notNull("name");
		return this;
	}
	
	public CmsCategorySQL andNameIsEmpty() {
		empty("name");
		return this;
	}

	public CmsCategorySQL andNameIsNotEmpty() {
		notEmpty("name");
		return this;
	}
       public CmsCategorySQL andNameLike(java.lang.String value) {
    	   addCriterion("name", value, ConditionMode.FUZZY, "name", "java.lang.String", "String");
    	   return this;
      }

      public CmsCategorySQL andNameNotLike(java.lang.String value) {
          addCriterion("name", value, ConditionMode.NOT_FUZZY, "name", "java.lang.String", "String");
          return this;
      }
      public CmsCategorySQL andNameEqualTo(java.lang.String value) {
          addCriterion("name", value, ConditionMode.EQUAL, "name", "java.lang.String", "String");
          return this;
      }

      public CmsCategorySQL andNameNotEqualTo(java.lang.String value) {
          addCriterion("name", value, ConditionMode.NOT_EQUAL, "name", "java.lang.String", "String");
          return this;
      }

      public CmsCategorySQL andNameGreaterThan(java.lang.String value) {
          addCriterion("name", value, ConditionMode.GREATER_THEN, "name", "java.lang.String", "String");
          return this;
      }

      public CmsCategorySQL andNameGreaterThanOrEqualTo(java.lang.String value) {
          addCriterion("name", value, ConditionMode.GREATER_EQUAL, "name", "java.lang.String", "String");
          return this;
      }

      public CmsCategorySQL andNameLessThan(java.lang.String value) {
          addCriterion("name", value, ConditionMode.LESS_THEN, "name", "java.lang.String", "String");
          return this;
      }

      public CmsCategorySQL andNameLessThanOrEqualTo(java.lang.String value) {
          addCriterion("name", value, ConditionMode.LESS_EQUAL, "name", "java.lang.String", "String");
          return this;
      }

      public CmsCategorySQL andNameBetween(java.lang.String value1, java.lang.String value2) {
    	  addCriterion("name", value1, value2, ConditionMode.BETWEEN, "name", "java.lang.String", "String");
    	  return this;
      }

      public CmsCategorySQL andNameNotBetween(java.lang.String value1, java.lang.String value2) {
          addCriterion("name", value1, value2, ConditionMode.NOT_BETWEEN, "name", "java.lang.String", "String");
          return this;
      }
        
      public CmsCategorySQL andNameIn(List<java.lang.String> values) {
          addCriterion("name", values, ConditionMode.IN, "name", "java.lang.String", "String");
          return this;
      }

      public CmsCategorySQL andNameNotIn(List<java.lang.String> values) {
          addCriterion("name", values, ConditionMode.NOT_IN, "name", "java.lang.String", "String");
          return this;
      }
	public CmsCategorySQL andDescriptionIsNull() {
		isnull("description");
		return this;
	}
	
	public CmsCategorySQL andDescriptionIsNotNull() {
		notNull("description");
		return this;
	}
	
	public CmsCategorySQL andDescriptionIsEmpty() {
		empty("description");
		return this;
	}

	public CmsCategorySQL andDescriptionIsNotEmpty() {
		notEmpty("description");
		return this;
	}
       public CmsCategorySQL andDescriptionLike(java.lang.String value) {
    	   addCriterion("description", value, ConditionMode.FUZZY, "description", "java.lang.String", "String");
    	   return this;
      }

      public CmsCategorySQL andDescriptionNotLike(java.lang.String value) {
          addCriterion("description", value, ConditionMode.NOT_FUZZY, "description", "java.lang.String", "String");
          return this;
      }
      public CmsCategorySQL andDescriptionEqualTo(java.lang.String value) {
          addCriterion("description", value, ConditionMode.EQUAL, "description", "java.lang.String", "String");
          return this;
      }

      public CmsCategorySQL andDescriptionNotEqualTo(java.lang.String value) {
          addCriterion("description", value, ConditionMode.NOT_EQUAL, "description", "java.lang.String", "String");
          return this;
      }

      public CmsCategorySQL andDescriptionGreaterThan(java.lang.String value) {
          addCriterion("description", value, ConditionMode.GREATER_THEN, "description", "java.lang.String", "String");
          return this;
      }

      public CmsCategorySQL andDescriptionGreaterThanOrEqualTo(java.lang.String value) {
          addCriterion("description", value, ConditionMode.GREATER_EQUAL, "description", "java.lang.String", "String");
          return this;
      }

      public CmsCategorySQL andDescriptionLessThan(java.lang.String value) {
          addCriterion("description", value, ConditionMode.LESS_THEN, "description", "java.lang.String", "String");
          return this;
      }

      public CmsCategorySQL andDescriptionLessThanOrEqualTo(java.lang.String value) {
          addCriterion("description", value, ConditionMode.LESS_EQUAL, "description", "java.lang.String", "String");
          return this;
      }

      public CmsCategorySQL andDescriptionBetween(java.lang.String value1, java.lang.String value2) {
    	  addCriterion("description", value1, value2, ConditionMode.BETWEEN, "description", "java.lang.String", "String");
    	  return this;
      }

      public CmsCategorySQL andDescriptionNotBetween(java.lang.String value1, java.lang.String value2) {
          addCriterion("description", value1, value2, ConditionMode.NOT_BETWEEN, "description", "java.lang.String", "String");
          return this;
      }
        
      public CmsCategorySQL andDescriptionIn(List<java.lang.String> values) {
          addCriterion("description", values, ConditionMode.IN, "description", "java.lang.String", "String");
          return this;
      }

      public CmsCategorySQL andDescriptionNotIn(List<java.lang.String> values) {
          addCriterion("description", values, ConditionMode.NOT_IN, "description", "java.lang.String", "String");
          return this;
      }
	public CmsCategorySQL andIconIsNull() {
		isnull("icon");
		return this;
	}
	
	public CmsCategorySQL andIconIsNotNull() {
		notNull("icon");
		return this;
	}
	
	public CmsCategorySQL andIconIsEmpty() {
		empty("icon");
		return this;
	}

	public CmsCategorySQL andIconIsNotEmpty() {
		notEmpty("icon");
		return this;
	}
       public CmsCategorySQL andIconLike(java.lang.String value) {
    	   addCriterion("icon", value, ConditionMode.FUZZY, "icon", "java.lang.String", "String");
    	   return this;
      }

      public CmsCategorySQL andIconNotLike(java.lang.String value) {
          addCriterion("icon", value, ConditionMode.NOT_FUZZY, "icon", "java.lang.String", "String");
          return this;
      }
      public CmsCategorySQL andIconEqualTo(java.lang.String value) {
          addCriterion("icon", value, ConditionMode.EQUAL, "icon", "java.lang.String", "String");
          return this;
      }

      public CmsCategorySQL andIconNotEqualTo(java.lang.String value) {
          addCriterion("icon", value, ConditionMode.NOT_EQUAL, "icon", "java.lang.String", "String");
          return this;
      }

      public CmsCategorySQL andIconGreaterThan(java.lang.String value) {
          addCriterion("icon", value, ConditionMode.GREATER_THEN, "icon", "java.lang.String", "String");
          return this;
      }

      public CmsCategorySQL andIconGreaterThanOrEqualTo(java.lang.String value) {
          addCriterion("icon", value, ConditionMode.GREATER_EQUAL, "icon", "java.lang.String", "String");
          return this;
      }

      public CmsCategorySQL andIconLessThan(java.lang.String value) {
          addCriterion("icon", value, ConditionMode.LESS_THEN, "icon", "java.lang.String", "String");
          return this;
      }

      public CmsCategorySQL andIconLessThanOrEqualTo(java.lang.String value) {
          addCriterion("icon", value, ConditionMode.LESS_EQUAL, "icon", "java.lang.String", "String");
          return this;
      }

      public CmsCategorySQL andIconBetween(java.lang.String value1, java.lang.String value2) {
    	  addCriterion("icon", value1, value2, ConditionMode.BETWEEN, "icon", "java.lang.String", "String");
    	  return this;
      }

      public CmsCategorySQL andIconNotBetween(java.lang.String value1, java.lang.String value2) {
          addCriterion("icon", value1, value2, ConditionMode.NOT_BETWEEN, "icon", "java.lang.String", "String");
          return this;
      }
        
      public CmsCategorySQL andIconIn(List<java.lang.String> values) {
          addCriterion("icon", values, ConditionMode.IN, "icon", "java.lang.String", "String");
          return this;
      }

      public CmsCategorySQL andIconNotIn(List<java.lang.String> values) {
          addCriterion("icon", values, ConditionMode.NOT_IN, "icon", "java.lang.String", "String");
          return this;
      }
	public CmsCategorySQL andUserIdIsNull() {
		isnull("user_id");
		return this;
	}
	
	public CmsCategorySQL andUserIdIsNotNull() {
		notNull("user_id");
		return this;
	}
	
	public CmsCategorySQL andUserIdIsEmpty() {
		empty("user_id");
		return this;
	}

	public CmsCategorySQL andUserIdIsNotEmpty() {
		notEmpty("user_id");
		return this;
	}
      public CmsCategorySQL andUserIdEqualTo(java.lang.Long value) {
          addCriterion("user_id", value, ConditionMode.EQUAL, "userId", "java.lang.Long", "Float");
          return this;
      }

      public CmsCategorySQL andUserIdNotEqualTo(java.lang.Long value) {
          addCriterion("user_id", value, ConditionMode.NOT_EQUAL, "userId", "java.lang.Long", "Float");
          return this;
      }

      public CmsCategorySQL andUserIdGreaterThan(java.lang.Long value) {
          addCriterion("user_id", value, ConditionMode.GREATER_THEN, "userId", "java.lang.Long", "Float");
          return this;
      }

      public CmsCategorySQL andUserIdGreaterThanOrEqualTo(java.lang.Long value) {
          addCriterion("user_id", value, ConditionMode.GREATER_EQUAL, "userId", "java.lang.Long", "Float");
          return this;
      }

      public CmsCategorySQL andUserIdLessThan(java.lang.Long value) {
          addCriterion("user_id", value, ConditionMode.LESS_THEN, "userId", "java.lang.Long", "Float");
          return this;
      }

      public CmsCategorySQL andUserIdLessThanOrEqualTo(java.lang.Long value) {
          addCriterion("user_id", value, ConditionMode.LESS_EQUAL, "userId", "java.lang.Long", "Float");
          return this;
      }

      public CmsCategorySQL andUserIdBetween(java.lang.Long value1, java.lang.Long value2) {
    	  addCriterion("user_id", value1, value2, ConditionMode.BETWEEN, "userId", "java.lang.Long", "Float");
    	  return this;
      }

      public CmsCategorySQL andUserIdNotBetween(java.lang.Long value1, java.lang.Long value2) {
          addCriterion("user_id", value1, value2, ConditionMode.NOT_BETWEEN, "userId", "java.lang.Long", "Float");
          return this;
      }
        
      public CmsCategorySQL andUserIdIn(List<java.lang.Long> values) {
          addCriterion("user_id", values, ConditionMode.IN, "userId", "java.lang.Long", "Float");
          return this;
      }

      public CmsCategorySQL andUserIdNotIn(List<java.lang.Long> values) {
          addCriterion("user_id", values, ConditionMode.NOT_IN, "userId", "java.lang.Long", "Float");
          return this;
      }
	public CmsCategorySQL andPublishIsNull() {
		isnull("publish");
		return this;
	}
	
	public CmsCategorySQL andPublishIsNotNull() {
		notNull("publish");
		return this;
	}
	
	public CmsCategorySQL andPublishIsEmpty() {
		empty("publish");
		return this;
	}

	public CmsCategorySQL andPublishIsNotEmpty() {
		notEmpty("publish");
		return this;
	}
      public CmsCategorySQL andPublishEqualTo(java.lang.Integer value) {
          addCriterion("publish", value, ConditionMode.EQUAL, "publish", "java.lang.Integer", "Float");
          return this;
      }

      public CmsCategorySQL andPublishNotEqualTo(java.lang.Integer value) {
          addCriterion("publish", value, ConditionMode.NOT_EQUAL, "publish", "java.lang.Integer", "Float");
          return this;
      }

      public CmsCategorySQL andPublishGreaterThan(java.lang.Integer value) {
          addCriterion("publish", value, ConditionMode.GREATER_THEN, "publish", "java.lang.Integer", "Float");
          return this;
      }

      public CmsCategorySQL andPublishGreaterThanOrEqualTo(java.lang.Integer value) {
          addCriterion("publish", value, ConditionMode.GREATER_EQUAL, "publish", "java.lang.Integer", "Float");
          return this;
      }

      public CmsCategorySQL andPublishLessThan(java.lang.Integer value) {
          addCriterion("publish", value, ConditionMode.LESS_THEN, "publish", "java.lang.Integer", "Float");
          return this;
      }

      public CmsCategorySQL andPublishLessThanOrEqualTo(java.lang.Integer value) {
          addCriterion("publish", value, ConditionMode.LESS_EQUAL, "publish", "java.lang.Integer", "Float");
          return this;
      }

      public CmsCategorySQL andPublishBetween(java.lang.Integer value1, java.lang.Integer value2) {
    	  addCriterion("publish", value1, value2, ConditionMode.BETWEEN, "publish", "java.lang.Integer", "Float");
    	  return this;
      }

      public CmsCategorySQL andPublishNotBetween(java.lang.Integer value1, java.lang.Integer value2) {
          addCriterion("publish", value1, value2, ConditionMode.NOT_BETWEEN, "publish", "java.lang.Integer", "Float");
          return this;
      }
        
      public CmsCategorySQL andPublishIn(List<java.lang.Integer> values) {
          addCriterion("publish", values, ConditionMode.IN, "publish", "java.lang.Integer", "Float");
          return this;
      }

      public CmsCategorySQL andPublishNotIn(List<java.lang.Integer> values) {
          addCriterion("publish", values, ConditionMode.NOT_IN, "publish", "java.lang.Integer", "Float");
          return this;
      }
	public CmsCategorySQL andTypeIsNull() {
		isnull("type");
		return this;
	}
	
	public CmsCategorySQL andTypeIsNotNull() {
		notNull("type");
		return this;
	}
	
	public CmsCategorySQL andTypeIsEmpty() {
		empty("type");
		return this;
	}

	public CmsCategorySQL andTypeIsNotEmpty() {
		notEmpty("type");
		return this;
	}
      public CmsCategorySQL andTypeEqualTo(java.lang.Integer value) {
          addCriterion("type", value, ConditionMode.EQUAL, "type", "java.lang.Integer", "Float");
          return this;
      }

      public CmsCategorySQL andTypeNotEqualTo(java.lang.Integer value) {
          addCriterion("type", value, ConditionMode.NOT_EQUAL, "type", "java.lang.Integer", "Float");
          return this;
      }

      public CmsCategorySQL andTypeGreaterThan(java.lang.Integer value) {
          addCriterion("type", value, ConditionMode.GREATER_THEN, "type", "java.lang.Integer", "Float");
          return this;
      }

      public CmsCategorySQL andTypeGreaterThanOrEqualTo(java.lang.Integer value) {
          addCriterion("type", value, ConditionMode.GREATER_EQUAL, "type", "java.lang.Integer", "Float");
          return this;
      }

      public CmsCategorySQL andTypeLessThan(java.lang.Integer value) {
          addCriterion("type", value, ConditionMode.LESS_THEN, "type", "java.lang.Integer", "Float");
          return this;
      }

      public CmsCategorySQL andTypeLessThanOrEqualTo(java.lang.Integer value) {
          addCriterion("type", value, ConditionMode.LESS_EQUAL, "type", "java.lang.Integer", "Float");
          return this;
      }

      public CmsCategorySQL andTypeBetween(java.lang.Integer value1, java.lang.Integer value2) {
    	  addCriterion("type", value1, value2, ConditionMode.BETWEEN, "type", "java.lang.Integer", "Float");
    	  return this;
      }

      public CmsCategorySQL andTypeNotBetween(java.lang.Integer value1, java.lang.Integer value2) {
          addCriterion("type", value1, value2, ConditionMode.NOT_BETWEEN, "type", "java.lang.Integer", "Float");
          return this;
      }
        
      public CmsCategorySQL andTypeIn(List<java.lang.Integer> values) {
          addCriterion("type", values, ConditionMode.IN, "type", "java.lang.Integer", "Float");
          return this;
      }

      public CmsCategorySQL andTypeNotIn(List<java.lang.Integer> values) {
          addCriterion("type", values, ConditionMode.NOT_IN, "type", "java.lang.Integer", "Float");
          return this;
      }
	public CmsCategorySQL andAliasIsNull() {
		isnull("alias");
		return this;
	}
	
	public CmsCategorySQL andAliasIsNotNull() {
		notNull("alias");
		return this;
	}
	
	public CmsCategorySQL andAliasIsEmpty() {
		empty("alias");
		return this;
	}

	public CmsCategorySQL andAliasIsNotEmpty() {
		notEmpty("alias");
		return this;
	}
       public CmsCategorySQL andAliasLike(java.lang.String value) {
    	   addCriterion("alias", value, ConditionMode.FUZZY, "alias", "java.lang.String", "Float");
    	   return this;
      }

      public CmsCategorySQL andAliasNotLike(java.lang.String value) {
          addCriterion("alias", value, ConditionMode.NOT_FUZZY, "alias", "java.lang.String", "Float");
          return this;
      }
      public CmsCategorySQL andAliasEqualTo(java.lang.String value) {
          addCriterion("alias", value, ConditionMode.EQUAL, "alias", "java.lang.String", "String");
          return this;
      }

      public CmsCategorySQL andAliasNotEqualTo(java.lang.String value) {
          addCriterion("alias", value, ConditionMode.NOT_EQUAL, "alias", "java.lang.String", "String");
          return this;
      }

      public CmsCategorySQL andAliasGreaterThan(java.lang.String value) {
          addCriterion("alias", value, ConditionMode.GREATER_THEN, "alias", "java.lang.String", "String");
          return this;
      }

      public CmsCategorySQL andAliasGreaterThanOrEqualTo(java.lang.String value) {
          addCriterion("alias", value, ConditionMode.GREATER_EQUAL, "alias", "java.lang.String", "String");
          return this;
      }

      public CmsCategorySQL andAliasLessThan(java.lang.String value) {
          addCriterion("alias", value, ConditionMode.LESS_THEN, "alias", "java.lang.String", "String");
          return this;
      }

      public CmsCategorySQL andAliasLessThanOrEqualTo(java.lang.String value) {
          addCriterion("alias", value, ConditionMode.LESS_EQUAL, "alias", "java.lang.String", "String");
          return this;
      }

      public CmsCategorySQL andAliasBetween(java.lang.String value1, java.lang.String value2) {
    	  addCriterion("alias", value1, value2, ConditionMode.BETWEEN, "alias", "java.lang.String", "String");
    	  return this;
      }

      public CmsCategorySQL andAliasNotBetween(java.lang.String value1, java.lang.String value2) {
          addCriterion("alias", value1, value2, ConditionMode.NOT_BETWEEN, "alias", "java.lang.String", "String");
          return this;
      }
        
      public CmsCategorySQL andAliasIn(List<java.lang.String> values) {
          addCriterion("alias", values, ConditionMode.IN, "alias", "java.lang.String", "String");
          return this;
      }

      public CmsCategorySQL andAliasNotIn(List<java.lang.String> values) {
          addCriterion("alias", values, ConditionMode.NOT_IN, "alias", "java.lang.String", "String");
          return this;
      }
	public CmsCategorySQL andFlagIsNull() {
		isnull("flag");
		return this;
	}
	
	public CmsCategorySQL andFlagIsNotNull() {
		notNull("flag");
		return this;
	}
	
	public CmsCategorySQL andFlagIsEmpty() {
		empty("flag");
		return this;
	}

	public CmsCategorySQL andFlagIsNotEmpty() {
		notEmpty("flag");
		return this;
	}
       public CmsCategorySQL andFlagLike(java.lang.String value) {
    	   addCriterion("flag", value, ConditionMode.FUZZY, "flag", "java.lang.String", "String");
    	   return this;
      }

      public CmsCategorySQL andFlagNotLike(java.lang.String value) {
          addCriterion("flag", value, ConditionMode.NOT_FUZZY, "flag", "java.lang.String", "String");
          return this;
      }
      public CmsCategorySQL andFlagEqualTo(java.lang.String value) {
          addCriterion("flag", value, ConditionMode.EQUAL, "flag", "java.lang.String", "String");
          return this;
      }

      public CmsCategorySQL andFlagNotEqualTo(java.lang.String value) {
          addCriterion("flag", value, ConditionMode.NOT_EQUAL, "flag", "java.lang.String", "String");
          return this;
      }

      public CmsCategorySQL andFlagGreaterThan(java.lang.String value) {
          addCriterion("flag", value, ConditionMode.GREATER_THEN, "flag", "java.lang.String", "String");
          return this;
      }

      public CmsCategorySQL andFlagGreaterThanOrEqualTo(java.lang.String value) {
          addCriterion("flag", value, ConditionMode.GREATER_EQUAL, "flag", "java.lang.String", "String");
          return this;
      }

      public CmsCategorySQL andFlagLessThan(java.lang.String value) {
          addCriterion("flag", value, ConditionMode.LESS_THEN, "flag", "java.lang.String", "String");
          return this;
      }

      public CmsCategorySQL andFlagLessThanOrEqualTo(java.lang.String value) {
          addCriterion("flag", value, ConditionMode.LESS_EQUAL, "flag", "java.lang.String", "String");
          return this;
      }

      public CmsCategorySQL andFlagBetween(java.lang.String value1, java.lang.String value2) {
    	  addCriterion("flag", value1, value2, ConditionMode.BETWEEN, "flag", "java.lang.String", "String");
    	  return this;
      }

      public CmsCategorySQL andFlagNotBetween(java.lang.String value1, java.lang.String value2) {
          addCriterion("flag", value1, value2, ConditionMode.NOT_BETWEEN, "flag", "java.lang.String", "String");
          return this;
      }
        
      public CmsCategorySQL andFlagIn(List<java.lang.String> values) {
          addCriterion("flag", values, ConditionMode.IN, "flag", "java.lang.String", "String");
          return this;
      }

      public CmsCategorySQL andFlagNotIn(List<java.lang.String> values) {
          addCriterion("flag", values, ConditionMode.NOT_IN, "flag", "java.lang.String", "String");
          return this;
      }
	public CmsCategorySQL andStyleIsNull() {
		isnull("style");
		return this;
	}
	
	public CmsCategorySQL andStyleIsNotNull() {
		notNull("style");
		return this;
	}
	
	public CmsCategorySQL andStyleIsEmpty() {
		empty("style");
		return this;
	}

	public CmsCategorySQL andStyleIsNotEmpty() {
		notEmpty("style");
		return this;
	}
       public CmsCategorySQL andStyleLike(java.lang.String value) {
    	   addCriterion("style", value, ConditionMode.FUZZY, "style", "java.lang.String", "String");
    	   return this;
      }

      public CmsCategorySQL andStyleNotLike(java.lang.String value) {
          addCriterion("style", value, ConditionMode.NOT_FUZZY, "style", "java.lang.String", "String");
          return this;
      }
      public CmsCategorySQL andStyleEqualTo(java.lang.String value) {
          addCriterion("style", value, ConditionMode.EQUAL, "style", "java.lang.String", "String");
          return this;
      }

      public CmsCategorySQL andStyleNotEqualTo(java.lang.String value) {
          addCriterion("style", value, ConditionMode.NOT_EQUAL, "style", "java.lang.String", "String");
          return this;
      }

      public CmsCategorySQL andStyleGreaterThan(java.lang.String value) {
          addCriterion("style", value, ConditionMode.GREATER_THEN, "style", "java.lang.String", "String");
          return this;
      }

      public CmsCategorySQL andStyleGreaterThanOrEqualTo(java.lang.String value) {
          addCriterion("style", value, ConditionMode.GREATER_EQUAL, "style", "java.lang.String", "String");
          return this;
      }

      public CmsCategorySQL andStyleLessThan(java.lang.String value) {
          addCriterion("style", value, ConditionMode.LESS_THEN, "style", "java.lang.String", "String");
          return this;
      }

      public CmsCategorySQL andStyleLessThanOrEqualTo(java.lang.String value) {
          addCriterion("style", value, ConditionMode.LESS_EQUAL, "style", "java.lang.String", "String");
          return this;
      }

      public CmsCategorySQL andStyleBetween(java.lang.String value1, java.lang.String value2) {
    	  addCriterion("style", value1, value2, ConditionMode.BETWEEN, "style", "java.lang.String", "String");
    	  return this;
      }

      public CmsCategorySQL andStyleNotBetween(java.lang.String value1, java.lang.String value2) {
          addCriterion("style", value1, value2, ConditionMode.NOT_BETWEEN, "style", "java.lang.String", "String");
          return this;
      }
        
      public CmsCategorySQL andStyleIn(List<java.lang.String> values) {
          addCriterion("style", values, ConditionMode.IN, "style", "java.lang.String", "String");
          return this;
      }

      public CmsCategorySQL andStyleNotIn(List<java.lang.String> values) {
          addCriterion("style", values, ConditionMode.NOT_IN, "style", "java.lang.String", "String");
          return this;
      }
	public CmsCategorySQL andSystemIdIsNull() {
		isnull("system_id");
		return this;
	}
	
	public CmsCategorySQL andSystemIdIsNotNull() {
		notNull("system_id");
		return this;
	}
	
	public CmsCategorySQL andSystemIdIsEmpty() {
		empty("system_id");
		return this;
	}

	public CmsCategorySQL andSystemIdIsNotEmpty() {
		notEmpty("system_id");
		return this;
	}
      public CmsCategorySQL andSystemIdEqualTo(java.lang.Integer value) {
          addCriterion("system_id", value, ConditionMode.EQUAL, "systemId", "java.lang.Integer", "Float");
          return this;
      }

      public CmsCategorySQL andSystemIdNotEqualTo(java.lang.Integer value) {
          addCriterion("system_id", value, ConditionMode.NOT_EQUAL, "systemId", "java.lang.Integer", "Float");
          return this;
      }

      public CmsCategorySQL andSystemIdGreaterThan(java.lang.Integer value) {
          addCriterion("system_id", value, ConditionMode.GREATER_THEN, "systemId", "java.lang.Integer", "Float");
          return this;
      }

      public CmsCategorySQL andSystemIdGreaterThanOrEqualTo(java.lang.Integer value) {
          addCriterion("system_id", value, ConditionMode.GREATER_EQUAL, "systemId", "java.lang.Integer", "Float");
          return this;
      }

      public CmsCategorySQL andSystemIdLessThan(java.lang.Integer value) {
          addCriterion("system_id", value, ConditionMode.LESS_THEN, "systemId", "java.lang.Integer", "Float");
          return this;
      }

      public CmsCategorySQL andSystemIdLessThanOrEqualTo(java.lang.Integer value) {
          addCriterion("system_id", value, ConditionMode.LESS_EQUAL, "systemId", "java.lang.Integer", "Float");
          return this;
      }

      public CmsCategorySQL andSystemIdBetween(java.lang.Integer value1, java.lang.Integer value2) {
    	  addCriterion("system_id", value1, value2, ConditionMode.BETWEEN, "systemId", "java.lang.Integer", "Float");
    	  return this;
      }

      public CmsCategorySQL andSystemIdNotBetween(java.lang.Integer value1, java.lang.Integer value2) {
          addCriterion("system_id", value1, value2, ConditionMode.NOT_BETWEEN, "systemId", "java.lang.Integer", "Float");
          return this;
      }
        
      public CmsCategorySQL andSystemIdIn(List<java.lang.Integer> values) {
          addCriterion("system_id", values, ConditionMode.IN, "systemId", "java.lang.Integer", "Float");
          return this;
      }

      public CmsCategorySQL andSystemIdNotIn(List<java.lang.Integer> values) {
          addCriterion("system_id", values, ConditionMode.NOT_IN, "systemId", "java.lang.Integer", "Float");
          return this;
      }
	public CmsCategorySQL andCtimeIsNull() {
		isnull("ctime");
		return this;
	}
	
	public CmsCategorySQL andCtimeIsNotNull() {
		notNull("ctime");
		return this;
	}
	
	public CmsCategorySQL andCtimeIsEmpty() {
		empty("ctime");
		return this;
	}

	public CmsCategorySQL andCtimeIsNotEmpty() {
		notEmpty("ctime");
		return this;
	}
      public CmsCategorySQL andCtimeEqualTo(java.util.Date value) {
          addCriterion("ctime", value, ConditionMode.EQUAL, "ctime", "java.util.Date", "String");
          return this;
      }

      public CmsCategorySQL andCtimeNotEqualTo(java.util.Date value) {
          addCriterion("ctime", value, ConditionMode.NOT_EQUAL, "ctime", "java.util.Date", "String");
          return this;
      }

      public CmsCategorySQL andCtimeGreaterThan(java.util.Date value) {
          addCriterion("ctime", value, ConditionMode.GREATER_THEN, "ctime", "java.util.Date", "String");
          return this;
      }

      public CmsCategorySQL andCtimeGreaterThanOrEqualTo(java.util.Date value) {
          addCriterion("ctime", value, ConditionMode.GREATER_EQUAL, "ctime", "java.util.Date", "String");
          return this;
      }

      public CmsCategorySQL andCtimeLessThan(java.util.Date value) {
          addCriterion("ctime", value, ConditionMode.LESS_THEN, "ctime", "java.util.Date", "String");
          return this;
      }

      public CmsCategorySQL andCtimeLessThanOrEqualTo(java.util.Date value) {
          addCriterion("ctime", value, ConditionMode.LESS_EQUAL, "ctime", "java.util.Date", "String");
          return this;
      }

      public CmsCategorySQL andCtimeBetween(java.util.Date value1, java.util.Date value2) {
    	  addCriterion("ctime", value1, value2, ConditionMode.BETWEEN, "ctime", "java.util.Date", "String");
    	  return this;
      }

      public CmsCategorySQL andCtimeNotBetween(java.util.Date value1, java.util.Date value2) {
          addCriterion("ctime", value1, value2, ConditionMode.NOT_BETWEEN, "ctime", "java.util.Date", "String");
          return this;
      }
        
      public CmsCategorySQL andCtimeIn(List<java.util.Date> values) {
          addCriterion("ctime", values, ConditionMode.IN, "ctime", "java.util.Date", "String");
          return this;
      }

      public CmsCategorySQL andCtimeNotIn(List<java.util.Date> values) {
          addCriterion("ctime", values, ConditionMode.NOT_IN, "ctime", "java.util.Date", "String");
          return this;
      }
	public CmsCategorySQL andMtimeIsNull() {
		isnull("mtime");
		return this;
	}
	
	public CmsCategorySQL andMtimeIsNotNull() {
		notNull("mtime");
		return this;
	}
	
	public CmsCategorySQL andMtimeIsEmpty() {
		empty("mtime");
		return this;
	}

	public CmsCategorySQL andMtimeIsNotEmpty() {
		notEmpty("mtime");
		return this;
	}
      public CmsCategorySQL andMtimeEqualTo(java.util.Date value) {
          addCriterion("mtime", value, ConditionMode.EQUAL, "mtime", "java.util.Date", "String");
          return this;
      }

      public CmsCategorySQL andMtimeNotEqualTo(java.util.Date value) {
          addCriterion("mtime", value, ConditionMode.NOT_EQUAL, "mtime", "java.util.Date", "String");
          return this;
      }

      public CmsCategorySQL andMtimeGreaterThan(java.util.Date value) {
          addCriterion("mtime", value, ConditionMode.GREATER_THEN, "mtime", "java.util.Date", "String");
          return this;
      }

      public CmsCategorySQL andMtimeGreaterThanOrEqualTo(java.util.Date value) {
          addCriterion("mtime", value, ConditionMode.GREATER_EQUAL, "mtime", "java.util.Date", "String");
          return this;
      }

      public CmsCategorySQL andMtimeLessThan(java.util.Date value) {
          addCriterion("mtime", value, ConditionMode.LESS_THEN, "mtime", "java.util.Date", "String");
          return this;
      }

      public CmsCategorySQL andMtimeLessThanOrEqualTo(java.util.Date value) {
          addCriterion("mtime", value, ConditionMode.LESS_EQUAL, "mtime", "java.util.Date", "String");
          return this;
      }

      public CmsCategorySQL andMtimeBetween(java.util.Date value1, java.util.Date value2) {
    	  addCriterion("mtime", value1, value2, ConditionMode.BETWEEN, "mtime", "java.util.Date", "String");
    	  return this;
      }

      public CmsCategorySQL andMtimeNotBetween(java.util.Date value1, java.util.Date value2) {
          addCriterion("mtime", value1, value2, ConditionMode.NOT_BETWEEN, "mtime", "java.util.Date", "String");
          return this;
      }
        
      public CmsCategorySQL andMtimeIn(List<java.util.Date> values) {
          addCriterion("mtime", values, ConditionMode.IN, "mtime", "java.util.Date", "String");
          return this;
      }

      public CmsCategorySQL andMtimeNotIn(List<java.util.Date> values) {
          addCriterion("mtime", values, ConditionMode.NOT_IN, "mtime", "java.util.Date", "String");
          return this;
      }
	public CmsCategorySQL andOrdersIsNull() {
		isnull("orders");
		return this;
	}
	
	public CmsCategorySQL andOrdersIsNotNull() {
		notNull("orders");
		return this;
	}
	
	public CmsCategorySQL andOrdersIsEmpty() {
		empty("orders");
		return this;
	}

	public CmsCategorySQL andOrdersIsNotEmpty() {
		notEmpty("orders");
		return this;
	}
      public CmsCategorySQL andOrdersEqualTo(java.lang.Long value) {
          addCriterion("orders", value, ConditionMode.EQUAL, "orders", "java.lang.Long", "Float");
          return this;
      }

      public CmsCategorySQL andOrdersNotEqualTo(java.lang.Long value) {
          addCriterion("orders", value, ConditionMode.NOT_EQUAL, "orders", "java.lang.Long", "Float");
          return this;
      }

      public CmsCategorySQL andOrdersGreaterThan(java.lang.Long value) {
          addCriterion("orders", value, ConditionMode.GREATER_THEN, "orders", "java.lang.Long", "Float");
          return this;
      }

      public CmsCategorySQL andOrdersGreaterThanOrEqualTo(java.lang.Long value) {
          addCriterion("orders", value, ConditionMode.GREATER_EQUAL, "orders", "java.lang.Long", "Float");
          return this;
      }

      public CmsCategorySQL andOrdersLessThan(java.lang.Long value) {
          addCriterion("orders", value, ConditionMode.LESS_THEN, "orders", "java.lang.Long", "Float");
          return this;
      }

      public CmsCategorySQL andOrdersLessThanOrEqualTo(java.lang.Long value) {
          addCriterion("orders", value, ConditionMode.LESS_EQUAL, "orders", "java.lang.Long", "Float");
          return this;
      }

      public CmsCategorySQL andOrdersBetween(java.lang.Long value1, java.lang.Long value2) {
    	  addCriterion("orders", value1, value2, ConditionMode.BETWEEN, "orders", "java.lang.Long", "Float");
    	  return this;
      }

      public CmsCategorySQL andOrdersNotBetween(java.lang.Long value1, java.lang.Long value2) {
          addCriterion("orders", value1, value2, ConditionMode.NOT_BETWEEN, "orders", "java.lang.Long", "Float");
          return this;
      }
        
      public CmsCategorySQL andOrdersIn(List<java.lang.Long> values) {
          addCriterion("orders", values, ConditionMode.IN, "orders", "java.lang.Long", "Float");
          return this;
      }

      public CmsCategorySQL andOrdersNotIn(List<java.lang.Long> values) {
          addCriterion("orders", values, ConditionMode.NOT_IN, "orders", "java.lang.Long", "Float");
          return this;
      }
}