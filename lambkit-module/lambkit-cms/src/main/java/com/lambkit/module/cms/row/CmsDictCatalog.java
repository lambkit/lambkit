/**
 * Copyright (c) 2015-2017, Henry Yang 杨勇 (gismail@foxmail.com).
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.lambkit.module.cms.row;

import com.lambkit.db.RowModel;

/**
 * @author yangyong 
 * @website: www.lambkit.com
 * @email: gismail@foxmail.com
 * @date ${date}
 * @version 1.0
 * @since 1.0
 */

public class CmsDictCatalog extends RowModel<CmsDictCatalog> {
	public CmsDictCatalog() {
		setTableName("cms_dict_catalog");
		setPrimaryKey("dict_catalog_id");
	}
	public java.lang.Long getDictCatalogId() {
		return this.get("dict_catalog_id");
	}
	public void setDictCatalogId(java.lang.Long dictCatalogId) {
		this.set("dict_catalog_id", dictCatalogId);
	}
	public java.lang.String getDictName() {
		return this.get("dict_name");
	}
	public void setDictName(java.lang.String dictName) {
		this.set("dict_name", dictName);
	}
	public java.lang.String getDictKey() {
		return this.get("dict_key");
	}
	public void setDictKey(java.lang.String dictKey) {
		this.set("dict_key", dictKey);
	}
	public java.lang.Integer getStatus() {
		return this.get("status");
	}
	public void setStatus(java.lang.Integer status) {
		this.set("status", status);
	}
	public java.lang.String getRemark() {
		return this.get("remark");
	}
	public void setRemark(java.lang.String remark) {
		this.set("remark", remark);
	}
	public java.util.Date getCreated() {
		return this.get("created");
	}
	public void setCreated(java.util.Date created) {
		this.set("created", created);
	}
}
