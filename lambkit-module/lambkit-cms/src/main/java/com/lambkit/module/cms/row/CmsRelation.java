/**
 * Copyright (c) 2015-2017, Henry Yang 杨勇 (gismail@foxmail.com).
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.lambkit.module.cms.row;

import com.lambkit.db.RowModel;

/**
 * @author yangyong 
 * @website: www.lambkit.com
 * @email: gismail@foxmail.com
 * @date ${date}
 * @version 1.0
 * @since 1.0
 */

public class CmsRelation extends RowModel<CmsRelation> {
	public CmsRelation() {
		setTableName("cms_relation");
		setPrimaryKey("id");
	}
	public java.lang.Long getId() {
		return this.get("id");
	}
	public void setId(java.lang.Long id) {
		this.set("id", id);
	}
	public java.lang.Long getMainId() {
		return this.get("main_id");
	}
	public void setMainId(java.lang.Long mainId) {
		this.set("main_id", mainId);
	}
	public java.lang.String getMainType() {
		return this.get("main_type");
	}
	public void setMainType(java.lang.String mainType) {
		this.set("main_type", mainType);
	}
	public java.lang.Long getRelationId() {
		return this.get("relation_id");
	}
	public void setRelationId(java.lang.Long relationId) {
		this.set("relation_id", relationId);
	}
	public java.lang.String getRelationType() {
		return this.get("relation_type");
	}
	public void setRelationType(java.lang.String relationType) {
		this.set("relation_type", relationType);
	}
	public java.util.Date getCreateTime() {
		return this.get("create_time");
	}
	public void setCreateTime(java.util.Date createTime) {
		this.set("create_time", createTime);
	}
	public java.lang.String getAssociatedType() {
		return this.get("associated_type");
	}
	public void setAssociatedType(java.lang.String associatedType) {
		this.set("associated_type", associatedType);
	}
}
