/**
 * Copyright (c) 2015-2017, Henry Yang 杨勇 (gismail@foxmail.com).
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.lambkit.module.cms.row;

import com.lambkit.db.RowModel;

/**
 * @author yangyong 
 * @website: www.lambkit.com
 * @email: gismail@foxmail.com
 * @date ${date}
 * @version 1.0
 * @since 1.0
 */

public class CmsNotice extends RowModel<CmsNotice> {
	public CmsNotice() {
		setTableName("cms_notice");
		setPrimaryKey("notice_id");
	}
	public java.lang.Long getNoticeId() {
		return this.get("notice_id");
	}
	public void setNoticeId(java.lang.Long noticeId) {
		this.set("notice_id", noticeId);
	}
	public java.lang.String getTitle() {
		return this.get("title");
	}
	public void setTitle(java.lang.String title) {
		this.set("title", title);
	}
	public java.lang.String getContent() {
		return this.get("content");
	}
	public void setContent(java.lang.String content) {
		this.set("content", content);
	}
	public java.util.Date getCreated() {
		return this.get("created");
	}
	public void setCreated(java.util.Date created) {
		this.set("created", created);
	}
	public java.lang.Integer getUserid() {
		return this.get("userid");
	}
	public void setUserid(java.lang.Integer userid) {
		this.set("userid", userid);
	}
	public java.lang.Integer getNoticeType() {
		return this.get("notice_type");
	}
	public void setNoticeType(java.lang.Integer noticeType) {
		this.set("notice_type", noticeType);
	}
	public java.lang.Integer getStatus() {
		return this.get("status");
	}
	public void setStatus(java.lang.Integer status) {
		this.set("status", status);
	}
}
