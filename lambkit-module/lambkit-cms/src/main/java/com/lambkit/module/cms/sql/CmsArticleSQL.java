/**
 * Copyright (c) 2015-2017, Henry Yang 杨勇 (gismail@foxmail.com).
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.lambkit.module.cms.sql;

import com.lambkit.db.sql.ConditionMode;
import com.lambkit.db.sql.Column;
import com.lambkit.db.sql.Columns;
import com.lambkit.db.sql.Example;

import java.io.Serializable;
import java.util.List;

/**
 * @author yangyong 
 * @website: www.lambkit.com
 * @email: gismail@foxmail.com
 * @date ${date}
 * @version 1.0
 * @since 1.0
 */

public class CmsArticleSQL extends Columns implements Serializable {
	private static final long serialVersionUID = 1L;
	
	public static CmsArticleSQL of() {
		return new CmsArticleSQL();
	}
	
	public static CmsArticleSQL by(Column column) {
		CmsArticleSQL that = new CmsArticleSQL();
		that.add(column);
        return that;
    }

    public static CmsArticleSQL by(String name, Object value) {
        return (CmsArticleSQL) create().eq(name, value);
    }
    
    public Example example() {
    	return Example.create("cms_article", this);
    }
    
    /**
     * equals
     *
     * @param name
     * @param value
     * @return
     */
    public CmsArticleSQL eq(String name, Object value) {
    	super.eq(name, value);
        return this;
    }

    /**
     * not equals !=
     *
     * @param name
     * @param value
     * @return
     */
    public CmsArticleSQL ne(String name, Object value) {
    	super.ne(name, value);
        return this;
    }


    /**
     * like
     *
     * @param name
     * @param value
     * @return
     */

    public CmsArticleSQL like(String name, Object value) {
    	super.like(name, value);
        return this;
    }
    
    public CmsArticleSQL notLike(String name, Object value) {
    	super.notLike(name, value);
        return this;
    }

    /**
     * 大于 great than
     *
     * @param name
     * @param value
     * @return
     */
    public CmsArticleSQL gt(String name, Object value) {
    	super.gt(name, value);
        return this;
    }

    /**
     * 大于等于 great or equal
     *
     * @param name
     * @param value
     * @return
     */
    public CmsArticleSQL ge(String name, Object value) {
    	super.ge(name, value);
        return this;
    }

    /**
     * 小于 less than
     *
     * @param name
     * @param value
     * @return
     */
    public CmsArticleSQL lt(String name, Object value) {
    	super.lt(name, value);
        return this;
    }

    /**
     * 小于等于 less or equal
     *
     * @param name
     * @param value
     * @return
     */
    public CmsArticleSQL le(String name, Object value) {
    	super.le(name, value);
        return this;
    }
    
    public CmsArticleSQL isnull(String name) {
    	super.isnull(name);
        return this;
    } 

    public CmsArticleSQL notNull(String name) {
    	super.notNull(name);
        return this;
    } 
    
    public CmsArticleSQL empty(String name) {
    	super.empty(name);
        return this;
    } 
    
    public CmsArticleSQL notEmpty(String name) {
    	super.notEmpty(name);
        return this;
    } 
    
    public CmsArticleSQL add(Column column) {
    	super.add(column);
    	return this;
    }
    
    /**************************/
	
	public void addCriterion(String name, Object value, ConditionMode logic, String property, String typeHandler, String valueType) {
		 if (value == null) {
			 throw new RuntimeException("Value for " + property + " cannot be null");
		 }
		 add(Column.create(name, value, logic, typeHandler, valueType));
	}
   
	public void addCriterion(String name, Object value1, Object value2, ConditionMode logic, String property, String typeHandler, String valueType) {
		 if (value1 == null || value2 == null) {
			 throw new RuntimeException("Between values for " + property + " cannot be null");
		 }
		 add(Column.create(name, value1, value2, logic, typeHandler, valueType));
	}
		 
	public CmsArticleSQL andArticleIdIsNull() {
		isnull("article_id");
		return this;
	}
	
	public CmsArticleSQL andArticleIdIsNotNull() {
		notNull("article_id");
		return this;
	}
	
	public CmsArticleSQL andArticleIdIsEmpty() {
		empty("article_id");
		return this;
	}

	public CmsArticleSQL andArticleIdIsNotEmpty() {
		notEmpty("article_id");
		return this;
	}
      public CmsArticleSQL andArticleIdEqualTo(java.lang.Long value) {
          addCriterion("article_id", value, ConditionMode.EQUAL, "articleId", "java.lang.Long", "Float");
          return this;
      }

      public CmsArticleSQL andArticleIdNotEqualTo(java.lang.Long value) {
          addCriterion("article_id", value, ConditionMode.NOT_EQUAL, "articleId", "java.lang.Long", "Float");
          return this;
      }

      public CmsArticleSQL andArticleIdGreaterThan(java.lang.Long value) {
          addCriterion("article_id", value, ConditionMode.GREATER_THEN, "articleId", "java.lang.Long", "Float");
          return this;
      }

      public CmsArticleSQL andArticleIdGreaterThanOrEqualTo(java.lang.Long value) {
          addCriterion("article_id", value, ConditionMode.GREATER_EQUAL, "articleId", "java.lang.Long", "Float");
          return this;
      }

      public CmsArticleSQL andArticleIdLessThan(java.lang.Long value) {
          addCriterion("article_id", value, ConditionMode.LESS_THEN, "articleId", "java.lang.Long", "Float");
          return this;
      }

      public CmsArticleSQL andArticleIdLessThanOrEqualTo(java.lang.Long value) {
          addCriterion("article_id", value, ConditionMode.LESS_EQUAL, "articleId", "java.lang.Long", "Float");
          return this;
      }

      public CmsArticleSQL andArticleIdBetween(java.lang.Long value1, java.lang.Long value2) {
    	  addCriterion("article_id", value1, value2, ConditionMode.BETWEEN, "articleId", "java.lang.Long", "Float");
    	  return this;
      }

      public CmsArticleSQL andArticleIdNotBetween(java.lang.Long value1, java.lang.Long value2) {
          addCriterion("article_id", value1, value2, ConditionMode.NOT_BETWEEN, "articleId", "java.lang.Long", "Float");
          return this;
      }
        
      public CmsArticleSQL andArticleIdIn(List<java.lang.Long> values) {
          addCriterion("article_id", values, ConditionMode.IN, "articleId", "java.lang.Long", "Float");
          return this;
      }

      public CmsArticleSQL andArticleIdNotIn(List<java.lang.Long> values) {
          addCriterion("article_id", values, ConditionMode.NOT_IN, "articleId", "java.lang.Long", "Float");
          return this;
      }
	public CmsArticleSQL andTopicIdIsNull() {
		isnull("topic_id");
		return this;
	}
	
	public CmsArticleSQL andTopicIdIsNotNull() {
		notNull("topic_id");
		return this;
	}
	
	public CmsArticleSQL andTopicIdIsEmpty() {
		empty("topic_id");
		return this;
	}

	public CmsArticleSQL andTopicIdIsNotEmpty() {
		notEmpty("topic_id");
		return this;
	}
      public CmsArticleSQL andTopicIdEqualTo(java.lang.Integer value) {
          addCriterion("topic_id", value, ConditionMode.EQUAL, "topicId", "java.lang.Integer", "Float");
          return this;
      }

      public CmsArticleSQL andTopicIdNotEqualTo(java.lang.Integer value) {
          addCriterion("topic_id", value, ConditionMode.NOT_EQUAL, "topicId", "java.lang.Integer", "Float");
          return this;
      }

      public CmsArticleSQL andTopicIdGreaterThan(java.lang.Integer value) {
          addCriterion("topic_id", value, ConditionMode.GREATER_THEN, "topicId", "java.lang.Integer", "Float");
          return this;
      }

      public CmsArticleSQL andTopicIdGreaterThanOrEqualTo(java.lang.Integer value) {
          addCriterion("topic_id", value, ConditionMode.GREATER_EQUAL, "topicId", "java.lang.Integer", "Float");
          return this;
      }

      public CmsArticleSQL andTopicIdLessThan(java.lang.Integer value) {
          addCriterion("topic_id", value, ConditionMode.LESS_THEN, "topicId", "java.lang.Integer", "Float");
          return this;
      }

      public CmsArticleSQL andTopicIdLessThanOrEqualTo(java.lang.Integer value) {
          addCriterion("topic_id", value, ConditionMode.LESS_EQUAL, "topicId", "java.lang.Integer", "Float");
          return this;
      }

      public CmsArticleSQL andTopicIdBetween(java.lang.Integer value1, java.lang.Integer value2) {
    	  addCriterion("topic_id", value1, value2, ConditionMode.BETWEEN, "topicId", "java.lang.Integer", "Float");
    	  return this;
      }

      public CmsArticleSQL andTopicIdNotBetween(java.lang.Integer value1, java.lang.Integer value2) {
          addCriterion("topic_id", value1, value2, ConditionMode.NOT_BETWEEN, "topicId", "java.lang.Integer", "Float");
          return this;
      }
        
      public CmsArticleSQL andTopicIdIn(List<java.lang.Integer> values) {
          addCriterion("topic_id", values, ConditionMode.IN, "topicId", "java.lang.Integer", "Float");
          return this;
      }

      public CmsArticleSQL andTopicIdNotIn(List<java.lang.Integer> values) {
          addCriterion("topic_id", values, ConditionMode.NOT_IN, "topicId", "java.lang.Integer", "Float");
          return this;
      }
	public CmsArticleSQL andTitleIsNull() {
		isnull("title");
		return this;
	}
	
	public CmsArticleSQL andTitleIsNotNull() {
		notNull("title");
		return this;
	}
	
	public CmsArticleSQL andTitleIsEmpty() {
		empty("title");
		return this;
	}

	public CmsArticleSQL andTitleIsNotEmpty() {
		notEmpty("title");
		return this;
	}
       public CmsArticleSQL andTitleLike(java.lang.String value) {
    	   addCriterion("title", value, ConditionMode.FUZZY, "title", "java.lang.String", "Float");
    	   return this;
      }

      public CmsArticleSQL andTitleNotLike(java.lang.String value) {
          addCriterion("title", value, ConditionMode.NOT_FUZZY, "title", "java.lang.String", "Float");
          return this;
      }
      public CmsArticleSQL andTitleEqualTo(java.lang.String value) {
          addCriterion("title", value, ConditionMode.EQUAL, "title", "java.lang.String", "String");
          return this;
      }

      public CmsArticleSQL andTitleNotEqualTo(java.lang.String value) {
          addCriterion("title", value, ConditionMode.NOT_EQUAL, "title", "java.lang.String", "String");
          return this;
      }

      public CmsArticleSQL andTitleGreaterThan(java.lang.String value) {
          addCriterion("title", value, ConditionMode.GREATER_THEN, "title", "java.lang.String", "String");
          return this;
      }

      public CmsArticleSQL andTitleGreaterThanOrEqualTo(java.lang.String value) {
          addCriterion("title", value, ConditionMode.GREATER_EQUAL, "title", "java.lang.String", "String");
          return this;
      }

      public CmsArticleSQL andTitleLessThan(java.lang.String value) {
          addCriterion("title", value, ConditionMode.LESS_THEN, "title", "java.lang.String", "String");
          return this;
      }

      public CmsArticleSQL andTitleLessThanOrEqualTo(java.lang.String value) {
          addCriterion("title", value, ConditionMode.LESS_EQUAL, "title", "java.lang.String", "String");
          return this;
      }

      public CmsArticleSQL andTitleBetween(java.lang.String value1, java.lang.String value2) {
    	  addCriterion("title", value1, value2, ConditionMode.BETWEEN, "title", "java.lang.String", "String");
    	  return this;
      }

      public CmsArticleSQL andTitleNotBetween(java.lang.String value1, java.lang.String value2) {
          addCriterion("title", value1, value2, ConditionMode.NOT_BETWEEN, "title", "java.lang.String", "String");
          return this;
      }
        
      public CmsArticleSQL andTitleIn(List<java.lang.String> values) {
          addCriterion("title", values, ConditionMode.IN, "title", "java.lang.String", "String");
          return this;
      }

      public CmsArticleSQL andTitleNotIn(List<java.lang.String> values) {
          addCriterion("title", values, ConditionMode.NOT_IN, "title", "java.lang.String", "String");
          return this;
      }
	public CmsArticleSQL andAuthorIsNull() {
		isnull("author");
		return this;
	}
	
	public CmsArticleSQL andAuthorIsNotNull() {
		notNull("author");
		return this;
	}
	
	public CmsArticleSQL andAuthorIsEmpty() {
		empty("author");
		return this;
	}

	public CmsArticleSQL andAuthorIsNotEmpty() {
		notEmpty("author");
		return this;
	}
       public CmsArticleSQL andAuthorLike(java.lang.String value) {
    	   addCriterion("author", value, ConditionMode.FUZZY, "author", "java.lang.String", "String");
    	   return this;
      }

      public CmsArticleSQL andAuthorNotLike(java.lang.String value) {
          addCriterion("author", value, ConditionMode.NOT_FUZZY, "author", "java.lang.String", "String");
          return this;
      }
      public CmsArticleSQL andAuthorEqualTo(java.lang.String value) {
          addCriterion("author", value, ConditionMode.EQUAL, "author", "java.lang.String", "String");
          return this;
      }

      public CmsArticleSQL andAuthorNotEqualTo(java.lang.String value) {
          addCriterion("author", value, ConditionMode.NOT_EQUAL, "author", "java.lang.String", "String");
          return this;
      }

      public CmsArticleSQL andAuthorGreaterThan(java.lang.String value) {
          addCriterion("author", value, ConditionMode.GREATER_THEN, "author", "java.lang.String", "String");
          return this;
      }

      public CmsArticleSQL andAuthorGreaterThanOrEqualTo(java.lang.String value) {
          addCriterion("author", value, ConditionMode.GREATER_EQUAL, "author", "java.lang.String", "String");
          return this;
      }

      public CmsArticleSQL andAuthorLessThan(java.lang.String value) {
          addCriterion("author", value, ConditionMode.LESS_THEN, "author", "java.lang.String", "String");
          return this;
      }

      public CmsArticleSQL andAuthorLessThanOrEqualTo(java.lang.String value) {
          addCriterion("author", value, ConditionMode.LESS_EQUAL, "author", "java.lang.String", "String");
          return this;
      }

      public CmsArticleSQL andAuthorBetween(java.lang.String value1, java.lang.String value2) {
    	  addCriterion("author", value1, value2, ConditionMode.BETWEEN, "author", "java.lang.String", "String");
    	  return this;
      }

      public CmsArticleSQL andAuthorNotBetween(java.lang.String value1, java.lang.String value2) {
          addCriterion("author", value1, value2, ConditionMode.NOT_BETWEEN, "author", "java.lang.String", "String");
          return this;
      }
        
      public CmsArticleSQL andAuthorIn(List<java.lang.String> values) {
          addCriterion("author", values, ConditionMode.IN, "author", "java.lang.String", "String");
          return this;
      }

      public CmsArticleSQL andAuthorNotIn(List<java.lang.String> values) {
          addCriterion("author", values, ConditionMode.NOT_IN, "author", "java.lang.String", "String");
          return this;
      }
	public CmsArticleSQL andFromurlIsNull() {
		isnull("fromurl");
		return this;
	}
	
	public CmsArticleSQL andFromurlIsNotNull() {
		notNull("fromurl");
		return this;
	}
	
	public CmsArticleSQL andFromurlIsEmpty() {
		empty("fromurl");
		return this;
	}

	public CmsArticleSQL andFromurlIsNotEmpty() {
		notEmpty("fromurl");
		return this;
	}
       public CmsArticleSQL andFromurlLike(java.lang.String value) {
    	   addCriterion("fromurl", value, ConditionMode.FUZZY, "fromurl", "java.lang.String", "String");
    	   return this;
      }

      public CmsArticleSQL andFromurlNotLike(java.lang.String value) {
          addCriterion("fromurl", value, ConditionMode.NOT_FUZZY, "fromurl", "java.lang.String", "String");
          return this;
      }
      public CmsArticleSQL andFromurlEqualTo(java.lang.String value) {
          addCriterion("fromurl", value, ConditionMode.EQUAL, "fromurl", "java.lang.String", "String");
          return this;
      }

      public CmsArticleSQL andFromurlNotEqualTo(java.lang.String value) {
          addCriterion("fromurl", value, ConditionMode.NOT_EQUAL, "fromurl", "java.lang.String", "String");
          return this;
      }

      public CmsArticleSQL andFromurlGreaterThan(java.lang.String value) {
          addCriterion("fromurl", value, ConditionMode.GREATER_THEN, "fromurl", "java.lang.String", "String");
          return this;
      }

      public CmsArticleSQL andFromurlGreaterThanOrEqualTo(java.lang.String value) {
          addCriterion("fromurl", value, ConditionMode.GREATER_EQUAL, "fromurl", "java.lang.String", "String");
          return this;
      }

      public CmsArticleSQL andFromurlLessThan(java.lang.String value) {
          addCriterion("fromurl", value, ConditionMode.LESS_THEN, "fromurl", "java.lang.String", "String");
          return this;
      }

      public CmsArticleSQL andFromurlLessThanOrEqualTo(java.lang.String value) {
          addCriterion("fromurl", value, ConditionMode.LESS_EQUAL, "fromurl", "java.lang.String", "String");
          return this;
      }

      public CmsArticleSQL andFromurlBetween(java.lang.String value1, java.lang.String value2) {
    	  addCriterion("fromurl", value1, value2, ConditionMode.BETWEEN, "fromurl", "java.lang.String", "String");
    	  return this;
      }

      public CmsArticleSQL andFromurlNotBetween(java.lang.String value1, java.lang.String value2) {
          addCriterion("fromurl", value1, value2, ConditionMode.NOT_BETWEEN, "fromurl", "java.lang.String", "String");
          return this;
      }
        
      public CmsArticleSQL andFromurlIn(List<java.lang.String> values) {
          addCriterion("fromurl", values, ConditionMode.IN, "fromurl", "java.lang.String", "String");
          return this;
      }

      public CmsArticleSQL andFromurlNotIn(List<java.lang.String> values) {
          addCriterion("fromurl", values, ConditionMode.NOT_IN, "fromurl", "java.lang.String", "String");
          return this;
      }
	public CmsArticleSQL andImageIsNull() {
		isnull("image");
		return this;
	}
	
	public CmsArticleSQL andImageIsNotNull() {
		notNull("image");
		return this;
	}
	
	public CmsArticleSQL andImageIsEmpty() {
		empty("image");
		return this;
	}

	public CmsArticleSQL andImageIsNotEmpty() {
		notEmpty("image");
		return this;
	}
       public CmsArticleSQL andImageLike(java.lang.String value) {
    	   addCriterion("image", value, ConditionMode.FUZZY, "image", "java.lang.String", "String");
    	   return this;
      }

      public CmsArticleSQL andImageNotLike(java.lang.String value) {
          addCriterion("image", value, ConditionMode.NOT_FUZZY, "image", "java.lang.String", "String");
          return this;
      }
      public CmsArticleSQL andImageEqualTo(java.lang.String value) {
          addCriterion("image", value, ConditionMode.EQUAL, "image", "java.lang.String", "String");
          return this;
      }

      public CmsArticleSQL andImageNotEqualTo(java.lang.String value) {
          addCriterion("image", value, ConditionMode.NOT_EQUAL, "image", "java.lang.String", "String");
          return this;
      }

      public CmsArticleSQL andImageGreaterThan(java.lang.String value) {
          addCriterion("image", value, ConditionMode.GREATER_THEN, "image", "java.lang.String", "String");
          return this;
      }

      public CmsArticleSQL andImageGreaterThanOrEqualTo(java.lang.String value) {
          addCriterion("image", value, ConditionMode.GREATER_EQUAL, "image", "java.lang.String", "String");
          return this;
      }

      public CmsArticleSQL andImageLessThan(java.lang.String value) {
          addCriterion("image", value, ConditionMode.LESS_THEN, "image", "java.lang.String", "String");
          return this;
      }

      public CmsArticleSQL andImageLessThanOrEqualTo(java.lang.String value) {
          addCriterion("image", value, ConditionMode.LESS_EQUAL, "image", "java.lang.String", "String");
          return this;
      }

      public CmsArticleSQL andImageBetween(java.lang.String value1, java.lang.String value2) {
    	  addCriterion("image", value1, value2, ConditionMode.BETWEEN, "image", "java.lang.String", "String");
    	  return this;
      }

      public CmsArticleSQL andImageNotBetween(java.lang.String value1, java.lang.String value2) {
          addCriterion("image", value1, value2, ConditionMode.NOT_BETWEEN, "image", "java.lang.String", "String");
          return this;
      }
        
      public CmsArticleSQL andImageIn(List<java.lang.String> values) {
          addCriterion("image", values, ConditionMode.IN, "image", "java.lang.String", "String");
          return this;
      }

      public CmsArticleSQL andImageNotIn(List<java.lang.String> values) {
          addCriterion("image", values, ConditionMode.NOT_IN, "image", "java.lang.String", "String");
          return this;
      }
	public CmsArticleSQL andKeywordsIsNull() {
		isnull("keywords");
		return this;
	}
	
	public CmsArticleSQL andKeywordsIsNotNull() {
		notNull("keywords");
		return this;
	}
	
	public CmsArticleSQL andKeywordsIsEmpty() {
		empty("keywords");
		return this;
	}

	public CmsArticleSQL andKeywordsIsNotEmpty() {
		notEmpty("keywords");
		return this;
	}
       public CmsArticleSQL andKeywordsLike(java.lang.String value) {
    	   addCriterion("keywords", value, ConditionMode.FUZZY, "keywords", "java.lang.String", "String");
    	   return this;
      }

      public CmsArticleSQL andKeywordsNotLike(java.lang.String value) {
          addCriterion("keywords", value, ConditionMode.NOT_FUZZY, "keywords", "java.lang.String", "String");
          return this;
      }
      public CmsArticleSQL andKeywordsEqualTo(java.lang.String value) {
          addCriterion("keywords", value, ConditionMode.EQUAL, "keywords", "java.lang.String", "String");
          return this;
      }

      public CmsArticleSQL andKeywordsNotEqualTo(java.lang.String value) {
          addCriterion("keywords", value, ConditionMode.NOT_EQUAL, "keywords", "java.lang.String", "String");
          return this;
      }

      public CmsArticleSQL andKeywordsGreaterThan(java.lang.String value) {
          addCriterion("keywords", value, ConditionMode.GREATER_THEN, "keywords", "java.lang.String", "String");
          return this;
      }

      public CmsArticleSQL andKeywordsGreaterThanOrEqualTo(java.lang.String value) {
          addCriterion("keywords", value, ConditionMode.GREATER_EQUAL, "keywords", "java.lang.String", "String");
          return this;
      }

      public CmsArticleSQL andKeywordsLessThan(java.lang.String value) {
          addCriterion("keywords", value, ConditionMode.LESS_THEN, "keywords", "java.lang.String", "String");
          return this;
      }

      public CmsArticleSQL andKeywordsLessThanOrEqualTo(java.lang.String value) {
          addCriterion("keywords", value, ConditionMode.LESS_EQUAL, "keywords", "java.lang.String", "String");
          return this;
      }

      public CmsArticleSQL andKeywordsBetween(java.lang.String value1, java.lang.String value2) {
    	  addCriterion("keywords", value1, value2, ConditionMode.BETWEEN, "keywords", "java.lang.String", "String");
    	  return this;
      }

      public CmsArticleSQL andKeywordsNotBetween(java.lang.String value1, java.lang.String value2) {
          addCriterion("keywords", value1, value2, ConditionMode.NOT_BETWEEN, "keywords", "java.lang.String", "String");
          return this;
      }
        
      public CmsArticleSQL andKeywordsIn(List<java.lang.String> values) {
          addCriterion("keywords", values, ConditionMode.IN, "keywords", "java.lang.String", "String");
          return this;
      }

      public CmsArticleSQL andKeywordsNotIn(List<java.lang.String> values) {
          addCriterion("keywords", values, ConditionMode.NOT_IN, "keywords", "java.lang.String", "String");
          return this;
      }
	public CmsArticleSQL andDescriptionIsNull() {
		isnull("description");
		return this;
	}
	
	public CmsArticleSQL andDescriptionIsNotNull() {
		notNull("description");
		return this;
	}
	
	public CmsArticleSQL andDescriptionIsEmpty() {
		empty("description");
		return this;
	}

	public CmsArticleSQL andDescriptionIsNotEmpty() {
		notEmpty("description");
		return this;
	}
       public CmsArticleSQL andDescriptionLike(java.lang.String value) {
    	   addCriterion("description", value, ConditionMode.FUZZY, "description", "java.lang.String", "String");
    	   return this;
      }

      public CmsArticleSQL andDescriptionNotLike(java.lang.String value) {
          addCriterion("description", value, ConditionMode.NOT_FUZZY, "description", "java.lang.String", "String");
          return this;
      }
      public CmsArticleSQL andDescriptionEqualTo(java.lang.String value) {
          addCriterion("description", value, ConditionMode.EQUAL, "description", "java.lang.String", "String");
          return this;
      }

      public CmsArticleSQL andDescriptionNotEqualTo(java.lang.String value) {
          addCriterion("description", value, ConditionMode.NOT_EQUAL, "description", "java.lang.String", "String");
          return this;
      }

      public CmsArticleSQL andDescriptionGreaterThan(java.lang.String value) {
          addCriterion("description", value, ConditionMode.GREATER_THEN, "description", "java.lang.String", "String");
          return this;
      }

      public CmsArticleSQL andDescriptionGreaterThanOrEqualTo(java.lang.String value) {
          addCriterion("description", value, ConditionMode.GREATER_EQUAL, "description", "java.lang.String", "String");
          return this;
      }

      public CmsArticleSQL andDescriptionLessThan(java.lang.String value) {
          addCriterion("description", value, ConditionMode.LESS_THEN, "description", "java.lang.String", "String");
          return this;
      }

      public CmsArticleSQL andDescriptionLessThanOrEqualTo(java.lang.String value) {
          addCriterion("description", value, ConditionMode.LESS_EQUAL, "description", "java.lang.String", "String");
          return this;
      }

      public CmsArticleSQL andDescriptionBetween(java.lang.String value1, java.lang.String value2) {
    	  addCriterion("description", value1, value2, ConditionMode.BETWEEN, "description", "java.lang.String", "String");
    	  return this;
      }

      public CmsArticleSQL andDescriptionNotBetween(java.lang.String value1, java.lang.String value2) {
          addCriterion("description", value1, value2, ConditionMode.NOT_BETWEEN, "description", "java.lang.String", "String");
          return this;
      }
        
      public CmsArticleSQL andDescriptionIn(List<java.lang.String> values) {
          addCriterion("description", values, ConditionMode.IN, "description", "java.lang.String", "String");
          return this;
      }

      public CmsArticleSQL andDescriptionNotIn(List<java.lang.String> values) {
          addCriterion("description", values, ConditionMode.NOT_IN, "description", "java.lang.String", "String");
          return this;
      }
	public CmsArticleSQL andTypeIsNull() {
		isnull("type");
		return this;
	}
	
	public CmsArticleSQL andTypeIsNotNull() {
		notNull("type");
		return this;
	}
	
	public CmsArticleSQL andTypeIsEmpty() {
		empty("type");
		return this;
	}

	public CmsArticleSQL andTypeIsNotEmpty() {
		notEmpty("type");
		return this;
	}
      public CmsArticleSQL andTypeEqualTo(java.lang.Integer value) {
          addCriterion("type", value, ConditionMode.EQUAL, "type", "java.lang.Integer", "Float");
          return this;
      }

      public CmsArticleSQL andTypeNotEqualTo(java.lang.Integer value) {
          addCriterion("type", value, ConditionMode.NOT_EQUAL, "type", "java.lang.Integer", "Float");
          return this;
      }

      public CmsArticleSQL andTypeGreaterThan(java.lang.Integer value) {
          addCriterion("type", value, ConditionMode.GREATER_THEN, "type", "java.lang.Integer", "Float");
          return this;
      }

      public CmsArticleSQL andTypeGreaterThanOrEqualTo(java.lang.Integer value) {
          addCriterion("type", value, ConditionMode.GREATER_EQUAL, "type", "java.lang.Integer", "Float");
          return this;
      }

      public CmsArticleSQL andTypeLessThan(java.lang.Integer value) {
          addCriterion("type", value, ConditionMode.LESS_THEN, "type", "java.lang.Integer", "Float");
          return this;
      }

      public CmsArticleSQL andTypeLessThanOrEqualTo(java.lang.Integer value) {
          addCriterion("type", value, ConditionMode.LESS_EQUAL, "type", "java.lang.Integer", "Float");
          return this;
      }

      public CmsArticleSQL andTypeBetween(java.lang.Integer value1, java.lang.Integer value2) {
    	  addCriterion("type", value1, value2, ConditionMode.BETWEEN, "type", "java.lang.Integer", "Float");
    	  return this;
      }

      public CmsArticleSQL andTypeNotBetween(java.lang.Integer value1, java.lang.Integer value2) {
          addCriterion("type", value1, value2, ConditionMode.NOT_BETWEEN, "type", "java.lang.Integer", "Float");
          return this;
      }
        
      public CmsArticleSQL andTypeIn(List<java.lang.Integer> values) {
          addCriterion("type", values, ConditionMode.IN, "type", "java.lang.Integer", "Float");
          return this;
      }

      public CmsArticleSQL andTypeNotIn(List<java.lang.Integer> values) {
          addCriterion("type", values, ConditionMode.NOT_IN, "type", "java.lang.Integer", "Float");
          return this;
      }
	public CmsArticleSQL andTagIsNull() {
		isnull("tag");
		return this;
	}
	
	public CmsArticleSQL andTagIsNotNull() {
		notNull("tag");
		return this;
	}
	
	public CmsArticleSQL andTagIsEmpty() {
		empty("tag");
		return this;
	}

	public CmsArticleSQL andTagIsNotEmpty() {
		notEmpty("tag");
		return this;
	}
       public CmsArticleSQL andTagLike(java.lang.String value) {
    	   addCriterion("tag", value, ConditionMode.FUZZY, "tag", "java.lang.String", "Float");
    	   return this;
      }

      public CmsArticleSQL andTagNotLike(java.lang.String value) {
          addCriterion("tag", value, ConditionMode.NOT_FUZZY, "tag", "java.lang.String", "Float");
          return this;
      }
      public CmsArticleSQL andTagEqualTo(java.lang.String value) {
          addCriterion("tag", value, ConditionMode.EQUAL, "tag", "java.lang.String", "String");
          return this;
      }

      public CmsArticleSQL andTagNotEqualTo(java.lang.String value) {
          addCriterion("tag", value, ConditionMode.NOT_EQUAL, "tag", "java.lang.String", "String");
          return this;
      }

      public CmsArticleSQL andTagGreaterThan(java.lang.String value) {
          addCriterion("tag", value, ConditionMode.GREATER_THEN, "tag", "java.lang.String", "String");
          return this;
      }

      public CmsArticleSQL andTagGreaterThanOrEqualTo(java.lang.String value) {
          addCriterion("tag", value, ConditionMode.GREATER_EQUAL, "tag", "java.lang.String", "String");
          return this;
      }

      public CmsArticleSQL andTagLessThan(java.lang.String value) {
          addCriterion("tag", value, ConditionMode.LESS_THEN, "tag", "java.lang.String", "String");
          return this;
      }

      public CmsArticleSQL andTagLessThanOrEqualTo(java.lang.String value) {
          addCriterion("tag", value, ConditionMode.LESS_EQUAL, "tag", "java.lang.String", "String");
          return this;
      }

      public CmsArticleSQL andTagBetween(java.lang.String value1, java.lang.String value2) {
    	  addCriterion("tag", value1, value2, ConditionMode.BETWEEN, "tag", "java.lang.String", "String");
    	  return this;
      }

      public CmsArticleSQL andTagNotBetween(java.lang.String value1, java.lang.String value2) {
          addCriterion("tag", value1, value2, ConditionMode.NOT_BETWEEN, "tag", "java.lang.String", "String");
          return this;
      }
        
      public CmsArticleSQL andTagIn(List<java.lang.String> values) {
          addCriterion("tag", values, ConditionMode.IN, "tag", "java.lang.String", "String");
          return this;
      }

      public CmsArticleSQL andTagNotIn(List<java.lang.String> values) {
          addCriterion("tag", values, ConditionMode.NOT_IN, "tag", "java.lang.String", "String");
          return this;
      }
	public CmsArticleSQL andAliasIsNull() {
		isnull("alias");
		return this;
	}
	
	public CmsArticleSQL andAliasIsNotNull() {
		notNull("alias");
		return this;
	}
	
	public CmsArticleSQL andAliasIsEmpty() {
		empty("alias");
		return this;
	}

	public CmsArticleSQL andAliasIsNotEmpty() {
		notEmpty("alias");
		return this;
	}
       public CmsArticleSQL andAliasLike(java.lang.String value) {
    	   addCriterion("alias", value, ConditionMode.FUZZY, "alias", "java.lang.String", "String");
    	   return this;
      }

      public CmsArticleSQL andAliasNotLike(java.lang.String value) {
          addCriterion("alias", value, ConditionMode.NOT_FUZZY, "alias", "java.lang.String", "String");
          return this;
      }
      public CmsArticleSQL andAliasEqualTo(java.lang.String value) {
          addCriterion("alias", value, ConditionMode.EQUAL, "alias", "java.lang.String", "String");
          return this;
      }

      public CmsArticleSQL andAliasNotEqualTo(java.lang.String value) {
          addCriterion("alias", value, ConditionMode.NOT_EQUAL, "alias", "java.lang.String", "String");
          return this;
      }

      public CmsArticleSQL andAliasGreaterThan(java.lang.String value) {
          addCriterion("alias", value, ConditionMode.GREATER_THEN, "alias", "java.lang.String", "String");
          return this;
      }

      public CmsArticleSQL andAliasGreaterThanOrEqualTo(java.lang.String value) {
          addCriterion("alias", value, ConditionMode.GREATER_EQUAL, "alias", "java.lang.String", "String");
          return this;
      }

      public CmsArticleSQL andAliasLessThan(java.lang.String value) {
          addCriterion("alias", value, ConditionMode.LESS_THEN, "alias", "java.lang.String", "String");
          return this;
      }

      public CmsArticleSQL andAliasLessThanOrEqualTo(java.lang.String value) {
          addCriterion("alias", value, ConditionMode.LESS_EQUAL, "alias", "java.lang.String", "String");
          return this;
      }

      public CmsArticleSQL andAliasBetween(java.lang.String value1, java.lang.String value2) {
    	  addCriterion("alias", value1, value2, ConditionMode.BETWEEN, "alias", "java.lang.String", "String");
    	  return this;
      }

      public CmsArticleSQL andAliasNotBetween(java.lang.String value1, java.lang.String value2) {
          addCriterion("alias", value1, value2, ConditionMode.NOT_BETWEEN, "alias", "java.lang.String", "String");
          return this;
      }
        
      public CmsArticleSQL andAliasIn(List<java.lang.String> values) {
          addCriterion("alias", values, ConditionMode.IN, "alias", "java.lang.String", "String");
          return this;
      }

      public CmsArticleSQL andAliasNotIn(List<java.lang.String> values) {
          addCriterion("alias", values, ConditionMode.NOT_IN, "alias", "java.lang.String", "String");
          return this;
      }
	public CmsArticleSQL andStyleIsNull() {
		isnull("style");
		return this;
	}
	
	public CmsArticleSQL andStyleIsNotNull() {
		notNull("style");
		return this;
	}
	
	public CmsArticleSQL andStyleIsEmpty() {
		empty("style");
		return this;
	}

	public CmsArticleSQL andStyleIsNotEmpty() {
		notEmpty("style");
		return this;
	}
       public CmsArticleSQL andStyleLike(java.lang.String value) {
    	   addCriterion("style", value, ConditionMode.FUZZY, "style", "java.lang.String", "String");
    	   return this;
      }

      public CmsArticleSQL andStyleNotLike(java.lang.String value) {
          addCriterion("style", value, ConditionMode.NOT_FUZZY, "style", "java.lang.String", "String");
          return this;
      }
      public CmsArticleSQL andStyleEqualTo(java.lang.String value) {
          addCriterion("style", value, ConditionMode.EQUAL, "style", "java.lang.String", "String");
          return this;
      }

      public CmsArticleSQL andStyleNotEqualTo(java.lang.String value) {
          addCriterion("style", value, ConditionMode.NOT_EQUAL, "style", "java.lang.String", "String");
          return this;
      }

      public CmsArticleSQL andStyleGreaterThan(java.lang.String value) {
          addCriterion("style", value, ConditionMode.GREATER_THEN, "style", "java.lang.String", "String");
          return this;
      }

      public CmsArticleSQL andStyleGreaterThanOrEqualTo(java.lang.String value) {
          addCriterion("style", value, ConditionMode.GREATER_EQUAL, "style", "java.lang.String", "String");
          return this;
      }

      public CmsArticleSQL andStyleLessThan(java.lang.String value) {
          addCriterion("style", value, ConditionMode.LESS_THEN, "style", "java.lang.String", "String");
          return this;
      }

      public CmsArticleSQL andStyleLessThanOrEqualTo(java.lang.String value) {
          addCriterion("style", value, ConditionMode.LESS_EQUAL, "style", "java.lang.String", "String");
          return this;
      }

      public CmsArticleSQL andStyleBetween(java.lang.String value1, java.lang.String value2) {
    	  addCriterion("style", value1, value2, ConditionMode.BETWEEN, "style", "java.lang.String", "String");
    	  return this;
      }

      public CmsArticleSQL andStyleNotBetween(java.lang.String value1, java.lang.String value2) {
          addCriterion("style", value1, value2, ConditionMode.NOT_BETWEEN, "style", "java.lang.String", "String");
          return this;
      }
        
      public CmsArticleSQL andStyleIn(List<java.lang.String> values) {
          addCriterion("style", values, ConditionMode.IN, "style", "java.lang.String", "String");
          return this;
      }

      public CmsArticleSQL andStyleNotIn(List<java.lang.String> values) {
          addCriterion("style", values, ConditionMode.NOT_IN, "style", "java.lang.String", "String");
          return this;
      }
	public CmsArticleSQL andAllowcommentsIsNull() {
		isnull("allowcomments");
		return this;
	}
	
	public CmsArticleSQL andAllowcommentsIsNotNull() {
		notNull("allowcomments");
		return this;
	}
	
	public CmsArticleSQL andAllowcommentsIsEmpty() {
		empty("allowcomments");
		return this;
	}

	public CmsArticleSQL andAllowcommentsIsNotEmpty() {
		notEmpty("allowcomments");
		return this;
	}
      public CmsArticleSQL andAllowcommentsEqualTo(java.lang.Integer value) {
          addCriterion("allowcomments", value, ConditionMode.EQUAL, "allowcomments", "java.lang.Integer", "Float");
          return this;
      }

      public CmsArticleSQL andAllowcommentsNotEqualTo(java.lang.Integer value) {
          addCriterion("allowcomments", value, ConditionMode.NOT_EQUAL, "allowcomments", "java.lang.Integer", "Float");
          return this;
      }

      public CmsArticleSQL andAllowcommentsGreaterThan(java.lang.Integer value) {
          addCriterion("allowcomments", value, ConditionMode.GREATER_THEN, "allowcomments", "java.lang.Integer", "Float");
          return this;
      }

      public CmsArticleSQL andAllowcommentsGreaterThanOrEqualTo(java.lang.Integer value) {
          addCriterion("allowcomments", value, ConditionMode.GREATER_EQUAL, "allowcomments", "java.lang.Integer", "Float");
          return this;
      }

      public CmsArticleSQL andAllowcommentsLessThan(java.lang.Integer value) {
          addCriterion("allowcomments", value, ConditionMode.LESS_THEN, "allowcomments", "java.lang.Integer", "Float");
          return this;
      }

      public CmsArticleSQL andAllowcommentsLessThanOrEqualTo(java.lang.Integer value) {
          addCriterion("allowcomments", value, ConditionMode.LESS_EQUAL, "allowcomments", "java.lang.Integer", "Float");
          return this;
      }

      public CmsArticleSQL andAllowcommentsBetween(java.lang.Integer value1, java.lang.Integer value2) {
    	  addCriterion("allowcomments", value1, value2, ConditionMode.BETWEEN, "allowcomments", "java.lang.Integer", "Float");
    	  return this;
      }

      public CmsArticleSQL andAllowcommentsNotBetween(java.lang.Integer value1, java.lang.Integer value2) {
          addCriterion("allowcomments", value1, value2, ConditionMode.NOT_BETWEEN, "allowcomments", "java.lang.Integer", "Float");
          return this;
      }
        
      public CmsArticleSQL andAllowcommentsIn(List<java.lang.Integer> values) {
          addCriterion("allowcomments", values, ConditionMode.IN, "allowcomments", "java.lang.Integer", "Float");
          return this;
      }

      public CmsArticleSQL andAllowcommentsNotIn(List<java.lang.Integer> values) {
          addCriterion("allowcomments", values, ConditionMode.NOT_IN, "allowcomments", "java.lang.Integer", "Float");
          return this;
      }
	public CmsArticleSQL andStatusIsNull() {
		isnull("status");
		return this;
	}
	
	public CmsArticleSQL andStatusIsNotNull() {
		notNull("status");
		return this;
	}
	
	public CmsArticleSQL andStatusIsEmpty() {
		empty("status");
		return this;
	}

	public CmsArticleSQL andStatusIsNotEmpty() {
		notEmpty("status");
		return this;
	}
      public CmsArticleSQL andStatusEqualTo(java.lang.Integer value) {
          addCriterion("status", value, ConditionMode.EQUAL, "status", "java.lang.Integer", "Float");
          return this;
      }

      public CmsArticleSQL andStatusNotEqualTo(java.lang.Integer value) {
          addCriterion("status", value, ConditionMode.NOT_EQUAL, "status", "java.lang.Integer", "Float");
          return this;
      }

      public CmsArticleSQL andStatusGreaterThan(java.lang.Integer value) {
          addCriterion("status", value, ConditionMode.GREATER_THEN, "status", "java.lang.Integer", "Float");
          return this;
      }

      public CmsArticleSQL andStatusGreaterThanOrEqualTo(java.lang.Integer value) {
          addCriterion("status", value, ConditionMode.GREATER_EQUAL, "status", "java.lang.Integer", "Float");
          return this;
      }

      public CmsArticleSQL andStatusLessThan(java.lang.Integer value) {
          addCriterion("status", value, ConditionMode.LESS_THEN, "status", "java.lang.Integer", "Float");
          return this;
      }

      public CmsArticleSQL andStatusLessThanOrEqualTo(java.lang.Integer value) {
          addCriterion("status", value, ConditionMode.LESS_EQUAL, "status", "java.lang.Integer", "Float");
          return this;
      }

      public CmsArticleSQL andStatusBetween(java.lang.Integer value1, java.lang.Integer value2) {
    	  addCriterion("status", value1, value2, ConditionMode.BETWEEN, "status", "java.lang.Integer", "Float");
    	  return this;
      }

      public CmsArticleSQL andStatusNotBetween(java.lang.Integer value1, java.lang.Integer value2) {
          addCriterion("status", value1, value2, ConditionMode.NOT_BETWEEN, "status", "java.lang.Integer", "Float");
          return this;
      }
        
      public CmsArticleSQL andStatusIn(List<java.lang.Integer> values) {
          addCriterion("status", values, ConditionMode.IN, "status", "java.lang.Integer", "Float");
          return this;
      }

      public CmsArticleSQL andStatusNotIn(List<java.lang.Integer> values) {
          addCriterion("status", values, ConditionMode.NOT_IN, "status", "java.lang.Integer", "Float");
          return this;
      }
	public CmsArticleSQL andContentIsNull() {
		isnull("content");
		return this;
	}
	
	public CmsArticleSQL andContentIsNotNull() {
		notNull("content");
		return this;
	}
	
	public CmsArticleSQL andContentIsEmpty() {
		empty("content");
		return this;
	}

	public CmsArticleSQL andContentIsNotEmpty() {
		notEmpty("content");
		return this;
	}
       public CmsArticleSQL andContentLike(java.lang.String value) {
    	   addCriterion("content", value, ConditionMode.FUZZY, "content", "java.lang.String", "Float");
    	   return this;
      }

      public CmsArticleSQL andContentNotLike(java.lang.String value) {
          addCriterion("content", value, ConditionMode.NOT_FUZZY, "content", "java.lang.String", "Float");
          return this;
      }
      public CmsArticleSQL andContentEqualTo(java.lang.String value) {
          addCriterion("content", value, ConditionMode.EQUAL, "content", "java.lang.String", "String");
          return this;
      }

      public CmsArticleSQL andContentNotEqualTo(java.lang.String value) {
          addCriterion("content", value, ConditionMode.NOT_EQUAL, "content", "java.lang.String", "String");
          return this;
      }

      public CmsArticleSQL andContentGreaterThan(java.lang.String value) {
          addCriterion("content", value, ConditionMode.GREATER_THEN, "content", "java.lang.String", "String");
          return this;
      }

      public CmsArticleSQL andContentGreaterThanOrEqualTo(java.lang.String value) {
          addCriterion("content", value, ConditionMode.GREATER_EQUAL, "content", "java.lang.String", "String");
          return this;
      }

      public CmsArticleSQL andContentLessThan(java.lang.String value) {
          addCriterion("content", value, ConditionMode.LESS_THEN, "content", "java.lang.String", "String");
          return this;
      }

      public CmsArticleSQL andContentLessThanOrEqualTo(java.lang.String value) {
          addCriterion("content", value, ConditionMode.LESS_EQUAL, "content", "java.lang.String", "String");
          return this;
      }

      public CmsArticleSQL andContentBetween(java.lang.String value1, java.lang.String value2) {
    	  addCriterion("content", value1, value2, ConditionMode.BETWEEN, "content", "java.lang.String", "String");
    	  return this;
      }

      public CmsArticleSQL andContentNotBetween(java.lang.String value1, java.lang.String value2) {
          addCriterion("content", value1, value2, ConditionMode.NOT_BETWEEN, "content", "java.lang.String", "String");
          return this;
      }
        
      public CmsArticleSQL andContentIn(List<java.lang.String> values) {
          addCriterion("content", values, ConditionMode.IN, "content", "java.lang.String", "String");
          return this;
      }

      public CmsArticleSQL andContentNotIn(List<java.lang.String> values) {
          addCriterion("content", values, ConditionMode.NOT_IN, "content", "java.lang.String", "String");
          return this;
      }
	public CmsArticleSQL andEditModeIsNull() {
		isnull("edit_mode");
		return this;
	}
	
	public CmsArticleSQL andEditModeIsNotNull() {
		notNull("edit_mode");
		return this;
	}
	
	public CmsArticleSQL andEditModeIsEmpty() {
		empty("edit_mode");
		return this;
	}

	public CmsArticleSQL andEditModeIsNotEmpty() {
		notEmpty("edit_mode");
		return this;
	}
       public CmsArticleSQL andEditModeLike(java.lang.String value) {
    	   addCriterion("edit_mode", value, ConditionMode.FUZZY, "editMode", "java.lang.String", "String");
    	   return this;
      }

      public CmsArticleSQL andEditModeNotLike(java.lang.String value) {
          addCriterion("edit_mode", value, ConditionMode.NOT_FUZZY, "editMode", "java.lang.String", "String");
          return this;
      }
      public CmsArticleSQL andEditModeEqualTo(java.lang.String value) {
          addCriterion("edit_mode", value, ConditionMode.EQUAL, "editMode", "java.lang.String", "String");
          return this;
      }

      public CmsArticleSQL andEditModeNotEqualTo(java.lang.String value) {
          addCriterion("edit_mode", value, ConditionMode.NOT_EQUAL, "editMode", "java.lang.String", "String");
          return this;
      }

      public CmsArticleSQL andEditModeGreaterThan(java.lang.String value) {
          addCriterion("edit_mode", value, ConditionMode.GREATER_THEN, "editMode", "java.lang.String", "String");
          return this;
      }

      public CmsArticleSQL andEditModeGreaterThanOrEqualTo(java.lang.String value) {
          addCriterion("edit_mode", value, ConditionMode.GREATER_EQUAL, "editMode", "java.lang.String", "String");
          return this;
      }

      public CmsArticleSQL andEditModeLessThan(java.lang.String value) {
          addCriterion("edit_mode", value, ConditionMode.LESS_THEN, "editMode", "java.lang.String", "String");
          return this;
      }

      public CmsArticleSQL andEditModeLessThanOrEqualTo(java.lang.String value) {
          addCriterion("edit_mode", value, ConditionMode.LESS_EQUAL, "editMode", "java.lang.String", "String");
          return this;
      }

      public CmsArticleSQL andEditModeBetween(java.lang.String value1, java.lang.String value2) {
    	  addCriterion("edit_mode", value1, value2, ConditionMode.BETWEEN, "editMode", "java.lang.String", "String");
    	  return this;
      }

      public CmsArticleSQL andEditModeNotBetween(java.lang.String value1, java.lang.String value2) {
          addCriterion("edit_mode", value1, value2, ConditionMode.NOT_BETWEEN, "editMode", "java.lang.String", "String");
          return this;
      }
        
      public CmsArticleSQL andEditModeIn(List<java.lang.String> values) {
          addCriterion("edit_mode", values, ConditionMode.IN, "editMode", "java.lang.String", "String");
          return this;
      }

      public CmsArticleSQL andEditModeNotIn(List<java.lang.String> values) {
          addCriterion("edit_mode", values, ConditionMode.NOT_IN, "editMode", "java.lang.String", "String");
          return this;
      }
	public CmsArticleSQL andUserIdIsNull() {
		isnull("user_id");
		return this;
	}
	
	public CmsArticleSQL andUserIdIsNotNull() {
		notNull("user_id");
		return this;
	}
	
	public CmsArticleSQL andUserIdIsEmpty() {
		empty("user_id");
		return this;
	}

	public CmsArticleSQL andUserIdIsNotEmpty() {
		notEmpty("user_id");
		return this;
	}
      public CmsArticleSQL andUserIdEqualTo(java.lang.Long value) {
          addCriterion("user_id", value, ConditionMode.EQUAL, "userId", "java.lang.Long", "Float");
          return this;
      }

      public CmsArticleSQL andUserIdNotEqualTo(java.lang.Long value) {
          addCriterion("user_id", value, ConditionMode.NOT_EQUAL, "userId", "java.lang.Long", "Float");
          return this;
      }

      public CmsArticleSQL andUserIdGreaterThan(java.lang.Long value) {
          addCriterion("user_id", value, ConditionMode.GREATER_THEN, "userId", "java.lang.Long", "Float");
          return this;
      }

      public CmsArticleSQL andUserIdGreaterThanOrEqualTo(java.lang.Long value) {
          addCriterion("user_id", value, ConditionMode.GREATER_EQUAL, "userId", "java.lang.Long", "Float");
          return this;
      }

      public CmsArticleSQL andUserIdLessThan(java.lang.Long value) {
          addCriterion("user_id", value, ConditionMode.LESS_THEN, "userId", "java.lang.Long", "Float");
          return this;
      }

      public CmsArticleSQL andUserIdLessThanOrEqualTo(java.lang.Long value) {
          addCriterion("user_id", value, ConditionMode.LESS_EQUAL, "userId", "java.lang.Long", "Float");
          return this;
      }

      public CmsArticleSQL andUserIdBetween(java.lang.Long value1, java.lang.Long value2) {
    	  addCriterion("user_id", value1, value2, ConditionMode.BETWEEN, "userId", "java.lang.Long", "Float");
    	  return this;
      }

      public CmsArticleSQL andUserIdNotBetween(java.lang.Long value1, java.lang.Long value2) {
          addCriterion("user_id", value1, value2, ConditionMode.NOT_BETWEEN, "userId", "java.lang.Long", "Float");
          return this;
      }
        
      public CmsArticleSQL andUserIdIn(List<java.lang.Long> values) {
          addCriterion("user_id", values, ConditionMode.IN, "userId", "java.lang.Long", "Float");
          return this;
      }

      public CmsArticleSQL andUserIdNotIn(List<java.lang.Long> values) {
          addCriterion("user_id", values, ConditionMode.NOT_IN, "userId", "java.lang.Long", "Float");
          return this;
      }
	public CmsArticleSQL andReadnumberIsNull() {
		isnull("readnumber");
		return this;
	}
	
	public CmsArticleSQL andReadnumberIsNotNull() {
		notNull("readnumber");
		return this;
	}
	
	public CmsArticleSQL andReadnumberIsEmpty() {
		empty("readnumber");
		return this;
	}

	public CmsArticleSQL andReadnumberIsNotEmpty() {
		notEmpty("readnumber");
		return this;
	}
      public CmsArticleSQL andReadnumberEqualTo(java.lang.Integer value) {
          addCriterion("readnumber", value, ConditionMode.EQUAL, "readnumber", "java.lang.Integer", "Float");
          return this;
      }

      public CmsArticleSQL andReadnumberNotEqualTo(java.lang.Integer value) {
          addCriterion("readnumber", value, ConditionMode.NOT_EQUAL, "readnumber", "java.lang.Integer", "Float");
          return this;
      }

      public CmsArticleSQL andReadnumberGreaterThan(java.lang.Integer value) {
          addCriterion("readnumber", value, ConditionMode.GREATER_THEN, "readnumber", "java.lang.Integer", "Float");
          return this;
      }

      public CmsArticleSQL andReadnumberGreaterThanOrEqualTo(java.lang.Integer value) {
          addCriterion("readnumber", value, ConditionMode.GREATER_EQUAL, "readnumber", "java.lang.Integer", "Float");
          return this;
      }

      public CmsArticleSQL andReadnumberLessThan(java.lang.Integer value) {
          addCriterion("readnumber", value, ConditionMode.LESS_THEN, "readnumber", "java.lang.Integer", "Float");
          return this;
      }

      public CmsArticleSQL andReadnumberLessThanOrEqualTo(java.lang.Integer value) {
          addCriterion("readnumber", value, ConditionMode.LESS_EQUAL, "readnumber", "java.lang.Integer", "Float");
          return this;
      }

      public CmsArticleSQL andReadnumberBetween(java.lang.Integer value1, java.lang.Integer value2) {
    	  addCriterion("readnumber", value1, value2, ConditionMode.BETWEEN, "readnumber", "java.lang.Integer", "Float");
    	  return this;
      }

      public CmsArticleSQL andReadnumberNotBetween(java.lang.Integer value1, java.lang.Integer value2) {
          addCriterion("readnumber", value1, value2, ConditionMode.NOT_BETWEEN, "readnumber", "java.lang.Integer", "Float");
          return this;
      }
        
      public CmsArticleSQL andReadnumberIn(List<java.lang.Integer> values) {
          addCriterion("readnumber", values, ConditionMode.IN, "readnumber", "java.lang.Integer", "Float");
          return this;
      }

      public CmsArticleSQL andReadnumberNotIn(List<java.lang.Integer> values) {
          addCriterion("readnumber", values, ConditionMode.NOT_IN, "readnumber", "java.lang.Integer", "Float");
          return this;
      }
	public CmsArticleSQL andTopIsNull() {
		isnull("top");
		return this;
	}
	
	public CmsArticleSQL andTopIsNotNull() {
		notNull("top");
		return this;
	}
	
	public CmsArticleSQL andTopIsEmpty() {
		empty("top");
		return this;
	}

	public CmsArticleSQL andTopIsNotEmpty() {
		notEmpty("top");
		return this;
	}
      public CmsArticleSQL andTopEqualTo(java.lang.Integer value) {
          addCriterion("top", value, ConditionMode.EQUAL, "top", "java.lang.Integer", "Float");
          return this;
      }

      public CmsArticleSQL andTopNotEqualTo(java.lang.Integer value) {
          addCriterion("top", value, ConditionMode.NOT_EQUAL, "top", "java.lang.Integer", "Float");
          return this;
      }

      public CmsArticleSQL andTopGreaterThan(java.lang.Integer value) {
          addCriterion("top", value, ConditionMode.GREATER_THEN, "top", "java.lang.Integer", "Float");
          return this;
      }

      public CmsArticleSQL andTopGreaterThanOrEqualTo(java.lang.Integer value) {
          addCriterion("top", value, ConditionMode.GREATER_EQUAL, "top", "java.lang.Integer", "Float");
          return this;
      }

      public CmsArticleSQL andTopLessThan(java.lang.Integer value) {
          addCriterion("top", value, ConditionMode.LESS_THEN, "top", "java.lang.Integer", "Float");
          return this;
      }

      public CmsArticleSQL andTopLessThanOrEqualTo(java.lang.Integer value) {
          addCriterion("top", value, ConditionMode.LESS_EQUAL, "top", "java.lang.Integer", "Float");
          return this;
      }

      public CmsArticleSQL andTopBetween(java.lang.Integer value1, java.lang.Integer value2) {
    	  addCriterion("top", value1, value2, ConditionMode.BETWEEN, "top", "java.lang.Integer", "Float");
    	  return this;
      }

      public CmsArticleSQL andTopNotBetween(java.lang.Integer value1, java.lang.Integer value2) {
          addCriterion("top", value1, value2, ConditionMode.NOT_BETWEEN, "top", "java.lang.Integer", "Float");
          return this;
      }
        
      public CmsArticleSQL andTopIn(List<java.lang.Integer> values) {
          addCriterion("top", values, ConditionMode.IN, "top", "java.lang.Integer", "Float");
          return this;
      }

      public CmsArticleSQL andTopNotIn(List<java.lang.Integer> values) {
          addCriterion("top", values, ConditionMode.NOT_IN, "top", "java.lang.Integer", "Float");
          return this;
      }
	public CmsArticleSQL andSystemIdIsNull() {
		isnull("system_id");
		return this;
	}
	
	public CmsArticleSQL andSystemIdIsNotNull() {
		notNull("system_id");
		return this;
	}
	
	public CmsArticleSQL andSystemIdIsEmpty() {
		empty("system_id");
		return this;
	}

	public CmsArticleSQL andSystemIdIsNotEmpty() {
		notEmpty("system_id");
		return this;
	}
      public CmsArticleSQL andSystemIdEqualTo(java.lang.Integer value) {
          addCriterion("system_id", value, ConditionMode.EQUAL, "systemId", "java.lang.Integer", "Float");
          return this;
      }

      public CmsArticleSQL andSystemIdNotEqualTo(java.lang.Integer value) {
          addCriterion("system_id", value, ConditionMode.NOT_EQUAL, "systemId", "java.lang.Integer", "Float");
          return this;
      }

      public CmsArticleSQL andSystemIdGreaterThan(java.lang.Integer value) {
          addCriterion("system_id", value, ConditionMode.GREATER_THEN, "systemId", "java.lang.Integer", "Float");
          return this;
      }

      public CmsArticleSQL andSystemIdGreaterThanOrEqualTo(java.lang.Integer value) {
          addCriterion("system_id", value, ConditionMode.GREATER_EQUAL, "systemId", "java.lang.Integer", "Float");
          return this;
      }

      public CmsArticleSQL andSystemIdLessThan(java.lang.Integer value) {
          addCriterion("system_id", value, ConditionMode.LESS_THEN, "systemId", "java.lang.Integer", "Float");
          return this;
      }

      public CmsArticleSQL andSystemIdLessThanOrEqualTo(java.lang.Integer value) {
          addCriterion("system_id", value, ConditionMode.LESS_EQUAL, "systemId", "java.lang.Integer", "Float");
          return this;
      }

      public CmsArticleSQL andSystemIdBetween(java.lang.Integer value1, java.lang.Integer value2) {
    	  addCriterion("system_id", value1, value2, ConditionMode.BETWEEN, "systemId", "java.lang.Integer", "Float");
    	  return this;
      }

      public CmsArticleSQL andSystemIdNotBetween(java.lang.Integer value1, java.lang.Integer value2) {
          addCriterion("system_id", value1, value2, ConditionMode.NOT_BETWEEN, "systemId", "java.lang.Integer", "Float");
          return this;
      }
        
      public CmsArticleSQL andSystemIdIn(List<java.lang.Integer> values) {
          addCriterion("system_id", values, ConditionMode.IN, "systemId", "java.lang.Integer", "Float");
          return this;
      }

      public CmsArticleSQL andSystemIdNotIn(List<java.lang.Integer> values) {
          addCriterion("system_id", values, ConditionMode.NOT_IN, "systemId", "java.lang.Integer", "Float");
          return this;
      }
	public CmsArticleSQL andCtimeIsNull() {
		isnull("ctime");
		return this;
	}
	
	public CmsArticleSQL andCtimeIsNotNull() {
		notNull("ctime");
		return this;
	}
	
	public CmsArticleSQL andCtimeIsEmpty() {
		empty("ctime");
		return this;
	}

	public CmsArticleSQL andCtimeIsNotEmpty() {
		notEmpty("ctime");
		return this;
	}
      public CmsArticleSQL andCtimeEqualTo(java.util.Date value) {
          addCriterion("ctime", value, ConditionMode.EQUAL, "ctime", "java.util.Date", "String");
          return this;
      }

      public CmsArticleSQL andCtimeNotEqualTo(java.util.Date value) {
          addCriterion("ctime", value, ConditionMode.NOT_EQUAL, "ctime", "java.util.Date", "String");
          return this;
      }

      public CmsArticleSQL andCtimeGreaterThan(java.util.Date value) {
          addCriterion("ctime", value, ConditionMode.GREATER_THEN, "ctime", "java.util.Date", "String");
          return this;
      }

      public CmsArticleSQL andCtimeGreaterThanOrEqualTo(java.util.Date value) {
          addCriterion("ctime", value, ConditionMode.GREATER_EQUAL, "ctime", "java.util.Date", "String");
          return this;
      }

      public CmsArticleSQL andCtimeLessThan(java.util.Date value) {
          addCriterion("ctime", value, ConditionMode.LESS_THEN, "ctime", "java.util.Date", "String");
          return this;
      }

      public CmsArticleSQL andCtimeLessThanOrEqualTo(java.util.Date value) {
          addCriterion("ctime", value, ConditionMode.LESS_EQUAL, "ctime", "java.util.Date", "String");
          return this;
      }

      public CmsArticleSQL andCtimeBetween(java.util.Date value1, java.util.Date value2) {
    	  addCriterion("ctime", value1, value2, ConditionMode.BETWEEN, "ctime", "java.util.Date", "String");
    	  return this;
      }

      public CmsArticleSQL andCtimeNotBetween(java.util.Date value1, java.util.Date value2) {
          addCriterion("ctime", value1, value2, ConditionMode.NOT_BETWEEN, "ctime", "java.util.Date", "String");
          return this;
      }
        
      public CmsArticleSQL andCtimeIn(List<java.util.Date> values) {
          addCriterion("ctime", values, ConditionMode.IN, "ctime", "java.util.Date", "String");
          return this;
      }

      public CmsArticleSQL andCtimeNotIn(List<java.util.Date> values) {
          addCriterion("ctime", values, ConditionMode.NOT_IN, "ctime", "java.util.Date", "String");
          return this;
      }
	public CmsArticleSQL andOrdersIsNull() {
		isnull("orders");
		return this;
	}
	
	public CmsArticleSQL andOrdersIsNotNull() {
		notNull("orders");
		return this;
	}
	
	public CmsArticleSQL andOrdersIsEmpty() {
		empty("orders");
		return this;
	}

	public CmsArticleSQL andOrdersIsNotEmpty() {
		notEmpty("orders");
		return this;
	}
      public CmsArticleSQL andOrdersEqualTo(java.lang.Long value) {
          addCriterion("orders", value, ConditionMode.EQUAL, "orders", "java.lang.Long", "Float");
          return this;
      }

      public CmsArticleSQL andOrdersNotEqualTo(java.lang.Long value) {
          addCriterion("orders", value, ConditionMode.NOT_EQUAL, "orders", "java.lang.Long", "Float");
          return this;
      }

      public CmsArticleSQL andOrdersGreaterThan(java.lang.Long value) {
          addCriterion("orders", value, ConditionMode.GREATER_THEN, "orders", "java.lang.Long", "Float");
          return this;
      }

      public CmsArticleSQL andOrdersGreaterThanOrEqualTo(java.lang.Long value) {
          addCriterion("orders", value, ConditionMode.GREATER_EQUAL, "orders", "java.lang.Long", "Float");
          return this;
      }

      public CmsArticleSQL andOrdersLessThan(java.lang.Long value) {
          addCriterion("orders", value, ConditionMode.LESS_THEN, "orders", "java.lang.Long", "Float");
          return this;
      }

      public CmsArticleSQL andOrdersLessThanOrEqualTo(java.lang.Long value) {
          addCriterion("orders", value, ConditionMode.LESS_EQUAL, "orders", "java.lang.Long", "Float");
          return this;
      }

      public CmsArticleSQL andOrdersBetween(java.lang.Long value1, java.lang.Long value2) {
    	  addCriterion("orders", value1, value2, ConditionMode.BETWEEN, "orders", "java.lang.Long", "Float");
    	  return this;
      }

      public CmsArticleSQL andOrdersNotBetween(java.lang.Long value1, java.lang.Long value2) {
          addCriterion("orders", value1, value2, ConditionMode.NOT_BETWEEN, "orders", "java.lang.Long", "Float");
          return this;
      }
        
      public CmsArticleSQL andOrdersIn(List<java.lang.Long> values) {
          addCriterion("orders", values, ConditionMode.IN, "orders", "java.lang.Long", "Float");
          return this;
      }

      public CmsArticleSQL andOrdersNotIn(List<java.lang.Long> values) {
          addCriterion("orders", values, ConditionMode.NOT_IN, "orders", "java.lang.Long", "Float");
          return this;
      }
}