/**
 * Copyright (c) 2015-2017, Henry Yang 杨勇 (gismail@foxmail.com).
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.lambkit.module.cms.sql;

import com.lambkit.db.sql.ConditionMode;
import com.lambkit.db.sql.Column;
import com.lambkit.db.sql.Columns;
import com.lambkit.db.sql.Example;

import java.io.Serializable;
import java.util.List;

/**
 * @author yangyong 
 * @website: www.lambkit.com
 * @email: gismail@foxmail.com
 * @date ${date}
 * @version 1.0
 * @since 1.0
 */

public class CmsMenuSQL extends Columns implements Serializable {
	private static final long serialVersionUID = 1L;
	
	public static CmsMenuSQL of() {
		return new CmsMenuSQL();
	}
	
	public static CmsMenuSQL by(Column column) {
		CmsMenuSQL that = new CmsMenuSQL();
		that.add(column);
        return that;
    }

    public static CmsMenuSQL by(String name, Object value) {
        return (CmsMenuSQL) create().eq(name, value);
    }
    
    public Example example() {
    	return Example.create("cms_menu", this);
    }
    
    /**
     * equals
     *
     * @param name
     * @param value
     * @return
     */
    public CmsMenuSQL eq(String name, Object value) {
    	super.eq(name, value);
        return this;
    }

    /**
     * not equals !=
     *
     * @param name
     * @param value
     * @return
     */
    public CmsMenuSQL ne(String name, Object value) {
    	super.ne(name, value);
        return this;
    }


    /**
     * like
     *
     * @param name
     * @param value
     * @return
     */

    public CmsMenuSQL like(String name, Object value) {
    	super.like(name, value);
        return this;
    }
    
    public CmsMenuSQL notLike(String name, Object value) {
    	super.notLike(name, value);
        return this;
    }

    /**
     * 大于 great than
     *
     * @param name
     * @param value
     * @return
     */
    public CmsMenuSQL gt(String name, Object value) {
    	super.gt(name, value);
        return this;
    }

    /**
     * 大于等于 great or equal
     *
     * @param name
     * @param value
     * @return
     */
    public CmsMenuSQL ge(String name, Object value) {
    	super.ge(name, value);
        return this;
    }

    /**
     * 小于 less than
     *
     * @param name
     * @param value
     * @return
     */
    public CmsMenuSQL lt(String name, Object value) {
    	super.lt(name, value);
        return this;
    }

    /**
     * 小于等于 less or equal
     *
     * @param name
     * @param value
     * @return
     */
    public CmsMenuSQL le(String name, Object value) {
    	super.le(name, value);
        return this;
    }
    
    public CmsMenuSQL isnull(String name) {
    	super.isnull(name);
        return this;
    } 

    public CmsMenuSQL notNull(String name) {
    	super.notNull(name);
        return this;
    } 
    
    public CmsMenuSQL empty(String name) {
    	super.empty(name);
        return this;
    } 
    
    public CmsMenuSQL notEmpty(String name) {
    	super.notEmpty(name);
        return this;
    } 
    
    public CmsMenuSQL add(Column column) {
    	super.add(column);
    	return this;
    }
    
    /**************************/
	
	public void addCriterion(String name, Object value, ConditionMode logic, String property, String typeHandler, String valueType) {
		 if (value == null) {
			 throw new RuntimeException("Value for " + property + " cannot be null");
		 }
		 add(Column.create(name, value, logic, typeHandler, valueType));
	}
   
	public void addCriterion(String name, Object value1, Object value2, ConditionMode logic, String property, String typeHandler, String valueType) {
		 if (value1 == null || value2 == null) {
			 throw new RuntimeException("Between values for " + property + " cannot be null");
		 }
		 add(Column.create(name, value1, value2, logic, typeHandler, valueType));
	}
		 
	public CmsMenuSQL andMenuIdIsNull() {
		isnull("menu_id");
		return this;
	}
	
	public CmsMenuSQL andMenuIdIsNotNull() {
		notNull("menu_id");
		return this;
	}
	
	public CmsMenuSQL andMenuIdIsEmpty() {
		empty("menu_id");
		return this;
	}

	public CmsMenuSQL andMenuIdIsNotEmpty() {
		notEmpty("menu_id");
		return this;
	}
      public CmsMenuSQL andMenuIdEqualTo(java.lang.Integer value) {
          addCriterion("menu_id", value, ConditionMode.EQUAL, "menuId", "java.lang.Integer", "Float");
          return this;
      }

      public CmsMenuSQL andMenuIdNotEqualTo(java.lang.Integer value) {
          addCriterion("menu_id", value, ConditionMode.NOT_EQUAL, "menuId", "java.lang.Integer", "Float");
          return this;
      }

      public CmsMenuSQL andMenuIdGreaterThan(java.lang.Integer value) {
          addCriterion("menu_id", value, ConditionMode.GREATER_THEN, "menuId", "java.lang.Integer", "Float");
          return this;
      }

      public CmsMenuSQL andMenuIdGreaterThanOrEqualTo(java.lang.Integer value) {
          addCriterion("menu_id", value, ConditionMode.GREATER_EQUAL, "menuId", "java.lang.Integer", "Float");
          return this;
      }

      public CmsMenuSQL andMenuIdLessThan(java.lang.Integer value) {
          addCriterion("menu_id", value, ConditionMode.LESS_THEN, "menuId", "java.lang.Integer", "Float");
          return this;
      }

      public CmsMenuSQL andMenuIdLessThanOrEqualTo(java.lang.Integer value) {
          addCriterion("menu_id", value, ConditionMode.LESS_EQUAL, "menuId", "java.lang.Integer", "Float");
          return this;
      }

      public CmsMenuSQL andMenuIdBetween(java.lang.Integer value1, java.lang.Integer value2) {
    	  addCriterion("menu_id", value1, value2, ConditionMode.BETWEEN, "menuId", "java.lang.Integer", "Float");
    	  return this;
      }

      public CmsMenuSQL andMenuIdNotBetween(java.lang.Integer value1, java.lang.Integer value2) {
          addCriterion("menu_id", value1, value2, ConditionMode.NOT_BETWEEN, "menuId", "java.lang.Integer", "Float");
          return this;
      }
        
      public CmsMenuSQL andMenuIdIn(List<java.lang.Integer> values) {
          addCriterion("menu_id", values, ConditionMode.IN, "menuId", "java.lang.Integer", "Float");
          return this;
      }

      public CmsMenuSQL andMenuIdNotIn(List<java.lang.Integer> values) {
          addCriterion("menu_id", values, ConditionMode.NOT_IN, "menuId", "java.lang.Integer", "Float");
          return this;
      }
	public CmsMenuSQL andPidIsNull() {
		isnull("pid");
		return this;
	}
	
	public CmsMenuSQL andPidIsNotNull() {
		notNull("pid");
		return this;
	}
	
	public CmsMenuSQL andPidIsEmpty() {
		empty("pid");
		return this;
	}

	public CmsMenuSQL andPidIsNotEmpty() {
		notEmpty("pid");
		return this;
	}
      public CmsMenuSQL andPidEqualTo(java.lang.Integer value) {
          addCriterion("pid", value, ConditionMode.EQUAL, "pid", "java.lang.Integer", "Float");
          return this;
      }

      public CmsMenuSQL andPidNotEqualTo(java.lang.Integer value) {
          addCriterion("pid", value, ConditionMode.NOT_EQUAL, "pid", "java.lang.Integer", "Float");
          return this;
      }

      public CmsMenuSQL andPidGreaterThan(java.lang.Integer value) {
          addCriterion("pid", value, ConditionMode.GREATER_THEN, "pid", "java.lang.Integer", "Float");
          return this;
      }

      public CmsMenuSQL andPidGreaterThanOrEqualTo(java.lang.Integer value) {
          addCriterion("pid", value, ConditionMode.GREATER_EQUAL, "pid", "java.lang.Integer", "Float");
          return this;
      }

      public CmsMenuSQL andPidLessThan(java.lang.Integer value) {
          addCriterion("pid", value, ConditionMode.LESS_THEN, "pid", "java.lang.Integer", "Float");
          return this;
      }

      public CmsMenuSQL andPidLessThanOrEqualTo(java.lang.Integer value) {
          addCriterion("pid", value, ConditionMode.LESS_EQUAL, "pid", "java.lang.Integer", "Float");
          return this;
      }

      public CmsMenuSQL andPidBetween(java.lang.Integer value1, java.lang.Integer value2) {
    	  addCriterion("pid", value1, value2, ConditionMode.BETWEEN, "pid", "java.lang.Integer", "Float");
    	  return this;
      }

      public CmsMenuSQL andPidNotBetween(java.lang.Integer value1, java.lang.Integer value2) {
          addCriterion("pid", value1, value2, ConditionMode.NOT_BETWEEN, "pid", "java.lang.Integer", "Float");
          return this;
      }
        
      public CmsMenuSQL andPidIn(List<java.lang.Integer> values) {
          addCriterion("pid", values, ConditionMode.IN, "pid", "java.lang.Integer", "Float");
          return this;
      }

      public CmsMenuSQL andPidNotIn(List<java.lang.Integer> values) {
          addCriterion("pid", values, ConditionMode.NOT_IN, "pid", "java.lang.Integer", "Float");
          return this;
      }
	public CmsMenuSQL andPathIsNull() {
		isnull("path");
		return this;
	}
	
	public CmsMenuSQL andPathIsNotNull() {
		notNull("path");
		return this;
	}
	
	public CmsMenuSQL andPathIsEmpty() {
		empty("path");
		return this;
	}

	public CmsMenuSQL andPathIsNotEmpty() {
		notEmpty("path");
		return this;
	}
       public CmsMenuSQL andPathLike(java.lang.String value) {
    	   addCriterion("path", value, ConditionMode.FUZZY, "path", "java.lang.String", "Float");
    	   return this;
      }

      public CmsMenuSQL andPathNotLike(java.lang.String value) {
          addCriterion("path", value, ConditionMode.NOT_FUZZY, "path", "java.lang.String", "Float");
          return this;
      }
      public CmsMenuSQL andPathEqualTo(java.lang.String value) {
          addCriterion("path", value, ConditionMode.EQUAL, "path", "java.lang.String", "String");
          return this;
      }

      public CmsMenuSQL andPathNotEqualTo(java.lang.String value) {
          addCriterion("path", value, ConditionMode.NOT_EQUAL, "path", "java.lang.String", "String");
          return this;
      }

      public CmsMenuSQL andPathGreaterThan(java.lang.String value) {
          addCriterion("path", value, ConditionMode.GREATER_THEN, "path", "java.lang.String", "String");
          return this;
      }

      public CmsMenuSQL andPathGreaterThanOrEqualTo(java.lang.String value) {
          addCriterion("path", value, ConditionMode.GREATER_EQUAL, "path", "java.lang.String", "String");
          return this;
      }

      public CmsMenuSQL andPathLessThan(java.lang.String value) {
          addCriterion("path", value, ConditionMode.LESS_THEN, "path", "java.lang.String", "String");
          return this;
      }

      public CmsMenuSQL andPathLessThanOrEqualTo(java.lang.String value) {
          addCriterion("path", value, ConditionMode.LESS_EQUAL, "path", "java.lang.String", "String");
          return this;
      }

      public CmsMenuSQL andPathBetween(java.lang.String value1, java.lang.String value2) {
    	  addCriterion("path", value1, value2, ConditionMode.BETWEEN, "path", "java.lang.String", "String");
    	  return this;
      }

      public CmsMenuSQL andPathNotBetween(java.lang.String value1, java.lang.String value2) {
          addCriterion("path", value1, value2, ConditionMode.NOT_BETWEEN, "path", "java.lang.String", "String");
          return this;
      }
        
      public CmsMenuSQL andPathIn(List<java.lang.String> values) {
          addCriterion("path", values, ConditionMode.IN, "path", "java.lang.String", "String");
          return this;
      }

      public CmsMenuSQL andPathNotIn(List<java.lang.String> values) {
          addCriterion("path", values, ConditionMode.NOT_IN, "path", "java.lang.String", "String");
          return this;
      }
	public CmsMenuSQL andNameIsNull() {
		isnull("name");
		return this;
	}
	
	public CmsMenuSQL andNameIsNotNull() {
		notNull("name");
		return this;
	}
	
	public CmsMenuSQL andNameIsEmpty() {
		empty("name");
		return this;
	}

	public CmsMenuSQL andNameIsNotEmpty() {
		notEmpty("name");
		return this;
	}
       public CmsMenuSQL andNameLike(java.lang.String value) {
    	   addCriterion("name", value, ConditionMode.FUZZY, "name", "java.lang.String", "String");
    	   return this;
      }

      public CmsMenuSQL andNameNotLike(java.lang.String value) {
          addCriterion("name", value, ConditionMode.NOT_FUZZY, "name", "java.lang.String", "String");
          return this;
      }
      public CmsMenuSQL andNameEqualTo(java.lang.String value) {
          addCriterion("name", value, ConditionMode.EQUAL, "name", "java.lang.String", "String");
          return this;
      }

      public CmsMenuSQL andNameNotEqualTo(java.lang.String value) {
          addCriterion("name", value, ConditionMode.NOT_EQUAL, "name", "java.lang.String", "String");
          return this;
      }

      public CmsMenuSQL andNameGreaterThan(java.lang.String value) {
          addCriterion("name", value, ConditionMode.GREATER_THEN, "name", "java.lang.String", "String");
          return this;
      }

      public CmsMenuSQL andNameGreaterThanOrEqualTo(java.lang.String value) {
          addCriterion("name", value, ConditionMode.GREATER_EQUAL, "name", "java.lang.String", "String");
          return this;
      }

      public CmsMenuSQL andNameLessThan(java.lang.String value) {
          addCriterion("name", value, ConditionMode.LESS_THEN, "name", "java.lang.String", "String");
          return this;
      }

      public CmsMenuSQL andNameLessThanOrEqualTo(java.lang.String value) {
          addCriterion("name", value, ConditionMode.LESS_EQUAL, "name", "java.lang.String", "String");
          return this;
      }

      public CmsMenuSQL andNameBetween(java.lang.String value1, java.lang.String value2) {
    	  addCriterion("name", value1, value2, ConditionMode.BETWEEN, "name", "java.lang.String", "String");
    	  return this;
      }

      public CmsMenuSQL andNameNotBetween(java.lang.String value1, java.lang.String value2) {
          addCriterion("name", value1, value2, ConditionMode.NOT_BETWEEN, "name", "java.lang.String", "String");
          return this;
      }
        
      public CmsMenuSQL andNameIn(List<java.lang.String> values) {
          addCriterion("name", values, ConditionMode.IN, "name", "java.lang.String", "String");
          return this;
      }

      public CmsMenuSQL andNameNotIn(List<java.lang.String> values) {
          addCriterion("name", values, ConditionMode.NOT_IN, "name", "java.lang.String", "String");
          return this;
      }
	public CmsMenuSQL andUrlIsNull() {
		isnull("url");
		return this;
	}
	
	public CmsMenuSQL andUrlIsNotNull() {
		notNull("url");
		return this;
	}
	
	public CmsMenuSQL andUrlIsEmpty() {
		empty("url");
		return this;
	}

	public CmsMenuSQL andUrlIsNotEmpty() {
		notEmpty("url");
		return this;
	}
       public CmsMenuSQL andUrlLike(java.lang.String value) {
    	   addCriterion("url", value, ConditionMode.FUZZY, "url", "java.lang.String", "String");
    	   return this;
      }

      public CmsMenuSQL andUrlNotLike(java.lang.String value) {
          addCriterion("url", value, ConditionMode.NOT_FUZZY, "url", "java.lang.String", "String");
          return this;
      }
      public CmsMenuSQL andUrlEqualTo(java.lang.String value) {
          addCriterion("url", value, ConditionMode.EQUAL, "url", "java.lang.String", "String");
          return this;
      }

      public CmsMenuSQL andUrlNotEqualTo(java.lang.String value) {
          addCriterion("url", value, ConditionMode.NOT_EQUAL, "url", "java.lang.String", "String");
          return this;
      }

      public CmsMenuSQL andUrlGreaterThan(java.lang.String value) {
          addCriterion("url", value, ConditionMode.GREATER_THEN, "url", "java.lang.String", "String");
          return this;
      }

      public CmsMenuSQL andUrlGreaterThanOrEqualTo(java.lang.String value) {
          addCriterion("url", value, ConditionMode.GREATER_EQUAL, "url", "java.lang.String", "String");
          return this;
      }

      public CmsMenuSQL andUrlLessThan(java.lang.String value) {
          addCriterion("url", value, ConditionMode.LESS_THEN, "url", "java.lang.String", "String");
          return this;
      }

      public CmsMenuSQL andUrlLessThanOrEqualTo(java.lang.String value) {
          addCriterion("url", value, ConditionMode.LESS_EQUAL, "url", "java.lang.String", "String");
          return this;
      }

      public CmsMenuSQL andUrlBetween(java.lang.String value1, java.lang.String value2) {
    	  addCriterion("url", value1, value2, ConditionMode.BETWEEN, "url", "java.lang.String", "String");
    	  return this;
      }

      public CmsMenuSQL andUrlNotBetween(java.lang.String value1, java.lang.String value2) {
          addCriterion("url", value1, value2, ConditionMode.NOT_BETWEEN, "url", "java.lang.String", "String");
          return this;
      }
        
      public CmsMenuSQL andUrlIn(List<java.lang.String> values) {
          addCriterion("url", values, ConditionMode.IN, "url", "java.lang.String", "String");
          return this;
      }

      public CmsMenuSQL andUrlNotIn(List<java.lang.String> values) {
          addCriterion("url", values, ConditionMode.NOT_IN, "url", "java.lang.String", "String");
          return this;
      }
	public CmsMenuSQL andTargetIsNull() {
		isnull("target");
		return this;
	}
	
	public CmsMenuSQL andTargetIsNotNull() {
		notNull("target");
		return this;
	}
	
	public CmsMenuSQL andTargetIsEmpty() {
		empty("target");
		return this;
	}

	public CmsMenuSQL andTargetIsNotEmpty() {
		notEmpty("target");
		return this;
	}
       public CmsMenuSQL andTargetLike(java.lang.String value) {
    	   addCriterion("target", value, ConditionMode.FUZZY, "target", "java.lang.String", "String");
    	   return this;
      }

      public CmsMenuSQL andTargetNotLike(java.lang.String value) {
          addCriterion("target", value, ConditionMode.NOT_FUZZY, "target", "java.lang.String", "String");
          return this;
      }
      public CmsMenuSQL andTargetEqualTo(java.lang.String value) {
          addCriterion("target", value, ConditionMode.EQUAL, "target", "java.lang.String", "String");
          return this;
      }

      public CmsMenuSQL andTargetNotEqualTo(java.lang.String value) {
          addCriterion("target", value, ConditionMode.NOT_EQUAL, "target", "java.lang.String", "String");
          return this;
      }

      public CmsMenuSQL andTargetGreaterThan(java.lang.String value) {
          addCriterion("target", value, ConditionMode.GREATER_THEN, "target", "java.lang.String", "String");
          return this;
      }

      public CmsMenuSQL andTargetGreaterThanOrEqualTo(java.lang.String value) {
          addCriterion("target", value, ConditionMode.GREATER_EQUAL, "target", "java.lang.String", "String");
          return this;
      }

      public CmsMenuSQL andTargetLessThan(java.lang.String value) {
          addCriterion("target", value, ConditionMode.LESS_THEN, "target", "java.lang.String", "String");
          return this;
      }

      public CmsMenuSQL andTargetLessThanOrEqualTo(java.lang.String value) {
          addCriterion("target", value, ConditionMode.LESS_EQUAL, "target", "java.lang.String", "String");
          return this;
      }

      public CmsMenuSQL andTargetBetween(java.lang.String value1, java.lang.String value2) {
    	  addCriterion("target", value1, value2, ConditionMode.BETWEEN, "target", "java.lang.String", "String");
    	  return this;
      }

      public CmsMenuSQL andTargetNotBetween(java.lang.String value1, java.lang.String value2) {
          addCriterion("target", value1, value2, ConditionMode.NOT_BETWEEN, "target", "java.lang.String", "String");
          return this;
      }
        
      public CmsMenuSQL andTargetIn(List<java.lang.String> values) {
          addCriterion("target", values, ConditionMode.IN, "target", "java.lang.String", "String");
          return this;
      }

      public CmsMenuSQL andTargetNotIn(List<java.lang.String> values) {
          addCriterion("target", values, ConditionMode.NOT_IN, "target", "java.lang.String", "String");
          return this;
      }
	public CmsMenuSQL andOrdersIsNull() {
		isnull("orders");
		return this;
	}
	
	public CmsMenuSQL andOrdersIsNotNull() {
		notNull("orders");
		return this;
	}
	
	public CmsMenuSQL andOrdersIsEmpty() {
		empty("orders");
		return this;
	}

	public CmsMenuSQL andOrdersIsNotEmpty() {
		notEmpty("orders");
		return this;
	}
      public CmsMenuSQL andOrdersEqualTo(java.lang.Long value) {
          addCriterion("orders", value, ConditionMode.EQUAL, "orders", "java.lang.Long", "Float");
          return this;
      }

      public CmsMenuSQL andOrdersNotEqualTo(java.lang.Long value) {
          addCriterion("orders", value, ConditionMode.NOT_EQUAL, "orders", "java.lang.Long", "Float");
          return this;
      }

      public CmsMenuSQL andOrdersGreaterThan(java.lang.Long value) {
          addCriterion("orders", value, ConditionMode.GREATER_THEN, "orders", "java.lang.Long", "Float");
          return this;
      }

      public CmsMenuSQL andOrdersGreaterThanOrEqualTo(java.lang.Long value) {
          addCriterion("orders", value, ConditionMode.GREATER_EQUAL, "orders", "java.lang.Long", "Float");
          return this;
      }

      public CmsMenuSQL andOrdersLessThan(java.lang.Long value) {
          addCriterion("orders", value, ConditionMode.LESS_THEN, "orders", "java.lang.Long", "Float");
          return this;
      }

      public CmsMenuSQL andOrdersLessThanOrEqualTo(java.lang.Long value) {
          addCriterion("orders", value, ConditionMode.LESS_EQUAL, "orders", "java.lang.Long", "Float");
          return this;
      }

      public CmsMenuSQL andOrdersBetween(java.lang.Long value1, java.lang.Long value2) {
    	  addCriterion("orders", value1, value2, ConditionMode.BETWEEN, "orders", "java.lang.Long", "Float");
    	  return this;
      }

      public CmsMenuSQL andOrdersNotBetween(java.lang.Long value1, java.lang.Long value2) {
          addCriterion("orders", value1, value2, ConditionMode.NOT_BETWEEN, "orders", "java.lang.Long", "Float");
          return this;
      }
        
      public CmsMenuSQL andOrdersIn(List<java.lang.Long> values) {
          addCriterion("orders", values, ConditionMode.IN, "orders", "java.lang.Long", "Float");
          return this;
      }

      public CmsMenuSQL andOrdersNotIn(List<java.lang.Long> values) {
          addCriterion("orders", values, ConditionMode.NOT_IN, "orders", "java.lang.Long", "Float");
          return this;
      }
}