/**
 * Copyright (c) 2015-2017, Henry Yang 杨勇 (gismail@foxmail.com).
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.lambkit.module.cms.row;

import com.lambkit.db.RowModel;

/**
 * @author yangyong 
 * @website: www.lambkit.com
 * @email: gismail@foxmail.com
 * @date ${date}
 * @version 1.0
 * @since 1.0
 */

public class CmsDict extends RowModel<CmsDict> {
	public CmsDict() {
		setTableName("cms_dict");
		setPrimaryKey("dict_id");
	}
	public java.lang.Long getDictId() {
		return this.get("dict_id");
	}
	public void setDictId(java.lang.Long dictId) {
		this.set("dict_id", dictId);
	}
	public java.lang.String getDictKey() {
		return this.get("dict_key");
	}
	public void setDictKey(java.lang.String dictKey) {
		this.set("dict_key", dictKey);
	}
	public java.lang.String getDictValue() {
		return this.get("dict_value");
	}
	public void setDictValue(java.lang.String dictValue) {
		this.set("dict_value", dictValue);
	}
	public java.lang.String getDictTitle() {
		return this.get("dict_title");
	}
	public void setDictTitle(java.lang.String dictTitle) {
		this.set("dict_title", dictTitle);
	}
	public java.lang.Integer getOrders() {
		return this.get("orders");
	}
	public void setOrders(java.lang.Integer orders) {
		this.set("orders", orders);
	}
	public java.lang.Integer getStatus() {
		return this.get("status");
	}
	public void setStatus(java.lang.Integer status) {
		this.set("status", status);
	}
	public java.lang.Integer getDictDefault() {
		return this.get("dict_default");
	}
	public void setDictDefault(java.lang.Integer dictDefault) {
		this.set("dict_default", dictDefault);
	}
	public java.lang.String getViewStyle() {
		return this.get("view_style");
	}
	public void setViewStyle(java.lang.String viewStyle) {
		this.set("view_style", viewStyle);
	}
	public java.lang.String getViewType() {
		return this.get("view_type");
	}
	public void setViewType(java.lang.String viewType) {
		this.set("view_type", viewType);
	}
	public java.lang.String getRemark() {
		return this.get("remark");
	}
	public void setRemark(java.lang.String remark) {
		this.set("remark", remark);
	}
	public java.util.Date getCreated() {
		return this.get("created");
	}
	public void setCreated(java.util.Date created) {
		this.set("created", created);
	}
}
