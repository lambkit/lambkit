/**
 * Copyright (c) 2015-2017, Henry Yang 杨勇 (gismail@foxmail.com).
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.lambkit.module.cms.sql;

import com.lambkit.db.sql.ConditionMode;
import com.lambkit.db.sql.Column;
import com.lambkit.db.sql.Columns;
import com.lambkit.db.sql.Example;

import java.io.Serializable;
import java.util.List;

/**
 * @author yangyong 
 * @website: www.lambkit.com
 * @email: gismail@foxmail.com
 * @date ${date}
 * @version 1.0
 * @since 1.0
 */

public class CmsArticleScoreSQL extends Columns implements Serializable {
	private static final long serialVersionUID = 1L;
	
	public static CmsArticleScoreSQL of() {
		return new CmsArticleScoreSQL();
	}
	
	public static CmsArticleScoreSQL by(Column column) {
		CmsArticleScoreSQL that = new CmsArticleScoreSQL();
		that.add(column);
        return that;
    }

    public static CmsArticleScoreSQL by(String name, Object value) {
        return (CmsArticleScoreSQL) create().eq(name, value);
    }
    
    public Example example() {
    	return Example.create("cms_article_score", this);
    }
    
    /**
     * equals
     *
     * @param name
     * @param value
     * @return
     */
    public CmsArticleScoreSQL eq(String name, Object value) {
    	super.eq(name, value);
        return this;
    }

    /**
     * not equals !=
     *
     * @param name
     * @param value
     * @return
     */
    public CmsArticleScoreSQL ne(String name, Object value) {
    	super.ne(name, value);
        return this;
    }


    /**
     * like
     *
     * @param name
     * @param value
     * @return
     */

    public CmsArticleScoreSQL like(String name, Object value) {
    	super.like(name, value);
        return this;
    }
    
    public CmsArticleScoreSQL notLike(String name, Object value) {
    	super.notLike(name, value);
        return this;
    }

    /**
     * 大于 great than
     *
     * @param name
     * @param value
     * @return
     */
    public CmsArticleScoreSQL gt(String name, Object value) {
    	super.gt(name, value);
        return this;
    }

    /**
     * 大于等于 great or equal
     *
     * @param name
     * @param value
     * @return
     */
    public CmsArticleScoreSQL ge(String name, Object value) {
    	super.ge(name, value);
        return this;
    }

    /**
     * 小于 less than
     *
     * @param name
     * @param value
     * @return
     */
    public CmsArticleScoreSQL lt(String name, Object value) {
    	super.lt(name, value);
        return this;
    }

    /**
     * 小于等于 less or equal
     *
     * @param name
     * @param value
     * @return
     */
    public CmsArticleScoreSQL le(String name, Object value) {
    	super.le(name, value);
        return this;
    }
    
    public CmsArticleScoreSQL isnull(String name) {
    	super.isnull(name);
        return this;
    } 

    public CmsArticleScoreSQL notNull(String name) {
    	super.notNull(name);
        return this;
    } 
    
    public CmsArticleScoreSQL empty(String name) {
    	super.empty(name);
        return this;
    } 
    
    public CmsArticleScoreSQL notEmpty(String name) {
    	super.notEmpty(name);
        return this;
    } 
    
    public CmsArticleScoreSQL add(Column column) {
    	super.add(column);
    	return this;
    }
    
    /**************************/
	
	public void addCriterion(String name, Object value, ConditionMode logic, String property, String typeHandler, String valueType) {
		 if (value == null) {
			 throw new RuntimeException("Value for " + property + " cannot be null");
		 }
		 add(Column.create(name, value, logic, typeHandler, valueType));
	}
   
	public void addCriterion(String name, Object value1, Object value2, ConditionMode logic, String property, String typeHandler, String valueType) {
		 if (value1 == null || value2 == null) {
			 throw new RuntimeException("Between values for " + property + " cannot be null");
		 }
		 add(Column.create(name, value1, value2, logic, typeHandler, valueType));
	}
		 
	public CmsArticleScoreSQL andScoreIdIsNull() {
		isnull("score_id");
		return this;
	}
	
	public CmsArticleScoreSQL andScoreIdIsNotNull() {
		notNull("score_id");
		return this;
	}
	
	public CmsArticleScoreSQL andScoreIdIsEmpty() {
		empty("score_id");
		return this;
	}

	public CmsArticleScoreSQL andScoreIdIsNotEmpty() {
		notEmpty("score_id");
		return this;
	}
      public CmsArticleScoreSQL andScoreIdEqualTo(java.lang.Long value) {
          addCriterion("score_id", value, ConditionMode.EQUAL, "scoreId", "java.lang.Long", "Float");
          return this;
      }

      public CmsArticleScoreSQL andScoreIdNotEqualTo(java.lang.Long value) {
          addCriterion("score_id", value, ConditionMode.NOT_EQUAL, "scoreId", "java.lang.Long", "Float");
          return this;
      }

      public CmsArticleScoreSQL andScoreIdGreaterThan(java.lang.Long value) {
          addCriterion("score_id", value, ConditionMode.GREATER_THEN, "scoreId", "java.lang.Long", "Float");
          return this;
      }

      public CmsArticleScoreSQL andScoreIdGreaterThanOrEqualTo(java.lang.Long value) {
          addCriterion("score_id", value, ConditionMode.GREATER_EQUAL, "scoreId", "java.lang.Long", "Float");
          return this;
      }

      public CmsArticleScoreSQL andScoreIdLessThan(java.lang.Long value) {
          addCriterion("score_id", value, ConditionMode.LESS_THEN, "scoreId", "java.lang.Long", "Float");
          return this;
      }

      public CmsArticleScoreSQL andScoreIdLessThanOrEqualTo(java.lang.Long value) {
          addCriterion("score_id", value, ConditionMode.LESS_EQUAL, "scoreId", "java.lang.Long", "Float");
          return this;
      }

      public CmsArticleScoreSQL andScoreIdBetween(java.lang.Long value1, java.lang.Long value2) {
    	  addCriterion("score_id", value1, value2, ConditionMode.BETWEEN, "scoreId", "java.lang.Long", "Float");
    	  return this;
      }

      public CmsArticleScoreSQL andScoreIdNotBetween(java.lang.Long value1, java.lang.Long value2) {
          addCriterion("score_id", value1, value2, ConditionMode.NOT_BETWEEN, "scoreId", "java.lang.Long", "Float");
          return this;
      }
        
      public CmsArticleScoreSQL andScoreIdIn(List<java.lang.Long> values) {
          addCriterion("score_id", values, ConditionMode.IN, "scoreId", "java.lang.Long", "Float");
          return this;
      }

      public CmsArticleScoreSQL andScoreIdNotIn(List<java.lang.Long> values) {
          addCriterion("score_id", values, ConditionMode.NOT_IN, "scoreId", "java.lang.Long", "Float");
          return this;
      }
	public CmsArticleScoreSQL andUserIdIsNull() {
		isnull("user_id");
		return this;
	}
	
	public CmsArticleScoreSQL andUserIdIsNotNull() {
		notNull("user_id");
		return this;
	}
	
	public CmsArticleScoreSQL andUserIdIsEmpty() {
		empty("user_id");
		return this;
	}

	public CmsArticleScoreSQL andUserIdIsNotEmpty() {
		notEmpty("user_id");
		return this;
	}
      public CmsArticleScoreSQL andUserIdEqualTo(java.lang.Long value) {
          addCriterion("user_id", value, ConditionMode.EQUAL, "userId", "java.lang.Long", "Float");
          return this;
      }

      public CmsArticleScoreSQL andUserIdNotEqualTo(java.lang.Long value) {
          addCriterion("user_id", value, ConditionMode.NOT_EQUAL, "userId", "java.lang.Long", "Float");
          return this;
      }

      public CmsArticleScoreSQL andUserIdGreaterThan(java.lang.Long value) {
          addCriterion("user_id", value, ConditionMode.GREATER_THEN, "userId", "java.lang.Long", "Float");
          return this;
      }

      public CmsArticleScoreSQL andUserIdGreaterThanOrEqualTo(java.lang.Long value) {
          addCriterion("user_id", value, ConditionMode.GREATER_EQUAL, "userId", "java.lang.Long", "Float");
          return this;
      }

      public CmsArticleScoreSQL andUserIdLessThan(java.lang.Long value) {
          addCriterion("user_id", value, ConditionMode.LESS_THEN, "userId", "java.lang.Long", "Float");
          return this;
      }

      public CmsArticleScoreSQL andUserIdLessThanOrEqualTo(java.lang.Long value) {
          addCriterion("user_id", value, ConditionMode.LESS_EQUAL, "userId", "java.lang.Long", "Float");
          return this;
      }

      public CmsArticleScoreSQL andUserIdBetween(java.lang.Long value1, java.lang.Long value2) {
    	  addCriterion("user_id", value1, value2, ConditionMode.BETWEEN, "userId", "java.lang.Long", "Float");
    	  return this;
      }

      public CmsArticleScoreSQL andUserIdNotBetween(java.lang.Long value1, java.lang.Long value2) {
          addCriterion("user_id", value1, value2, ConditionMode.NOT_BETWEEN, "userId", "java.lang.Long", "Float");
          return this;
      }
        
      public CmsArticleScoreSQL andUserIdIn(List<java.lang.Long> values) {
          addCriterion("user_id", values, ConditionMode.IN, "userId", "java.lang.Long", "Float");
          return this;
      }

      public CmsArticleScoreSQL andUserIdNotIn(List<java.lang.Long> values) {
          addCriterion("user_id", values, ConditionMode.NOT_IN, "userId", "java.lang.Long", "Float");
          return this;
      }
	public CmsArticleScoreSQL andItemIdIsNull() {
		isnull("item_id");
		return this;
	}
	
	public CmsArticleScoreSQL andItemIdIsNotNull() {
		notNull("item_id");
		return this;
	}
	
	public CmsArticleScoreSQL andItemIdIsEmpty() {
		empty("item_id");
		return this;
	}

	public CmsArticleScoreSQL andItemIdIsNotEmpty() {
		notEmpty("item_id");
		return this;
	}
      public CmsArticleScoreSQL andItemIdEqualTo(java.lang.Long value) {
          addCriterion("item_id", value, ConditionMode.EQUAL, "itemId", "java.lang.Long", "Float");
          return this;
      }

      public CmsArticleScoreSQL andItemIdNotEqualTo(java.lang.Long value) {
          addCriterion("item_id", value, ConditionMode.NOT_EQUAL, "itemId", "java.lang.Long", "Float");
          return this;
      }

      public CmsArticleScoreSQL andItemIdGreaterThan(java.lang.Long value) {
          addCriterion("item_id", value, ConditionMode.GREATER_THEN, "itemId", "java.lang.Long", "Float");
          return this;
      }

      public CmsArticleScoreSQL andItemIdGreaterThanOrEqualTo(java.lang.Long value) {
          addCriterion("item_id", value, ConditionMode.GREATER_EQUAL, "itemId", "java.lang.Long", "Float");
          return this;
      }

      public CmsArticleScoreSQL andItemIdLessThan(java.lang.Long value) {
          addCriterion("item_id", value, ConditionMode.LESS_THEN, "itemId", "java.lang.Long", "Float");
          return this;
      }

      public CmsArticleScoreSQL andItemIdLessThanOrEqualTo(java.lang.Long value) {
          addCriterion("item_id", value, ConditionMode.LESS_EQUAL, "itemId", "java.lang.Long", "Float");
          return this;
      }

      public CmsArticleScoreSQL andItemIdBetween(java.lang.Long value1, java.lang.Long value2) {
    	  addCriterion("item_id", value1, value2, ConditionMode.BETWEEN, "itemId", "java.lang.Long", "Float");
    	  return this;
      }

      public CmsArticleScoreSQL andItemIdNotBetween(java.lang.Long value1, java.lang.Long value2) {
          addCriterion("item_id", value1, value2, ConditionMode.NOT_BETWEEN, "itemId", "java.lang.Long", "Float");
          return this;
      }
        
      public CmsArticleScoreSQL andItemIdIn(List<java.lang.Long> values) {
          addCriterion("item_id", values, ConditionMode.IN, "itemId", "java.lang.Long", "Float");
          return this;
      }

      public CmsArticleScoreSQL andItemIdNotIn(List<java.lang.Long> values) {
          addCriterion("item_id", values, ConditionMode.NOT_IN, "itemId", "java.lang.Long", "Float");
          return this;
      }
	public CmsArticleScoreSQL andScoreIsNull() {
		isnull("score");
		return this;
	}
	
	public CmsArticleScoreSQL andScoreIsNotNull() {
		notNull("score");
		return this;
	}
	
	public CmsArticleScoreSQL andScoreIsEmpty() {
		empty("score");
		return this;
	}

	public CmsArticleScoreSQL andScoreIsNotEmpty() {
		notEmpty("score");
		return this;
	}
      public CmsArticleScoreSQL andScoreEqualTo(java.lang.Integer value) {
          addCriterion("score", value, ConditionMode.EQUAL, "score", "java.lang.Integer", "Float");
          return this;
      }

      public CmsArticleScoreSQL andScoreNotEqualTo(java.lang.Integer value) {
          addCriterion("score", value, ConditionMode.NOT_EQUAL, "score", "java.lang.Integer", "Float");
          return this;
      }

      public CmsArticleScoreSQL andScoreGreaterThan(java.lang.Integer value) {
          addCriterion("score", value, ConditionMode.GREATER_THEN, "score", "java.lang.Integer", "Float");
          return this;
      }

      public CmsArticleScoreSQL andScoreGreaterThanOrEqualTo(java.lang.Integer value) {
          addCriterion("score", value, ConditionMode.GREATER_EQUAL, "score", "java.lang.Integer", "Float");
          return this;
      }

      public CmsArticleScoreSQL andScoreLessThan(java.lang.Integer value) {
          addCriterion("score", value, ConditionMode.LESS_THEN, "score", "java.lang.Integer", "Float");
          return this;
      }

      public CmsArticleScoreSQL andScoreLessThanOrEqualTo(java.lang.Integer value) {
          addCriterion("score", value, ConditionMode.LESS_EQUAL, "score", "java.lang.Integer", "Float");
          return this;
      }

      public CmsArticleScoreSQL andScoreBetween(java.lang.Integer value1, java.lang.Integer value2) {
    	  addCriterion("score", value1, value2, ConditionMode.BETWEEN, "score", "java.lang.Integer", "Float");
    	  return this;
      }

      public CmsArticleScoreSQL andScoreNotBetween(java.lang.Integer value1, java.lang.Integer value2) {
          addCriterion("score", value1, value2, ConditionMode.NOT_BETWEEN, "score", "java.lang.Integer", "Float");
          return this;
      }
        
      public CmsArticleScoreSQL andScoreIn(List<java.lang.Integer> values) {
          addCriterion("score", values, ConditionMode.IN, "score", "java.lang.Integer", "Float");
          return this;
      }

      public CmsArticleScoreSQL andScoreNotIn(List<java.lang.Integer> values) {
          addCriterion("score", values, ConditionMode.NOT_IN, "score", "java.lang.Integer", "Float");
          return this;
      }
	public CmsArticleScoreSQL andTimeIsNull() {
		isnull("time");
		return this;
	}
	
	public CmsArticleScoreSQL andTimeIsNotNull() {
		notNull("time");
		return this;
	}
	
	public CmsArticleScoreSQL andTimeIsEmpty() {
		empty("time");
		return this;
	}

	public CmsArticleScoreSQL andTimeIsNotEmpty() {
		notEmpty("time");
		return this;
	}
      public CmsArticleScoreSQL andTimeEqualTo(java.util.Date value) {
          addCriterion("time", value, ConditionMode.EQUAL, "time", "java.util.Date", "String");
          return this;
      }

      public CmsArticleScoreSQL andTimeNotEqualTo(java.util.Date value) {
          addCriterion("time", value, ConditionMode.NOT_EQUAL, "time", "java.util.Date", "String");
          return this;
      }

      public CmsArticleScoreSQL andTimeGreaterThan(java.util.Date value) {
          addCriterion("time", value, ConditionMode.GREATER_THEN, "time", "java.util.Date", "String");
          return this;
      }

      public CmsArticleScoreSQL andTimeGreaterThanOrEqualTo(java.util.Date value) {
          addCriterion("time", value, ConditionMode.GREATER_EQUAL, "time", "java.util.Date", "String");
          return this;
      }

      public CmsArticleScoreSQL andTimeLessThan(java.util.Date value) {
          addCriterion("time", value, ConditionMode.LESS_THEN, "time", "java.util.Date", "String");
          return this;
      }

      public CmsArticleScoreSQL andTimeLessThanOrEqualTo(java.util.Date value) {
          addCriterion("time", value, ConditionMode.LESS_EQUAL, "time", "java.util.Date", "String");
          return this;
      }

      public CmsArticleScoreSQL andTimeBetween(java.util.Date value1, java.util.Date value2) {
    	  addCriterion("time", value1, value2, ConditionMode.BETWEEN, "time", "java.util.Date", "String");
    	  return this;
      }

      public CmsArticleScoreSQL andTimeNotBetween(java.util.Date value1, java.util.Date value2) {
          addCriterion("time", value1, value2, ConditionMode.NOT_BETWEEN, "time", "java.util.Date", "String");
          return this;
      }
        
      public CmsArticleScoreSQL andTimeIn(List<java.util.Date> values) {
          addCriterion("time", values, ConditionMode.IN, "time", "java.util.Date", "String");
          return this;
      }

      public CmsArticleScoreSQL andTimeNotIn(List<java.util.Date> values) {
          addCriterion("time", values, ConditionMode.NOT_IN, "time", "java.util.Date", "String");
          return this;
      }
}