/**
 * Copyright (c) 2015-2017, Henry Yang 杨勇 (gismail@foxmail.com).
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.lambkit.module.cms.sql;

import com.lambkit.db.sql.ConditionMode;
import com.lambkit.db.sql.Column;
import com.lambkit.db.sql.Columns;
import com.lambkit.db.sql.Example;

import java.io.Serializable;
import java.util.List;

/**
 * @author yangyong 
 * @website: www.lambkit.com
 * @email: gismail@foxmail.com
 * @date ${date}
 * @version 1.0
 * @since 1.0
 */

public class CmsMessageSQL extends Columns implements Serializable {
	private static final long serialVersionUID = 1L;
	
	public static CmsMessageSQL of() {
		return new CmsMessageSQL();
	}
	
	public static CmsMessageSQL by(Column column) {
		CmsMessageSQL that = new CmsMessageSQL();
		that.add(column);
        return that;
    }

    public static CmsMessageSQL by(String name, Object value) {
        return (CmsMessageSQL) create().eq(name, value);
    }
    
    public Example example() {
    	return Example.create("cms_message", this);
    }
    
    /**
     * equals
     *
     * @param name
     * @param value
     * @return
     */
    public CmsMessageSQL eq(String name, Object value) {
    	super.eq(name, value);
        return this;
    }

    /**
     * not equals !=
     *
     * @param name
     * @param value
     * @return
     */
    public CmsMessageSQL ne(String name, Object value) {
    	super.ne(name, value);
        return this;
    }


    /**
     * like
     *
     * @param name
     * @param value
     * @return
     */

    public CmsMessageSQL like(String name, Object value) {
    	super.like(name, value);
        return this;
    }
    
    public CmsMessageSQL notLike(String name, Object value) {
    	super.notLike(name, value);
        return this;
    }

    /**
     * 大于 great than
     *
     * @param name
     * @param value
     * @return
     */
    public CmsMessageSQL gt(String name, Object value) {
    	super.gt(name, value);
        return this;
    }

    /**
     * 大于等于 great or equal
     *
     * @param name
     * @param value
     * @return
     */
    public CmsMessageSQL ge(String name, Object value) {
    	super.ge(name, value);
        return this;
    }

    /**
     * 小于 less than
     *
     * @param name
     * @param value
     * @return
     */
    public CmsMessageSQL lt(String name, Object value) {
    	super.lt(name, value);
        return this;
    }

    /**
     * 小于等于 less or equal
     *
     * @param name
     * @param value
     * @return
     */
    public CmsMessageSQL le(String name, Object value) {
    	super.le(name, value);
        return this;
    }
    
    public CmsMessageSQL isnull(String name) {
    	super.isnull(name);
        return this;
    } 

    public CmsMessageSQL notNull(String name) {
    	super.notNull(name);
        return this;
    } 
    
    public CmsMessageSQL empty(String name) {
    	super.empty(name);
        return this;
    } 
    
    public CmsMessageSQL notEmpty(String name) {
    	super.notEmpty(name);
        return this;
    } 
    
    public CmsMessageSQL add(Column column) {
    	super.add(column);
    	return this;
    }
    
    /**************************/
	
	public void addCriterion(String name, Object value, ConditionMode logic, String property, String typeHandler, String valueType) {
		 if (value == null) {
			 throw new RuntimeException("Value for " + property + " cannot be null");
		 }
		 add(Column.create(name, value, logic, typeHandler, valueType));
	}
   
	public void addCriterion(String name, Object value1, Object value2, ConditionMode logic, String property, String typeHandler, String valueType) {
		 if (value1 == null || value2 == null) {
			 throw new RuntimeException("Between values for " + property + " cannot be null");
		 }
		 add(Column.create(name, value1, value2, logic, typeHandler, valueType));
	}
		 
	public CmsMessageSQL andMessageIdIsNull() {
		isnull("message_id");
		return this;
	}
	
	public CmsMessageSQL andMessageIdIsNotNull() {
		notNull("message_id");
		return this;
	}
	
	public CmsMessageSQL andMessageIdIsEmpty() {
		empty("message_id");
		return this;
	}

	public CmsMessageSQL andMessageIdIsNotEmpty() {
		notEmpty("message_id");
		return this;
	}
      public CmsMessageSQL andMessageIdEqualTo(java.lang.Integer value) {
          addCriterion("message_id", value, ConditionMode.EQUAL, "messageId", "java.lang.Integer", "Float");
          return this;
      }

      public CmsMessageSQL andMessageIdNotEqualTo(java.lang.Integer value) {
          addCriterion("message_id", value, ConditionMode.NOT_EQUAL, "messageId", "java.lang.Integer", "Float");
          return this;
      }

      public CmsMessageSQL andMessageIdGreaterThan(java.lang.Integer value) {
          addCriterion("message_id", value, ConditionMode.GREATER_THEN, "messageId", "java.lang.Integer", "Float");
          return this;
      }

      public CmsMessageSQL andMessageIdGreaterThanOrEqualTo(java.lang.Integer value) {
          addCriterion("message_id", value, ConditionMode.GREATER_EQUAL, "messageId", "java.lang.Integer", "Float");
          return this;
      }

      public CmsMessageSQL andMessageIdLessThan(java.lang.Integer value) {
          addCriterion("message_id", value, ConditionMode.LESS_THEN, "messageId", "java.lang.Integer", "Float");
          return this;
      }

      public CmsMessageSQL andMessageIdLessThanOrEqualTo(java.lang.Integer value) {
          addCriterion("message_id", value, ConditionMode.LESS_EQUAL, "messageId", "java.lang.Integer", "Float");
          return this;
      }

      public CmsMessageSQL andMessageIdBetween(java.lang.Integer value1, java.lang.Integer value2) {
    	  addCriterion("message_id", value1, value2, ConditionMode.BETWEEN, "messageId", "java.lang.Integer", "Float");
    	  return this;
      }

      public CmsMessageSQL andMessageIdNotBetween(java.lang.Integer value1, java.lang.Integer value2) {
          addCriterion("message_id", value1, value2, ConditionMode.NOT_BETWEEN, "messageId", "java.lang.Integer", "Float");
          return this;
      }
        
      public CmsMessageSQL andMessageIdIn(List<java.lang.Integer> values) {
          addCriterion("message_id", values, ConditionMode.IN, "messageId", "java.lang.Integer", "Float");
          return this;
      }

      public CmsMessageSQL andMessageIdNotIn(List<java.lang.Integer> values) {
          addCriterion("message_id", values, ConditionMode.NOT_IN, "messageId", "java.lang.Integer", "Float");
          return this;
      }
	public CmsMessageSQL andMessageTitleIsNull() {
		isnull("message_title");
		return this;
	}
	
	public CmsMessageSQL andMessageTitleIsNotNull() {
		notNull("message_title");
		return this;
	}
	
	public CmsMessageSQL andMessageTitleIsEmpty() {
		empty("message_title");
		return this;
	}

	public CmsMessageSQL andMessageTitleIsNotEmpty() {
		notEmpty("message_title");
		return this;
	}
       public CmsMessageSQL andMessageTitleLike(java.lang.String value) {
    	   addCriterion("message_title", value, ConditionMode.FUZZY, "messageTitle", "java.lang.String", "Float");
    	   return this;
      }

      public CmsMessageSQL andMessageTitleNotLike(java.lang.String value) {
          addCriterion("message_title", value, ConditionMode.NOT_FUZZY, "messageTitle", "java.lang.String", "Float");
          return this;
      }
      public CmsMessageSQL andMessageTitleEqualTo(java.lang.String value) {
          addCriterion("message_title", value, ConditionMode.EQUAL, "messageTitle", "java.lang.String", "String");
          return this;
      }

      public CmsMessageSQL andMessageTitleNotEqualTo(java.lang.String value) {
          addCriterion("message_title", value, ConditionMode.NOT_EQUAL, "messageTitle", "java.lang.String", "String");
          return this;
      }

      public CmsMessageSQL andMessageTitleGreaterThan(java.lang.String value) {
          addCriterion("message_title", value, ConditionMode.GREATER_THEN, "messageTitle", "java.lang.String", "String");
          return this;
      }

      public CmsMessageSQL andMessageTitleGreaterThanOrEqualTo(java.lang.String value) {
          addCriterion("message_title", value, ConditionMode.GREATER_EQUAL, "messageTitle", "java.lang.String", "String");
          return this;
      }

      public CmsMessageSQL andMessageTitleLessThan(java.lang.String value) {
          addCriterion("message_title", value, ConditionMode.LESS_THEN, "messageTitle", "java.lang.String", "String");
          return this;
      }

      public CmsMessageSQL andMessageTitleLessThanOrEqualTo(java.lang.String value) {
          addCriterion("message_title", value, ConditionMode.LESS_EQUAL, "messageTitle", "java.lang.String", "String");
          return this;
      }

      public CmsMessageSQL andMessageTitleBetween(java.lang.String value1, java.lang.String value2) {
    	  addCriterion("message_title", value1, value2, ConditionMode.BETWEEN, "messageTitle", "java.lang.String", "String");
    	  return this;
      }

      public CmsMessageSQL andMessageTitleNotBetween(java.lang.String value1, java.lang.String value2) {
          addCriterion("message_title", value1, value2, ConditionMode.NOT_BETWEEN, "messageTitle", "java.lang.String", "String");
          return this;
      }
        
      public CmsMessageSQL andMessageTitleIn(List<java.lang.String> values) {
          addCriterion("message_title", values, ConditionMode.IN, "messageTitle", "java.lang.String", "String");
          return this;
      }

      public CmsMessageSQL andMessageTitleNotIn(List<java.lang.String> values) {
          addCriterion("message_title", values, ConditionMode.NOT_IN, "messageTitle", "java.lang.String", "String");
          return this;
      }
	public CmsMessageSQL andMessageContentIsNull() {
		isnull("message_content");
		return this;
	}
	
	public CmsMessageSQL andMessageContentIsNotNull() {
		notNull("message_content");
		return this;
	}
	
	public CmsMessageSQL andMessageContentIsEmpty() {
		empty("message_content");
		return this;
	}

	public CmsMessageSQL andMessageContentIsNotEmpty() {
		notEmpty("message_content");
		return this;
	}
       public CmsMessageSQL andMessageContentLike(java.lang.String value) {
    	   addCriterion("message_content", value, ConditionMode.FUZZY, "messageContent", "java.lang.String", "String");
    	   return this;
      }

      public CmsMessageSQL andMessageContentNotLike(java.lang.String value) {
          addCriterion("message_content", value, ConditionMode.NOT_FUZZY, "messageContent", "java.lang.String", "String");
          return this;
      }
      public CmsMessageSQL andMessageContentEqualTo(java.lang.String value) {
          addCriterion("message_content", value, ConditionMode.EQUAL, "messageContent", "java.lang.String", "String");
          return this;
      }

      public CmsMessageSQL andMessageContentNotEqualTo(java.lang.String value) {
          addCriterion("message_content", value, ConditionMode.NOT_EQUAL, "messageContent", "java.lang.String", "String");
          return this;
      }

      public CmsMessageSQL andMessageContentGreaterThan(java.lang.String value) {
          addCriterion("message_content", value, ConditionMode.GREATER_THEN, "messageContent", "java.lang.String", "String");
          return this;
      }

      public CmsMessageSQL andMessageContentGreaterThanOrEqualTo(java.lang.String value) {
          addCriterion("message_content", value, ConditionMode.GREATER_EQUAL, "messageContent", "java.lang.String", "String");
          return this;
      }

      public CmsMessageSQL andMessageContentLessThan(java.lang.String value) {
          addCriterion("message_content", value, ConditionMode.LESS_THEN, "messageContent", "java.lang.String", "String");
          return this;
      }

      public CmsMessageSQL andMessageContentLessThanOrEqualTo(java.lang.String value) {
          addCriterion("message_content", value, ConditionMode.LESS_EQUAL, "messageContent", "java.lang.String", "String");
          return this;
      }

      public CmsMessageSQL andMessageContentBetween(java.lang.String value1, java.lang.String value2) {
    	  addCriterion("message_content", value1, value2, ConditionMode.BETWEEN, "messageContent", "java.lang.String", "String");
    	  return this;
      }

      public CmsMessageSQL andMessageContentNotBetween(java.lang.String value1, java.lang.String value2) {
          addCriterion("message_content", value1, value2, ConditionMode.NOT_BETWEEN, "messageContent", "java.lang.String", "String");
          return this;
      }
        
      public CmsMessageSQL andMessageContentIn(List<java.lang.String> values) {
          addCriterion("message_content", values, ConditionMode.IN, "messageContent", "java.lang.String", "String");
          return this;
      }

      public CmsMessageSQL andMessageContentNotIn(List<java.lang.String> values) {
          addCriterion("message_content", values, ConditionMode.NOT_IN, "messageContent", "java.lang.String", "String");
          return this;
      }
	public CmsMessageSQL andMessageToUserIdIsNull() {
		isnull("message_to_user_id");
		return this;
	}
	
	public CmsMessageSQL andMessageToUserIdIsNotNull() {
		notNull("message_to_user_id");
		return this;
	}
	
	public CmsMessageSQL andMessageToUserIdIsEmpty() {
		empty("message_to_user_id");
		return this;
	}

	public CmsMessageSQL andMessageToUserIdIsNotEmpty() {
		notEmpty("message_to_user_id");
		return this;
	}
      public CmsMessageSQL andMessageToUserIdEqualTo(java.lang.Long value) {
          addCriterion("message_to_user_id", value, ConditionMode.EQUAL, "messageToUserId", "java.lang.Long", "Float");
          return this;
      }

      public CmsMessageSQL andMessageToUserIdNotEqualTo(java.lang.Long value) {
          addCriterion("message_to_user_id", value, ConditionMode.NOT_EQUAL, "messageToUserId", "java.lang.Long", "Float");
          return this;
      }

      public CmsMessageSQL andMessageToUserIdGreaterThan(java.lang.Long value) {
          addCriterion("message_to_user_id", value, ConditionMode.GREATER_THEN, "messageToUserId", "java.lang.Long", "Float");
          return this;
      }

      public CmsMessageSQL andMessageToUserIdGreaterThanOrEqualTo(java.lang.Long value) {
          addCriterion("message_to_user_id", value, ConditionMode.GREATER_EQUAL, "messageToUserId", "java.lang.Long", "Float");
          return this;
      }

      public CmsMessageSQL andMessageToUserIdLessThan(java.lang.Long value) {
          addCriterion("message_to_user_id", value, ConditionMode.LESS_THEN, "messageToUserId", "java.lang.Long", "Float");
          return this;
      }

      public CmsMessageSQL andMessageToUserIdLessThanOrEqualTo(java.lang.Long value) {
          addCriterion("message_to_user_id", value, ConditionMode.LESS_EQUAL, "messageToUserId", "java.lang.Long", "Float");
          return this;
      }

      public CmsMessageSQL andMessageToUserIdBetween(java.lang.Long value1, java.lang.Long value2) {
    	  addCriterion("message_to_user_id", value1, value2, ConditionMode.BETWEEN, "messageToUserId", "java.lang.Long", "Float");
    	  return this;
      }

      public CmsMessageSQL andMessageToUserIdNotBetween(java.lang.Long value1, java.lang.Long value2) {
          addCriterion("message_to_user_id", value1, value2, ConditionMode.NOT_BETWEEN, "messageToUserId", "java.lang.Long", "Float");
          return this;
      }
        
      public CmsMessageSQL andMessageToUserIdIn(List<java.lang.Long> values) {
          addCriterion("message_to_user_id", values, ConditionMode.IN, "messageToUserId", "java.lang.Long", "Float");
          return this;
      }

      public CmsMessageSQL andMessageToUserIdNotIn(List<java.lang.Long> values) {
          addCriterion("message_to_user_id", values, ConditionMode.NOT_IN, "messageToUserId", "java.lang.Long", "Float");
          return this;
      }
	public CmsMessageSQL andMessageIsReadIsNull() {
		isnull("message_is_read");
		return this;
	}
	
	public CmsMessageSQL andMessageIsReadIsNotNull() {
		notNull("message_is_read");
		return this;
	}
	
	public CmsMessageSQL andMessageIsReadIsEmpty() {
		empty("message_is_read");
		return this;
	}

	public CmsMessageSQL andMessageIsReadIsNotEmpty() {
		notEmpty("message_is_read");
		return this;
	}
      public CmsMessageSQL andMessageIsReadEqualTo(java.lang.Integer value) {
          addCriterion("message_is_read", value, ConditionMode.EQUAL, "messageIsRead", "java.lang.Integer", "Float");
          return this;
      }

      public CmsMessageSQL andMessageIsReadNotEqualTo(java.lang.Integer value) {
          addCriterion("message_is_read", value, ConditionMode.NOT_EQUAL, "messageIsRead", "java.lang.Integer", "Float");
          return this;
      }

      public CmsMessageSQL andMessageIsReadGreaterThan(java.lang.Integer value) {
          addCriterion("message_is_read", value, ConditionMode.GREATER_THEN, "messageIsRead", "java.lang.Integer", "Float");
          return this;
      }

      public CmsMessageSQL andMessageIsReadGreaterThanOrEqualTo(java.lang.Integer value) {
          addCriterion("message_is_read", value, ConditionMode.GREATER_EQUAL, "messageIsRead", "java.lang.Integer", "Float");
          return this;
      }

      public CmsMessageSQL andMessageIsReadLessThan(java.lang.Integer value) {
          addCriterion("message_is_read", value, ConditionMode.LESS_THEN, "messageIsRead", "java.lang.Integer", "Float");
          return this;
      }

      public CmsMessageSQL andMessageIsReadLessThanOrEqualTo(java.lang.Integer value) {
          addCriterion("message_is_read", value, ConditionMode.LESS_EQUAL, "messageIsRead", "java.lang.Integer", "Float");
          return this;
      }

      public CmsMessageSQL andMessageIsReadBetween(java.lang.Integer value1, java.lang.Integer value2) {
    	  addCriterion("message_is_read", value1, value2, ConditionMode.BETWEEN, "messageIsRead", "java.lang.Integer", "Float");
    	  return this;
      }

      public CmsMessageSQL andMessageIsReadNotBetween(java.lang.Integer value1, java.lang.Integer value2) {
          addCriterion("message_is_read", value1, value2, ConditionMode.NOT_BETWEEN, "messageIsRead", "java.lang.Integer", "Float");
          return this;
      }
        
      public CmsMessageSQL andMessageIsReadIn(List<java.lang.Integer> values) {
          addCriterion("message_is_read", values, ConditionMode.IN, "messageIsRead", "java.lang.Integer", "Float");
          return this;
      }

      public CmsMessageSQL andMessageIsReadNotIn(List<java.lang.Integer> values) {
          addCriterion("message_is_read", values, ConditionMode.NOT_IN, "messageIsRead", "java.lang.Integer", "Float");
          return this;
      }
	public CmsMessageSQL andMessageTimeIsNull() {
		isnull("message_time");
		return this;
	}
	
	public CmsMessageSQL andMessageTimeIsNotNull() {
		notNull("message_time");
		return this;
	}
	
	public CmsMessageSQL andMessageTimeIsEmpty() {
		empty("message_time");
		return this;
	}

	public CmsMessageSQL andMessageTimeIsNotEmpty() {
		notEmpty("message_time");
		return this;
	}
      public CmsMessageSQL andMessageTimeEqualTo(java.util.Date value) {
          addCriterion("message_time", value, ConditionMode.EQUAL, "messageTime", "java.util.Date", "String");
          return this;
      }

      public CmsMessageSQL andMessageTimeNotEqualTo(java.util.Date value) {
          addCriterion("message_time", value, ConditionMode.NOT_EQUAL, "messageTime", "java.util.Date", "String");
          return this;
      }

      public CmsMessageSQL andMessageTimeGreaterThan(java.util.Date value) {
          addCriterion("message_time", value, ConditionMode.GREATER_THEN, "messageTime", "java.util.Date", "String");
          return this;
      }

      public CmsMessageSQL andMessageTimeGreaterThanOrEqualTo(java.util.Date value) {
          addCriterion("message_time", value, ConditionMode.GREATER_EQUAL, "messageTime", "java.util.Date", "String");
          return this;
      }

      public CmsMessageSQL andMessageTimeLessThan(java.util.Date value) {
          addCriterion("message_time", value, ConditionMode.LESS_THEN, "messageTime", "java.util.Date", "String");
          return this;
      }

      public CmsMessageSQL andMessageTimeLessThanOrEqualTo(java.util.Date value) {
          addCriterion("message_time", value, ConditionMode.LESS_EQUAL, "messageTime", "java.util.Date", "String");
          return this;
      }

      public CmsMessageSQL andMessageTimeBetween(java.util.Date value1, java.util.Date value2) {
    	  addCriterion("message_time", value1, value2, ConditionMode.BETWEEN, "messageTime", "java.util.Date", "String");
    	  return this;
      }

      public CmsMessageSQL andMessageTimeNotBetween(java.util.Date value1, java.util.Date value2) {
          addCriterion("message_time", value1, value2, ConditionMode.NOT_BETWEEN, "messageTime", "java.util.Date", "String");
          return this;
      }
        
      public CmsMessageSQL andMessageTimeIn(List<java.util.Date> values) {
          addCriterion("message_time", values, ConditionMode.IN, "messageTime", "java.util.Date", "String");
          return this;
      }

      public CmsMessageSQL andMessageTimeNotIn(List<java.util.Date> values) {
          addCriterion("message_time", values, ConditionMode.NOT_IN, "messageTime", "java.util.Date", "String");
          return this;
      }
	public CmsMessageSQL andMessageFromUserIdIsNull() {
		isnull("message_from_user_id");
		return this;
	}
	
	public CmsMessageSQL andMessageFromUserIdIsNotNull() {
		notNull("message_from_user_id");
		return this;
	}
	
	public CmsMessageSQL andMessageFromUserIdIsEmpty() {
		empty("message_from_user_id");
		return this;
	}

	public CmsMessageSQL andMessageFromUserIdIsNotEmpty() {
		notEmpty("message_from_user_id");
		return this;
	}
      public CmsMessageSQL andMessageFromUserIdEqualTo(java.lang.Long value) {
          addCriterion("message_from_user_id", value, ConditionMode.EQUAL, "messageFromUserId", "java.lang.Long", "Float");
          return this;
      }

      public CmsMessageSQL andMessageFromUserIdNotEqualTo(java.lang.Long value) {
          addCriterion("message_from_user_id", value, ConditionMode.NOT_EQUAL, "messageFromUserId", "java.lang.Long", "Float");
          return this;
      }

      public CmsMessageSQL andMessageFromUserIdGreaterThan(java.lang.Long value) {
          addCriterion("message_from_user_id", value, ConditionMode.GREATER_THEN, "messageFromUserId", "java.lang.Long", "Float");
          return this;
      }

      public CmsMessageSQL andMessageFromUserIdGreaterThanOrEqualTo(java.lang.Long value) {
          addCriterion("message_from_user_id", value, ConditionMode.GREATER_EQUAL, "messageFromUserId", "java.lang.Long", "Float");
          return this;
      }

      public CmsMessageSQL andMessageFromUserIdLessThan(java.lang.Long value) {
          addCriterion("message_from_user_id", value, ConditionMode.LESS_THEN, "messageFromUserId", "java.lang.Long", "Float");
          return this;
      }

      public CmsMessageSQL andMessageFromUserIdLessThanOrEqualTo(java.lang.Long value) {
          addCriterion("message_from_user_id", value, ConditionMode.LESS_EQUAL, "messageFromUserId", "java.lang.Long", "Float");
          return this;
      }

      public CmsMessageSQL andMessageFromUserIdBetween(java.lang.Long value1, java.lang.Long value2) {
    	  addCriterion("message_from_user_id", value1, value2, ConditionMode.BETWEEN, "messageFromUserId", "java.lang.Long", "Float");
    	  return this;
      }

      public CmsMessageSQL andMessageFromUserIdNotBetween(java.lang.Long value1, java.lang.Long value2) {
          addCriterion("message_from_user_id", value1, value2, ConditionMode.NOT_BETWEEN, "messageFromUserId", "java.lang.Long", "Float");
          return this;
      }
        
      public CmsMessageSQL andMessageFromUserIdIn(List<java.lang.Long> values) {
          addCriterion("message_from_user_id", values, ConditionMode.IN, "messageFromUserId", "java.lang.Long", "Float");
          return this;
      }

      public CmsMessageSQL andMessageFromUserIdNotIn(List<java.lang.Long> values) {
          addCriterion("message_from_user_id", values, ConditionMode.NOT_IN, "messageFromUserId", "java.lang.Long", "Float");
          return this;
      }
	public CmsMessageSQL andMessageRefTypeIsNull() {
		isnull("message_ref_type");
		return this;
	}
	
	public CmsMessageSQL andMessageRefTypeIsNotNull() {
		notNull("message_ref_type");
		return this;
	}
	
	public CmsMessageSQL andMessageRefTypeIsEmpty() {
		empty("message_ref_type");
		return this;
	}

	public CmsMessageSQL andMessageRefTypeIsNotEmpty() {
		notEmpty("message_ref_type");
		return this;
	}
       public CmsMessageSQL andMessageRefTypeLike(java.lang.String value) {
    	   addCriterion("message_ref_type", value, ConditionMode.FUZZY, "messageRefType", "java.lang.String", "Float");
    	   return this;
      }

      public CmsMessageSQL andMessageRefTypeNotLike(java.lang.String value) {
          addCriterion("message_ref_type", value, ConditionMode.NOT_FUZZY, "messageRefType", "java.lang.String", "Float");
          return this;
      }
      public CmsMessageSQL andMessageRefTypeEqualTo(java.lang.String value) {
          addCriterion("message_ref_type", value, ConditionMode.EQUAL, "messageRefType", "java.lang.String", "String");
          return this;
      }

      public CmsMessageSQL andMessageRefTypeNotEqualTo(java.lang.String value) {
          addCriterion("message_ref_type", value, ConditionMode.NOT_EQUAL, "messageRefType", "java.lang.String", "String");
          return this;
      }

      public CmsMessageSQL andMessageRefTypeGreaterThan(java.lang.String value) {
          addCriterion("message_ref_type", value, ConditionMode.GREATER_THEN, "messageRefType", "java.lang.String", "String");
          return this;
      }

      public CmsMessageSQL andMessageRefTypeGreaterThanOrEqualTo(java.lang.String value) {
          addCriterion("message_ref_type", value, ConditionMode.GREATER_EQUAL, "messageRefType", "java.lang.String", "String");
          return this;
      }

      public CmsMessageSQL andMessageRefTypeLessThan(java.lang.String value) {
          addCriterion("message_ref_type", value, ConditionMode.LESS_THEN, "messageRefType", "java.lang.String", "String");
          return this;
      }

      public CmsMessageSQL andMessageRefTypeLessThanOrEqualTo(java.lang.String value) {
          addCriterion("message_ref_type", value, ConditionMode.LESS_EQUAL, "messageRefType", "java.lang.String", "String");
          return this;
      }

      public CmsMessageSQL andMessageRefTypeBetween(java.lang.String value1, java.lang.String value2) {
    	  addCriterion("message_ref_type", value1, value2, ConditionMode.BETWEEN, "messageRefType", "java.lang.String", "String");
    	  return this;
      }

      public CmsMessageSQL andMessageRefTypeNotBetween(java.lang.String value1, java.lang.String value2) {
          addCriterion("message_ref_type", value1, value2, ConditionMode.NOT_BETWEEN, "messageRefType", "java.lang.String", "String");
          return this;
      }
        
      public CmsMessageSQL andMessageRefTypeIn(List<java.lang.String> values) {
          addCriterion("message_ref_type", values, ConditionMode.IN, "messageRefType", "java.lang.String", "String");
          return this;
      }

      public CmsMessageSQL andMessageRefTypeNotIn(List<java.lang.String> values) {
          addCriterion("message_ref_type", values, ConditionMode.NOT_IN, "messageRefType", "java.lang.String", "String");
          return this;
      }
	public CmsMessageSQL andMessageRefIdIsNull() {
		isnull("message_ref_id");
		return this;
	}
	
	public CmsMessageSQL andMessageRefIdIsNotNull() {
		notNull("message_ref_id");
		return this;
	}
	
	public CmsMessageSQL andMessageRefIdIsEmpty() {
		empty("message_ref_id");
		return this;
	}

	public CmsMessageSQL andMessageRefIdIsNotEmpty() {
		notEmpty("message_ref_id");
		return this;
	}
      public CmsMessageSQL andMessageRefIdEqualTo(java.lang.Long value) {
          addCriterion("message_ref_id", value, ConditionMode.EQUAL, "messageRefId", "java.lang.Long", "Float");
          return this;
      }

      public CmsMessageSQL andMessageRefIdNotEqualTo(java.lang.Long value) {
          addCriterion("message_ref_id", value, ConditionMode.NOT_EQUAL, "messageRefId", "java.lang.Long", "Float");
          return this;
      }

      public CmsMessageSQL andMessageRefIdGreaterThan(java.lang.Long value) {
          addCriterion("message_ref_id", value, ConditionMode.GREATER_THEN, "messageRefId", "java.lang.Long", "Float");
          return this;
      }

      public CmsMessageSQL andMessageRefIdGreaterThanOrEqualTo(java.lang.Long value) {
          addCriterion("message_ref_id", value, ConditionMode.GREATER_EQUAL, "messageRefId", "java.lang.Long", "Float");
          return this;
      }

      public CmsMessageSQL andMessageRefIdLessThan(java.lang.Long value) {
          addCriterion("message_ref_id", value, ConditionMode.LESS_THEN, "messageRefId", "java.lang.Long", "Float");
          return this;
      }

      public CmsMessageSQL andMessageRefIdLessThanOrEqualTo(java.lang.Long value) {
          addCriterion("message_ref_id", value, ConditionMode.LESS_EQUAL, "messageRefId", "java.lang.Long", "Float");
          return this;
      }

      public CmsMessageSQL andMessageRefIdBetween(java.lang.Long value1, java.lang.Long value2) {
    	  addCriterion("message_ref_id", value1, value2, ConditionMode.BETWEEN, "messageRefId", "java.lang.Long", "Float");
    	  return this;
      }

      public CmsMessageSQL andMessageRefIdNotBetween(java.lang.Long value1, java.lang.Long value2) {
          addCriterion("message_ref_id", value1, value2, ConditionMode.NOT_BETWEEN, "messageRefId", "java.lang.Long", "Float");
          return this;
      }
        
      public CmsMessageSQL andMessageRefIdIn(List<java.lang.Long> values) {
          addCriterion("message_ref_id", values, ConditionMode.IN, "messageRefId", "java.lang.Long", "Float");
          return this;
      }

      public CmsMessageSQL andMessageRefIdNotIn(List<java.lang.Long> values) {
          addCriterion("message_ref_id", values, ConditionMode.NOT_IN, "messageRefId", "java.lang.Long", "Float");
          return this;
      }
	public CmsMessageSQL andMessageFromUserPhoneIsNull() {
		isnull("message_from_user_phone");
		return this;
	}
	
	public CmsMessageSQL andMessageFromUserPhoneIsNotNull() {
		notNull("message_from_user_phone");
		return this;
	}
	
	public CmsMessageSQL andMessageFromUserPhoneIsEmpty() {
		empty("message_from_user_phone");
		return this;
	}

	public CmsMessageSQL andMessageFromUserPhoneIsNotEmpty() {
		notEmpty("message_from_user_phone");
		return this;
	}
       public CmsMessageSQL andMessageFromUserPhoneLike(java.lang.String value) {
    	   addCriterion("message_from_user_phone", value, ConditionMode.FUZZY, "messageFromUserPhone", "java.lang.String", "Float");
    	   return this;
      }

      public CmsMessageSQL andMessageFromUserPhoneNotLike(java.lang.String value) {
          addCriterion("message_from_user_phone", value, ConditionMode.NOT_FUZZY, "messageFromUserPhone", "java.lang.String", "Float");
          return this;
      }
      public CmsMessageSQL andMessageFromUserPhoneEqualTo(java.lang.String value) {
          addCriterion("message_from_user_phone", value, ConditionMode.EQUAL, "messageFromUserPhone", "java.lang.String", "String");
          return this;
      }

      public CmsMessageSQL andMessageFromUserPhoneNotEqualTo(java.lang.String value) {
          addCriterion("message_from_user_phone", value, ConditionMode.NOT_EQUAL, "messageFromUserPhone", "java.lang.String", "String");
          return this;
      }

      public CmsMessageSQL andMessageFromUserPhoneGreaterThan(java.lang.String value) {
          addCriterion("message_from_user_phone", value, ConditionMode.GREATER_THEN, "messageFromUserPhone", "java.lang.String", "String");
          return this;
      }

      public CmsMessageSQL andMessageFromUserPhoneGreaterThanOrEqualTo(java.lang.String value) {
          addCriterion("message_from_user_phone", value, ConditionMode.GREATER_EQUAL, "messageFromUserPhone", "java.lang.String", "String");
          return this;
      }

      public CmsMessageSQL andMessageFromUserPhoneLessThan(java.lang.String value) {
          addCriterion("message_from_user_phone", value, ConditionMode.LESS_THEN, "messageFromUserPhone", "java.lang.String", "String");
          return this;
      }

      public CmsMessageSQL andMessageFromUserPhoneLessThanOrEqualTo(java.lang.String value) {
          addCriterion("message_from_user_phone", value, ConditionMode.LESS_EQUAL, "messageFromUserPhone", "java.lang.String", "String");
          return this;
      }

      public CmsMessageSQL andMessageFromUserPhoneBetween(java.lang.String value1, java.lang.String value2) {
    	  addCriterion("message_from_user_phone", value1, value2, ConditionMode.BETWEEN, "messageFromUserPhone", "java.lang.String", "String");
    	  return this;
      }

      public CmsMessageSQL andMessageFromUserPhoneNotBetween(java.lang.String value1, java.lang.String value2) {
          addCriterion("message_from_user_phone", value1, value2, ConditionMode.NOT_BETWEEN, "messageFromUserPhone", "java.lang.String", "String");
          return this;
      }
        
      public CmsMessageSQL andMessageFromUserPhoneIn(List<java.lang.String> values) {
          addCriterion("message_from_user_phone", values, ConditionMode.IN, "messageFromUserPhone", "java.lang.String", "String");
          return this;
      }

      public CmsMessageSQL andMessageFromUserPhoneNotIn(List<java.lang.String> values) {
          addCriterion("message_from_user_phone", values, ConditionMode.NOT_IN, "messageFromUserPhone", "java.lang.String", "String");
          return this;
      }
	public CmsMessageSQL andMessageFromUserNameIsNull() {
		isnull("message_from_user_name");
		return this;
	}
	
	public CmsMessageSQL andMessageFromUserNameIsNotNull() {
		notNull("message_from_user_name");
		return this;
	}
	
	public CmsMessageSQL andMessageFromUserNameIsEmpty() {
		empty("message_from_user_name");
		return this;
	}

	public CmsMessageSQL andMessageFromUserNameIsNotEmpty() {
		notEmpty("message_from_user_name");
		return this;
	}
       public CmsMessageSQL andMessageFromUserNameLike(java.lang.String value) {
    	   addCriterion("message_from_user_name", value, ConditionMode.FUZZY, "messageFromUserName", "java.lang.String", "String");
    	   return this;
      }

      public CmsMessageSQL andMessageFromUserNameNotLike(java.lang.String value) {
          addCriterion("message_from_user_name", value, ConditionMode.NOT_FUZZY, "messageFromUserName", "java.lang.String", "String");
          return this;
      }
      public CmsMessageSQL andMessageFromUserNameEqualTo(java.lang.String value) {
          addCriterion("message_from_user_name", value, ConditionMode.EQUAL, "messageFromUserName", "java.lang.String", "String");
          return this;
      }

      public CmsMessageSQL andMessageFromUserNameNotEqualTo(java.lang.String value) {
          addCriterion("message_from_user_name", value, ConditionMode.NOT_EQUAL, "messageFromUserName", "java.lang.String", "String");
          return this;
      }

      public CmsMessageSQL andMessageFromUserNameGreaterThan(java.lang.String value) {
          addCriterion("message_from_user_name", value, ConditionMode.GREATER_THEN, "messageFromUserName", "java.lang.String", "String");
          return this;
      }

      public CmsMessageSQL andMessageFromUserNameGreaterThanOrEqualTo(java.lang.String value) {
          addCriterion("message_from_user_name", value, ConditionMode.GREATER_EQUAL, "messageFromUserName", "java.lang.String", "String");
          return this;
      }

      public CmsMessageSQL andMessageFromUserNameLessThan(java.lang.String value) {
          addCriterion("message_from_user_name", value, ConditionMode.LESS_THEN, "messageFromUserName", "java.lang.String", "String");
          return this;
      }

      public CmsMessageSQL andMessageFromUserNameLessThanOrEqualTo(java.lang.String value) {
          addCriterion("message_from_user_name", value, ConditionMode.LESS_EQUAL, "messageFromUserName", "java.lang.String", "String");
          return this;
      }

      public CmsMessageSQL andMessageFromUserNameBetween(java.lang.String value1, java.lang.String value2) {
    	  addCriterion("message_from_user_name", value1, value2, ConditionMode.BETWEEN, "messageFromUserName", "java.lang.String", "String");
    	  return this;
      }

      public CmsMessageSQL andMessageFromUserNameNotBetween(java.lang.String value1, java.lang.String value2) {
          addCriterion("message_from_user_name", value1, value2, ConditionMode.NOT_BETWEEN, "messageFromUserName", "java.lang.String", "String");
          return this;
      }
        
      public CmsMessageSQL andMessageFromUserNameIn(List<java.lang.String> values) {
          addCriterion("message_from_user_name", values, ConditionMode.IN, "messageFromUserName", "java.lang.String", "String");
          return this;
      }

      public CmsMessageSQL andMessageFromUserNameNotIn(List<java.lang.String> values) {
          addCriterion("message_from_user_name", values, ConditionMode.NOT_IN, "messageFromUserName", "java.lang.String", "String");
          return this;
      }
}