/**
 * Copyright (c) 2015-2017, Henry Yang 杨勇 (gismail@foxmail.com).
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.lambkit.module.cms.row;

import com.lambkit.db.RowModel;

/**
 * @author yangyong 
 * @website: www.lambkit.com
 * @email: gismail@foxmail.com
 * @date ${date}
 * @version 1.0
 * @since 1.0
 */

public class CmsArticleScore extends RowModel<CmsArticleScore> {
	public CmsArticleScore() {
		setTableName("cms_article_score");
		setPrimaryKey("score_id");
	}
	public java.lang.Long getScoreId() {
		return this.get("score_id");
	}
	public void setScoreId(java.lang.Long scoreId) {
		this.set("score_id", scoreId);
	}
	public java.lang.Long getUserId() {
		return this.get("user_id");
	}
	public void setUserId(java.lang.Long userId) {
		this.set("user_id", userId);
	}
	public java.lang.Long getItemId() {
		return this.get("item_id");
	}
	public void setItemId(java.lang.Long itemId) {
		this.set("item_id", itemId);
	}
	public java.lang.Integer getScore() {
		return this.get("score");
	}
	public void setScore(java.lang.Integer score) {
		this.set("score", score);
	}
	public java.util.Date getTime() {
		return this.get("time");
	}
	public void setTime(java.util.Date time) {
		this.set("time", time);
	}
}
