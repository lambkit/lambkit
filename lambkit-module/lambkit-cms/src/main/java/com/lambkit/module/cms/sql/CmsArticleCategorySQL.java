/**
 * Copyright (c) 2015-2017, Henry Yang 杨勇 (gismail@foxmail.com).
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.lambkit.module.cms.sql;

import com.lambkit.db.sql.ConditionMode;
import com.lambkit.db.sql.Column;
import com.lambkit.db.sql.Columns;
import com.lambkit.db.sql.Example;

import java.io.Serializable;
import java.util.List;

/**
 * @author yangyong 
 * @website: www.lambkit.com
 * @email: gismail@foxmail.com
 * @date ${date}
 * @version 1.0
 * @since 1.0
 */

public class CmsArticleCategorySQL extends Columns implements Serializable {
	private static final long serialVersionUID = 1L;
	
	public static CmsArticleCategorySQL of() {
		return new CmsArticleCategorySQL();
	}
	
	public static CmsArticleCategorySQL by(Column column) {
		CmsArticleCategorySQL that = new CmsArticleCategorySQL();
		that.add(column);
        return that;
    }

    public static CmsArticleCategorySQL by(String name, Object value) {
        return (CmsArticleCategorySQL) create().eq(name, value);
    }
    
    public Example example() {
    	return Example.create("cms_article_category", this);
    }
    
    /**
     * equals
     *
     * @param name
     * @param value
     * @return
     */
    public CmsArticleCategorySQL eq(String name, Object value) {
    	super.eq(name, value);
        return this;
    }

    /**
     * not equals !=
     *
     * @param name
     * @param value
     * @return
     */
    public CmsArticleCategorySQL ne(String name, Object value) {
    	super.ne(name, value);
        return this;
    }


    /**
     * like
     *
     * @param name
     * @param value
     * @return
     */

    public CmsArticleCategorySQL like(String name, Object value) {
    	super.like(name, value);
        return this;
    }
    
    public CmsArticleCategorySQL notLike(String name, Object value) {
    	super.notLike(name, value);
        return this;
    }

    /**
     * 大于 great than
     *
     * @param name
     * @param value
     * @return
     */
    public CmsArticleCategorySQL gt(String name, Object value) {
    	super.gt(name, value);
        return this;
    }

    /**
     * 大于等于 great or equal
     *
     * @param name
     * @param value
     * @return
     */
    public CmsArticleCategorySQL ge(String name, Object value) {
    	super.ge(name, value);
        return this;
    }

    /**
     * 小于 less than
     *
     * @param name
     * @param value
     * @return
     */
    public CmsArticleCategorySQL lt(String name, Object value) {
    	super.lt(name, value);
        return this;
    }

    /**
     * 小于等于 less or equal
     *
     * @param name
     * @param value
     * @return
     */
    public CmsArticleCategorySQL le(String name, Object value) {
    	super.le(name, value);
        return this;
    }
    
    public CmsArticleCategorySQL isnull(String name) {
    	super.isnull(name);
        return this;
    } 

    public CmsArticleCategorySQL notNull(String name) {
    	super.notNull(name);
        return this;
    } 
    
    public CmsArticleCategorySQL empty(String name) {
    	super.empty(name);
        return this;
    } 
    
    public CmsArticleCategorySQL notEmpty(String name) {
    	super.notEmpty(name);
        return this;
    } 
    
    public CmsArticleCategorySQL add(Column column) {
    	super.add(column);
    	return this;
    }
    
    /**************************/
	
	public void addCriterion(String name, Object value, ConditionMode logic, String property, String typeHandler, String valueType) {
		 if (value == null) {
			 throw new RuntimeException("Value for " + property + " cannot be null");
		 }
		 add(Column.create(name, value, logic, typeHandler, valueType));
	}
   
	public void addCriterion(String name, Object value1, Object value2, ConditionMode logic, String property, String typeHandler, String valueType) {
		 if (value1 == null || value2 == null) {
			 throw new RuntimeException("Between values for " + property + " cannot be null");
		 }
		 add(Column.create(name, value1, value2, logic, typeHandler, valueType));
	}
		 
	public CmsArticleCategorySQL andArticleCategoryIdIsNull() {
		isnull("article_category_id");
		return this;
	}
	
	public CmsArticleCategorySQL andArticleCategoryIdIsNotNull() {
		notNull("article_category_id");
		return this;
	}
	
	public CmsArticleCategorySQL andArticleCategoryIdIsEmpty() {
		empty("article_category_id");
		return this;
	}

	public CmsArticleCategorySQL andArticleCategoryIdIsNotEmpty() {
		notEmpty("article_category_id");
		return this;
	}
      public CmsArticleCategorySQL andArticleCategoryIdEqualTo(java.lang.Long value) {
          addCriterion("article_category_id", value, ConditionMode.EQUAL, "articleCategoryId", "java.lang.Long", "Float");
          return this;
      }

      public CmsArticleCategorySQL andArticleCategoryIdNotEqualTo(java.lang.Long value) {
          addCriterion("article_category_id", value, ConditionMode.NOT_EQUAL, "articleCategoryId", "java.lang.Long", "Float");
          return this;
      }

      public CmsArticleCategorySQL andArticleCategoryIdGreaterThan(java.lang.Long value) {
          addCriterion("article_category_id", value, ConditionMode.GREATER_THEN, "articleCategoryId", "java.lang.Long", "Float");
          return this;
      }

      public CmsArticleCategorySQL andArticleCategoryIdGreaterThanOrEqualTo(java.lang.Long value) {
          addCriterion("article_category_id", value, ConditionMode.GREATER_EQUAL, "articleCategoryId", "java.lang.Long", "Float");
          return this;
      }

      public CmsArticleCategorySQL andArticleCategoryIdLessThan(java.lang.Long value) {
          addCriterion("article_category_id", value, ConditionMode.LESS_THEN, "articleCategoryId", "java.lang.Long", "Float");
          return this;
      }

      public CmsArticleCategorySQL andArticleCategoryIdLessThanOrEqualTo(java.lang.Long value) {
          addCriterion("article_category_id", value, ConditionMode.LESS_EQUAL, "articleCategoryId", "java.lang.Long", "Float");
          return this;
      }

      public CmsArticleCategorySQL andArticleCategoryIdBetween(java.lang.Long value1, java.lang.Long value2) {
    	  addCriterion("article_category_id", value1, value2, ConditionMode.BETWEEN, "articleCategoryId", "java.lang.Long", "Float");
    	  return this;
      }

      public CmsArticleCategorySQL andArticleCategoryIdNotBetween(java.lang.Long value1, java.lang.Long value2) {
          addCriterion("article_category_id", value1, value2, ConditionMode.NOT_BETWEEN, "articleCategoryId", "java.lang.Long", "Float");
          return this;
      }
        
      public CmsArticleCategorySQL andArticleCategoryIdIn(List<java.lang.Long> values) {
          addCriterion("article_category_id", values, ConditionMode.IN, "articleCategoryId", "java.lang.Long", "Float");
          return this;
      }

      public CmsArticleCategorySQL andArticleCategoryIdNotIn(List<java.lang.Long> values) {
          addCriterion("article_category_id", values, ConditionMode.NOT_IN, "articleCategoryId", "java.lang.Long", "Float");
          return this;
      }
	public CmsArticleCategorySQL andArticleIdIsNull() {
		isnull("article_id");
		return this;
	}
	
	public CmsArticleCategorySQL andArticleIdIsNotNull() {
		notNull("article_id");
		return this;
	}
	
	public CmsArticleCategorySQL andArticleIdIsEmpty() {
		empty("article_id");
		return this;
	}

	public CmsArticleCategorySQL andArticleIdIsNotEmpty() {
		notEmpty("article_id");
		return this;
	}
      public CmsArticleCategorySQL andArticleIdEqualTo(java.lang.Long value) {
          addCriterion("article_id", value, ConditionMode.EQUAL, "articleId", "java.lang.Long", "Float");
          return this;
      }

      public CmsArticleCategorySQL andArticleIdNotEqualTo(java.lang.Long value) {
          addCriterion("article_id", value, ConditionMode.NOT_EQUAL, "articleId", "java.lang.Long", "Float");
          return this;
      }

      public CmsArticleCategorySQL andArticleIdGreaterThan(java.lang.Long value) {
          addCriterion("article_id", value, ConditionMode.GREATER_THEN, "articleId", "java.lang.Long", "Float");
          return this;
      }

      public CmsArticleCategorySQL andArticleIdGreaterThanOrEqualTo(java.lang.Long value) {
          addCriterion("article_id", value, ConditionMode.GREATER_EQUAL, "articleId", "java.lang.Long", "Float");
          return this;
      }

      public CmsArticleCategorySQL andArticleIdLessThan(java.lang.Long value) {
          addCriterion("article_id", value, ConditionMode.LESS_THEN, "articleId", "java.lang.Long", "Float");
          return this;
      }

      public CmsArticleCategorySQL andArticleIdLessThanOrEqualTo(java.lang.Long value) {
          addCriterion("article_id", value, ConditionMode.LESS_EQUAL, "articleId", "java.lang.Long", "Float");
          return this;
      }

      public CmsArticleCategorySQL andArticleIdBetween(java.lang.Long value1, java.lang.Long value2) {
    	  addCriterion("article_id", value1, value2, ConditionMode.BETWEEN, "articleId", "java.lang.Long", "Float");
    	  return this;
      }

      public CmsArticleCategorySQL andArticleIdNotBetween(java.lang.Long value1, java.lang.Long value2) {
          addCriterion("article_id", value1, value2, ConditionMode.NOT_BETWEEN, "articleId", "java.lang.Long", "Float");
          return this;
      }
        
      public CmsArticleCategorySQL andArticleIdIn(List<java.lang.Long> values) {
          addCriterion("article_id", values, ConditionMode.IN, "articleId", "java.lang.Long", "Float");
          return this;
      }

      public CmsArticleCategorySQL andArticleIdNotIn(List<java.lang.Long> values) {
          addCriterion("article_id", values, ConditionMode.NOT_IN, "articleId", "java.lang.Long", "Float");
          return this;
      }
	public CmsArticleCategorySQL andCategoryIdIsNull() {
		isnull("category_id");
		return this;
	}
	
	public CmsArticleCategorySQL andCategoryIdIsNotNull() {
		notNull("category_id");
		return this;
	}
	
	public CmsArticleCategorySQL andCategoryIdIsEmpty() {
		empty("category_id");
		return this;
	}

	public CmsArticleCategorySQL andCategoryIdIsNotEmpty() {
		notEmpty("category_id");
		return this;
	}
      public CmsArticleCategorySQL andCategoryIdEqualTo(java.lang.Long value) {
          addCriterion("category_id", value, ConditionMode.EQUAL, "categoryId", "java.lang.Long", "Float");
          return this;
      }

      public CmsArticleCategorySQL andCategoryIdNotEqualTo(java.lang.Long value) {
          addCriterion("category_id", value, ConditionMode.NOT_EQUAL, "categoryId", "java.lang.Long", "Float");
          return this;
      }

      public CmsArticleCategorySQL andCategoryIdGreaterThan(java.lang.Long value) {
          addCriterion("category_id", value, ConditionMode.GREATER_THEN, "categoryId", "java.lang.Long", "Float");
          return this;
      }

      public CmsArticleCategorySQL andCategoryIdGreaterThanOrEqualTo(java.lang.Long value) {
          addCriterion("category_id", value, ConditionMode.GREATER_EQUAL, "categoryId", "java.lang.Long", "Float");
          return this;
      }

      public CmsArticleCategorySQL andCategoryIdLessThan(java.lang.Long value) {
          addCriterion("category_id", value, ConditionMode.LESS_THEN, "categoryId", "java.lang.Long", "Float");
          return this;
      }

      public CmsArticleCategorySQL andCategoryIdLessThanOrEqualTo(java.lang.Long value) {
          addCriterion("category_id", value, ConditionMode.LESS_EQUAL, "categoryId", "java.lang.Long", "Float");
          return this;
      }

      public CmsArticleCategorySQL andCategoryIdBetween(java.lang.Long value1, java.lang.Long value2) {
    	  addCriterion("category_id", value1, value2, ConditionMode.BETWEEN, "categoryId", "java.lang.Long", "Float");
    	  return this;
      }

      public CmsArticleCategorySQL andCategoryIdNotBetween(java.lang.Long value1, java.lang.Long value2) {
          addCriterion("category_id", value1, value2, ConditionMode.NOT_BETWEEN, "categoryId", "java.lang.Long", "Float");
          return this;
      }
        
      public CmsArticleCategorySQL andCategoryIdIn(List<java.lang.Long> values) {
          addCriterion("category_id", values, ConditionMode.IN, "categoryId", "java.lang.Long", "Float");
          return this;
      }

      public CmsArticleCategorySQL andCategoryIdNotIn(List<java.lang.Long> values) {
          addCriterion("category_id", values, ConditionMode.NOT_IN, "categoryId", "java.lang.Long", "Float");
          return this;
      }
}