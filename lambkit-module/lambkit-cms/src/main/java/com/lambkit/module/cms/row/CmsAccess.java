/**
 * Copyright (c) 2015-2017, Henry Yang 杨勇 (gismail@foxmail.com).
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.lambkit.module.cms.row;

import com.lambkit.db.RowModel;

/**
 * @author yangyong 
 * @website: www.lambkit.com
 * @email: gismail@foxmail.com
 * @date ${date}
 * @version 1.0
 * @since 1.0
 */

public class CmsAccess extends RowModel<CmsAccess> {
	public CmsAccess() {
		setTableName("cms_access");
		setPrimaryKey("access_id");
	}
	public java.lang.Long getAccessId() {
		return this.get("access_id");
	}
	public void setAccessId(java.lang.Long accessId) {
		this.set("access_id", accessId);
	}
	public java.lang.Long getUserId() {
		return this.get("user_id");
	}
	public void setUserId(java.lang.Long userId) {
		this.set("user_id", userId);
	}
	public java.lang.Integer getReftype() {
		return this.get("reftype");
	}
	public void setReftype(java.lang.Integer reftype) {
		this.set("reftype", reftype);
	}
	public java.lang.Long getRefid() {
		return this.get("refid");
	}
	public void setRefid(java.lang.Long refid) {
		this.set("refid", refid);
	}
	public java.util.Date getAccessTime() {
		return this.get("access_time");
	}
	public void setAccessTime(java.util.Date accessTime) {
		this.set("access_time", accessTime);
	}
	public java.lang.String getAccessType() {
		return this.get("access_type");
	}
	public void setAccessType(java.lang.String accessType) {
		this.set("access_type", accessType);
	}
}
