/**
 * Copyright (c) 2015-2017, Henry Yang 杨勇 (gismail@foxmail.com).
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.lambkit.module.cms.sql;

import com.lambkit.db.sql.ConditionMode;
import com.lambkit.db.sql.Column;
import com.lambkit.db.sql.Columns;
import com.lambkit.db.sql.Example;

import java.io.Serializable;
import java.util.List;

/**
 * @author yangyong 
 * @website: www.lambkit.com
 * @email: gismail@foxmail.com
 * @date ${date}
 * @version 1.0
 * @since 1.0
 */

public class CmsAttachmentSQL extends Columns implements Serializable {
	private static final long serialVersionUID = 1L;
	
	public static CmsAttachmentSQL of() {
		return new CmsAttachmentSQL();
	}
	
	public static CmsAttachmentSQL by(Column column) {
		CmsAttachmentSQL that = new CmsAttachmentSQL();
		that.add(column);
        return that;
    }

    public static CmsAttachmentSQL by(String name, Object value) {
        return (CmsAttachmentSQL) create().eq(name, value);
    }
    
    public Example example() {
    	return Example.create("cms_attachment", this);
    }
    
    /**
     * equals
     *
     * @param name
     * @param value
     * @return
     */
    public CmsAttachmentSQL eq(String name, Object value) {
    	super.eq(name, value);
        return this;
    }

    /**
     * not equals !=
     *
     * @param name
     * @param value
     * @return
     */
    public CmsAttachmentSQL ne(String name, Object value) {
    	super.ne(name, value);
        return this;
    }


    /**
     * like
     *
     * @param name
     * @param value
     * @return
     */

    public CmsAttachmentSQL like(String name, Object value) {
    	super.like(name, value);
        return this;
    }
    
    public CmsAttachmentSQL notLike(String name, Object value) {
    	super.notLike(name, value);
        return this;
    }

    /**
     * 大于 great than
     *
     * @param name
     * @param value
     * @return
     */
    public CmsAttachmentSQL gt(String name, Object value) {
    	super.gt(name, value);
        return this;
    }

    /**
     * 大于等于 great or equal
     *
     * @param name
     * @param value
     * @return
     */
    public CmsAttachmentSQL ge(String name, Object value) {
    	super.ge(name, value);
        return this;
    }

    /**
     * 小于 less than
     *
     * @param name
     * @param value
     * @return
     */
    public CmsAttachmentSQL lt(String name, Object value) {
    	super.lt(name, value);
        return this;
    }

    /**
     * 小于等于 less or equal
     *
     * @param name
     * @param value
     * @return
     */
    public CmsAttachmentSQL le(String name, Object value) {
    	super.le(name, value);
        return this;
    }
    
    public CmsAttachmentSQL isnull(String name) {
    	super.isnull(name);
        return this;
    } 

    public CmsAttachmentSQL notNull(String name) {
    	super.notNull(name);
        return this;
    } 
    
    public CmsAttachmentSQL empty(String name) {
    	super.empty(name);
        return this;
    } 
    
    public CmsAttachmentSQL notEmpty(String name) {
    	super.notEmpty(name);
        return this;
    } 
    
    public CmsAttachmentSQL add(Column column) {
    	super.add(column);
    	return this;
    }
    
    /**************************/
	
	public void addCriterion(String name, Object value, ConditionMode logic, String property, String typeHandler, String valueType) {
		 if (value == null) {
			 throw new RuntimeException("Value for " + property + " cannot be null");
		 }
		 add(Column.create(name, value, logic, typeHandler, valueType));
	}
   
	public void addCriterion(String name, Object value1, Object value2, ConditionMode logic, String property, String typeHandler, String valueType) {
		 if (value1 == null || value2 == null) {
			 throw new RuntimeException("Between values for " + property + " cannot be null");
		 }
		 add(Column.create(name, value1, value2, logic, typeHandler, valueType));
	}
		 
	public CmsAttachmentSQL andAttachmentIdIsNull() {
		isnull("attachment_id");
		return this;
	}
	
	public CmsAttachmentSQL andAttachmentIdIsNotNull() {
		notNull("attachment_id");
		return this;
	}
	
	public CmsAttachmentSQL andAttachmentIdIsEmpty() {
		empty("attachment_id");
		return this;
	}

	public CmsAttachmentSQL andAttachmentIdIsNotEmpty() {
		notEmpty("attachment_id");
		return this;
	}
      public CmsAttachmentSQL andAttachmentIdEqualTo(java.lang.Integer value) {
          addCriterion("attachment_id", value, ConditionMode.EQUAL, "attachmentId", "java.lang.Integer", "Float");
          return this;
      }

      public CmsAttachmentSQL andAttachmentIdNotEqualTo(java.lang.Integer value) {
          addCriterion("attachment_id", value, ConditionMode.NOT_EQUAL, "attachmentId", "java.lang.Integer", "Float");
          return this;
      }

      public CmsAttachmentSQL andAttachmentIdGreaterThan(java.lang.Integer value) {
          addCriterion("attachment_id", value, ConditionMode.GREATER_THEN, "attachmentId", "java.lang.Integer", "Float");
          return this;
      }

      public CmsAttachmentSQL andAttachmentIdGreaterThanOrEqualTo(java.lang.Integer value) {
          addCriterion("attachment_id", value, ConditionMode.GREATER_EQUAL, "attachmentId", "java.lang.Integer", "Float");
          return this;
      }

      public CmsAttachmentSQL andAttachmentIdLessThan(java.lang.Integer value) {
          addCriterion("attachment_id", value, ConditionMode.LESS_THEN, "attachmentId", "java.lang.Integer", "Float");
          return this;
      }

      public CmsAttachmentSQL andAttachmentIdLessThanOrEqualTo(java.lang.Integer value) {
          addCriterion("attachment_id", value, ConditionMode.LESS_EQUAL, "attachmentId", "java.lang.Integer", "Float");
          return this;
      }

      public CmsAttachmentSQL andAttachmentIdBetween(java.lang.Integer value1, java.lang.Integer value2) {
    	  addCriterion("attachment_id", value1, value2, ConditionMode.BETWEEN, "attachmentId", "java.lang.Integer", "Float");
    	  return this;
      }

      public CmsAttachmentSQL andAttachmentIdNotBetween(java.lang.Integer value1, java.lang.Integer value2) {
          addCriterion("attachment_id", value1, value2, ConditionMode.NOT_BETWEEN, "attachmentId", "java.lang.Integer", "Float");
          return this;
      }
        
      public CmsAttachmentSQL andAttachmentIdIn(List<java.lang.Integer> values) {
          addCriterion("attachment_id", values, ConditionMode.IN, "attachmentId", "java.lang.Integer", "Float");
          return this;
      }

      public CmsAttachmentSQL andAttachmentIdNotIn(List<java.lang.Integer> values) {
          addCriterion("attachment_id", values, ConditionMode.NOT_IN, "attachmentId", "java.lang.Integer", "Float");
          return this;
      }
	public CmsAttachmentSQL andUserIdIsNull() {
		isnull("user_id");
		return this;
	}
	
	public CmsAttachmentSQL andUserIdIsNotNull() {
		notNull("user_id");
		return this;
	}
	
	public CmsAttachmentSQL andUserIdIsEmpty() {
		empty("user_id");
		return this;
	}

	public CmsAttachmentSQL andUserIdIsNotEmpty() {
		notEmpty("user_id");
		return this;
	}
      public CmsAttachmentSQL andUserIdEqualTo(java.lang.Long value) {
          addCriterion("user_id", value, ConditionMode.EQUAL, "userId", "java.lang.Long", "Float");
          return this;
      }

      public CmsAttachmentSQL andUserIdNotEqualTo(java.lang.Long value) {
          addCriterion("user_id", value, ConditionMode.NOT_EQUAL, "userId", "java.lang.Long", "Float");
          return this;
      }

      public CmsAttachmentSQL andUserIdGreaterThan(java.lang.Long value) {
          addCriterion("user_id", value, ConditionMode.GREATER_THEN, "userId", "java.lang.Long", "Float");
          return this;
      }

      public CmsAttachmentSQL andUserIdGreaterThanOrEqualTo(java.lang.Long value) {
          addCriterion("user_id", value, ConditionMode.GREATER_EQUAL, "userId", "java.lang.Long", "Float");
          return this;
      }

      public CmsAttachmentSQL andUserIdLessThan(java.lang.Long value) {
          addCriterion("user_id", value, ConditionMode.LESS_THEN, "userId", "java.lang.Long", "Float");
          return this;
      }

      public CmsAttachmentSQL andUserIdLessThanOrEqualTo(java.lang.Long value) {
          addCriterion("user_id", value, ConditionMode.LESS_EQUAL, "userId", "java.lang.Long", "Float");
          return this;
      }

      public CmsAttachmentSQL andUserIdBetween(java.lang.Long value1, java.lang.Long value2) {
    	  addCriterion("user_id", value1, value2, ConditionMode.BETWEEN, "userId", "java.lang.Long", "Float");
    	  return this;
      }

      public CmsAttachmentSQL andUserIdNotBetween(java.lang.Long value1, java.lang.Long value2) {
          addCriterion("user_id", value1, value2, ConditionMode.NOT_BETWEEN, "userId", "java.lang.Long", "Float");
          return this;
      }
        
      public CmsAttachmentSQL andUserIdIn(List<java.lang.Long> values) {
          addCriterion("user_id", values, ConditionMode.IN, "userId", "java.lang.Long", "Float");
          return this;
      }

      public CmsAttachmentSQL andUserIdNotIn(List<java.lang.Long> values) {
          addCriterion("user_id", values, ConditionMode.NOT_IN, "userId", "java.lang.Long", "Float");
          return this;
      }
	public CmsAttachmentSQL andTitleIsNull() {
		isnull("title");
		return this;
	}
	
	public CmsAttachmentSQL andTitleIsNotNull() {
		notNull("title");
		return this;
	}
	
	public CmsAttachmentSQL andTitleIsEmpty() {
		empty("title");
		return this;
	}

	public CmsAttachmentSQL andTitleIsNotEmpty() {
		notEmpty("title");
		return this;
	}
       public CmsAttachmentSQL andTitleLike(java.lang.String value) {
    	   addCriterion("title", value, ConditionMode.FUZZY, "title", "java.lang.String", "Float");
    	   return this;
      }

      public CmsAttachmentSQL andTitleNotLike(java.lang.String value) {
          addCriterion("title", value, ConditionMode.NOT_FUZZY, "title", "java.lang.String", "Float");
          return this;
      }
      public CmsAttachmentSQL andTitleEqualTo(java.lang.String value) {
          addCriterion("title", value, ConditionMode.EQUAL, "title", "java.lang.String", "String");
          return this;
      }

      public CmsAttachmentSQL andTitleNotEqualTo(java.lang.String value) {
          addCriterion("title", value, ConditionMode.NOT_EQUAL, "title", "java.lang.String", "String");
          return this;
      }

      public CmsAttachmentSQL andTitleGreaterThan(java.lang.String value) {
          addCriterion("title", value, ConditionMode.GREATER_THEN, "title", "java.lang.String", "String");
          return this;
      }

      public CmsAttachmentSQL andTitleGreaterThanOrEqualTo(java.lang.String value) {
          addCriterion("title", value, ConditionMode.GREATER_EQUAL, "title", "java.lang.String", "String");
          return this;
      }

      public CmsAttachmentSQL andTitleLessThan(java.lang.String value) {
          addCriterion("title", value, ConditionMode.LESS_THEN, "title", "java.lang.String", "String");
          return this;
      }

      public CmsAttachmentSQL andTitleLessThanOrEqualTo(java.lang.String value) {
          addCriterion("title", value, ConditionMode.LESS_EQUAL, "title", "java.lang.String", "String");
          return this;
      }

      public CmsAttachmentSQL andTitleBetween(java.lang.String value1, java.lang.String value2) {
    	  addCriterion("title", value1, value2, ConditionMode.BETWEEN, "title", "java.lang.String", "String");
    	  return this;
      }

      public CmsAttachmentSQL andTitleNotBetween(java.lang.String value1, java.lang.String value2) {
          addCriterion("title", value1, value2, ConditionMode.NOT_BETWEEN, "title", "java.lang.String", "String");
          return this;
      }
        
      public CmsAttachmentSQL andTitleIn(List<java.lang.String> values) {
          addCriterion("title", values, ConditionMode.IN, "title", "java.lang.String", "String");
          return this;
      }

      public CmsAttachmentSQL andTitleNotIn(List<java.lang.String> values) {
          addCriterion("title", values, ConditionMode.NOT_IN, "title", "java.lang.String", "String");
          return this;
      }
	public CmsAttachmentSQL andDescriptionIsNull() {
		isnull("description");
		return this;
	}
	
	public CmsAttachmentSQL andDescriptionIsNotNull() {
		notNull("description");
		return this;
	}
	
	public CmsAttachmentSQL andDescriptionIsEmpty() {
		empty("description");
		return this;
	}

	public CmsAttachmentSQL andDescriptionIsNotEmpty() {
		notEmpty("description");
		return this;
	}
       public CmsAttachmentSQL andDescriptionLike(java.lang.String value) {
    	   addCriterion("description", value, ConditionMode.FUZZY, "description", "java.lang.String", "String");
    	   return this;
      }

      public CmsAttachmentSQL andDescriptionNotLike(java.lang.String value) {
          addCriterion("description", value, ConditionMode.NOT_FUZZY, "description", "java.lang.String", "String");
          return this;
      }
      public CmsAttachmentSQL andDescriptionEqualTo(java.lang.String value) {
          addCriterion("description", value, ConditionMode.EQUAL, "description", "java.lang.String", "String");
          return this;
      }

      public CmsAttachmentSQL andDescriptionNotEqualTo(java.lang.String value) {
          addCriterion("description", value, ConditionMode.NOT_EQUAL, "description", "java.lang.String", "String");
          return this;
      }

      public CmsAttachmentSQL andDescriptionGreaterThan(java.lang.String value) {
          addCriterion("description", value, ConditionMode.GREATER_THEN, "description", "java.lang.String", "String");
          return this;
      }

      public CmsAttachmentSQL andDescriptionGreaterThanOrEqualTo(java.lang.String value) {
          addCriterion("description", value, ConditionMode.GREATER_EQUAL, "description", "java.lang.String", "String");
          return this;
      }

      public CmsAttachmentSQL andDescriptionLessThan(java.lang.String value) {
          addCriterion("description", value, ConditionMode.LESS_THEN, "description", "java.lang.String", "String");
          return this;
      }

      public CmsAttachmentSQL andDescriptionLessThanOrEqualTo(java.lang.String value) {
          addCriterion("description", value, ConditionMode.LESS_EQUAL, "description", "java.lang.String", "String");
          return this;
      }

      public CmsAttachmentSQL andDescriptionBetween(java.lang.String value1, java.lang.String value2) {
    	  addCriterion("description", value1, value2, ConditionMode.BETWEEN, "description", "java.lang.String", "String");
    	  return this;
      }

      public CmsAttachmentSQL andDescriptionNotBetween(java.lang.String value1, java.lang.String value2) {
          addCriterion("description", value1, value2, ConditionMode.NOT_BETWEEN, "description", "java.lang.String", "String");
          return this;
      }
        
      public CmsAttachmentSQL andDescriptionIn(List<java.lang.String> values) {
          addCriterion("description", values, ConditionMode.IN, "description", "java.lang.String", "String");
          return this;
      }

      public CmsAttachmentSQL andDescriptionNotIn(List<java.lang.String> values) {
          addCriterion("description", values, ConditionMode.NOT_IN, "description", "java.lang.String", "String");
          return this;
      }
	public CmsAttachmentSQL andPathIsNull() {
		isnull("path");
		return this;
	}
	
	public CmsAttachmentSQL andPathIsNotNull() {
		notNull("path");
		return this;
	}
	
	public CmsAttachmentSQL andPathIsEmpty() {
		empty("path");
		return this;
	}

	public CmsAttachmentSQL andPathIsNotEmpty() {
		notEmpty("path");
		return this;
	}
       public CmsAttachmentSQL andPathLike(java.lang.String value) {
    	   addCriterion("path", value, ConditionMode.FUZZY, "path", "java.lang.String", "String");
    	   return this;
      }

      public CmsAttachmentSQL andPathNotLike(java.lang.String value) {
          addCriterion("path", value, ConditionMode.NOT_FUZZY, "path", "java.lang.String", "String");
          return this;
      }
      public CmsAttachmentSQL andPathEqualTo(java.lang.String value) {
          addCriterion("path", value, ConditionMode.EQUAL, "path", "java.lang.String", "String");
          return this;
      }

      public CmsAttachmentSQL andPathNotEqualTo(java.lang.String value) {
          addCriterion("path", value, ConditionMode.NOT_EQUAL, "path", "java.lang.String", "String");
          return this;
      }

      public CmsAttachmentSQL andPathGreaterThan(java.lang.String value) {
          addCriterion("path", value, ConditionMode.GREATER_THEN, "path", "java.lang.String", "String");
          return this;
      }

      public CmsAttachmentSQL andPathGreaterThanOrEqualTo(java.lang.String value) {
          addCriterion("path", value, ConditionMode.GREATER_EQUAL, "path", "java.lang.String", "String");
          return this;
      }

      public CmsAttachmentSQL andPathLessThan(java.lang.String value) {
          addCriterion("path", value, ConditionMode.LESS_THEN, "path", "java.lang.String", "String");
          return this;
      }

      public CmsAttachmentSQL andPathLessThanOrEqualTo(java.lang.String value) {
          addCriterion("path", value, ConditionMode.LESS_EQUAL, "path", "java.lang.String", "String");
          return this;
      }

      public CmsAttachmentSQL andPathBetween(java.lang.String value1, java.lang.String value2) {
    	  addCriterion("path", value1, value2, ConditionMode.BETWEEN, "path", "java.lang.String", "String");
    	  return this;
      }

      public CmsAttachmentSQL andPathNotBetween(java.lang.String value1, java.lang.String value2) {
          addCriterion("path", value1, value2, ConditionMode.NOT_BETWEEN, "path", "java.lang.String", "String");
          return this;
      }
        
      public CmsAttachmentSQL andPathIn(List<java.lang.String> values) {
          addCriterion("path", values, ConditionMode.IN, "path", "java.lang.String", "String");
          return this;
      }

      public CmsAttachmentSQL andPathNotIn(List<java.lang.String> values) {
          addCriterion("path", values, ConditionMode.NOT_IN, "path", "java.lang.String", "String");
          return this;
      }
	public CmsAttachmentSQL andMimeTypeIsNull() {
		isnull("mime_type");
		return this;
	}
	
	public CmsAttachmentSQL andMimeTypeIsNotNull() {
		notNull("mime_type");
		return this;
	}
	
	public CmsAttachmentSQL andMimeTypeIsEmpty() {
		empty("mime_type");
		return this;
	}

	public CmsAttachmentSQL andMimeTypeIsNotEmpty() {
		notEmpty("mime_type");
		return this;
	}
       public CmsAttachmentSQL andMimeTypeLike(java.lang.String value) {
    	   addCriterion("mime_type", value, ConditionMode.FUZZY, "mimeType", "java.lang.String", "String");
    	   return this;
      }

      public CmsAttachmentSQL andMimeTypeNotLike(java.lang.String value) {
          addCriterion("mime_type", value, ConditionMode.NOT_FUZZY, "mimeType", "java.lang.String", "String");
          return this;
      }
      public CmsAttachmentSQL andMimeTypeEqualTo(java.lang.String value) {
          addCriterion("mime_type", value, ConditionMode.EQUAL, "mimeType", "java.lang.String", "String");
          return this;
      }

      public CmsAttachmentSQL andMimeTypeNotEqualTo(java.lang.String value) {
          addCriterion("mime_type", value, ConditionMode.NOT_EQUAL, "mimeType", "java.lang.String", "String");
          return this;
      }

      public CmsAttachmentSQL andMimeTypeGreaterThan(java.lang.String value) {
          addCriterion("mime_type", value, ConditionMode.GREATER_THEN, "mimeType", "java.lang.String", "String");
          return this;
      }

      public CmsAttachmentSQL andMimeTypeGreaterThanOrEqualTo(java.lang.String value) {
          addCriterion("mime_type", value, ConditionMode.GREATER_EQUAL, "mimeType", "java.lang.String", "String");
          return this;
      }

      public CmsAttachmentSQL andMimeTypeLessThan(java.lang.String value) {
          addCriterion("mime_type", value, ConditionMode.LESS_THEN, "mimeType", "java.lang.String", "String");
          return this;
      }

      public CmsAttachmentSQL andMimeTypeLessThanOrEqualTo(java.lang.String value) {
          addCriterion("mime_type", value, ConditionMode.LESS_EQUAL, "mimeType", "java.lang.String", "String");
          return this;
      }

      public CmsAttachmentSQL andMimeTypeBetween(java.lang.String value1, java.lang.String value2) {
    	  addCriterion("mime_type", value1, value2, ConditionMode.BETWEEN, "mimeType", "java.lang.String", "String");
    	  return this;
      }

      public CmsAttachmentSQL andMimeTypeNotBetween(java.lang.String value1, java.lang.String value2) {
          addCriterion("mime_type", value1, value2, ConditionMode.NOT_BETWEEN, "mimeType", "java.lang.String", "String");
          return this;
      }
        
      public CmsAttachmentSQL andMimeTypeIn(List<java.lang.String> values) {
          addCriterion("mime_type", values, ConditionMode.IN, "mimeType", "java.lang.String", "String");
          return this;
      }

      public CmsAttachmentSQL andMimeTypeNotIn(List<java.lang.String> values) {
          addCriterion("mime_type", values, ConditionMode.NOT_IN, "mimeType", "java.lang.String", "String");
          return this;
      }
	public CmsAttachmentSQL andSuffixIsNull() {
		isnull("suffix");
		return this;
	}
	
	public CmsAttachmentSQL andSuffixIsNotNull() {
		notNull("suffix");
		return this;
	}
	
	public CmsAttachmentSQL andSuffixIsEmpty() {
		empty("suffix");
		return this;
	}

	public CmsAttachmentSQL andSuffixIsNotEmpty() {
		notEmpty("suffix");
		return this;
	}
       public CmsAttachmentSQL andSuffixLike(java.lang.String value) {
    	   addCriterion("suffix", value, ConditionMode.FUZZY, "suffix", "java.lang.String", "String");
    	   return this;
      }

      public CmsAttachmentSQL andSuffixNotLike(java.lang.String value) {
          addCriterion("suffix", value, ConditionMode.NOT_FUZZY, "suffix", "java.lang.String", "String");
          return this;
      }
      public CmsAttachmentSQL andSuffixEqualTo(java.lang.String value) {
          addCriterion("suffix", value, ConditionMode.EQUAL, "suffix", "java.lang.String", "String");
          return this;
      }

      public CmsAttachmentSQL andSuffixNotEqualTo(java.lang.String value) {
          addCriterion("suffix", value, ConditionMode.NOT_EQUAL, "suffix", "java.lang.String", "String");
          return this;
      }

      public CmsAttachmentSQL andSuffixGreaterThan(java.lang.String value) {
          addCriterion("suffix", value, ConditionMode.GREATER_THEN, "suffix", "java.lang.String", "String");
          return this;
      }

      public CmsAttachmentSQL andSuffixGreaterThanOrEqualTo(java.lang.String value) {
          addCriterion("suffix", value, ConditionMode.GREATER_EQUAL, "suffix", "java.lang.String", "String");
          return this;
      }

      public CmsAttachmentSQL andSuffixLessThan(java.lang.String value) {
          addCriterion("suffix", value, ConditionMode.LESS_THEN, "suffix", "java.lang.String", "String");
          return this;
      }

      public CmsAttachmentSQL andSuffixLessThanOrEqualTo(java.lang.String value) {
          addCriterion("suffix", value, ConditionMode.LESS_EQUAL, "suffix", "java.lang.String", "String");
          return this;
      }

      public CmsAttachmentSQL andSuffixBetween(java.lang.String value1, java.lang.String value2) {
    	  addCriterion("suffix", value1, value2, ConditionMode.BETWEEN, "suffix", "java.lang.String", "String");
    	  return this;
      }

      public CmsAttachmentSQL andSuffixNotBetween(java.lang.String value1, java.lang.String value2) {
          addCriterion("suffix", value1, value2, ConditionMode.NOT_BETWEEN, "suffix", "java.lang.String", "String");
          return this;
      }
        
      public CmsAttachmentSQL andSuffixIn(List<java.lang.String> values) {
          addCriterion("suffix", values, ConditionMode.IN, "suffix", "java.lang.String", "String");
          return this;
      }

      public CmsAttachmentSQL andSuffixNotIn(List<java.lang.String> values) {
          addCriterion("suffix", values, ConditionMode.NOT_IN, "suffix", "java.lang.String", "String");
          return this;
      }
	public CmsAttachmentSQL andTypeIsNull() {
		isnull("type");
		return this;
	}
	
	public CmsAttachmentSQL andTypeIsNotNull() {
		notNull("type");
		return this;
	}
	
	public CmsAttachmentSQL andTypeIsEmpty() {
		empty("type");
		return this;
	}

	public CmsAttachmentSQL andTypeIsNotEmpty() {
		notEmpty("type");
		return this;
	}
       public CmsAttachmentSQL andTypeLike(java.lang.String value) {
    	   addCriterion("type", value, ConditionMode.FUZZY, "type", "java.lang.String", "String");
    	   return this;
      }

      public CmsAttachmentSQL andTypeNotLike(java.lang.String value) {
          addCriterion("type", value, ConditionMode.NOT_FUZZY, "type", "java.lang.String", "String");
          return this;
      }
      public CmsAttachmentSQL andTypeEqualTo(java.lang.String value) {
          addCriterion("type", value, ConditionMode.EQUAL, "type", "java.lang.String", "String");
          return this;
      }

      public CmsAttachmentSQL andTypeNotEqualTo(java.lang.String value) {
          addCriterion("type", value, ConditionMode.NOT_EQUAL, "type", "java.lang.String", "String");
          return this;
      }

      public CmsAttachmentSQL andTypeGreaterThan(java.lang.String value) {
          addCriterion("type", value, ConditionMode.GREATER_THEN, "type", "java.lang.String", "String");
          return this;
      }

      public CmsAttachmentSQL andTypeGreaterThanOrEqualTo(java.lang.String value) {
          addCriterion("type", value, ConditionMode.GREATER_EQUAL, "type", "java.lang.String", "String");
          return this;
      }

      public CmsAttachmentSQL andTypeLessThan(java.lang.String value) {
          addCriterion("type", value, ConditionMode.LESS_THEN, "type", "java.lang.String", "String");
          return this;
      }

      public CmsAttachmentSQL andTypeLessThanOrEqualTo(java.lang.String value) {
          addCriterion("type", value, ConditionMode.LESS_EQUAL, "type", "java.lang.String", "String");
          return this;
      }

      public CmsAttachmentSQL andTypeBetween(java.lang.String value1, java.lang.String value2) {
    	  addCriterion("type", value1, value2, ConditionMode.BETWEEN, "type", "java.lang.String", "String");
    	  return this;
      }

      public CmsAttachmentSQL andTypeNotBetween(java.lang.String value1, java.lang.String value2) {
          addCriterion("type", value1, value2, ConditionMode.NOT_BETWEEN, "type", "java.lang.String", "String");
          return this;
      }
        
      public CmsAttachmentSQL andTypeIn(List<java.lang.String> values) {
          addCriterion("type", values, ConditionMode.IN, "type", "java.lang.String", "String");
          return this;
      }

      public CmsAttachmentSQL andTypeNotIn(List<java.lang.String> values) {
          addCriterion("type", values, ConditionMode.NOT_IN, "type", "java.lang.String", "String");
          return this;
      }
	public CmsAttachmentSQL andFlagIsNull() {
		isnull("flag");
		return this;
	}
	
	public CmsAttachmentSQL andFlagIsNotNull() {
		notNull("flag");
		return this;
	}
	
	public CmsAttachmentSQL andFlagIsEmpty() {
		empty("flag");
		return this;
	}

	public CmsAttachmentSQL andFlagIsNotEmpty() {
		notEmpty("flag");
		return this;
	}
       public CmsAttachmentSQL andFlagLike(java.lang.String value) {
    	   addCriterion("flag", value, ConditionMode.FUZZY, "flag", "java.lang.String", "String");
    	   return this;
      }

      public CmsAttachmentSQL andFlagNotLike(java.lang.String value) {
          addCriterion("flag", value, ConditionMode.NOT_FUZZY, "flag", "java.lang.String", "String");
          return this;
      }
      public CmsAttachmentSQL andFlagEqualTo(java.lang.String value) {
          addCriterion("flag", value, ConditionMode.EQUAL, "flag", "java.lang.String", "String");
          return this;
      }

      public CmsAttachmentSQL andFlagNotEqualTo(java.lang.String value) {
          addCriterion("flag", value, ConditionMode.NOT_EQUAL, "flag", "java.lang.String", "String");
          return this;
      }

      public CmsAttachmentSQL andFlagGreaterThan(java.lang.String value) {
          addCriterion("flag", value, ConditionMode.GREATER_THEN, "flag", "java.lang.String", "String");
          return this;
      }

      public CmsAttachmentSQL andFlagGreaterThanOrEqualTo(java.lang.String value) {
          addCriterion("flag", value, ConditionMode.GREATER_EQUAL, "flag", "java.lang.String", "String");
          return this;
      }

      public CmsAttachmentSQL andFlagLessThan(java.lang.String value) {
          addCriterion("flag", value, ConditionMode.LESS_THEN, "flag", "java.lang.String", "String");
          return this;
      }

      public CmsAttachmentSQL andFlagLessThanOrEqualTo(java.lang.String value) {
          addCriterion("flag", value, ConditionMode.LESS_EQUAL, "flag", "java.lang.String", "String");
          return this;
      }

      public CmsAttachmentSQL andFlagBetween(java.lang.String value1, java.lang.String value2) {
    	  addCriterion("flag", value1, value2, ConditionMode.BETWEEN, "flag", "java.lang.String", "String");
    	  return this;
      }

      public CmsAttachmentSQL andFlagNotBetween(java.lang.String value1, java.lang.String value2) {
          addCriterion("flag", value1, value2, ConditionMode.NOT_BETWEEN, "flag", "java.lang.String", "String");
          return this;
      }
        
      public CmsAttachmentSQL andFlagIn(List<java.lang.String> values) {
          addCriterion("flag", values, ConditionMode.IN, "flag", "java.lang.String", "String");
          return this;
      }

      public CmsAttachmentSQL andFlagNotIn(List<java.lang.String> values) {
          addCriterion("flag", values, ConditionMode.NOT_IN, "flag", "java.lang.String", "String");
          return this;
      }
	public CmsAttachmentSQL andOrderNumberIsNull() {
		isnull("order_number");
		return this;
	}
	
	public CmsAttachmentSQL andOrderNumberIsNotNull() {
		notNull("order_number");
		return this;
	}
	
	public CmsAttachmentSQL andOrderNumberIsEmpty() {
		empty("order_number");
		return this;
	}

	public CmsAttachmentSQL andOrderNumberIsNotEmpty() {
		notEmpty("order_number");
		return this;
	}
      public CmsAttachmentSQL andOrderNumberEqualTo(java.lang.Integer value) {
          addCriterion("order_number", value, ConditionMode.EQUAL, "orderNumber", "java.lang.Integer", "Float");
          return this;
      }

      public CmsAttachmentSQL andOrderNumberNotEqualTo(java.lang.Integer value) {
          addCriterion("order_number", value, ConditionMode.NOT_EQUAL, "orderNumber", "java.lang.Integer", "Float");
          return this;
      }

      public CmsAttachmentSQL andOrderNumberGreaterThan(java.lang.Integer value) {
          addCriterion("order_number", value, ConditionMode.GREATER_THEN, "orderNumber", "java.lang.Integer", "Float");
          return this;
      }

      public CmsAttachmentSQL andOrderNumberGreaterThanOrEqualTo(java.lang.Integer value) {
          addCriterion("order_number", value, ConditionMode.GREATER_EQUAL, "orderNumber", "java.lang.Integer", "Float");
          return this;
      }

      public CmsAttachmentSQL andOrderNumberLessThan(java.lang.Integer value) {
          addCriterion("order_number", value, ConditionMode.LESS_THEN, "orderNumber", "java.lang.Integer", "Float");
          return this;
      }

      public CmsAttachmentSQL andOrderNumberLessThanOrEqualTo(java.lang.Integer value) {
          addCriterion("order_number", value, ConditionMode.LESS_EQUAL, "orderNumber", "java.lang.Integer", "Float");
          return this;
      }

      public CmsAttachmentSQL andOrderNumberBetween(java.lang.Integer value1, java.lang.Integer value2) {
    	  addCriterion("order_number", value1, value2, ConditionMode.BETWEEN, "orderNumber", "java.lang.Integer", "Float");
    	  return this;
      }

      public CmsAttachmentSQL andOrderNumberNotBetween(java.lang.Integer value1, java.lang.Integer value2) {
          addCriterion("order_number", value1, value2, ConditionMode.NOT_BETWEEN, "orderNumber", "java.lang.Integer", "Float");
          return this;
      }
        
      public CmsAttachmentSQL andOrderNumberIn(List<java.lang.Integer> values) {
          addCriterion("order_number", values, ConditionMode.IN, "orderNumber", "java.lang.Integer", "Float");
          return this;
      }

      public CmsAttachmentSQL andOrderNumberNotIn(List<java.lang.Integer> values) {
          addCriterion("order_number", values, ConditionMode.NOT_IN, "orderNumber", "java.lang.Integer", "Float");
          return this;
      }
	public CmsAttachmentSQL andAccessibleIsNull() {
		isnull("accessible");
		return this;
	}
	
	public CmsAttachmentSQL andAccessibleIsNotNull() {
		notNull("accessible");
		return this;
	}
	
	public CmsAttachmentSQL andAccessibleIsEmpty() {
		empty("accessible");
		return this;
	}

	public CmsAttachmentSQL andAccessibleIsNotEmpty() {
		notEmpty("accessible");
		return this;
	}
      public CmsAttachmentSQL andAccessibleEqualTo(java.lang.Integer value) {
          addCriterion("accessible", value, ConditionMode.EQUAL, "accessible", "java.lang.Integer", "Float");
          return this;
      }

      public CmsAttachmentSQL andAccessibleNotEqualTo(java.lang.Integer value) {
          addCriterion("accessible", value, ConditionMode.NOT_EQUAL, "accessible", "java.lang.Integer", "Float");
          return this;
      }

      public CmsAttachmentSQL andAccessibleGreaterThan(java.lang.Integer value) {
          addCriterion("accessible", value, ConditionMode.GREATER_THEN, "accessible", "java.lang.Integer", "Float");
          return this;
      }

      public CmsAttachmentSQL andAccessibleGreaterThanOrEqualTo(java.lang.Integer value) {
          addCriterion("accessible", value, ConditionMode.GREATER_EQUAL, "accessible", "java.lang.Integer", "Float");
          return this;
      }

      public CmsAttachmentSQL andAccessibleLessThan(java.lang.Integer value) {
          addCriterion("accessible", value, ConditionMode.LESS_THEN, "accessible", "java.lang.Integer", "Float");
          return this;
      }

      public CmsAttachmentSQL andAccessibleLessThanOrEqualTo(java.lang.Integer value) {
          addCriterion("accessible", value, ConditionMode.LESS_EQUAL, "accessible", "java.lang.Integer", "Float");
          return this;
      }

      public CmsAttachmentSQL andAccessibleBetween(java.lang.Integer value1, java.lang.Integer value2) {
    	  addCriterion("accessible", value1, value2, ConditionMode.BETWEEN, "accessible", "java.lang.Integer", "Float");
    	  return this;
      }

      public CmsAttachmentSQL andAccessibleNotBetween(java.lang.Integer value1, java.lang.Integer value2) {
          addCriterion("accessible", value1, value2, ConditionMode.NOT_BETWEEN, "accessible", "java.lang.Integer", "Float");
          return this;
      }
        
      public CmsAttachmentSQL andAccessibleIn(List<java.lang.Integer> values) {
          addCriterion("accessible", values, ConditionMode.IN, "accessible", "java.lang.Integer", "Float");
          return this;
      }

      public CmsAttachmentSQL andAccessibleNotIn(List<java.lang.Integer> values) {
          addCriterion("accessible", values, ConditionMode.NOT_IN, "accessible", "java.lang.Integer", "Float");
          return this;
      }
	public CmsAttachmentSQL andCreatedIsNull() {
		isnull("created");
		return this;
	}
	
	public CmsAttachmentSQL andCreatedIsNotNull() {
		notNull("created");
		return this;
	}
	
	public CmsAttachmentSQL andCreatedIsEmpty() {
		empty("created");
		return this;
	}

	public CmsAttachmentSQL andCreatedIsNotEmpty() {
		notEmpty("created");
		return this;
	}
      public CmsAttachmentSQL andCreatedEqualTo(java.util.Date value) {
          addCriterion("created", value, ConditionMode.EQUAL, "created", "java.util.Date", "String");
          return this;
      }

      public CmsAttachmentSQL andCreatedNotEqualTo(java.util.Date value) {
          addCriterion("created", value, ConditionMode.NOT_EQUAL, "created", "java.util.Date", "String");
          return this;
      }

      public CmsAttachmentSQL andCreatedGreaterThan(java.util.Date value) {
          addCriterion("created", value, ConditionMode.GREATER_THEN, "created", "java.util.Date", "String");
          return this;
      }

      public CmsAttachmentSQL andCreatedGreaterThanOrEqualTo(java.util.Date value) {
          addCriterion("created", value, ConditionMode.GREATER_EQUAL, "created", "java.util.Date", "String");
          return this;
      }

      public CmsAttachmentSQL andCreatedLessThan(java.util.Date value) {
          addCriterion("created", value, ConditionMode.LESS_THEN, "created", "java.util.Date", "String");
          return this;
      }

      public CmsAttachmentSQL andCreatedLessThanOrEqualTo(java.util.Date value) {
          addCriterion("created", value, ConditionMode.LESS_EQUAL, "created", "java.util.Date", "String");
          return this;
      }

      public CmsAttachmentSQL andCreatedBetween(java.util.Date value1, java.util.Date value2) {
    	  addCriterion("created", value1, value2, ConditionMode.BETWEEN, "created", "java.util.Date", "String");
    	  return this;
      }

      public CmsAttachmentSQL andCreatedNotBetween(java.util.Date value1, java.util.Date value2) {
          addCriterion("created", value1, value2, ConditionMode.NOT_BETWEEN, "created", "java.util.Date", "String");
          return this;
      }
        
      public CmsAttachmentSQL andCreatedIn(List<java.util.Date> values) {
          addCriterion("created", values, ConditionMode.IN, "created", "java.util.Date", "String");
          return this;
      }

      public CmsAttachmentSQL andCreatedNotIn(List<java.util.Date> values) {
          addCriterion("created", values, ConditionMode.NOT_IN, "created", "java.util.Date", "String");
          return this;
      }
	public CmsAttachmentSQL andModifiedIsNull() {
		isnull("modified");
		return this;
	}
	
	public CmsAttachmentSQL andModifiedIsNotNull() {
		notNull("modified");
		return this;
	}
	
	public CmsAttachmentSQL andModifiedIsEmpty() {
		empty("modified");
		return this;
	}

	public CmsAttachmentSQL andModifiedIsNotEmpty() {
		notEmpty("modified");
		return this;
	}
      public CmsAttachmentSQL andModifiedEqualTo(java.util.Date value) {
          addCriterion("modified", value, ConditionMode.EQUAL, "modified", "java.util.Date", "String");
          return this;
      }

      public CmsAttachmentSQL andModifiedNotEqualTo(java.util.Date value) {
          addCriterion("modified", value, ConditionMode.NOT_EQUAL, "modified", "java.util.Date", "String");
          return this;
      }

      public CmsAttachmentSQL andModifiedGreaterThan(java.util.Date value) {
          addCriterion("modified", value, ConditionMode.GREATER_THEN, "modified", "java.util.Date", "String");
          return this;
      }

      public CmsAttachmentSQL andModifiedGreaterThanOrEqualTo(java.util.Date value) {
          addCriterion("modified", value, ConditionMode.GREATER_EQUAL, "modified", "java.util.Date", "String");
          return this;
      }

      public CmsAttachmentSQL andModifiedLessThan(java.util.Date value) {
          addCriterion("modified", value, ConditionMode.LESS_THEN, "modified", "java.util.Date", "String");
          return this;
      }

      public CmsAttachmentSQL andModifiedLessThanOrEqualTo(java.util.Date value) {
          addCriterion("modified", value, ConditionMode.LESS_EQUAL, "modified", "java.util.Date", "String");
          return this;
      }

      public CmsAttachmentSQL andModifiedBetween(java.util.Date value1, java.util.Date value2) {
    	  addCriterion("modified", value1, value2, ConditionMode.BETWEEN, "modified", "java.util.Date", "String");
    	  return this;
      }

      public CmsAttachmentSQL andModifiedNotBetween(java.util.Date value1, java.util.Date value2) {
          addCriterion("modified", value1, value2, ConditionMode.NOT_BETWEEN, "modified", "java.util.Date", "String");
          return this;
      }
        
      public CmsAttachmentSQL andModifiedIn(List<java.util.Date> values) {
          addCriterion("modified", values, ConditionMode.IN, "modified", "java.util.Date", "String");
          return this;
      }

      public CmsAttachmentSQL andModifiedNotIn(List<java.util.Date> values) {
          addCriterion("modified", values, ConditionMode.NOT_IN, "modified", "java.util.Date", "String");
          return this;
      }
}