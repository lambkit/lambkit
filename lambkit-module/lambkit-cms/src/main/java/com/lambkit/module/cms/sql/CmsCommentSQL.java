/**
 * Copyright (c) 2015-2017, Henry Yang 杨勇 (gismail@foxmail.com).
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.lambkit.module.cms.sql;

import com.lambkit.db.sql.ConditionMode;
import com.lambkit.db.sql.Column;
import com.lambkit.db.sql.Columns;
import com.lambkit.db.sql.Example;

import java.io.Serializable;
import java.util.List;

/**
 * @author yangyong 
 * @website: www.lambkit.com
 * @email: gismail@foxmail.com
 * @date ${date}
 * @version 1.0
 * @since 1.0
 */

public class CmsCommentSQL extends Columns implements Serializable {
	private static final long serialVersionUID = 1L;
	
	public static CmsCommentSQL of() {
		return new CmsCommentSQL();
	}
	
	public static CmsCommentSQL by(Column column) {
		CmsCommentSQL that = new CmsCommentSQL();
		that.add(column);
        return that;
    }

    public static CmsCommentSQL by(String name, Object value) {
        return (CmsCommentSQL) create().eq(name, value);
    }
    
    public Example example() {
    	return Example.create("cms_comment", this);
    }
    
    /**
     * equals
     *
     * @param name
     * @param value
     * @return
     */
    public CmsCommentSQL eq(String name, Object value) {
    	super.eq(name, value);
        return this;
    }

    /**
     * not equals !=
     *
     * @param name
     * @param value
     * @return
     */
    public CmsCommentSQL ne(String name, Object value) {
    	super.ne(name, value);
        return this;
    }


    /**
     * like
     *
     * @param name
     * @param value
     * @return
     */

    public CmsCommentSQL like(String name, Object value) {
    	super.like(name, value);
        return this;
    }
    
    public CmsCommentSQL notLike(String name, Object value) {
    	super.notLike(name, value);
        return this;
    }

    /**
     * 大于 great than
     *
     * @param name
     * @param value
     * @return
     */
    public CmsCommentSQL gt(String name, Object value) {
    	super.gt(name, value);
        return this;
    }

    /**
     * 大于等于 great or equal
     *
     * @param name
     * @param value
     * @return
     */
    public CmsCommentSQL ge(String name, Object value) {
    	super.ge(name, value);
        return this;
    }

    /**
     * 小于 less than
     *
     * @param name
     * @param value
     * @return
     */
    public CmsCommentSQL lt(String name, Object value) {
    	super.lt(name, value);
        return this;
    }

    /**
     * 小于等于 less or equal
     *
     * @param name
     * @param value
     * @return
     */
    public CmsCommentSQL le(String name, Object value) {
    	super.le(name, value);
        return this;
    }
    
    public CmsCommentSQL isnull(String name) {
    	super.isnull(name);
        return this;
    } 

    public CmsCommentSQL notNull(String name) {
    	super.notNull(name);
        return this;
    } 
    
    public CmsCommentSQL empty(String name) {
    	super.empty(name);
        return this;
    } 
    
    public CmsCommentSQL notEmpty(String name) {
    	super.notEmpty(name);
        return this;
    } 
    
    public CmsCommentSQL add(Column column) {
    	super.add(column);
    	return this;
    }
    
    /**************************/
	
	public void addCriterion(String name, Object value, ConditionMode logic, String property, String typeHandler, String valueType) {
		 if (value == null) {
			 throw new RuntimeException("Value for " + property + " cannot be null");
		 }
		 add(Column.create(name, value, logic, typeHandler, valueType));
	}
   
	public void addCriterion(String name, Object value1, Object value2, ConditionMode logic, String property, String typeHandler, String valueType) {
		 if (value1 == null || value2 == null) {
			 throw new RuntimeException("Between values for " + property + " cannot be null");
		 }
		 add(Column.create(name, value1, value2, logic, typeHandler, valueType));
	}
		 
	public CmsCommentSQL andCommentIdIsNull() {
		isnull("comment_id");
		return this;
	}
	
	public CmsCommentSQL andCommentIdIsNotNull() {
		notNull("comment_id");
		return this;
	}
	
	public CmsCommentSQL andCommentIdIsEmpty() {
		empty("comment_id");
		return this;
	}

	public CmsCommentSQL andCommentIdIsNotEmpty() {
		notEmpty("comment_id");
		return this;
	}
      public CmsCommentSQL andCommentIdEqualTo(java.lang.Long value) {
          addCriterion("comment_id", value, ConditionMode.EQUAL, "commentId", "java.lang.Long", "Float");
          return this;
      }

      public CmsCommentSQL andCommentIdNotEqualTo(java.lang.Long value) {
          addCriterion("comment_id", value, ConditionMode.NOT_EQUAL, "commentId", "java.lang.Long", "Float");
          return this;
      }

      public CmsCommentSQL andCommentIdGreaterThan(java.lang.Long value) {
          addCriterion("comment_id", value, ConditionMode.GREATER_THEN, "commentId", "java.lang.Long", "Float");
          return this;
      }

      public CmsCommentSQL andCommentIdGreaterThanOrEqualTo(java.lang.Long value) {
          addCriterion("comment_id", value, ConditionMode.GREATER_EQUAL, "commentId", "java.lang.Long", "Float");
          return this;
      }

      public CmsCommentSQL andCommentIdLessThan(java.lang.Long value) {
          addCriterion("comment_id", value, ConditionMode.LESS_THEN, "commentId", "java.lang.Long", "Float");
          return this;
      }

      public CmsCommentSQL andCommentIdLessThanOrEqualTo(java.lang.Long value) {
          addCriterion("comment_id", value, ConditionMode.LESS_EQUAL, "commentId", "java.lang.Long", "Float");
          return this;
      }

      public CmsCommentSQL andCommentIdBetween(java.lang.Long value1, java.lang.Long value2) {
    	  addCriterion("comment_id", value1, value2, ConditionMode.BETWEEN, "commentId", "java.lang.Long", "Float");
    	  return this;
      }

      public CmsCommentSQL andCommentIdNotBetween(java.lang.Long value1, java.lang.Long value2) {
          addCriterion("comment_id", value1, value2, ConditionMode.NOT_BETWEEN, "commentId", "java.lang.Long", "Float");
          return this;
      }
        
      public CmsCommentSQL andCommentIdIn(List<java.lang.Long> values) {
          addCriterion("comment_id", values, ConditionMode.IN, "commentId", "java.lang.Long", "Float");
          return this;
      }

      public CmsCommentSQL andCommentIdNotIn(List<java.lang.Long> values) {
          addCriterion("comment_id", values, ConditionMode.NOT_IN, "commentId", "java.lang.Long", "Float");
          return this;
      }
	public CmsCommentSQL andParentIdIsNull() {
		isnull("parent_id");
		return this;
	}
	
	public CmsCommentSQL andParentIdIsNotNull() {
		notNull("parent_id");
		return this;
	}
	
	public CmsCommentSQL andParentIdIsEmpty() {
		empty("parent_id");
		return this;
	}

	public CmsCommentSQL andParentIdIsNotEmpty() {
		notEmpty("parent_id");
		return this;
	}
      public CmsCommentSQL andParentIdEqualTo(java.lang.Long value) {
          addCriterion("parent_id", value, ConditionMode.EQUAL, "parentId", "java.lang.Long", "Float");
          return this;
      }

      public CmsCommentSQL andParentIdNotEqualTo(java.lang.Long value) {
          addCriterion("parent_id", value, ConditionMode.NOT_EQUAL, "parentId", "java.lang.Long", "Float");
          return this;
      }

      public CmsCommentSQL andParentIdGreaterThan(java.lang.Long value) {
          addCriterion("parent_id", value, ConditionMode.GREATER_THEN, "parentId", "java.lang.Long", "Float");
          return this;
      }

      public CmsCommentSQL andParentIdGreaterThanOrEqualTo(java.lang.Long value) {
          addCriterion("parent_id", value, ConditionMode.GREATER_EQUAL, "parentId", "java.lang.Long", "Float");
          return this;
      }

      public CmsCommentSQL andParentIdLessThan(java.lang.Long value) {
          addCriterion("parent_id", value, ConditionMode.LESS_THEN, "parentId", "java.lang.Long", "Float");
          return this;
      }

      public CmsCommentSQL andParentIdLessThanOrEqualTo(java.lang.Long value) {
          addCriterion("parent_id", value, ConditionMode.LESS_EQUAL, "parentId", "java.lang.Long", "Float");
          return this;
      }

      public CmsCommentSQL andParentIdBetween(java.lang.Long value1, java.lang.Long value2) {
    	  addCriterion("parent_id", value1, value2, ConditionMode.BETWEEN, "parentId", "java.lang.Long", "Float");
    	  return this;
      }

      public CmsCommentSQL andParentIdNotBetween(java.lang.Long value1, java.lang.Long value2) {
          addCriterion("parent_id", value1, value2, ConditionMode.NOT_BETWEEN, "parentId", "java.lang.Long", "Float");
          return this;
      }
        
      public CmsCommentSQL andParentIdIn(List<java.lang.Long> values) {
          addCriterion("parent_id", values, ConditionMode.IN, "parentId", "java.lang.Long", "Float");
          return this;
      }

      public CmsCommentSQL andParentIdNotIn(List<java.lang.Long> values) {
          addCriterion("parent_id", values, ConditionMode.NOT_IN, "parentId", "java.lang.Long", "Float");
          return this;
      }
	public CmsCommentSQL andPathIsNull() {
		isnull("path");
		return this;
	}
	
	public CmsCommentSQL andPathIsNotNull() {
		notNull("path");
		return this;
	}
	
	public CmsCommentSQL andPathIsEmpty() {
		empty("path");
		return this;
	}

	public CmsCommentSQL andPathIsNotEmpty() {
		notEmpty("path");
		return this;
	}
       public CmsCommentSQL andPathLike(java.lang.String value) {
    	   addCriterion("path", value, ConditionMode.FUZZY, "path", "java.lang.String", "Float");
    	   return this;
      }

      public CmsCommentSQL andPathNotLike(java.lang.String value) {
          addCriterion("path", value, ConditionMode.NOT_FUZZY, "path", "java.lang.String", "Float");
          return this;
      }
      public CmsCommentSQL andPathEqualTo(java.lang.String value) {
          addCriterion("path", value, ConditionMode.EQUAL, "path", "java.lang.String", "String");
          return this;
      }

      public CmsCommentSQL andPathNotEqualTo(java.lang.String value) {
          addCriterion("path", value, ConditionMode.NOT_EQUAL, "path", "java.lang.String", "String");
          return this;
      }

      public CmsCommentSQL andPathGreaterThan(java.lang.String value) {
          addCriterion("path", value, ConditionMode.GREATER_THEN, "path", "java.lang.String", "String");
          return this;
      }

      public CmsCommentSQL andPathGreaterThanOrEqualTo(java.lang.String value) {
          addCriterion("path", value, ConditionMode.GREATER_EQUAL, "path", "java.lang.String", "String");
          return this;
      }

      public CmsCommentSQL andPathLessThan(java.lang.String value) {
          addCriterion("path", value, ConditionMode.LESS_THEN, "path", "java.lang.String", "String");
          return this;
      }

      public CmsCommentSQL andPathLessThanOrEqualTo(java.lang.String value) {
          addCriterion("path", value, ConditionMode.LESS_EQUAL, "path", "java.lang.String", "String");
          return this;
      }

      public CmsCommentSQL andPathBetween(java.lang.String value1, java.lang.String value2) {
    	  addCriterion("path", value1, value2, ConditionMode.BETWEEN, "path", "java.lang.String", "String");
    	  return this;
      }

      public CmsCommentSQL andPathNotBetween(java.lang.String value1, java.lang.String value2) {
          addCriterion("path", value1, value2, ConditionMode.NOT_BETWEEN, "path", "java.lang.String", "String");
          return this;
      }
        
      public CmsCommentSQL andPathIn(List<java.lang.String> values) {
          addCriterion("path", values, ConditionMode.IN, "path", "java.lang.String", "String");
          return this;
      }

      public CmsCommentSQL andPathNotIn(List<java.lang.String> values) {
          addCriterion("path", values, ConditionMode.NOT_IN, "path", "java.lang.String", "String");
          return this;
      }
	public CmsCommentSQL andReftypeIsNull() {
		isnull("reftype");
		return this;
	}
	
	public CmsCommentSQL andReftypeIsNotNull() {
		notNull("reftype");
		return this;
	}
	
	public CmsCommentSQL andReftypeIsEmpty() {
		empty("reftype");
		return this;
	}

	public CmsCommentSQL andReftypeIsNotEmpty() {
		notEmpty("reftype");
		return this;
	}
      public CmsCommentSQL andReftypeEqualTo(java.lang.Integer value) {
          addCriterion("reftype", value, ConditionMode.EQUAL, "reftype", "java.lang.Integer", "Float");
          return this;
      }

      public CmsCommentSQL andReftypeNotEqualTo(java.lang.Integer value) {
          addCriterion("reftype", value, ConditionMode.NOT_EQUAL, "reftype", "java.lang.Integer", "Float");
          return this;
      }

      public CmsCommentSQL andReftypeGreaterThan(java.lang.Integer value) {
          addCriterion("reftype", value, ConditionMode.GREATER_THEN, "reftype", "java.lang.Integer", "Float");
          return this;
      }

      public CmsCommentSQL andReftypeGreaterThanOrEqualTo(java.lang.Integer value) {
          addCriterion("reftype", value, ConditionMode.GREATER_EQUAL, "reftype", "java.lang.Integer", "Float");
          return this;
      }

      public CmsCommentSQL andReftypeLessThan(java.lang.Integer value) {
          addCriterion("reftype", value, ConditionMode.LESS_THEN, "reftype", "java.lang.Integer", "Float");
          return this;
      }

      public CmsCommentSQL andReftypeLessThanOrEqualTo(java.lang.Integer value) {
          addCriterion("reftype", value, ConditionMode.LESS_EQUAL, "reftype", "java.lang.Integer", "Float");
          return this;
      }

      public CmsCommentSQL andReftypeBetween(java.lang.Integer value1, java.lang.Integer value2) {
    	  addCriterion("reftype", value1, value2, ConditionMode.BETWEEN, "reftype", "java.lang.Integer", "Float");
    	  return this;
      }

      public CmsCommentSQL andReftypeNotBetween(java.lang.Integer value1, java.lang.Integer value2) {
          addCriterion("reftype", value1, value2, ConditionMode.NOT_BETWEEN, "reftype", "java.lang.Integer", "Float");
          return this;
      }
        
      public CmsCommentSQL andReftypeIn(List<java.lang.Integer> values) {
          addCriterion("reftype", values, ConditionMode.IN, "reftype", "java.lang.Integer", "Float");
          return this;
      }

      public CmsCommentSQL andReftypeNotIn(List<java.lang.Integer> values) {
          addCriterion("reftype", values, ConditionMode.NOT_IN, "reftype", "java.lang.Integer", "Float");
          return this;
      }
	public CmsCommentSQL andRefidIsNull() {
		isnull("refid");
		return this;
	}
	
	public CmsCommentSQL andRefidIsNotNull() {
		notNull("refid");
		return this;
	}
	
	public CmsCommentSQL andRefidIsEmpty() {
		empty("refid");
		return this;
	}

	public CmsCommentSQL andRefidIsNotEmpty() {
		notEmpty("refid");
		return this;
	}
      public CmsCommentSQL andRefidEqualTo(java.lang.Long value) {
          addCriterion("refid", value, ConditionMode.EQUAL, "refid", "java.lang.Long", "Float");
          return this;
      }

      public CmsCommentSQL andRefidNotEqualTo(java.lang.Long value) {
          addCriterion("refid", value, ConditionMode.NOT_EQUAL, "refid", "java.lang.Long", "Float");
          return this;
      }

      public CmsCommentSQL andRefidGreaterThan(java.lang.Long value) {
          addCriterion("refid", value, ConditionMode.GREATER_THEN, "refid", "java.lang.Long", "Float");
          return this;
      }

      public CmsCommentSQL andRefidGreaterThanOrEqualTo(java.lang.Long value) {
          addCriterion("refid", value, ConditionMode.GREATER_EQUAL, "refid", "java.lang.Long", "Float");
          return this;
      }

      public CmsCommentSQL andRefidLessThan(java.lang.Long value) {
          addCriterion("refid", value, ConditionMode.LESS_THEN, "refid", "java.lang.Long", "Float");
          return this;
      }

      public CmsCommentSQL andRefidLessThanOrEqualTo(java.lang.Long value) {
          addCriterion("refid", value, ConditionMode.LESS_EQUAL, "refid", "java.lang.Long", "Float");
          return this;
      }

      public CmsCommentSQL andRefidBetween(java.lang.Long value1, java.lang.Long value2) {
    	  addCriterion("refid", value1, value2, ConditionMode.BETWEEN, "refid", "java.lang.Long", "Float");
    	  return this;
      }

      public CmsCommentSQL andRefidNotBetween(java.lang.Long value1, java.lang.Long value2) {
          addCriterion("refid", value1, value2, ConditionMode.NOT_BETWEEN, "refid", "java.lang.Long", "Float");
          return this;
      }
        
      public CmsCommentSQL andRefidIn(List<java.lang.Long> values) {
          addCriterion("refid", values, ConditionMode.IN, "refid", "java.lang.Long", "Float");
          return this;
      }

      public CmsCommentSQL andRefidNotIn(List<java.lang.Long> values) {
          addCriterion("refid", values, ConditionMode.NOT_IN, "refid", "java.lang.Long", "Float");
          return this;
      }
	public CmsCommentSQL andUserIdIsNull() {
		isnull("user_id");
		return this;
	}
	
	public CmsCommentSQL andUserIdIsNotNull() {
		notNull("user_id");
		return this;
	}
	
	public CmsCommentSQL andUserIdIsEmpty() {
		empty("user_id");
		return this;
	}

	public CmsCommentSQL andUserIdIsNotEmpty() {
		notEmpty("user_id");
		return this;
	}
      public CmsCommentSQL andUserIdEqualTo(java.lang.Long value) {
          addCriterion("user_id", value, ConditionMode.EQUAL, "userId", "java.lang.Long", "Float");
          return this;
      }

      public CmsCommentSQL andUserIdNotEqualTo(java.lang.Long value) {
          addCriterion("user_id", value, ConditionMode.NOT_EQUAL, "userId", "java.lang.Long", "Float");
          return this;
      }

      public CmsCommentSQL andUserIdGreaterThan(java.lang.Long value) {
          addCriterion("user_id", value, ConditionMode.GREATER_THEN, "userId", "java.lang.Long", "Float");
          return this;
      }

      public CmsCommentSQL andUserIdGreaterThanOrEqualTo(java.lang.Long value) {
          addCriterion("user_id", value, ConditionMode.GREATER_EQUAL, "userId", "java.lang.Long", "Float");
          return this;
      }

      public CmsCommentSQL andUserIdLessThan(java.lang.Long value) {
          addCriterion("user_id", value, ConditionMode.LESS_THEN, "userId", "java.lang.Long", "Float");
          return this;
      }

      public CmsCommentSQL andUserIdLessThanOrEqualTo(java.lang.Long value) {
          addCriterion("user_id", value, ConditionMode.LESS_EQUAL, "userId", "java.lang.Long", "Float");
          return this;
      }

      public CmsCommentSQL andUserIdBetween(java.lang.Long value1, java.lang.Long value2) {
    	  addCriterion("user_id", value1, value2, ConditionMode.BETWEEN, "userId", "java.lang.Long", "Float");
    	  return this;
      }

      public CmsCommentSQL andUserIdNotBetween(java.lang.Long value1, java.lang.Long value2) {
          addCriterion("user_id", value1, value2, ConditionMode.NOT_BETWEEN, "userId", "java.lang.Long", "Float");
          return this;
      }
        
      public CmsCommentSQL andUserIdIn(List<java.lang.Long> values) {
          addCriterion("user_id", values, ConditionMode.IN, "userId", "java.lang.Long", "Float");
          return this;
      }

      public CmsCommentSQL andUserIdNotIn(List<java.lang.Long> values) {
          addCriterion("user_id", values, ConditionMode.NOT_IN, "userId", "java.lang.Long", "Float");
          return this;
      }
	public CmsCommentSQL andContentIsNull() {
		isnull("content");
		return this;
	}
	
	public CmsCommentSQL andContentIsNotNull() {
		notNull("content");
		return this;
	}
	
	public CmsCommentSQL andContentIsEmpty() {
		empty("content");
		return this;
	}

	public CmsCommentSQL andContentIsNotEmpty() {
		notEmpty("content");
		return this;
	}
       public CmsCommentSQL andContentLike(java.lang.String value) {
    	   addCriterion("content", value, ConditionMode.FUZZY, "content", "java.lang.String", "Float");
    	   return this;
      }

      public CmsCommentSQL andContentNotLike(java.lang.String value) {
          addCriterion("content", value, ConditionMode.NOT_FUZZY, "content", "java.lang.String", "Float");
          return this;
      }
      public CmsCommentSQL andContentEqualTo(java.lang.String value) {
          addCriterion("content", value, ConditionMode.EQUAL, "content", "java.lang.String", "String");
          return this;
      }

      public CmsCommentSQL andContentNotEqualTo(java.lang.String value) {
          addCriterion("content", value, ConditionMode.NOT_EQUAL, "content", "java.lang.String", "String");
          return this;
      }

      public CmsCommentSQL andContentGreaterThan(java.lang.String value) {
          addCriterion("content", value, ConditionMode.GREATER_THEN, "content", "java.lang.String", "String");
          return this;
      }

      public CmsCommentSQL andContentGreaterThanOrEqualTo(java.lang.String value) {
          addCriterion("content", value, ConditionMode.GREATER_EQUAL, "content", "java.lang.String", "String");
          return this;
      }

      public CmsCommentSQL andContentLessThan(java.lang.String value) {
          addCriterion("content", value, ConditionMode.LESS_THEN, "content", "java.lang.String", "String");
          return this;
      }

      public CmsCommentSQL andContentLessThanOrEqualTo(java.lang.String value) {
          addCriterion("content", value, ConditionMode.LESS_EQUAL, "content", "java.lang.String", "String");
          return this;
      }

      public CmsCommentSQL andContentBetween(java.lang.String value1, java.lang.String value2) {
    	  addCriterion("content", value1, value2, ConditionMode.BETWEEN, "content", "java.lang.String", "String");
    	  return this;
      }

      public CmsCommentSQL andContentNotBetween(java.lang.String value1, java.lang.String value2) {
          addCriterion("content", value1, value2, ConditionMode.NOT_BETWEEN, "content", "java.lang.String", "String");
          return this;
      }
        
      public CmsCommentSQL andContentIn(List<java.lang.String> values) {
          addCriterion("content", values, ConditionMode.IN, "content", "java.lang.String", "String");
          return this;
      }

      public CmsCommentSQL andContentNotIn(List<java.lang.String> values) {
          addCriterion("content", values, ConditionMode.NOT_IN, "content", "java.lang.String", "String");
          return this;
      }
	public CmsCommentSQL andStatusIsNull() {
		isnull("status");
		return this;
	}
	
	public CmsCommentSQL andStatusIsNotNull() {
		notNull("status");
		return this;
	}
	
	public CmsCommentSQL andStatusIsEmpty() {
		empty("status");
		return this;
	}

	public CmsCommentSQL andStatusIsNotEmpty() {
		notEmpty("status");
		return this;
	}
      public CmsCommentSQL andStatusEqualTo(java.lang.Integer value) {
          addCriterion("status", value, ConditionMode.EQUAL, "status", "java.lang.Integer", "Float");
          return this;
      }

      public CmsCommentSQL andStatusNotEqualTo(java.lang.Integer value) {
          addCriterion("status", value, ConditionMode.NOT_EQUAL, "status", "java.lang.Integer", "Float");
          return this;
      }

      public CmsCommentSQL andStatusGreaterThan(java.lang.Integer value) {
          addCriterion("status", value, ConditionMode.GREATER_THEN, "status", "java.lang.Integer", "Float");
          return this;
      }

      public CmsCommentSQL andStatusGreaterThanOrEqualTo(java.lang.Integer value) {
          addCriterion("status", value, ConditionMode.GREATER_EQUAL, "status", "java.lang.Integer", "Float");
          return this;
      }

      public CmsCommentSQL andStatusLessThan(java.lang.Integer value) {
          addCriterion("status", value, ConditionMode.LESS_THEN, "status", "java.lang.Integer", "Float");
          return this;
      }

      public CmsCommentSQL andStatusLessThanOrEqualTo(java.lang.Integer value) {
          addCriterion("status", value, ConditionMode.LESS_EQUAL, "status", "java.lang.Integer", "Float");
          return this;
      }

      public CmsCommentSQL andStatusBetween(java.lang.Integer value1, java.lang.Integer value2) {
    	  addCriterion("status", value1, value2, ConditionMode.BETWEEN, "status", "java.lang.Integer", "Float");
    	  return this;
      }

      public CmsCommentSQL andStatusNotBetween(java.lang.Integer value1, java.lang.Integer value2) {
          addCriterion("status", value1, value2, ConditionMode.NOT_BETWEEN, "status", "java.lang.Integer", "Float");
          return this;
      }
        
      public CmsCommentSQL andStatusIn(List<java.lang.Integer> values) {
          addCriterion("status", values, ConditionMode.IN, "status", "java.lang.Integer", "Float");
          return this;
      }

      public CmsCommentSQL andStatusNotIn(List<java.lang.Integer> values) {
          addCriterion("status", values, ConditionMode.NOT_IN, "status", "java.lang.Integer", "Float");
          return this;
      }
	public CmsCommentSQL andIpIsNull() {
		isnull("ip");
		return this;
	}
	
	public CmsCommentSQL andIpIsNotNull() {
		notNull("ip");
		return this;
	}
	
	public CmsCommentSQL andIpIsEmpty() {
		empty("ip");
		return this;
	}

	public CmsCommentSQL andIpIsNotEmpty() {
		notEmpty("ip");
		return this;
	}
       public CmsCommentSQL andIpLike(java.lang.String value) {
    	   addCriterion("ip", value, ConditionMode.FUZZY, "ip", "java.lang.String", "Float");
    	   return this;
      }

      public CmsCommentSQL andIpNotLike(java.lang.String value) {
          addCriterion("ip", value, ConditionMode.NOT_FUZZY, "ip", "java.lang.String", "Float");
          return this;
      }
      public CmsCommentSQL andIpEqualTo(java.lang.String value) {
          addCriterion("ip", value, ConditionMode.EQUAL, "ip", "java.lang.String", "String");
          return this;
      }

      public CmsCommentSQL andIpNotEqualTo(java.lang.String value) {
          addCriterion("ip", value, ConditionMode.NOT_EQUAL, "ip", "java.lang.String", "String");
          return this;
      }

      public CmsCommentSQL andIpGreaterThan(java.lang.String value) {
          addCriterion("ip", value, ConditionMode.GREATER_THEN, "ip", "java.lang.String", "String");
          return this;
      }

      public CmsCommentSQL andIpGreaterThanOrEqualTo(java.lang.String value) {
          addCriterion("ip", value, ConditionMode.GREATER_EQUAL, "ip", "java.lang.String", "String");
          return this;
      }

      public CmsCommentSQL andIpLessThan(java.lang.String value) {
          addCriterion("ip", value, ConditionMode.LESS_THEN, "ip", "java.lang.String", "String");
          return this;
      }

      public CmsCommentSQL andIpLessThanOrEqualTo(java.lang.String value) {
          addCriterion("ip", value, ConditionMode.LESS_EQUAL, "ip", "java.lang.String", "String");
          return this;
      }

      public CmsCommentSQL andIpBetween(java.lang.String value1, java.lang.String value2) {
    	  addCriterion("ip", value1, value2, ConditionMode.BETWEEN, "ip", "java.lang.String", "String");
    	  return this;
      }

      public CmsCommentSQL andIpNotBetween(java.lang.String value1, java.lang.String value2) {
          addCriterion("ip", value1, value2, ConditionMode.NOT_BETWEEN, "ip", "java.lang.String", "String");
          return this;
      }
        
      public CmsCommentSQL andIpIn(List<java.lang.String> values) {
          addCriterion("ip", values, ConditionMode.IN, "ip", "java.lang.String", "String");
          return this;
      }

      public CmsCommentSQL andIpNotIn(List<java.lang.String> values) {
          addCriterion("ip", values, ConditionMode.NOT_IN, "ip", "java.lang.String", "String");
          return this;
      }
	public CmsCommentSQL andAgentIsNull() {
		isnull("agent");
		return this;
	}
	
	public CmsCommentSQL andAgentIsNotNull() {
		notNull("agent");
		return this;
	}
	
	public CmsCommentSQL andAgentIsEmpty() {
		empty("agent");
		return this;
	}

	public CmsCommentSQL andAgentIsNotEmpty() {
		notEmpty("agent");
		return this;
	}
       public CmsCommentSQL andAgentLike(java.lang.String value) {
    	   addCriterion("agent", value, ConditionMode.FUZZY, "agent", "java.lang.String", "String");
    	   return this;
      }

      public CmsCommentSQL andAgentNotLike(java.lang.String value) {
          addCriterion("agent", value, ConditionMode.NOT_FUZZY, "agent", "java.lang.String", "String");
          return this;
      }
      public CmsCommentSQL andAgentEqualTo(java.lang.String value) {
          addCriterion("agent", value, ConditionMode.EQUAL, "agent", "java.lang.String", "String");
          return this;
      }

      public CmsCommentSQL andAgentNotEqualTo(java.lang.String value) {
          addCriterion("agent", value, ConditionMode.NOT_EQUAL, "agent", "java.lang.String", "String");
          return this;
      }

      public CmsCommentSQL andAgentGreaterThan(java.lang.String value) {
          addCriterion("agent", value, ConditionMode.GREATER_THEN, "agent", "java.lang.String", "String");
          return this;
      }

      public CmsCommentSQL andAgentGreaterThanOrEqualTo(java.lang.String value) {
          addCriterion("agent", value, ConditionMode.GREATER_EQUAL, "agent", "java.lang.String", "String");
          return this;
      }

      public CmsCommentSQL andAgentLessThan(java.lang.String value) {
          addCriterion("agent", value, ConditionMode.LESS_THEN, "agent", "java.lang.String", "String");
          return this;
      }

      public CmsCommentSQL andAgentLessThanOrEqualTo(java.lang.String value) {
          addCriterion("agent", value, ConditionMode.LESS_EQUAL, "agent", "java.lang.String", "String");
          return this;
      }

      public CmsCommentSQL andAgentBetween(java.lang.String value1, java.lang.String value2) {
    	  addCriterion("agent", value1, value2, ConditionMode.BETWEEN, "agent", "java.lang.String", "String");
    	  return this;
      }

      public CmsCommentSQL andAgentNotBetween(java.lang.String value1, java.lang.String value2) {
          addCriterion("agent", value1, value2, ConditionMode.NOT_BETWEEN, "agent", "java.lang.String", "String");
          return this;
      }
        
      public CmsCommentSQL andAgentIn(List<java.lang.String> values) {
          addCriterion("agent", values, ConditionMode.IN, "agent", "java.lang.String", "String");
          return this;
      }

      public CmsCommentSQL andAgentNotIn(List<java.lang.String> values) {
          addCriterion("agent", values, ConditionMode.NOT_IN, "agent", "java.lang.String", "String");
          return this;
      }
	public CmsCommentSQL andSystemIdIsNull() {
		isnull("system_id");
		return this;
	}
	
	public CmsCommentSQL andSystemIdIsNotNull() {
		notNull("system_id");
		return this;
	}
	
	public CmsCommentSQL andSystemIdIsEmpty() {
		empty("system_id");
		return this;
	}

	public CmsCommentSQL andSystemIdIsNotEmpty() {
		notEmpty("system_id");
		return this;
	}
      public CmsCommentSQL andSystemIdEqualTo(java.lang.Integer value) {
          addCriterion("system_id", value, ConditionMode.EQUAL, "systemId", "java.lang.Integer", "Float");
          return this;
      }

      public CmsCommentSQL andSystemIdNotEqualTo(java.lang.Integer value) {
          addCriterion("system_id", value, ConditionMode.NOT_EQUAL, "systemId", "java.lang.Integer", "Float");
          return this;
      }

      public CmsCommentSQL andSystemIdGreaterThan(java.lang.Integer value) {
          addCriterion("system_id", value, ConditionMode.GREATER_THEN, "systemId", "java.lang.Integer", "Float");
          return this;
      }

      public CmsCommentSQL andSystemIdGreaterThanOrEqualTo(java.lang.Integer value) {
          addCriterion("system_id", value, ConditionMode.GREATER_EQUAL, "systemId", "java.lang.Integer", "Float");
          return this;
      }

      public CmsCommentSQL andSystemIdLessThan(java.lang.Integer value) {
          addCriterion("system_id", value, ConditionMode.LESS_THEN, "systemId", "java.lang.Integer", "Float");
          return this;
      }

      public CmsCommentSQL andSystemIdLessThanOrEqualTo(java.lang.Integer value) {
          addCriterion("system_id", value, ConditionMode.LESS_EQUAL, "systemId", "java.lang.Integer", "Float");
          return this;
      }

      public CmsCommentSQL andSystemIdBetween(java.lang.Integer value1, java.lang.Integer value2) {
    	  addCriterion("system_id", value1, value2, ConditionMode.BETWEEN, "systemId", "java.lang.Integer", "Float");
    	  return this;
      }

      public CmsCommentSQL andSystemIdNotBetween(java.lang.Integer value1, java.lang.Integer value2) {
          addCriterion("system_id", value1, value2, ConditionMode.NOT_BETWEEN, "systemId", "java.lang.Integer", "Float");
          return this;
      }
        
      public CmsCommentSQL andSystemIdIn(List<java.lang.Integer> values) {
          addCriterion("system_id", values, ConditionMode.IN, "systemId", "java.lang.Integer", "Float");
          return this;
      }

      public CmsCommentSQL andSystemIdNotIn(List<java.lang.Integer> values) {
          addCriterion("system_id", values, ConditionMode.NOT_IN, "systemId", "java.lang.Integer", "Float");
          return this;
      }
	public CmsCommentSQL andCtimeIsNull() {
		isnull("ctime");
		return this;
	}
	
	public CmsCommentSQL andCtimeIsNotNull() {
		notNull("ctime");
		return this;
	}
	
	public CmsCommentSQL andCtimeIsEmpty() {
		empty("ctime");
		return this;
	}

	public CmsCommentSQL andCtimeIsNotEmpty() {
		notEmpty("ctime");
		return this;
	}
      public CmsCommentSQL andCtimeEqualTo(java.util.Date value) {
          addCriterion("ctime", value, ConditionMode.EQUAL, "ctime", "java.util.Date", "String");
          return this;
      }

      public CmsCommentSQL andCtimeNotEqualTo(java.util.Date value) {
          addCriterion("ctime", value, ConditionMode.NOT_EQUAL, "ctime", "java.util.Date", "String");
          return this;
      }

      public CmsCommentSQL andCtimeGreaterThan(java.util.Date value) {
          addCriterion("ctime", value, ConditionMode.GREATER_THEN, "ctime", "java.util.Date", "String");
          return this;
      }

      public CmsCommentSQL andCtimeGreaterThanOrEqualTo(java.util.Date value) {
          addCriterion("ctime", value, ConditionMode.GREATER_EQUAL, "ctime", "java.util.Date", "String");
          return this;
      }

      public CmsCommentSQL andCtimeLessThan(java.util.Date value) {
          addCriterion("ctime", value, ConditionMode.LESS_THEN, "ctime", "java.util.Date", "String");
          return this;
      }

      public CmsCommentSQL andCtimeLessThanOrEqualTo(java.util.Date value) {
          addCriterion("ctime", value, ConditionMode.LESS_EQUAL, "ctime", "java.util.Date", "String");
          return this;
      }

      public CmsCommentSQL andCtimeBetween(java.util.Date value1, java.util.Date value2) {
    	  addCriterion("ctime", value1, value2, ConditionMode.BETWEEN, "ctime", "java.util.Date", "String");
    	  return this;
      }

      public CmsCommentSQL andCtimeNotBetween(java.util.Date value1, java.util.Date value2) {
          addCriterion("ctime", value1, value2, ConditionMode.NOT_BETWEEN, "ctime", "java.util.Date", "String");
          return this;
      }
        
      public CmsCommentSQL andCtimeIn(List<java.util.Date> values) {
          addCriterion("ctime", values, ConditionMode.IN, "ctime", "java.util.Date", "String");
          return this;
      }

      public CmsCommentSQL andCtimeNotIn(List<java.util.Date> values) {
          addCriterion("ctime", values, ConditionMode.NOT_IN, "ctime", "java.util.Date", "String");
          return this;
      }
}