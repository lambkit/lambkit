/**
 * Copyright (c) 2015-2017, Henry Yang 杨勇 (gismail@foxmail.com).
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.lambkit.module.cms.sql;

import com.lambkit.db.sql.ConditionMode;
import com.lambkit.db.sql.Column;
import com.lambkit.db.sql.Columns;
import com.lambkit.db.sql.Example;

import java.io.Serializable;
import java.util.List;

/**
 * @author yangyong 
 * @website: www.lambkit.com
 * @email: gismail@foxmail.com
 * @date ${date}
 * @version 1.0
 * @since 1.0
 */

public class CmsPageTemplateSQL extends Columns implements Serializable {
	private static final long serialVersionUID = 1L;
	
	public static CmsPageTemplateSQL of() {
		return new CmsPageTemplateSQL();
	}
	
	public static CmsPageTemplateSQL by(Column column) {
		CmsPageTemplateSQL that = new CmsPageTemplateSQL();
		that.add(column);
        return that;
    }

    public static CmsPageTemplateSQL by(String name, Object value) {
        return (CmsPageTemplateSQL) create().eq(name, value);
    }
    
    public Example example() {
    	return Example.create("cms_page_template", this);
    }
    
    /**
     * equals
     *
     * @param name
     * @param value
     * @return
     */
    public CmsPageTemplateSQL eq(String name, Object value) {
    	super.eq(name, value);
        return this;
    }

    /**
     * not equals !=
     *
     * @param name
     * @param value
     * @return
     */
    public CmsPageTemplateSQL ne(String name, Object value) {
    	super.ne(name, value);
        return this;
    }


    /**
     * like
     *
     * @param name
     * @param value
     * @return
     */

    public CmsPageTemplateSQL like(String name, Object value) {
    	super.like(name, value);
        return this;
    }
    
    public CmsPageTemplateSQL notLike(String name, Object value) {
    	super.notLike(name, value);
        return this;
    }

    /**
     * 大于 great than
     *
     * @param name
     * @param value
     * @return
     */
    public CmsPageTemplateSQL gt(String name, Object value) {
    	super.gt(name, value);
        return this;
    }

    /**
     * 大于等于 great or equal
     *
     * @param name
     * @param value
     * @return
     */
    public CmsPageTemplateSQL ge(String name, Object value) {
    	super.ge(name, value);
        return this;
    }

    /**
     * 小于 less than
     *
     * @param name
     * @param value
     * @return
     */
    public CmsPageTemplateSQL lt(String name, Object value) {
    	super.lt(name, value);
        return this;
    }

    /**
     * 小于等于 less or equal
     *
     * @param name
     * @param value
     * @return
     */
    public CmsPageTemplateSQL le(String name, Object value) {
    	super.le(name, value);
        return this;
    }
    
    public CmsPageTemplateSQL isnull(String name) {
    	super.isnull(name);
        return this;
    } 

    public CmsPageTemplateSQL notNull(String name) {
    	super.notNull(name);
        return this;
    } 
    
    public CmsPageTemplateSQL empty(String name) {
    	super.empty(name);
        return this;
    } 
    
    public CmsPageTemplateSQL notEmpty(String name) {
    	super.notEmpty(name);
        return this;
    } 
    
    public CmsPageTemplateSQL add(Column column) {
    	super.add(column);
    	return this;
    }
    
    /**************************/
	
	public void addCriterion(String name, Object value, ConditionMode logic, String property, String typeHandler, String valueType) {
		 if (value == null) {
			 throw new RuntimeException("Value for " + property + " cannot be null");
		 }
		 add(Column.create(name, value, logic, typeHandler, valueType));
	}
   
	public void addCriterion(String name, Object value1, Object value2, ConditionMode logic, String property, String typeHandler, String valueType) {
		 if (value1 == null || value2 == null) {
			 throw new RuntimeException("Between values for " + property + " cannot be null");
		 }
		 add(Column.create(name, value1, value2, logic, typeHandler, valueType));
	}
		 
	public CmsPageTemplateSQL andTempIdIsNull() {
		isnull("temp_id");
		return this;
	}
	
	public CmsPageTemplateSQL andTempIdIsNotNull() {
		notNull("temp_id");
		return this;
	}
	
	public CmsPageTemplateSQL andTempIdIsEmpty() {
		empty("temp_id");
		return this;
	}

	public CmsPageTemplateSQL andTempIdIsNotEmpty() {
		notEmpty("temp_id");
		return this;
	}
      public CmsPageTemplateSQL andTempIdEqualTo(java.lang.Long value) {
          addCriterion("temp_id", value, ConditionMode.EQUAL, "tempId", "java.lang.Long", "Float");
          return this;
      }

      public CmsPageTemplateSQL andTempIdNotEqualTo(java.lang.Long value) {
          addCriterion("temp_id", value, ConditionMode.NOT_EQUAL, "tempId", "java.lang.Long", "Float");
          return this;
      }

      public CmsPageTemplateSQL andTempIdGreaterThan(java.lang.Long value) {
          addCriterion("temp_id", value, ConditionMode.GREATER_THEN, "tempId", "java.lang.Long", "Float");
          return this;
      }

      public CmsPageTemplateSQL andTempIdGreaterThanOrEqualTo(java.lang.Long value) {
          addCriterion("temp_id", value, ConditionMode.GREATER_EQUAL, "tempId", "java.lang.Long", "Float");
          return this;
      }

      public CmsPageTemplateSQL andTempIdLessThan(java.lang.Long value) {
          addCriterion("temp_id", value, ConditionMode.LESS_THEN, "tempId", "java.lang.Long", "Float");
          return this;
      }

      public CmsPageTemplateSQL andTempIdLessThanOrEqualTo(java.lang.Long value) {
          addCriterion("temp_id", value, ConditionMode.LESS_EQUAL, "tempId", "java.lang.Long", "Float");
          return this;
      }

      public CmsPageTemplateSQL andTempIdBetween(java.lang.Long value1, java.lang.Long value2) {
    	  addCriterion("temp_id", value1, value2, ConditionMode.BETWEEN, "tempId", "java.lang.Long", "Float");
    	  return this;
      }

      public CmsPageTemplateSQL andTempIdNotBetween(java.lang.Long value1, java.lang.Long value2) {
          addCriterion("temp_id", value1, value2, ConditionMode.NOT_BETWEEN, "tempId", "java.lang.Long", "Float");
          return this;
      }
        
      public CmsPageTemplateSQL andTempIdIn(List<java.lang.Long> values) {
          addCriterion("temp_id", values, ConditionMode.IN, "tempId", "java.lang.Long", "Float");
          return this;
      }

      public CmsPageTemplateSQL andTempIdNotIn(List<java.lang.Long> values) {
          addCriterion("temp_id", values, ConditionMode.NOT_IN, "tempId", "java.lang.Long", "Float");
          return this;
      }
	public CmsPageTemplateSQL andNameIsNull() {
		isnull("name");
		return this;
	}
	
	public CmsPageTemplateSQL andNameIsNotNull() {
		notNull("name");
		return this;
	}
	
	public CmsPageTemplateSQL andNameIsEmpty() {
		empty("name");
		return this;
	}

	public CmsPageTemplateSQL andNameIsNotEmpty() {
		notEmpty("name");
		return this;
	}
       public CmsPageTemplateSQL andNameLike(java.lang.String value) {
    	   addCriterion("name", value, ConditionMode.FUZZY, "name", "java.lang.String", "Float");
    	   return this;
      }

      public CmsPageTemplateSQL andNameNotLike(java.lang.String value) {
          addCriterion("name", value, ConditionMode.NOT_FUZZY, "name", "java.lang.String", "Float");
          return this;
      }
      public CmsPageTemplateSQL andNameEqualTo(java.lang.String value) {
          addCriterion("name", value, ConditionMode.EQUAL, "name", "java.lang.String", "String");
          return this;
      }

      public CmsPageTemplateSQL andNameNotEqualTo(java.lang.String value) {
          addCriterion("name", value, ConditionMode.NOT_EQUAL, "name", "java.lang.String", "String");
          return this;
      }

      public CmsPageTemplateSQL andNameGreaterThan(java.lang.String value) {
          addCriterion("name", value, ConditionMode.GREATER_THEN, "name", "java.lang.String", "String");
          return this;
      }

      public CmsPageTemplateSQL andNameGreaterThanOrEqualTo(java.lang.String value) {
          addCriterion("name", value, ConditionMode.GREATER_EQUAL, "name", "java.lang.String", "String");
          return this;
      }

      public CmsPageTemplateSQL andNameLessThan(java.lang.String value) {
          addCriterion("name", value, ConditionMode.LESS_THEN, "name", "java.lang.String", "String");
          return this;
      }

      public CmsPageTemplateSQL andNameLessThanOrEqualTo(java.lang.String value) {
          addCriterion("name", value, ConditionMode.LESS_EQUAL, "name", "java.lang.String", "String");
          return this;
      }

      public CmsPageTemplateSQL andNameBetween(java.lang.String value1, java.lang.String value2) {
    	  addCriterion("name", value1, value2, ConditionMode.BETWEEN, "name", "java.lang.String", "String");
    	  return this;
      }

      public CmsPageTemplateSQL andNameNotBetween(java.lang.String value1, java.lang.String value2) {
          addCriterion("name", value1, value2, ConditionMode.NOT_BETWEEN, "name", "java.lang.String", "String");
          return this;
      }
        
      public CmsPageTemplateSQL andNameIn(List<java.lang.String> values) {
          addCriterion("name", values, ConditionMode.IN, "name", "java.lang.String", "String");
          return this;
      }

      public CmsPageTemplateSQL andNameNotIn(List<java.lang.String> values) {
          addCriterion("name", values, ConditionMode.NOT_IN, "name", "java.lang.String", "String");
          return this;
      }
	public CmsPageTemplateSQL andTitleIsNull() {
		isnull("title");
		return this;
	}
	
	public CmsPageTemplateSQL andTitleIsNotNull() {
		notNull("title");
		return this;
	}
	
	public CmsPageTemplateSQL andTitleIsEmpty() {
		empty("title");
		return this;
	}

	public CmsPageTemplateSQL andTitleIsNotEmpty() {
		notEmpty("title");
		return this;
	}
       public CmsPageTemplateSQL andTitleLike(java.lang.String value) {
    	   addCriterion("title", value, ConditionMode.FUZZY, "title", "java.lang.String", "String");
    	   return this;
      }

      public CmsPageTemplateSQL andTitleNotLike(java.lang.String value) {
          addCriterion("title", value, ConditionMode.NOT_FUZZY, "title", "java.lang.String", "String");
          return this;
      }
      public CmsPageTemplateSQL andTitleEqualTo(java.lang.String value) {
          addCriterion("title", value, ConditionMode.EQUAL, "title", "java.lang.String", "String");
          return this;
      }

      public CmsPageTemplateSQL andTitleNotEqualTo(java.lang.String value) {
          addCriterion("title", value, ConditionMode.NOT_EQUAL, "title", "java.lang.String", "String");
          return this;
      }

      public CmsPageTemplateSQL andTitleGreaterThan(java.lang.String value) {
          addCriterion("title", value, ConditionMode.GREATER_THEN, "title", "java.lang.String", "String");
          return this;
      }

      public CmsPageTemplateSQL andTitleGreaterThanOrEqualTo(java.lang.String value) {
          addCriterion("title", value, ConditionMode.GREATER_EQUAL, "title", "java.lang.String", "String");
          return this;
      }

      public CmsPageTemplateSQL andTitleLessThan(java.lang.String value) {
          addCriterion("title", value, ConditionMode.LESS_THEN, "title", "java.lang.String", "String");
          return this;
      }

      public CmsPageTemplateSQL andTitleLessThanOrEqualTo(java.lang.String value) {
          addCriterion("title", value, ConditionMode.LESS_EQUAL, "title", "java.lang.String", "String");
          return this;
      }

      public CmsPageTemplateSQL andTitleBetween(java.lang.String value1, java.lang.String value2) {
    	  addCriterion("title", value1, value2, ConditionMode.BETWEEN, "title", "java.lang.String", "String");
    	  return this;
      }

      public CmsPageTemplateSQL andTitleNotBetween(java.lang.String value1, java.lang.String value2) {
          addCriterion("title", value1, value2, ConditionMode.NOT_BETWEEN, "title", "java.lang.String", "String");
          return this;
      }
        
      public CmsPageTemplateSQL andTitleIn(List<java.lang.String> values) {
          addCriterion("title", values, ConditionMode.IN, "title", "java.lang.String", "String");
          return this;
      }

      public CmsPageTemplateSQL andTitleNotIn(List<java.lang.String> values) {
          addCriterion("title", values, ConditionMode.NOT_IN, "title", "java.lang.String", "String");
          return this;
      }
	public CmsPageTemplateSQL andCatalogIsNull() {
		isnull("catalog");
		return this;
	}
	
	public CmsPageTemplateSQL andCatalogIsNotNull() {
		notNull("catalog");
		return this;
	}
	
	public CmsPageTemplateSQL andCatalogIsEmpty() {
		empty("catalog");
		return this;
	}

	public CmsPageTemplateSQL andCatalogIsNotEmpty() {
		notEmpty("catalog");
		return this;
	}
       public CmsPageTemplateSQL andCatalogLike(java.lang.String value) {
    	   addCriterion("catalog", value, ConditionMode.FUZZY, "catalog", "java.lang.String", "String");
    	   return this;
      }

      public CmsPageTemplateSQL andCatalogNotLike(java.lang.String value) {
          addCriterion("catalog", value, ConditionMode.NOT_FUZZY, "catalog", "java.lang.String", "String");
          return this;
      }
      public CmsPageTemplateSQL andCatalogEqualTo(java.lang.String value) {
          addCriterion("catalog", value, ConditionMode.EQUAL, "catalog", "java.lang.String", "String");
          return this;
      }

      public CmsPageTemplateSQL andCatalogNotEqualTo(java.lang.String value) {
          addCriterion("catalog", value, ConditionMode.NOT_EQUAL, "catalog", "java.lang.String", "String");
          return this;
      }

      public CmsPageTemplateSQL andCatalogGreaterThan(java.lang.String value) {
          addCriterion("catalog", value, ConditionMode.GREATER_THEN, "catalog", "java.lang.String", "String");
          return this;
      }

      public CmsPageTemplateSQL andCatalogGreaterThanOrEqualTo(java.lang.String value) {
          addCriterion("catalog", value, ConditionMode.GREATER_EQUAL, "catalog", "java.lang.String", "String");
          return this;
      }

      public CmsPageTemplateSQL andCatalogLessThan(java.lang.String value) {
          addCriterion("catalog", value, ConditionMode.LESS_THEN, "catalog", "java.lang.String", "String");
          return this;
      }

      public CmsPageTemplateSQL andCatalogLessThanOrEqualTo(java.lang.String value) {
          addCriterion("catalog", value, ConditionMode.LESS_EQUAL, "catalog", "java.lang.String", "String");
          return this;
      }

      public CmsPageTemplateSQL andCatalogBetween(java.lang.String value1, java.lang.String value2) {
    	  addCriterion("catalog", value1, value2, ConditionMode.BETWEEN, "catalog", "java.lang.String", "String");
    	  return this;
      }

      public CmsPageTemplateSQL andCatalogNotBetween(java.lang.String value1, java.lang.String value2) {
          addCriterion("catalog", value1, value2, ConditionMode.NOT_BETWEEN, "catalog", "java.lang.String", "String");
          return this;
      }
        
      public CmsPageTemplateSQL andCatalogIn(List<java.lang.String> values) {
          addCriterion("catalog", values, ConditionMode.IN, "catalog", "java.lang.String", "String");
          return this;
      }

      public CmsPageTemplateSQL andCatalogNotIn(List<java.lang.String> values) {
          addCriterion("catalog", values, ConditionMode.NOT_IN, "catalog", "java.lang.String", "String");
          return this;
      }
	public CmsPageTemplateSQL andTypeIsNull() {
		isnull("type");
		return this;
	}
	
	public CmsPageTemplateSQL andTypeIsNotNull() {
		notNull("type");
		return this;
	}
	
	public CmsPageTemplateSQL andTypeIsEmpty() {
		empty("type");
		return this;
	}

	public CmsPageTemplateSQL andTypeIsNotEmpty() {
		notEmpty("type");
		return this;
	}
       public CmsPageTemplateSQL andTypeLike(java.lang.String value) {
    	   addCriterion("type", value, ConditionMode.FUZZY, "type", "java.lang.String", "String");
    	   return this;
      }

      public CmsPageTemplateSQL andTypeNotLike(java.lang.String value) {
          addCriterion("type", value, ConditionMode.NOT_FUZZY, "type", "java.lang.String", "String");
          return this;
      }
      public CmsPageTemplateSQL andTypeEqualTo(java.lang.String value) {
          addCriterion("type", value, ConditionMode.EQUAL, "type", "java.lang.String", "String");
          return this;
      }

      public CmsPageTemplateSQL andTypeNotEqualTo(java.lang.String value) {
          addCriterion("type", value, ConditionMode.NOT_EQUAL, "type", "java.lang.String", "String");
          return this;
      }

      public CmsPageTemplateSQL andTypeGreaterThan(java.lang.String value) {
          addCriterion("type", value, ConditionMode.GREATER_THEN, "type", "java.lang.String", "String");
          return this;
      }

      public CmsPageTemplateSQL andTypeGreaterThanOrEqualTo(java.lang.String value) {
          addCriterion("type", value, ConditionMode.GREATER_EQUAL, "type", "java.lang.String", "String");
          return this;
      }

      public CmsPageTemplateSQL andTypeLessThan(java.lang.String value) {
          addCriterion("type", value, ConditionMode.LESS_THEN, "type", "java.lang.String", "String");
          return this;
      }

      public CmsPageTemplateSQL andTypeLessThanOrEqualTo(java.lang.String value) {
          addCriterion("type", value, ConditionMode.LESS_EQUAL, "type", "java.lang.String", "String");
          return this;
      }

      public CmsPageTemplateSQL andTypeBetween(java.lang.String value1, java.lang.String value2) {
    	  addCriterion("type", value1, value2, ConditionMode.BETWEEN, "type", "java.lang.String", "String");
    	  return this;
      }

      public CmsPageTemplateSQL andTypeNotBetween(java.lang.String value1, java.lang.String value2) {
          addCriterion("type", value1, value2, ConditionMode.NOT_BETWEEN, "type", "java.lang.String", "String");
          return this;
      }
        
      public CmsPageTemplateSQL andTypeIn(List<java.lang.String> values) {
          addCriterion("type", values, ConditionMode.IN, "type", "java.lang.String", "String");
          return this;
      }

      public CmsPageTemplateSQL andTypeNotIn(List<java.lang.String> values) {
          addCriterion("type", values, ConditionMode.NOT_IN, "type", "java.lang.String", "String");
          return this;
      }
	public CmsPageTemplateSQL andContentIsNull() {
		isnull("content");
		return this;
	}
	
	public CmsPageTemplateSQL andContentIsNotNull() {
		notNull("content");
		return this;
	}
	
	public CmsPageTemplateSQL andContentIsEmpty() {
		empty("content");
		return this;
	}

	public CmsPageTemplateSQL andContentIsNotEmpty() {
		notEmpty("content");
		return this;
	}
       public CmsPageTemplateSQL andContentLike(java.lang.String value) {
    	   addCriterion("content", value, ConditionMode.FUZZY, "content", "java.lang.String", "String");
    	   return this;
      }

      public CmsPageTemplateSQL andContentNotLike(java.lang.String value) {
          addCriterion("content", value, ConditionMode.NOT_FUZZY, "content", "java.lang.String", "String");
          return this;
      }
      public CmsPageTemplateSQL andContentEqualTo(java.lang.String value) {
          addCriterion("content", value, ConditionMode.EQUAL, "content", "java.lang.String", "String");
          return this;
      }

      public CmsPageTemplateSQL andContentNotEqualTo(java.lang.String value) {
          addCriterion("content", value, ConditionMode.NOT_EQUAL, "content", "java.lang.String", "String");
          return this;
      }

      public CmsPageTemplateSQL andContentGreaterThan(java.lang.String value) {
          addCriterion("content", value, ConditionMode.GREATER_THEN, "content", "java.lang.String", "String");
          return this;
      }

      public CmsPageTemplateSQL andContentGreaterThanOrEqualTo(java.lang.String value) {
          addCriterion("content", value, ConditionMode.GREATER_EQUAL, "content", "java.lang.String", "String");
          return this;
      }

      public CmsPageTemplateSQL andContentLessThan(java.lang.String value) {
          addCriterion("content", value, ConditionMode.LESS_THEN, "content", "java.lang.String", "String");
          return this;
      }

      public CmsPageTemplateSQL andContentLessThanOrEqualTo(java.lang.String value) {
          addCriterion("content", value, ConditionMode.LESS_EQUAL, "content", "java.lang.String", "String");
          return this;
      }

      public CmsPageTemplateSQL andContentBetween(java.lang.String value1, java.lang.String value2) {
    	  addCriterion("content", value1, value2, ConditionMode.BETWEEN, "content", "java.lang.String", "String");
    	  return this;
      }

      public CmsPageTemplateSQL andContentNotBetween(java.lang.String value1, java.lang.String value2) {
          addCriterion("content", value1, value2, ConditionMode.NOT_BETWEEN, "content", "java.lang.String", "String");
          return this;
      }
        
      public CmsPageTemplateSQL andContentIn(List<java.lang.String> values) {
          addCriterion("content", values, ConditionMode.IN, "content", "java.lang.String", "String");
          return this;
      }

      public CmsPageTemplateSQL andContentNotIn(List<java.lang.String> values) {
          addCriterion("content", values, ConditionMode.NOT_IN, "content", "java.lang.String", "String");
          return this;
      }
	public CmsPageTemplateSQL andEditModeIsNull() {
		isnull("edit_mode");
		return this;
	}
	
	public CmsPageTemplateSQL andEditModeIsNotNull() {
		notNull("edit_mode");
		return this;
	}
	
	public CmsPageTemplateSQL andEditModeIsEmpty() {
		empty("edit_mode");
		return this;
	}

	public CmsPageTemplateSQL andEditModeIsNotEmpty() {
		notEmpty("edit_mode");
		return this;
	}
       public CmsPageTemplateSQL andEditModeLike(java.lang.String value) {
    	   addCriterion("edit_mode", value, ConditionMode.FUZZY, "editMode", "java.lang.String", "String");
    	   return this;
      }

      public CmsPageTemplateSQL andEditModeNotLike(java.lang.String value) {
          addCriterion("edit_mode", value, ConditionMode.NOT_FUZZY, "editMode", "java.lang.String", "String");
          return this;
      }
      public CmsPageTemplateSQL andEditModeEqualTo(java.lang.String value) {
          addCriterion("edit_mode", value, ConditionMode.EQUAL, "editMode", "java.lang.String", "String");
          return this;
      }

      public CmsPageTemplateSQL andEditModeNotEqualTo(java.lang.String value) {
          addCriterion("edit_mode", value, ConditionMode.NOT_EQUAL, "editMode", "java.lang.String", "String");
          return this;
      }

      public CmsPageTemplateSQL andEditModeGreaterThan(java.lang.String value) {
          addCriterion("edit_mode", value, ConditionMode.GREATER_THEN, "editMode", "java.lang.String", "String");
          return this;
      }

      public CmsPageTemplateSQL andEditModeGreaterThanOrEqualTo(java.lang.String value) {
          addCriterion("edit_mode", value, ConditionMode.GREATER_EQUAL, "editMode", "java.lang.String", "String");
          return this;
      }

      public CmsPageTemplateSQL andEditModeLessThan(java.lang.String value) {
          addCriterion("edit_mode", value, ConditionMode.LESS_THEN, "editMode", "java.lang.String", "String");
          return this;
      }

      public CmsPageTemplateSQL andEditModeLessThanOrEqualTo(java.lang.String value) {
          addCriterion("edit_mode", value, ConditionMode.LESS_EQUAL, "editMode", "java.lang.String", "String");
          return this;
      }

      public CmsPageTemplateSQL andEditModeBetween(java.lang.String value1, java.lang.String value2) {
    	  addCriterion("edit_mode", value1, value2, ConditionMode.BETWEEN, "editMode", "java.lang.String", "String");
    	  return this;
      }

      public CmsPageTemplateSQL andEditModeNotBetween(java.lang.String value1, java.lang.String value2) {
          addCriterion("edit_mode", value1, value2, ConditionMode.NOT_BETWEEN, "editMode", "java.lang.String", "String");
          return this;
      }
        
      public CmsPageTemplateSQL andEditModeIn(List<java.lang.String> values) {
          addCriterion("edit_mode", values, ConditionMode.IN, "editMode", "java.lang.String", "String");
          return this;
      }

      public CmsPageTemplateSQL andEditModeNotIn(List<java.lang.String> values) {
          addCriterion("edit_mode", values, ConditionMode.NOT_IN, "editMode", "java.lang.String", "String");
          return this;
      }
	public CmsPageTemplateSQL andDescriptionIsNull() {
		isnull("description");
		return this;
	}
	
	public CmsPageTemplateSQL andDescriptionIsNotNull() {
		notNull("description");
		return this;
	}
	
	public CmsPageTemplateSQL andDescriptionIsEmpty() {
		empty("description");
		return this;
	}

	public CmsPageTemplateSQL andDescriptionIsNotEmpty() {
		notEmpty("description");
		return this;
	}
       public CmsPageTemplateSQL andDescriptionLike(java.lang.String value) {
    	   addCriterion("description", value, ConditionMode.FUZZY, "description", "java.lang.String", "String");
    	   return this;
      }

      public CmsPageTemplateSQL andDescriptionNotLike(java.lang.String value) {
          addCriterion("description", value, ConditionMode.NOT_FUZZY, "description", "java.lang.String", "String");
          return this;
      }
      public CmsPageTemplateSQL andDescriptionEqualTo(java.lang.String value) {
          addCriterion("description", value, ConditionMode.EQUAL, "description", "java.lang.String", "String");
          return this;
      }

      public CmsPageTemplateSQL andDescriptionNotEqualTo(java.lang.String value) {
          addCriterion("description", value, ConditionMode.NOT_EQUAL, "description", "java.lang.String", "String");
          return this;
      }

      public CmsPageTemplateSQL andDescriptionGreaterThan(java.lang.String value) {
          addCriterion("description", value, ConditionMode.GREATER_THEN, "description", "java.lang.String", "String");
          return this;
      }

      public CmsPageTemplateSQL andDescriptionGreaterThanOrEqualTo(java.lang.String value) {
          addCriterion("description", value, ConditionMode.GREATER_EQUAL, "description", "java.lang.String", "String");
          return this;
      }

      public CmsPageTemplateSQL andDescriptionLessThan(java.lang.String value) {
          addCriterion("description", value, ConditionMode.LESS_THEN, "description", "java.lang.String", "String");
          return this;
      }

      public CmsPageTemplateSQL andDescriptionLessThanOrEqualTo(java.lang.String value) {
          addCriterion("description", value, ConditionMode.LESS_EQUAL, "description", "java.lang.String", "String");
          return this;
      }

      public CmsPageTemplateSQL andDescriptionBetween(java.lang.String value1, java.lang.String value2) {
    	  addCriterion("description", value1, value2, ConditionMode.BETWEEN, "description", "java.lang.String", "String");
    	  return this;
      }

      public CmsPageTemplateSQL andDescriptionNotBetween(java.lang.String value1, java.lang.String value2) {
          addCriterion("description", value1, value2, ConditionMode.NOT_BETWEEN, "description", "java.lang.String", "String");
          return this;
      }
        
      public CmsPageTemplateSQL andDescriptionIn(List<java.lang.String> values) {
          addCriterion("description", values, ConditionMode.IN, "description", "java.lang.String", "String");
          return this;
      }

      public CmsPageTemplateSQL andDescriptionNotIn(List<java.lang.String> values) {
          addCriterion("description", values, ConditionMode.NOT_IN, "description", "java.lang.String", "String");
          return this;
      }
	public CmsPageTemplateSQL andStatusIsNull() {
		isnull("status");
		return this;
	}
	
	public CmsPageTemplateSQL andStatusIsNotNull() {
		notNull("status");
		return this;
	}
	
	public CmsPageTemplateSQL andStatusIsEmpty() {
		empty("status");
		return this;
	}

	public CmsPageTemplateSQL andStatusIsNotEmpty() {
		notEmpty("status");
		return this;
	}
      public CmsPageTemplateSQL andStatusEqualTo(java.lang.Integer value) {
          addCriterion("status", value, ConditionMode.EQUAL, "status", "java.lang.Integer", "Float");
          return this;
      }

      public CmsPageTemplateSQL andStatusNotEqualTo(java.lang.Integer value) {
          addCriterion("status", value, ConditionMode.NOT_EQUAL, "status", "java.lang.Integer", "Float");
          return this;
      }

      public CmsPageTemplateSQL andStatusGreaterThan(java.lang.Integer value) {
          addCriterion("status", value, ConditionMode.GREATER_THEN, "status", "java.lang.Integer", "Float");
          return this;
      }

      public CmsPageTemplateSQL andStatusGreaterThanOrEqualTo(java.lang.Integer value) {
          addCriterion("status", value, ConditionMode.GREATER_EQUAL, "status", "java.lang.Integer", "Float");
          return this;
      }

      public CmsPageTemplateSQL andStatusLessThan(java.lang.Integer value) {
          addCriterion("status", value, ConditionMode.LESS_THEN, "status", "java.lang.Integer", "Float");
          return this;
      }

      public CmsPageTemplateSQL andStatusLessThanOrEqualTo(java.lang.Integer value) {
          addCriterion("status", value, ConditionMode.LESS_EQUAL, "status", "java.lang.Integer", "Float");
          return this;
      }

      public CmsPageTemplateSQL andStatusBetween(java.lang.Integer value1, java.lang.Integer value2) {
    	  addCriterion("status", value1, value2, ConditionMode.BETWEEN, "status", "java.lang.Integer", "Float");
    	  return this;
      }

      public CmsPageTemplateSQL andStatusNotBetween(java.lang.Integer value1, java.lang.Integer value2) {
          addCriterion("status", value1, value2, ConditionMode.NOT_BETWEEN, "status", "java.lang.Integer", "Float");
          return this;
      }
        
      public CmsPageTemplateSQL andStatusIn(List<java.lang.Integer> values) {
          addCriterion("status", values, ConditionMode.IN, "status", "java.lang.Integer", "Float");
          return this;
      }

      public CmsPageTemplateSQL andStatusNotIn(List<java.lang.Integer> values) {
          addCriterion("status", values, ConditionMode.NOT_IN, "status", "java.lang.Integer", "Float");
          return this;
      }
	public CmsPageTemplateSQL andUpdateTimeIsNull() {
		isnull("update_time");
		return this;
	}
	
	public CmsPageTemplateSQL andUpdateTimeIsNotNull() {
		notNull("update_time");
		return this;
	}
	
	public CmsPageTemplateSQL andUpdateTimeIsEmpty() {
		empty("update_time");
		return this;
	}

	public CmsPageTemplateSQL andUpdateTimeIsNotEmpty() {
		notEmpty("update_time");
		return this;
	}
      public CmsPageTemplateSQL andUpdateTimeEqualTo(java.util.Date value) {
          addCriterion("update_time", value, ConditionMode.EQUAL, "updateTime", "java.util.Date", "String");
          return this;
      }

      public CmsPageTemplateSQL andUpdateTimeNotEqualTo(java.util.Date value) {
          addCriterion("update_time", value, ConditionMode.NOT_EQUAL, "updateTime", "java.util.Date", "String");
          return this;
      }

      public CmsPageTemplateSQL andUpdateTimeGreaterThan(java.util.Date value) {
          addCriterion("update_time", value, ConditionMode.GREATER_THEN, "updateTime", "java.util.Date", "String");
          return this;
      }

      public CmsPageTemplateSQL andUpdateTimeGreaterThanOrEqualTo(java.util.Date value) {
          addCriterion("update_time", value, ConditionMode.GREATER_EQUAL, "updateTime", "java.util.Date", "String");
          return this;
      }

      public CmsPageTemplateSQL andUpdateTimeLessThan(java.util.Date value) {
          addCriterion("update_time", value, ConditionMode.LESS_THEN, "updateTime", "java.util.Date", "String");
          return this;
      }

      public CmsPageTemplateSQL andUpdateTimeLessThanOrEqualTo(java.util.Date value) {
          addCriterion("update_time", value, ConditionMode.LESS_EQUAL, "updateTime", "java.util.Date", "String");
          return this;
      }

      public CmsPageTemplateSQL andUpdateTimeBetween(java.util.Date value1, java.util.Date value2) {
    	  addCriterion("update_time", value1, value2, ConditionMode.BETWEEN, "updateTime", "java.util.Date", "String");
    	  return this;
      }

      public CmsPageTemplateSQL andUpdateTimeNotBetween(java.util.Date value1, java.util.Date value2) {
          addCriterion("update_time", value1, value2, ConditionMode.NOT_BETWEEN, "updateTime", "java.util.Date", "String");
          return this;
      }
        
      public CmsPageTemplateSQL andUpdateTimeIn(List<java.util.Date> values) {
          addCriterion("update_time", values, ConditionMode.IN, "updateTime", "java.util.Date", "String");
          return this;
      }

      public CmsPageTemplateSQL andUpdateTimeNotIn(List<java.util.Date> values) {
          addCriterion("update_time", values, ConditionMode.NOT_IN, "updateTime", "java.util.Date", "String");
          return this;
      }
	public CmsPageTemplateSQL andUserIdIsNull() {
		isnull("user_id");
		return this;
	}
	
	public CmsPageTemplateSQL andUserIdIsNotNull() {
		notNull("user_id");
		return this;
	}
	
	public CmsPageTemplateSQL andUserIdIsEmpty() {
		empty("user_id");
		return this;
	}

	public CmsPageTemplateSQL andUserIdIsNotEmpty() {
		notEmpty("user_id");
		return this;
	}
      public CmsPageTemplateSQL andUserIdEqualTo(java.lang.Long value) {
          addCriterion("user_id", value, ConditionMode.EQUAL, "userId", "java.lang.Long", "Float");
          return this;
      }

      public CmsPageTemplateSQL andUserIdNotEqualTo(java.lang.Long value) {
          addCriterion("user_id", value, ConditionMode.NOT_EQUAL, "userId", "java.lang.Long", "Float");
          return this;
      }

      public CmsPageTemplateSQL andUserIdGreaterThan(java.lang.Long value) {
          addCriterion("user_id", value, ConditionMode.GREATER_THEN, "userId", "java.lang.Long", "Float");
          return this;
      }

      public CmsPageTemplateSQL andUserIdGreaterThanOrEqualTo(java.lang.Long value) {
          addCriterion("user_id", value, ConditionMode.GREATER_EQUAL, "userId", "java.lang.Long", "Float");
          return this;
      }

      public CmsPageTemplateSQL andUserIdLessThan(java.lang.Long value) {
          addCriterion("user_id", value, ConditionMode.LESS_THEN, "userId", "java.lang.Long", "Float");
          return this;
      }

      public CmsPageTemplateSQL andUserIdLessThanOrEqualTo(java.lang.Long value) {
          addCriterion("user_id", value, ConditionMode.LESS_EQUAL, "userId", "java.lang.Long", "Float");
          return this;
      }

      public CmsPageTemplateSQL andUserIdBetween(java.lang.Long value1, java.lang.Long value2) {
    	  addCriterion("user_id", value1, value2, ConditionMode.BETWEEN, "userId", "java.lang.Long", "Float");
    	  return this;
      }

      public CmsPageTemplateSQL andUserIdNotBetween(java.lang.Long value1, java.lang.Long value2) {
          addCriterion("user_id", value1, value2, ConditionMode.NOT_BETWEEN, "userId", "java.lang.Long", "Float");
          return this;
      }
        
      public CmsPageTemplateSQL andUserIdIn(List<java.lang.Long> values) {
          addCriterion("user_id", values, ConditionMode.IN, "userId", "java.lang.Long", "Float");
          return this;
      }

      public CmsPageTemplateSQL andUserIdNotIn(List<java.lang.Long> values) {
          addCriterion("user_id", values, ConditionMode.NOT_IN, "userId", "java.lang.Long", "Float");
          return this;
      }
}