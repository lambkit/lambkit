/**
 * Copyright (c) 2015-2017, Henry Yang 杨勇 (gismail@foxmail.com).
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.lambkit.module.cms.sql;

import com.lambkit.db.sql.ConditionMode;
import com.lambkit.db.sql.Column;
import com.lambkit.db.sql.Columns;
import com.lambkit.db.sql.Example;

import java.io.Serializable;
import java.util.List;

/**
 * @author yangyong 
 * @website: www.lambkit.com
 * @email: gismail@foxmail.com
 * @date ${date}
 * @version 1.0
 * @since 1.0
 */

public class CmsTopicSQL extends Columns implements Serializable {
	private static final long serialVersionUID = 1L;
	
	public static CmsTopicSQL of() {
		return new CmsTopicSQL();
	}
	
	public static CmsTopicSQL by(Column column) {
		CmsTopicSQL that = new CmsTopicSQL();
		that.add(column);
        return that;
    }

    public static CmsTopicSQL by(String name, Object value) {
        return (CmsTopicSQL) create().eq(name, value);
    }
    
    public Example example() {
    	return Example.create("cms_topic", this);
    }
    
    /**
     * equals
     *
     * @param name
     * @param value
     * @return
     */
    public CmsTopicSQL eq(String name, Object value) {
    	super.eq(name, value);
        return this;
    }

    /**
     * not equals !=
     *
     * @param name
     * @param value
     * @return
     */
    public CmsTopicSQL ne(String name, Object value) {
    	super.ne(name, value);
        return this;
    }


    /**
     * like
     *
     * @param name
     * @param value
     * @return
     */

    public CmsTopicSQL like(String name, Object value) {
    	super.like(name, value);
        return this;
    }
    
    public CmsTopicSQL notLike(String name, Object value) {
    	super.notLike(name, value);
        return this;
    }

    /**
     * 大于 great than
     *
     * @param name
     * @param value
     * @return
     */
    public CmsTopicSQL gt(String name, Object value) {
    	super.gt(name, value);
        return this;
    }

    /**
     * 大于等于 great or equal
     *
     * @param name
     * @param value
     * @return
     */
    public CmsTopicSQL ge(String name, Object value) {
    	super.ge(name, value);
        return this;
    }

    /**
     * 小于 less than
     *
     * @param name
     * @param value
     * @return
     */
    public CmsTopicSQL lt(String name, Object value) {
    	super.lt(name, value);
        return this;
    }

    /**
     * 小于等于 less or equal
     *
     * @param name
     * @param value
     * @return
     */
    public CmsTopicSQL le(String name, Object value) {
    	super.le(name, value);
        return this;
    }
    
    public CmsTopicSQL isnull(String name) {
    	super.isnull(name);
        return this;
    } 

    public CmsTopicSQL notNull(String name) {
    	super.notNull(name);
        return this;
    } 
    
    public CmsTopicSQL empty(String name) {
    	super.empty(name);
        return this;
    } 
    
    public CmsTopicSQL notEmpty(String name) {
    	super.notEmpty(name);
        return this;
    } 
    
    public CmsTopicSQL add(Column column) {
    	super.add(column);
    	return this;
    }
    
    /**************************/
	
	public void addCriterion(String name, Object value, ConditionMode logic, String property, String typeHandler, String valueType) {
		 if (value == null) {
			 throw new RuntimeException("Value for " + property + " cannot be null");
		 }
		 add(Column.create(name, value, logic, typeHandler, valueType));
	}
   
	public void addCriterion(String name, Object value1, Object value2, ConditionMode logic, String property, String typeHandler, String valueType) {
		 if (value1 == null || value2 == null) {
			 throw new RuntimeException("Between values for " + property + " cannot be null");
		 }
		 add(Column.create(name, value1, value2, logic, typeHandler, valueType));
	}
		 
	public CmsTopicSQL andTopicIdIsNull() {
		isnull("topic_id");
		return this;
	}
	
	public CmsTopicSQL andTopicIdIsNotNull() {
		notNull("topic_id");
		return this;
	}
	
	public CmsTopicSQL andTopicIdIsEmpty() {
		empty("topic_id");
		return this;
	}

	public CmsTopicSQL andTopicIdIsNotEmpty() {
		notEmpty("topic_id");
		return this;
	}
      public CmsTopicSQL andTopicIdEqualTo(java.lang.Integer value) {
          addCriterion("topic_id", value, ConditionMode.EQUAL, "topicId", "java.lang.Integer", "Float");
          return this;
      }

      public CmsTopicSQL andTopicIdNotEqualTo(java.lang.Integer value) {
          addCriterion("topic_id", value, ConditionMode.NOT_EQUAL, "topicId", "java.lang.Integer", "Float");
          return this;
      }

      public CmsTopicSQL andTopicIdGreaterThan(java.lang.Integer value) {
          addCriterion("topic_id", value, ConditionMode.GREATER_THEN, "topicId", "java.lang.Integer", "Float");
          return this;
      }

      public CmsTopicSQL andTopicIdGreaterThanOrEqualTo(java.lang.Integer value) {
          addCriterion("topic_id", value, ConditionMode.GREATER_EQUAL, "topicId", "java.lang.Integer", "Float");
          return this;
      }

      public CmsTopicSQL andTopicIdLessThan(java.lang.Integer value) {
          addCriterion("topic_id", value, ConditionMode.LESS_THEN, "topicId", "java.lang.Integer", "Float");
          return this;
      }

      public CmsTopicSQL andTopicIdLessThanOrEqualTo(java.lang.Integer value) {
          addCriterion("topic_id", value, ConditionMode.LESS_EQUAL, "topicId", "java.lang.Integer", "Float");
          return this;
      }

      public CmsTopicSQL andTopicIdBetween(java.lang.Integer value1, java.lang.Integer value2) {
    	  addCriterion("topic_id", value1, value2, ConditionMode.BETWEEN, "topicId", "java.lang.Integer", "Float");
    	  return this;
      }

      public CmsTopicSQL andTopicIdNotBetween(java.lang.Integer value1, java.lang.Integer value2) {
          addCriterion("topic_id", value1, value2, ConditionMode.NOT_BETWEEN, "topicId", "java.lang.Integer", "Float");
          return this;
      }
        
      public CmsTopicSQL andTopicIdIn(List<java.lang.Integer> values) {
          addCriterion("topic_id", values, ConditionMode.IN, "topicId", "java.lang.Integer", "Float");
          return this;
      }

      public CmsTopicSQL andTopicIdNotIn(List<java.lang.Integer> values) {
          addCriterion("topic_id", values, ConditionMode.NOT_IN, "topicId", "java.lang.Integer", "Float");
          return this;
      }
	public CmsTopicSQL andTitleIsNull() {
		isnull("title");
		return this;
	}
	
	public CmsTopicSQL andTitleIsNotNull() {
		notNull("title");
		return this;
	}
	
	public CmsTopicSQL andTitleIsEmpty() {
		empty("title");
		return this;
	}

	public CmsTopicSQL andTitleIsNotEmpty() {
		notEmpty("title");
		return this;
	}
       public CmsTopicSQL andTitleLike(java.lang.String value) {
    	   addCriterion("title", value, ConditionMode.FUZZY, "title", "java.lang.String", "Float");
    	   return this;
      }

      public CmsTopicSQL andTitleNotLike(java.lang.String value) {
          addCriterion("title", value, ConditionMode.NOT_FUZZY, "title", "java.lang.String", "Float");
          return this;
      }
      public CmsTopicSQL andTitleEqualTo(java.lang.String value) {
          addCriterion("title", value, ConditionMode.EQUAL, "title", "java.lang.String", "String");
          return this;
      }

      public CmsTopicSQL andTitleNotEqualTo(java.lang.String value) {
          addCriterion("title", value, ConditionMode.NOT_EQUAL, "title", "java.lang.String", "String");
          return this;
      }

      public CmsTopicSQL andTitleGreaterThan(java.lang.String value) {
          addCriterion("title", value, ConditionMode.GREATER_THEN, "title", "java.lang.String", "String");
          return this;
      }

      public CmsTopicSQL andTitleGreaterThanOrEqualTo(java.lang.String value) {
          addCriterion("title", value, ConditionMode.GREATER_EQUAL, "title", "java.lang.String", "String");
          return this;
      }

      public CmsTopicSQL andTitleLessThan(java.lang.String value) {
          addCriterion("title", value, ConditionMode.LESS_THEN, "title", "java.lang.String", "String");
          return this;
      }

      public CmsTopicSQL andTitleLessThanOrEqualTo(java.lang.String value) {
          addCriterion("title", value, ConditionMode.LESS_EQUAL, "title", "java.lang.String", "String");
          return this;
      }

      public CmsTopicSQL andTitleBetween(java.lang.String value1, java.lang.String value2) {
    	  addCriterion("title", value1, value2, ConditionMode.BETWEEN, "title", "java.lang.String", "String");
    	  return this;
      }

      public CmsTopicSQL andTitleNotBetween(java.lang.String value1, java.lang.String value2) {
          addCriterion("title", value1, value2, ConditionMode.NOT_BETWEEN, "title", "java.lang.String", "String");
          return this;
      }
        
      public CmsTopicSQL andTitleIn(List<java.lang.String> values) {
          addCriterion("title", values, ConditionMode.IN, "title", "java.lang.String", "String");
          return this;
      }

      public CmsTopicSQL andTitleNotIn(List<java.lang.String> values) {
          addCriterion("title", values, ConditionMode.NOT_IN, "title", "java.lang.String", "String");
          return this;
      }
	public CmsTopicSQL andDescriptionIsNull() {
		isnull("description");
		return this;
	}
	
	public CmsTopicSQL andDescriptionIsNotNull() {
		notNull("description");
		return this;
	}
	
	public CmsTopicSQL andDescriptionIsEmpty() {
		empty("description");
		return this;
	}

	public CmsTopicSQL andDescriptionIsNotEmpty() {
		notEmpty("description");
		return this;
	}
       public CmsTopicSQL andDescriptionLike(java.lang.String value) {
    	   addCriterion("description", value, ConditionMode.FUZZY, "description", "java.lang.String", "String");
    	   return this;
      }

      public CmsTopicSQL andDescriptionNotLike(java.lang.String value) {
          addCriterion("description", value, ConditionMode.NOT_FUZZY, "description", "java.lang.String", "String");
          return this;
      }
      public CmsTopicSQL andDescriptionEqualTo(java.lang.String value) {
          addCriterion("description", value, ConditionMode.EQUAL, "description", "java.lang.String", "String");
          return this;
      }

      public CmsTopicSQL andDescriptionNotEqualTo(java.lang.String value) {
          addCriterion("description", value, ConditionMode.NOT_EQUAL, "description", "java.lang.String", "String");
          return this;
      }

      public CmsTopicSQL andDescriptionGreaterThan(java.lang.String value) {
          addCriterion("description", value, ConditionMode.GREATER_THEN, "description", "java.lang.String", "String");
          return this;
      }

      public CmsTopicSQL andDescriptionGreaterThanOrEqualTo(java.lang.String value) {
          addCriterion("description", value, ConditionMode.GREATER_EQUAL, "description", "java.lang.String", "String");
          return this;
      }

      public CmsTopicSQL andDescriptionLessThan(java.lang.String value) {
          addCriterion("description", value, ConditionMode.LESS_THEN, "description", "java.lang.String", "String");
          return this;
      }

      public CmsTopicSQL andDescriptionLessThanOrEqualTo(java.lang.String value) {
          addCriterion("description", value, ConditionMode.LESS_EQUAL, "description", "java.lang.String", "String");
          return this;
      }

      public CmsTopicSQL andDescriptionBetween(java.lang.String value1, java.lang.String value2) {
    	  addCriterion("description", value1, value2, ConditionMode.BETWEEN, "description", "java.lang.String", "String");
    	  return this;
      }

      public CmsTopicSQL andDescriptionNotBetween(java.lang.String value1, java.lang.String value2) {
          addCriterion("description", value1, value2, ConditionMode.NOT_BETWEEN, "description", "java.lang.String", "String");
          return this;
      }
        
      public CmsTopicSQL andDescriptionIn(List<java.lang.String> values) {
          addCriterion("description", values, ConditionMode.IN, "description", "java.lang.String", "String");
          return this;
      }

      public CmsTopicSQL andDescriptionNotIn(List<java.lang.String> values) {
          addCriterion("description", values, ConditionMode.NOT_IN, "description", "java.lang.String", "String");
          return this;
      }
	public CmsTopicSQL andUrlIsNull() {
		isnull("url");
		return this;
	}
	
	public CmsTopicSQL andUrlIsNotNull() {
		notNull("url");
		return this;
	}
	
	public CmsTopicSQL andUrlIsEmpty() {
		empty("url");
		return this;
	}

	public CmsTopicSQL andUrlIsNotEmpty() {
		notEmpty("url");
		return this;
	}
       public CmsTopicSQL andUrlLike(java.lang.String value) {
    	   addCriterion("url", value, ConditionMode.FUZZY, "url", "java.lang.String", "String");
    	   return this;
      }

      public CmsTopicSQL andUrlNotLike(java.lang.String value) {
          addCriterion("url", value, ConditionMode.NOT_FUZZY, "url", "java.lang.String", "String");
          return this;
      }
      public CmsTopicSQL andUrlEqualTo(java.lang.String value) {
          addCriterion("url", value, ConditionMode.EQUAL, "url", "java.lang.String", "String");
          return this;
      }

      public CmsTopicSQL andUrlNotEqualTo(java.lang.String value) {
          addCriterion("url", value, ConditionMode.NOT_EQUAL, "url", "java.lang.String", "String");
          return this;
      }

      public CmsTopicSQL andUrlGreaterThan(java.lang.String value) {
          addCriterion("url", value, ConditionMode.GREATER_THEN, "url", "java.lang.String", "String");
          return this;
      }

      public CmsTopicSQL andUrlGreaterThanOrEqualTo(java.lang.String value) {
          addCriterion("url", value, ConditionMode.GREATER_EQUAL, "url", "java.lang.String", "String");
          return this;
      }

      public CmsTopicSQL andUrlLessThan(java.lang.String value) {
          addCriterion("url", value, ConditionMode.LESS_THEN, "url", "java.lang.String", "String");
          return this;
      }

      public CmsTopicSQL andUrlLessThanOrEqualTo(java.lang.String value) {
          addCriterion("url", value, ConditionMode.LESS_EQUAL, "url", "java.lang.String", "String");
          return this;
      }

      public CmsTopicSQL andUrlBetween(java.lang.String value1, java.lang.String value2) {
    	  addCriterion("url", value1, value2, ConditionMode.BETWEEN, "url", "java.lang.String", "String");
    	  return this;
      }

      public CmsTopicSQL andUrlNotBetween(java.lang.String value1, java.lang.String value2) {
          addCriterion("url", value1, value2, ConditionMode.NOT_BETWEEN, "url", "java.lang.String", "String");
          return this;
      }
        
      public CmsTopicSQL andUrlIn(List<java.lang.String> values) {
          addCriterion("url", values, ConditionMode.IN, "url", "java.lang.String", "String");
          return this;
      }

      public CmsTopicSQL andUrlNotIn(List<java.lang.String> values) {
          addCriterion("url", values, ConditionMode.NOT_IN, "url", "java.lang.String", "String");
          return this;
      }
	public CmsTopicSQL andCtimeIsNull() {
		isnull("ctime");
		return this;
	}
	
	public CmsTopicSQL andCtimeIsNotNull() {
		notNull("ctime");
		return this;
	}
	
	public CmsTopicSQL andCtimeIsEmpty() {
		empty("ctime");
		return this;
	}

	public CmsTopicSQL andCtimeIsNotEmpty() {
		notEmpty("ctime");
		return this;
	}
      public CmsTopicSQL andCtimeEqualTo(java.util.Date value) {
          addCriterion("ctime", value, ConditionMode.EQUAL, "ctime", "java.util.Date", "String");
          return this;
      }

      public CmsTopicSQL andCtimeNotEqualTo(java.util.Date value) {
          addCriterion("ctime", value, ConditionMode.NOT_EQUAL, "ctime", "java.util.Date", "String");
          return this;
      }

      public CmsTopicSQL andCtimeGreaterThan(java.util.Date value) {
          addCriterion("ctime", value, ConditionMode.GREATER_THEN, "ctime", "java.util.Date", "String");
          return this;
      }

      public CmsTopicSQL andCtimeGreaterThanOrEqualTo(java.util.Date value) {
          addCriterion("ctime", value, ConditionMode.GREATER_EQUAL, "ctime", "java.util.Date", "String");
          return this;
      }

      public CmsTopicSQL andCtimeLessThan(java.util.Date value) {
          addCriterion("ctime", value, ConditionMode.LESS_THEN, "ctime", "java.util.Date", "String");
          return this;
      }

      public CmsTopicSQL andCtimeLessThanOrEqualTo(java.util.Date value) {
          addCriterion("ctime", value, ConditionMode.LESS_EQUAL, "ctime", "java.util.Date", "String");
          return this;
      }

      public CmsTopicSQL andCtimeBetween(java.util.Date value1, java.util.Date value2) {
    	  addCriterion("ctime", value1, value2, ConditionMode.BETWEEN, "ctime", "java.util.Date", "String");
    	  return this;
      }

      public CmsTopicSQL andCtimeNotBetween(java.util.Date value1, java.util.Date value2) {
          addCriterion("ctime", value1, value2, ConditionMode.NOT_BETWEEN, "ctime", "java.util.Date", "String");
          return this;
      }
        
      public CmsTopicSQL andCtimeIn(List<java.util.Date> values) {
          addCriterion("ctime", values, ConditionMode.IN, "ctime", "java.util.Date", "String");
          return this;
      }

      public CmsTopicSQL andCtimeNotIn(List<java.util.Date> values) {
          addCriterion("ctime", values, ConditionMode.NOT_IN, "ctime", "java.util.Date", "String");
          return this;
      }
}