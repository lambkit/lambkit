package com.lambkit.module.cms;

import com.jfinal.template.source.ISource;
import com.lambkit.core.Lambkit;
import com.lambkit.module.cms.row.CmsPageTemplate;

/**
 * @author yangyong(孤竹行)
 */
public class DbSource implements ISource {

    private CmsPageTemplate templateModel;
    private String encoding = "UTF-8";

    public DbSource(CmsPageTemplate templateModel) {
        this.templateModel = templateModel;
    }

    public DbSource(CmsPageTemplate templateModel, String encoding) {
        this.templateModel = templateModel;
        this.encoding = encoding;
    }

    @Override
    public boolean isModified() {
        CmsConfig config = Lambkit.config(CmsConfig.class);
        return config.isModified();
    }

    @Override
    public String getCacheKey() {
        if(templateModel==null) {
            return "templateID:0";
        }
        return "templateID:" + templateModel.getTempId();
    }

    @Override
    public StringBuilder getContent() {
        StringBuilder ret = new StringBuilder();
        if(templateModel==null) {
            return ret;
        }
        ret.append(templateModel.getContent());
        return ret;
    }

    @Override
    public String getEncoding() {
        return encoding;
    }
}
