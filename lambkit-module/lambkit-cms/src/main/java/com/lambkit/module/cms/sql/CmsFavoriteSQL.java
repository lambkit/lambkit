/**
 * Copyright (c) 2015-2017, Henry Yang 杨勇 (gismail@foxmail.com).
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.lambkit.module.cms.sql;

import com.lambkit.db.sql.ConditionMode;
import com.lambkit.db.sql.Column;
import com.lambkit.db.sql.Columns;
import com.lambkit.db.sql.Example;

import java.io.Serializable;
import java.util.List;

/**
 * @author yangyong 
 * @website: www.lambkit.com
 * @email: gismail@foxmail.com
 * @date ${date}
 * @version 1.0
 * @since 1.0
 */

public class CmsFavoriteSQL extends Columns implements Serializable {
	private static final long serialVersionUID = 1L;
	
	public static CmsFavoriteSQL of() {
		return new CmsFavoriteSQL();
	}
	
	public static CmsFavoriteSQL by(Column column) {
		CmsFavoriteSQL that = new CmsFavoriteSQL();
		that.add(column);
        return that;
    }

    public static CmsFavoriteSQL by(String name, Object value) {
        return (CmsFavoriteSQL) create().eq(name, value);
    }
    
    public Example example() {
    	return Example.create("cms_favorite", this);
    }
    
    /**
     * equals
     *
     * @param name
     * @param value
     * @return
     */
    public CmsFavoriteSQL eq(String name, Object value) {
    	super.eq(name, value);
        return this;
    }

    /**
     * not equals !=
     *
     * @param name
     * @param value
     * @return
     */
    public CmsFavoriteSQL ne(String name, Object value) {
    	super.ne(name, value);
        return this;
    }


    /**
     * like
     *
     * @param name
     * @param value
     * @return
     */

    public CmsFavoriteSQL like(String name, Object value) {
    	super.like(name, value);
        return this;
    }
    
    public CmsFavoriteSQL notLike(String name, Object value) {
    	super.notLike(name, value);
        return this;
    }

    /**
     * 大于 great than
     *
     * @param name
     * @param value
     * @return
     */
    public CmsFavoriteSQL gt(String name, Object value) {
    	super.gt(name, value);
        return this;
    }

    /**
     * 大于等于 great or equal
     *
     * @param name
     * @param value
     * @return
     */
    public CmsFavoriteSQL ge(String name, Object value) {
    	super.ge(name, value);
        return this;
    }

    /**
     * 小于 less than
     *
     * @param name
     * @param value
     * @return
     */
    public CmsFavoriteSQL lt(String name, Object value) {
    	super.lt(name, value);
        return this;
    }

    /**
     * 小于等于 less or equal
     *
     * @param name
     * @param value
     * @return
     */
    public CmsFavoriteSQL le(String name, Object value) {
    	super.le(name, value);
        return this;
    }
    
    public CmsFavoriteSQL isnull(String name) {
    	super.isnull(name);
        return this;
    } 

    public CmsFavoriteSQL notNull(String name) {
    	super.notNull(name);
        return this;
    } 
    
    public CmsFavoriteSQL empty(String name) {
    	super.empty(name);
        return this;
    } 
    
    public CmsFavoriteSQL notEmpty(String name) {
    	super.notEmpty(name);
        return this;
    } 
    
    public CmsFavoriteSQL add(Column column) {
    	super.add(column);
    	return this;
    }
    
    /**************************/
	
	public void addCriterion(String name, Object value, ConditionMode logic, String property, String typeHandler, String valueType) {
		 if (value == null) {
			 throw new RuntimeException("Value for " + property + " cannot be null");
		 }
		 add(Column.create(name, value, logic, typeHandler, valueType));
	}
   
	public void addCriterion(String name, Object value1, Object value2, ConditionMode logic, String property, String typeHandler, String valueType) {
		 if (value1 == null || value2 == null) {
			 throw new RuntimeException("Between values for " + property + " cannot be null");
		 }
		 add(Column.create(name, value1, value2, logic, typeHandler, valueType));
	}
		 
	public CmsFavoriteSQL andFavoriteIdIsNull() {
		isnull("favorite_id");
		return this;
	}
	
	public CmsFavoriteSQL andFavoriteIdIsNotNull() {
		notNull("favorite_id");
		return this;
	}
	
	public CmsFavoriteSQL andFavoriteIdIsEmpty() {
		empty("favorite_id");
		return this;
	}

	public CmsFavoriteSQL andFavoriteIdIsNotEmpty() {
		notEmpty("favorite_id");
		return this;
	}
      public CmsFavoriteSQL andFavoriteIdEqualTo(java.lang.Integer value) {
          addCriterion("favorite_id", value, ConditionMode.EQUAL, "favoriteId", "java.lang.Integer", "Float");
          return this;
      }

      public CmsFavoriteSQL andFavoriteIdNotEqualTo(java.lang.Integer value) {
          addCriterion("favorite_id", value, ConditionMode.NOT_EQUAL, "favoriteId", "java.lang.Integer", "Float");
          return this;
      }

      public CmsFavoriteSQL andFavoriteIdGreaterThan(java.lang.Integer value) {
          addCriterion("favorite_id", value, ConditionMode.GREATER_THEN, "favoriteId", "java.lang.Integer", "Float");
          return this;
      }

      public CmsFavoriteSQL andFavoriteIdGreaterThanOrEqualTo(java.lang.Integer value) {
          addCriterion("favorite_id", value, ConditionMode.GREATER_EQUAL, "favoriteId", "java.lang.Integer", "Float");
          return this;
      }

      public CmsFavoriteSQL andFavoriteIdLessThan(java.lang.Integer value) {
          addCriterion("favorite_id", value, ConditionMode.LESS_THEN, "favoriteId", "java.lang.Integer", "Float");
          return this;
      }

      public CmsFavoriteSQL andFavoriteIdLessThanOrEqualTo(java.lang.Integer value) {
          addCriterion("favorite_id", value, ConditionMode.LESS_EQUAL, "favoriteId", "java.lang.Integer", "Float");
          return this;
      }

      public CmsFavoriteSQL andFavoriteIdBetween(java.lang.Integer value1, java.lang.Integer value2) {
    	  addCriterion("favorite_id", value1, value2, ConditionMode.BETWEEN, "favoriteId", "java.lang.Integer", "Float");
    	  return this;
      }

      public CmsFavoriteSQL andFavoriteIdNotBetween(java.lang.Integer value1, java.lang.Integer value2) {
          addCriterion("favorite_id", value1, value2, ConditionMode.NOT_BETWEEN, "favoriteId", "java.lang.Integer", "Float");
          return this;
      }
        
      public CmsFavoriteSQL andFavoriteIdIn(List<java.lang.Integer> values) {
          addCriterion("favorite_id", values, ConditionMode.IN, "favoriteId", "java.lang.Integer", "Float");
          return this;
      }

      public CmsFavoriteSQL andFavoriteIdNotIn(List<java.lang.Integer> values) {
          addCriterion("favorite_id", values, ConditionMode.NOT_IN, "favoriteId", "java.lang.Integer", "Float");
          return this;
      }
	public CmsFavoriteSQL andUserIdIsNull() {
		isnull("user_id");
		return this;
	}
	
	public CmsFavoriteSQL andUserIdIsNotNull() {
		notNull("user_id");
		return this;
	}
	
	public CmsFavoriteSQL andUserIdIsEmpty() {
		empty("user_id");
		return this;
	}

	public CmsFavoriteSQL andUserIdIsNotEmpty() {
		notEmpty("user_id");
		return this;
	}
      public CmsFavoriteSQL andUserIdEqualTo(java.lang.Long value) {
          addCriterion("user_id", value, ConditionMode.EQUAL, "userId", "java.lang.Long", "Float");
          return this;
      }

      public CmsFavoriteSQL andUserIdNotEqualTo(java.lang.Long value) {
          addCriterion("user_id", value, ConditionMode.NOT_EQUAL, "userId", "java.lang.Long", "Float");
          return this;
      }

      public CmsFavoriteSQL andUserIdGreaterThan(java.lang.Long value) {
          addCriterion("user_id", value, ConditionMode.GREATER_THEN, "userId", "java.lang.Long", "Float");
          return this;
      }

      public CmsFavoriteSQL andUserIdGreaterThanOrEqualTo(java.lang.Long value) {
          addCriterion("user_id", value, ConditionMode.GREATER_EQUAL, "userId", "java.lang.Long", "Float");
          return this;
      }

      public CmsFavoriteSQL andUserIdLessThan(java.lang.Long value) {
          addCriterion("user_id", value, ConditionMode.LESS_THEN, "userId", "java.lang.Long", "Float");
          return this;
      }

      public CmsFavoriteSQL andUserIdLessThanOrEqualTo(java.lang.Long value) {
          addCriterion("user_id", value, ConditionMode.LESS_EQUAL, "userId", "java.lang.Long", "Float");
          return this;
      }

      public CmsFavoriteSQL andUserIdBetween(java.lang.Long value1, java.lang.Long value2) {
    	  addCriterion("user_id", value1, value2, ConditionMode.BETWEEN, "userId", "java.lang.Long", "Float");
    	  return this;
      }

      public CmsFavoriteSQL andUserIdNotBetween(java.lang.Long value1, java.lang.Long value2) {
          addCriterion("user_id", value1, value2, ConditionMode.NOT_BETWEEN, "userId", "java.lang.Long", "Float");
          return this;
      }
        
      public CmsFavoriteSQL andUserIdIn(List<java.lang.Long> values) {
          addCriterion("user_id", values, ConditionMode.IN, "userId", "java.lang.Long", "Float");
          return this;
      }

      public CmsFavoriteSQL andUserIdNotIn(List<java.lang.Long> values) {
          addCriterion("user_id", values, ConditionMode.NOT_IN, "userId", "java.lang.Long", "Float");
          return this;
      }
	public CmsFavoriteSQL andRefIdIsNull() {
		isnull("ref_id");
		return this;
	}
	
	public CmsFavoriteSQL andRefIdIsNotNull() {
		notNull("ref_id");
		return this;
	}
	
	public CmsFavoriteSQL andRefIdIsEmpty() {
		empty("ref_id");
		return this;
	}

	public CmsFavoriteSQL andRefIdIsNotEmpty() {
		notEmpty("ref_id");
		return this;
	}
      public CmsFavoriteSQL andRefIdEqualTo(java.lang.Long value) {
          addCriterion("ref_id", value, ConditionMode.EQUAL, "refId", "java.lang.Long", "Float");
          return this;
      }

      public CmsFavoriteSQL andRefIdNotEqualTo(java.lang.Long value) {
          addCriterion("ref_id", value, ConditionMode.NOT_EQUAL, "refId", "java.lang.Long", "Float");
          return this;
      }

      public CmsFavoriteSQL andRefIdGreaterThan(java.lang.Long value) {
          addCriterion("ref_id", value, ConditionMode.GREATER_THEN, "refId", "java.lang.Long", "Float");
          return this;
      }

      public CmsFavoriteSQL andRefIdGreaterThanOrEqualTo(java.lang.Long value) {
          addCriterion("ref_id", value, ConditionMode.GREATER_EQUAL, "refId", "java.lang.Long", "Float");
          return this;
      }

      public CmsFavoriteSQL andRefIdLessThan(java.lang.Long value) {
          addCriterion("ref_id", value, ConditionMode.LESS_THEN, "refId", "java.lang.Long", "Float");
          return this;
      }

      public CmsFavoriteSQL andRefIdLessThanOrEqualTo(java.lang.Long value) {
          addCriterion("ref_id", value, ConditionMode.LESS_EQUAL, "refId", "java.lang.Long", "Float");
          return this;
      }

      public CmsFavoriteSQL andRefIdBetween(java.lang.Long value1, java.lang.Long value2) {
    	  addCriterion("ref_id", value1, value2, ConditionMode.BETWEEN, "refId", "java.lang.Long", "Float");
    	  return this;
      }

      public CmsFavoriteSQL andRefIdNotBetween(java.lang.Long value1, java.lang.Long value2) {
          addCriterion("ref_id", value1, value2, ConditionMode.NOT_BETWEEN, "refId", "java.lang.Long", "Float");
          return this;
      }
        
      public CmsFavoriteSQL andRefIdIn(List<java.lang.Long> values) {
          addCriterion("ref_id", values, ConditionMode.IN, "refId", "java.lang.Long", "Float");
          return this;
      }

      public CmsFavoriteSQL andRefIdNotIn(List<java.lang.Long> values) {
          addCriterion("ref_id", values, ConditionMode.NOT_IN, "refId", "java.lang.Long", "Float");
          return this;
      }
	public CmsFavoriteSQL andRefTypeIsNull() {
		isnull("ref_type");
		return this;
	}
	
	public CmsFavoriteSQL andRefTypeIsNotNull() {
		notNull("ref_type");
		return this;
	}
	
	public CmsFavoriteSQL andRefTypeIsEmpty() {
		empty("ref_type");
		return this;
	}

	public CmsFavoriteSQL andRefTypeIsNotEmpty() {
		notEmpty("ref_type");
		return this;
	}
      public CmsFavoriteSQL andRefTypeEqualTo(java.lang.Integer value) {
          addCriterion("ref_type", value, ConditionMode.EQUAL, "refType", "java.lang.Integer", "Float");
          return this;
      }

      public CmsFavoriteSQL andRefTypeNotEqualTo(java.lang.Integer value) {
          addCriterion("ref_type", value, ConditionMode.NOT_EQUAL, "refType", "java.lang.Integer", "Float");
          return this;
      }

      public CmsFavoriteSQL andRefTypeGreaterThan(java.lang.Integer value) {
          addCriterion("ref_type", value, ConditionMode.GREATER_THEN, "refType", "java.lang.Integer", "Float");
          return this;
      }

      public CmsFavoriteSQL andRefTypeGreaterThanOrEqualTo(java.lang.Integer value) {
          addCriterion("ref_type", value, ConditionMode.GREATER_EQUAL, "refType", "java.lang.Integer", "Float");
          return this;
      }

      public CmsFavoriteSQL andRefTypeLessThan(java.lang.Integer value) {
          addCriterion("ref_type", value, ConditionMode.LESS_THEN, "refType", "java.lang.Integer", "Float");
          return this;
      }

      public CmsFavoriteSQL andRefTypeLessThanOrEqualTo(java.lang.Integer value) {
          addCriterion("ref_type", value, ConditionMode.LESS_EQUAL, "refType", "java.lang.Integer", "Float");
          return this;
      }

      public CmsFavoriteSQL andRefTypeBetween(java.lang.Integer value1, java.lang.Integer value2) {
    	  addCriterion("ref_type", value1, value2, ConditionMode.BETWEEN, "refType", "java.lang.Integer", "Float");
    	  return this;
      }

      public CmsFavoriteSQL andRefTypeNotBetween(java.lang.Integer value1, java.lang.Integer value2) {
          addCriterion("ref_type", value1, value2, ConditionMode.NOT_BETWEEN, "refType", "java.lang.Integer", "Float");
          return this;
      }
        
      public CmsFavoriteSQL andRefTypeIn(List<java.lang.Integer> values) {
          addCriterion("ref_type", values, ConditionMode.IN, "refType", "java.lang.Integer", "Float");
          return this;
      }

      public CmsFavoriteSQL andRefTypeNotIn(List<java.lang.Integer> values) {
          addCriterion("ref_type", values, ConditionMode.NOT_IN, "refType", "java.lang.Integer", "Float");
          return this;
      }
	public CmsFavoriteSQL andRefCategoryIsNull() {
		isnull("ref_category");
		return this;
	}
	
	public CmsFavoriteSQL andRefCategoryIsNotNull() {
		notNull("ref_category");
		return this;
	}
	
	public CmsFavoriteSQL andRefCategoryIsEmpty() {
		empty("ref_category");
		return this;
	}

	public CmsFavoriteSQL andRefCategoryIsNotEmpty() {
		notEmpty("ref_category");
		return this;
	}
      public CmsFavoriteSQL andRefCategoryEqualTo(java.lang.Long value) {
          addCriterion("ref_category", value, ConditionMode.EQUAL, "refCategory", "java.lang.Long", "Float");
          return this;
      }

      public CmsFavoriteSQL andRefCategoryNotEqualTo(java.lang.Long value) {
          addCriterion("ref_category", value, ConditionMode.NOT_EQUAL, "refCategory", "java.lang.Long", "Float");
          return this;
      }

      public CmsFavoriteSQL andRefCategoryGreaterThan(java.lang.Long value) {
          addCriterion("ref_category", value, ConditionMode.GREATER_THEN, "refCategory", "java.lang.Long", "Float");
          return this;
      }

      public CmsFavoriteSQL andRefCategoryGreaterThanOrEqualTo(java.lang.Long value) {
          addCriterion("ref_category", value, ConditionMode.GREATER_EQUAL, "refCategory", "java.lang.Long", "Float");
          return this;
      }

      public CmsFavoriteSQL andRefCategoryLessThan(java.lang.Long value) {
          addCriterion("ref_category", value, ConditionMode.LESS_THEN, "refCategory", "java.lang.Long", "Float");
          return this;
      }

      public CmsFavoriteSQL andRefCategoryLessThanOrEqualTo(java.lang.Long value) {
          addCriterion("ref_category", value, ConditionMode.LESS_EQUAL, "refCategory", "java.lang.Long", "Float");
          return this;
      }

      public CmsFavoriteSQL andRefCategoryBetween(java.lang.Long value1, java.lang.Long value2) {
    	  addCriterion("ref_category", value1, value2, ConditionMode.BETWEEN, "refCategory", "java.lang.Long", "Float");
    	  return this;
      }

      public CmsFavoriteSQL andRefCategoryNotBetween(java.lang.Long value1, java.lang.Long value2) {
          addCriterion("ref_category", value1, value2, ConditionMode.NOT_BETWEEN, "refCategory", "java.lang.Long", "Float");
          return this;
      }
        
      public CmsFavoriteSQL andRefCategoryIn(List<java.lang.Long> values) {
          addCriterion("ref_category", values, ConditionMode.IN, "refCategory", "java.lang.Long", "Float");
          return this;
      }

      public CmsFavoriteSQL andRefCategoryNotIn(List<java.lang.Long> values) {
          addCriterion("ref_category", values, ConditionMode.NOT_IN, "refCategory", "java.lang.Long", "Float");
          return this;
      }
	public CmsFavoriteSQL andRefTagIsNull() {
		isnull("ref_tag");
		return this;
	}
	
	public CmsFavoriteSQL andRefTagIsNotNull() {
		notNull("ref_tag");
		return this;
	}
	
	public CmsFavoriteSQL andRefTagIsEmpty() {
		empty("ref_tag");
		return this;
	}

	public CmsFavoriteSQL andRefTagIsNotEmpty() {
		notEmpty("ref_tag");
		return this;
	}
       public CmsFavoriteSQL andRefTagLike(java.lang.String value) {
    	   addCriterion("ref_tag", value, ConditionMode.FUZZY, "refTag", "java.lang.String", "Float");
    	   return this;
      }

      public CmsFavoriteSQL andRefTagNotLike(java.lang.String value) {
          addCriterion("ref_tag", value, ConditionMode.NOT_FUZZY, "refTag", "java.lang.String", "Float");
          return this;
      }
      public CmsFavoriteSQL andRefTagEqualTo(java.lang.String value) {
          addCriterion("ref_tag", value, ConditionMode.EQUAL, "refTag", "java.lang.String", "String");
          return this;
      }

      public CmsFavoriteSQL andRefTagNotEqualTo(java.lang.String value) {
          addCriterion("ref_tag", value, ConditionMode.NOT_EQUAL, "refTag", "java.lang.String", "String");
          return this;
      }

      public CmsFavoriteSQL andRefTagGreaterThan(java.lang.String value) {
          addCriterion("ref_tag", value, ConditionMode.GREATER_THEN, "refTag", "java.lang.String", "String");
          return this;
      }

      public CmsFavoriteSQL andRefTagGreaterThanOrEqualTo(java.lang.String value) {
          addCriterion("ref_tag", value, ConditionMode.GREATER_EQUAL, "refTag", "java.lang.String", "String");
          return this;
      }

      public CmsFavoriteSQL andRefTagLessThan(java.lang.String value) {
          addCriterion("ref_tag", value, ConditionMode.LESS_THEN, "refTag", "java.lang.String", "String");
          return this;
      }

      public CmsFavoriteSQL andRefTagLessThanOrEqualTo(java.lang.String value) {
          addCriterion("ref_tag", value, ConditionMode.LESS_EQUAL, "refTag", "java.lang.String", "String");
          return this;
      }

      public CmsFavoriteSQL andRefTagBetween(java.lang.String value1, java.lang.String value2) {
    	  addCriterion("ref_tag", value1, value2, ConditionMode.BETWEEN, "refTag", "java.lang.String", "String");
    	  return this;
      }

      public CmsFavoriteSQL andRefTagNotBetween(java.lang.String value1, java.lang.String value2) {
          addCriterion("ref_tag", value1, value2, ConditionMode.NOT_BETWEEN, "refTag", "java.lang.String", "String");
          return this;
      }
        
      public CmsFavoriteSQL andRefTagIn(List<java.lang.String> values) {
          addCriterion("ref_tag", values, ConditionMode.IN, "refTag", "java.lang.String", "String");
          return this;
      }

      public CmsFavoriteSQL andRefTagNotIn(List<java.lang.String> values) {
          addCriterion("ref_tag", values, ConditionMode.NOT_IN, "refTag", "java.lang.String", "String");
          return this;
      }
	public CmsFavoriteSQL andNoteIsNull() {
		isnull("note");
		return this;
	}
	
	public CmsFavoriteSQL andNoteIsNotNull() {
		notNull("note");
		return this;
	}
	
	public CmsFavoriteSQL andNoteIsEmpty() {
		empty("note");
		return this;
	}

	public CmsFavoriteSQL andNoteIsNotEmpty() {
		notEmpty("note");
		return this;
	}
       public CmsFavoriteSQL andNoteLike(java.lang.String value) {
    	   addCriterion("note", value, ConditionMode.FUZZY, "note", "java.lang.String", "String");
    	   return this;
      }

      public CmsFavoriteSQL andNoteNotLike(java.lang.String value) {
          addCriterion("note", value, ConditionMode.NOT_FUZZY, "note", "java.lang.String", "String");
          return this;
      }
      public CmsFavoriteSQL andNoteEqualTo(java.lang.String value) {
          addCriterion("note", value, ConditionMode.EQUAL, "note", "java.lang.String", "String");
          return this;
      }

      public CmsFavoriteSQL andNoteNotEqualTo(java.lang.String value) {
          addCriterion("note", value, ConditionMode.NOT_EQUAL, "note", "java.lang.String", "String");
          return this;
      }

      public CmsFavoriteSQL andNoteGreaterThan(java.lang.String value) {
          addCriterion("note", value, ConditionMode.GREATER_THEN, "note", "java.lang.String", "String");
          return this;
      }

      public CmsFavoriteSQL andNoteGreaterThanOrEqualTo(java.lang.String value) {
          addCriterion("note", value, ConditionMode.GREATER_EQUAL, "note", "java.lang.String", "String");
          return this;
      }

      public CmsFavoriteSQL andNoteLessThan(java.lang.String value) {
          addCriterion("note", value, ConditionMode.LESS_THEN, "note", "java.lang.String", "String");
          return this;
      }

      public CmsFavoriteSQL andNoteLessThanOrEqualTo(java.lang.String value) {
          addCriterion("note", value, ConditionMode.LESS_EQUAL, "note", "java.lang.String", "String");
          return this;
      }

      public CmsFavoriteSQL andNoteBetween(java.lang.String value1, java.lang.String value2) {
    	  addCriterion("note", value1, value2, ConditionMode.BETWEEN, "note", "java.lang.String", "String");
    	  return this;
      }

      public CmsFavoriteSQL andNoteNotBetween(java.lang.String value1, java.lang.String value2) {
          addCriterion("note", value1, value2, ConditionMode.NOT_BETWEEN, "note", "java.lang.String", "String");
          return this;
      }
        
      public CmsFavoriteSQL andNoteIn(List<java.lang.String> values) {
          addCriterion("note", values, ConditionMode.IN, "note", "java.lang.String", "String");
          return this;
      }

      public CmsFavoriteSQL andNoteNotIn(List<java.lang.String> values) {
          addCriterion("note", values, ConditionMode.NOT_IN, "note", "java.lang.String", "String");
          return this;
      }
	public CmsFavoriteSQL andCtimeIsNull() {
		isnull("ctime");
		return this;
	}
	
	public CmsFavoriteSQL andCtimeIsNotNull() {
		notNull("ctime");
		return this;
	}
	
	public CmsFavoriteSQL andCtimeIsEmpty() {
		empty("ctime");
		return this;
	}

	public CmsFavoriteSQL andCtimeIsNotEmpty() {
		notEmpty("ctime");
		return this;
	}
      public CmsFavoriteSQL andCtimeEqualTo(java.util.Date value) {
          addCriterion("ctime", value, ConditionMode.EQUAL, "ctime", "java.util.Date", "String");
          return this;
      }

      public CmsFavoriteSQL andCtimeNotEqualTo(java.util.Date value) {
          addCriterion("ctime", value, ConditionMode.NOT_EQUAL, "ctime", "java.util.Date", "String");
          return this;
      }

      public CmsFavoriteSQL andCtimeGreaterThan(java.util.Date value) {
          addCriterion("ctime", value, ConditionMode.GREATER_THEN, "ctime", "java.util.Date", "String");
          return this;
      }

      public CmsFavoriteSQL andCtimeGreaterThanOrEqualTo(java.util.Date value) {
          addCriterion("ctime", value, ConditionMode.GREATER_EQUAL, "ctime", "java.util.Date", "String");
          return this;
      }

      public CmsFavoriteSQL andCtimeLessThan(java.util.Date value) {
          addCriterion("ctime", value, ConditionMode.LESS_THEN, "ctime", "java.util.Date", "String");
          return this;
      }

      public CmsFavoriteSQL andCtimeLessThanOrEqualTo(java.util.Date value) {
          addCriterion("ctime", value, ConditionMode.LESS_EQUAL, "ctime", "java.util.Date", "String");
          return this;
      }

      public CmsFavoriteSQL andCtimeBetween(java.util.Date value1, java.util.Date value2) {
    	  addCriterion("ctime", value1, value2, ConditionMode.BETWEEN, "ctime", "java.util.Date", "String");
    	  return this;
      }

      public CmsFavoriteSQL andCtimeNotBetween(java.util.Date value1, java.util.Date value2) {
          addCriterion("ctime", value1, value2, ConditionMode.NOT_BETWEEN, "ctime", "java.util.Date", "String");
          return this;
      }
        
      public CmsFavoriteSQL andCtimeIn(List<java.util.Date> values) {
          addCriterion("ctime", values, ConditionMode.IN, "ctime", "java.util.Date", "String");
          return this;
      }

      public CmsFavoriteSQL andCtimeNotIn(List<java.util.Date> values) {
          addCriterion("ctime", values, ConditionMode.NOT_IN, "ctime", "java.util.Date", "String");
          return this;
      }
}