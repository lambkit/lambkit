/**
 * Copyright (c) 2015-2017, Henry Yang 杨勇 (gismail@foxmail.com).
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.lambkit.module.cms.row;

import com.lambkit.db.RowModel;

/**
 * @author yangyong 
 * @website: www.lambkit.com
 * @email: gismail@foxmail.com
 * @date ${date}
 * @version 1.0
 * @since 1.0
 */

public class CmsBanner extends RowModel<CmsBanner> {
	public CmsBanner() {
		setTableName("cms_banner");
		setPrimaryKey("banner_id");
	}
	public java.lang.Long getBannerId() {
		return this.get("banner_id");
	}
	public void setBannerId(java.lang.Long bannerId) {
		this.set("banner_id", bannerId);
	}
	public java.lang.String getTitle() {
		return this.get("title");
	}
	public void setTitle(java.lang.String title) {
		this.set("title", title);
	}
	public java.lang.String getUrl() {
		return this.get("url");
	}
	public void setUrl(java.lang.String url) {
		this.set("url", url);
	}
	public java.lang.Integer getTarget() {
		return this.get("target");
	}
	public void setTarget(java.lang.Integer target) {
		this.set("target", target);
	}
	public java.lang.Integer getGroupId() {
		return this.get("group_id");
	}
	public void setGroupId(java.lang.Integer groupId) {
		this.set("group_id", groupId);
	}
	public java.lang.String getPic() {
		return this.get("pic");
	}
	public void setPic(java.lang.String pic) {
		this.set("pic", pic);
	}
	public java.lang.Integer getOrders() {
		return this.get("orders");
	}
	public void setOrders(java.lang.Integer orders) {
		this.set("orders", orders);
	}
	public java.lang.Integer getStatus() {
		return this.get("status");
	}
	public void setStatus(java.lang.Integer status) {
		this.set("status", status);
	}
}
