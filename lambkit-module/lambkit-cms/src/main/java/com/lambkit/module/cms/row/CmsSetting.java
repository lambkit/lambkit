/**
 * Copyright (c) 2015-2017, Henry Yang 杨勇 (gismail@foxmail.com).
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.lambkit.module.cms.row;

import com.lambkit.db.RowModel;

/**
 * @author yangyong 
 * @website: www.lambkit.com
 * @email: gismail@foxmail.com
 * @date ${date}
 * @version 1.0
 * @since 1.0
 */

public class CmsSetting extends RowModel<CmsSetting> {
	public CmsSetting() {
		setTableName("cms_setting");
		setPrimaryKey("setting_id");
	}
	public java.lang.Integer getSettingId() {
		return this.get("setting_id");
	}
	public void setSettingId(java.lang.Integer settingId) {
		this.set("setting_id", settingId);
	}
	public java.lang.String getSettingKey() {
		return this.get("setting_key");
	}
	public void setSettingKey(java.lang.String settingKey) {
		this.set("setting_key", settingKey);
	}
	public java.lang.String getSettingValue() {
		return this.get("setting_value");
	}
	public void setSettingValue(java.lang.String settingValue) {
		this.set("setting_value", settingValue);
	}
	public java.lang.String getSettingName() {
		return this.get("setting_name");
	}
	public void setSettingName(java.lang.String settingName) {
		this.set("setting_name", settingName);
	}
	public java.lang.Integer getBisys() {
		return this.get("bisys");
	}
	public void setBisys(java.lang.Integer bisys) {
		this.set("bisys", bisys);
	}
	public java.lang.String getRemark() {
		return this.get("remark");
	}
	public void setRemark(java.lang.String remark) {
		this.set("remark", remark);
	}
	public java.util.Date getCreated() {
		return this.get("created");
	}
	public void setCreated(java.util.Date created) {
		this.set("created", created);
	}
}
