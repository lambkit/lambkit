/**
 * Copyright (c) 2015-2017, Henry Yang 杨勇 (gismail@foxmail.com).
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.lambkit.module.cms.row;

import com.lambkit.db.RowModel;

/**
 * @author yangyong 
 * @website: www.lambkit.com
 * @email: gismail@foxmail.com
 * @date ${date}
 * @version 1.0
 * @since 1.0
 */

public class CmsFavorite extends RowModel<CmsFavorite> {
	public CmsFavorite() {
		setTableName("cms_favorite");
		setPrimaryKey("favorite_id");
	}
	public java.lang.Integer getFavoriteId() {
		return this.get("favorite_id");
	}
	public void setFavoriteId(java.lang.Integer favoriteId) {
		this.set("favorite_id", favoriteId);
	}
	public java.lang.Long getUserId() {
		return this.get("user_id");
	}
	public void setUserId(java.lang.Long userId) {
		this.set("user_id", userId);
	}
	public java.lang.Long getRefId() {
		return this.get("ref_id");
	}
	public void setRefId(java.lang.Long refId) {
		this.set("ref_id", refId);
	}
	public java.lang.Integer getRefType() {
		return this.get("ref_type");
	}
	public void setRefType(java.lang.Integer refType) {
		this.set("ref_type", refType);
	}
	public java.lang.Long getRefCategory() {
		return this.get("ref_category");
	}
	public void setRefCategory(java.lang.Long refCategory) {
		this.set("ref_category", refCategory);
	}
	public java.lang.String getRefTag() {
		return this.get("ref_tag");
	}
	public void setRefTag(java.lang.String refTag) {
		this.set("ref_tag", refTag);
	}
	public java.lang.String getNote() {
		return this.get("note");
	}
	public void setNote(java.lang.String note) {
		this.set("note", note);
	}
	public java.util.Date getCtime() {
		return this.get("ctime");
	}
	public void setCtime(java.util.Date ctime) {
		this.set("ctime", ctime);
	}
}
