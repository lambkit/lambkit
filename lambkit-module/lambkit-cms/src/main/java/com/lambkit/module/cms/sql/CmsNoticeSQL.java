/**
 * Copyright (c) 2015-2017, Henry Yang 杨勇 (gismail@foxmail.com).
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.lambkit.module.cms.sql;

import com.lambkit.db.sql.ConditionMode;
import com.lambkit.db.sql.Column;
import com.lambkit.db.sql.Columns;
import com.lambkit.db.sql.Example;

import java.io.Serializable;
import java.util.List;

/**
 * @author yangyong 
 * @website: www.lambkit.com
 * @email: gismail@foxmail.com
 * @date ${date}
 * @version 1.0
 * @since 1.0
 */

public class CmsNoticeSQL extends Columns implements Serializable {
	private static final long serialVersionUID = 1L;
	
	public static CmsNoticeSQL of() {
		return new CmsNoticeSQL();
	}
	
	public static CmsNoticeSQL by(Column column) {
		CmsNoticeSQL that = new CmsNoticeSQL();
		that.add(column);
        return that;
    }

    public static CmsNoticeSQL by(String name, Object value) {
        return (CmsNoticeSQL) create().eq(name, value);
    }
    
    public Example example() {
    	return Example.create("cms_notice", this);
    }
    
    /**
     * equals
     *
     * @param name
     * @param value
     * @return
     */
    public CmsNoticeSQL eq(String name, Object value) {
    	super.eq(name, value);
        return this;
    }

    /**
     * not equals !=
     *
     * @param name
     * @param value
     * @return
     */
    public CmsNoticeSQL ne(String name, Object value) {
    	super.ne(name, value);
        return this;
    }


    /**
     * like
     *
     * @param name
     * @param value
     * @return
     */

    public CmsNoticeSQL like(String name, Object value) {
    	super.like(name, value);
        return this;
    }
    
    public CmsNoticeSQL notLike(String name, Object value) {
    	super.notLike(name, value);
        return this;
    }

    /**
     * 大于 great than
     *
     * @param name
     * @param value
     * @return
     */
    public CmsNoticeSQL gt(String name, Object value) {
    	super.gt(name, value);
        return this;
    }

    /**
     * 大于等于 great or equal
     *
     * @param name
     * @param value
     * @return
     */
    public CmsNoticeSQL ge(String name, Object value) {
    	super.ge(name, value);
        return this;
    }

    /**
     * 小于 less than
     *
     * @param name
     * @param value
     * @return
     */
    public CmsNoticeSQL lt(String name, Object value) {
    	super.lt(name, value);
        return this;
    }

    /**
     * 小于等于 less or equal
     *
     * @param name
     * @param value
     * @return
     */
    public CmsNoticeSQL le(String name, Object value) {
    	super.le(name, value);
        return this;
    }
    
    public CmsNoticeSQL isnull(String name) {
    	super.isnull(name);
        return this;
    } 

    public CmsNoticeSQL notNull(String name) {
    	super.notNull(name);
        return this;
    } 
    
    public CmsNoticeSQL empty(String name) {
    	super.empty(name);
        return this;
    } 
    
    public CmsNoticeSQL notEmpty(String name) {
    	super.notEmpty(name);
        return this;
    } 
    
    public CmsNoticeSQL add(Column column) {
    	super.add(column);
    	return this;
    }
    
    /**************************/
	
	public void addCriterion(String name, Object value, ConditionMode logic, String property, String typeHandler, String valueType) {
		 if (value == null) {
			 throw new RuntimeException("Value for " + property + " cannot be null");
		 }
		 add(Column.create(name, value, logic, typeHandler, valueType));
	}
   
	public void addCriterion(String name, Object value1, Object value2, ConditionMode logic, String property, String typeHandler, String valueType) {
		 if (value1 == null || value2 == null) {
			 throw new RuntimeException("Between values for " + property + " cannot be null");
		 }
		 add(Column.create(name, value1, value2, logic, typeHandler, valueType));
	}
		 
	public CmsNoticeSQL andNoticeIdIsNull() {
		isnull("notice_id");
		return this;
	}
	
	public CmsNoticeSQL andNoticeIdIsNotNull() {
		notNull("notice_id");
		return this;
	}
	
	public CmsNoticeSQL andNoticeIdIsEmpty() {
		empty("notice_id");
		return this;
	}

	public CmsNoticeSQL andNoticeIdIsNotEmpty() {
		notEmpty("notice_id");
		return this;
	}
      public CmsNoticeSQL andNoticeIdEqualTo(java.lang.Long value) {
          addCriterion("notice_id", value, ConditionMode.EQUAL, "noticeId", "java.lang.Long", "Float");
          return this;
      }

      public CmsNoticeSQL andNoticeIdNotEqualTo(java.lang.Long value) {
          addCriterion("notice_id", value, ConditionMode.NOT_EQUAL, "noticeId", "java.lang.Long", "Float");
          return this;
      }

      public CmsNoticeSQL andNoticeIdGreaterThan(java.lang.Long value) {
          addCriterion("notice_id", value, ConditionMode.GREATER_THEN, "noticeId", "java.lang.Long", "Float");
          return this;
      }

      public CmsNoticeSQL andNoticeIdGreaterThanOrEqualTo(java.lang.Long value) {
          addCriterion("notice_id", value, ConditionMode.GREATER_EQUAL, "noticeId", "java.lang.Long", "Float");
          return this;
      }

      public CmsNoticeSQL andNoticeIdLessThan(java.lang.Long value) {
          addCriterion("notice_id", value, ConditionMode.LESS_THEN, "noticeId", "java.lang.Long", "Float");
          return this;
      }

      public CmsNoticeSQL andNoticeIdLessThanOrEqualTo(java.lang.Long value) {
          addCriterion("notice_id", value, ConditionMode.LESS_EQUAL, "noticeId", "java.lang.Long", "Float");
          return this;
      }

      public CmsNoticeSQL andNoticeIdBetween(java.lang.Long value1, java.lang.Long value2) {
    	  addCriterion("notice_id", value1, value2, ConditionMode.BETWEEN, "noticeId", "java.lang.Long", "Float");
    	  return this;
      }

      public CmsNoticeSQL andNoticeIdNotBetween(java.lang.Long value1, java.lang.Long value2) {
          addCriterion("notice_id", value1, value2, ConditionMode.NOT_BETWEEN, "noticeId", "java.lang.Long", "Float");
          return this;
      }
        
      public CmsNoticeSQL andNoticeIdIn(List<java.lang.Long> values) {
          addCriterion("notice_id", values, ConditionMode.IN, "noticeId", "java.lang.Long", "Float");
          return this;
      }

      public CmsNoticeSQL andNoticeIdNotIn(List<java.lang.Long> values) {
          addCriterion("notice_id", values, ConditionMode.NOT_IN, "noticeId", "java.lang.Long", "Float");
          return this;
      }
	public CmsNoticeSQL andTitleIsNull() {
		isnull("title");
		return this;
	}
	
	public CmsNoticeSQL andTitleIsNotNull() {
		notNull("title");
		return this;
	}
	
	public CmsNoticeSQL andTitleIsEmpty() {
		empty("title");
		return this;
	}

	public CmsNoticeSQL andTitleIsNotEmpty() {
		notEmpty("title");
		return this;
	}
       public CmsNoticeSQL andTitleLike(java.lang.String value) {
    	   addCriterion("title", value, ConditionMode.FUZZY, "title", "java.lang.String", "Float");
    	   return this;
      }

      public CmsNoticeSQL andTitleNotLike(java.lang.String value) {
          addCriterion("title", value, ConditionMode.NOT_FUZZY, "title", "java.lang.String", "Float");
          return this;
      }
      public CmsNoticeSQL andTitleEqualTo(java.lang.String value) {
          addCriterion("title", value, ConditionMode.EQUAL, "title", "java.lang.String", "String");
          return this;
      }

      public CmsNoticeSQL andTitleNotEqualTo(java.lang.String value) {
          addCriterion("title", value, ConditionMode.NOT_EQUAL, "title", "java.lang.String", "String");
          return this;
      }

      public CmsNoticeSQL andTitleGreaterThan(java.lang.String value) {
          addCriterion("title", value, ConditionMode.GREATER_THEN, "title", "java.lang.String", "String");
          return this;
      }

      public CmsNoticeSQL andTitleGreaterThanOrEqualTo(java.lang.String value) {
          addCriterion("title", value, ConditionMode.GREATER_EQUAL, "title", "java.lang.String", "String");
          return this;
      }

      public CmsNoticeSQL andTitleLessThan(java.lang.String value) {
          addCriterion("title", value, ConditionMode.LESS_THEN, "title", "java.lang.String", "String");
          return this;
      }

      public CmsNoticeSQL andTitleLessThanOrEqualTo(java.lang.String value) {
          addCriterion("title", value, ConditionMode.LESS_EQUAL, "title", "java.lang.String", "String");
          return this;
      }

      public CmsNoticeSQL andTitleBetween(java.lang.String value1, java.lang.String value2) {
    	  addCriterion("title", value1, value2, ConditionMode.BETWEEN, "title", "java.lang.String", "String");
    	  return this;
      }

      public CmsNoticeSQL andTitleNotBetween(java.lang.String value1, java.lang.String value2) {
          addCriterion("title", value1, value2, ConditionMode.NOT_BETWEEN, "title", "java.lang.String", "String");
          return this;
      }
        
      public CmsNoticeSQL andTitleIn(List<java.lang.String> values) {
          addCriterion("title", values, ConditionMode.IN, "title", "java.lang.String", "String");
          return this;
      }

      public CmsNoticeSQL andTitleNotIn(List<java.lang.String> values) {
          addCriterion("title", values, ConditionMode.NOT_IN, "title", "java.lang.String", "String");
          return this;
      }
	public CmsNoticeSQL andContentIsNull() {
		isnull("content");
		return this;
	}
	
	public CmsNoticeSQL andContentIsNotNull() {
		notNull("content");
		return this;
	}
	
	public CmsNoticeSQL andContentIsEmpty() {
		empty("content");
		return this;
	}

	public CmsNoticeSQL andContentIsNotEmpty() {
		notEmpty("content");
		return this;
	}
       public CmsNoticeSQL andContentLike(java.lang.String value) {
    	   addCriterion("content", value, ConditionMode.FUZZY, "content", "java.lang.String", "String");
    	   return this;
      }

      public CmsNoticeSQL andContentNotLike(java.lang.String value) {
          addCriterion("content", value, ConditionMode.NOT_FUZZY, "content", "java.lang.String", "String");
          return this;
      }
      public CmsNoticeSQL andContentEqualTo(java.lang.String value) {
          addCriterion("content", value, ConditionMode.EQUAL, "content", "java.lang.String", "String");
          return this;
      }

      public CmsNoticeSQL andContentNotEqualTo(java.lang.String value) {
          addCriterion("content", value, ConditionMode.NOT_EQUAL, "content", "java.lang.String", "String");
          return this;
      }

      public CmsNoticeSQL andContentGreaterThan(java.lang.String value) {
          addCriterion("content", value, ConditionMode.GREATER_THEN, "content", "java.lang.String", "String");
          return this;
      }

      public CmsNoticeSQL andContentGreaterThanOrEqualTo(java.lang.String value) {
          addCriterion("content", value, ConditionMode.GREATER_EQUAL, "content", "java.lang.String", "String");
          return this;
      }

      public CmsNoticeSQL andContentLessThan(java.lang.String value) {
          addCriterion("content", value, ConditionMode.LESS_THEN, "content", "java.lang.String", "String");
          return this;
      }

      public CmsNoticeSQL andContentLessThanOrEqualTo(java.lang.String value) {
          addCriterion("content", value, ConditionMode.LESS_EQUAL, "content", "java.lang.String", "String");
          return this;
      }

      public CmsNoticeSQL andContentBetween(java.lang.String value1, java.lang.String value2) {
    	  addCriterion("content", value1, value2, ConditionMode.BETWEEN, "content", "java.lang.String", "String");
    	  return this;
      }

      public CmsNoticeSQL andContentNotBetween(java.lang.String value1, java.lang.String value2) {
          addCriterion("content", value1, value2, ConditionMode.NOT_BETWEEN, "content", "java.lang.String", "String");
          return this;
      }
        
      public CmsNoticeSQL andContentIn(List<java.lang.String> values) {
          addCriterion("content", values, ConditionMode.IN, "content", "java.lang.String", "String");
          return this;
      }

      public CmsNoticeSQL andContentNotIn(List<java.lang.String> values) {
          addCriterion("content", values, ConditionMode.NOT_IN, "content", "java.lang.String", "String");
          return this;
      }
	public CmsNoticeSQL andCreatedIsNull() {
		isnull("created");
		return this;
	}
	
	public CmsNoticeSQL andCreatedIsNotNull() {
		notNull("created");
		return this;
	}
	
	public CmsNoticeSQL andCreatedIsEmpty() {
		empty("created");
		return this;
	}

	public CmsNoticeSQL andCreatedIsNotEmpty() {
		notEmpty("created");
		return this;
	}
      public CmsNoticeSQL andCreatedEqualTo(java.util.Date value) {
          addCriterion("created", value, ConditionMode.EQUAL, "created", "java.util.Date", "String");
          return this;
      }

      public CmsNoticeSQL andCreatedNotEqualTo(java.util.Date value) {
          addCriterion("created", value, ConditionMode.NOT_EQUAL, "created", "java.util.Date", "String");
          return this;
      }

      public CmsNoticeSQL andCreatedGreaterThan(java.util.Date value) {
          addCriterion("created", value, ConditionMode.GREATER_THEN, "created", "java.util.Date", "String");
          return this;
      }

      public CmsNoticeSQL andCreatedGreaterThanOrEqualTo(java.util.Date value) {
          addCriterion("created", value, ConditionMode.GREATER_EQUAL, "created", "java.util.Date", "String");
          return this;
      }

      public CmsNoticeSQL andCreatedLessThan(java.util.Date value) {
          addCriterion("created", value, ConditionMode.LESS_THEN, "created", "java.util.Date", "String");
          return this;
      }

      public CmsNoticeSQL andCreatedLessThanOrEqualTo(java.util.Date value) {
          addCriterion("created", value, ConditionMode.LESS_EQUAL, "created", "java.util.Date", "String");
          return this;
      }

      public CmsNoticeSQL andCreatedBetween(java.util.Date value1, java.util.Date value2) {
    	  addCriterion("created", value1, value2, ConditionMode.BETWEEN, "created", "java.util.Date", "String");
    	  return this;
      }

      public CmsNoticeSQL andCreatedNotBetween(java.util.Date value1, java.util.Date value2) {
          addCriterion("created", value1, value2, ConditionMode.NOT_BETWEEN, "created", "java.util.Date", "String");
          return this;
      }
        
      public CmsNoticeSQL andCreatedIn(List<java.util.Date> values) {
          addCriterion("created", values, ConditionMode.IN, "created", "java.util.Date", "String");
          return this;
      }

      public CmsNoticeSQL andCreatedNotIn(List<java.util.Date> values) {
          addCriterion("created", values, ConditionMode.NOT_IN, "created", "java.util.Date", "String");
          return this;
      }
	public CmsNoticeSQL andUseridIsNull() {
		isnull("userid");
		return this;
	}
	
	public CmsNoticeSQL andUseridIsNotNull() {
		notNull("userid");
		return this;
	}
	
	public CmsNoticeSQL andUseridIsEmpty() {
		empty("userid");
		return this;
	}

	public CmsNoticeSQL andUseridIsNotEmpty() {
		notEmpty("userid");
		return this;
	}
      public CmsNoticeSQL andUseridEqualTo(java.lang.Integer value) {
          addCriterion("userid", value, ConditionMode.EQUAL, "userid", "java.lang.Integer", "Float");
          return this;
      }

      public CmsNoticeSQL andUseridNotEqualTo(java.lang.Integer value) {
          addCriterion("userid", value, ConditionMode.NOT_EQUAL, "userid", "java.lang.Integer", "Float");
          return this;
      }

      public CmsNoticeSQL andUseridGreaterThan(java.lang.Integer value) {
          addCriterion("userid", value, ConditionMode.GREATER_THEN, "userid", "java.lang.Integer", "Float");
          return this;
      }

      public CmsNoticeSQL andUseridGreaterThanOrEqualTo(java.lang.Integer value) {
          addCriterion("userid", value, ConditionMode.GREATER_EQUAL, "userid", "java.lang.Integer", "Float");
          return this;
      }

      public CmsNoticeSQL andUseridLessThan(java.lang.Integer value) {
          addCriterion("userid", value, ConditionMode.LESS_THEN, "userid", "java.lang.Integer", "Float");
          return this;
      }

      public CmsNoticeSQL andUseridLessThanOrEqualTo(java.lang.Integer value) {
          addCriterion("userid", value, ConditionMode.LESS_EQUAL, "userid", "java.lang.Integer", "Float");
          return this;
      }

      public CmsNoticeSQL andUseridBetween(java.lang.Integer value1, java.lang.Integer value2) {
    	  addCriterion("userid", value1, value2, ConditionMode.BETWEEN, "userid", "java.lang.Integer", "Float");
    	  return this;
      }

      public CmsNoticeSQL andUseridNotBetween(java.lang.Integer value1, java.lang.Integer value2) {
          addCriterion("userid", value1, value2, ConditionMode.NOT_BETWEEN, "userid", "java.lang.Integer", "Float");
          return this;
      }
        
      public CmsNoticeSQL andUseridIn(List<java.lang.Integer> values) {
          addCriterion("userid", values, ConditionMode.IN, "userid", "java.lang.Integer", "Float");
          return this;
      }

      public CmsNoticeSQL andUseridNotIn(List<java.lang.Integer> values) {
          addCriterion("userid", values, ConditionMode.NOT_IN, "userid", "java.lang.Integer", "Float");
          return this;
      }
	public CmsNoticeSQL andNoticeTypeIsNull() {
		isnull("notice_type");
		return this;
	}
	
	public CmsNoticeSQL andNoticeTypeIsNotNull() {
		notNull("notice_type");
		return this;
	}
	
	public CmsNoticeSQL andNoticeTypeIsEmpty() {
		empty("notice_type");
		return this;
	}

	public CmsNoticeSQL andNoticeTypeIsNotEmpty() {
		notEmpty("notice_type");
		return this;
	}
      public CmsNoticeSQL andNoticeTypeEqualTo(java.lang.Integer value) {
          addCriterion("notice_type", value, ConditionMode.EQUAL, "noticeType", "java.lang.Integer", "Float");
          return this;
      }

      public CmsNoticeSQL andNoticeTypeNotEqualTo(java.lang.Integer value) {
          addCriterion("notice_type", value, ConditionMode.NOT_EQUAL, "noticeType", "java.lang.Integer", "Float");
          return this;
      }

      public CmsNoticeSQL andNoticeTypeGreaterThan(java.lang.Integer value) {
          addCriterion("notice_type", value, ConditionMode.GREATER_THEN, "noticeType", "java.lang.Integer", "Float");
          return this;
      }

      public CmsNoticeSQL andNoticeTypeGreaterThanOrEqualTo(java.lang.Integer value) {
          addCriterion("notice_type", value, ConditionMode.GREATER_EQUAL, "noticeType", "java.lang.Integer", "Float");
          return this;
      }

      public CmsNoticeSQL andNoticeTypeLessThan(java.lang.Integer value) {
          addCriterion("notice_type", value, ConditionMode.LESS_THEN, "noticeType", "java.lang.Integer", "Float");
          return this;
      }

      public CmsNoticeSQL andNoticeTypeLessThanOrEqualTo(java.lang.Integer value) {
          addCriterion("notice_type", value, ConditionMode.LESS_EQUAL, "noticeType", "java.lang.Integer", "Float");
          return this;
      }

      public CmsNoticeSQL andNoticeTypeBetween(java.lang.Integer value1, java.lang.Integer value2) {
    	  addCriterion("notice_type", value1, value2, ConditionMode.BETWEEN, "noticeType", "java.lang.Integer", "Float");
    	  return this;
      }

      public CmsNoticeSQL andNoticeTypeNotBetween(java.lang.Integer value1, java.lang.Integer value2) {
          addCriterion("notice_type", value1, value2, ConditionMode.NOT_BETWEEN, "noticeType", "java.lang.Integer", "Float");
          return this;
      }
        
      public CmsNoticeSQL andNoticeTypeIn(List<java.lang.Integer> values) {
          addCriterion("notice_type", values, ConditionMode.IN, "noticeType", "java.lang.Integer", "Float");
          return this;
      }

      public CmsNoticeSQL andNoticeTypeNotIn(List<java.lang.Integer> values) {
          addCriterion("notice_type", values, ConditionMode.NOT_IN, "noticeType", "java.lang.Integer", "Float");
          return this;
      }
	public CmsNoticeSQL andStatusIsNull() {
		isnull("status");
		return this;
	}
	
	public CmsNoticeSQL andStatusIsNotNull() {
		notNull("status");
		return this;
	}
	
	public CmsNoticeSQL andStatusIsEmpty() {
		empty("status");
		return this;
	}

	public CmsNoticeSQL andStatusIsNotEmpty() {
		notEmpty("status");
		return this;
	}
      public CmsNoticeSQL andStatusEqualTo(java.lang.Integer value) {
          addCriterion("status", value, ConditionMode.EQUAL, "status", "java.lang.Integer", "Float");
          return this;
      }

      public CmsNoticeSQL andStatusNotEqualTo(java.lang.Integer value) {
          addCriterion("status", value, ConditionMode.NOT_EQUAL, "status", "java.lang.Integer", "Float");
          return this;
      }

      public CmsNoticeSQL andStatusGreaterThan(java.lang.Integer value) {
          addCriterion("status", value, ConditionMode.GREATER_THEN, "status", "java.lang.Integer", "Float");
          return this;
      }

      public CmsNoticeSQL andStatusGreaterThanOrEqualTo(java.lang.Integer value) {
          addCriterion("status", value, ConditionMode.GREATER_EQUAL, "status", "java.lang.Integer", "Float");
          return this;
      }

      public CmsNoticeSQL andStatusLessThan(java.lang.Integer value) {
          addCriterion("status", value, ConditionMode.LESS_THEN, "status", "java.lang.Integer", "Float");
          return this;
      }

      public CmsNoticeSQL andStatusLessThanOrEqualTo(java.lang.Integer value) {
          addCriterion("status", value, ConditionMode.LESS_EQUAL, "status", "java.lang.Integer", "Float");
          return this;
      }

      public CmsNoticeSQL andStatusBetween(java.lang.Integer value1, java.lang.Integer value2) {
    	  addCriterion("status", value1, value2, ConditionMode.BETWEEN, "status", "java.lang.Integer", "Float");
    	  return this;
      }

      public CmsNoticeSQL andStatusNotBetween(java.lang.Integer value1, java.lang.Integer value2) {
          addCriterion("status", value1, value2, ConditionMode.NOT_BETWEEN, "status", "java.lang.Integer", "Float");
          return this;
      }
        
      public CmsNoticeSQL andStatusIn(List<java.lang.Integer> values) {
          addCriterion("status", values, ConditionMode.IN, "status", "java.lang.Integer", "Float");
          return this;
      }

      public CmsNoticeSQL andStatusNotIn(List<java.lang.Integer> values) {
          addCriterion("status", values, ConditionMode.NOT_IN, "status", "java.lang.Integer", "Float");
          return this;
      }
}