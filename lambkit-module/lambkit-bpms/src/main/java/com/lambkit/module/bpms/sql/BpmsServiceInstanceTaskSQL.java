/**
 * Copyright (c) 2015-2017, Henry Yang 杨勇 (gismail@foxmail.com).
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.lambkit.module.bpms.sql;

import com.lambkit.db.sql.ConditionMode;
import com.lambkit.db.sql.Column;
import com.lambkit.db.sql.Columns;
import com.lambkit.db.sql.Example;

import java.io.Serializable;
import java.util.List;

/**
 * @author yangyong 
 * @website: www.lambkit.com
 * @email: gismail@foxmail.com
 * @date ${date}
 * @version 1.0
 * @since 1.0
 */

public class BpmsServiceInstanceTaskSQL extends Columns implements Serializable {
	private static final long serialVersionUID = 1L;
	
	public static BpmsServiceInstanceTaskSQL of() {
		return new BpmsServiceInstanceTaskSQL();
	}
	
	public static BpmsServiceInstanceTaskSQL by(Column column) {
		BpmsServiceInstanceTaskSQL that = new BpmsServiceInstanceTaskSQL();
		that.add(column);
        return that;
    }

    public static BpmsServiceInstanceTaskSQL by(String name, Object value) {
        return (BpmsServiceInstanceTaskSQL) create().eq(name, value);
    }
    
    public Example example() {
    	return Example.create("bpms_service_instance_task", this);
    }
    
    /**
     * equals
     *
     * @param name
     * @param value
     * @return
     */
    public BpmsServiceInstanceTaskSQL eq(String name, Object value) {
    	super.eq(name, value);
        return this;
    }

    /**
     * not equals !=
     *
     * @param name
     * @param value
     * @return
     */
    public BpmsServiceInstanceTaskSQL ne(String name, Object value) {
    	super.ne(name, value);
        return this;
    }


    /**
     * like
     *
     * @param name
     * @param value
     * @return
     */

    public BpmsServiceInstanceTaskSQL like(String name, Object value) {
    	super.like(name, value);
        return this;
    }
    
    public BpmsServiceInstanceTaskSQL notLike(String name, Object value) {
    	super.notLike(name, value);
        return this;
    }

    /**
     * 大于 great than
     *
     * @param name
     * @param value
     * @return
     */
    public BpmsServiceInstanceTaskSQL gt(String name, Object value) {
    	super.gt(name, value);
        return this;
    }

    /**
     * 大于等于 great or equal
     *
     * @param name
     * @param value
     * @return
     */
    public BpmsServiceInstanceTaskSQL ge(String name, Object value) {
    	super.ge(name, value);
        return this;
    }

    /**
     * 小于 less than
     *
     * @param name
     * @param value
     * @return
     */
    public BpmsServiceInstanceTaskSQL lt(String name, Object value) {
    	super.lt(name, value);
        return this;
    }

    /**
     * 小于等于 less or equal
     *
     * @param name
     * @param value
     * @return
     */
    public BpmsServiceInstanceTaskSQL le(String name, Object value) {
    	super.le(name, value);
        return this;
    }
    
    public BpmsServiceInstanceTaskSQL isnull(String name) {
    	super.isnull(name);
        return this;
    } 

    public BpmsServiceInstanceTaskSQL notNull(String name) {
    	super.notNull(name);
        return this;
    } 
    
    public BpmsServiceInstanceTaskSQL empty(String name) {
    	super.empty(name);
        return this;
    } 
    
    public BpmsServiceInstanceTaskSQL notEmpty(String name) {
    	super.notEmpty(name);
        return this;
    } 
    
    public BpmsServiceInstanceTaskSQL add(Column column) {
    	super.add(column);
    	return this;
    }
    
    /**************************/
	
	public void addCriterion(String name, Object value, ConditionMode logic, String property, String typeHandler, String valueType) {
		 if (value == null) {
			 throw new RuntimeException("Value for " + property + " cannot be null");
		 }
		 add(Column.create(name, value, logic, typeHandler, valueType));
	}
   
	public void addCriterion(String name, Object value1, Object value2, ConditionMode logic, String property, String typeHandler, String valueType) {
		 if (value1 == null || value2 == null) {
			 throw new RuntimeException("Between values for " + property + " cannot be null");
		 }
		 add(Column.create(name, value1, value2, logic, typeHandler, valueType));
	}
		 
	public BpmsServiceInstanceTaskSQL andInstanceTaskIdIsNull() {
		isnull("instance_task_id");
		return this;
	}
	
	public BpmsServiceInstanceTaskSQL andInstanceTaskIdIsNotNull() {
		notNull("instance_task_id");
		return this;
	}
	
	public BpmsServiceInstanceTaskSQL andInstanceTaskIdIsEmpty() {
		empty("instance_task_id");
		return this;
	}

	public BpmsServiceInstanceTaskSQL andInstanceTaskIdIsNotEmpty() {
		notEmpty("instance_task_id");
		return this;
	}
      public BpmsServiceInstanceTaskSQL andInstanceTaskIdEqualTo(java.lang.Long value) {
          addCriterion("instance_task_id", value, ConditionMode.EQUAL, "instanceTaskId", "java.lang.Long", "Float");
          return this;
      }

      public BpmsServiceInstanceTaskSQL andInstanceTaskIdNotEqualTo(java.lang.Long value) {
          addCriterion("instance_task_id", value, ConditionMode.NOT_EQUAL, "instanceTaskId", "java.lang.Long", "Float");
          return this;
      }

      public BpmsServiceInstanceTaskSQL andInstanceTaskIdGreaterThan(java.lang.Long value) {
          addCriterion("instance_task_id", value, ConditionMode.GREATER_THEN, "instanceTaskId", "java.lang.Long", "Float");
          return this;
      }

      public BpmsServiceInstanceTaskSQL andInstanceTaskIdGreaterThanOrEqualTo(java.lang.Long value) {
          addCriterion("instance_task_id", value, ConditionMode.GREATER_EQUAL, "instanceTaskId", "java.lang.Long", "Float");
          return this;
      }

      public BpmsServiceInstanceTaskSQL andInstanceTaskIdLessThan(java.lang.Long value) {
          addCriterion("instance_task_id", value, ConditionMode.LESS_THEN, "instanceTaskId", "java.lang.Long", "Float");
          return this;
      }

      public BpmsServiceInstanceTaskSQL andInstanceTaskIdLessThanOrEqualTo(java.lang.Long value) {
          addCriterion("instance_task_id", value, ConditionMode.LESS_EQUAL, "instanceTaskId", "java.lang.Long", "Float");
          return this;
      }

      public BpmsServiceInstanceTaskSQL andInstanceTaskIdBetween(java.lang.Long value1, java.lang.Long value2) {
    	  addCriterion("instance_task_id", value1, value2, ConditionMode.BETWEEN, "instanceTaskId", "java.lang.Long", "Float");
    	  return this;
      }

      public BpmsServiceInstanceTaskSQL andInstanceTaskIdNotBetween(java.lang.Long value1, java.lang.Long value2) {
          addCriterion("instance_task_id", value1, value2, ConditionMode.NOT_BETWEEN, "instanceTaskId", "java.lang.Long", "Float");
          return this;
      }
        
      public BpmsServiceInstanceTaskSQL andInstanceTaskIdIn(List<java.lang.Long> values) {
          addCriterion("instance_task_id", values, ConditionMode.IN, "instanceTaskId", "java.lang.Long", "Float");
          return this;
      }

      public BpmsServiceInstanceTaskSQL andInstanceTaskIdNotIn(List<java.lang.Long> values) {
          addCriterion("instance_task_id", values, ConditionMode.NOT_IN, "instanceTaskId", "java.lang.Long", "Float");
          return this;
      }
	public BpmsServiceInstanceTaskSQL andFlowCodeIsNull() {
		isnull("flow_code");
		return this;
	}
	
	public BpmsServiceInstanceTaskSQL andFlowCodeIsNotNull() {
		notNull("flow_code");
		return this;
	}
	
	public BpmsServiceInstanceTaskSQL andFlowCodeIsEmpty() {
		empty("flow_code");
		return this;
	}

	public BpmsServiceInstanceTaskSQL andFlowCodeIsNotEmpty() {
		notEmpty("flow_code");
		return this;
	}
       public BpmsServiceInstanceTaskSQL andFlowCodeLike(java.lang.String value) {
    	   addCriterion("flow_code", value, ConditionMode.FUZZY, "flowCode", "java.lang.String", "Float");
    	   return this;
      }

      public BpmsServiceInstanceTaskSQL andFlowCodeNotLike(java.lang.String value) {
          addCriterion("flow_code", value, ConditionMode.NOT_FUZZY, "flowCode", "java.lang.String", "Float");
          return this;
      }
      public BpmsServiceInstanceTaskSQL andFlowCodeEqualTo(java.lang.String value) {
          addCriterion("flow_code", value, ConditionMode.EQUAL, "flowCode", "java.lang.String", "String");
          return this;
      }

      public BpmsServiceInstanceTaskSQL andFlowCodeNotEqualTo(java.lang.String value) {
          addCriterion("flow_code", value, ConditionMode.NOT_EQUAL, "flowCode", "java.lang.String", "String");
          return this;
      }

      public BpmsServiceInstanceTaskSQL andFlowCodeGreaterThan(java.lang.String value) {
          addCriterion("flow_code", value, ConditionMode.GREATER_THEN, "flowCode", "java.lang.String", "String");
          return this;
      }

      public BpmsServiceInstanceTaskSQL andFlowCodeGreaterThanOrEqualTo(java.lang.String value) {
          addCriterion("flow_code", value, ConditionMode.GREATER_EQUAL, "flowCode", "java.lang.String", "String");
          return this;
      }

      public BpmsServiceInstanceTaskSQL andFlowCodeLessThan(java.lang.String value) {
          addCriterion("flow_code", value, ConditionMode.LESS_THEN, "flowCode", "java.lang.String", "String");
          return this;
      }

      public BpmsServiceInstanceTaskSQL andFlowCodeLessThanOrEqualTo(java.lang.String value) {
          addCriterion("flow_code", value, ConditionMode.LESS_EQUAL, "flowCode", "java.lang.String", "String");
          return this;
      }

      public BpmsServiceInstanceTaskSQL andFlowCodeBetween(java.lang.String value1, java.lang.String value2) {
    	  addCriterion("flow_code", value1, value2, ConditionMode.BETWEEN, "flowCode", "java.lang.String", "String");
    	  return this;
      }

      public BpmsServiceInstanceTaskSQL andFlowCodeNotBetween(java.lang.String value1, java.lang.String value2) {
          addCriterion("flow_code", value1, value2, ConditionMode.NOT_BETWEEN, "flowCode", "java.lang.String", "String");
          return this;
      }
        
      public BpmsServiceInstanceTaskSQL andFlowCodeIn(List<java.lang.String> values) {
          addCriterion("flow_code", values, ConditionMode.IN, "flowCode", "java.lang.String", "String");
          return this;
      }

      public BpmsServiceInstanceTaskSQL andFlowCodeNotIn(List<java.lang.String> values) {
          addCriterion("flow_code", values, ConditionMode.NOT_IN, "flowCode", "java.lang.String", "String");
          return this;
      }
	public BpmsServiceInstanceTaskSQL andInstanceCodeIsNull() {
		isnull("instance_code");
		return this;
	}
	
	public BpmsServiceInstanceTaskSQL andInstanceCodeIsNotNull() {
		notNull("instance_code");
		return this;
	}
	
	public BpmsServiceInstanceTaskSQL andInstanceCodeIsEmpty() {
		empty("instance_code");
		return this;
	}

	public BpmsServiceInstanceTaskSQL andInstanceCodeIsNotEmpty() {
		notEmpty("instance_code");
		return this;
	}
       public BpmsServiceInstanceTaskSQL andInstanceCodeLike(java.lang.String value) {
    	   addCriterion("instance_code", value, ConditionMode.FUZZY, "instanceCode", "java.lang.String", "String");
    	   return this;
      }

      public BpmsServiceInstanceTaskSQL andInstanceCodeNotLike(java.lang.String value) {
          addCriterion("instance_code", value, ConditionMode.NOT_FUZZY, "instanceCode", "java.lang.String", "String");
          return this;
      }
      public BpmsServiceInstanceTaskSQL andInstanceCodeEqualTo(java.lang.String value) {
          addCriterion("instance_code", value, ConditionMode.EQUAL, "instanceCode", "java.lang.String", "String");
          return this;
      }

      public BpmsServiceInstanceTaskSQL andInstanceCodeNotEqualTo(java.lang.String value) {
          addCriterion("instance_code", value, ConditionMode.NOT_EQUAL, "instanceCode", "java.lang.String", "String");
          return this;
      }

      public BpmsServiceInstanceTaskSQL andInstanceCodeGreaterThan(java.lang.String value) {
          addCriterion("instance_code", value, ConditionMode.GREATER_THEN, "instanceCode", "java.lang.String", "String");
          return this;
      }

      public BpmsServiceInstanceTaskSQL andInstanceCodeGreaterThanOrEqualTo(java.lang.String value) {
          addCriterion("instance_code", value, ConditionMode.GREATER_EQUAL, "instanceCode", "java.lang.String", "String");
          return this;
      }

      public BpmsServiceInstanceTaskSQL andInstanceCodeLessThan(java.lang.String value) {
          addCriterion("instance_code", value, ConditionMode.LESS_THEN, "instanceCode", "java.lang.String", "String");
          return this;
      }

      public BpmsServiceInstanceTaskSQL andInstanceCodeLessThanOrEqualTo(java.lang.String value) {
          addCriterion("instance_code", value, ConditionMode.LESS_EQUAL, "instanceCode", "java.lang.String", "String");
          return this;
      }

      public BpmsServiceInstanceTaskSQL andInstanceCodeBetween(java.lang.String value1, java.lang.String value2) {
    	  addCriterion("instance_code", value1, value2, ConditionMode.BETWEEN, "instanceCode", "java.lang.String", "String");
    	  return this;
      }

      public BpmsServiceInstanceTaskSQL andInstanceCodeNotBetween(java.lang.String value1, java.lang.String value2) {
          addCriterion("instance_code", value1, value2, ConditionMode.NOT_BETWEEN, "instanceCode", "java.lang.String", "String");
          return this;
      }
        
      public BpmsServiceInstanceTaskSQL andInstanceCodeIn(List<java.lang.String> values) {
          addCriterion("instance_code", values, ConditionMode.IN, "instanceCode", "java.lang.String", "String");
          return this;
      }

      public BpmsServiceInstanceTaskSQL andInstanceCodeNotIn(List<java.lang.String> values) {
          addCriterion("instance_code", values, ConditionMode.NOT_IN, "instanceCode", "java.lang.String", "String");
          return this;
      }
	public BpmsServiceInstanceTaskSQL andNodeCodeIsNull() {
		isnull("node_code");
		return this;
	}
	
	public BpmsServiceInstanceTaskSQL andNodeCodeIsNotNull() {
		notNull("node_code");
		return this;
	}
	
	public BpmsServiceInstanceTaskSQL andNodeCodeIsEmpty() {
		empty("node_code");
		return this;
	}

	public BpmsServiceInstanceTaskSQL andNodeCodeIsNotEmpty() {
		notEmpty("node_code");
		return this;
	}
       public BpmsServiceInstanceTaskSQL andNodeCodeLike(java.lang.String value) {
    	   addCriterion("node_code", value, ConditionMode.FUZZY, "nodeCode", "java.lang.String", "String");
    	   return this;
      }

      public BpmsServiceInstanceTaskSQL andNodeCodeNotLike(java.lang.String value) {
          addCriterion("node_code", value, ConditionMode.NOT_FUZZY, "nodeCode", "java.lang.String", "String");
          return this;
      }
      public BpmsServiceInstanceTaskSQL andNodeCodeEqualTo(java.lang.String value) {
          addCriterion("node_code", value, ConditionMode.EQUAL, "nodeCode", "java.lang.String", "String");
          return this;
      }

      public BpmsServiceInstanceTaskSQL andNodeCodeNotEqualTo(java.lang.String value) {
          addCriterion("node_code", value, ConditionMode.NOT_EQUAL, "nodeCode", "java.lang.String", "String");
          return this;
      }

      public BpmsServiceInstanceTaskSQL andNodeCodeGreaterThan(java.lang.String value) {
          addCriterion("node_code", value, ConditionMode.GREATER_THEN, "nodeCode", "java.lang.String", "String");
          return this;
      }

      public BpmsServiceInstanceTaskSQL andNodeCodeGreaterThanOrEqualTo(java.lang.String value) {
          addCriterion("node_code", value, ConditionMode.GREATER_EQUAL, "nodeCode", "java.lang.String", "String");
          return this;
      }

      public BpmsServiceInstanceTaskSQL andNodeCodeLessThan(java.lang.String value) {
          addCriterion("node_code", value, ConditionMode.LESS_THEN, "nodeCode", "java.lang.String", "String");
          return this;
      }

      public BpmsServiceInstanceTaskSQL andNodeCodeLessThanOrEqualTo(java.lang.String value) {
          addCriterion("node_code", value, ConditionMode.LESS_EQUAL, "nodeCode", "java.lang.String", "String");
          return this;
      }

      public BpmsServiceInstanceTaskSQL andNodeCodeBetween(java.lang.String value1, java.lang.String value2) {
    	  addCriterion("node_code", value1, value2, ConditionMode.BETWEEN, "nodeCode", "java.lang.String", "String");
    	  return this;
      }

      public BpmsServiceInstanceTaskSQL andNodeCodeNotBetween(java.lang.String value1, java.lang.String value2) {
          addCriterion("node_code", value1, value2, ConditionMode.NOT_BETWEEN, "nodeCode", "java.lang.String", "String");
          return this;
      }
        
      public BpmsServiceInstanceTaskSQL andNodeCodeIn(List<java.lang.String> values) {
          addCriterion("node_code", values, ConditionMode.IN, "nodeCode", "java.lang.String", "String");
          return this;
      }

      public BpmsServiceInstanceTaskSQL andNodeCodeNotIn(List<java.lang.String> values) {
          addCriterion("node_code", values, ConditionMode.NOT_IN, "nodeCode", "java.lang.String", "String");
          return this;
      }
	public BpmsServiceInstanceTaskSQL andNodeNameIsNull() {
		isnull("node_name");
		return this;
	}
	
	public BpmsServiceInstanceTaskSQL andNodeNameIsNotNull() {
		notNull("node_name");
		return this;
	}
	
	public BpmsServiceInstanceTaskSQL andNodeNameIsEmpty() {
		empty("node_name");
		return this;
	}

	public BpmsServiceInstanceTaskSQL andNodeNameIsNotEmpty() {
		notEmpty("node_name");
		return this;
	}
       public BpmsServiceInstanceTaskSQL andNodeNameLike(java.lang.String value) {
    	   addCriterion("node_name", value, ConditionMode.FUZZY, "nodeName", "java.lang.String", "String");
    	   return this;
      }

      public BpmsServiceInstanceTaskSQL andNodeNameNotLike(java.lang.String value) {
          addCriterion("node_name", value, ConditionMode.NOT_FUZZY, "nodeName", "java.lang.String", "String");
          return this;
      }
      public BpmsServiceInstanceTaskSQL andNodeNameEqualTo(java.lang.String value) {
          addCriterion("node_name", value, ConditionMode.EQUAL, "nodeName", "java.lang.String", "String");
          return this;
      }

      public BpmsServiceInstanceTaskSQL andNodeNameNotEqualTo(java.lang.String value) {
          addCriterion("node_name", value, ConditionMode.NOT_EQUAL, "nodeName", "java.lang.String", "String");
          return this;
      }

      public BpmsServiceInstanceTaskSQL andNodeNameGreaterThan(java.lang.String value) {
          addCriterion("node_name", value, ConditionMode.GREATER_THEN, "nodeName", "java.lang.String", "String");
          return this;
      }

      public BpmsServiceInstanceTaskSQL andNodeNameGreaterThanOrEqualTo(java.lang.String value) {
          addCriterion("node_name", value, ConditionMode.GREATER_EQUAL, "nodeName", "java.lang.String", "String");
          return this;
      }

      public BpmsServiceInstanceTaskSQL andNodeNameLessThan(java.lang.String value) {
          addCriterion("node_name", value, ConditionMode.LESS_THEN, "nodeName", "java.lang.String", "String");
          return this;
      }

      public BpmsServiceInstanceTaskSQL andNodeNameLessThanOrEqualTo(java.lang.String value) {
          addCriterion("node_name", value, ConditionMode.LESS_EQUAL, "nodeName", "java.lang.String", "String");
          return this;
      }

      public BpmsServiceInstanceTaskSQL andNodeNameBetween(java.lang.String value1, java.lang.String value2) {
    	  addCriterion("node_name", value1, value2, ConditionMode.BETWEEN, "nodeName", "java.lang.String", "String");
    	  return this;
      }

      public BpmsServiceInstanceTaskSQL andNodeNameNotBetween(java.lang.String value1, java.lang.String value2) {
          addCriterion("node_name", value1, value2, ConditionMode.NOT_BETWEEN, "nodeName", "java.lang.String", "String");
          return this;
      }
        
      public BpmsServiceInstanceTaskSQL andNodeNameIn(List<java.lang.String> values) {
          addCriterion("node_name", values, ConditionMode.IN, "nodeName", "java.lang.String", "String");
          return this;
      }

      public BpmsServiceInstanceTaskSQL andNodeNameNotIn(List<java.lang.String> values) {
          addCriterion("node_name", values, ConditionMode.NOT_IN, "nodeName", "java.lang.String", "String");
          return this;
      }
	public BpmsServiceInstanceTaskSQL andNodeTypeIsNull() {
		isnull("node_type");
		return this;
	}
	
	public BpmsServiceInstanceTaskSQL andNodeTypeIsNotNull() {
		notNull("node_type");
		return this;
	}
	
	public BpmsServiceInstanceTaskSQL andNodeTypeIsEmpty() {
		empty("node_type");
		return this;
	}

	public BpmsServiceInstanceTaskSQL andNodeTypeIsNotEmpty() {
		notEmpty("node_type");
		return this;
	}
      public BpmsServiceInstanceTaskSQL andNodeTypeEqualTo(java.lang.Integer value) {
          addCriterion("node_type", value, ConditionMode.EQUAL, "nodeType", "java.lang.Integer", "Float");
          return this;
      }

      public BpmsServiceInstanceTaskSQL andNodeTypeNotEqualTo(java.lang.Integer value) {
          addCriterion("node_type", value, ConditionMode.NOT_EQUAL, "nodeType", "java.lang.Integer", "Float");
          return this;
      }

      public BpmsServiceInstanceTaskSQL andNodeTypeGreaterThan(java.lang.Integer value) {
          addCriterion("node_type", value, ConditionMode.GREATER_THEN, "nodeType", "java.lang.Integer", "Float");
          return this;
      }

      public BpmsServiceInstanceTaskSQL andNodeTypeGreaterThanOrEqualTo(java.lang.Integer value) {
          addCriterion("node_type", value, ConditionMode.GREATER_EQUAL, "nodeType", "java.lang.Integer", "Float");
          return this;
      }

      public BpmsServiceInstanceTaskSQL andNodeTypeLessThan(java.lang.Integer value) {
          addCriterion("node_type", value, ConditionMode.LESS_THEN, "nodeType", "java.lang.Integer", "Float");
          return this;
      }

      public BpmsServiceInstanceTaskSQL andNodeTypeLessThanOrEqualTo(java.lang.Integer value) {
          addCriterion("node_type", value, ConditionMode.LESS_EQUAL, "nodeType", "java.lang.Integer", "Float");
          return this;
      }

      public BpmsServiceInstanceTaskSQL andNodeTypeBetween(java.lang.Integer value1, java.lang.Integer value2) {
    	  addCriterion("node_type", value1, value2, ConditionMode.BETWEEN, "nodeType", "java.lang.Integer", "Float");
    	  return this;
      }

      public BpmsServiceInstanceTaskSQL andNodeTypeNotBetween(java.lang.Integer value1, java.lang.Integer value2) {
          addCriterion("node_type", value1, value2, ConditionMode.NOT_BETWEEN, "nodeType", "java.lang.Integer", "Float");
          return this;
      }
        
      public BpmsServiceInstanceTaskSQL andNodeTypeIn(List<java.lang.Integer> values) {
          addCriterion("node_type", values, ConditionMode.IN, "nodeType", "java.lang.Integer", "Float");
          return this;
      }

      public BpmsServiceInstanceTaskSQL andNodeTypeNotIn(List<java.lang.Integer> values) {
          addCriterion("node_type", values, ConditionMode.NOT_IN, "nodeType", "java.lang.Integer", "Float");
          return this;
      }
	public BpmsServiceInstanceTaskSQL andTargetNodeCodeIsNull() {
		isnull("target_node_code");
		return this;
	}
	
	public BpmsServiceInstanceTaskSQL andTargetNodeCodeIsNotNull() {
		notNull("target_node_code");
		return this;
	}
	
	public BpmsServiceInstanceTaskSQL andTargetNodeCodeIsEmpty() {
		empty("target_node_code");
		return this;
	}

	public BpmsServiceInstanceTaskSQL andTargetNodeCodeIsNotEmpty() {
		notEmpty("target_node_code");
		return this;
	}
       public BpmsServiceInstanceTaskSQL andTargetNodeCodeLike(java.lang.String value) {
    	   addCriterion("target_node_code", value, ConditionMode.FUZZY, "targetNodeCode", "java.lang.String", "Float");
    	   return this;
      }

      public BpmsServiceInstanceTaskSQL andTargetNodeCodeNotLike(java.lang.String value) {
          addCriterion("target_node_code", value, ConditionMode.NOT_FUZZY, "targetNodeCode", "java.lang.String", "Float");
          return this;
      }
      public BpmsServiceInstanceTaskSQL andTargetNodeCodeEqualTo(java.lang.String value) {
          addCriterion("target_node_code", value, ConditionMode.EQUAL, "targetNodeCode", "java.lang.String", "String");
          return this;
      }

      public BpmsServiceInstanceTaskSQL andTargetNodeCodeNotEqualTo(java.lang.String value) {
          addCriterion("target_node_code", value, ConditionMode.NOT_EQUAL, "targetNodeCode", "java.lang.String", "String");
          return this;
      }

      public BpmsServiceInstanceTaskSQL andTargetNodeCodeGreaterThan(java.lang.String value) {
          addCriterion("target_node_code", value, ConditionMode.GREATER_THEN, "targetNodeCode", "java.lang.String", "String");
          return this;
      }

      public BpmsServiceInstanceTaskSQL andTargetNodeCodeGreaterThanOrEqualTo(java.lang.String value) {
          addCriterion("target_node_code", value, ConditionMode.GREATER_EQUAL, "targetNodeCode", "java.lang.String", "String");
          return this;
      }

      public BpmsServiceInstanceTaskSQL andTargetNodeCodeLessThan(java.lang.String value) {
          addCriterion("target_node_code", value, ConditionMode.LESS_THEN, "targetNodeCode", "java.lang.String", "String");
          return this;
      }

      public BpmsServiceInstanceTaskSQL andTargetNodeCodeLessThanOrEqualTo(java.lang.String value) {
          addCriterion("target_node_code", value, ConditionMode.LESS_EQUAL, "targetNodeCode", "java.lang.String", "String");
          return this;
      }

      public BpmsServiceInstanceTaskSQL andTargetNodeCodeBetween(java.lang.String value1, java.lang.String value2) {
    	  addCriterion("target_node_code", value1, value2, ConditionMode.BETWEEN, "targetNodeCode", "java.lang.String", "String");
    	  return this;
      }

      public BpmsServiceInstanceTaskSQL andTargetNodeCodeNotBetween(java.lang.String value1, java.lang.String value2) {
          addCriterion("target_node_code", value1, value2, ConditionMode.NOT_BETWEEN, "targetNodeCode", "java.lang.String", "String");
          return this;
      }
        
      public BpmsServiceInstanceTaskSQL andTargetNodeCodeIn(List<java.lang.String> values) {
          addCriterion("target_node_code", values, ConditionMode.IN, "targetNodeCode", "java.lang.String", "String");
          return this;
      }

      public BpmsServiceInstanceTaskSQL andTargetNodeCodeNotIn(List<java.lang.String> values) {
          addCriterion("target_node_code", values, ConditionMode.NOT_IN, "targetNodeCode", "java.lang.String", "String");
          return this;
      }
	public BpmsServiceInstanceTaskSQL andTargetNodeNameIsNull() {
		isnull("target_node_name");
		return this;
	}
	
	public BpmsServiceInstanceTaskSQL andTargetNodeNameIsNotNull() {
		notNull("target_node_name");
		return this;
	}
	
	public BpmsServiceInstanceTaskSQL andTargetNodeNameIsEmpty() {
		empty("target_node_name");
		return this;
	}

	public BpmsServiceInstanceTaskSQL andTargetNodeNameIsNotEmpty() {
		notEmpty("target_node_name");
		return this;
	}
       public BpmsServiceInstanceTaskSQL andTargetNodeNameLike(java.lang.String value) {
    	   addCriterion("target_node_name", value, ConditionMode.FUZZY, "targetNodeName", "java.lang.String", "String");
    	   return this;
      }

      public BpmsServiceInstanceTaskSQL andTargetNodeNameNotLike(java.lang.String value) {
          addCriterion("target_node_name", value, ConditionMode.NOT_FUZZY, "targetNodeName", "java.lang.String", "String");
          return this;
      }
      public BpmsServiceInstanceTaskSQL andTargetNodeNameEqualTo(java.lang.String value) {
          addCriterion("target_node_name", value, ConditionMode.EQUAL, "targetNodeName", "java.lang.String", "String");
          return this;
      }

      public BpmsServiceInstanceTaskSQL andTargetNodeNameNotEqualTo(java.lang.String value) {
          addCriterion("target_node_name", value, ConditionMode.NOT_EQUAL, "targetNodeName", "java.lang.String", "String");
          return this;
      }

      public BpmsServiceInstanceTaskSQL andTargetNodeNameGreaterThan(java.lang.String value) {
          addCriterion("target_node_name", value, ConditionMode.GREATER_THEN, "targetNodeName", "java.lang.String", "String");
          return this;
      }

      public BpmsServiceInstanceTaskSQL andTargetNodeNameGreaterThanOrEqualTo(java.lang.String value) {
          addCriterion("target_node_name", value, ConditionMode.GREATER_EQUAL, "targetNodeName", "java.lang.String", "String");
          return this;
      }

      public BpmsServiceInstanceTaskSQL andTargetNodeNameLessThan(java.lang.String value) {
          addCriterion("target_node_name", value, ConditionMode.LESS_THEN, "targetNodeName", "java.lang.String", "String");
          return this;
      }

      public BpmsServiceInstanceTaskSQL andTargetNodeNameLessThanOrEqualTo(java.lang.String value) {
          addCriterion("target_node_name", value, ConditionMode.LESS_EQUAL, "targetNodeName", "java.lang.String", "String");
          return this;
      }

      public BpmsServiceInstanceTaskSQL andTargetNodeNameBetween(java.lang.String value1, java.lang.String value2) {
    	  addCriterion("target_node_name", value1, value2, ConditionMode.BETWEEN, "targetNodeName", "java.lang.String", "String");
    	  return this;
      }

      public BpmsServiceInstanceTaskSQL andTargetNodeNameNotBetween(java.lang.String value1, java.lang.String value2) {
          addCriterion("target_node_name", value1, value2, ConditionMode.NOT_BETWEEN, "targetNodeName", "java.lang.String", "String");
          return this;
      }
        
      public BpmsServiceInstanceTaskSQL andTargetNodeNameIn(List<java.lang.String> values) {
          addCriterion("target_node_name", values, ConditionMode.IN, "targetNodeName", "java.lang.String", "String");
          return this;
      }

      public BpmsServiceInstanceTaskSQL andTargetNodeNameNotIn(List<java.lang.String> values) {
          addCriterion("target_node_name", values, ConditionMode.NOT_IN, "targetNodeName", "java.lang.String", "String");
          return this;
      }
	public BpmsServiceInstanceTaskSQL andApproverIsNull() {
		isnull("approver");
		return this;
	}
	
	public BpmsServiceInstanceTaskSQL andApproverIsNotNull() {
		notNull("approver");
		return this;
	}
	
	public BpmsServiceInstanceTaskSQL andApproverIsEmpty() {
		empty("approver");
		return this;
	}

	public BpmsServiceInstanceTaskSQL andApproverIsNotEmpty() {
		notEmpty("approver");
		return this;
	}
       public BpmsServiceInstanceTaskSQL andApproverLike(java.lang.String value) {
    	   addCriterion("approver", value, ConditionMode.FUZZY, "approver", "java.lang.String", "String");
    	   return this;
      }

      public BpmsServiceInstanceTaskSQL andApproverNotLike(java.lang.String value) {
          addCriterion("approver", value, ConditionMode.NOT_FUZZY, "approver", "java.lang.String", "String");
          return this;
      }
      public BpmsServiceInstanceTaskSQL andApproverEqualTo(java.lang.String value) {
          addCriterion("approver", value, ConditionMode.EQUAL, "approver", "java.lang.String", "String");
          return this;
      }

      public BpmsServiceInstanceTaskSQL andApproverNotEqualTo(java.lang.String value) {
          addCriterion("approver", value, ConditionMode.NOT_EQUAL, "approver", "java.lang.String", "String");
          return this;
      }

      public BpmsServiceInstanceTaskSQL andApproverGreaterThan(java.lang.String value) {
          addCriterion("approver", value, ConditionMode.GREATER_THEN, "approver", "java.lang.String", "String");
          return this;
      }

      public BpmsServiceInstanceTaskSQL andApproverGreaterThanOrEqualTo(java.lang.String value) {
          addCriterion("approver", value, ConditionMode.GREATER_EQUAL, "approver", "java.lang.String", "String");
          return this;
      }

      public BpmsServiceInstanceTaskSQL andApproverLessThan(java.lang.String value) {
          addCriterion("approver", value, ConditionMode.LESS_THEN, "approver", "java.lang.String", "String");
          return this;
      }

      public BpmsServiceInstanceTaskSQL andApproverLessThanOrEqualTo(java.lang.String value) {
          addCriterion("approver", value, ConditionMode.LESS_EQUAL, "approver", "java.lang.String", "String");
          return this;
      }

      public BpmsServiceInstanceTaskSQL andApproverBetween(java.lang.String value1, java.lang.String value2) {
    	  addCriterion("approver", value1, value2, ConditionMode.BETWEEN, "approver", "java.lang.String", "String");
    	  return this;
      }

      public BpmsServiceInstanceTaskSQL andApproverNotBetween(java.lang.String value1, java.lang.String value2) {
          addCriterion("approver", value1, value2, ConditionMode.NOT_BETWEEN, "approver", "java.lang.String", "String");
          return this;
      }
        
      public BpmsServiceInstanceTaskSQL andApproverIn(List<java.lang.String> values) {
          addCriterion("approver", values, ConditionMode.IN, "approver", "java.lang.String", "String");
          return this;
      }

      public BpmsServiceInstanceTaskSQL andApproverNotIn(List<java.lang.String> values) {
          addCriterion("approver", values, ConditionMode.NOT_IN, "approver", "java.lang.String", "String");
          return this;
      }
	public BpmsServiceInstanceTaskSQL andPermissionFlagIsNull() {
		isnull("permission_flag");
		return this;
	}
	
	public BpmsServiceInstanceTaskSQL andPermissionFlagIsNotNull() {
		notNull("permission_flag");
		return this;
	}
	
	public BpmsServiceInstanceTaskSQL andPermissionFlagIsEmpty() {
		empty("permission_flag");
		return this;
	}

	public BpmsServiceInstanceTaskSQL andPermissionFlagIsNotEmpty() {
		notEmpty("permission_flag");
		return this;
	}
       public BpmsServiceInstanceTaskSQL andPermissionFlagLike(java.lang.String value) {
    	   addCriterion("permission_flag", value, ConditionMode.FUZZY, "permissionFlag", "java.lang.String", "String");
    	   return this;
      }

      public BpmsServiceInstanceTaskSQL andPermissionFlagNotLike(java.lang.String value) {
          addCriterion("permission_flag", value, ConditionMode.NOT_FUZZY, "permissionFlag", "java.lang.String", "String");
          return this;
      }
      public BpmsServiceInstanceTaskSQL andPermissionFlagEqualTo(java.lang.String value) {
          addCriterion("permission_flag", value, ConditionMode.EQUAL, "permissionFlag", "java.lang.String", "String");
          return this;
      }

      public BpmsServiceInstanceTaskSQL andPermissionFlagNotEqualTo(java.lang.String value) {
          addCriterion("permission_flag", value, ConditionMode.NOT_EQUAL, "permissionFlag", "java.lang.String", "String");
          return this;
      }

      public BpmsServiceInstanceTaskSQL andPermissionFlagGreaterThan(java.lang.String value) {
          addCriterion("permission_flag", value, ConditionMode.GREATER_THEN, "permissionFlag", "java.lang.String", "String");
          return this;
      }

      public BpmsServiceInstanceTaskSQL andPermissionFlagGreaterThanOrEqualTo(java.lang.String value) {
          addCriterion("permission_flag", value, ConditionMode.GREATER_EQUAL, "permissionFlag", "java.lang.String", "String");
          return this;
      }

      public BpmsServiceInstanceTaskSQL andPermissionFlagLessThan(java.lang.String value) {
          addCriterion("permission_flag", value, ConditionMode.LESS_THEN, "permissionFlag", "java.lang.String", "String");
          return this;
      }

      public BpmsServiceInstanceTaskSQL andPermissionFlagLessThanOrEqualTo(java.lang.String value) {
          addCriterion("permission_flag", value, ConditionMode.LESS_EQUAL, "permissionFlag", "java.lang.String", "String");
          return this;
      }

      public BpmsServiceInstanceTaskSQL andPermissionFlagBetween(java.lang.String value1, java.lang.String value2) {
    	  addCriterion("permission_flag", value1, value2, ConditionMode.BETWEEN, "permissionFlag", "java.lang.String", "String");
    	  return this;
      }

      public BpmsServiceInstanceTaskSQL andPermissionFlagNotBetween(java.lang.String value1, java.lang.String value2) {
          addCriterion("permission_flag", value1, value2, ConditionMode.NOT_BETWEEN, "permissionFlag", "java.lang.String", "String");
          return this;
      }
        
      public BpmsServiceInstanceTaskSQL andPermissionFlagIn(List<java.lang.String> values) {
          addCriterion("permission_flag", values, ConditionMode.IN, "permissionFlag", "java.lang.String", "String");
          return this;
      }

      public BpmsServiceInstanceTaskSQL andPermissionFlagNotIn(List<java.lang.String> values) {
          addCriterion("permission_flag", values, ConditionMode.NOT_IN, "permissionFlag", "java.lang.String", "String");
          return this;
      }
	public BpmsServiceInstanceTaskSQL andFlowStatusIsNull() {
		isnull("flow_status");
		return this;
	}
	
	public BpmsServiceInstanceTaskSQL andFlowStatusIsNotNull() {
		notNull("flow_status");
		return this;
	}
	
	public BpmsServiceInstanceTaskSQL andFlowStatusIsEmpty() {
		empty("flow_status");
		return this;
	}

	public BpmsServiceInstanceTaskSQL andFlowStatusIsNotEmpty() {
		notEmpty("flow_status");
		return this;
	}
      public BpmsServiceInstanceTaskSQL andFlowStatusEqualTo(java.lang.Integer value) {
          addCriterion("flow_status", value, ConditionMode.EQUAL, "flowStatus", "java.lang.Integer", "Float");
          return this;
      }

      public BpmsServiceInstanceTaskSQL andFlowStatusNotEqualTo(java.lang.Integer value) {
          addCriterion("flow_status", value, ConditionMode.NOT_EQUAL, "flowStatus", "java.lang.Integer", "Float");
          return this;
      }

      public BpmsServiceInstanceTaskSQL andFlowStatusGreaterThan(java.lang.Integer value) {
          addCriterion("flow_status", value, ConditionMode.GREATER_THEN, "flowStatus", "java.lang.Integer", "Float");
          return this;
      }

      public BpmsServiceInstanceTaskSQL andFlowStatusGreaterThanOrEqualTo(java.lang.Integer value) {
          addCriterion("flow_status", value, ConditionMode.GREATER_EQUAL, "flowStatus", "java.lang.Integer", "Float");
          return this;
      }

      public BpmsServiceInstanceTaskSQL andFlowStatusLessThan(java.lang.Integer value) {
          addCriterion("flow_status", value, ConditionMode.LESS_THEN, "flowStatus", "java.lang.Integer", "Float");
          return this;
      }

      public BpmsServiceInstanceTaskSQL andFlowStatusLessThanOrEqualTo(java.lang.Integer value) {
          addCriterion("flow_status", value, ConditionMode.LESS_EQUAL, "flowStatus", "java.lang.Integer", "Float");
          return this;
      }

      public BpmsServiceInstanceTaskSQL andFlowStatusBetween(java.lang.Integer value1, java.lang.Integer value2) {
    	  addCriterion("flow_status", value1, value2, ConditionMode.BETWEEN, "flowStatus", "java.lang.Integer", "Float");
    	  return this;
      }

      public BpmsServiceInstanceTaskSQL andFlowStatusNotBetween(java.lang.Integer value1, java.lang.Integer value2) {
          addCriterion("flow_status", value1, value2, ConditionMode.NOT_BETWEEN, "flowStatus", "java.lang.Integer", "Float");
          return this;
      }
        
      public BpmsServiceInstanceTaskSQL andFlowStatusIn(List<java.lang.Integer> values) {
          addCriterion("flow_status", values, ConditionMode.IN, "flowStatus", "java.lang.Integer", "Float");
          return this;
      }

      public BpmsServiceInstanceTaskSQL andFlowStatusNotIn(List<java.lang.Integer> values) {
          addCriterion("flow_status", values, ConditionMode.NOT_IN, "flowStatus", "java.lang.Integer", "Float");
          return this;
      }
	public BpmsServiceInstanceTaskSQL andMessageIsNull() {
		isnull("message");
		return this;
	}
	
	public BpmsServiceInstanceTaskSQL andMessageIsNotNull() {
		notNull("message");
		return this;
	}
	
	public BpmsServiceInstanceTaskSQL andMessageIsEmpty() {
		empty("message");
		return this;
	}

	public BpmsServiceInstanceTaskSQL andMessageIsNotEmpty() {
		notEmpty("message");
		return this;
	}
       public BpmsServiceInstanceTaskSQL andMessageLike(java.lang.String value) {
    	   addCriterion("message", value, ConditionMode.FUZZY, "message", "java.lang.String", "Float");
    	   return this;
      }

      public BpmsServiceInstanceTaskSQL andMessageNotLike(java.lang.String value) {
          addCriterion("message", value, ConditionMode.NOT_FUZZY, "message", "java.lang.String", "Float");
          return this;
      }
      public BpmsServiceInstanceTaskSQL andMessageEqualTo(java.lang.String value) {
          addCriterion("message", value, ConditionMode.EQUAL, "message", "java.lang.String", "String");
          return this;
      }

      public BpmsServiceInstanceTaskSQL andMessageNotEqualTo(java.lang.String value) {
          addCriterion("message", value, ConditionMode.NOT_EQUAL, "message", "java.lang.String", "String");
          return this;
      }

      public BpmsServiceInstanceTaskSQL andMessageGreaterThan(java.lang.String value) {
          addCriterion("message", value, ConditionMode.GREATER_THEN, "message", "java.lang.String", "String");
          return this;
      }

      public BpmsServiceInstanceTaskSQL andMessageGreaterThanOrEqualTo(java.lang.String value) {
          addCriterion("message", value, ConditionMode.GREATER_EQUAL, "message", "java.lang.String", "String");
          return this;
      }

      public BpmsServiceInstanceTaskSQL andMessageLessThan(java.lang.String value) {
          addCriterion("message", value, ConditionMode.LESS_THEN, "message", "java.lang.String", "String");
          return this;
      }

      public BpmsServiceInstanceTaskSQL andMessageLessThanOrEqualTo(java.lang.String value) {
          addCriterion("message", value, ConditionMode.LESS_EQUAL, "message", "java.lang.String", "String");
          return this;
      }

      public BpmsServiceInstanceTaskSQL andMessageBetween(java.lang.String value1, java.lang.String value2) {
    	  addCriterion("message", value1, value2, ConditionMode.BETWEEN, "message", "java.lang.String", "String");
    	  return this;
      }

      public BpmsServiceInstanceTaskSQL andMessageNotBetween(java.lang.String value1, java.lang.String value2) {
          addCriterion("message", value1, value2, ConditionMode.NOT_BETWEEN, "message", "java.lang.String", "String");
          return this;
      }
        
      public BpmsServiceInstanceTaskSQL andMessageIn(List<java.lang.String> values) {
          addCriterion("message", values, ConditionMode.IN, "message", "java.lang.String", "String");
          return this;
      }

      public BpmsServiceInstanceTaskSQL andMessageNotIn(List<java.lang.String> values) {
          addCriterion("message", values, ConditionMode.NOT_IN, "message", "java.lang.String", "String");
          return this;
      }
	public BpmsServiceInstanceTaskSQL andCreateTimeIsNull() {
		isnull("create_time");
		return this;
	}
	
	public BpmsServiceInstanceTaskSQL andCreateTimeIsNotNull() {
		notNull("create_time");
		return this;
	}
	
	public BpmsServiceInstanceTaskSQL andCreateTimeIsEmpty() {
		empty("create_time");
		return this;
	}

	public BpmsServiceInstanceTaskSQL andCreateTimeIsNotEmpty() {
		notEmpty("create_time");
		return this;
	}
      public BpmsServiceInstanceTaskSQL andCreateTimeEqualTo(java.util.Date value) {
          addCriterion("create_time", value, ConditionMode.EQUAL, "createTime", "java.util.Date", "String");
          return this;
      }

      public BpmsServiceInstanceTaskSQL andCreateTimeNotEqualTo(java.util.Date value) {
          addCriterion("create_time", value, ConditionMode.NOT_EQUAL, "createTime", "java.util.Date", "String");
          return this;
      }

      public BpmsServiceInstanceTaskSQL andCreateTimeGreaterThan(java.util.Date value) {
          addCriterion("create_time", value, ConditionMode.GREATER_THEN, "createTime", "java.util.Date", "String");
          return this;
      }

      public BpmsServiceInstanceTaskSQL andCreateTimeGreaterThanOrEqualTo(java.util.Date value) {
          addCriterion("create_time", value, ConditionMode.GREATER_EQUAL, "createTime", "java.util.Date", "String");
          return this;
      }

      public BpmsServiceInstanceTaskSQL andCreateTimeLessThan(java.util.Date value) {
          addCriterion("create_time", value, ConditionMode.LESS_THEN, "createTime", "java.util.Date", "String");
          return this;
      }

      public BpmsServiceInstanceTaskSQL andCreateTimeLessThanOrEqualTo(java.util.Date value) {
          addCriterion("create_time", value, ConditionMode.LESS_EQUAL, "createTime", "java.util.Date", "String");
          return this;
      }

      public BpmsServiceInstanceTaskSQL andCreateTimeBetween(java.util.Date value1, java.util.Date value2) {
    	  addCriterion("create_time", value1, value2, ConditionMode.BETWEEN, "createTime", "java.util.Date", "String");
    	  return this;
      }

      public BpmsServiceInstanceTaskSQL andCreateTimeNotBetween(java.util.Date value1, java.util.Date value2) {
          addCriterion("create_time", value1, value2, ConditionMode.NOT_BETWEEN, "createTime", "java.util.Date", "String");
          return this;
      }
        
      public BpmsServiceInstanceTaskSQL andCreateTimeIn(List<java.util.Date> values) {
          addCriterion("create_time", values, ConditionMode.IN, "createTime", "java.util.Date", "String");
          return this;
      }

      public BpmsServiceInstanceTaskSQL andCreateTimeNotIn(List<java.util.Date> values) {
          addCriterion("create_time", values, ConditionMode.NOT_IN, "createTime", "java.util.Date", "String");
          return this;
      }
	public BpmsServiceInstanceTaskSQL andUpdateTimeIsNull() {
		isnull("update_time");
		return this;
	}
	
	public BpmsServiceInstanceTaskSQL andUpdateTimeIsNotNull() {
		notNull("update_time");
		return this;
	}
	
	public BpmsServiceInstanceTaskSQL andUpdateTimeIsEmpty() {
		empty("update_time");
		return this;
	}

	public BpmsServiceInstanceTaskSQL andUpdateTimeIsNotEmpty() {
		notEmpty("update_time");
		return this;
	}
      public BpmsServiceInstanceTaskSQL andUpdateTimeEqualTo(java.util.Date value) {
          addCriterion("update_time", value, ConditionMode.EQUAL, "updateTime", "java.util.Date", "String");
          return this;
      }

      public BpmsServiceInstanceTaskSQL andUpdateTimeNotEqualTo(java.util.Date value) {
          addCriterion("update_time", value, ConditionMode.NOT_EQUAL, "updateTime", "java.util.Date", "String");
          return this;
      }

      public BpmsServiceInstanceTaskSQL andUpdateTimeGreaterThan(java.util.Date value) {
          addCriterion("update_time", value, ConditionMode.GREATER_THEN, "updateTime", "java.util.Date", "String");
          return this;
      }

      public BpmsServiceInstanceTaskSQL andUpdateTimeGreaterThanOrEqualTo(java.util.Date value) {
          addCriterion("update_time", value, ConditionMode.GREATER_EQUAL, "updateTime", "java.util.Date", "String");
          return this;
      }

      public BpmsServiceInstanceTaskSQL andUpdateTimeLessThan(java.util.Date value) {
          addCriterion("update_time", value, ConditionMode.LESS_THEN, "updateTime", "java.util.Date", "String");
          return this;
      }

      public BpmsServiceInstanceTaskSQL andUpdateTimeLessThanOrEqualTo(java.util.Date value) {
          addCriterion("update_time", value, ConditionMode.LESS_EQUAL, "updateTime", "java.util.Date", "String");
          return this;
      }

      public BpmsServiceInstanceTaskSQL andUpdateTimeBetween(java.util.Date value1, java.util.Date value2) {
    	  addCriterion("update_time", value1, value2, ConditionMode.BETWEEN, "updateTime", "java.util.Date", "String");
    	  return this;
      }

      public BpmsServiceInstanceTaskSQL andUpdateTimeNotBetween(java.util.Date value1, java.util.Date value2) {
          addCriterion("update_time", value1, value2, ConditionMode.NOT_BETWEEN, "updateTime", "java.util.Date", "String");
          return this;
      }
        
      public BpmsServiceInstanceTaskSQL andUpdateTimeIn(List<java.util.Date> values) {
          addCriterion("update_time", values, ConditionMode.IN, "updateTime", "java.util.Date", "String");
          return this;
      }

      public BpmsServiceInstanceTaskSQL andUpdateTimeNotIn(List<java.util.Date> values) {
          addCriterion("update_time", values, ConditionMode.NOT_IN, "updateTime", "java.util.Date", "String");
          return this;
      }
	public BpmsServiceInstanceTaskSQL andDelFlagIsNull() {
		isnull("del_flag");
		return this;
	}
	
	public BpmsServiceInstanceTaskSQL andDelFlagIsNotNull() {
		notNull("del_flag");
		return this;
	}
	
	public BpmsServiceInstanceTaskSQL andDelFlagIsEmpty() {
		empty("del_flag");
		return this;
	}

	public BpmsServiceInstanceTaskSQL andDelFlagIsNotEmpty() {
		notEmpty("del_flag");
		return this;
	}
       public BpmsServiceInstanceTaskSQL andDelFlagLike(java.lang.String value) {
    	   addCriterion("del_flag", value, ConditionMode.FUZZY, "delFlag", "java.lang.String", "String");
    	   return this;
      }

      public BpmsServiceInstanceTaskSQL andDelFlagNotLike(java.lang.String value) {
          addCriterion("del_flag", value, ConditionMode.NOT_FUZZY, "delFlag", "java.lang.String", "String");
          return this;
      }
      public BpmsServiceInstanceTaskSQL andDelFlagEqualTo(java.lang.String value) {
          addCriterion("del_flag", value, ConditionMode.EQUAL, "delFlag", "java.lang.String", "String");
          return this;
      }

      public BpmsServiceInstanceTaskSQL andDelFlagNotEqualTo(java.lang.String value) {
          addCriterion("del_flag", value, ConditionMode.NOT_EQUAL, "delFlag", "java.lang.String", "String");
          return this;
      }

      public BpmsServiceInstanceTaskSQL andDelFlagGreaterThan(java.lang.String value) {
          addCriterion("del_flag", value, ConditionMode.GREATER_THEN, "delFlag", "java.lang.String", "String");
          return this;
      }

      public BpmsServiceInstanceTaskSQL andDelFlagGreaterThanOrEqualTo(java.lang.String value) {
          addCriterion("del_flag", value, ConditionMode.GREATER_EQUAL, "delFlag", "java.lang.String", "String");
          return this;
      }

      public BpmsServiceInstanceTaskSQL andDelFlagLessThan(java.lang.String value) {
          addCriterion("del_flag", value, ConditionMode.LESS_THEN, "delFlag", "java.lang.String", "String");
          return this;
      }

      public BpmsServiceInstanceTaskSQL andDelFlagLessThanOrEqualTo(java.lang.String value) {
          addCriterion("del_flag", value, ConditionMode.LESS_EQUAL, "delFlag", "java.lang.String", "String");
          return this;
      }

      public BpmsServiceInstanceTaskSQL andDelFlagBetween(java.lang.String value1, java.lang.String value2) {
    	  addCriterion("del_flag", value1, value2, ConditionMode.BETWEEN, "delFlag", "java.lang.String", "String");
    	  return this;
      }

      public BpmsServiceInstanceTaskSQL andDelFlagNotBetween(java.lang.String value1, java.lang.String value2) {
          addCriterion("del_flag", value1, value2, ConditionMode.NOT_BETWEEN, "delFlag", "java.lang.String", "String");
          return this;
      }
        
      public BpmsServiceInstanceTaskSQL andDelFlagIn(List<java.lang.String> values) {
          addCriterion("del_flag", values, ConditionMode.IN, "delFlag", "java.lang.String", "String");
          return this;
      }

      public BpmsServiceInstanceTaskSQL andDelFlagNotIn(List<java.lang.String> values) {
          addCriterion("del_flag", values, ConditionMode.NOT_IN, "delFlag", "java.lang.String", "String");
          return this;
      }
	public BpmsServiceInstanceTaskSQL andUserCodeIsNull() {
		isnull("user_code");
		return this;
	}
	
	public BpmsServiceInstanceTaskSQL andUserCodeIsNotNull() {
		notNull("user_code");
		return this;
	}
	
	public BpmsServiceInstanceTaskSQL andUserCodeIsEmpty() {
		empty("user_code");
		return this;
	}

	public BpmsServiceInstanceTaskSQL andUserCodeIsNotEmpty() {
		notEmpty("user_code");
		return this;
	}
       public BpmsServiceInstanceTaskSQL andUserCodeLike(java.lang.String value) {
    	   addCriterion("user_code", value, ConditionMode.FUZZY, "userCode", "java.lang.String", "String");
    	   return this;
      }

      public BpmsServiceInstanceTaskSQL andUserCodeNotLike(java.lang.String value) {
          addCriterion("user_code", value, ConditionMode.NOT_FUZZY, "userCode", "java.lang.String", "String");
          return this;
      }
      public BpmsServiceInstanceTaskSQL andUserCodeEqualTo(java.lang.String value) {
          addCriterion("user_code", value, ConditionMode.EQUAL, "userCode", "java.lang.String", "String");
          return this;
      }

      public BpmsServiceInstanceTaskSQL andUserCodeNotEqualTo(java.lang.String value) {
          addCriterion("user_code", value, ConditionMode.NOT_EQUAL, "userCode", "java.lang.String", "String");
          return this;
      }

      public BpmsServiceInstanceTaskSQL andUserCodeGreaterThan(java.lang.String value) {
          addCriterion("user_code", value, ConditionMode.GREATER_THEN, "userCode", "java.lang.String", "String");
          return this;
      }

      public BpmsServiceInstanceTaskSQL andUserCodeGreaterThanOrEqualTo(java.lang.String value) {
          addCriterion("user_code", value, ConditionMode.GREATER_EQUAL, "userCode", "java.lang.String", "String");
          return this;
      }

      public BpmsServiceInstanceTaskSQL andUserCodeLessThan(java.lang.String value) {
          addCriterion("user_code", value, ConditionMode.LESS_THEN, "userCode", "java.lang.String", "String");
          return this;
      }

      public BpmsServiceInstanceTaskSQL andUserCodeLessThanOrEqualTo(java.lang.String value) {
          addCriterion("user_code", value, ConditionMode.LESS_EQUAL, "userCode", "java.lang.String", "String");
          return this;
      }

      public BpmsServiceInstanceTaskSQL andUserCodeBetween(java.lang.String value1, java.lang.String value2) {
    	  addCriterion("user_code", value1, value2, ConditionMode.BETWEEN, "userCode", "java.lang.String", "String");
    	  return this;
      }

      public BpmsServiceInstanceTaskSQL andUserCodeNotBetween(java.lang.String value1, java.lang.String value2) {
          addCriterion("user_code", value1, value2, ConditionMode.NOT_BETWEEN, "userCode", "java.lang.String", "String");
          return this;
      }
        
      public BpmsServiceInstanceTaskSQL andUserCodeIn(List<java.lang.String> values) {
          addCriterion("user_code", values, ConditionMode.IN, "userCode", "java.lang.String", "String");
          return this;
      }

      public BpmsServiceInstanceTaskSQL andUserCodeNotIn(List<java.lang.String> values) {
          addCriterion("user_code", values, ConditionMode.NOT_IN, "userCode", "java.lang.String", "String");
          return this;
      }
}