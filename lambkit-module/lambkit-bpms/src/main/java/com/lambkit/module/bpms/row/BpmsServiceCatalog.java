/**
 * Copyright (c) 2015-2017, Henry Yang 杨勇 (gismail@foxmail.com).
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.lambkit.module.bpms.row;

import com.lambkit.db.RowModel;

/**
 * @author yangyong 
 * @website: www.lambkit.com
 * @email: gismail@foxmail.com
 * @date ${date}
 * @version 1.0
 * @since 1.0
 */

public class BpmsServiceCatalog extends RowModel<BpmsServiceCatalog> {
	public BpmsServiceCatalog() {
		setTableName("bpms_service_catalog");
		setPrimaryKey("catalog_id");
	}
	public java.lang.Long getCatalogId() {
		return this.get("catalog_id");
	}
	public void setCatalogId(java.lang.Long catalogId) {
		this.set("catalog_id", catalogId);
	}
	public java.lang.String getCatalogCode() {
		return this.get("catalog_code");
	}
	public void setCatalogCode(java.lang.String catalogCode) {
		this.set("catalog_code", catalogCode);
	}
	public java.lang.String getCatalogName() {
		return this.get("catalog_name");
	}
	public void setCatalogName(java.lang.String catalogName) {
		this.set("catalog_name", catalogName);
	}
	public java.util.Date getCreateTime() {
		return this.get("create_time");
	}
	public void setCreateTime(java.util.Date createTime) {
		this.set("create_time", createTime);
	}
	public java.util.Date getUpdateTime() {
		return this.get("update_time");
	}
	public void setUpdateTime(java.util.Date updateTime) {
		this.set("update_time", updateTime);
	}
	public java.lang.String getDelFlag() {
		return this.get("del_flag");
	}
	public void setDelFlag(java.lang.String delFlag) {
		this.set("del_flag", delFlag);
	}
	public java.lang.String getUserCode() {
		return this.get("user_code");
	}
	public void setUserCode(java.lang.String userCode) {
		this.set("user_code", userCode);
	}
}
