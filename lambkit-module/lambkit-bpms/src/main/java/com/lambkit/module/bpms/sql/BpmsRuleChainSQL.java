/**
 * Copyright (c) 2015-2017, Henry Yang 杨勇 (gismail@foxmail.com).
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.lambkit.module.bpms.sql;

import com.lambkit.db.sql.ConditionMode;
import com.lambkit.db.sql.Column;
import com.lambkit.db.sql.Columns;
import com.lambkit.db.sql.Example;

import java.io.Serializable;
import java.util.List;

/**
 * @author yangyong 
 * @website: www.lambkit.com
 * @email: gismail@foxmail.com
 * @date ${date}
 * @version 1.0
 * @since 1.0
 */

public class BpmsRuleChainSQL extends Columns implements Serializable {
	private static final long serialVersionUID = 1L;
	
	public static BpmsRuleChainSQL of() {
		return new BpmsRuleChainSQL();
	}
	
	public static BpmsRuleChainSQL by(Column column) {
		BpmsRuleChainSQL that = new BpmsRuleChainSQL();
		that.add(column);
        return that;
    }

    public static BpmsRuleChainSQL by(String name, Object value) {
        return (BpmsRuleChainSQL) create().eq(name, value);
    }
    
    public Example example() {
    	return Example.create("bpms_rule_chain", this);
    }
    
    /**
     * equals
     *
     * @param name
     * @param value
     * @return
     */
    public BpmsRuleChainSQL eq(String name, Object value) {
    	super.eq(name, value);
        return this;
    }

    /**
     * not equals !=
     *
     * @param name
     * @param value
     * @return
     */
    public BpmsRuleChainSQL ne(String name, Object value) {
    	super.ne(name, value);
        return this;
    }


    /**
     * like
     *
     * @param name
     * @param value
     * @return
     */

    public BpmsRuleChainSQL like(String name, Object value) {
    	super.like(name, value);
        return this;
    }
    
    public BpmsRuleChainSQL notLike(String name, Object value) {
    	super.notLike(name, value);
        return this;
    }

    /**
     * 大于 great than
     *
     * @param name
     * @param value
     * @return
     */
    public BpmsRuleChainSQL gt(String name, Object value) {
    	super.gt(name, value);
        return this;
    }

    /**
     * 大于等于 great or equal
     *
     * @param name
     * @param value
     * @return
     */
    public BpmsRuleChainSQL ge(String name, Object value) {
    	super.ge(name, value);
        return this;
    }

    /**
     * 小于 less than
     *
     * @param name
     * @param value
     * @return
     */
    public BpmsRuleChainSQL lt(String name, Object value) {
    	super.lt(name, value);
        return this;
    }

    /**
     * 小于等于 less or equal
     *
     * @param name
     * @param value
     * @return
     */
    public BpmsRuleChainSQL le(String name, Object value) {
    	super.le(name, value);
        return this;
    }
    
    public BpmsRuleChainSQL isnull(String name) {
    	super.isnull(name);
        return this;
    } 

    public BpmsRuleChainSQL notNull(String name) {
    	super.notNull(name);
        return this;
    } 
    
    public BpmsRuleChainSQL empty(String name) {
    	super.empty(name);
        return this;
    } 
    
    public BpmsRuleChainSQL notEmpty(String name) {
    	super.notEmpty(name);
        return this;
    } 
    
    public BpmsRuleChainSQL add(Column column) {
    	super.add(column);
    	return this;
    }
    
    /**************************/
	
	public void addCriterion(String name, Object value, ConditionMode logic, String property, String typeHandler, String valueType) {
		 if (value == null) {
			 throw new RuntimeException("Value for " + property + " cannot be null");
		 }
		 add(Column.create(name, value, logic, typeHandler, valueType));
	}
   
	public void addCriterion(String name, Object value1, Object value2, ConditionMode logic, String property, String typeHandler, String valueType) {
		 if (value1 == null || value2 == null) {
			 throw new RuntimeException("Between values for " + property + " cannot be null");
		 }
		 add(Column.create(name, value1, value2, logic, typeHandler, valueType));
	}
		 
	public BpmsRuleChainSQL andChainIdIsNull() {
		isnull("chain_id");
		return this;
	}
	
	public BpmsRuleChainSQL andChainIdIsNotNull() {
		notNull("chain_id");
		return this;
	}
	
	public BpmsRuleChainSQL andChainIdIsEmpty() {
		empty("chain_id");
		return this;
	}

	public BpmsRuleChainSQL andChainIdIsNotEmpty() {
		notEmpty("chain_id");
		return this;
	}
      public BpmsRuleChainSQL andChainIdEqualTo(java.lang.Long value) {
          addCriterion("chain_id", value, ConditionMode.EQUAL, "chainId", "java.lang.Long", "Float");
          return this;
      }

      public BpmsRuleChainSQL andChainIdNotEqualTo(java.lang.Long value) {
          addCriterion("chain_id", value, ConditionMode.NOT_EQUAL, "chainId", "java.lang.Long", "Float");
          return this;
      }

      public BpmsRuleChainSQL andChainIdGreaterThan(java.lang.Long value) {
          addCriterion("chain_id", value, ConditionMode.GREATER_THEN, "chainId", "java.lang.Long", "Float");
          return this;
      }

      public BpmsRuleChainSQL andChainIdGreaterThanOrEqualTo(java.lang.Long value) {
          addCriterion("chain_id", value, ConditionMode.GREATER_EQUAL, "chainId", "java.lang.Long", "Float");
          return this;
      }

      public BpmsRuleChainSQL andChainIdLessThan(java.lang.Long value) {
          addCriterion("chain_id", value, ConditionMode.LESS_THEN, "chainId", "java.lang.Long", "Float");
          return this;
      }

      public BpmsRuleChainSQL andChainIdLessThanOrEqualTo(java.lang.Long value) {
          addCriterion("chain_id", value, ConditionMode.LESS_EQUAL, "chainId", "java.lang.Long", "Float");
          return this;
      }

      public BpmsRuleChainSQL andChainIdBetween(java.lang.Long value1, java.lang.Long value2) {
    	  addCriterion("chain_id", value1, value2, ConditionMode.BETWEEN, "chainId", "java.lang.Long", "Float");
    	  return this;
      }

      public BpmsRuleChainSQL andChainIdNotBetween(java.lang.Long value1, java.lang.Long value2) {
          addCriterion("chain_id", value1, value2, ConditionMode.NOT_BETWEEN, "chainId", "java.lang.Long", "Float");
          return this;
      }
        
      public BpmsRuleChainSQL andChainIdIn(List<java.lang.Long> values) {
          addCriterion("chain_id", values, ConditionMode.IN, "chainId", "java.lang.Long", "Float");
          return this;
      }

      public BpmsRuleChainSQL andChainIdNotIn(List<java.lang.Long> values) {
          addCriterion("chain_id", values, ConditionMode.NOT_IN, "chainId", "java.lang.Long", "Float");
          return this;
      }
	public BpmsRuleChainSQL andApplicationNameIsNull() {
		isnull("application_name");
		return this;
	}
	
	public BpmsRuleChainSQL andApplicationNameIsNotNull() {
		notNull("application_name");
		return this;
	}
	
	public BpmsRuleChainSQL andApplicationNameIsEmpty() {
		empty("application_name");
		return this;
	}

	public BpmsRuleChainSQL andApplicationNameIsNotEmpty() {
		notEmpty("application_name");
		return this;
	}
       public BpmsRuleChainSQL andApplicationNameLike(java.lang.String value) {
    	   addCriterion("application_name", value, ConditionMode.FUZZY, "applicationName", "java.lang.String", "Float");
    	   return this;
      }

      public BpmsRuleChainSQL andApplicationNameNotLike(java.lang.String value) {
          addCriterion("application_name", value, ConditionMode.NOT_FUZZY, "applicationName", "java.lang.String", "Float");
          return this;
      }
      public BpmsRuleChainSQL andApplicationNameEqualTo(java.lang.String value) {
          addCriterion("application_name", value, ConditionMode.EQUAL, "applicationName", "java.lang.String", "String");
          return this;
      }

      public BpmsRuleChainSQL andApplicationNameNotEqualTo(java.lang.String value) {
          addCriterion("application_name", value, ConditionMode.NOT_EQUAL, "applicationName", "java.lang.String", "String");
          return this;
      }

      public BpmsRuleChainSQL andApplicationNameGreaterThan(java.lang.String value) {
          addCriterion("application_name", value, ConditionMode.GREATER_THEN, "applicationName", "java.lang.String", "String");
          return this;
      }

      public BpmsRuleChainSQL andApplicationNameGreaterThanOrEqualTo(java.lang.String value) {
          addCriterion("application_name", value, ConditionMode.GREATER_EQUAL, "applicationName", "java.lang.String", "String");
          return this;
      }

      public BpmsRuleChainSQL andApplicationNameLessThan(java.lang.String value) {
          addCriterion("application_name", value, ConditionMode.LESS_THEN, "applicationName", "java.lang.String", "String");
          return this;
      }

      public BpmsRuleChainSQL andApplicationNameLessThanOrEqualTo(java.lang.String value) {
          addCriterion("application_name", value, ConditionMode.LESS_EQUAL, "applicationName", "java.lang.String", "String");
          return this;
      }

      public BpmsRuleChainSQL andApplicationNameBetween(java.lang.String value1, java.lang.String value2) {
    	  addCriterion("application_name", value1, value2, ConditionMode.BETWEEN, "applicationName", "java.lang.String", "String");
    	  return this;
      }

      public BpmsRuleChainSQL andApplicationNameNotBetween(java.lang.String value1, java.lang.String value2) {
          addCriterion("application_name", value1, value2, ConditionMode.NOT_BETWEEN, "applicationName", "java.lang.String", "String");
          return this;
      }
        
      public BpmsRuleChainSQL andApplicationNameIn(List<java.lang.String> values) {
          addCriterion("application_name", values, ConditionMode.IN, "applicationName", "java.lang.String", "String");
          return this;
      }

      public BpmsRuleChainSQL andApplicationNameNotIn(List<java.lang.String> values) {
          addCriterion("application_name", values, ConditionMode.NOT_IN, "applicationName", "java.lang.String", "String");
          return this;
      }
	public BpmsRuleChainSQL andChainNameIsNull() {
		isnull("chain_name");
		return this;
	}
	
	public BpmsRuleChainSQL andChainNameIsNotNull() {
		notNull("chain_name");
		return this;
	}
	
	public BpmsRuleChainSQL andChainNameIsEmpty() {
		empty("chain_name");
		return this;
	}

	public BpmsRuleChainSQL andChainNameIsNotEmpty() {
		notEmpty("chain_name");
		return this;
	}
       public BpmsRuleChainSQL andChainNameLike(java.lang.String value) {
    	   addCriterion("chain_name", value, ConditionMode.FUZZY, "chainName", "java.lang.String", "String");
    	   return this;
      }

      public BpmsRuleChainSQL andChainNameNotLike(java.lang.String value) {
          addCriterion("chain_name", value, ConditionMode.NOT_FUZZY, "chainName", "java.lang.String", "String");
          return this;
      }
      public BpmsRuleChainSQL andChainNameEqualTo(java.lang.String value) {
          addCriterion("chain_name", value, ConditionMode.EQUAL, "chainName", "java.lang.String", "String");
          return this;
      }

      public BpmsRuleChainSQL andChainNameNotEqualTo(java.lang.String value) {
          addCriterion("chain_name", value, ConditionMode.NOT_EQUAL, "chainName", "java.lang.String", "String");
          return this;
      }

      public BpmsRuleChainSQL andChainNameGreaterThan(java.lang.String value) {
          addCriterion("chain_name", value, ConditionMode.GREATER_THEN, "chainName", "java.lang.String", "String");
          return this;
      }

      public BpmsRuleChainSQL andChainNameGreaterThanOrEqualTo(java.lang.String value) {
          addCriterion("chain_name", value, ConditionMode.GREATER_EQUAL, "chainName", "java.lang.String", "String");
          return this;
      }

      public BpmsRuleChainSQL andChainNameLessThan(java.lang.String value) {
          addCriterion("chain_name", value, ConditionMode.LESS_THEN, "chainName", "java.lang.String", "String");
          return this;
      }

      public BpmsRuleChainSQL andChainNameLessThanOrEqualTo(java.lang.String value) {
          addCriterion("chain_name", value, ConditionMode.LESS_EQUAL, "chainName", "java.lang.String", "String");
          return this;
      }

      public BpmsRuleChainSQL andChainNameBetween(java.lang.String value1, java.lang.String value2) {
    	  addCriterion("chain_name", value1, value2, ConditionMode.BETWEEN, "chainName", "java.lang.String", "String");
    	  return this;
      }

      public BpmsRuleChainSQL andChainNameNotBetween(java.lang.String value1, java.lang.String value2) {
          addCriterion("chain_name", value1, value2, ConditionMode.NOT_BETWEEN, "chainName", "java.lang.String", "String");
          return this;
      }
        
      public BpmsRuleChainSQL andChainNameIn(List<java.lang.String> values) {
          addCriterion("chain_name", values, ConditionMode.IN, "chainName", "java.lang.String", "String");
          return this;
      }

      public BpmsRuleChainSQL andChainNameNotIn(List<java.lang.String> values) {
          addCriterion("chain_name", values, ConditionMode.NOT_IN, "chainName", "java.lang.String", "String");
          return this;
      }
	public BpmsRuleChainSQL andChainDescIsNull() {
		isnull("chain_desc");
		return this;
	}
	
	public BpmsRuleChainSQL andChainDescIsNotNull() {
		notNull("chain_desc");
		return this;
	}
	
	public BpmsRuleChainSQL andChainDescIsEmpty() {
		empty("chain_desc");
		return this;
	}

	public BpmsRuleChainSQL andChainDescIsNotEmpty() {
		notEmpty("chain_desc");
		return this;
	}
       public BpmsRuleChainSQL andChainDescLike(java.lang.String value) {
    	   addCriterion("chain_desc", value, ConditionMode.FUZZY, "chainDesc", "java.lang.String", "String");
    	   return this;
      }

      public BpmsRuleChainSQL andChainDescNotLike(java.lang.String value) {
          addCriterion("chain_desc", value, ConditionMode.NOT_FUZZY, "chainDesc", "java.lang.String", "String");
          return this;
      }
      public BpmsRuleChainSQL andChainDescEqualTo(java.lang.String value) {
          addCriterion("chain_desc", value, ConditionMode.EQUAL, "chainDesc", "java.lang.String", "String");
          return this;
      }

      public BpmsRuleChainSQL andChainDescNotEqualTo(java.lang.String value) {
          addCriterion("chain_desc", value, ConditionMode.NOT_EQUAL, "chainDesc", "java.lang.String", "String");
          return this;
      }

      public BpmsRuleChainSQL andChainDescGreaterThan(java.lang.String value) {
          addCriterion("chain_desc", value, ConditionMode.GREATER_THEN, "chainDesc", "java.lang.String", "String");
          return this;
      }

      public BpmsRuleChainSQL andChainDescGreaterThanOrEqualTo(java.lang.String value) {
          addCriterion("chain_desc", value, ConditionMode.GREATER_EQUAL, "chainDesc", "java.lang.String", "String");
          return this;
      }

      public BpmsRuleChainSQL andChainDescLessThan(java.lang.String value) {
          addCriterion("chain_desc", value, ConditionMode.LESS_THEN, "chainDesc", "java.lang.String", "String");
          return this;
      }

      public BpmsRuleChainSQL andChainDescLessThanOrEqualTo(java.lang.String value) {
          addCriterion("chain_desc", value, ConditionMode.LESS_EQUAL, "chainDesc", "java.lang.String", "String");
          return this;
      }

      public BpmsRuleChainSQL andChainDescBetween(java.lang.String value1, java.lang.String value2) {
    	  addCriterion("chain_desc", value1, value2, ConditionMode.BETWEEN, "chainDesc", "java.lang.String", "String");
    	  return this;
      }

      public BpmsRuleChainSQL andChainDescNotBetween(java.lang.String value1, java.lang.String value2) {
          addCriterion("chain_desc", value1, value2, ConditionMode.NOT_BETWEEN, "chainDesc", "java.lang.String", "String");
          return this;
      }
        
      public BpmsRuleChainSQL andChainDescIn(List<java.lang.String> values) {
          addCriterion("chain_desc", values, ConditionMode.IN, "chainDesc", "java.lang.String", "String");
          return this;
      }

      public BpmsRuleChainSQL andChainDescNotIn(List<java.lang.String> values) {
          addCriterion("chain_desc", values, ConditionMode.NOT_IN, "chainDesc", "java.lang.String", "String");
          return this;
      }
	public BpmsRuleChainSQL andElDataIsNull() {
		isnull("el_data");
		return this;
	}
	
	public BpmsRuleChainSQL andElDataIsNotNull() {
		notNull("el_data");
		return this;
	}
	
	public BpmsRuleChainSQL andElDataIsEmpty() {
		empty("el_data");
		return this;
	}

	public BpmsRuleChainSQL andElDataIsNotEmpty() {
		notEmpty("el_data");
		return this;
	}
       public BpmsRuleChainSQL andElDataLike(java.lang.String value) {
    	   addCriterion("el_data", value, ConditionMode.FUZZY, "elData", "java.lang.String", "String");
    	   return this;
      }

      public BpmsRuleChainSQL andElDataNotLike(java.lang.String value) {
          addCriterion("el_data", value, ConditionMode.NOT_FUZZY, "elData", "java.lang.String", "String");
          return this;
      }
      public BpmsRuleChainSQL andElDataEqualTo(java.lang.String value) {
          addCriterion("el_data", value, ConditionMode.EQUAL, "elData", "java.lang.String", "String");
          return this;
      }

      public BpmsRuleChainSQL andElDataNotEqualTo(java.lang.String value) {
          addCriterion("el_data", value, ConditionMode.NOT_EQUAL, "elData", "java.lang.String", "String");
          return this;
      }

      public BpmsRuleChainSQL andElDataGreaterThan(java.lang.String value) {
          addCriterion("el_data", value, ConditionMode.GREATER_THEN, "elData", "java.lang.String", "String");
          return this;
      }

      public BpmsRuleChainSQL andElDataGreaterThanOrEqualTo(java.lang.String value) {
          addCriterion("el_data", value, ConditionMode.GREATER_EQUAL, "elData", "java.lang.String", "String");
          return this;
      }

      public BpmsRuleChainSQL andElDataLessThan(java.lang.String value) {
          addCriterion("el_data", value, ConditionMode.LESS_THEN, "elData", "java.lang.String", "String");
          return this;
      }

      public BpmsRuleChainSQL andElDataLessThanOrEqualTo(java.lang.String value) {
          addCriterion("el_data", value, ConditionMode.LESS_EQUAL, "elData", "java.lang.String", "String");
          return this;
      }

      public BpmsRuleChainSQL andElDataBetween(java.lang.String value1, java.lang.String value2) {
    	  addCriterion("el_data", value1, value2, ConditionMode.BETWEEN, "elData", "java.lang.String", "String");
    	  return this;
      }

      public BpmsRuleChainSQL andElDataNotBetween(java.lang.String value1, java.lang.String value2) {
          addCriterion("el_data", value1, value2, ConditionMode.NOT_BETWEEN, "elData", "java.lang.String", "String");
          return this;
      }
        
      public BpmsRuleChainSQL andElDataIn(List<java.lang.String> values) {
          addCriterion("el_data", values, ConditionMode.IN, "elData", "java.lang.String", "String");
          return this;
      }

      public BpmsRuleChainSQL andElDataNotIn(List<java.lang.String> values) {
          addCriterion("el_data", values, ConditionMode.NOT_IN, "elData", "java.lang.String", "String");
          return this;
      }
	public BpmsRuleChainSQL andStatusIsNull() {
		isnull("status");
		return this;
	}
	
	public BpmsRuleChainSQL andStatusIsNotNull() {
		notNull("status");
		return this;
	}
	
	public BpmsRuleChainSQL andStatusIsEmpty() {
		empty("status");
		return this;
	}

	public BpmsRuleChainSQL andStatusIsNotEmpty() {
		notEmpty("status");
		return this;
	}
      public BpmsRuleChainSQL andStatusEqualTo(java.lang.Integer value) {
          addCriterion("status", value, ConditionMode.EQUAL, "status", "java.lang.Integer", "Float");
          return this;
      }

      public BpmsRuleChainSQL andStatusNotEqualTo(java.lang.Integer value) {
          addCriterion("status", value, ConditionMode.NOT_EQUAL, "status", "java.lang.Integer", "Float");
          return this;
      }

      public BpmsRuleChainSQL andStatusGreaterThan(java.lang.Integer value) {
          addCriterion("status", value, ConditionMode.GREATER_THEN, "status", "java.lang.Integer", "Float");
          return this;
      }

      public BpmsRuleChainSQL andStatusGreaterThanOrEqualTo(java.lang.Integer value) {
          addCriterion("status", value, ConditionMode.GREATER_EQUAL, "status", "java.lang.Integer", "Float");
          return this;
      }

      public BpmsRuleChainSQL andStatusLessThan(java.lang.Integer value) {
          addCriterion("status", value, ConditionMode.LESS_THEN, "status", "java.lang.Integer", "Float");
          return this;
      }

      public BpmsRuleChainSQL andStatusLessThanOrEqualTo(java.lang.Integer value) {
          addCriterion("status", value, ConditionMode.LESS_EQUAL, "status", "java.lang.Integer", "Float");
          return this;
      }

      public BpmsRuleChainSQL andStatusBetween(java.lang.Integer value1, java.lang.Integer value2) {
    	  addCriterion("status", value1, value2, ConditionMode.BETWEEN, "status", "java.lang.Integer", "Float");
    	  return this;
      }

      public BpmsRuleChainSQL andStatusNotBetween(java.lang.Integer value1, java.lang.Integer value2) {
          addCriterion("status", value1, value2, ConditionMode.NOT_BETWEEN, "status", "java.lang.Integer", "Float");
          return this;
      }
        
      public BpmsRuleChainSQL andStatusIn(List<java.lang.Integer> values) {
          addCriterion("status", values, ConditionMode.IN, "status", "java.lang.Integer", "Float");
          return this;
      }

      public BpmsRuleChainSQL andStatusNotIn(List<java.lang.Integer> values) {
          addCriterion("status", values, ConditionMode.NOT_IN, "status", "java.lang.Integer", "Float");
          return this;
      }
	public BpmsRuleChainSQL andUpdateTimeIsNull() {
		isnull("update_time");
		return this;
	}
	
	public BpmsRuleChainSQL andUpdateTimeIsNotNull() {
		notNull("update_time");
		return this;
	}
	
	public BpmsRuleChainSQL andUpdateTimeIsEmpty() {
		empty("update_time");
		return this;
	}

	public BpmsRuleChainSQL andUpdateTimeIsNotEmpty() {
		notEmpty("update_time");
		return this;
	}
      public BpmsRuleChainSQL andUpdateTimeEqualTo(java.util.Date value) {
          addCriterion("update_time", value, ConditionMode.EQUAL, "updateTime", "java.util.Date", "String");
          return this;
      }

      public BpmsRuleChainSQL andUpdateTimeNotEqualTo(java.util.Date value) {
          addCriterion("update_time", value, ConditionMode.NOT_EQUAL, "updateTime", "java.util.Date", "String");
          return this;
      }

      public BpmsRuleChainSQL andUpdateTimeGreaterThan(java.util.Date value) {
          addCriterion("update_time", value, ConditionMode.GREATER_THEN, "updateTime", "java.util.Date", "String");
          return this;
      }

      public BpmsRuleChainSQL andUpdateTimeGreaterThanOrEqualTo(java.util.Date value) {
          addCriterion("update_time", value, ConditionMode.GREATER_EQUAL, "updateTime", "java.util.Date", "String");
          return this;
      }

      public BpmsRuleChainSQL andUpdateTimeLessThan(java.util.Date value) {
          addCriterion("update_time", value, ConditionMode.LESS_THEN, "updateTime", "java.util.Date", "String");
          return this;
      }

      public BpmsRuleChainSQL andUpdateTimeLessThanOrEqualTo(java.util.Date value) {
          addCriterion("update_time", value, ConditionMode.LESS_EQUAL, "updateTime", "java.util.Date", "String");
          return this;
      }

      public BpmsRuleChainSQL andUpdateTimeBetween(java.util.Date value1, java.util.Date value2) {
    	  addCriterion("update_time", value1, value2, ConditionMode.BETWEEN, "updateTime", "java.util.Date", "String");
    	  return this;
      }

      public BpmsRuleChainSQL andUpdateTimeNotBetween(java.util.Date value1, java.util.Date value2) {
          addCriterion("update_time", value1, value2, ConditionMode.NOT_BETWEEN, "updateTime", "java.util.Date", "String");
          return this;
      }
        
      public BpmsRuleChainSQL andUpdateTimeIn(List<java.util.Date> values) {
          addCriterion("update_time", values, ConditionMode.IN, "updateTime", "java.util.Date", "String");
          return this;
      }

      public BpmsRuleChainSQL andUpdateTimeNotIn(List<java.util.Date> values) {
          addCriterion("update_time", values, ConditionMode.NOT_IN, "updateTime", "java.util.Date", "String");
          return this;
      }
	public BpmsRuleChainSQL andUpdaterIsNull() {
		isnull("updater");
		return this;
	}
	
	public BpmsRuleChainSQL andUpdaterIsNotNull() {
		notNull("updater");
		return this;
	}
	
	public BpmsRuleChainSQL andUpdaterIsEmpty() {
		empty("updater");
		return this;
	}

	public BpmsRuleChainSQL andUpdaterIsNotEmpty() {
		notEmpty("updater");
		return this;
	}
       public BpmsRuleChainSQL andUpdaterLike(java.lang.String value) {
    	   addCriterion("updater", value, ConditionMode.FUZZY, "updater", "java.lang.String", "String");
    	   return this;
      }

      public BpmsRuleChainSQL andUpdaterNotLike(java.lang.String value) {
          addCriterion("updater", value, ConditionMode.NOT_FUZZY, "updater", "java.lang.String", "String");
          return this;
      }
      public BpmsRuleChainSQL andUpdaterEqualTo(java.lang.String value) {
          addCriterion("updater", value, ConditionMode.EQUAL, "updater", "java.lang.String", "String");
          return this;
      }

      public BpmsRuleChainSQL andUpdaterNotEqualTo(java.lang.String value) {
          addCriterion("updater", value, ConditionMode.NOT_EQUAL, "updater", "java.lang.String", "String");
          return this;
      }

      public BpmsRuleChainSQL andUpdaterGreaterThan(java.lang.String value) {
          addCriterion("updater", value, ConditionMode.GREATER_THEN, "updater", "java.lang.String", "String");
          return this;
      }

      public BpmsRuleChainSQL andUpdaterGreaterThanOrEqualTo(java.lang.String value) {
          addCriterion("updater", value, ConditionMode.GREATER_EQUAL, "updater", "java.lang.String", "String");
          return this;
      }

      public BpmsRuleChainSQL andUpdaterLessThan(java.lang.String value) {
          addCriterion("updater", value, ConditionMode.LESS_THEN, "updater", "java.lang.String", "String");
          return this;
      }

      public BpmsRuleChainSQL andUpdaterLessThanOrEqualTo(java.lang.String value) {
          addCriterion("updater", value, ConditionMode.LESS_EQUAL, "updater", "java.lang.String", "String");
          return this;
      }

      public BpmsRuleChainSQL andUpdaterBetween(java.lang.String value1, java.lang.String value2) {
    	  addCriterion("updater", value1, value2, ConditionMode.BETWEEN, "updater", "java.lang.String", "String");
    	  return this;
      }

      public BpmsRuleChainSQL andUpdaterNotBetween(java.lang.String value1, java.lang.String value2) {
          addCriterion("updater", value1, value2, ConditionMode.NOT_BETWEEN, "updater", "java.lang.String", "String");
          return this;
      }
        
      public BpmsRuleChainSQL andUpdaterIn(List<java.lang.String> values) {
          addCriterion("updater", values, ConditionMode.IN, "updater", "java.lang.String", "String");
          return this;
      }

      public BpmsRuleChainSQL andUpdaterNotIn(List<java.lang.String> values) {
          addCriterion("updater", values, ConditionMode.NOT_IN, "updater", "java.lang.String", "String");
          return this;
      }
}