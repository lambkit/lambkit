/**
 * Copyright (c) 2015-2017, Henry Yang 杨勇 (gismail@foxmail.com).
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.lambkit.module.bpms.sql;

import com.lambkit.db.sql.ConditionMode;
import com.lambkit.db.sql.Column;
import com.lambkit.db.sql.Columns;
import com.lambkit.db.sql.Example;

import java.io.Serializable;
import java.util.List;

/**
 * @author yangyong 
 * @website: www.lambkit.com
 * @email: gismail@foxmail.com
 * @date ${date}
 * @version 1.0
 * @since 1.0
 */

public class BpmsServiceFlowSQL extends Columns implements Serializable {
	private static final long serialVersionUID = 1L;
	
	public static BpmsServiceFlowSQL of() {
		return new BpmsServiceFlowSQL();
	}
	
	public static BpmsServiceFlowSQL by(Column column) {
		BpmsServiceFlowSQL that = new BpmsServiceFlowSQL();
		that.add(column);
        return that;
    }

    public static BpmsServiceFlowSQL by(String name, Object value) {
        return (BpmsServiceFlowSQL) create().eq(name, value);
    }
    
    public Example example() {
    	return Example.create("bpms_service_flow", this);
    }
    
    /**
     * equals
     *
     * @param name
     * @param value
     * @return
     */
    public BpmsServiceFlowSQL eq(String name, Object value) {
    	super.eq(name, value);
        return this;
    }

    /**
     * not equals !=
     *
     * @param name
     * @param value
     * @return
     */
    public BpmsServiceFlowSQL ne(String name, Object value) {
    	super.ne(name, value);
        return this;
    }


    /**
     * like
     *
     * @param name
     * @param value
     * @return
     */

    public BpmsServiceFlowSQL like(String name, Object value) {
    	super.like(name, value);
        return this;
    }
    
    public BpmsServiceFlowSQL notLike(String name, Object value) {
    	super.notLike(name, value);
        return this;
    }

    /**
     * 大于 great than
     *
     * @param name
     * @param value
     * @return
     */
    public BpmsServiceFlowSQL gt(String name, Object value) {
    	super.gt(name, value);
        return this;
    }

    /**
     * 大于等于 great or equal
     *
     * @param name
     * @param value
     * @return
     */
    public BpmsServiceFlowSQL ge(String name, Object value) {
    	super.ge(name, value);
        return this;
    }

    /**
     * 小于 less than
     *
     * @param name
     * @param value
     * @return
     */
    public BpmsServiceFlowSQL lt(String name, Object value) {
    	super.lt(name, value);
        return this;
    }

    /**
     * 小于等于 less or equal
     *
     * @param name
     * @param value
     * @return
     */
    public BpmsServiceFlowSQL le(String name, Object value) {
    	super.le(name, value);
        return this;
    }
    
    public BpmsServiceFlowSQL isnull(String name) {
    	super.isnull(name);
        return this;
    } 

    public BpmsServiceFlowSQL notNull(String name) {
    	super.notNull(name);
        return this;
    } 
    
    public BpmsServiceFlowSQL empty(String name) {
    	super.empty(name);
        return this;
    } 
    
    public BpmsServiceFlowSQL notEmpty(String name) {
    	super.notEmpty(name);
        return this;
    } 
    
    public BpmsServiceFlowSQL add(Column column) {
    	super.add(column);
    	return this;
    }
    
    /**************************/
	
	public void addCriterion(String name, Object value, ConditionMode logic, String property, String typeHandler, String valueType) {
		 if (value == null) {
			 throw new RuntimeException("Value for " + property + " cannot be null");
		 }
		 add(Column.create(name, value, logic, typeHandler, valueType));
	}
   
	public void addCriterion(String name, Object value1, Object value2, ConditionMode logic, String property, String typeHandler, String valueType) {
		 if (value1 == null || value2 == null) {
			 throw new RuntimeException("Between values for " + property + " cannot be null");
		 }
		 add(Column.create(name, value1, value2, logic, typeHandler, valueType));
	}
		 
	public BpmsServiceFlowSQL andFlowIdIsNull() {
		isnull("flow_id");
		return this;
	}
	
	public BpmsServiceFlowSQL andFlowIdIsNotNull() {
		notNull("flow_id");
		return this;
	}
	
	public BpmsServiceFlowSQL andFlowIdIsEmpty() {
		empty("flow_id");
		return this;
	}

	public BpmsServiceFlowSQL andFlowIdIsNotEmpty() {
		notEmpty("flow_id");
		return this;
	}
      public BpmsServiceFlowSQL andFlowIdEqualTo(java.lang.Long value) {
          addCriterion("flow_id", value, ConditionMode.EQUAL, "flowId", "java.lang.Long", "Float");
          return this;
      }

      public BpmsServiceFlowSQL andFlowIdNotEqualTo(java.lang.Long value) {
          addCriterion("flow_id", value, ConditionMode.NOT_EQUAL, "flowId", "java.lang.Long", "Float");
          return this;
      }

      public BpmsServiceFlowSQL andFlowIdGreaterThan(java.lang.Long value) {
          addCriterion("flow_id", value, ConditionMode.GREATER_THEN, "flowId", "java.lang.Long", "Float");
          return this;
      }

      public BpmsServiceFlowSQL andFlowIdGreaterThanOrEqualTo(java.lang.Long value) {
          addCriterion("flow_id", value, ConditionMode.GREATER_EQUAL, "flowId", "java.lang.Long", "Float");
          return this;
      }

      public BpmsServiceFlowSQL andFlowIdLessThan(java.lang.Long value) {
          addCriterion("flow_id", value, ConditionMode.LESS_THEN, "flowId", "java.lang.Long", "Float");
          return this;
      }

      public BpmsServiceFlowSQL andFlowIdLessThanOrEqualTo(java.lang.Long value) {
          addCriterion("flow_id", value, ConditionMode.LESS_EQUAL, "flowId", "java.lang.Long", "Float");
          return this;
      }

      public BpmsServiceFlowSQL andFlowIdBetween(java.lang.Long value1, java.lang.Long value2) {
    	  addCriterion("flow_id", value1, value2, ConditionMode.BETWEEN, "flowId", "java.lang.Long", "Float");
    	  return this;
      }

      public BpmsServiceFlowSQL andFlowIdNotBetween(java.lang.Long value1, java.lang.Long value2) {
          addCriterion("flow_id", value1, value2, ConditionMode.NOT_BETWEEN, "flowId", "java.lang.Long", "Float");
          return this;
      }
        
      public BpmsServiceFlowSQL andFlowIdIn(List<java.lang.Long> values) {
          addCriterion("flow_id", values, ConditionMode.IN, "flowId", "java.lang.Long", "Float");
          return this;
      }

      public BpmsServiceFlowSQL andFlowIdNotIn(List<java.lang.Long> values) {
          addCriterion("flow_id", values, ConditionMode.NOT_IN, "flowId", "java.lang.Long", "Float");
          return this;
      }
	public BpmsServiceFlowSQL andCatalogCodeIsNull() {
		isnull("catalog_code");
		return this;
	}
	
	public BpmsServiceFlowSQL andCatalogCodeIsNotNull() {
		notNull("catalog_code");
		return this;
	}
	
	public BpmsServiceFlowSQL andCatalogCodeIsEmpty() {
		empty("catalog_code");
		return this;
	}

	public BpmsServiceFlowSQL andCatalogCodeIsNotEmpty() {
		notEmpty("catalog_code");
		return this;
	}
       public BpmsServiceFlowSQL andCatalogCodeLike(java.lang.String value) {
    	   addCriterion("catalog_code", value, ConditionMode.FUZZY, "catalogCode", "java.lang.String", "Float");
    	   return this;
      }

      public BpmsServiceFlowSQL andCatalogCodeNotLike(java.lang.String value) {
          addCriterion("catalog_code", value, ConditionMode.NOT_FUZZY, "catalogCode", "java.lang.String", "Float");
          return this;
      }
      public BpmsServiceFlowSQL andCatalogCodeEqualTo(java.lang.String value) {
          addCriterion("catalog_code", value, ConditionMode.EQUAL, "catalogCode", "java.lang.String", "String");
          return this;
      }

      public BpmsServiceFlowSQL andCatalogCodeNotEqualTo(java.lang.String value) {
          addCriterion("catalog_code", value, ConditionMode.NOT_EQUAL, "catalogCode", "java.lang.String", "String");
          return this;
      }

      public BpmsServiceFlowSQL andCatalogCodeGreaterThan(java.lang.String value) {
          addCriterion("catalog_code", value, ConditionMode.GREATER_THEN, "catalogCode", "java.lang.String", "String");
          return this;
      }

      public BpmsServiceFlowSQL andCatalogCodeGreaterThanOrEqualTo(java.lang.String value) {
          addCriterion("catalog_code", value, ConditionMode.GREATER_EQUAL, "catalogCode", "java.lang.String", "String");
          return this;
      }

      public BpmsServiceFlowSQL andCatalogCodeLessThan(java.lang.String value) {
          addCriterion("catalog_code", value, ConditionMode.LESS_THEN, "catalogCode", "java.lang.String", "String");
          return this;
      }

      public BpmsServiceFlowSQL andCatalogCodeLessThanOrEqualTo(java.lang.String value) {
          addCriterion("catalog_code", value, ConditionMode.LESS_EQUAL, "catalogCode", "java.lang.String", "String");
          return this;
      }

      public BpmsServiceFlowSQL andCatalogCodeBetween(java.lang.String value1, java.lang.String value2) {
    	  addCriterion("catalog_code", value1, value2, ConditionMode.BETWEEN, "catalogCode", "java.lang.String", "String");
    	  return this;
      }

      public BpmsServiceFlowSQL andCatalogCodeNotBetween(java.lang.String value1, java.lang.String value2) {
          addCriterion("catalog_code", value1, value2, ConditionMode.NOT_BETWEEN, "catalogCode", "java.lang.String", "String");
          return this;
      }
        
      public BpmsServiceFlowSQL andCatalogCodeIn(List<java.lang.String> values) {
          addCriterion("catalog_code", values, ConditionMode.IN, "catalogCode", "java.lang.String", "String");
          return this;
      }

      public BpmsServiceFlowSQL andCatalogCodeNotIn(List<java.lang.String> values) {
          addCriterion("catalog_code", values, ConditionMode.NOT_IN, "catalogCode", "java.lang.String", "String");
          return this;
      }
	public BpmsServiceFlowSQL andFlowCodeIsNull() {
		isnull("flow_code");
		return this;
	}
	
	public BpmsServiceFlowSQL andFlowCodeIsNotNull() {
		notNull("flow_code");
		return this;
	}
	
	public BpmsServiceFlowSQL andFlowCodeIsEmpty() {
		empty("flow_code");
		return this;
	}

	public BpmsServiceFlowSQL andFlowCodeIsNotEmpty() {
		notEmpty("flow_code");
		return this;
	}
       public BpmsServiceFlowSQL andFlowCodeLike(java.lang.String value) {
    	   addCriterion("flow_code", value, ConditionMode.FUZZY, "flowCode", "java.lang.String", "String");
    	   return this;
      }

      public BpmsServiceFlowSQL andFlowCodeNotLike(java.lang.String value) {
          addCriterion("flow_code", value, ConditionMode.NOT_FUZZY, "flowCode", "java.lang.String", "String");
          return this;
      }
      public BpmsServiceFlowSQL andFlowCodeEqualTo(java.lang.String value) {
          addCriterion("flow_code", value, ConditionMode.EQUAL, "flowCode", "java.lang.String", "String");
          return this;
      }

      public BpmsServiceFlowSQL andFlowCodeNotEqualTo(java.lang.String value) {
          addCriterion("flow_code", value, ConditionMode.NOT_EQUAL, "flowCode", "java.lang.String", "String");
          return this;
      }

      public BpmsServiceFlowSQL andFlowCodeGreaterThan(java.lang.String value) {
          addCriterion("flow_code", value, ConditionMode.GREATER_THEN, "flowCode", "java.lang.String", "String");
          return this;
      }

      public BpmsServiceFlowSQL andFlowCodeGreaterThanOrEqualTo(java.lang.String value) {
          addCriterion("flow_code", value, ConditionMode.GREATER_EQUAL, "flowCode", "java.lang.String", "String");
          return this;
      }

      public BpmsServiceFlowSQL andFlowCodeLessThan(java.lang.String value) {
          addCriterion("flow_code", value, ConditionMode.LESS_THEN, "flowCode", "java.lang.String", "String");
          return this;
      }

      public BpmsServiceFlowSQL andFlowCodeLessThanOrEqualTo(java.lang.String value) {
          addCriterion("flow_code", value, ConditionMode.LESS_EQUAL, "flowCode", "java.lang.String", "String");
          return this;
      }

      public BpmsServiceFlowSQL andFlowCodeBetween(java.lang.String value1, java.lang.String value2) {
    	  addCriterion("flow_code", value1, value2, ConditionMode.BETWEEN, "flowCode", "java.lang.String", "String");
    	  return this;
      }

      public BpmsServiceFlowSQL andFlowCodeNotBetween(java.lang.String value1, java.lang.String value2) {
          addCriterion("flow_code", value1, value2, ConditionMode.NOT_BETWEEN, "flowCode", "java.lang.String", "String");
          return this;
      }
        
      public BpmsServiceFlowSQL andFlowCodeIn(List<java.lang.String> values) {
          addCriterion("flow_code", values, ConditionMode.IN, "flowCode", "java.lang.String", "String");
          return this;
      }

      public BpmsServiceFlowSQL andFlowCodeNotIn(List<java.lang.String> values) {
          addCriterion("flow_code", values, ConditionMode.NOT_IN, "flowCode", "java.lang.String", "String");
          return this;
      }
	public BpmsServiceFlowSQL andFlowNameIsNull() {
		isnull("flow_name");
		return this;
	}
	
	public BpmsServiceFlowSQL andFlowNameIsNotNull() {
		notNull("flow_name");
		return this;
	}
	
	public BpmsServiceFlowSQL andFlowNameIsEmpty() {
		empty("flow_name");
		return this;
	}

	public BpmsServiceFlowSQL andFlowNameIsNotEmpty() {
		notEmpty("flow_name");
		return this;
	}
       public BpmsServiceFlowSQL andFlowNameLike(java.lang.String value) {
    	   addCriterion("flow_name", value, ConditionMode.FUZZY, "flowName", "java.lang.String", "String");
    	   return this;
      }

      public BpmsServiceFlowSQL andFlowNameNotLike(java.lang.String value) {
          addCriterion("flow_name", value, ConditionMode.NOT_FUZZY, "flowName", "java.lang.String", "String");
          return this;
      }
      public BpmsServiceFlowSQL andFlowNameEqualTo(java.lang.String value) {
          addCriterion("flow_name", value, ConditionMode.EQUAL, "flowName", "java.lang.String", "String");
          return this;
      }

      public BpmsServiceFlowSQL andFlowNameNotEqualTo(java.lang.String value) {
          addCriterion("flow_name", value, ConditionMode.NOT_EQUAL, "flowName", "java.lang.String", "String");
          return this;
      }

      public BpmsServiceFlowSQL andFlowNameGreaterThan(java.lang.String value) {
          addCriterion("flow_name", value, ConditionMode.GREATER_THEN, "flowName", "java.lang.String", "String");
          return this;
      }

      public BpmsServiceFlowSQL andFlowNameGreaterThanOrEqualTo(java.lang.String value) {
          addCriterion("flow_name", value, ConditionMode.GREATER_EQUAL, "flowName", "java.lang.String", "String");
          return this;
      }

      public BpmsServiceFlowSQL andFlowNameLessThan(java.lang.String value) {
          addCriterion("flow_name", value, ConditionMode.LESS_THEN, "flowName", "java.lang.String", "String");
          return this;
      }

      public BpmsServiceFlowSQL andFlowNameLessThanOrEqualTo(java.lang.String value) {
          addCriterion("flow_name", value, ConditionMode.LESS_EQUAL, "flowName", "java.lang.String", "String");
          return this;
      }

      public BpmsServiceFlowSQL andFlowNameBetween(java.lang.String value1, java.lang.String value2) {
    	  addCriterion("flow_name", value1, value2, ConditionMode.BETWEEN, "flowName", "java.lang.String", "String");
    	  return this;
      }

      public BpmsServiceFlowSQL andFlowNameNotBetween(java.lang.String value1, java.lang.String value2) {
          addCriterion("flow_name", value1, value2, ConditionMode.NOT_BETWEEN, "flowName", "java.lang.String", "String");
          return this;
      }
        
      public BpmsServiceFlowSQL andFlowNameIn(List<java.lang.String> values) {
          addCriterion("flow_name", values, ConditionMode.IN, "flowName", "java.lang.String", "String");
          return this;
      }

      public BpmsServiceFlowSQL andFlowNameNotIn(List<java.lang.String> values) {
          addCriterion("flow_name", values, ConditionMode.NOT_IN, "flowName", "java.lang.String", "String");
          return this;
      }
	public BpmsServiceFlowSQL andFlowVersionIsNull() {
		isnull("flow_version");
		return this;
	}
	
	public BpmsServiceFlowSQL andFlowVersionIsNotNull() {
		notNull("flow_version");
		return this;
	}
	
	public BpmsServiceFlowSQL andFlowVersionIsEmpty() {
		empty("flow_version");
		return this;
	}

	public BpmsServiceFlowSQL andFlowVersionIsNotEmpty() {
		notEmpty("flow_version");
		return this;
	}
       public BpmsServiceFlowSQL andFlowVersionLike(java.lang.String value) {
    	   addCriterion("flow_version", value, ConditionMode.FUZZY, "flowVersion", "java.lang.String", "String");
    	   return this;
      }

      public BpmsServiceFlowSQL andFlowVersionNotLike(java.lang.String value) {
          addCriterion("flow_version", value, ConditionMode.NOT_FUZZY, "flowVersion", "java.lang.String", "String");
          return this;
      }
      public BpmsServiceFlowSQL andFlowVersionEqualTo(java.lang.String value) {
          addCriterion("flow_version", value, ConditionMode.EQUAL, "flowVersion", "java.lang.String", "String");
          return this;
      }

      public BpmsServiceFlowSQL andFlowVersionNotEqualTo(java.lang.String value) {
          addCriterion("flow_version", value, ConditionMode.NOT_EQUAL, "flowVersion", "java.lang.String", "String");
          return this;
      }

      public BpmsServiceFlowSQL andFlowVersionGreaterThan(java.lang.String value) {
          addCriterion("flow_version", value, ConditionMode.GREATER_THEN, "flowVersion", "java.lang.String", "String");
          return this;
      }

      public BpmsServiceFlowSQL andFlowVersionGreaterThanOrEqualTo(java.lang.String value) {
          addCriterion("flow_version", value, ConditionMode.GREATER_EQUAL, "flowVersion", "java.lang.String", "String");
          return this;
      }

      public BpmsServiceFlowSQL andFlowVersionLessThan(java.lang.String value) {
          addCriterion("flow_version", value, ConditionMode.LESS_THEN, "flowVersion", "java.lang.String", "String");
          return this;
      }

      public BpmsServiceFlowSQL andFlowVersionLessThanOrEqualTo(java.lang.String value) {
          addCriterion("flow_version", value, ConditionMode.LESS_EQUAL, "flowVersion", "java.lang.String", "String");
          return this;
      }

      public BpmsServiceFlowSQL andFlowVersionBetween(java.lang.String value1, java.lang.String value2) {
    	  addCriterion("flow_version", value1, value2, ConditionMode.BETWEEN, "flowVersion", "java.lang.String", "String");
    	  return this;
      }

      public BpmsServiceFlowSQL andFlowVersionNotBetween(java.lang.String value1, java.lang.String value2) {
          addCriterion("flow_version", value1, value2, ConditionMode.NOT_BETWEEN, "flowVersion", "java.lang.String", "String");
          return this;
      }
        
      public BpmsServiceFlowSQL andFlowVersionIn(List<java.lang.String> values) {
          addCriterion("flow_version", values, ConditionMode.IN, "flowVersion", "java.lang.String", "String");
          return this;
      }

      public BpmsServiceFlowSQL andFlowVersionNotIn(List<java.lang.String> values) {
          addCriterion("flow_version", values, ConditionMode.NOT_IN, "flowVersion", "java.lang.String", "String");
          return this;
      }
	public BpmsServiceFlowSQL andIsPublishIsNull() {
		isnull("is_publish");
		return this;
	}
	
	public BpmsServiceFlowSQL andIsPublishIsNotNull() {
		notNull("is_publish");
		return this;
	}
	
	public BpmsServiceFlowSQL andIsPublishIsEmpty() {
		empty("is_publish");
		return this;
	}

	public BpmsServiceFlowSQL andIsPublishIsNotEmpty() {
		notEmpty("is_publish");
		return this;
	}
      public BpmsServiceFlowSQL andIsPublishEqualTo(java.lang.Integer value) {
          addCriterion("is_publish", value, ConditionMode.EQUAL, "isPublish", "java.lang.Integer", "Float");
          return this;
      }

      public BpmsServiceFlowSQL andIsPublishNotEqualTo(java.lang.Integer value) {
          addCriterion("is_publish", value, ConditionMode.NOT_EQUAL, "isPublish", "java.lang.Integer", "Float");
          return this;
      }

      public BpmsServiceFlowSQL andIsPublishGreaterThan(java.lang.Integer value) {
          addCriterion("is_publish", value, ConditionMode.GREATER_THEN, "isPublish", "java.lang.Integer", "Float");
          return this;
      }

      public BpmsServiceFlowSQL andIsPublishGreaterThanOrEqualTo(java.lang.Integer value) {
          addCriterion("is_publish", value, ConditionMode.GREATER_EQUAL, "isPublish", "java.lang.Integer", "Float");
          return this;
      }

      public BpmsServiceFlowSQL andIsPublishLessThan(java.lang.Integer value) {
          addCriterion("is_publish", value, ConditionMode.LESS_THEN, "isPublish", "java.lang.Integer", "Float");
          return this;
      }

      public BpmsServiceFlowSQL andIsPublishLessThanOrEqualTo(java.lang.Integer value) {
          addCriterion("is_publish", value, ConditionMode.LESS_EQUAL, "isPublish", "java.lang.Integer", "Float");
          return this;
      }

      public BpmsServiceFlowSQL andIsPublishBetween(java.lang.Integer value1, java.lang.Integer value2) {
    	  addCriterion("is_publish", value1, value2, ConditionMode.BETWEEN, "isPublish", "java.lang.Integer", "Float");
    	  return this;
      }

      public BpmsServiceFlowSQL andIsPublishNotBetween(java.lang.Integer value1, java.lang.Integer value2) {
          addCriterion("is_publish", value1, value2, ConditionMode.NOT_BETWEEN, "isPublish", "java.lang.Integer", "Float");
          return this;
      }
        
      public BpmsServiceFlowSQL andIsPublishIn(List<java.lang.Integer> values) {
          addCriterion("is_publish", values, ConditionMode.IN, "isPublish", "java.lang.Integer", "Float");
          return this;
      }

      public BpmsServiceFlowSQL andIsPublishNotIn(List<java.lang.Integer> values) {
          addCriterion("is_publish", values, ConditionMode.NOT_IN, "isPublish", "java.lang.Integer", "Float");
          return this;
      }
	public BpmsServiceFlowSQL andCreateTimeIsNull() {
		isnull("create_time");
		return this;
	}
	
	public BpmsServiceFlowSQL andCreateTimeIsNotNull() {
		notNull("create_time");
		return this;
	}
	
	public BpmsServiceFlowSQL andCreateTimeIsEmpty() {
		empty("create_time");
		return this;
	}

	public BpmsServiceFlowSQL andCreateTimeIsNotEmpty() {
		notEmpty("create_time");
		return this;
	}
      public BpmsServiceFlowSQL andCreateTimeEqualTo(java.util.Date value) {
          addCriterion("create_time", value, ConditionMode.EQUAL, "createTime", "java.util.Date", "String");
          return this;
      }

      public BpmsServiceFlowSQL andCreateTimeNotEqualTo(java.util.Date value) {
          addCriterion("create_time", value, ConditionMode.NOT_EQUAL, "createTime", "java.util.Date", "String");
          return this;
      }

      public BpmsServiceFlowSQL andCreateTimeGreaterThan(java.util.Date value) {
          addCriterion("create_time", value, ConditionMode.GREATER_THEN, "createTime", "java.util.Date", "String");
          return this;
      }

      public BpmsServiceFlowSQL andCreateTimeGreaterThanOrEqualTo(java.util.Date value) {
          addCriterion("create_time", value, ConditionMode.GREATER_EQUAL, "createTime", "java.util.Date", "String");
          return this;
      }

      public BpmsServiceFlowSQL andCreateTimeLessThan(java.util.Date value) {
          addCriterion("create_time", value, ConditionMode.LESS_THEN, "createTime", "java.util.Date", "String");
          return this;
      }

      public BpmsServiceFlowSQL andCreateTimeLessThanOrEqualTo(java.util.Date value) {
          addCriterion("create_time", value, ConditionMode.LESS_EQUAL, "createTime", "java.util.Date", "String");
          return this;
      }

      public BpmsServiceFlowSQL andCreateTimeBetween(java.util.Date value1, java.util.Date value2) {
    	  addCriterion("create_time", value1, value2, ConditionMode.BETWEEN, "createTime", "java.util.Date", "String");
    	  return this;
      }

      public BpmsServiceFlowSQL andCreateTimeNotBetween(java.util.Date value1, java.util.Date value2) {
          addCriterion("create_time", value1, value2, ConditionMode.NOT_BETWEEN, "createTime", "java.util.Date", "String");
          return this;
      }
        
      public BpmsServiceFlowSQL andCreateTimeIn(List<java.util.Date> values) {
          addCriterion("create_time", values, ConditionMode.IN, "createTime", "java.util.Date", "String");
          return this;
      }

      public BpmsServiceFlowSQL andCreateTimeNotIn(List<java.util.Date> values) {
          addCriterion("create_time", values, ConditionMode.NOT_IN, "createTime", "java.util.Date", "String");
          return this;
      }
	public BpmsServiceFlowSQL andUpdateTimeIsNull() {
		isnull("update_time");
		return this;
	}
	
	public BpmsServiceFlowSQL andUpdateTimeIsNotNull() {
		notNull("update_time");
		return this;
	}
	
	public BpmsServiceFlowSQL andUpdateTimeIsEmpty() {
		empty("update_time");
		return this;
	}

	public BpmsServiceFlowSQL andUpdateTimeIsNotEmpty() {
		notEmpty("update_time");
		return this;
	}
      public BpmsServiceFlowSQL andUpdateTimeEqualTo(java.util.Date value) {
          addCriterion("update_time", value, ConditionMode.EQUAL, "updateTime", "java.util.Date", "String");
          return this;
      }

      public BpmsServiceFlowSQL andUpdateTimeNotEqualTo(java.util.Date value) {
          addCriterion("update_time", value, ConditionMode.NOT_EQUAL, "updateTime", "java.util.Date", "String");
          return this;
      }

      public BpmsServiceFlowSQL andUpdateTimeGreaterThan(java.util.Date value) {
          addCriterion("update_time", value, ConditionMode.GREATER_THEN, "updateTime", "java.util.Date", "String");
          return this;
      }

      public BpmsServiceFlowSQL andUpdateTimeGreaterThanOrEqualTo(java.util.Date value) {
          addCriterion("update_time", value, ConditionMode.GREATER_EQUAL, "updateTime", "java.util.Date", "String");
          return this;
      }

      public BpmsServiceFlowSQL andUpdateTimeLessThan(java.util.Date value) {
          addCriterion("update_time", value, ConditionMode.LESS_THEN, "updateTime", "java.util.Date", "String");
          return this;
      }

      public BpmsServiceFlowSQL andUpdateTimeLessThanOrEqualTo(java.util.Date value) {
          addCriterion("update_time", value, ConditionMode.LESS_EQUAL, "updateTime", "java.util.Date", "String");
          return this;
      }

      public BpmsServiceFlowSQL andUpdateTimeBetween(java.util.Date value1, java.util.Date value2) {
    	  addCriterion("update_time", value1, value2, ConditionMode.BETWEEN, "updateTime", "java.util.Date", "String");
    	  return this;
      }

      public BpmsServiceFlowSQL andUpdateTimeNotBetween(java.util.Date value1, java.util.Date value2) {
          addCriterion("update_time", value1, value2, ConditionMode.NOT_BETWEEN, "updateTime", "java.util.Date", "String");
          return this;
      }
        
      public BpmsServiceFlowSQL andUpdateTimeIn(List<java.util.Date> values) {
          addCriterion("update_time", values, ConditionMode.IN, "updateTime", "java.util.Date", "String");
          return this;
      }

      public BpmsServiceFlowSQL andUpdateTimeNotIn(List<java.util.Date> values) {
          addCriterion("update_time", values, ConditionMode.NOT_IN, "updateTime", "java.util.Date", "String");
          return this;
      }
	public BpmsServiceFlowSQL andDelFlagIsNull() {
		isnull("del_flag");
		return this;
	}
	
	public BpmsServiceFlowSQL andDelFlagIsNotNull() {
		notNull("del_flag");
		return this;
	}
	
	public BpmsServiceFlowSQL andDelFlagIsEmpty() {
		empty("del_flag");
		return this;
	}

	public BpmsServiceFlowSQL andDelFlagIsNotEmpty() {
		notEmpty("del_flag");
		return this;
	}
       public BpmsServiceFlowSQL andDelFlagLike(java.lang.String value) {
    	   addCriterion("del_flag", value, ConditionMode.FUZZY, "delFlag", "java.lang.String", "String");
    	   return this;
      }

      public BpmsServiceFlowSQL andDelFlagNotLike(java.lang.String value) {
          addCriterion("del_flag", value, ConditionMode.NOT_FUZZY, "delFlag", "java.lang.String", "String");
          return this;
      }
      public BpmsServiceFlowSQL andDelFlagEqualTo(java.lang.String value) {
          addCriterion("del_flag", value, ConditionMode.EQUAL, "delFlag", "java.lang.String", "String");
          return this;
      }

      public BpmsServiceFlowSQL andDelFlagNotEqualTo(java.lang.String value) {
          addCriterion("del_flag", value, ConditionMode.NOT_EQUAL, "delFlag", "java.lang.String", "String");
          return this;
      }

      public BpmsServiceFlowSQL andDelFlagGreaterThan(java.lang.String value) {
          addCriterion("del_flag", value, ConditionMode.GREATER_THEN, "delFlag", "java.lang.String", "String");
          return this;
      }

      public BpmsServiceFlowSQL andDelFlagGreaterThanOrEqualTo(java.lang.String value) {
          addCriterion("del_flag", value, ConditionMode.GREATER_EQUAL, "delFlag", "java.lang.String", "String");
          return this;
      }

      public BpmsServiceFlowSQL andDelFlagLessThan(java.lang.String value) {
          addCriterion("del_flag", value, ConditionMode.LESS_THEN, "delFlag", "java.lang.String", "String");
          return this;
      }

      public BpmsServiceFlowSQL andDelFlagLessThanOrEqualTo(java.lang.String value) {
          addCriterion("del_flag", value, ConditionMode.LESS_EQUAL, "delFlag", "java.lang.String", "String");
          return this;
      }

      public BpmsServiceFlowSQL andDelFlagBetween(java.lang.String value1, java.lang.String value2) {
    	  addCriterion("del_flag", value1, value2, ConditionMode.BETWEEN, "delFlag", "java.lang.String", "String");
    	  return this;
      }

      public BpmsServiceFlowSQL andDelFlagNotBetween(java.lang.String value1, java.lang.String value2) {
          addCriterion("del_flag", value1, value2, ConditionMode.NOT_BETWEEN, "delFlag", "java.lang.String", "String");
          return this;
      }
        
      public BpmsServiceFlowSQL andDelFlagIn(List<java.lang.String> values) {
          addCriterion("del_flag", values, ConditionMode.IN, "delFlag", "java.lang.String", "String");
          return this;
      }

      public BpmsServiceFlowSQL andDelFlagNotIn(List<java.lang.String> values) {
          addCriterion("del_flag", values, ConditionMode.NOT_IN, "delFlag", "java.lang.String", "String");
          return this;
      }
	public BpmsServiceFlowSQL andUserCodeIsNull() {
		isnull("user_code");
		return this;
	}
	
	public BpmsServiceFlowSQL andUserCodeIsNotNull() {
		notNull("user_code");
		return this;
	}
	
	public BpmsServiceFlowSQL andUserCodeIsEmpty() {
		empty("user_code");
		return this;
	}

	public BpmsServiceFlowSQL andUserCodeIsNotEmpty() {
		notEmpty("user_code");
		return this;
	}
       public BpmsServiceFlowSQL andUserCodeLike(java.lang.String value) {
    	   addCriterion("user_code", value, ConditionMode.FUZZY, "userCode", "java.lang.String", "String");
    	   return this;
      }

      public BpmsServiceFlowSQL andUserCodeNotLike(java.lang.String value) {
          addCriterion("user_code", value, ConditionMode.NOT_FUZZY, "userCode", "java.lang.String", "String");
          return this;
      }
      public BpmsServiceFlowSQL andUserCodeEqualTo(java.lang.String value) {
          addCriterion("user_code", value, ConditionMode.EQUAL, "userCode", "java.lang.String", "String");
          return this;
      }

      public BpmsServiceFlowSQL andUserCodeNotEqualTo(java.lang.String value) {
          addCriterion("user_code", value, ConditionMode.NOT_EQUAL, "userCode", "java.lang.String", "String");
          return this;
      }

      public BpmsServiceFlowSQL andUserCodeGreaterThan(java.lang.String value) {
          addCriterion("user_code", value, ConditionMode.GREATER_THEN, "userCode", "java.lang.String", "String");
          return this;
      }

      public BpmsServiceFlowSQL andUserCodeGreaterThanOrEqualTo(java.lang.String value) {
          addCriterion("user_code", value, ConditionMode.GREATER_EQUAL, "userCode", "java.lang.String", "String");
          return this;
      }

      public BpmsServiceFlowSQL andUserCodeLessThan(java.lang.String value) {
          addCriterion("user_code", value, ConditionMode.LESS_THEN, "userCode", "java.lang.String", "String");
          return this;
      }

      public BpmsServiceFlowSQL andUserCodeLessThanOrEqualTo(java.lang.String value) {
          addCriterion("user_code", value, ConditionMode.LESS_EQUAL, "userCode", "java.lang.String", "String");
          return this;
      }

      public BpmsServiceFlowSQL andUserCodeBetween(java.lang.String value1, java.lang.String value2) {
    	  addCriterion("user_code", value1, value2, ConditionMode.BETWEEN, "userCode", "java.lang.String", "String");
    	  return this;
      }

      public BpmsServiceFlowSQL andUserCodeNotBetween(java.lang.String value1, java.lang.String value2) {
          addCriterion("user_code", value1, value2, ConditionMode.NOT_BETWEEN, "userCode", "java.lang.String", "String");
          return this;
      }
        
      public BpmsServiceFlowSQL andUserCodeIn(List<java.lang.String> values) {
          addCriterion("user_code", values, ConditionMode.IN, "userCode", "java.lang.String", "String");
          return this;
      }

      public BpmsServiceFlowSQL andUserCodeNotIn(List<java.lang.String> values) {
          addCriterion("user_code", values, ConditionMode.NOT_IN, "userCode", "java.lang.String", "String");
          return this;
      }
}