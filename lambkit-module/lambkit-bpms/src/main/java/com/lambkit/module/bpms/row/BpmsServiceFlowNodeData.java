/**
 * Copyright (c) 2015-2017, Henry Yang 杨勇 (gismail@foxmail.com).
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.lambkit.module.bpms.row;

import com.lambkit.db.RowModel;

/**
 * @author yangyong 
 * @website: www.lambkit.com
 * @email: gismail@foxmail.com
 * @date ${date}
 * @version 1.0
 * @since 1.0
 */

public class BpmsServiceFlowNodeData extends RowModel<BpmsServiceFlowNodeData> {
	public BpmsServiceFlowNodeData() {
		setTableName("bpms_service_flow_node_data");
		setPrimaryKey("node_data_id");
	}
	public java.lang.Long getNodeDataId() {
		return this.get("node_data_id");
	}
	public void setNodeDataId(java.lang.Long nodeDataId) {
		this.set("node_data_id", nodeDataId);
	}
	public java.lang.String getFlowCode() {
		return this.get("flow_code");
	}
	public void setFlowCode(java.lang.String flowCode) {
		this.set("flow_code", flowCode);
	}
	public java.lang.String getNodeCode() {
		return this.get("node_code");
	}
	public void setNodeCode(java.lang.String nodeCode) {
		this.set("node_code", nodeCode);
	}
	public java.lang.String getDataName() {
		return this.get("data_name");
	}
	public void setDataName(java.lang.String dataName) {
		this.set("data_name", dataName);
	}
	public java.lang.Integer getDataType() {
		return this.get("data_type");
	}
	public void setDataType(java.lang.Integer dataType) {
		this.set("data_type", dataType);
	}
	public java.lang.String getDataValue() {
		return this.get("data_value");
	}
	public void setDataValue(java.lang.String dataValue) {
		this.set("data_value", dataValue);
	}
	public java.lang.String getCreateUser() {
		return this.get("create_user");
	}
	public void setCreateUser(java.lang.String createUser) {
		this.set("create_user", createUser);
	}
	public java.util.Date getCreateTime() {
		return this.get("create_time");
	}
	public void setCreateTime(java.util.Date createTime) {
		this.set("create_time", createTime);
	}
	public java.util.Date getUpdateTime() {
		return this.get("update_time");
	}
	public void setUpdateTime(java.util.Date updateTime) {
		this.set("update_time", updateTime);
	}
	public java.lang.String getDelFlag() {
		return this.get("del_flag");
	}
	public void setDelFlag(java.lang.String delFlag) {
		this.set("del_flag", delFlag);
	}
	public java.lang.String getUserCode() {
		return this.get("user_code");
	}
	public void setUserCode(java.lang.String userCode) {
		this.set("user_code", userCode);
	}
}
