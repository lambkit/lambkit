/**
 * Copyright (c) 2015-2017, Henry Yang 杨勇 (gismail@foxmail.com).
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.lambkit.module.bpms.sql;

import com.lambkit.db.sql.ConditionMode;
import com.lambkit.db.sql.Column;
import com.lambkit.db.sql.Columns;
import com.lambkit.db.sql.Example;

import java.io.Serializable;
import java.util.List;

/**
 * @author yangyong 
 * @website: www.lambkit.com
 * @email: gismail@foxmail.com
 * @date ${date}
 * @version 1.0
 * @since 1.0
 */

public class BpmsServiceFlowNodeSkipSQL extends Columns implements Serializable {
	private static final long serialVersionUID = 1L;
	
	public static BpmsServiceFlowNodeSkipSQL of() {
		return new BpmsServiceFlowNodeSkipSQL();
	}
	
	public static BpmsServiceFlowNodeSkipSQL by(Column column) {
		BpmsServiceFlowNodeSkipSQL that = new BpmsServiceFlowNodeSkipSQL();
		that.add(column);
        return that;
    }

    public static BpmsServiceFlowNodeSkipSQL by(String name, Object value) {
        return (BpmsServiceFlowNodeSkipSQL) create().eq(name, value);
    }
    
    public Example example() {
    	return Example.create("bpms_service_flow_node_skip", this);
    }
    
    /**
     * equals
     *
     * @param name
     * @param value
     * @return
     */
    public BpmsServiceFlowNodeSkipSQL eq(String name, Object value) {
    	super.eq(name, value);
        return this;
    }

    /**
     * not equals !=
     *
     * @param name
     * @param value
     * @return
     */
    public BpmsServiceFlowNodeSkipSQL ne(String name, Object value) {
    	super.ne(name, value);
        return this;
    }


    /**
     * like
     *
     * @param name
     * @param value
     * @return
     */

    public BpmsServiceFlowNodeSkipSQL like(String name, Object value) {
    	super.like(name, value);
        return this;
    }
    
    public BpmsServiceFlowNodeSkipSQL notLike(String name, Object value) {
    	super.notLike(name, value);
        return this;
    }

    /**
     * 大于 great than
     *
     * @param name
     * @param value
     * @return
     */
    public BpmsServiceFlowNodeSkipSQL gt(String name, Object value) {
    	super.gt(name, value);
        return this;
    }

    /**
     * 大于等于 great or equal
     *
     * @param name
     * @param value
     * @return
     */
    public BpmsServiceFlowNodeSkipSQL ge(String name, Object value) {
    	super.ge(name, value);
        return this;
    }

    /**
     * 小于 less than
     *
     * @param name
     * @param value
     * @return
     */
    public BpmsServiceFlowNodeSkipSQL lt(String name, Object value) {
    	super.lt(name, value);
        return this;
    }

    /**
     * 小于等于 less or equal
     *
     * @param name
     * @param value
     * @return
     */
    public BpmsServiceFlowNodeSkipSQL le(String name, Object value) {
    	super.le(name, value);
        return this;
    }
    
    public BpmsServiceFlowNodeSkipSQL isnull(String name) {
    	super.isnull(name);
        return this;
    } 

    public BpmsServiceFlowNodeSkipSQL notNull(String name) {
    	super.notNull(name);
        return this;
    } 
    
    public BpmsServiceFlowNodeSkipSQL empty(String name) {
    	super.empty(name);
        return this;
    } 
    
    public BpmsServiceFlowNodeSkipSQL notEmpty(String name) {
    	super.notEmpty(name);
        return this;
    } 
    
    public BpmsServiceFlowNodeSkipSQL add(Column column) {
    	super.add(column);
    	return this;
    }
    
    /**************************/
	
	public void addCriterion(String name, Object value, ConditionMode logic, String property, String typeHandler, String valueType) {
		 if (value == null) {
			 throw new RuntimeException("Value for " + property + " cannot be null");
		 }
		 add(Column.create(name, value, logic, typeHandler, valueType));
	}
   
	public void addCriterion(String name, Object value1, Object value2, ConditionMode logic, String property, String typeHandler, String valueType) {
		 if (value1 == null || value2 == null) {
			 throw new RuntimeException("Between values for " + property + " cannot be null");
		 }
		 add(Column.create(name, value1, value2, logic, typeHandler, valueType));
	}
		 
	public BpmsServiceFlowNodeSkipSQL andNodeSkipIdIsNull() {
		isnull("node_skip_id");
		return this;
	}
	
	public BpmsServiceFlowNodeSkipSQL andNodeSkipIdIsNotNull() {
		notNull("node_skip_id");
		return this;
	}
	
	public BpmsServiceFlowNodeSkipSQL andNodeSkipIdIsEmpty() {
		empty("node_skip_id");
		return this;
	}

	public BpmsServiceFlowNodeSkipSQL andNodeSkipIdIsNotEmpty() {
		notEmpty("node_skip_id");
		return this;
	}
      public BpmsServiceFlowNodeSkipSQL andNodeSkipIdEqualTo(java.lang.Long value) {
          addCriterion("node_skip_id", value, ConditionMode.EQUAL, "nodeSkipId", "java.lang.Long", "Float");
          return this;
      }

      public BpmsServiceFlowNodeSkipSQL andNodeSkipIdNotEqualTo(java.lang.Long value) {
          addCriterion("node_skip_id", value, ConditionMode.NOT_EQUAL, "nodeSkipId", "java.lang.Long", "Float");
          return this;
      }

      public BpmsServiceFlowNodeSkipSQL andNodeSkipIdGreaterThan(java.lang.Long value) {
          addCriterion("node_skip_id", value, ConditionMode.GREATER_THEN, "nodeSkipId", "java.lang.Long", "Float");
          return this;
      }

      public BpmsServiceFlowNodeSkipSQL andNodeSkipIdGreaterThanOrEqualTo(java.lang.Long value) {
          addCriterion("node_skip_id", value, ConditionMode.GREATER_EQUAL, "nodeSkipId", "java.lang.Long", "Float");
          return this;
      }

      public BpmsServiceFlowNodeSkipSQL andNodeSkipIdLessThan(java.lang.Long value) {
          addCriterion("node_skip_id", value, ConditionMode.LESS_THEN, "nodeSkipId", "java.lang.Long", "Float");
          return this;
      }

      public BpmsServiceFlowNodeSkipSQL andNodeSkipIdLessThanOrEqualTo(java.lang.Long value) {
          addCriterion("node_skip_id", value, ConditionMode.LESS_EQUAL, "nodeSkipId", "java.lang.Long", "Float");
          return this;
      }

      public BpmsServiceFlowNodeSkipSQL andNodeSkipIdBetween(java.lang.Long value1, java.lang.Long value2) {
    	  addCriterion("node_skip_id", value1, value2, ConditionMode.BETWEEN, "nodeSkipId", "java.lang.Long", "Float");
    	  return this;
      }

      public BpmsServiceFlowNodeSkipSQL andNodeSkipIdNotBetween(java.lang.Long value1, java.lang.Long value2) {
          addCriterion("node_skip_id", value1, value2, ConditionMode.NOT_BETWEEN, "nodeSkipId", "java.lang.Long", "Float");
          return this;
      }
        
      public BpmsServiceFlowNodeSkipSQL andNodeSkipIdIn(List<java.lang.Long> values) {
          addCriterion("node_skip_id", values, ConditionMode.IN, "nodeSkipId", "java.lang.Long", "Float");
          return this;
      }

      public BpmsServiceFlowNodeSkipSQL andNodeSkipIdNotIn(List<java.lang.Long> values) {
          addCriterion("node_skip_id", values, ConditionMode.NOT_IN, "nodeSkipId", "java.lang.Long", "Float");
          return this;
      }
	public BpmsServiceFlowNodeSkipSQL andFlowCodeIsNull() {
		isnull("flow_code");
		return this;
	}
	
	public BpmsServiceFlowNodeSkipSQL andFlowCodeIsNotNull() {
		notNull("flow_code");
		return this;
	}
	
	public BpmsServiceFlowNodeSkipSQL andFlowCodeIsEmpty() {
		empty("flow_code");
		return this;
	}

	public BpmsServiceFlowNodeSkipSQL andFlowCodeIsNotEmpty() {
		notEmpty("flow_code");
		return this;
	}
       public BpmsServiceFlowNodeSkipSQL andFlowCodeLike(java.lang.String value) {
    	   addCriterion("flow_code", value, ConditionMode.FUZZY, "flowCode", "java.lang.String", "Float");
    	   return this;
      }

      public BpmsServiceFlowNodeSkipSQL andFlowCodeNotLike(java.lang.String value) {
          addCriterion("flow_code", value, ConditionMode.NOT_FUZZY, "flowCode", "java.lang.String", "Float");
          return this;
      }
      public BpmsServiceFlowNodeSkipSQL andFlowCodeEqualTo(java.lang.String value) {
          addCriterion("flow_code", value, ConditionMode.EQUAL, "flowCode", "java.lang.String", "String");
          return this;
      }

      public BpmsServiceFlowNodeSkipSQL andFlowCodeNotEqualTo(java.lang.String value) {
          addCriterion("flow_code", value, ConditionMode.NOT_EQUAL, "flowCode", "java.lang.String", "String");
          return this;
      }

      public BpmsServiceFlowNodeSkipSQL andFlowCodeGreaterThan(java.lang.String value) {
          addCriterion("flow_code", value, ConditionMode.GREATER_THEN, "flowCode", "java.lang.String", "String");
          return this;
      }

      public BpmsServiceFlowNodeSkipSQL andFlowCodeGreaterThanOrEqualTo(java.lang.String value) {
          addCriterion("flow_code", value, ConditionMode.GREATER_EQUAL, "flowCode", "java.lang.String", "String");
          return this;
      }

      public BpmsServiceFlowNodeSkipSQL andFlowCodeLessThan(java.lang.String value) {
          addCriterion("flow_code", value, ConditionMode.LESS_THEN, "flowCode", "java.lang.String", "String");
          return this;
      }

      public BpmsServiceFlowNodeSkipSQL andFlowCodeLessThanOrEqualTo(java.lang.String value) {
          addCriterion("flow_code", value, ConditionMode.LESS_EQUAL, "flowCode", "java.lang.String", "String");
          return this;
      }

      public BpmsServiceFlowNodeSkipSQL andFlowCodeBetween(java.lang.String value1, java.lang.String value2) {
    	  addCriterion("flow_code", value1, value2, ConditionMode.BETWEEN, "flowCode", "java.lang.String", "String");
    	  return this;
      }

      public BpmsServiceFlowNodeSkipSQL andFlowCodeNotBetween(java.lang.String value1, java.lang.String value2) {
          addCriterion("flow_code", value1, value2, ConditionMode.NOT_BETWEEN, "flowCode", "java.lang.String", "String");
          return this;
      }
        
      public BpmsServiceFlowNodeSkipSQL andFlowCodeIn(List<java.lang.String> values) {
          addCriterion("flow_code", values, ConditionMode.IN, "flowCode", "java.lang.String", "String");
          return this;
      }

      public BpmsServiceFlowNodeSkipSQL andFlowCodeNotIn(List<java.lang.String> values) {
          addCriterion("flow_code", values, ConditionMode.NOT_IN, "flowCode", "java.lang.String", "String");
          return this;
      }
	public BpmsServiceFlowNodeSkipSQL andNowNodeCodeIsNull() {
		isnull("now_node_code");
		return this;
	}
	
	public BpmsServiceFlowNodeSkipSQL andNowNodeCodeIsNotNull() {
		notNull("now_node_code");
		return this;
	}
	
	public BpmsServiceFlowNodeSkipSQL andNowNodeCodeIsEmpty() {
		empty("now_node_code");
		return this;
	}

	public BpmsServiceFlowNodeSkipSQL andNowNodeCodeIsNotEmpty() {
		notEmpty("now_node_code");
		return this;
	}
       public BpmsServiceFlowNodeSkipSQL andNowNodeCodeLike(java.lang.String value) {
    	   addCriterion("now_node_code", value, ConditionMode.FUZZY, "nowNodeCode", "java.lang.String", "String");
    	   return this;
      }

      public BpmsServiceFlowNodeSkipSQL andNowNodeCodeNotLike(java.lang.String value) {
          addCriterion("now_node_code", value, ConditionMode.NOT_FUZZY, "nowNodeCode", "java.lang.String", "String");
          return this;
      }
      public BpmsServiceFlowNodeSkipSQL andNowNodeCodeEqualTo(java.lang.String value) {
          addCriterion("now_node_code", value, ConditionMode.EQUAL, "nowNodeCode", "java.lang.String", "String");
          return this;
      }

      public BpmsServiceFlowNodeSkipSQL andNowNodeCodeNotEqualTo(java.lang.String value) {
          addCriterion("now_node_code", value, ConditionMode.NOT_EQUAL, "nowNodeCode", "java.lang.String", "String");
          return this;
      }

      public BpmsServiceFlowNodeSkipSQL andNowNodeCodeGreaterThan(java.lang.String value) {
          addCriterion("now_node_code", value, ConditionMode.GREATER_THEN, "nowNodeCode", "java.lang.String", "String");
          return this;
      }

      public BpmsServiceFlowNodeSkipSQL andNowNodeCodeGreaterThanOrEqualTo(java.lang.String value) {
          addCriterion("now_node_code", value, ConditionMode.GREATER_EQUAL, "nowNodeCode", "java.lang.String", "String");
          return this;
      }

      public BpmsServiceFlowNodeSkipSQL andNowNodeCodeLessThan(java.lang.String value) {
          addCriterion("now_node_code", value, ConditionMode.LESS_THEN, "nowNodeCode", "java.lang.String", "String");
          return this;
      }

      public BpmsServiceFlowNodeSkipSQL andNowNodeCodeLessThanOrEqualTo(java.lang.String value) {
          addCriterion("now_node_code", value, ConditionMode.LESS_EQUAL, "nowNodeCode", "java.lang.String", "String");
          return this;
      }

      public BpmsServiceFlowNodeSkipSQL andNowNodeCodeBetween(java.lang.String value1, java.lang.String value2) {
    	  addCriterion("now_node_code", value1, value2, ConditionMode.BETWEEN, "nowNodeCode", "java.lang.String", "String");
    	  return this;
      }

      public BpmsServiceFlowNodeSkipSQL andNowNodeCodeNotBetween(java.lang.String value1, java.lang.String value2) {
          addCriterion("now_node_code", value1, value2, ConditionMode.NOT_BETWEEN, "nowNodeCode", "java.lang.String", "String");
          return this;
      }
        
      public BpmsServiceFlowNodeSkipSQL andNowNodeCodeIn(List<java.lang.String> values) {
          addCriterion("now_node_code", values, ConditionMode.IN, "nowNodeCode", "java.lang.String", "String");
          return this;
      }

      public BpmsServiceFlowNodeSkipSQL andNowNodeCodeNotIn(List<java.lang.String> values) {
          addCriterion("now_node_code", values, ConditionMode.NOT_IN, "nowNodeCode", "java.lang.String", "String");
          return this;
      }
	public BpmsServiceFlowNodeSkipSQL andNowNodeTypeIsNull() {
		isnull("now_node_type");
		return this;
	}
	
	public BpmsServiceFlowNodeSkipSQL andNowNodeTypeIsNotNull() {
		notNull("now_node_type");
		return this;
	}
	
	public BpmsServiceFlowNodeSkipSQL andNowNodeTypeIsEmpty() {
		empty("now_node_type");
		return this;
	}

	public BpmsServiceFlowNodeSkipSQL andNowNodeTypeIsNotEmpty() {
		notEmpty("now_node_type");
		return this;
	}
      public BpmsServiceFlowNodeSkipSQL andNowNodeTypeEqualTo(java.lang.Integer value) {
          addCriterion("now_node_type", value, ConditionMode.EQUAL, "nowNodeType", "java.lang.Integer", "Float");
          return this;
      }

      public BpmsServiceFlowNodeSkipSQL andNowNodeTypeNotEqualTo(java.lang.Integer value) {
          addCriterion("now_node_type", value, ConditionMode.NOT_EQUAL, "nowNodeType", "java.lang.Integer", "Float");
          return this;
      }

      public BpmsServiceFlowNodeSkipSQL andNowNodeTypeGreaterThan(java.lang.Integer value) {
          addCriterion("now_node_type", value, ConditionMode.GREATER_THEN, "nowNodeType", "java.lang.Integer", "Float");
          return this;
      }

      public BpmsServiceFlowNodeSkipSQL andNowNodeTypeGreaterThanOrEqualTo(java.lang.Integer value) {
          addCriterion("now_node_type", value, ConditionMode.GREATER_EQUAL, "nowNodeType", "java.lang.Integer", "Float");
          return this;
      }

      public BpmsServiceFlowNodeSkipSQL andNowNodeTypeLessThan(java.lang.Integer value) {
          addCriterion("now_node_type", value, ConditionMode.LESS_THEN, "nowNodeType", "java.lang.Integer", "Float");
          return this;
      }

      public BpmsServiceFlowNodeSkipSQL andNowNodeTypeLessThanOrEqualTo(java.lang.Integer value) {
          addCriterion("now_node_type", value, ConditionMode.LESS_EQUAL, "nowNodeType", "java.lang.Integer", "Float");
          return this;
      }

      public BpmsServiceFlowNodeSkipSQL andNowNodeTypeBetween(java.lang.Integer value1, java.lang.Integer value2) {
    	  addCriterion("now_node_type", value1, value2, ConditionMode.BETWEEN, "nowNodeType", "java.lang.Integer", "Float");
    	  return this;
      }

      public BpmsServiceFlowNodeSkipSQL andNowNodeTypeNotBetween(java.lang.Integer value1, java.lang.Integer value2) {
          addCriterion("now_node_type", value1, value2, ConditionMode.NOT_BETWEEN, "nowNodeType", "java.lang.Integer", "Float");
          return this;
      }
        
      public BpmsServiceFlowNodeSkipSQL andNowNodeTypeIn(List<java.lang.Integer> values) {
          addCriterion("now_node_type", values, ConditionMode.IN, "nowNodeType", "java.lang.Integer", "Float");
          return this;
      }

      public BpmsServiceFlowNodeSkipSQL andNowNodeTypeNotIn(List<java.lang.Integer> values) {
          addCriterion("now_node_type", values, ConditionMode.NOT_IN, "nowNodeType", "java.lang.Integer", "Float");
          return this;
      }
	public BpmsServiceFlowNodeSkipSQL andNextNodeCodeIsNull() {
		isnull("next_node_code");
		return this;
	}
	
	public BpmsServiceFlowNodeSkipSQL andNextNodeCodeIsNotNull() {
		notNull("next_node_code");
		return this;
	}
	
	public BpmsServiceFlowNodeSkipSQL andNextNodeCodeIsEmpty() {
		empty("next_node_code");
		return this;
	}

	public BpmsServiceFlowNodeSkipSQL andNextNodeCodeIsNotEmpty() {
		notEmpty("next_node_code");
		return this;
	}
       public BpmsServiceFlowNodeSkipSQL andNextNodeCodeLike(java.lang.String value) {
    	   addCriterion("next_node_code", value, ConditionMode.FUZZY, "nextNodeCode", "java.lang.String", "Float");
    	   return this;
      }

      public BpmsServiceFlowNodeSkipSQL andNextNodeCodeNotLike(java.lang.String value) {
          addCriterion("next_node_code", value, ConditionMode.NOT_FUZZY, "nextNodeCode", "java.lang.String", "Float");
          return this;
      }
      public BpmsServiceFlowNodeSkipSQL andNextNodeCodeEqualTo(java.lang.String value) {
          addCriterion("next_node_code", value, ConditionMode.EQUAL, "nextNodeCode", "java.lang.String", "String");
          return this;
      }

      public BpmsServiceFlowNodeSkipSQL andNextNodeCodeNotEqualTo(java.lang.String value) {
          addCriterion("next_node_code", value, ConditionMode.NOT_EQUAL, "nextNodeCode", "java.lang.String", "String");
          return this;
      }

      public BpmsServiceFlowNodeSkipSQL andNextNodeCodeGreaterThan(java.lang.String value) {
          addCriterion("next_node_code", value, ConditionMode.GREATER_THEN, "nextNodeCode", "java.lang.String", "String");
          return this;
      }

      public BpmsServiceFlowNodeSkipSQL andNextNodeCodeGreaterThanOrEqualTo(java.lang.String value) {
          addCriterion("next_node_code", value, ConditionMode.GREATER_EQUAL, "nextNodeCode", "java.lang.String", "String");
          return this;
      }

      public BpmsServiceFlowNodeSkipSQL andNextNodeCodeLessThan(java.lang.String value) {
          addCriterion("next_node_code", value, ConditionMode.LESS_THEN, "nextNodeCode", "java.lang.String", "String");
          return this;
      }

      public BpmsServiceFlowNodeSkipSQL andNextNodeCodeLessThanOrEqualTo(java.lang.String value) {
          addCriterion("next_node_code", value, ConditionMode.LESS_EQUAL, "nextNodeCode", "java.lang.String", "String");
          return this;
      }

      public BpmsServiceFlowNodeSkipSQL andNextNodeCodeBetween(java.lang.String value1, java.lang.String value2) {
    	  addCriterion("next_node_code", value1, value2, ConditionMode.BETWEEN, "nextNodeCode", "java.lang.String", "String");
    	  return this;
      }

      public BpmsServiceFlowNodeSkipSQL andNextNodeCodeNotBetween(java.lang.String value1, java.lang.String value2) {
          addCriterion("next_node_code", value1, value2, ConditionMode.NOT_BETWEEN, "nextNodeCode", "java.lang.String", "String");
          return this;
      }
        
      public BpmsServiceFlowNodeSkipSQL andNextNodeCodeIn(List<java.lang.String> values) {
          addCriterion("next_node_code", values, ConditionMode.IN, "nextNodeCode", "java.lang.String", "String");
          return this;
      }

      public BpmsServiceFlowNodeSkipSQL andNextNodeCodeNotIn(List<java.lang.String> values) {
          addCriterion("next_node_code", values, ConditionMode.NOT_IN, "nextNodeCode", "java.lang.String", "String");
          return this;
      }
	public BpmsServiceFlowNodeSkipSQL andNextNodeTypeIsNull() {
		isnull("next_node_type");
		return this;
	}
	
	public BpmsServiceFlowNodeSkipSQL andNextNodeTypeIsNotNull() {
		notNull("next_node_type");
		return this;
	}
	
	public BpmsServiceFlowNodeSkipSQL andNextNodeTypeIsEmpty() {
		empty("next_node_type");
		return this;
	}

	public BpmsServiceFlowNodeSkipSQL andNextNodeTypeIsNotEmpty() {
		notEmpty("next_node_type");
		return this;
	}
      public BpmsServiceFlowNodeSkipSQL andNextNodeTypeEqualTo(java.lang.Integer value) {
          addCriterion("next_node_type", value, ConditionMode.EQUAL, "nextNodeType", "java.lang.Integer", "Float");
          return this;
      }

      public BpmsServiceFlowNodeSkipSQL andNextNodeTypeNotEqualTo(java.lang.Integer value) {
          addCriterion("next_node_type", value, ConditionMode.NOT_EQUAL, "nextNodeType", "java.lang.Integer", "Float");
          return this;
      }

      public BpmsServiceFlowNodeSkipSQL andNextNodeTypeGreaterThan(java.lang.Integer value) {
          addCriterion("next_node_type", value, ConditionMode.GREATER_THEN, "nextNodeType", "java.lang.Integer", "Float");
          return this;
      }

      public BpmsServiceFlowNodeSkipSQL andNextNodeTypeGreaterThanOrEqualTo(java.lang.Integer value) {
          addCriterion("next_node_type", value, ConditionMode.GREATER_EQUAL, "nextNodeType", "java.lang.Integer", "Float");
          return this;
      }

      public BpmsServiceFlowNodeSkipSQL andNextNodeTypeLessThan(java.lang.Integer value) {
          addCriterion("next_node_type", value, ConditionMode.LESS_THEN, "nextNodeType", "java.lang.Integer", "Float");
          return this;
      }

      public BpmsServiceFlowNodeSkipSQL andNextNodeTypeLessThanOrEqualTo(java.lang.Integer value) {
          addCriterion("next_node_type", value, ConditionMode.LESS_EQUAL, "nextNodeType", "java.lang.Integer", "Float");
          return this;
      }

      public BpmsServiceFlowNodeSkipSQL andNextNodeTypeBetween(java.lang.Integer value1, java.lang.Integer value2) {
    	  addCriterion("next_node_type", value1, value2, ConditionMode.BETWEEN, "nextNodeType", "java.lang.Integer", "Float");
    	  return this;
      }

      public BpmsServiceFlowNodeSkipSQL andNextNodeTypeNotBetween(java.lang.Integer value1, java.lang.Integer value2) {
          addCriterion("next_node_type", value1, value2, ConditionMode.NOT_BETWEEN, "nextNodeType", "java.lang.Integer", "Float");
          return this;
      }
        
      public BpmsServiceFlowNodeSkipSQL andNextNodeTypeIn(List<java.lang.Integer> values) {
          addCriterion("next_node_type", values, ConditionMode.IN, "nextNodeType", "java.lang.Integer", "Float");
          return this;
      }

      public BpmsServiceFlowNodeSkipSQL andNextNodeTypeNotIn(List<java.lang.Integer> values) {
          addCriterion("next_node_type", values, ConditionMode.NOT_IN, "nextNodeType", "java.lang.Integer", "Float");
          return this;
      }
	public BpmsServiceFlowNodeSkipSQL andSkipNameIsNull() {
		isnull("skip_name");
		return this;
	}
	
	public BpmsServiceFlowNodeSkipSQL andSkipNameIsNotNull() {
		notNull("skip_name");
		return this;
	}
	
	public BpmsServiceFlowNodeSkipSQL andSkipNameIsEmpty() {
		empty("skip_name");
		return this;
	}

	public BpmsServiceFlowNodeSkipSQL andSkipNameIsNotEmpty() {
		notEmpty("skip_name");
		return this;
	}
       public BpmsServiceFlowNodeSkipSQL andSkipNameLike(java.lang.String value) {
    	   addCriterion("skip_name", value, ConditionMode.FUZZY, "skipName", "java.lang.String", "Float");
    	   return this;
      }

      public BpmsServiceFlowNodeSkipSQL andSkipNameNotLike(java.lang.String value) {
          addCriterion("skip_name", value, ConditionMode.NOT_FUZZY, "skipName", "java.lang.String", "Float");
          return this;
      }
      public BpmsServiceFlowNodeSkipSQL andSkipNameEqualTo(java.lang.String value) {
          addCriterion("skip_name", value, ConditionMode.EQUAL, "skipName", "java.lang.String", "String");
          return this;
      }

      public BpmsServiceFlowNodeSkipSQL andSkipNameNotEqualTo(java.lang.String value) {
          addCriterion("skip_name", value, ConditionMode.NOT_EQUAL, "skipName", "java.lang.String", "String");
          return this;
      }

      public BpmsServiceFlowNodeSkipSQL andSkipNameGreaterThan(java.lang.String value) {
          addCriterion("skip_name", value, ConditionMode.GREATER_THEN, "skipName", "java.lang.String", "String");
          return this;
      }

      public BpmsServiceFlowNodeSkipSQL andSkipNameGreaterThanOrEqualTo(java.lang.String value) {
          addCriterion("skip_name", value, ConditionMode.GREATER_EQUAL, "skipName", "java.lang.String", "String");
          return this;
      }

      public BpmsServiceFlowNodeSkipSQL andSkipNameLessThan(java.lang.String value) {
          addCriterion("skip_name", value, ConditionMode.LESS_THEN, "skipName", "java.lang.String", "String");
          return this;
      }

      public BpmsServiceFlowNodeSkipSQL andSkipNameLessThanOrEqualTo(java.lang.String value) {
          addCriterion("skip_name", value, ConditionMode.LESS_EQUAL, "skipName", "java.lang.String", "String");
          return this;
      }

      public BpmsServiceFlowNodeSkipSQL andSkipNameBetween(java.lang.String value1, java.lang.String value2) {
    	  addCriterion("skip_name", value1, value2, ConditionMode.BETWEEN, "skipName", "java.lang.String", "String");
    	  return this;
      }

      public BpmsServiceFlowNodeSkipSQL andSkipNameNotBetween(java.lang.String value1, java.lang.String value2) {
          addCriterion("skip_name", value1, value2, ConditionMode.NOT_BETWEEN, "skipName", "java.lang.String", "String");
          return this;
      }
        
      public BpmsServiceFlowNodeSkipSQL andSkipNameIn(List<java.lang.String> values) {
          addCriterion("skip_name", values, ConditionMode.IN, "skipName", "java.lang.String", "String");
          return this;
      }

      public BpmsServiceFlowNodeSkipSQL andSkipNameNotIn(List<java.lang.String> values) {
          addCriterion("skip_name", values, ConditionMode.NOT_IN, "skipName", "java.lang.String", "String");
          return this;
      }
	public BpmsServiceFlowNodeSkipSQL andSkipTypeIsNull() {
		isnull("skip_type");
		return this;
	}
	
	public BpmsServiceFlowNodeSkipSQL andSkipTypeIsNotNull() {
		notNull("skip_type");
		return this;
	}
	
	public BpmsServiceFlowNodeSkipSQL andSkipTypeIsEmpty() {
		empty("skip_type");
		return this;
	}

	public BpmsServiceFlowNodeSkipSQL andSkipTypeIsNotEmpty() {
		notEmpty("skip_type");
		return this;
	}
       public BpmsServiceFlowNodeSkipSQL andSkipTypeLike(java.lang.String value) {
    	   addCriterion("skip_type", value, ConditionMode.FUZZY, "skipType", "java.lang.String", "String");
    	   return this;
      }

      public BpmsServiceFlowNodeSkipSQL andSkipTypeNotLike(java.lang.String value) {
          addCriterion("skip_type", value, ConditionMode.NOT_FUZZY, "skipType", "java.lang.String", "String");
          return this;
      }
      public BpmsServiceFlowNodeSkipSQL andSkipTypeEqualTo(java.lang.String value) {
          addCriterion("skip_type", value, ConditionMode.EQUAL, "skipType", "java.lang.String", "String");
          return this;
      }

      public BpmsServiceFlowNodeSkipSQL andSkipTypeNotEqualTo(java.lang.String value) {
          addCriterion("skip_type", value, ConditionMode.NOT_EQUAL, "skipType", "java.lang.String", "String");
          return this;
      }

      public BpmsServiceFlowNodeSkipSQL andSkipTypeGreaterThan(java.lang.String value) {
          addCriterion("skip_type", value, ConditionMode.GREATER_THEN, "skipType", "java.lang.String", "String");
          return this;
      }

      public BpmsServiceFlowNodeSkipSQL andSkipTypeGreaterThanOrEqualTo(java.lang.String value) {
          addCriterion("skip_type", value, ConditionMode.GREATER_EQUAL, "skipType", "java.lang.String", "String");
          return this;
      }

      public BpmsServiceFlowNodeSkipSQL andSkipTypeLessThan(java.lang.String value) {
          addCriterion("skip_type", value, ConditionMode.LESS_THEN, "skipType", "java.lang.String", "String");
          return this;
      }

      public BpmsServiceFlowNodeSkipSQL andSkipTypeLessThanOrEqualTo(java.lang.String value) {
          addCriterion("skip_type", value, ConditionMode.LESS_EQUAL, "skipType", "java.lang.String", "String");
          return this;
      }

      public BpmsServiceFlowNodeSkipSQL andSkipTypeBetween(java.lang.String value1, java.lang.String value2) {
    	  addCriterion("skip_type", value1, value2, ConditionMode.BETWEEN, "skipType", "java.lang.String", "String");
    	  return this;
      }

      public BpmsServiceFlowNodeSkipSQL andSkipTypeNotBetween(java.lang.String value1, java.lang.String value2) {
          addCriterion("skip_type", value1, value2, ConditionMode.NOT_BETWEEN, "skipType", "java.lang.String", "String");
          return this;
      }
        
      public BpmsServiceFlowNodeSkipSQL andSkipTypeIn(List<java.lang.String> values) {
          addCriterion("skip_type", values, ConditionMode.IN, "skipType", "java.lang.String", "String");
          return this;
      }

      public BpmsServiceFlowNodeSkipSQL andSkipTypeNotIn(List<java.lang.String> values) {
          addCriterion("skip_type", values, ConditionMode.NOT_IN, "skipType", "java.lang.String", "String");
          return this;
      }
	public BpmsServiceFlowNodeSkipSQL andSkipConditionIsNull() {
		isnull("skip_condition");
		return this;
	}
	
	public BpmsServiceFlowNodeSkipSQL andSkipConditionIsNotNull() {
		notNull("skip_condition");
		return this;
	}
	
	public BpmsServiceFlowNodeSkipSQL andSkipConditionIsEmpty() {
		empty("skip_condition");
		return this;
	}

	public BpmsServiceFlowNodeSkipSQL andSkipConditionIsNotEmpty() {
		notEmpty("skip_condition");
		return this;
	}
       public BpmsServiceFlowNodeSkipSQL andSkipConditionLike(java.lang.String value) {
    	   addCriterion("skip_condition", value, ConditionMode.FUZZY, "skipCondition", "java.lang.String", "String");
    	   return this;
      }

      public BpmsServiceFlowNodeSkipSQL andSkipConditionNotLike(java.lang.String value) {
          addCriterion("skip_condition", value, ConditionMode.NOT_FUZZY, "skipCondition", "java.lang.String", "String");
          return this;
      }
      public BpmsServiceFlowNodeSkipSQL andSkipConditionEqualTo(java.lang.String value) {
          addCriterion("skip_condition", value, ConditionMode.EQUAL, "skipCondition", "java.lang.String", "String");
          return this;
      }

      public BpmsServiceFlowNodeSkipSQL andSkipConditionNotEqualTo(java.lang.String value) {
          addCriterion("skip_condition", value, ConditionMode.NOT_EQUAL, "skipCondition", "java.lang.String", "String");
          return this;
      }

      public BpmsServiceFlowNodeSkipSQL andSkipConditionGreaterThan(java.lang.String value) {
          addCriterion("skip_condition", value, ConditionMode.GREATER_THEN, "skipCondition", "java.lang.String", "String");
          return this;
      }

      public BpmsServiceFlowNodeSkipSQL andSkipConditionGreaterThanOrEqualTo(java.lang.String value) {
          addCriterion("skip_condition", value, ConditionMode.GREATER_EQUAL, "skipCondition", "java.lang.String", "String");
          return this;
      }

      public BpmsServiceFlowNodeSkipSQL andSkipConditionLessThan(java.lang.String value) {
          addCriterion("skip_condition", value, ConditionMode.LESS_THEN, "skipCondition", "java.lang.String", "String");
          return this;
      }

      public BpmsServiceFlowNodeSkipSQL andSkipConditionLessThanOrEqualTo(java.lang.String value) {
          addCriterion("skip_condition", value, ConditionMode.LESS_EQUAL, "skipCondition", "java.lang.String", "String");
          return this;
      }

      public BpmsServiceFlowNodeSkipSQL andSkipConditionBetween(java.lang.String value1, java.lang.String value2) {
    	  addCriterion("skip_condition", value1, value2, ConditionMode.BETWEEN, "skipCondition", "java.lang.String", "String");
    	  return this;
      }

      public BpmsServiceFlowNodeSkipSQL andSkipConditionNotBetween(java.lang.String value1, java.lang.String value2) {
          addCriterion("skip_condition", value1, value2, ConditionMode.NOT_BETWEEN, "skipCondition", "java.lang.String", "String");
          return this;
      }
        
      public BpmsServiceFlowNodeSkipSQL andSkipConditionIn(List<java.lang.String> values) {
          addCriterion("skip_condition", values, ConditionMode.IN, "skipCondition", "java.lang.String", "String");
          return this;
      }

      public BpmsServiceFlowNodeSkipSQL andSkipConditionNotIn(List<java.lang.String> values) {
          addCriterion("skip_condition", values, ConditionMode.NOT_IN, "skipCondition", "java.lang.String", "String");
          return this;
      }
	public BpmsServiceFlowNodeSkipSQL andCoordinateIsNull() {
		isnull("coordinate");
		return this;
	}
	
	public BpmsServiceFlowNodeSkipSQL andCoordinateIsNotNull() {
		notNull("coordinate");
		return this;
	}
	
	public BpmsServiceFlowNodeSkipSQL andCoordinateIsEmpty() {
		empty("coordinate");
		return this;
	}

	public BpmsServiceFlowNodeSkipSQL andCoordinateIsNotEmpty() {
		notEmpty("coordinate");
		return this;
	}
       public BpmsServiceFlowNodeSkipSQL andCoordinateLike(java.lang.String value) {
    	   addCriterion("coordinate", value, ConditionMode.FUZZY, "coordinate", "java.lang.String", "String");
    	   return this;
      }

      public BpmsServiceFlowNodeSkipSQL andCoordinateNotLike(java.lang.String value) {
          addCriterion("coordinate", value, ConditionMode.NOT_FUZZY, "coordinate", "java.lang.String", "String");
          return this;
      }
      public BpmsServiceFlowNodeSkipSQL andCoordinateEqualTo(java.lang.String value) {
          addCriterion("coordinate", value, ConditionMode.EQUAL, "coordinate", "java.lang.String", "String");
          return this;
      }

      public BpmsServiceFlowNodeSkipSQL andCoordinateNotEqualTo(java.lang.String value) {
          addCriterion("coordinate", value, ConditionMode.NOT_EQUAL, "coordinate", "java.lang.String", "String");
          return this;
      }

      public BpmsServiceFlowNodeSkipSQL andCoordinateGreaterThan(java.lang.String value) {
          addCriterion("coordinate", value, ConditionMode.GREATER_THEN, "coordinate", "java.lang.String", "String");
          return this;
      }

      public BpmsServiceFlowNodeSkipSQL andCoordinateGreaterThanOrEqualTo(java.lang.String value) {
          addCriterion("coordinate", value, ConditionMode.GREATER_EQUAL, "coordinate", "java.lang.String", "String");
          return this;
      }

      public BpmsServiceFlowNodeSkipSQL andCoordinateLessThan(java.lang.String value) {
          addCriterion("coordinate", value, ConditionMode.LESS_THEN, "coordinate", "java.lang.String", "String");
          return this;
      }

      public BpmsServiceFlowNodeSkipSQL andCoordinateLessThanOrEqualTo(java.lang.String value) {
          addCriterion("coordinate", value, ConditionMode.LESS_EQUAL, "coordinate", "java.lang.String", "String");
          return this;
      }

      public BpmsServiceFlowNodeSkipSQL andCoordinateBetween(java.lang.String value1, java.lang.String value2) {
    	  addCriterion("coordinate", value1, value2, ConditionMode.BETWEEN, "coordinate", "java.lang.String", "String");
    	  return this;
      }

      public BpmsServiceFlowNodeSkipSQL andCoordinateNotBetween(java.lang.String value1, java.lang.String value2) {
          addCriterion("coordinate", value1, value2, ConditionMode.NOT_BETWEEN, "coordinate", "java.lang.String", "String");
          return this;
      }
        
      public BpmsServiceFlowNodeSkipSQL andCoordinateIn(List<java.lang.String> values) {
          addCriterion("coordinate", values, ConditionMode.IN, "coordinate", "java.lang.String", "String");
          return this;
      }

      public BpmsServiceFlowNodeSkipSQL andCoordinateNotIn(List<java.lang.String> values) {
          addCriterion("coordinate", values, ConditionMode.NOT_IN, "coordinate", "java.lang.String", "String");
          return this;
      }
	public BpmsServiceFlowNodeSkipSQL andCreateTimeIsNull() {
		isnull("create_time");
		return this;
	}
	
	public BpmsServiceFlowNodeSkipSQL andCreateTimeIsNotNull() {
		notNull("create_time");
		return this;
	}
	
	public BpmsServiceFlowNodeSkipSQL andCreateTimeIsEmpty() {
		empty("create_time");
		return this;
	}

	public BpmsServiceFlowNodeSkipSQL andCreateTimeIsNotEmpty() {
		notEmpty("create_time");
		return this;
	}
      public BpmsServiceFlowNodeSkipSQL andCreateTimeEqualTo(java.util.Date value) {
          addCriterion("create_time", value, ConditionMode.EQUAL, "createTime", "java.util.Date", "String");
          return this;
      }

      public BpmsServiceFlowNodeSkipSQL andCreateTimeNotEqualTo(java.util.Date value) {
          addCriterion("create_time", value, ConditionMode.NOT_EQUAL, "createTime", "java.util.Date", "String");
          return this;
      }

      public BpmsServiceFlowNodeSkipSQL andCreateTimeGreaterThan(java.util.Date value) {
          addCriterion("create_time", value, ConditionMode.GREATER_THEN, "createTime", "java.util.Date", "String");
          return this;
      }

      public BpmsServiceFlowNodeSkipSQL andCreateTimeGreaterThanOrEqualTo(java.util.Date value) {
          addCriterion("create_time", value, ConditionMode.GREATER_EQUAL, "createTime", "java.util.Date", "String");
          return this;
      }

      public BpmsServiceFlowNodeSkipSQL andCreateTimeLessThan(java.util.Date value) {
          addCriterion("create_time", value, ConditionMode.LESS_THEN, "createTime", "java.util.Date", "String");
          return this;
      }

      public BpmsServiceFlowNodeSkipSQL andCreateTimeLessThanOrEqualTo(java.util.Date value) {
          addCriterion("create_time", value, ConditionMode.LESS_EQUAL, "createTime", "java.util.Date", "String");
          return this;
      }

      public BpmsServiceFlowNodeSkipSQL andCreateTimeBetween(java.util.Date value1, java.util.Date value2) {
    	  addCriterion("create_time", value1, value2, ConditionMode.BETWEEN, "createTime", "java.util.Date", "String");
    	  return this;
      }

      public BpmsServiceFlowNodeSkipSQL andCreateTimeNotBetween(java.util.Date value1, java.util.Date value2) {
          addCriterion("create_time", value1, value2, ConditionMode.NOT_BETWEEN, "createTime", "java.util.Date", "String");
          return this;
      }
        
      public BpmsServiceFlowNodeSkipSQL andCreateTimeIn(List<java.util.Date> values) {
          addCriterion("create_time", values, ConditionMode.IN, "createTime", "java.util.Date", "String");
          return this;
      }

      public BpmsServiceFlowNodeSkipSQL andCreateTimeNotIn(List<java.util.Date> values) {
          addCriterion("create_time", values, ConditionMode.NOT_IN, "createTime", "java.util.Date", "String");
          return this;
      }
	public BpmsServiceFlowNodeSkipSQL andUpdateTimeIsNull() {
		isnull("update_time");
		return this;
	}
	
	public BpmsServiceFlowNodeSkipSQL andUpdateTimeIsNotNull() {
		notNull("update_time");
		return this;
	}
	
	public BpmsServiceFlowNodeSkipSQL andUpdateTimeIsEmpty() {
		empty("update_time");
		return this;
	}

	public BpmsServiceFlowNodeSkipSQL andUpdateTimeIsNotEmpty() {
		notEmpty("update_time");
		return this;
	}
      public BpmsServiceFlowNodeSkipSQL andUpdateTimeEqualTo(java.util.Date value) {
          addCriterion("update_time", value, ConditionMode.EQUAL, "updateTime", "java.util.Date", "String");
          return this;
      }

      public BpmsServiceFlowNodeSkipSQL andUpdateTimeNotEqualTo(java.util.Date value) {
          addCriterion("update_time", value, ConditionMode.NOT_EQUAL, "updateTime", "java.util.Date", "String");
          return this;
      }

      public BpmsServiceFlowNodeSkipSQL andUpdateTimeGreaterThan(java.util.Date value) {
          addCriterion("update_time", value, ConditionMode.GREATER_THEN, "updateTime", "java.util.Date", "String");
          return this;
      }

      public BpmsServiceFlowNodeSkipSQL andUpdateTimeGreaterThanOrEqualTo(java.util.Date value) {
          addCriterion("update_time", value, ConditionMode.GREATER_EQUAL, "updateTime", "java.util.Date", "String");
          return this;
      }

      public BpmsServiceFlowNodeSkipSQL andUpdateTimeLessThan(java.util.Date value) {
          addCriterion("update_time", value, ConditionMode.LESS_THEN, "updateTime", "java.util.Date", "String");
          return this;
      }

      public BpmsServiceFlowNodeSkipSQL andUpdateTimeLessThanOrEqualTo(java.util.Date value) {
          addCriterion("update_time", value, ConditionMode.LESS_EQUAL, "updateTime", "java.util.Date", "String");
          return this;
      }

      public BpmsServiceFlowNodeSkipSQL andUpdateTimeBetween(java.util.Date value1, java.util.Date value2) {
    	  addCriterion("update_time", value1, value2, ConditionMode.BETWEEN, "updateTime", "java.util.Date", "String");
    	  return this;
      }

      public BpmsServiceFlowNodeSkipSQL andUpdateTimeNotBetween(java.util.Date value1, java.util.Date value2) {
          addCriterion("update_time", value1, value2, ConditionMode.NOT_BETWEEN, "updateTime", "java.util.Date", "String");
          return this;
      }
        
      public BpmsServiceFlowNodeSkipSQL andUpdateTimeIn(List<java.util.Date> values) {
          addCriterion("update_time", values, ConditionMode.IN, "updateTime", "java.util.Date", "String");
          return this;
      }

      public BpmsServiceFlowNodeSkipSQL andUpdateTimeNotIn(List<java.util.Date> values) {
          addCriterion("update_time", values, ConditionMode.NOT_IN, "updateTime", "java.util.Date", "String");
          return this;
      }
	public BpmsServiceFlowNodeSkipSQL andDelFlagIsNull() {
		isnull("del_flag");
		return this;
	}
	
	public BpmsServiceFlowNodeSkipSQL andDelFlagIsNotNull() {
		notNull("del_flag");
		return this;
	}
	
	public BpmsServiceFlowNodeSkipSQL andDelFlagIsEmpty() {
		empty("del_flag");
		return this;
	}

	public BpmsServiceFlowNodeSkipSQL andDelFlagIsNotEmpty() {
		notEmpty("del_flag");
		return this;
	}
       public BpmsServiceFlowNodeSkipSQL andDelFlagLike(java.lang.String value) {
    	   addCriterion("del_flag", value, ConditionMode.FUZZY, "delFlag", "java.lang.String", "String");
    	   return this;
      }

      public BpmsServiceFlowNodeSkipSQL andDelFlagNotLike(java.lang.String value) {
          addCriterion("del_flag", value, ConditionMode.NOT_FUZZY, "delFlag", "java.lang.String", "String");
          return this;
      }
      public BpmsServiceFlowNodeSkipSQL andDelFlagEqualTo(java.lang.String value) {
          addCriterion("del_flag", value, ConditionMode.EQUAL, "delFlag", "java.lang.String", "String");
          return this;
      }

      public BpmsServiceFlowNodeSkipSQL andDelFlagNotEqualTo(java.lang.String value) {
          addCriterion("del_flag", value, ConditionMode.NOT_EQUAL, "delFlag", "java.lang.String", "String");
          return this;
      }

      public BpmsServiceFlowNodeSkipSQL andDelFlagGreaterThan(java.lang.String value) {
          addCriterion("del_flag", value, ConditionMode.GREATER_THEN, "delFlag", "java.lang.String", "String");
          return this;
      }

      public BpmsServiceFlowNodeSkipSQL andDelFlagGreaterThanOrEqualTo(java.lang.String value) {
          addCriterion("del_flag", value, ConditionMode.GREATER_EQUAL, "delFlag", "java.lang.String", "String");
          return this;
      }

      public BpmsServiceFlowNodeSkipSQL andDelFlagLessThan(java.lang.String value) {
          addCriterion("del_flag", value, ConditionMode.LESS_THEN, "delFlag", "java.lang.String", "String");
          return this;
      }

      public BpmsServiceFlowNodeSkipSQL andDelFlagLessThanOrEqualTo(java.lang.String value) {
          addCriterion("del_flag", value, ConditionMode.LESS_EQUAL, "delFlag", "java.lang.String", "String");
          return this;
      }

      public BpmsServiceFlowNodeSkipSQL andDelFlagBetween(java.lang.String value1, java.lang.String value2) {
    	  addCriterion("del_flag", value1, value2, ConditionMode.BETWEEN, "delFlag", "java.lang.String", "String");
    	  return this;
      }

      public BpmsServiceFlowNodeSkipSQL andDelFlagNotBetween(java.lang.String value1, java.lang.String value2) {
          addCriterion("del_flag", value1, value2, ConditionMode.NOT_BETWEEN, "delFlag", "java.lang.String", "String");
          return this;
      }
        
      public BpmsServiceFlowNodeSkipSQL andDelFlagIn(List<java.lang.String> values) {
          addCriterion("del_flag", values, ConditionMode.IN, "delFlag", "java.lang.String", "String");
          return this;
      }

      public BpmsServiceFlowNodeSkipSQL andDelFlagNotIn(List<java.lang.String> values) {
          addCriterion("del_flag", values, ConditionMode.NOT_IN, "delFlag", "java.lang.String", "String");
          return this;
      }
	public BpmsServiceFlowNodeSkipSQL andUserCodeIsNull() {
		isnull("user_code");
		return this;
	}
	
	public BpmsServiceFlowNodeSkipSQL andUserCodeIsNotNull() {
		notNull("user_code");
		return this;
	}
	
	public BpmsServiceFlowNodeSkipSQL andUserCodeIsEmpty() {
		empty("user_code");
		return this;
	}

	public BpmsServiceFlowNodeSkipSQL andUserCodeIsNotEmpty() {
		notEmpty("user_code");
		return this;
	}
       public BpmsServiceFlowNodeSkipSQL andUserCodeLike(java.lang.String value) {
    	   addCriterion("user_code", value, ConditionMode.FUZZY, "userCode", "java.lang.String", "String");
    	   return this;
      }

      public BpmsServiceFlowNodeSkipSQL andUserCodeNotLike(java.lang.String value) {
          addCriterion("user_code", value, ConditionMode.NOT_FUZZY, "userCode", "java.lang.String", "String");
          return this;
      }
      public BpmsServiceFlowNodeSkipSQL andUserCodeEqualTo(java.lang.String value) {
          addCriterion("user_code", value, ConditionMode.EQUAL, "userCode", "java.lang.String", "String");
          return this;
      }

      public BpmsServiceFlowNodeSkipSQL andUserCodeNotEqualTo(java.lang.String value) {
          addCriterion("user_code", value, ConditionMode.NOT_EQUAL, "userCode", "java.lang.String", "String");
          return this;
      }

      public BpmsServiceFlowNodeSkipSQL andUserCodeGreaterThan(java.lang.String value) {
          addCriterion("user_code", value, ConditionMode.GREATER_THEN, "userCode", "java.lang.String", "String");
          return this;
      }

      public BpmsServiceFlowNodeSkipSQL andUserCodeGreaterThanOrEqualTo(java.lang.String value) {
          addCriterion("user_code", value, ConditionMode.GREATER_EQUAL, "userCode", "java.lang.String", "String");
          return this;
      }

      public BpmsServiceFlowNodeSkipSQL andUserCodeLessThan(java.lang.String value) {
          addCriterion("user_code", value, ConditionMode.LESS_THEN, "userCode", "java.lang.String", "String");
          return this;
      }

      public BpmsServiceFlowNodeSkipSQL andUserCodeLessThanOrEqualTo(java.lang.String value) {
          addCriterion("user_code", value, ConditionMode.LESS_EQUAL, "userCode", "java.lang.String", "String");
          return this;
      }

      public BpmsServiceFlowNodeSkipSQL andUserCodeBetween(java.lang.String value1, java.lang.String value2) {
    	  addCriterion("user_code", value1, value2, ConditionMode.BETWEEN, "userCode", "java.lang.String", "String");
    	  return this;
      }

      public BpmsServiceFlowNodeSkipSQL andUserCodeNotBetween(java.lang.String value1, java.lang.String value2) {
          addCriterion("user_code", value1, value2, ConditionMode.NOT_BETWEEN, "userCode", "java.lang.String", "String");
          return this;
      }
        
      public BpmsServiceFlowNodeSkipSQL andUserCodeIn(List<java.lang.String> values) {
          addCriterion("user_code", values, ConditionMode.IN, "userCode", "java.lang.String", "String");
          return this;
      }

      public BpmsServiceFlowNodeSkipSQL andUserCodeNotIn(List<java.lang.String> values) {
          addCriterion("user_code", values, ConditionMode.NOT_IN, "userCode", "java.lang.String", "String");
          return this;
      }
}