/**
 * Copyright (c) 2015-2017, Henry Yang 杨勇 (gismail@foxmail.com).
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.lambkit.module.bpms.row;

import com.lambkit.db.RowModel;

/**
 * @author yangyong 
 * @website: www.lambkit.com
 * @email: gismail@foxmail.com
 * @date ${date}
 * @version 1.0
 * @since 1.0
 */

public class BpmsServiceNode extends RowModel<BpmsServiceNode> {
	public BpmsServiceNode() {
		setTableName("bpms_service_node");
		setPrimaryKey("node_id");
	}
	public java.lang.Long getNodeId() {
		return this.get("node_id");
	}
	public void setNodeId(java.lang.Long nodeId) {
		this.set("node_id", nodeId);
	}
	public java.lang.String getNodeCode() {
		return this.get("node_code");
	}
	public void setNodeCode(java.lang.String nodeCode) {
		this.set("node_code", nodeCode);
	}
	public java.lang.String getNodeName() {
		return this.get("node_name");
	}
	public void setNodeName(java.lang.String nodeName) {
		this.set("node_name", nodeName);
	}
	public java.lang.Integer getNodeType() {
		return this.get("node_type");
	}
	public void setNodeType(java.lang.Integer nodeType) {
		this.set("node_type", nodeType);
	}
	public java.lang.String getPermissionFlag() {
		return this.get("permission_flag");
	}
	public void setPermissionFlag(java.lang.String permissionFlag) {
		this.set("permission_flag", permissionFlag);
	}
	public java.lang.Integer getNodeStatus() {
		return this.get("node_status");
	}
	public void setNodeStatus(java.lang.Integer nodeStatus) {
		this.set("node_status", nodeStatus);
	}
	public java.lang.String getCreateUser() {
		return this.get("create_user");
	}
	public void setCreateUser(java.lang.String createUser) {
		this.set("create_user", createUser);
	}
	public java.util.Date getCreateTime() {
		return this.get("create_time");
	}
	public void setCreateTime(java.util.Date createTime) {
		this.set("create_time", createTime);
	}
	public java.util.Date getUpdateTime() {
		return this.get("update_time");
	}
	public void setUpdateTime(java.util.Date updateTime) {
		this.set("update_time", updateTime);
	}
	public java.lang.String getDelFlag() {
		return this.get("del_flag");
	}
	public void setDelFlag(java.lang.String delFlag) {
		this.set("del_flag", delFlag);
	}
	public java.lang.String getUserCode() {
		return this.get("user_code");
	}
	public void setUserCode(java.lang.String userCode) {
		this.set("user_code", userCode);
	}
	public java.lang.String getNodeService() {
		return this.get("node_service");
	}
	public void setNodeService(java.lang.String nodeService) {
		this.set("node_service", nodeService);
	}
}
