/**
 * Copyright (c) 2015-2017, Henry Yang 杨勇 (gismail@foxmail.com).
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.lambkit.module.bpms.sql;

import com.lambkit.db.sql.ConditionMode;
import com.lambkit.db.sql.Column;
import com.lambkit.db.sql.Columns;
import com.lambkit.db.sql.Example;

import java.io.Serializable;
import java.util.List;

/**
 * @author yangyong 
 * @website: www.lambkit.com
 * @email: gismail@foxmail.com
 * @date ${date}
 * @version 1.0
 * @since 1.0
 */

public class BpmsServiceFlowNodeDataSQL extends Columns implements Serializable {
	private static final long serialVersionUID = 1L;
	
	public static BpmsServiceFlowNodeDataSQL of() {
		return new BpmsServiceFlowNodeDataSQL();
	}
	
	public static BpmsServiceFlowNodeDataSQL by(Column column) {
		BpmsServiceFlowNodeDataSQL that = new BpmsServiceFlowNodeDataSQL();
		that.add(column);
        return that;
    }

    public static BpmsServiceFlowNodeDataSQL by(String name, Object value) {
        return (BpmsServiceFlowNodeDataSQL) create().eq(name, value);
    }
    
    public Example example() {
    	return Example.create("bpms_service_flow_node_data", this);
    }
    
    /**
     * equals
     *
     * @param name
     * @param value
     * @return
     */
    public BpmsServiceFlowNodeDataSQL eq(String name, Object value) {
    	super.eq(name, value);
        return this;
    }

    /**
     * not equals !=
     *
     * @param name
     * @param value
     * @return
     */
    public BpmsServiceFlowNodeDataSQL ne(String name, Object value) {
    	super.ne(name, value);
        return this;
    }


    /**
     * like
     *
     * @param name
     * @param value
     * @return
     */

    public BpmsServiceFlowNodeDataSQL like(String name, Object value) {
    	super.like(name, value);
        return this;
    }
    
    public BpmsServiceFlowNodeDataSQL notLike(String name, Object value) {
    	super.notLike(name, value);
        return this;
    }

    /**
     * 大于 great than
     *
     * @param name
     * @param value
     * @return
     */
    public BpmsServiceFlowNodeDataSQL gt(String name, Object value) {
    	super.gt(name, value);
        return this;
    }

    /**
     * 大于等于 great or equal
     *
     * @param name
     * @param value
     * @return
     */
    public BpmsServiceFlowNodeDataSQL ge(String name, Object value) {
    	super.ge(name, value);
        return this;
    }

    /**
     * 小于 less than
     *
     * @param name
     * @param value
     * @return
     */
    public BpmsServiceFlowNodeDataSQL lt(String name, Object value) {
    	super.lt(name, value);
        return this;
    }

    /**
     * 小于等于 less or equal
     *
     * @param name
     * @param value
     * @return
     */
    public BpmsServiceFlowNodeDataSQL le(String name, Object value) {
    	super.le(name, value);
        return this;
    }
    
    public BpmsServiceFlowNodeDataSQL isnull(String name) {
    	super.isnull(name);
        return this;
    } 

    public BpmsServiceFlowNodeDataSQL notNull(String name) {
    	super.notNull(name);
        return this;
    } 
    
    public BpmsServiceFlowNodeDataSQL empty(String name) {
    	super.empty(name);
        return this;
    } 
    
    public BpmsServiceFlowNodeDataSQL notEmpty(String name) {
    	super.notEmpty(name);
        return this;
    } 
    
    public BpmsServiceFlowNodeDataSQL add(Column column) {
    	super.add(column);
    	return this;
    }
    
    /**************************/
	
	public void addCriterion(String name, Object value, ConditionMode logic, String property, String typeHandler, String valueType) {
		 if (value == null) {
			 throw new RuntimeException("Value for " + property + " cannot be null");
		 }
		 add(Column.create(name, value, logic, typeHandler, valueType));
	}
   
	public void addCriterion(String name, Object value1, Object value2, ConditionMode logic, String property, String typeHandler, String valueType) {
		 if (value1 == null || value2 == null) {
			 throw new RuntimeException("Between values for " + property + " cannot be null");
		 }
		 add(Column.create(name, value1, value2, logic, typeHandler, valueType));
	}
		 
	public BpmsServiceFlowNodeDataSQL andNodeDataIdIsNull() {
		isnull("node_data_id");
		return this;
	}
	
	public BpmsServiceFlowNodeDataSQL andNodeDataIdIsNotNull() {
		notNull("node_data_id");
		return this;
	}
	
	public BpmsServiceFlowNodeDataSQL andNodeDataIdIsEmpty() {
		empty("node_data_id");
		return this;
	}

	public BpmsServiceFlowNodeDataSQL andNodeDataIdIsNotEmpty() {
		notEmpty("node_data_id");
		return this;
	}
      public BpmsServiceFlowNodeDataSQL andNodeDataIdEqualTo(java.lang.Long value) {
          addCriterion("node_data_id", value, ConditionMode.EQUAL, "nodeDataId", "java.lang.Long", "Float");
          return this;
      }

      public BpmsServiceFlowNodeDataSQL andNodeDataIdNotEqualTo(java.lang.Long value) {
          addCriterion("node_data_id", value, ConditionMode.NOT_EQUAL, "nodeDataId", "java.lang.Long", "Float");
          return this;
      }

      public BpmsServiceFlowNodeDataSQL andNodeDataIdGreaterThan(java.lang.Long value) {
          addCriterion("node_data_id", value, ConditionMode.GREATER_THEN, "nodeDataId", "java.lang.Long", "Float");
          return this;
      }

      public BpmsServiceFlowNodeDataSQL andNodeDataIdGreaterThanOrEqualTo(java.lang.Long value) {
          addCriterion("node_data_id", value, ConditionMode.GREATER_EQUAL, "nodeDataId", "java.lang.Long", "Float");
          return this;
      }

      public BpmsServiceFlowNodeDataSQL andNodeDataIdLessThan(java.lang.Long value) {
          addCriterion("node_data_id", value, ConditionMode.LESS_THEN, "nodeDataId", "java.lang.Long", "Float");
          return this;
      }

      public BpmsServiceFlowNodeDataSQL andNodeDataIdLessThanOrEqualTo(java.lang.Long value) {
          addCriterion("node_data_id", value, ConditionMode.LESS_EQUAL, "nodeDataId", "java.lang.Long", "Float");
          return this;
      }

      public BpmsServiceFlowNodeDataSQL andNodeDataIdBetween(java.lang.Long value1, java.lang.Long value2) {
    	  addCriterion("node_data_id", value1, value2, ConditionMode.BETWEEN, "nodeDataId", "java.lang.Long", "Float");
    	  return this;
      }

      public BpmsServiceFlowNodeDataSQL andNodeDataIdNotBetween(java.lang.Long value1, java.lang.Long value2) {
          addCriterion("node_data_id", value1, value2, ConditionMode.NOT_BETWEEN, "nodeDataId", "java.lang.Long", "Float");
          return this;
      }
        
      public BpmsServiceFlowNodeDataSQL andNodeDataIdIn(List<java.lang.Long> values) {
          addCriterion("node_data_id", values, ConditionMode.IN, "nodeDataId", "java.lang.Long", "Float");
          return this;
      }

      public BpmsServiceFlowNodeDataSQL andNodeDataIdNotIn(List<java.lang.Long> values) {
          addCriterion("node_data_id", values, ConditionMode.NOT_IN, "nodeDataId", "java.lang.Long", "Float");
          return this;
      }
	public BpmsServiceFlowNodeDataSQL andFlowCodeIsNull() {
		isnull("flow_code");
		return this;
	}
	
	public BpmsServiceFlowNodeDataSQL andFlowCodeIsNotNull() {
		notNull("flow_code");
		return this;
	}
	
	public BpmsServiceFlowNodeDataSQL andFlowCodeIsEmpty() {
		empty("flow_code");
		return this;
	}

	public BpmsServiceFlowNodeDataSQL andFlowCodeIsNotEmpty() {
		notEmpty("flow_code");
		return this;
	}
       public BpmsServiceFlowNodeDataSQL andFlowCodeLike(java.lang.String value) {
    	   addCriterion("flow_code", value, ConditionMode.FUZZY, "flowCode", "java.lang.String", "Float");
    	   return this;
      }

      public BpmsServiceFlowNodeDataSQL andFlowCodeNotLike(java.lang.String value) {
          addCriterion("flow_code", value, ConditionMode.NOT_FUZZY, "flowCode", "java.lang.String", "Float");
          return this;
      }
      public BpmsServiceFlowNodeDataSQL andFlowCodeEqualTo(java.lang.String value) {
          addCriterion("flow_code", value, ConditionMode.EQUAL, "flowCode", "java.lang.String", "String");
          return this;
      }

      public BpmsServiceFlowNodeDataSQL andFlowCodeNotEqualTo(java.lang.String value) {
          addCriterion("flow_code", value, ConditionMode.NOT_EQUAL, "flowCode", "java.lang.String", "String");
          return this;
      }

      public BpmsServiceFlowNodeDataSQL andFlowCodeGreaterThan(java.lang.String value) {
          addCriterion("flow_code", value, ConditionMode.GREATER_THEN, "flowCode", "java.lang.String", "String");
          return this;
      }

      public BpmsServiceFlowNodeDataSQL andFlowCodeGreaterThanOrEqualTo(java.lang.String value) {
          addCriterion("flow_code", value, ConditionMode.GREATER_EQUAL, "flowCode", "java.lang.String", "String");
          return this;
      }

      public BpmsServiceFlowNodeDataSQL andFlowCodeLessThan(java.lang.String value) {
          addCriterion("flow_code", value, ConditionMode.LESS_THEN, "flowCode", "java.lang.String", "String");
          return this;
      }

      public BpmsServiceFlowNodeDataSQL andFlowCodeLessThanOrEqualTo(java.lang.String value) {
          addCriterion("flow_code", value, ConditionMode.LESS_EQUAL, "flowCode", "java.lang.String", "String");
          return this;
      }

      public BpmsServiceFlowNodeDataSQL andFlowCodeBetween(java.lang.String value1, java.lang.String value2) {
    	  addCriterion("flow_code", value1, value2, ConditionMode.BETWEEN, "flowCode", "java.lang.String", "String");
    	  return this;
      }

      public BpmsServiceFlowNodeDataSQL andFlowCodeNotBetween(java.lang.String value1, java.lang.String value2) {
          addCriterion("flow_code", value1, value2, ConditionMode.NOT_BETWEEN, "flowCode", "java.lang.String", "String");
          return this;
      }
        
      public BpmsServiceFlowNodeDataSQL andFlowCodeIn(List<java.lang.String> values) {
          addCriterion("flow_code", values, ConditionMode.IN, "flowCode", "java.lang.String", "String");
          return this;
      }

      public BpmsServiceFlowNodeDataSQL andFlowCodeNotIn(List<java.lang.String> values) {
          addCriterion("flow_code", values, ConditionMode.NOT_IN, "flowCode", "java.lang.String", "String");
          return this;
      }
	public BpmsServiceFlowNodeDataSQL andNodeCodeIsNull() {
		isnull("node_code");
		return this;
	}
	
	public BpmsServiceFlowNodeDataSQL andNodeCodeIsNotNull() {
		notNull("node_code");
		return this;
	}
	
	public BpmsServiceFlowNodeDataSQL andNodeCodeIsEmpty() {
		empty("node_code");
		return this;
	}

	public BpmsServiceFlowNodeDataSQL andNodeCodeIsNotEmpty() {
		notEmpty("node_code");
		return this;
	}
       public BpmsServiceFlowNodeDataSQL andNodeCodeLike(java.lang.String value) {
    	   addCriterion("node_code", value, ConditionMode.FUZZY, "nodeCode", "java.lang.String", "String");
    	   return this;
      }

      public BpmsServiceFlowNodeDataSQL andNodeCodeNotLike(java.lang.String value) {
          addCriterion("node_code", value, ConditionMode.NOT_FUZZY, "nodeCode", "java.lang.String", "String");
          return this;
      }
      public BpmsServiceFlowNodeDataSQL andNodeCodeEqualTo(java.lang.String value) {
          addCriterion("node_code", value, ConditionMode.EQUAL, "nodeCode", "java.lang.String", "String");
          return this;
      }

      public BpmsServiceFlowNodeDataSQL andNodeCodeNotEqualTo(java.lang.String value) {
          addCriterion("node_code", value, ConditionMode.NOT_EQUAL, "nodeCode", "java.lang.String", "String");
          return this;
      }

      public BpmsServiceFlowNodeDataSQL andNodeCodeGreaterThan(java.lang.String value) {
          addCriterion("node_code", value, ConditionMode.GREATER_THEN, "nodeCode", "java.lang.String", "String");
          return this;
      }

      public BpmsServiceFlowNodeDataSQL andNodeCodeGreaterThanOrEqualTo(java.lang.String value) {
          addCriterion("node_code", value, ConditionMode.GREATER_EQUAL, "nodeCode", "java.lang.String", "String");
          return this;
      }

      public BpmsServiceFlowNodeDataSQL andNodeCodeLessThan(java.lang.String value) {
          addCriterion("node_code", value, ConditionMode.LESS_THEN, "nodeCode", "java.lang.String", "String");
          return this;
      }

      public BpmsServiceFlowNodeDataSQL andNodeCodeLessThanOrEqualTo(java.lang.String value) {
          addCriterion("node_code", value, ConditionMode.LESS_EQUAL, "nodeCode", "java.lang.String", "String");
          return this;
      }

      public BpmsServiceFlowNodeDataSQL andNodeCodeBetween(java.lang.String value1, java.lang.String value2) {
    	  addCriterion("node_code", value1, value2, ConditionMode.BETWEEN, "nodeCode", "java.lang.String", "String");
    	  return this;
      }

      public BpmsServiceFlowNodeDataSQL andNodeCodeNotBetween(java.lang.String value1, java.lang.String value2) {
          addCriterion("node_code", value1, value2, ConditionMode.NOT_BETWEEN, "nodeCode", "java.lang.String", "String");
          return this;
      }
        
      public BpmsServiceFlowNodeDataSQL andNodeCodeIn(List<java.lang.String> values) {
          addCriterion("node_code", values, ConditionMode.IN, "nodeCode", "java.lang.String", "String");
          return this;
      }

      public BpmsServiceFlowNodeDataSQL andNodeCodeNotIn(List<java.lang.String> values) {
          addCriterion("node_code", values, ConditionMode.NOT_IN, "nodeCode", "java.lang.String", "String");
          return this;
      }
	public BpmsServiceFlowNodeDataSQL andDataNameIsNull() {
		isnull("data_name");
		return this;
	}
	
	public BpmsServiceFlowNodeDataSQL andDataNameIsNotNull() {
		notNull("data_name");
		return this;
	}
	
	public BpmsServiceFlowNodeDataSQL andDataNameIsEmpty() {
		empty("data_name");
		return this;
	}

	public BpmsServiceFlowNodeDataSQL andDataNameIsNotEmpty() {
		notEmpty("data_name");
		return this;
	}
       public BpmsServiceFlowNodeDataSQL andDataNameLike(java.lang.String value) {
    	   addCriterion("data_name", value, ConditionMode.FUZZY, "dataName", "java.lang.String", "String");
    	   return this;
      }

      public BpmsServiceFlowNodeDataSQL andDataNameNotLike(java.lang.String value) {
          addCriterion("data_name", value, ConditionMode.NOT_FUZZY, "dataName", "java.lang.String", "String");
          return this;
      }
      public BpmsServiceFlowNodeDataSQL andDataNameEqualTo(java.lang.String value) {
          addCriterion("data_name", value, ConditionMode.EQUAL, "dataName", "java.lang.String", "String");
          return this;
      }

      public BpmsServiceFlowNodeDataSQL andDataNameNotEqualTo(java.lang.String value) {
          addCriterion("data_name", value, ConditionMode.NOT_EQUAL, "dataName", "java.lang.String", "String");
          return this;
      }

      public BpmsServiceFlowNodeDataSQL andDataNameGreaterThan(java.lang.String value) {
          addCriterion("data_name", value, ConditionMode.GREATER_THEN, "dataName", "java.lang.String", "String");
          return this;
      }

      public BpmsServiceFlowNodeDataSQL andDataNameGreaterThanOrEqualTo(java.lang.String value) {
          addCriterion("data_name", value, ConditionMode.GREATER_EQUAL, "dataName", "java.lang.String", "String");
          return this;
      }

      public BpmsServiceFlowNodeDataSQL andDataNameLessThan(java.lang.String value) {
          addCriterion("data_name", value, ConditionMode.LESS_THEN, "dataName", "java.lang.String", "String");
          return this;
      }

      public BpmsServiceFlowNodeDataSQL andDataNameLessThanOrEqualTo(java.lang.String value) {
          addCriterion("data_name", value, ConditionMode.LESS_EQUAL, "dataName", "java.lang.String", "String");
          return this;
      }

      public BpmsServiceFlowNodeDataSQL andDataNameBetween(java.lang.String value1, java.lang.String value2) {
    	  addCriterion("data_name", value1, value2, ConditionMode.BETWEEN, "dataName", "java.lang.String", "String");
    	  return this;
      }

      public BpmsServiceFlowNodeDataSQL andDataNameNotBetween(java.lang.String value1, java.lang.String value2) {
          addCriterion("data_name", value1, value2, ConditionMode.NOT_BETWEEN, "dataName", "java.lang.String", "String");
          return this;
      }
        
      public BpmsServiceFlowNodeDataSQL andDataNameIn(List<java.lang.String> values) {
          addCriterion("data_name", values, ConditionMode.IN, "dataName", "java.lang.String", "String");
          return this;
      }

      public BpmsServiceFlowNodeDataSQL andDataNameNotIn(List<java.lang.String> values) {
          addCriterion("data_name", values, ConditionMode.NOT_IN, "dataName", "java.lang.String", "String");
          return this;
      }
	public BpmsServiceFlowNodeDataSQL andDataTypeIsNull() {
		isnull("data_type");
		return this;
	}
	
	public BpmsServiceFlowNodeDataSQL andDataTypeIsNotNull() {
		notNull("data_type");
		return this;
	}
	
	public BpmsServiceFlowNodeDataSQL andDataTypeIsEmpty() {
		empty("data_type");
		return this;
	}

	public BpmsServiceFlowNodeDataSQL andDataTypeIsNotEmpty() {
		notEmpty("data_type");
		return this;
	}
      public BpmsServiceFlowNodeDataSQL andDataTypeEqualTo(java.lang.Integer value) {
          addCriterion("data_type", value, ConditionMode.EQUAL, "dataType", "java.lang.Integer", "Float");
          return this;
      }

      public BpmsServiceFlowNodeDataSQL andDataTypeNotEqualTo(java.lang.Integer value) {
          addCriterion("data_type", value, ConditionMode.NOT_EQUAL, "dataType", "java.lang.Integer", "Float");
          return this;
      }

      public BpmsServiceFlowNodeDataSQL andDataTypeGreaterThan(java.lang.Integer value) {
          addCriterion("data_type", value, ConditionMode.GREATER_THEN, "dataType", "java.lang.Integer", "Float");
          return this;
      }

      public BpmsServiceFlowNodeDataSQL andDataTypeGreaterThanOrEqualTo(java.lang.Integer value) {
          addCriterion("data_type", value, ConditionMode.GREATER_EQUAL, "dataType", "java.lang.Integer", "Float");
          return this;
      }

      public BpmsServiceFlowNodeDataSQL andDataTypeLessThan(java.lang.Integer value) {
          addCriterion("data_type", value, ConditionMode.LESS_THEN, "dataType", "java.lang.Integer", "Float");
          return this;
      }

      public BpmsServiceFlowNodeDataSQL andDataTypeLessThanOrEqualTo(java.lang.Integer value) {
          addCriterion("data_type", value, ConditionMode.LESS_EQUAL, "dataType", "java.lang.Integer", "Float");
          return this;
      }

      public BpmsServiceFlowNodeDataSQL andDataTypeBetween(java.lang.Integer value1, java.lang.Integer value2) {
    	  addCriterion("data_type", value1, value2, ConditionMode.BETWEEN, "dataType", "java.lang.Integer", "Float");
    	  return this;
      }

      public BpmsServiceFlowNodeDataSQL andDataTypeNotBetween(java.lang.Integer value1, java.lang.Integer value2) {
          addCriterion("data_type", value1, value2, ConditionMode.NOT_BETWEEN, "dataType", "java.lang.Integer", "Float");
          return this;
      }
        
      public BpmsServiceFlowNodeDataSQL andDataTypeIn(List<java.lang.Integer> values) {
          addCriterion("data_type", values, ConditionMode.IN, "dataType", "java.lang.Integer", "Float");
          return this;
      }

      public BpmsServiceFlowNodeDataSQL andDataTypeNotIn(List<java.lang.Integer> values) {
          addCriterion("data_type", values, ConditionMode.NOT_IN, "dataType", "java.lang.Integer", "Float");
          return this;
      }
	public BpmsServiceFlowNodeDataSQL andDataValueIsNull() {
		isnull("data_value");
		return this;
	}
	
	public BpmsServiceFlowNodeDataSQL andDataValueIsNotNull() {
		notNull("data_value");
		return this;
	}
	
	public BpmsServiceFlowNodeDataSQL andDataValueIsEmpty() {
		empty("data_value");
		return this;
	}

	public BpmsServiceFlowNodeDataSQL andDataValueIsNotEmpty() {
		notEmpty("data_value");
		return this;
	}
       public BpmsServiceFlowNodeDataSQL andDataValueLike(java.lang.String value) {
    	   addCriterion("data_value", value, ConditionMode.FUZZY, "dataValue", "java.lang.String", "Float");
    	   return this;
      }

      public BpmsServiceFlowNodeDataSQL andDataValueNotLike(java.lang.String value) {
          addCriterion("data_value", value, ConditionMode.NOT_FUZZY, "dataValue", "java.lang.String", "Float");
          return this;
      }
      public BpmsServiceFlowNodeDataSQL andDataValueEqualTo(java.lang.String value) {
          addCriterion("data_value", value, ConditionMode.EQUAL, "dataValue", "java.lang.String", "String");
          return this;
      }

      public BpmsServiceFlowNodeDataSQL andDataValueNotEqualTo(java.lang.String value) {
          addCriterion("data_value", value, ConditionMode.NOT_EQUAL, "dataValue", "java.lang.String", "String");
          return this;
      }

      public BpmsServiceFlowNodeDataSQL andDataValueGreaterThan(java.lang.String value) {
          addCriterion("data_value", value, ConditionMode.GREATER_THEN, "dataValue", "java.lang.String", "String");
          return this;
      }

      public BpmsServiceFlowNodeDataSQL andDataValueGreaterThanOrEqualTo(java.lang.String value) {
          addCriterion("data_value", value, ConditionMode.GREATER_EQUAL, "dataValue", "java.lang.String", "String");
          return this;
      }

      public BpmsServiceFlowNodeDataSQL andDataValueLessThan(java.lang.String value) {
          addCriterion("data_value", value, ConditionMode.LESS_THEN, "dataValue", "java.lang.String", "String");
          return this;
      }

      public BpmsServiceFlowNodeDataSQL andDataValueLessThanOrEqualTo(java.lang.String value) {
          addCriterion("data_value", value, ConditionMode.LESS_EQUAL, "dataValue", "java.lang.String", "String");
          return this;
      }

      public BpmsServiceFlowNodeDataSQL andDataValueBetween(java.lang.String value1, java.lang.String value2) {
    	  addCriterion("data_value", value1, value2, ConditionMode.BETWEEN, "dataValue", "java.lang.String", "String");
    	  return this;
      }

      public BpmsServiceFlowNodeDataSQL andDataValueNotBetween(java.lang.String value1, java.lang.String value2) {
          addCriterion("data_value", value1, value2, ConditionMode.NOT_BETWEEN, "dataValue", "java.lang.String", "String");
          return this;
      }
        
      public BpmsServiceFlowNodeDataSQL andDataValueIn(List<java.lang.String> values) {
          addCriterion("data_value", values, ConditionMode.IN, "dataValue", "java.lang.String", "String");
          return this;
      }

      public BpmsServiceFlowNodeDataSQL andDataValueNotIn(List<java.lang.String> values) {
          addCriterion("data_value", values, ConditionMode.NOT_IN, "dataValue", "java.lang.String", "String");
          return this;
      }
	public BpmsServiceFlowNodeDataSQL andCreateUserIsNull() {
		isnull("create_user");
		return this;
	}
	
	public BpmsServiceFlowNodeDataSQL andCreateUserIsNotNull() {
		notNull("create_user");
		return this;
	}
	
	public BpmsServiceFlowNodeDataSQL andCreateUserIsEmpty() {
		empty("create_user");
		return this;
	}

	public BpmsServiceFlowNodeDataSQL andCreateUserIsNotEmpty() {
		notEmpty("create_user");
		return this;
	}
       public BpmsServiceFlowNodeDataSQL andCreateUserLike(java.lang.String value) {
    	   addCriterion("create_user", value, ConditionMode.FUZZY, "createUser", "java.lang.String", "String");
    	   return this;
      }

      public BpmsServiceFlowNodeDataSQL andCreateUserNotLike(java.lang.String value) {
          addCriterion("create_user", value, ConditionMode.NOT_FUZZY, "createUser", "java.lang.String", "String");
          return this;
      }
      public BpmsServiceFlowNodeDataSQL andCreateUserEqualTo(java.lang.String value) {
          addCriterion("create_user", value, ConditionMode.EQUAL, "createUser", "java.lang.String", "String");
          return this;
      }

      public BpmsServiceFlowNodeDataSQL andCreateUserNotEqualTo(java.lang.String value) {
          addCriterion("create_user", value, ConditionMode.NOT_EQUAL, "createUser", "java.lang.String", "String");
          return this;
      }

      public BpmsServiceFlowNodeDataSQL andCreateUserGreaterThan(java.lang.String value) {
          addCriterion("create_user", value, ConditionMode.GREATER_THEN, "createUser", "java.lang.String", "String");
          return this;
      }

      public BpmsServiceFlowNodeDataSQL andCreateUserGreaterThanOrEqualTo(java.lang.String value) {
          addCriterion("create_user", value, ConditionMode.GREATER_EQUAL, "createUser", "java.lang.String", "String");
          return this;
      }

      public BpmsServiceFlowNodeDataSQL andCreateUserLessThan(java.lang.String value) {
          addCriterion("create_user", value, ConditionMode.LESS_THEN, "createUser", "java.lang.String", "String");
          return this;
      }

      public BpmsServiceFlowNodeDataSQL andCreateUserLessThanOrEqualTo(java.lang.String value) {
          addCriterion("create_user", value, ConditionMode.LESS_EQUAL, "createUser", "java.lang.String", "String");
          return this;
      }

      public BpmsServiceFlowNodeDataSQL andCreateUserBetween(java.lang.String value1, java.lang.String value2) {
    	  addCriterion("create_user", value1, value2, ConditionMode.BETWEEN, "createUser", "java.lang.String", "String");
    	  return this;
      }

      public BpmsServiceFlowNodeDataSQL andCreateUserNotBetween(java.lang.String value1, java.lang.String value2) {
          addCriterion("create_user", value1, value2, ConditionMode.NOT_BETWEEN, "createUser", "java.lang.String", "String");
          return this;
      }
        
      public BpmsServiceFlowNodeDataSQL andCreateUserIn(List<java.lang.String> values) {
          addCriterion("create_user", values, ConditionMode.IN, "createUser", "java.lang.String", "String");
          return this;
      }

      public BpmsServiceFlowNodeDataSQL andCreateUserNotIn(List<java.lang.String> values) {
          addCriterion("create_user", values, ConditionMode.NOT_IN, "createUser", "java.lang.String", "String");
          return this;
      }
	public BpmsServiceFlowNodeDataSQL andCreateTimeIsNull() {
		isnull("create_time");
		return this;
	}
	
	public BpmsServiceFlowNodeDataSQL andCreateTimeIsNotNull() {
		notNull("create_time");
		return this;
	}
	
	public BpmsServiceFlowNodeDataSQL andCreateTimeIsEmpty() {
		empty("create_time");
		return this;
	}

	public BpmsServiceFlowNodeDataSQL andCreateTimeIsNotEmpty() {
		notEmpty("create_time");
		return this;
	}
      public BpmsServiceFlowNodeDataSQL andCreateTimeEqualTo(java.util.Date value) {
          addCriterion("create_time", value, ConditionMode.EQUAL, "createTime", "java.util.Date", "String");
          return this;
      }

      public BpmsServiceFlowNodeDataSQL andCreateTimeNotEqualTo(java.util.Date value) {
          addCriterion("create_time", value, ConditionMode.NOT_EQUAL, "createTime", "java.util.Date", "String");
          return this;
      }

      public BpmsServiceFlowNodeDataSQL andCreateTimeGreaterThan(java.util.Date value) {
          addCriterion("create_time", value, ConditionMode.GREATER_THEN, "createTime", "java.util.Date", "String");
          return this;
      }

      public BpmsServiceFlowNodeDataSQL andCreateTimeGreaterThanOrEqualTo(java.util.Date value) {
          addCriterion("create_time", value, ConditionMode.GREATER_EQUAL, "createTime", "java.util.Date", "String");
          return this;
      }

      public BpmsServiceFlowNodeDataSQL andCreateTimeLessThan(java.util.Date value) {
          addCriterion("create_time", value, ConditionMode.LESS_THEN, "createTime", "java.util.Date", "String");
          return this;
      }

      public BpmsServiceFlowNodeDataSQL andCreateTimeLessThanOrEqualTo(java.util.Date value) {
          addCriterion("create_time", value, ConditionMode.LESS_EQUAL, "createTime", "java.util.Date", "String");
          return this;
      }

      public BpmsServiceFlowNodeDataSQL andCreateTimeBetween(java.util.Date value1, java.util.Date value2) {
    	  addCriterion("create_time", value1, value2, ConditionMode.BETWEEN, "createTime", "java.util.Date", "String");
    	  return this;
      }

      public BpmsServiceFlowNodeDataSQL andCreateTimeNotBetween(java.util.Date value1, java.util.Date value2) {
          addCriterion("create_time", value1, value2, ConditionMode.NOT_BETWEEN, "createTime", "java.util.Date", "String");
          return this;
      }
        
      public BpmsServiceFlowNodeDataSQL andCreateTimeIn(List<java.util.Date> values) {
          addCriterion("create_time", values, ConditionMode.IN, "createTime", "java.util.Date", "String");
          return this;
      }

      public BpmsServiceFlowNodeDataSQL andCreateTimeNotIn(List<java.util.Date> values) {
          addCriterion("create_time", values, ConditionMode.NOT_IN, "createTime", "java.util.Date", "String");
          return this;
      }
	public BpmsServiceFlowNodeDataSQL andUpdateTimeIsNull() {
		isnull("update_time");
		return this;
	}
	
	public BpmsServiceFlowNodeDataSQL andUpdateTimeIsNotNull() {
		notNull("update_time");
		return this;
	}
	
	public BpmsServiceFlowNodeDataSQL andUpdateTimeIsEmpty() {
		empty("update_time");
		return this;
	}

	public BpmsServiceFlowNodeDataSQL andUpdateTimeIsNotEmpty() {
		notEmpty("update_time");
		return this;
	}
      public BpmsServiceFlowNodeDataSQL andUpdateTimeEqualTo(java.util.Date value) {
          addCriterion("update_time", value, ConditionMode.EQUAL, "updateTime", "java.util.Date", "String");
          return this;
      }

      public BpmsServiceFlowNodeDataSQL andUpdateTimeNotEqualTo(java.util.Date value) {
          addCriterion("update_time", value, ConditionMode.NOT_EQUAL, "updateTime", "java.util.Date", "String");
          return this;
      }

      public BpmsServiceFlowNodeDataSQL andUpdateTimeGreaterThan(java.util.Date value) {
          addCriterion("update_time", value, ConditionMode.GREATER_THEN, "updateTime", "java.util.Date", "String");
          return this;
      }

      public BpmsServiceFlowNodeDataSQL andUpdateTimeGreaterThanOrEqualTo(java.util.Date value) {
          addCriterion("update_time", value, ConditionMode.GREATER_EQUAL, "updateTime", "java.util.Date", "String");
          return this;
      }

      public BpmsServiceFlowNodeDataSQL andUpdateTimeLessThan(java.util.Date value) {
          addCriterion("update_time", value, ConditionMode.LESS_THEN, "updateTime", "java.util.Date", "String");
          return this;
      }

      public BpmsServiceFlowNodeDataSQL andUpdateTimeLessThanOrEqualTo(java.util.Date value) {
          addCriterion("update_time", value, ConditionMode.LESS_EQUAL, "updateTime", "java.util.Date", "String");
          return this;
      }

      public BpmsServiceFlowNodeDataSQL andUpdateTimeBetween(java.util.Date value1, java.util.Date value2) {
    	  addCriterion("update_time", value1, value2, ConditionMode.BETWEEN, "updateTime", "java.util.Date", "String");
    	  return this;
      }

      public BpmsServiceFlowNodeDataSQL andUpdateTimeNotBetween(java.util.Date value1, java.util.Date value2) {
          addCriterion("update_time", value1, value2, ConditionMode.NOT_BETWEEN, "updateTime", "java.util.Date", "String");
          return this;
      }
        
      public BpmsServiceFlowNodeDataSQL andUpdateTimeIn(List<java.util.Date> values) {
          addCriterion("update_time", values, ConditionMode.IN, "updateTime", "java.util.Date", "String");
          return this;
      }

      public BpmsServiceFlowNodeDataSQL andUpdateTimeNotIn(List<java.util.Date> values) {
          addCriterion("update_time", values, ConditionMode.NOT_IN, "updateTime", "java.util.Date", "String");
          return this;
      }
	public BpmsServiceFlowNodeDataSQL andDelFlagIsNull() {
		isnull("del_flag");
		return this;
	}
	
	public BpmsServiceFlowNodeDataSQL andDelFlagIsNotNull() {
		notNull("del_flag");
		return this;
	}
	
	public BpmsServiceFlowNodeDataSQL andDelFlagIsEmpty() {
		empty("del_flag");
		return this;
	}

	public BpmsServiceFlowNodeDataSQL andDelFlagIsNotEmpty() {
		notEmpty("del_flag");
		return this;
	}
       public BpmsServiceFlowNodeDataSQL andDelFlagLike(java.lang.String value) {
    	   addCriterion("del_flag", value, ConditionMode.FUZZY, "delFlag", "java.lang.String", "String");
    	   return this;
      }

      public BpmsServiceFlowNodeDataSQL andDelFlagNotLike(java.lang.String value) {
          addCriterion("del_flag", value, ConditionMode.NOT_FUZZY, "delFlag", "java.lang.String", "String");
          return this;
      }
      public BpmsServiceFlowNodeDataSQL andDelFlagEqualTo(java.lang.String value) {
          addCriterion("del_flag", value, ConditionMode.EQUAL, "delFlag", "java.lang.String", "String");
          return this;
      }

      public BpmsServiceFlowNodeDataSQL andDelFlagNotEqualTo(java.lang.String value) {
          addCriterion("del_flag", value, ConditionMode.NOT_EQUAL, "delFlag", "java.lang.String", "String");
          return this;
      }

      public BpmsServiceFlowNodeDataSQL andDelFlagGreaterThan(java.lang.String value) {
          addCriterion("del_flag", value, ConditionMode.GREATER_THEN, "delFlag", "java.lang.String", "String");
          return this;
      }

      public BpmsServiceFlowNodeDataSQL andDelFlagGreaterThanOrEqualTo(java.lang.String value) {
          addCriterion("del_flag", value, ConditionMode.GREATER_EQUAL, "delFlag", "java.lang.String", "String");
          return this;
      }

      public BpmsServiceFlowNodeDataSQL andDelFlagLessThan(java.lang.String value) {
          addCriterion("del_flag", value, ConditionMode.LESS_THEN, "delFlag", "java.lang.String", "String");
          return this;
      }

      public BpmsServiceFlowNodeDataSQL andDelFlagLessThanOrEqualTo(java.lang.String value) {
          addCriterion("del_flag", value, ConditionMode.LESS_EQUAL, "delFlag", "java.lang.String", "String");
          return this;
      }

      public BpmsServiceFlowNodeDataSQL andDelFlagBetween(java.lang.String value1, java.lang.String value2) {
    	  addCriterion("del_flag", value1, value2, ConditionMode.BETWEEN, "delFlag", "java.lang.String", "String");
    	  return this;
      }

      public BpmsServiceFlowNodeDataSQL andDelFlagNotBetween(java.lang.String value1, java.lang.String value2) {
          addCriterion("del_flag", value1, value2, ConditionMode.NOT_BETWEEN, "delFlag", "java.lang.String", "String");
          return this;
      }
        
      public BpmsServiceFlowNodeDataSQL andDelFlagIn(List<java.lang.String> values) {
          addCriterion("del_flag", values, ConditionMode.IN, "delFlag", "java.lang.String", "String");
          return this;
      }

      public BpmsServiceFlowNodeDataSQL andDelFlagNotIn(List<java.lang.String> values) {
          addCriterion("del_flag", values, ConditionMode.NOT_IN, "delFlag", "java.lang.String", "String");
          return this;
      }
	public BpmsServiceFlowNodeDataSQL andUserCodeIsNull() {
		isnull("user_code");
		return this;
	}
	
	public BpmsServiceFlowNodeDataSQL andUserCodeIsNotNull() {
		notNull("user_code");
		return this;
	}
	
	public BpmsServiceFlowNodeDataSQL andUserCodeIsEmpty() {
		empty("user_code");
		return this;
	}

	public BpmsServiceFlowNodeDataSQL andUserCodeIsNotEmpty() {
		notEmpty("user_code");
		return this;
	}
       public BpmsServiceFlowNodeDataSQL andUserCodeLike(java.lang.String value) {
    	   addCriterion("user_code", value, ConditionMode.FUZZY, "userCode", "java.lang.String", "String");
    	   return this;
      }

      public BpmsServiceFlowNodeDataSQL andUserCodeNotLike(java.lang.String value) {
          addCriterion("user_code", value, ConditionMode.NOT_FUZZY, "userCode", "java.lang.String", "String");
          return this;
      }
      public BpmsServiceFlowNodeDataSQL andUserCodeEqualTo(java.lang.String value) {
          addCriterion("user_code", value, ConditionMode.EQUAL, "userCode", "java.lang.String", "String");
          return this;
      }

      public BpmsServiceFlowNodeDataSQL andUserCodeNotEqualTo(java.lang.String value) {
          addCriterion("user_code", value, ConditionMode.NOT_EQUAL, "userCode", "java.lang.String", "String");
          return this;
      }

      public BpmsServiceFlowNodeDataSQL andUserCodeGreaterThan(java.lang.String value) {
          addCriterion("user_code", value, ConditionMode.GREATER_THEN, "userCode", "java.lang.String", "String");
          return this;
      }

      public BpmsServiceFlowNodeDataSQL andUserCodeGreaterThanOrEqualTo(java.lang.String value) {
          addCriterion("user_code", value, ConditionMode.GREATER_EQUAL, "userCode", "java.lang.String", "String");
          return this;
      }

      public BpmsServiceFlowNodeDataSQL andUserCodeLessThan(java.lang.String value) {
          addCriterion("user_code", value, ConditionMode.LESS_THEN, "userCode", "java.lang.String", "String");
          return this;
      }

      public BpmsServiceFlowNodeDataSQL andUserCodeLessThanOrEqualTo(java.lang.String value) {
          addCriterion("user_code", value, ConditionMode.LESS_EQUAL, "userCode", "java.lang.String", "String");
          return this;
      }

      public BpmsServiceFlowNodeDataSQL andUserCodeBetween(java.lang.String value1, java.lang.String value2) {
    	  addCriterion("user_code", value1, value2, ConditionMode.BETWEEN, "userCode", "java.lang.String", "String");
    	  return this;
      }

      public BpmsServiceFlowNodeDataSQL andUserCodeNotBetween(java.lang.String value1, java.lang.String value2) {
          addCriterion("user_code", value1, value2, ConditionMode.NOT_BETWEEN, "userCode", "java.lang.String", "String");
          return this;
      }
        
      public BpmsServiceFlowNodeDataSQL andUserCodeIn(List<java.lang.String> values) {
          addCriterion("user_code", values, ConditionMode.IN, "userCode", "java.lang.String", "String");
          return this;
      }

      public BpmsServiceFlowNodeDataSQL andUserCodeNotIn(List<java.lang.String> values) {
          addCriterion("user_code", values, ConditionMode.NOT_IN, "userCode", "java.lang.String", "String");
          return this;
      }
}