/**
 * Copyright (c) 2015-2017, Henry Yang 杨勇 (gismail@foxmail.com).
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.lambkit.module.bpms.sql;

import com.lambkit.db.sql.ConditionMode;
import com.lambkit.db.sql.Column;
import com.lambkit.db.sql.Columns;
import com.lambkit.db.sql.Example;

import java.io.Serializable;
import java.util.List;

/**
 * @author yangyong 
 * @website: www.lambkit.com
 * @email: gismail@foxmail.com
 * @date ${date}
 * @version 1.0
 * @since 1.0
 */

public class BpmsServiceInstanceSQL extends Columns implements Serializable {
	private static final long serialVersionUID = 1L;
	
	public static BpmsServiceInstanceSQL of() {
		return new BpmsServiceInstanceSQL();
	}
	
	public static BpmsServiceInstanceSQL by(Column column) {
		BpmsServiceInstanceSQL that = new BpmsServiceInstanceSQL();
		that.add(column);
        return that;
    }

    public static BpmsServiceInstanceSQL by(String name, Object value) {
        return (BpmsServiceInstanceSQL) create().eq(name, value);
    }
    
    public Example example() {
    	return Example.create("bpms_service_instance", this);
    }
    
    /**
     * equals
     *
     * @param name
     * @param value
     * @return
     */
    public BpmsServiceInstanceSQL eq(String name, Object value) {
    	super.eq(name, value);
        return this;
    }

    /**
     * not equals !=
     *
     * @param name
     * @param value
     * @return
     */
    public BpmsServiceInstanceSQL ne(String name, Object value) {
    	super.ne(name, value);
        return this;
    }


    /**
     * like
     *
     * @param name
     * @param value
     * @return
     */

    public BpmsServiceInstanceSQL like(String name, Object value) {
    	super.like(name, value);
        return this;
    }
    
    public BpmsServiceInstanceSQL notLike(String name, Object value) {
    	super.notLike(name, value);
        return this;
    }

    /**
     * 大于 great than
     *
     * @param name
     * @param value
     * @return
     */
    public BpmsServiceInstanceSQL gt(String name, Object value) {
    	super.gt(name, value);
        return this;
    }

    /**
     * 大于等于 great or equal
     *
     * @param name
     * @param value
     * @return
     */
    public BpmsServiceInstanceSQL ge(String name, Object value) {
    	super.ge(name, value);
        return this;
    }

    /**
     * 小于 less than
     *
     * @param name
     * @param value
     * @return
     */
    public BpmsServiceInstanceSQL lt(String name, Object value) {
    	super.lt(name, value);
        return this;
    }

    /**
     * 小于等于 less or equal
     *
     * @param name
     * @param value
     * @return
     */
    public BpmsServiceInstanceSQL le(String name, Object value) {
    	super.le(name, value);
        return this;
    }
    
    public BpmsServiceInstanceSQL isnull(String name) {
    	super.isnull(name);
        return this;
    } 

    public BpmsServiceInstanceSQL notNull(String name) {
    	super.notNull(name);
        return this;
    } 
    
    public BpmsServiceInstanceSQL empty(String name) {
    	super.empty(name);
        return this;
    } 
    
    public BpmsServiceInstanceSQL notEmpty(String name) {
    	super.notEmpty(name);
        return this;
    } 
    
    public BpmsServiceInstanceSQL add(Column column) {
    	super.add(column);
    	return this;
    }
    
    /**************************/
	
	public void addCriterion(String name, Object value, ConditionMode logic, String property, String typeHandler, String valueType) {
		 if (value == null) {
			 throw new RuntimeException("Value for " + property + " cannot be null");
		 }
		 add(Column.create(name, value, logic, typeHandler, valueType));
	}
   
	public void addCriterion(String name, Object value1, Object value2, ConditionMode logic, String property, String typeHandler, String valueType) {
		 if (value1 == null || value2 == null) {
			 throw new RuntimeException("Between values for " + property + " cannot be null");
		 }
		 add(Column.create(name, value1, value2, logic, typeHandler, valueType));
	}
		 
	public BpmsServiceInstanceSQL andInstanceIdIsNull() {
		isnull("instance_id");
		return this;
	}
	
	public BpmsServiceInstanceSQL andInstanceIdIsNotNull() {
		notNull("instance_id");
		return this;
	}
	
	public BpmsServiceInstanceSQL andInstanceIdIsEmpty() {
		empty("instance_id");
		return this;
	}

	public BpmsServiceInstanceSQL andInstanceIdIsNotEmpty() {
		notEmpty("instance_id");
		return this;
	}
      public BpmsServiceInstanceSQL andInstanceIdEqualTo(java.lang.Long value) {
          addCriterion("instance_id", value, ConditionMode.EQUAL, "instanceId", "java.lang.Long", "Float");
          return this;
      }

      public BpmsServiceInstanceSQL andInstanceIdNotEqualTo(java.lang.Long value) {
          addCriterion("instance_id", value, ConditionMode.NOT_EQUAL, "instanceId", "java.lang.Long", "Float");
          return this;
      }

      public BpmsServiceInstanceSQL andInstanceIdGreaterThan(java.lang.Long value) {
          addCriterion("instance_id", value, ConditionMode.GREATER_THEN, "instanceId", "java.lang.Long", "Float");
          return this;
      }

      public BpmsServiceInstanceSQL andInstanceIdGreaterThanOrEqualTo(java.lang.Long value) {
          addCriterion("instance_id", value, ConditionMode.GREATER_EQUAL, "instanceId", "java.lang.Long", "Float");
          return this;
      }

      public BpmsServiceInstanceSQL andInstanceIdLessThan(java.lang.Long value) {
          addCriterion("instance_id", value, ConditionMode.LESS_THEN, "instanceId", "java.lang.Long", "Float");
          return this;
      }

      public BpmsServiceInstanceSQL andInstanceIdLessThanOrEqualTo(java.lang.Long value) {
          addCriterion("instance_id", value, ConditionMode.LESS_EQUAL, "instanceId", "java.lang.Long", "Float");
          return this;
      }

      public BpmsServiceInstanceSQL andInstanceIdBetween(java.lang.Long value1, java.lang.Long value2) {
    	  addCriterion("instance_id", value1, value2, ConditionMode.BETWEEN, "instanceId", "java.lang.Long", "Float");
    	  return this;
      }

      public BpmsServiceInstanceSQL andInstanceIdNotBetween(java.lang.Long value1, java.lang.Long value2) {
          addCriterion("instance_id", value1, value2, ConditionMode.NOT_BETWEEN, "instanceId", "java.lang.Long", "Float");
          return this;
      }
        
      public BpmsServiceInstanceSQL andInstanceIdIn(List<java.lang.Long> values) {
          addCriterion("instance_id", values, ConditionMode.IN, "instanceId", "java.lang.Long", "Float");
          return this;
      }

      public BpmsServiceInstanceSQL andInstanceIdNotIn(List<java.lang.Long> values) {
          addCriterion("instance_id", values, ConditionMode.NOT_IN, "instanceId", "java.lang.Long", "Float");
          return this;
      }
	public BpmsServiceInstanceSQL andFlowCodeIsNull() {
		isnull("flow_code");
		return this;
	}
	
	public BpmsServiceInstanceSQL andFlowCodeIsNotNull() {
		notNull("flow_code");
		return this;
	}
	
	public BpmsServiceInstanceSQL andFlowCodeIsEmpty() {
		empty("flow_code");
		return this;
	}

	public BpmsServiceInstanceSQL andFlowCodeIsNotEmpty() {
		notEmpty("flow_code");
		return this;
	}
       public BpmsServiceInstanceSQL andFlowCodeLike(java.lang.String value) {
    	   addCriterion("flow_code", value, ConditionMode.FUZZY, "flowCode", "java.lang.String", "Float");
    	   return this;
      }

      public BpmsServiceInstanceSQL andFlowCodeNotLike(java.lang.String value) {
          addCriterion("flow_code", value, ConditionMode.NOT_FUZZY, "flowCode", "java.lang.String", "Float");
          return this;
      }
      public BpmsServiceInstanceSQL andFlowCodeEqualTo(java.lang.String value) {
          addCriterion("flow_code", value, ConditionMode.EQUAL, "flowCode", "java.lang.String", "String");
          return this;
      }

      public BpmsServiceInstanceSQL andFlowCodeNotEqualTo(java.lang.String value) {
          addCriterion("flow_code", value, ConditionMode.NOT_EQUAL, "flowCode", "java.lang.String", "String");
          return this;
      }

      public BpmsServiceInstanceSQL andFlowCodeGreaterThan(java.lang.String value) {
          addCriterion("flow_code", value, ConditionMode.GREATER_THEN, "flowCode", "java.lang.String", "String");
          return this;
      }

      public BpmsServiceInstanceSQL andFlowCodeGreaterThanOrEqualTo(java.lang.String value) {
          addCriterion("flow_code", value, ConditionMode.GREATER_EQUAL, "flowCode", "java.lang.String", "String");
          return this;
      }

      public BpmsServiceInstanceSQL andFlowCodeLessThan(java.lang.String value) {
          addCriterion("flow_code", value, ConditionMode.LESS_THEN, "flowCode", "java.lang.String", "String");
          return this;
      }

      public BpmsServiceInstanceSQL andFlowCodeLessThanOrEqualTo(java.lang.String value) {
          addCriterion("flow_code", value, ConditionMode.LESS_EQUAL, "flowCode", "java.lang.String", "String");
          return this;
      }

      public BpmsServiceInstanceSQL andFlowCodeBetween(java.lang.String value1, java.lang.String value2) {
    	  addCriterion("flow_code", value1, value2, ConditionMode.BETWEEN, "flowCode", "java.lang.String", "String");
    	  return this;
      }

      public BpmsServiceInstanceSQL andFlowCodeNotBetween(java.lang.String value1, java.lang.String value2) {
          addCriterion("flow_code", value1, value2, ConditionMode.NOT_BETWEEN, "flowCode", "java.lang.String", "String");
          return this;
      }
        
      public BpmsServiceInstanceSQL andFlowCodeIn(List<java.lang.String> values) {
          addCriterion("flow_code", values, ConditionMode.IN, "flowCode", "java.lang.String", "String");
          return this;
      }

      public BpmsServiceInstanceSQL andFlowCodeNotIn(List<java.lang.String> values) {
          addCriterion("flow_code", values, ConditionMode.NOT_IN, "flowCode", "java.lang.String", "String");
          return this;
      }
	public BpmsServiceInstanceSQL andInstanceCodeIsNull() {
		isnull("instance_code");
		return this;
	}
	
	public BpmsServiceInstanceSQL andInstanceCodeIsNotNull() {
		notNull("instance_code");
		return this;
	}
	
	public BpmsServiceInstanceSQL andInstanceCodeIsEmpty() {
		empty("instance_code");
		return this;
	}

	public BpmsServiceInstanceSQL andInstanceCodeIsNotEmpty() {
		notEmpty("instance_code");
		return this;
	}
       public BpmsServiceInstanceSQL andInstanceCodeLike(java.lang.String value) {
    	   addCriterion("instance_code", value, ConditionMode.FUZZY, "instanceCode", "java.lang.String", "String");
    	   return this;
      }

      public BpmsServiceInstanceSQL andInstanceCodeNotLike(java.lang.String value) {
          addCriterion("instance_code", value, ConditionMode.NOT_FUZZY, "instanceCode", "java.lang.String", "String");
          return this;
      }
      public BpmsServiceInstanceSQL andInstanceCodeEqualTo(java.lang.String value) {
          addCriterion("instance_code", value, ConditionMode.EQUAL, "instanceCode", "java.lang.String", "String");
          return this;
      }

      public BpmsServiceInstanceSQL andInstanceCodeNotEqualTo(java.lang.String value) {
          addCriterion("instance_code", value, ConditionMode.NOT_EQUAL, "instanceCode", "java.lang.String", "String");
          return this;
      }

      public BpmsServiceInstanceSQL andInstanceCodeGreaterThan(java.lang.String value) {
          addCriterion("instance_code", value, ConditionMode.GREATER_THEN, "instanceCode", "java.lang.String", "String");
          return this;
      }

      public BpmsServiceInstanceSQL andInstanceCodeGreaterThanOrEqualTo(java.lang.String value) {
          addCriterion("instance_code", value, ConditionMode.GREATER_EQUAL, "instanceCode", "java.lang.String", "String");
          return this;
      }

      public BpmsServiceInstanceSQL andInstanceCodeLessThan(java.lang.String value) {
          addCriterion("instance_code", value, ConditionMode.LESS_THEN, "instanceCode", "java.lang.String", "String");
          return this;
      }

      public BpmsServiceInstanceSQL andInstanceCodeLessThanOrEqualTo(java.lang.String value) {
          addCriterion("instance_code", value, ConditionMode.LESS_EQUAL, "instanceCode", "java.lang.String", "String");
          return this;
      }

      public BpmsServiceInstanceSQL andInstanceCodeBetween(java.lang.String value1, java.lang.String value2) {
    	  addCriterion("instance_code", value1, value2, ConditionMode.BETWEEN, "instanceCode", "java.lang.String", "String");
    	  return this;
      }

      public BpmsServiceInstanceSQL andInstanceCodeNotBetween(java.lang.String value1, java.lang.String value2) {
          addCriterion("instance_code", value1, value2, ConditionMode.NOT_BETWEEN, "instanceCode", "java.lang.String", "String");
          return this;
      }
        
      public BpmsServiceInstanceSQL andInstanceCodeIn(List<java.lang.String> values) {
          addCriterion("instance_code", values, ConditionMode.IN, "instanceCode", "java.lang.String", "String");
          return this;
      }

      public BpmsServiceInstanceSQL andInstanceCodeNotIn(List<java.lang.String> values) {
          addCriterion("instance_code", values, ConditionMode.NOT_IN, "instanceCode", "java.lang.String", "String");
          return this;
      }
	public BpmsServiceInstanceSQL andBusinessIdIsNull() {
		isnull("business_id");
		return this;
	}
	
	public BpmsServiceInstanceSQL andBusinessIdIsNotNull() {
		notNull("business_id");
		return this;
	}
	
	public BpmsServiceInstanceSQL andBusinessIdIsEmpty() {
		empty("business_id");
		return this;
	}

	public BpmsServiceInstanceSQL andBusinessIdIsNotEmpty() {
		notEmpty("business_id");
		return this;
	}
       public BpmsServiceInstanceSQL andBusinessIdLike(java.lang.String value) {
    	   addCriterion("business_id", value, ConditionMode.FUZZY, "businessId", "java.lang.String", "String");
    	   return this;
      }

      public BpmsServiceInstanceSQL andBusinessIdNotLike(java.lang.String value) {
          addCriterion("business_id", value, ConditionMode.NOT_FUZZY, "businessId", "java.lang.String", "String");
          return this;
      }
      public BpmsServiceInstanceSQL andBusinessIdEqualTo(java.lang.String value) {
          addCriterion("business_id", value, ConditionMode.EQUAL, "businessId", "java.lang.String", "String");
          return this;
      }

      public BpmsServiceInstanceSQL andBusinessIdNotEqualTo(java.lang.String value) {
          addCriterion("business_id", value, ConditionMode.NOT_EQUAL, "businessId", "java.lang.String", "String");
          return this;
      }

      public BpmsServiceInstanceSQL andBusinessIdGreaterThan(java.lang.String value) {
          addCriterion("business_id", value, ConditionMode.GREATER_THEN, "businessId", "java.lang.String", "String");
          return this;
      }

      public BpmsServiceInstanceSQL andBusinessIdGreaterThanOrEqualTo(java.lang.String value) {
          addCriterion("business_id", value, ConditionMode.GREATER_EQUAL, "businessId", "java.lang.String", "String");
          return this;
      }

      public BpmsServiceInstanceSQL andBusinessIdLessThan(java.lang.String value) {
          addCriterion("business_id", value, ConditionMode.LESS_THEN, "businessId", "java.lang.String", "String");
          return this;
      }

      public BpmsServiceInstanceSQL andBusinessIdLessThanOrEqualTo(java.lang.String value) {
          addCriterion("business_id", value, ConditionMode.LESS_EQUAL, "businessId", "java.lang.String", "String");
          return this;
      }

      public BpmsServiceInstanceSQL andBusinessIdBetween(java.lang.String value1, java.lang.String value2) {
    	  addCriterion("business_id", value1, value2, ConditionMode.BETWEEN, "businessId", "java.lang.String", "String");
    	  return this;
      }

      public BpmsServiceInstanceSQL andBusinessIdNotBetween(java.lang.String value1, java.lang.String value2) {
          addCriterion("business_id", value1, value2, ConditionMode.NOT_BETWEEN, "businessId", "java.lang.String", "String");
          return this;
      }
        
      public BpmsServiceInstanceSQL andBusinessIdIn(List<java.lang.String> values) {
          addCriterion("business_id", values, ConditionMode.IN, "businessId", "java.lang.String", "String");
          return this;
      }

      public BpmsServiceInstanceSQL andBusinessIdNotIn(List<java.lang.String> values) {
          addCriterion("business_id", values, ConditionMode.NOT_IN, "businessId", "java.lang.String", "String");
          return this;
      }
	public BpmsServiceInstanceSQL andFlowStatusIsNull() {
		isnull("flow_status");
		return this;
	}
	
	public BpmsServiceInstanceSQL andFlowStatusIsNotNull() {
		notNull("flow_status");
		return this;
	}
	
	public BpmsServiceInstanceSQL andFlowStatusIsEmpty() {
		empty("flow_status");
		return this;
	}

	public BpmsServiceInstanceSQL andFlowStatusIsNotEmpty() {
		notEmpty("flow_status");
		return this;
	}
      public BpmsServiceInstanceSQL andFlowStatusEqualTo(java.lang.Integer value) {
          addCriterion("flow_status", value, ConditionMode.EQUAL, "flowStatus", "java.lang.Integer", "Float");
          return this;
      }

      public BpmsServiceInstanceSQL andFlowStatusNotEqualTo(java.lang.Integer value) {
          addCriterion("flow_status", value, ConditionMode.NOT_EQUAL, "flowStatus", "java.lang.Integer", "Float");
          return this;
      }

      public BpmsServiceInstanceSQL andFlowStatusGreaterThan(java.lang.Integer value) {
          addCriterion("flow_status", value, ConditionMode.GREATER_THEN, "flowStatus", "java.lang.Integer", "Float");
          return this;
      }

      public BpmsServiceInstanceSQL andFlowStatusGreaterThanOrEqualTo(java.lang.Integer value) {
          addCriterion("flow_status", value, ConditionMode.GREATER_EQUAL, "flowStatus", "java.lang.Integer", "Float");
          return this;
      }

      public BpmsServiceInstanceSQL andFlowStatusLessThan(java.lang.Integer value) {
          addCriterion("flow_status", value, ConditionMode.LESS_THEN, "flowStatus", "java.lang.Integer", "Float");
          return this;
      }

      public BpmsServiceInstanceSQL andFlowStatusLessThanOrEqualTo(java.lang.Integer value) {
          addCriterion("flow_status", value, ConditionMode.LESS_EQUAL, "flowStatus", "java.lang.Integer", "Float");
          return this;
      }

      public BpmsServiceInstanceSQL andFlowStatusBetween(java.lang.Integer value1, java.lang.Integer value2) {
    	  addCriterion("flow_status", value1, value2, ConditionMode.BETWEEN, "flowStatus", "java.lang.Integer", "Float");
    	  return this;
      }

      public BpmsServiceInstanceSQL andFlowStatusNotBetween(java.lang.Integer value1, java.lang.Integer value2) {
          addCriterion("flow_status", value1, value2, ConditionMode.NOT_BETWEEN, "flowStatus", "java.lang.Integer", "Float");
          return this;
      }
        
      public BpmsServiceInstanceSQL andFlowStatusIn(List<java.lang.Integer> values) {
          addCriterion("flow_status", values, ConditionMode.IN, "flowStatus", "java.lang.Integer", "Float");
          return this;
      }

      public BpmsServiceInstanceSQL andFlowStatusNotIn(List<java.lang.Integer> values) {
          addCriterion("flow_status", values, ConditionMode.NOT_IN, "flowStatus", "java.lang.Integer", "Float");
          return this;
      }
	public BpmsServiceInstanceSQL andCreateUserIsNull() {
		isnull("create_user");
		return this;
	}
	
	public BpmsServiceInstanceSQL andCreateUserIsNotNull() {
		notNull("create_user");
		return this;
	}
	
	public BpmsServiceInstanceSQL andCreateUserIsEmpty() {
		empty("create_user");
		return this;
	}

	public BpmsServiceInstanceSQL andCreateUserIsNotEmpty() {
		notEmpty("create_user");
		return this;
	}
       public BpmsServiceInstanceSQL andCreateUserLike(java.lang.String value) {
    	   addCriterion("create_user", value, ConditionMode.FUZZY, "createUser", "java.lang.String", "Float");
    	   return this;
      }

      public BpmsServiceInstanceSQL andCreateUserNotLike(java.lang.String value) {
          addCriterion("create_user", value, ConditionMode.NOT_FUZZY, "createUser", "java.lang.String", "Float");
          return this;
      }
      public BpmsServiceInstanceSQL andCreateUserEqualTo(java.lang.String value) {
          addCriterion("create_user", value, ConditionMode.EQUAL, "createUser", "java.lang.String", "String");
          return this;
      }

      public BpmsServiceInstanceSQL andCreateUserNotEqualTo(java.lang.String value) {
          addCriterion("create_user", value, ConditionMode.NOT_EQUAL, "createUser", "java.lang.String", "String");
          return this;
      }

      public BpmsServiceInstanceSQL andCreateUserGreaterThan(java.lang.String value) {
          addCriterion("create_user", value, ConditionMode.GREATER_THEN, "createUser", "java.lang.String", "String");
          return this;
      }

      public BpmsServiceInstanceSQL andCreateUserGreaterThanOrEqualTo(java.lang.String value) {
          addCriterion("create_user", value, ConditionMode.GREATER_EQUAL, "createUser", "java.lang.String", "String");
          return this;
      }

      public BpmsServiceInstanceSQL andCreateUserLessThan(java.lang.String value) {
          addCriterion("create_user", value, ConditionMode.LESS_THEN, "createUser", "java.lang.String", "String");
          return this;
      }

      public BpmsServiceInstanceSQL andCreateUserLessThanOrEqualTo(java.lang.String value) {
          addCriterion("create_user", value, ConditionMode.LESS_EQUAL, "createUser", "java.lang.String", "String");
          return this;
      }

      public BpmsServiceInstanceSQL andCreateUserBetween(java.lang.String value1, java.lang.String value2) {
    	  addCriterion("create_user", value1, value2, ConditionMode.BETWEEN, "createUser", "java.lang.String", "String");
    	  return this;
      }

      public BpmsServiceInstanceSQL andCreateUserNotBetween(java.lang.String value1, java.lang.String value2) {
          addCriterion("create_user", value1, value2, ConditionMode.NOT_BETWEEN, "createUser", "java.lang.String", "String");
          return this;
      }
        
      public BpmsServiceInstanceSQL andCreateUserIn(List<java.lang.String> values) {
          addCriterion("create_user", values, ConditionMode.IN, "createUser", "java.lang.String", "String");
          return this;
      }

      public BpmsServiceInstanceSQL andCreateUserNotIn(List<java.lang.String> values) {
          addCriterion("create_user", values, ConditionMode.NOT_IN, "createUser", "java.lang.String", "String");
          return this;
      }
	public BpmsServiceInstanceSQL andCreateTimeIsNull() {
		isnull("create_time");
		return this;
	}
	
	public BpmsServiceInstanceSQL andCreateTimeIsNotNull() {
		notNull("create_time");
		return this;
	}
	
	public BpmsServiceInstanceSQL andCreateTimeIsEmpty() {
		empty("create_time");
		return this;
	}

	public BpmsServiceInstanceSQL andCreateTimeIsNotEmpty() {
		notEmpty("create_time");
		return this;
	}
      public BpmsServiceInstanceSQL andCreateTimeEqualTo(java.util.Date value) {
          addCriterion("create_time", value, ConditionMode.EQUAL, "createTime", "java.util.Date", "String");
          return this;
      }

      public BpmsServiceInstanceSQL andCreateTimeNotEqualTo(java.util.Date value) {
          addCriterion("create_time", value, ConditionMode.NOT_EQUAL, "createTime", "java.util.Date", "String");
          return this;
      }

      public BpmsServiceInstanceSQL andCreateTimeGreaterThan(java.util.Date value) {
          addCriterion("create_time", value, ConditionMode.GREATER_THEN, "createTime", "java.util.Date", "String");
          return this;
      }

      public BpmsServiceInstanceSQL andCreateTimeGreaterThanOrEqualTo(java.util.Date value) {
          addCriterion("create_time", value, ConditionMode.GREATER_EQUAL, "createTime", "java.util.Date", "String");
          return this;
      }

      public BpmsServiceInstanceSQL andCreateTimeLessThan(java.util.Date value) {
          addCriterion("create_time", value, ConditionMode.LESS_THEN, "createTime", "java.util.Date", "String");
          return this;
      }

      public BpmsServiceInstanceSQL andCreateTimeLessThanOrEqualTo(java.util.Date value) {
          addCriterion("create_time", value, ConditionMode.LESS_EQUAL, "createTime", "java.util.Date", "String");
          return this;
      }

      public BpmsServiceInstanceSQL andCreateTimeBetween(java.util.Date value1, java.util.Date value2) {
    	  addCriterion("create_time", value1, value2, ConditionMode.BETWEEN, "createTime", "java.util.Date", "String");
    	  return this;
      }

      public BpmsServiceInstanceSQL andCreateTimeNotBetween(java.util.Date value1, java.util.Date value2) {
          addCriterion("create_time", value1, value2, ConditionMode.NOT_BETWEEN, "createTime", "java.util.Date", "String");
          return this;
      }
        
      public BpmsServiceInstanceSQL andCreateTimeIn(List<java.util.Date> values) {
          addCriterion("create_time", values, ConditionMode.IN, "createTime", "java.util.Date", "String");
          return this;
      }

      public BpmsServiceInstanceSQL andCreateTimeNotIn(List<java.util.Date> values) {
          addCriterion("create_time", values, ConditionMode.NOT_IN, "createTime", "java.util.Date", "String");
          return this;
      }
	public BpmsServiceInstanceSQL andUpdateTimeIsNull() {
		isnull("update_time");
		return this;
	}
	
	public BpmsServiceInstanceSQL andUpdateTimeIsNotNull() {
		notNull("update_time");
		return this;
	}
	
	public BpmsServiceInstanceSQL andUpdateTimeIsEmpty() {
		empty("update_time");
		return this;
	}

	public BpmsServiceInstanceSQL andUpdateTimeIsNotEmpty() {
		notEmpty("update_time");
		return this;
	}
      public BpmsServiceInstanceSQL andUpdateTimeEqualTo(java.util.Date value) {
          addCriterion("update_time", value, ConditionMode.EQUAL, "updateTime", "java.util.Date", "String");
          return this;
      }

      public BpmsServiceInstanceSQL andUpdateTimeNotEqualTo(java.util.Date value) {
          addCriterion("update_time", value, ConditionMode.NOT_EQUAL, "updateTime", "java.util.Date", "String");
          return this;
      }

      public BpmsServiceInstanceSQL andUpdateTimeGreaterThan(java.util.Date value) {
          addCriterion("update_time", value, ConditionMode.GREATER_THEN, "updateTime", "java.util.Date", "String");
          return this;
      }

      public BpmsServiceInstanceSQL andUpdateTimeGreaterThanOrEqualTo(java.util.Date value) {
          addCriterion("update_time", value, ConditionMode.GREATER_EQUAL, "updateTime", "java.util.Date", "String");
          return this;
      }

      public BpmsServiceInstanceSQL andUpdateTimeLessThan(java.util.Date value) {
          addCriterion("update_time", value, ConditionMode.LESS_THEN, "updateTime", "java.util.Date", "String");
          return this;
      }

      public BpmsServiceInstanceSQL andUpdateTimeLessThanOrEqualTo(java.util.Date value) {
          addCriterion("update_time", value, ConditionMode.LESS_EQUAL, "updateTime", "java.util.Date", "String");
          return this;
      }

      public BpmsServiceInstanceSQL andUpdateTimeBetween(java.util.Date value1, java.util.Date value2) {
    	  addCriterion("update_time", value1, value2, ConditionMode.BETWEEN, "updateTime", "java.util.Date", "String");
    	  return this;
      }

      public BpmsServiceInstanceSQL andUpdateTimeNotBetween(java.util.Date value1, java.util.Date value2) {
          addCriterion("update_time", value1, value2, ConditionMode.NOT_BETWEEN, "updateTime", "java.util.Date", "String");
          return this;
      }
        
      public BpmsServiceInstanceSQL andUpdateTimeIn(List<java.util.Date> values) {
          addCriterion("update_time", values, ConditionMode.IN, "updateTime", "java.util.Date", "String");
          return this;
      }

      public BpmsServiceInstanceSQL andUpdateTimeNotIn(List<java.util.Date> values) {
          addCriterion("update_time", values, ConditionMode.NOT_IN, "updateTime", "java.util.Date", "String");
          return this;
      }
	public BpmsServiceInstanceSQL andDelFlagIsNull() {
		isnull("del_flag");
		return this;
	}
	
	public BpmsServiceInstanceSQL andDelFlagIsNotNull() {
		notNull("del_flag");
		return this;
	}
	
	public BpmsServiceInstanceSQL andDelFlagIsEmpty() {
		empty("del_flag");
		return this;
	}

	public BpmsServiceInstanceSQL andDelFlagIsNotEmpty() {
		notEmpty("del_flag");
		return this;
	}
       public BpmsServiceInstanceSQL andDelFlagLike(java.lang.String value) {
    	   addCriterion("del_flag", value, ConditionMode.FUZZY, "delFlag", "java.lang.String", "String");
    	   return this;
      }

      public BpmsServiceInstanceSQL andDelFlagNotLike(java.lang.String value) {
          addCriterion("del_flag", value, ConditionMode.NOT_FUZZY, "delFlag", "java.lang.String", "String");
          return this;
      }
      public BpmsServiceInstanceSQL andDelFlagEqualTo(java.lang.String value) {
          addCriterion("del_flag", value, ConditionMode.EQUAL, "delFlag", "java.lang.String", "String");
          return this;
      }

      public BpmsServiceInstanceSQL andDelFlagNotEqualTo(java.lang.String value) {
          addCriterion("del_flag", value, ConditionMode.NOT_EQUAL, "delFlag", "java.lang.String", "String");
          return this;
      }

      public BpmsServiceInstanceSQL andDelFlagGreaterThan(java.lang.String value) {
          addCriterion("del_flag", value, ConditionMode.GREATER_THEN, "delFlag", "java.lang.String", "String");
          return this;
      }

      public BpmsServiceInstanceSQL andDelFlagGreaterThanOrEqualTo(java.lang.String value) {
          addCriterion("del_flag", value, ConditionMode.GREATER_EQUAL, "delFlag", "java.lang.String", "String");
          return this;
      }

      public BpmsServiceInstanceSQL andDelFlagLessThan(java.lang.String value) {
          addCriterion("del_flag", value, ConditionMode.LESS_THEN, "delFlag", "java.lang.String", "String");
          return this;
      }

      public BpmsServiceInstanceSQL andDelFlagLessThanOrEqualTo(java.lang.String value) {
          addCriterion("del_flag", value, ConditionMode.LESS_EQUAL, "delFlag", "java.lang.String", "String");
          return this;
      }

      public BpmsServiceInstanceSQL andDelFlagBetween(java.lang.String value1, java.lang.String value2) {
    	  addCriterion("del_flag", value1, value2, ConditionMode.BETWEEN, "delFlag", "java.lang.String", "String");
    	  return this;
      }

      public BpmsServiceInstanceSQL andDelFlagNotBetween(java.lang.String value1, java.lang.String value2) {
          addCriterion("del_flag", value1, value2, ConditionMode.NOT_BETWEEN, "delFlag", "java.lang.String", "String");
          return this;
      }
        
      public BpmsServiceInstanceSQL andDelFlagIn(List<java.lang.String> values) {
          addCriterion("del_flag", values, ConditionMode.IN, "delFlag", "java.lang.String", "String");
          return this;
      }

      public BpmsServiceInstanceSQL andDelFlagNotIn(List<java.lang.String> values) {
          addCriterion("del_flag", values, ConditionMode.NOT_IN, "delFlag", "java.lang.String", "String");
          return this;
      }
	public BpmsServiceInstanceSQL andUserCodeIsNull() {
		isnull("user_code");
		return this;
	}
	
	public BpmsServiceInstanceSQL andUserCodeIsNotNull() {
		notNull("user_code");
		return this;
	}
	
	public BpmsServiceInstanceSQL andUserCodeIsEmpty() {
		empty("user_code");
		return this;
	}

	public BpmsServiceInstanceSQL andUserCodeIsNotEmpty() {
		notEmpty("user_code");
		return this;
	}
       public BpmsServiceInstanceSQL andUserCodeLike(java.lang.String value) {
    	   addCriterion("user_code", value, ConditionMode.FUZZY, "userCode", "java.lang.String", "String");
    	   return this;
      }

      public BpmsServiceInstanceSQL andUserCodeNotLike(java.lang.String value) {
          addCriterion("user_code", value, ConditionMode.NOT_FUZZY, "userCode", "java.lang.String", "String");
          return this;
      }
      public BpmsServiceInstanceSQL andUserCodeEqualTo(java.lang.String value) {
          addCriterion("user_code", value, ConditionMode.EQUAL, "userCode", "java.lang.String", "String");
          return this;
      }

      public BpmsServiceInstanceSQL andUserCodeNotEqualTo(java.lang.String value) {
          addCriterion("user_code", value, ConditionMode.NOT_EQUAL, "userCode", "java.lang.String", "String");
          return this;
      }

      public BpmsServiceInstanceSQL andUserCodeGreaterThan(java.lang.String value) {
          addCriterion("user_code", value, ConditionMode.GREATER_THEN, "userCode", "java.lang.String", "String");
          return this;
      }

      public BpmsServiceInstanceSQL andUserCodeGreaterThanOrEqualTo(java.lang.String value) {
          addCriterion("user_code", value, ConditionMode.GREATER_EQUAL, "userCode", "java.lang.String", "String");
          return this;
      }

      public BpmsServiceInstanceSQL andUserCodeLessThan(java.lang.String value) {
          addCriterion("user_code", value, ConditionMode.LESS_THEN, "userCode", "java.lang.String", "String");
          return this;
      }

      public BpmsServiceInstanceSQL andUserCodeLessThanOrEqualTo(java.lang.String value) {
          addCriterion("user_code", value, ConditionMode.LESS_EQUAL, "userCode", "java.lang.String", "String");
          return this;
      }

      public BpmsServiceInstanceSQL andUserCodeBetween(java.lang.String value1, java.lang.String value2) {
    	  addCriterion("user_code", value1, value2, ConditionMode.BETWEEN, "userCode", "java.lang.String", "String");
    	  return this;
      }

      public BpmsServiceInstanceSQL andUserCodeNotBetween(java.lang.String value1, java.lang.String value2) {
          addCriterion("user_code", value1, value2, ConditionMode.NOT_BETWEEN, "userCode", "java.lang.String", "String");
          return this;
      }
        
      public BpmsServiceInstanceSQL andUserCodeIn(List<java.lang.String> values) {
          addCriterion("user_code", values, ConditionMode.IN, "userCode", "java.lang.String", "String");
          return this;
      }

      public BpmsServiceInstanceSQL andUserCodeNotIn(List<java.lang.String> values) {
          addCriterion("user_code", values, ConditionMode.NOT_IN, "userCode", "java.lang.String", "String");
          return this;
      }
}