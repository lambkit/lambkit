/**
 * Copyright (c) 2015-2017, Henry Yang 杨勇 (gismail@foxmail.com).
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.lambkit.module.bpms.row;

import com.lambkit.db.RowModel;

/**
 * @author yangyong 
 * @website: www.lambkit.com
 * @email: gismail@foxmail.com
 * @date ${date}
 * @version 1.0
 * @since 1.0
 */

public class BpmsRuleChain extends RowModel<BpmsRuleChain> {
	public BpmsRuleChain() {
		setTableName("bpms_rule_chain");
		setPrimaryKey("chain_id");
	}
	public java.lang.Long getChainId() {
		return this.get("chain_id");
	}
	public void setChainId(java.lang.Long chainId) {
		this.set("chain_id", chainId);
	}
	public java.lang.String getApplicationName() {
		return this.get("application_name");
	}
	public void setApplicationName(java.lang.String applicationName) {
		this.set("application_name", applicationName);
	}
	public java.lang.String getChainName() {
		return this.get("chain_name");
	}
	public void setChainName(java.lang.String chainName) {
		this.set("chain_name", chainName);
	}
	public java.lang.String getChainDesc() {
		return this.get("chain_desc");
	}
	public void setChainDesc(java.lang.String chainDesc) {
		this.set("chain_desc", chainDesc);
	}
	public java.lang.String getElData() {
		return this.get("el_data");
	}
	public void setElData(java.lang.String elData) {
		this.set("el_data", elData);
	}
	public java.lang.Integer getStatus() {
		return this.get("status");
	}
	public void setStatus(java.lang.Integer status) {
		this.set("status", status);
	}
	public java.util.Date getUpdateTime() {
		return this.get("update_time");
	}
	public void setUpdateTime(java.util.Date updateTime) {
		this.set("update_time", updateTime);
	}
	public java.lang.String getUpdater() {
		return this.get("updater");
	}
	public void setUpdater(java.lang.String updater) {
		this.set("updater", updater);
	}
}
