/**
 * Copyright (c) 2015-2017, Henry Yang 杨勇 (gismail@foxmail.com).
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.lambkit.module.bpms.row;

import com.lambkit.db.RowModel;

/**
 * @author yangyong 
 * @website: www.lambkit.com
 * @email: gismail@foxmail.com
 * @date ${date}
 * @version 1.0
 * @since 1.0
 */

public class BpmsServiceFlowNodeSkip extends RowModel<BpmsServiceFlowNodeSkip> {
	public BpmsServiceFlowNodeSkip() {
		setTableName("bpms_service_flow_node_skip");
		setPrimaryKey("node_skip_id");
	}
	public java.lang.Long getNodeSkipId() {
		return this.get("node_skip_id");
	}
	public void setNodeSkipId(java.lang.Long nodeSkipId) {
		this.set("node_skip_id", nodeSkipId);
	}
	public java.lang.String getFlowCode() {
		return this.get("flow_code");
	}
	public void setFlowCode(java.lang.String flowCode) {
		this.set("flow_code", flowCode);
	}
	public java.lang.String getNowNodeCode() {
		return this.get("now_node_code");
	}
	public void setNowNodeCode(java.lang.String nowNodeCode) {
		this.set("now_node_code", nowNodeCode);
	}
	public java.lang.Integer getNowNodeType() {
		return this.get("now_node_type");
	}
	public void setNowNodeType(java.lang.Integer nowNodeType) {
		this.set("now_node_type", nowNodeType);
	}
	public java.lang.String getNextNodeCode() {
		return this.get("next_node_code");
	}
	public void setNextNodeCode(java.lang.String nextNodeCode) {
		this.set("next_node_code", nextNodeCode);
	}
	public java.lang.Integer getNextNodeType() {
		return this.get("next_node_type");
	}
	public void setNextNodeType(java.lang.Integer nextNodeType) {
		this.set("next_node_type", nextNodeType);
	}
	public java.lang.String getSkipName() {
		return this.get("skip_name");
	}
	public void setSkipName(java.lang.String skipName) {
		this.set("skip_name", skipName);
	}
	public java.lang.String getSkipType() {
		return this.get("skip_type");
	}
	public void setSkipType(java.lang.String skipType) {
		this.set("skip_type", skipType);
	}
	public java.lang.String getSkipCondition() {
		return this.get("skip_condition");
	}
	public void setSkipCondition(java.lang.String skipCondition) {
		this.set("skip_condition", skipCondition);
	}
	public java.lang.String getCoordinate() {
		return this.get("coordinate");
	}
	public void setCoordinate(java.lang.String coordinate) {
		this.set("coordinate", coordinate);
	}
	public java.util.Date getCreateTime() {
		return this.get("create_time");
	}
	public void setCreateTime(java.util.Date createTime) {
		this.set("create_time", createTime);
	}
	public java.util.Date getUpdateTime() {
		return this.get("update_time");
	}
	public void setUpdateTime(java.util.Date updateTime) {
		this.set("update_time", updateTime);
	}
	public java.lang.String getDelFlag() {
		return this.get("del_flag");
	}
	public void setDelFlag(java.lang.String delFlag) {
		this.set("del_flag", delFlag);
	}
	public java.lang.String getUserCode() {
		return this.get("user_code");
	}
	public void setUserCode(java.lang.String userCode) {
		this.set("user_code", userCode);
	}
}
