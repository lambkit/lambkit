package com.lambkit.test;

import cn.hutool.db.dialect.DriverNamePool;
import com.alibaba.druid.pool.DruidDataSource;
import com.lambkit.db.hutool.HutoolDb;
import com.lambkit.generator.Generator;
import com.lambkit.generator.GeneratorConfig;
import com.lambkit.generator.GeneratorType;

import java.util.HashMap;
import java.util.Map;

/**
 * @author yangyong(孤竹行)
 */
public class GeneratorKit {
    public static void main(String[] args) {
        DruidDataSource ds2 = new DruidDataSource();
        ds2.setUrl("jdbc:postgresql://localhost:5432/lambkit2");
        ds2.setUsername("postgres");
        ds2.setPassword("spFAyCBalefzzoBK");
        HutoolDb hutoolDb = new HutoolDb(ds2, DriverNamePool.DRIVER_POSTGRESQL);
        //
        GeneratorConfig config = new GeneratorConfig();
        // 模板地址
        String templatePath = "/template/jfinal";
        // 模板所在目录
        String rootPath = System.getProperty("user.dir");
        // 生成java代码的存放地址
        config.setOutRootDir(rootPath + "/lambkit-module/lambkit-bpms/src/main/java");
        // 生成前端文件文件夹
        config.setWebpages("app");
        // 表格配置方式
        //config.setMgrdb("dmsMgrdb");
        // 选择一种模板语言
        config.setEngine(GeneratorConfig.TYPE_JFINAL);
        // 选择一种处理引擎
        config.setType(GeneratorType.DB);
        // 包地址
        config.setBasepackage("com.lambkit.module.bpms");
        // 配置
        Map<String, Object> options = new HashMap<String, Object>();
        // 需要去掉的前缀
        options.put("tableRemovePrefixes", "lk_");

        options.put("includedTables", "bpms_rule_chain," +
                "bpms_rule_script," +
                "bpms_service_catalog," +
                "bpms_service_flow," +
                "bpms_service_flow_node," +
                "bpms_service_flow_node_data," +
                "bpms_service_flow_node_skip," +
                "bpms_service_instance," +
                "bpms_service_instance_task," +
                "bpms_service_node," +
                "bpms_service_task");
        options.put("moduleName", "Bpms");
        options.put("propName", "bpms");

        // 不进行生成的模板
        options.put("excludedTemplate", "#(moduleName)Config.java, #(classname).java,SQL.java,Module.java");

        // 代码生成
        Generator.execute(hutoolDb, rootPath, templatePath, options, config);
    }
}
