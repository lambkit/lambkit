##create bean,this is a class
#parse("/template/java_copyright.include")
package $!{basepackage}.row;

import com.lambkit.db.RowModel;

#parse("/template/java_author.include")
public class ${classname} extends RowModel<${classname}> {
	public ${classname}() {
		setTableName("${tableName}");
		setPrimaryKey("${primaryKey}");
	}
#foreach($column in $columns)##
	public ${column.javaType} get${column.upperName}() {
		return this.get("${column.name}");
	}
	public void set${column.upperName}(${column.javaType} ${column.attrName}) {
		this.set("${column.name}", ${column.attrName});
	}
#end##
}
