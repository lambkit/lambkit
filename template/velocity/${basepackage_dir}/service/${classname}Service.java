##create bean,this is a class
#parse("/template/java_copyright.include")
package $!{basepackage}.service;

import com.lambkit.core.Lambkit;
import com.lambkit.db.DbPool;
import com.lambkit.db.IDaoService;
import com.lambkit.db.IRowDao;
import $!{basepackage}.${moduleName}Config;
import $!{basepackage}.row.${classname};

#parse("/template/java_author.include")
public class ${classname}Service implements IDaoService<${classname}> {
	public IRowDao<${classname}> dao() {
		String dbPoolName = Lambkit.config(${moduleName}Config.class).getDbPoolName();
		return DbPool.use(dbPoolName).dao(${classname}.class);
	}

	public IRowDao<${classname}> dao(String dbPoolName) {
		return DbPool.use(dbPoolName).dao(${classname}.class);
	}
}
